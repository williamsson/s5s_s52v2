

#ifndef EBMMEMDEFS_H_
#define EBMMEMDEFS_H_

#define EBMMEMDEFS_VERSION 20200525

// ebmMemHandle type
#define MEM_TYPES		14			// no of MEM_TYPE_xxx
#define MEM_TYPE_STRING		1
#define MEM_TYPE_BIT		2
#define MEM_TYPE_UINT8		3
#define MEM_TYPE_INT8		4
#define MEM_TYPE_UINT16		5
#define MEM_TYPE_INT16		6
#define MEM_TYPE_UINT32		7
#define MEM_TYPE_INT32		8
#define MEM_TYPE_FLOAT		9
#define MEM_TYPE_DOUBLE		10
#define MEM_TYPE_UINT64		11
#define MEM_TYPE_UINTx3		12
#define MEM_TYPE_UINTx4		13
#define MEM_TYPE_UINTx5		14

// memtype FIFO, 0x100000 bit is set
#define MEM_TYPE_FIFO_STRING	33
#define MEM_TYPE_FIFO_BIT		34
#define MEM_TYPE_FIFO_UINT8		35
#define MEM_TYPE_FIFO_INT8		36
#define MEM_TYPE_FIFO_UINT16	37
#define MEM_TYPE_FIFO_INT16		38
#define MEM_TYPE_FIFO_UINT32	39
#define MEM_TYPE_FIFO_INT32		40
#define MEM_TYPE_FIFO_FLOAT		41
#define MEM_TYPE_FIFO_DOUBLE	42
#define MEM_TYPE_FIFO_UINT64	43
#define MEM_TYPE_FIFO_UINTx3	44
#define MEM_TYPE_FIFO_UINTx4	45
#define MEM_TYPE_FIFO_UINTx5	46


//----- MEM_TYPE_STRING -----------------------------
#define EBMMEM_STRING_SIZE 2
#define EBMMEM_STRING_APP_NAME 				0
#define EBMMEM_STRING_BOOT_NAME				1

//----- MEM_TYPE_BIT --------------------------------
#define EBMMEM_BIT_SIZE 					   80
#define EBMMEM_BIT_BOOTLOADER				   0
#define EBMMEM_BIT_REBOOT					   1
#define EBMMEM_BIT_SUBSCRIBE_FWUPDATE		2
#define EBMMEM_BIT_SUBSCRIBE_ALL			   3
#define EBMMEM_BIT_SEND_ALL_ONCE			   4

#define EBMMEM_BIT_INPUT_START				10
// input 1-28  	: 10-37
#define EBMMEM_BIT_INPUT_FAULT				38
#define EBMMEM_BIT_OUTPUT_START				40
// output 1-18 	: 40-57
#define EBMMEM_BIT_FORCE_OUTPUT_START		58
// output 1-18 	: 58-75
#define EBMMEM_BIT_GLUE1_ENABLE				76
#define EBMMEM_BIT_GLUE1_CLEAR_QUEUE		77
#define EBMMEM_BIT_GLUE2_ENABLE				78
#define EBMMEM_BIT_GLUE2_CLEAR_QUEUE		79

//----- MEM_TYPE_UINT8 -------------------------------
#define EBMMEM_UINT8_SIZE 				   	8
#define EBMMEM_UINT8_BOOT_STATE				0
#define EBMMEM_UINT8_FWUPDATE_CMD	 		1
#define FWUPDATE_CMD_NONE				   	0
#define FWUPDATE_CMD_RECEIVE_BOOTLOADER 	1
#define FWUPDATE_CMD_RECEIVE_APPLICATION 	2
#define FWUPDATE_CMD_RECEIVE_FINISHED		3
#define FWUPDATE_CMD_BURN_BOOTLOADER		4
#define FWUPDATE_CMD_BURN_APPLICATION		5
#define EBMMEM_UINT8_FWUPDATE_RESPONSE		2
#define FWUPDATE_RESPONSE_NONE				0
#define FWUPDATE_RESPONSE_RECEIVING			1
#define FWUPDATE_RESPONSE_RECEIVED_OK		2
#define FWUPDATE_RESPONSE_BURNING			3
#define FWUPDATE_RESPONSE_BURN_OK			4
#define FWUPDATE_RESPONSE_ERROR_WRITE		10
#define FWUPDATE_RESPONSE_ERROR_INTEGRITY	11
#define FWUPDATE_RESPONSE_ERROR_FIFO		12
#define EBMMEM_UINT8_FWUPDATE_PROGRESS		3
#define EBMMEM_UINT8_CONFIG_INPUT_01		4
#define EBMMEM_UINT8_CONFIG_INPUT_02		5
#define EBMMEM_UINT8_CONFIG_INPUT_03		6
#define EBMMEM_UINT8_CONFIG_INPUT_04		7

//----- MEM_TYPE_INT8 --------------------------------
#define EBMMEM_INT8_SIZE 0

//----- MEM_TYPE_UINT16 ------------------------------
#define EBMMEM_UINT16_SIZE 3
#define EBMMEM_UINT16_FIFO_FWUPDATE_FREE	0
#define EBMMEM_UINT16_ANALOG_OUT1_GAIN		1
#define EBMMEM_UINT16_ANALOG_OUT1_OFFSET	2

//----- MEM_TYPE_INT16 -------------------------------
#define EBMMEM_INT16_SIZE 0

//----- MEM_TYPE_UINT32 --------------------------------
#define EBMMEM_UINT32_SIZE                46
#define EBMMEM_UINT32_APP_VERSION 			0
#define EBMMEM_UINT32_BOOT_VERSION 			1
#define EBMMEM_UINT32_PASSWORD	 			2
#define EBMMEM_UINT32_HW_CPU_ARTNO 			3
#define EBMMEM_UINT32_HW_CPU_VERSION 		4
#define EBMMEM_UINT32_HW_CPU_SERIALNO 		5
#define EBMMEM_UINT32_HW_CPU_MFD	 		   6
#define EBMMEM_UINT32_USER_IP		 		   7
#define EBMMEM_UINT32_USER_NETMASK	 		8
#define EBMMEM_UINT32_USER_GATEWAY	 		9

#define EBMMEM_UINT32_WATCHDOG_TIME 		10
#define EBMMEM_UINT32_WATCHDOG_OUTPUTS 	11
#define EBMMEM_UINT32_ENCODER_1		 		12
#define EBMMEM_UINT32_ENCODER_2 			   13

#define EBMMEM_UINT32_GLUE1_PATTERN			20		// first page, second page: 1=1,1  2=2,1  3=2,2  4=3,2  5=3,3
#define EBMMEM_UINT32_GLUE1_OFFSET			21		// distance between mark and first glue dot (pulses)
#define EBMMEM_UINT32_GLUE1_SEP				22		// distance between glue dots  (pulses)
#define EBMMEM_UINT32_GLUE1_SIZE			   23		// Activated time (ms)
#define EBMMEM_UINT32_GLUE1_NOZZLE_TIME   24		// Nozzle activation time (ms)
#define EBMMEM_UINT32_GLUE1_MIN_MARK_SIZE	25		// Minimum mark size to be accepted (pulses)
#define EBMMEM_UINT32_GLUE1_MAX_MARK_SIZE	26		// Maximum mark size to be accepted (pulses)
#define EBMMEM_UINT32_GLUE1_MARK_MODE		27		// Glue mark mode, 0 = reading marks from input 2, 1 = extra cancel mark on input 3
#define EBMMEM_UINT32_GLUE1_SIGNAL_LEVEL_ON_MARK	28	// Signal level from mark sensor when reading a mark, 0 = inactive, 1 = active
#define EBMMEM_UINT32_GLUE1_MARK_SIZE		29		// Size of last read mark (pulses)
#define EBMMEM_UINT32_GLUE1_DOT_COUNT		30		// Glue dots counter
#define EBMMEM_UINT32_GLUE1_IN_QUEUE		31		// No of items in queue

#define EBMMEM_UINT32_GLUE2_PATTERN			32		// first page, second page: 1=1,1  2=2,1  3=2,2  4=3,2  5=3,3
#define EBMMEM_UINT32_GLUE2_OFFSET			33		// distance between mark and first glue dot (pulses)
#define EBMMEM_UINT32_GLUE2_SEP				34		// distance between glue dots  (pulses)
#define EBMMEM_UINT32_GLUE2_SIZE			   35		// Activated time (ms)
#define EBMMEM_UINT32_GLUE2_NOZZLE_TIME	36		// Nozzle activation time (ms)
#define EBMMEM_UINT32_GLUE2_MIN_MARK_SIZE	37		// Minimum mark size to be accepted (pulses)
#define EBMMEM_UINT32_GLUE2_MAX_MARK_SIZE	38		// Maximum mark size to be accepted (pulses)
#define EBMMEM_UINT32_GLUE2_MARK_MODE		39		// Glue mark mode, 0 = reading marks from input 2, 1 = extra cancel mark on input 3
#define EBMMEM_UINT32_GLUE2_SIGNAL_LEVEL_ON_MARK	40	// Signal level from mark sensor when reading a mark, 0 = inactive, 1 = active
#define EBMMEM_UINT32_GLUE2_MARK_SIZE		41		// Size of last read mark (pulses)
#define EBMMEM_UINT32_GLUE2_DOT_COUNT		42		// Glue dots counter
#define EBMMEM_UINT32_GLUE2_IN_QUEUE		43		// No of items in queue

#define EBMMEM_UINT32_GLUE_PULSE_SPEED		44		// Speed of transport pulses (pulses/s)
#define EBMMEM_UINT32_GLUE_TEST_PARAM		45

//----- MEM_TYPE_INT ---------------------------------
#define EBMMEM_INT32_SIZE 0

//----- MEM_TYPE_FLOAT -------------------------------
#define EBMMEM_FLOAT_SIZE 7
#define EBMMEM_FLOAT_ANALOG_IN1 		0
#define EBMMEM_FLOAT_ANALOG_IN2 		1
#define EBMMEM_FLOAT_ANALOG_IN3 		2
#define EBMMEM_FLOAT_ANALOG_OUT1 		3
#define EBMMEM_FLOAT_ANALOG_OUT2 		4
#define EBMMEM_FLOAT_ANALOG_OUT3 		5
#define EBMMEM_FLOAT_ANALOG_OUT4 		6
//----- MEM_TYPE_DOUBLE -------------------------------
#define EBMMEM_DOUBLE_SIZE 0
//----- MEM_TYPE_UINT64 --------
#define EBMMEM_UINT64_SIZE 1
#define EBMMEM_UINT64_MAC_ADDRESS		0
//----- MEM_TYPE_UINTx3 --------
#define EBMMEM_UINTx3_SIZE 0
//----- MEM_TYPE_UINTx4 --------
#define EBMMEM_UINTx4_SIZE 0
//----- MEM_TYPE_UINTx5 --------
#define EBMMEM_UINTx5_SIZE 0

//-------------FIFO TYPES---------------------------------

//----- MEM_TYPE_FIFO_UINT8 --------------------------
#define EBMMEM_FIFO_UINT8_FIRMWARE		0
#define EBMMEM_FIFO_UINT8_UART1_TX		1
#define EBMMEM_FIFO_UINT8_UART1_RX		2
#define EBMMEM_FIFO_UINT8_UART2_TX		3
#define EBMMEM_FIFO_UINT8_UART2_RX		4


// ------ MEM_TYPE_FIFO_UINT16------------------------
#define EBMMEM_FIFO_UINT16_SUBSCRIBE	0
#define EBMMEM_FIFO_UINT16_SEND_MEM		1
#define EBMMEM_FIFO_UINT16_GLUE1_MARK_SIZE	2
#define EBMMEM_FIFO_UINT16_GLUE2_MARK_SIZE	3

//----- UINTx5FIFO -----------------------------------
#define EBMMEM_FIFO_UINTx5_INPUT_INT	0

#endif /* EBMMEMDEFS_H_ */


#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <AsUDP.h>
#include <fsm.hpp>
#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <ebmMemDefs.h>
#include <ebmCommPacket.h>
#include <AsMath.h>
//#include <cstdarg>

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFFF; 
//bit32ValueMask_t bitMaskTest;
//bit32ValueMask_t bitMask;

#define LOG_BUF_SIZE 20
#define UNINITIALIZED 99999
       
#define CYCLIC           0,""
#define TERMINATE        1,""
#define ADD_MSG          2

void ecPacket_Init(ecPacket_t *ecPacket, unsigned int seqNo, unsigned short maxLength, bool useMaxLength, unsigned char key);
bool ecPacket_AddHandleData(ecPacket_t *ecPacket, unsigned short handle, char *data, unsigned short dataSize);
void ecPacket_Finalize(ecPacket_t *ecPacket);

bool ecPacket_CheckReceivedPacket(ecPacket_t *ecPacket, unsigned short receivedLength, unsigned int expectedSeqNo, unsigned char key);
bool ecPacket_GetNextHandleData(ecPacket_t *ecPacket, ecBlock_t **block, unsigned short **handle, char **data, unsigned short **dataSize);

unsigned short ecPacket_GetBytesAvailable(ecPacket_t *ecPacket);
char *ecPacket_GetBlock(ecPacket_t *ecPacket,int blockNo);
char *ecPacket_GetLastBlock(ecPacket_t *ecPacket);



bool ebmConnection_Receive(char * buf,UINT len, UINT eSeq);
void handleSeq(void);

void setBit (UDINT x, UDINT * var);
void clearBit (UDINT x, UDINT * var);
BOOL readBit (UDINT x, UDINT var);
REAL rabs(REAL a);
void histHandler (REAL value, REAL min, REAL max, BOOL res);
void calc_Avg_Var_stdDev (REAL x[100]);
REAL compenstationCurve (REAL x);
REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max);
UINT sendParameterBit (UINT param, BOOL  data, UINT func);
void sendParameterU8  (UINT param, USINT data, UINT func);
void sendParameterU16 (UINT param, UINT  data, UINT func);
void sendParameterI16 (UINT param, INT   data, UINT func);
UINT sendParameterU32 (UINT param, UDINT data, UINT func);
void sendParameterI32 (UINT param, DINT  data, UINT func);

#define SEND_ON_MODIFIED 0
#define FORCE_SEND 1
#define INITIALIZE 99
#define SECONDS_UNIT 0.0072

UDINT localCopyU32[EBMMEM_UINT32_SIZE];
BOOL  localCopyU32CheckEnable[EBMMEM_UINT32_SIZE];
BOOL  localCopyBit[EBMMEM_BIT_SIZE];

void UDP(int logAction, char *txt);
void glueHandler(void);

//unsigned char UDPLogIP[] = "192.168.0.11"; //h�kans lina
//unsigned char UDPLogIP[] = "192.168.0.10"; //mickes lina
//unsigned char UDPLogIP[] = "192.168.111.40"; //LSI lina + italy,
unsigned char UDPLogIP[] = "192.168.113.40"; //LSI lina 2, micke
UINT UDPLogPort = 10990;
ecPacket_t ecPacket;

void _INIT ProgramInit(void)
{
   seq = 0;
   //bitMask.value = 0x0000;
   //bitMask.mask = 0x0000;
   ebmGlue.reading.rMsg = ebmGlue.reading.sMsg = 0;
   sendReciveCycleMin = 10000000.0;
   sprintf(UDPDebug, "Program Init");
   ebmGlue.setting.offset.size = 7.5;
   ebmGlue.setting.glueGun.size = 3.0; //10.0;
   ebmGlue.setting.offset.pattern = 1;
   //ebmGlue.setting.offset.nozzleDelay = 39;
   ebmGlue.setting.offset.nozzleDelay = 47; //43ms 47ms new offset assembly
   ebmGlue.setting.glueGun.nozzleDelay = 3; //ms
   ebmGlue.reading.sendAllCnt = 0;
   offsetComp = 41.5;
   ghState = 0;
   
   for (int i=0 ; i < EBMMEM_UINT32_SIZE ; i++)
   {
      localCopyU32CheckEnable[i] = false;
   }
   //ebmGlue.setting.scale = 14.46; //7.23 pulses per milimeter
}
 
void _CYCLIC ProgramCyclic(void)
{
   glueHandler();
   UDP(CYCLIC);
}

void _EXIT ProgramExit(void)
{
   UDP(TERMINATE);
}

void ecPacket_Init(ecPacket_t *ecPacket, unsigned int seqNo, unsigned short maxLength, bool useMaxLength, unsigned char key)
{
   bzero(ecPacket, sizeof(ecPacket_t));
   if (key == 0)
      ecPacket->header.key = COMMON_PACKET_KEY;  // 'C'
   else
      ecPacket->header.key = key;
   ecPacket->header.version = PACKET_VERSION;
   ecPacket->header.type = PACKET_TYPE_DATA;
   ecPacket->header.length = sizeof(ecPacket->header);
   if ((maxLength == 0) || (maxLength > sizeof(ecPacket_t)))
      maxLength = sizeof(ecPacket_t);
   ecPacket->header.maxLength = maxLength;
   ecPacket->header.useMaxLength = useMaxLength;
   ecPacket->header.seqNo = seqNo;
}

bool ecPacket_AddHandleData(ecPacket_t *ecPacket, unsigned short handle, char *data, unsigned short dataSize)
{
   bool addBlock = false;
   char *p = ecPacket_GetLastBlock(ecPacket);

   if (p == NULL)	// empty packet
   {
      p = ecPacket->data;
      addBlock = true;
   }
   else if ( ((((ecBlock_t *)p)->serialData) || (((ecBlock_t *)p)->count == 1)) &&
      (*((unsigned short *)&(((ecBlock_t *)p)->data)) == handle))	// if serialData or first data in block and same handle, just add it without new handle
   {
      if ((p + ((ecBlock_t *)p)->length + dataSize - (char *)ecPacket) >= ecPacket->header.maxLength)
         return false;
      ((ecBlock_t *)p)->serialData = true;
      memcpy(p + ((ecBlock_t *)p)->length,data,dataSize);
      ((ecBlock_t *)p)->count += 1;
      ((ecBlock_t *)p)->length += dataSize;
      ecPacket->header.length += dataSize;
      return true;
   }
   else if (((ecBlock_t *)p)->dataSize != dataSize)  // add new block with new dataSize
   {
      p = p + ((ecBlock_t *)p)->length;	// Goto end
      addBlock = true;
   }
   if (addBlock)
   {
      if ((p + sizeof(ecBlock_t) + sizeof(handle) + dataSize - (char *)ecPacket) >= ecPacket->header.maxLength)
         return false;

      ((ecBlock_t *)p)->dataSize = dataSize;
      ((ecBlock_t *)p)->length = sizeof(ecBlock_t) - 1;	// -1, the last unsigned char data is not used
      ecPacket->header.length += sizeof(ecBlock_t) - 1;
      ((ecBlock_t *)p)->count = 0;
   }
   // Add data into block
   if ((p + ((ecBlock_t *)p)->length + sizeof(handle) + dataSize - (char *)ecPacket) >= ecPacket->header.maxLength)
      return false;
   memcpy(p + ((ecBlock_t *)p)->length,&handle,sizeof(handle));
   memcpy(p + ((ecBlock_t *)p)->length + sizeof(handle),data,dataSize);
   ((ecBlock_t *)p)->count += 1;
   ((ecBlock_t *)p)->length += sizeof(handle) + dataSize;
   ecPacket->header.length += sizeof(handle) + dataSize;

   return true;
}

void ecPacket_Finalize(ecPacket_t *ecPacket)
{
   ((char *)ecPacket)[ecPacket->header.length] = 0; // end packet with 0
   ++ecPacket->header.length;
   if (ecPacket->header.useMaxLength)
      ecPacket->header.length = ecPacket->header.maxLength;

   // optional crc calc below
}

bool ecPacket_CheckReceivedPacket(ecPacket_t *ecPacket, unsigned short receivedLength, unsigned int expectedSeqNo, unsigned char key)
{
   if (receivedLength < sizeof(ecPacket->header))  // check if we have received a complete header before reading it
   {
      printf("size < header\n");
      return false;
   }
   if (ecPacket->header.key != COMMON_PACKET_KEY)
   {
      if ((key == 0) || (ecPacket->header.key != key))
      {
         printf("Wrong packet key\n");
         return false;
      }
   }
   if (ecPacket->header.version != PACKET_VERSION)  // not right version
   {
      printf("Wrong packet version\n");
      return false;
   }
   if (ecPacket->header.length != receivedLength)  // not complete packet
   {
      printf("received size < packet %d %d\n",receivedLength, ecPacket->header.length);
      return false;
   }
   if (ecPacket->header.seqNo != expectedSeqNo)  // not right version
   {
      printf("Wrong seqno, received:%d, expected:%d\n",ecPacket->header.seqNo,expectedSeqNo);
      return false;
   }
   // optional crc check here
   return true;
}

bool ecPacket_GetNextHandleData(ecPacket_t *ecPacket, ecBlock_t **block, unsigned short **handle, char **data, unsigned short **dataSize)
{
   if (*block == NULL)				// first time
      *block = (ecBlock_t *)ecPacket->data;

   if ( **((char **)block) == 0)		// end of packet?
      return false;
   *dataSize = &((*block)->dataSize);

   if (*handle == NULL)				// first time
   {
      *handle = (unsigned short *)&((*block)->data);
      if (**handle == 0)
         return false;	// no data in block
      *data = sizeof(unsigned short) + (char *)(*handle);
      return true;
   }

   *data = *data + (**dataSize);
   if (*data >= (char *)(*block) + (*block)->length)	// end of block?
   {
      *block = (ecBlock_t *)((char *)(*block) + (*block)->length);	// goto next block
      if ( **((char **)block) == 0)		// end of packet?
         return false;
      *dataSize = &((*block)->dataSize);
      *handle = (unsigned short *)&((*block)->data);
      if (**handle == 0)
         return false;	// no data in block
      *data = sizeof(unsigned short) + (char *)(*handle);
      return true;
   }
   if ((*block)->serialData)
      return true;

   *handle = (unsigned short *)(*data);
   *data = sizeof(unsigned short) + (char *)(*handle);
   return true;
}

char *ecPacket_GotoEnd(ecPacket_t *ecPacket)
{
   char *p = (char *)ecPacket->data;

   while (*p != 0)
      p = p + ((ecBlock_t *)p)->length;
   return p;
}

unsigned short ecPacket_GetBytesAvailable(ecPacket_t *ecPacket)
{
   int bytesAvailable;
   if ((bytesAvailable = ecPacket->header.maxLength - ecPacket->header.length - sizeof(ecBlock_t) - sizeof(ebmMemHandle_t) - 1) < 0)		// -1 last char must be unused
      bytesAvailable = 0;
   return (unsigned short)bytesAvailable;
}

char *ecPacket_GetBlock(ecPacket_t *ecPacket,int blockNo)  // first block is 1
{
   int i = 1;
   char *p = (char *)ecPacket->data;
	
   //	p = p + sizeof(ecPacket->header);
   while ((*p != 0) && (i < blockNo))
   {
      p = p + ((ecBlock_t *)p)->length;
      i++;
   }
   if (*p == 0)
      return NULL;
   return p;	
}

char *ecPacket_GetLastBlock(ecPacket_t *ecPacket)
{
   char *p = (char *)ecPacket->data;
   char *p2;

   if (*p == 0)
      return NULL;
   while (*p != 0)
   {
      p2 = p;
      p = p + ((ecBlock_t *)p)->length;
   }
   return p2;
}
///////////////////------------------------------/////////////////////////


UINT sendParameterU32 (UINT param, UDINT data, UINT func)
{
   if (INITIALIZE == func)
   {
      localCopyU32[param] = data-1;
   }
   else if (FORCE_SEND == func)
   {
      localCopyU32[param] = data;
      ecPacket_AddHandleData(&ecPacket,param | MEM_TYPE_UINT32 << 10,(char *)(&data),sizeof(data));
      localCopyU32CheckEnable[param] = true;
      return 1;
   }
   else if (SEND_ON_MODIFIED == func)
   {
      if (data != localCopyU32[param]) {
         ecPacket_AddHandleData(&ecPacket,param | MEM_TYPE_UINT32 << 10,(char *)(&data),sizeof(data));
         localCopyU32[param] = data;
         return 1;
      }
   }
   return 0;
}

UINT sendParameterBit (UINT param, BOOL  data, UINT func)
{
   USINT tByte = 0x00;
   if (INITIALIZE == func)
   {
      localCopyBit[param] = false;
   }
   else if (FORCE_SEND == func)
   {
      localCopyBit[param] = data;
      if (data) tByte = 0x01;
      ecPacket_AddHandleData(&ecPacket,param | MEM_TYPE_BIT << 10,(char *)(&tByte),sizeof(tByte));
      return 1;
   }
   else if (SEND_ON_MODIFIED == func)
   {
      if (data != localCopyBit[param]) {
         if (data) tByte = 0x01;
         ecPacket_AddHandleData(&ecPacket,param | MEM_TYPE_BIT << 10,(char *)(&tByte),sizeof(tByte));
         localCopyBit[param] = data;
         return 1;
      }
   }
   return 0;
}
void sendParameterU8  (UINT param, USINT data, UINT func)
{
}
void sendParameterU16 (UINT param, UINT  data, UINT func)
{
}
void sendParameterI16 (UINT param, INT   data, UINT func)
{
}
void sendParameterI32 (UINT param, DINT  data, UINT func)
{
}

void UDP (int logAction, char *txt)
{
   static unsigned long currentIdent = 0;
   static UdpSend_typ udpS;
   static UdpOpen_typ udpO;
   static UdpClose_typ udpC;
   static UdpRecv_typ udpR;
   static UdpConnect_typ udpCnct;
   static LREAL timeAtSend;
   static BOOL startTimer;
   static BOOL reCheckIncomming;
   
   switch(logAction) //CYCLIC_SEND
   {
      case 0: 
         if(fState == 0) // Initialize
         {
            memset((void *)&udpS, 0, sizeof(UdpSend_typ));
            memset((void *)&udpO, 0, sizeof(UdpOpen_typ));
            memset((void *)&udpC, 0, sizeof(UdpClose_typ));
            memset((void *)&udpR, 0, sizeof(UdpRecv_typ));
            memset((void *)&udpCnct, 0, sizeof(UdpConnect_typ));
            fState = 1;
         }
         if(fState == 1) // Open comm
         {
            udpO.pIfAddr = (UDINT)0;
            udpO.port = UDPLogPort;
            udpO.options = 0; //udpOPT_REUSEPORT | udpOPT_REUSEADDR;//0;
            udpO.enable = 1;
            UdpOpen(&udpO);
            stateTimer = 0;
            fState = 2;
            
         }
         if(fState == 2) // Wait for UDP open to complete
         {
            if(udpO.status == ERR_FUB_BUSY)
            {
               if(stateTimer > 100)
                  fState = 100;
               else
                  UdpOpen(&udpO);   
            }
            else if(udpO.status != 0)
            {
               fState = 101;
            }
            else
            {
               currentIdent = udpO.ident;
               stateTimer = 0;
               fState = 3;
               //sprintf(UDPDebug, "Connected to ip %s" , UDPLogIP);
            }
         }
         if(fState == 3) // Transmit data
         {
            if (startTimer)
            {
               sendReciveCycleTime = globalTimeTick - timeAtSend;
               if (sendReciveCycleTime > sendReciveCycleMax) sendReciveCycleMax = sendReciveCycleTime;
               if (sendReciveCycleTime < sendReciveCycleMin) sendReciveCycleMin = sendReciveCycleTime;
               startTimer = false;
            }
            
            if(EBMSend.ix != logOut)
            {
               udpS.pData = (DINT)EBMSend.text[logOut];
               udpS.datalen = ((ecPacket_t *)EBMSend.text[logOut])->header.length;
               udpS.ident = currentIdent;
               udpS.pHost = (DINT)UDPLogIP;
               udpS.port = UDPLogPort;
               udpS.flags = 0;
               udpS.enable = 1;
               UdpSend(&udpS);
               stateTimer = 0;
               fState = 4;
               sentSeq = ((ecPacket_t *)EBMSend.text[logOut])->header.seqNo;
               sentSize = udpS.datalen;
               sentMax = (USINT)(EBMSend.text[logOut][5]) % 256;
               logOut = (logOut + 1) % LOG_BUF_SIZE;
               ebmGlue.reading.sMsg++;
               timeAtSend = globalTimeTick;
               startTimer = true;
               reCheckIncomming = false;
            }
         }
         if(fState == 4) // Wait for UDP send to complete
         {
            if(udpS.status == ERR_FUB_BUSY)
            {
               if(stateTimer > 100)
                  fState = 102;
               else
                  UdpSend(&udpS);   
            }
            else if(udpS.status != 0)
            {
               fState = 103;
            }
            else
            {
               udpR.datamax = MSG_BUF_SIZE;
               udpR.pData = (DINT)msg;
               udpR.ident = currentIdent;
               udpR.pIpAddr = (DINT)UDPLogIP;
               udpR.port = UDPLogPort;
               udpR.flags = 0;
               udpR.enable = 1;
               UdpRecv(&udpR);
               stateTimer = 0;
               fState = 6;
            }
         }
         else if(fState == 6) 
         {
            if(udpR.status == ERR_OK)
            {
               msg[udpR.recvlen] = 0;
               
               if (!ebmConnection_Receive(msg,udpR.recvlen, sentSeq))
               {
                  UdpRecv(&udpR);
                  fState = 4;
               }
               else
                  fState = 3;
               ebmGlue.reading.connected = true;   
               ebmGlue.reading.rMsg++;
               
            }
            else if(stateTimer > 15)
            {
               fState = 3;
               ebmGlue.reading.connected = false;
               stateTimer = 0;
               
            }
            else if(udpR.status == ERR_FUB_BUSY)
            {
               fubBusyCnt++;
               UdpRecv(&udpR);
            }
            else if(udpR.status == udpERR_NO_DATA)
            {
               noDataCnt++;
               UdpRecv(&udpR);
            }
            else
               fState = 104;
         }
         if(fState == 100) // error: 
         {      
            strcpy(UDPDebug, "UDP open: timeout");
         }
         if(fState == 101) // error: 
         {      
            sprintf(UDPDebug, "%s %d", "UDP open", udpO.status);
         }
         if(fState == 102) // error: 
         {      
            strcpy(UDPDebug, "UDP send: timeout");
         }
         if(fState == 103) // error: 
         {      
            sprintf(UDPDebug, "%s %d", "UDP send", udpS.status);
         }
         stateTimer++;
         break;
      
      case 1: //TERMINATE
         if(currentIdent)
         {
            udpC.enable = 1;
            udpC.ident = currentIdent;
            UdpClose(&udpC);
         }
         break;
      case ADD_MSG:
         memcpy((char *)EBMSend.text[EBMSend.ix], txt,((ecPacket_t *)txt)->header.length);
         EBMSend.ix = (logIn + 1) % LOG_BUF_SIZE;
         logIn = EBMSend.ix;
         break;
      default:
         break;
   }   
}

UDINT scaleIt (REAL in)
{
   return (UDINT)(in * ebmGlue.setting.scale);
}

BOOL sendParam (USINT func)
{
   send = 0;
   if (INITIALIZE != func)
   {
      ecPacket_Init(&ecPacket,0,200,0,'A');
      ecPacket.header.type = PACKET_TYPE_DATA;
      ecPacket.header.seqNo = 0;
   }
   /* 
   if (streamer.reading.northStartCnt > 30 && streamer.reading.southStartCnt > 30 && streamer.reading.southStartCnt % 25 == 0 )
   {
      ebmGlue.setting.offset.nozzleDelay = (UDINT)((streamer.reading.northRightAvg + streamer.reading.northLeftAvg + 23.5) / 2.5 )
   }
   else
   */
      ebmGlue.setting.offset.nozzleDelay = 47;
   
   send += sendParameterU32(EBMMEM_UINT32_GLUE1_PATTERN,ebmGlue.setting.glueGun.pattern,func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE1_OFFSET,scaleIt(ebmGlue.setting.glueGun.offset),func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE1_SEP,scaleIt(ebmGlue.setting.glueGun.separation),func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE1_SIZE,(UDINT)(ebmGlue.setting.glueGun.size),func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE1_NOZZLE_TIME,ebmGlue.setting.glueGun.nozzleDelay,func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE1_MIN_MARK_SIZE,scaleIt(ebmGlue.setting.glueGun.minMarkSize),func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE1_MAX_MARK_SIZE,scaleIt(ebmGlue.setting.glueGun.maxMarkSize),func);

   send += sendParameterU32(EBMMEM_UINT32_GLUE2_PATTERN,ebmGlue.setting.offset.pattern,func);
   //send += sendParameterU32(EBMMEM_UINT32_GLUE2_OFFSET,scaleIt(cutter.reading.sheetSize + ebmGlue.setting.offset.offset - offsetComp),func);
   if ((stacker.reading.input.streamerStopPlatePos + 1.0) < cutter.reading.sheetSize) //small adjust for badly placed stop plate
      send += sendParameterU32(EBMMEM_UINT32_GLUE2_OFFSET,scaleIt(cutter.reading.sheetSize + ebmGlue.setting.offset.offset - offsetComp - 0.5),func);
   else if ((stacker.reading.input.streamerStopPlatePos - 1.0) > cutter.reading.sheetSize) //small adjust for badly placed stop plate
      send += sendParameterU32(EBMMEM_UINT32_GLUE2_OFFSET,scaleIt(cutter.reading.sheetSize + ebmGlue.setting.offset.offset - offsetComp + 0.5),func);
   else
      send += sendParameterU32(EBMMEM_UINT32_GLUE2_OFFSET,scaleIt(cutter.reading.sheetSize + ebmGlue.setting.offset.offset - offsetComp),func);
   //tmp = (cutter.reading.sheetSize*0.5 + stacker.reading.input.streamerStopPlatePos*0.5 - 41.5 + ebmGlue.setting.offset.offset) * ebmGlue.setting.scale;
   send += sendParameterU32(EBMMEM_UINT32_GLUE2_SEP,scaleIt(ebmGlue.setting.offset.separation),func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE2_SIZE,(UDINT)(ebmGlue.setting.offset.size),func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE2_NOZZLE_TIME,ebmGlue.setting.offset.nozzleDelay,func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE2_MIN_MARK_SIZE,scaleIt(ebmGlue.setting.offset.minMarkSize),func);
   send += sendParameterU32(EBMMEM_UINT32_GLUE2_MAX_MARK_SIZE,scaleIt(ebmGlue.setting.offset.maxMarkSize),func);   
   
   
   send += sendParameterBit(EBMMEM_BIT_SUBSCRIBE_ALL,subAll,func); 
   
   send += sendParameterBit(EBMMEM_BIT_GLUE1_ENABLE,ebmGlue.setting.glueGun.enable,func);   
   send += sendParameterBit(EBMMEM_BIT_GLUE1_CLEAR_QUEUE,ebmGlue.command.glueGun.clearQueue,func);
   ebmGlue.command.glueGun.clearQueue = false;
  
   send += sendParameterBit(EBMMEM_BIT_OUTPUT_START+5,ebmGlue.setting.aimLight1,func);   
   send += sendParameterBit(EBMMEM_BIT_OUTPUT_START+6,ebmGlue.setting.aimLight2,func);
   send += sendParameterBit(EBMMEM_BIT_OUTPUT_START+7,ebmGlue.setting.aimLight3,func);   
   send += sendParameterBit(EBMMEM_BIT_OUTPUT_START+8,ebmGlue.setting.aimLight4,func);
   
   
   send += sendParameterBit(EBMMEM_BIT_GLUE2_ENABLE,ebmGlue.setting.offset.enable,func);   
   send += sendParameterBit(EBMMEM_BIT_GLUE2_CLEAR_QUEUE,ebmGlue.command.offset.clearQueue,func);
   ebmGlue.command.offset.clearQueue = false;
   
   send += sendParameterU32(EBMMEM_UINT32_GLUE_TEST_PARAM,ebmGlue.setting.testMode,func);
   
   if (send == 0) return false;
   return true;
}

void glueHandler (void)
{
   static USINT idleMsg;
   if (ghState == 20 && ebmGlue.reading.connected == false) ghState = 10;
   if (ebmGlue.command.sendAll && ghState != 0) {
      ghState = 10;
      ebmGlue.reading.sendAllCnt++;
   }
   
   
   
   if (ebmGlue.setting.offset.enable) ebmGlue.setting.offset.pattern = 1;
   
   if (ebmGlue.setting.glueGun.pattern == 0) ebmGlue.setting.glueGun.enable = 0;
   else ebmGlue.setting.glueGun.enable = 1;
   
   if (ebmGlue.command.glueGun.stop) {
      if (ebmGlue.command.glueGun.start) ebmGlue.command.glueGun.clearQueue = true;
      ebmGlue.setting.glueGun.enable = 0;
      ebmGlue.command.glueGun.start = false;
   }
   
   if (ebmGlue.command.glueGun.start) {
      ebmGlue.command.glueGun.stop = false;
   }
   
   if (ebmGlue.command.offset.start) {
      ebmGlue.command.offset.stop = false;
   }
   
   if (ebmGlue.command.offset.stop) {
      ebmGlue.command.offset.start = false;
   }
   
   if (ebmGlue.setting.glueGun.enable && globalPower) {
      if (stacker.reading.input.topCoverClosed) aimLightTimer += SECONDS_UNIT;
      else aimLightTimer = 0.0;
      
      if (stacker.reading.status.state == Status_Loading) aimLightTimer = 0.0;
      
      if (aimLightTimer < 300.0) ebmGlue.setting.aimLight1 = ebmGlue.setting.aimLight2 = ebmGlue.setting.aimLight3 = ebmGlue.setting.aimLight4 = true;
      else ebmGlue.setting.aimLight1 = ebmGlue.setting.aimLight2 = ebmGlue.setting.aimLight3 = ebmGlue.setting.aimLight4 = false;
   }
   else {
      ebmGlue.setting.aimLight1 = ebmGlue.setting.aimLight2 = ebmGlue.setting.aimLight3 = ebmGlue.setting.aimLight4 = false;
      aimLightTimer = 0.0;
   }

   switch (ghState)
   {
      case 0:
         sendParam(INITIALIZE);
         modified = 0;
         ghState = 10;
         subAll = 1;
         break;
      case 10:
         sendParam(FORCE_SEND);
         ecPacket.header.seqNo = seq = 0;
         ecPacket_Finalize(&ecPacket);
         sequenceFault = 0;
         UDP(ADD_MSG, (char *)(&ecPacket));
         ghState = 20;
         idleMsg = 0;
         break;
      case 20:
         if (sendParam(SEND_ON_MODIFIED))
         {
            ecPacket.header.seqNo = seq++;
            ecPacket_Finalize(&ecPacket);
            UDP(ADD_MSG, (char *)(&ecPacket));
            idleMsg = 0;
            modified++;
         }
         else if ((idleMsg++) % 15 == 0)
         {
            ecPacket_Init(&ecPacket,0,200,0,'A');
            ecPacket.header.type = PACKET_TYPE_DATA;
            ecPacket.header.seqNo = seq++;
            ecPacket_Finalize(&ecPacket);
            UDP(ADD_MSG, (char *)(&ecPacket));
            totalSize = ecPacket_GetBytesAvailable(&ecPacket);
         }
         else if ((idleMsg++) > 250)
         {
            sendParameterBit(EBMMEM_BIT_SUBSCRIBE_ALL,1,FORCE_SEND);
            ecPacket.header.seqNo = seq++;
            ecPacket_Finalize(&ecPacket);
            UDP(ADD_MSG, (char *)(&ecPacket));
            totalSize = ecPacket_GetBytesAvailable(&ecPacket);
            idleMsg = 0;
         }
         break;
   } 
   ebmGlue.command.readAll = false; 
   ebmGlue.command.sendAll = false;
}

void setBit (UDINT x, UDINT * var) 
{
   *var |= 1 << x;
}
void clearBit (UDINT x, UDINT * var) 
{
   *var &= ~(1 << x);
}

BOOL readBit (UDINT x, UDINT var)
{
   return ((var >> x) & 1);
}

void handleSeq(void)
{
   if (ebmGlue.reading.connected)
      ecPacket.header.seqNo = seq++;
   else 
      ecPacket.header.seqNo = seq = 0;
   ecPacket_Finalize(&ecPacket);
}

bool ebmConnection_Receive(char * buf,UINT len, UINT eSeq)
{
   
   ecPacket_t *p_ecPacket;
	
   p_ecPacket = (ecPacket_t *)(buf);
   if (!ecPacket_CheckReceivedPacket(p_ecPacket, len, eSeq, 'A'))
   {
      sprintf(UDPDebug, "Receive check fail");
      sequenceFault++;
      return false;
   }
   
   
   //ebmConnection->receivedSeqNo = p_ecPacket->header.seqNo;

   // -- The packet is valid
	
   if (p_ecPacket->header.type == PACKET_TYPE_DATA)
   {
      
      ecBlock_t *p_ecBlock = NULL;
      unsigned short *p_handle = NULL;
      char *p_data = NULL;
      unsigned short *p_dataSize = NULL;
      //char group;
      //int errCode;
      //bit32ValueMask_t *bit32ValueMask;
      float f1;
		
      while (ecPacket_GetNextHandleData(p_ecPacket, &p_ecBlock, &p_handle, &p_data, &p_dataSize))
      {
         ebmMemHandle_t *p_ebmMemHandle = (ebmMemHandle_t *)p_handle;
         //printf("new packet memtype:%d, adress:%d\n",p_ebmMemHandle->type, p_ebmMemHandle->address);

         switch (p_ebmMemHandle->type)
         {
            case MEM_TYPE_STRING:
               //	printf("group:%d, adress:%d, data:%s\n",group, p_ebmMemHandle->address,p_data);
               // shmSetTextByHandle(shmHandle, p_data, &errCode);
               break;
            case MEM_TYPE_BIT:
               ebmConReciveCall++;
               //bit32ValueMask = (bit32ValueMask_t *)p_data;
               //bit32ValueMask = ( *)p_data;
               
               //               if (p_ebmMemHandle->address == 0)
               //if (p_ebmMemHandle->address == EBMMEM_BIT_GLUE1_ENABLE)        ebmGlue.reading.glueGun.active = (BOOL)*p_data;//readBit(EBMMEM_BIT_GLUE1_ENABLE,bit32ValueMask->value);
               //else if (p_ebmMemHandle->address == EBMMEM_BIT_GLUE2_ENABLE)   ebmGlue.reading.offset.active = (BOOL)*p_data;//readBit(EBMMEM_BIT_GLUE2_ENABLE,bit32ValueMask->value);
               if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START)         ebmGlue.reading.input.in1 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+1)  ebmGlue.reading.input.in2 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+2)  ebmGlue.reading.input.in3 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+3)  ebmGlue.reading.input.in4 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+4)  ebmGlue.reading.input.in5 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+5)  ebmGlue.reading.input.in6 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+6)  ebmGlue.reading.input.in7 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+7)  ebmGlue.reading.input.in8 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+8)  ebmGlue.reading.input.in9 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+9)  ebmGlue.reading.input.in10 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+10)  ebmGlue.reading.input.in11 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+11)  ebmGlue.reading.input.in12 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+12)  ebmGlue.reading.input.in13 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+13)  ebmGlue.reading.input.in14 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+14)  ebmGlue.reading.input.in15 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+15)  ebmGlue.reading.input.in16 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+16)  ebmGlue.reading.input.in17 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+17)  ebmGlue.reading.input.in18 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+18)  ebmGlue.reading.input.in19 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+19)  ebmGlue.reading.input.in20 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+20)  ebmGlue.reading.input.in21 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+21)  ebmGlue.reading.input.in22 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+22)  ebmGlue.reading.input.in23 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+23)  ebmGlue.reading.input.in24 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+24)  ebmGlue.reading.input.in25 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+25)  ebmGlue.reading.input.in26 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+26)  ebmGlue.reading.input.in27 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_INPUT_START+27)  ebmGlue.reading.input.in28 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START)   ebmGlue.reading.output.out1 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+1) ebmGlue.reading.output.out2 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+2) ebmGlue.reading.output.out3 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+3) ebmGlue.reading.output.out4 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+4) ebmGlue.reading.output.out5 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+5) ebmGlue.reading.output.out6 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+6) ebmGlue.reading.output.out7 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+7) ebmGlue.reading.output.out8 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+8) ebmGlue.reading.output.out9 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+9) ebmGlue.reading.output.out10 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+10) ebmGlue.reading.output.out11 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+11) ebmGlue.reading.output.out12 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+12) ebmGlue.reading.output.out13 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+13) ebmGlue.reading.output.out14 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+14) ebmGlue.reading.output.out15 = (BOOL)*p_data;
               else if (p_ebmMemHandle->address == EBMMEM_BIT_OUTPUT_START+15) ebmGlue.reading.output.out16 = (BOOL)*p_data;
               
       
               break;
            case MEM_TYPE_UINT8: break;
            case MEM_TYPE_INT8: break;
            case MEM_TYPE_UINT16: break;
            case MEM_TYPE_INT16: break;
            case MEM_TYPE_UINT32:
               memType32cnt++;
               //memType32cnt++;
               //*((unsigned int *)p_data)
               //rDataUDINT[p_ebmMemHandle->address-5] = *((unsigned int *)p_data);
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE1_MARK_SIZE) {
               //ebmGlue.reading.markSize = (REAL)(*((unsigned int *)p_data)) / ebmGlue.setting.scale;
               //ms = *((unsigned int *)p_data);
               //if (ebmGlue.reading.pulseSpeed > 5) histHandler(ebmGlue.reading.markSize,ebmGlue.setting.glueGun.minMarkSize,ebmGlue.setting.glueGun.minMarkSize,false); 
               }
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE_PULSE_SPEED) {
                  
                  ebmGlue.reading.pulseSpeed = (REAL)(*((unsigned int *)p_data)) / ebmGlue.setting.scale;
                  ps = *((unsigned int *)p_data);
               }
               
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE1_DOT_COUNT) ebmGlue.reading.glueGun.dotCount = *((unsigned int *)p_data);
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE2_DOT_COUNT) ebmGlue.reading.offset.dotCount = *((unsigned int *)p_data);
               
               tmpGlueDots = ebmGlue.reading.glueGun.dotCount - atResetGlueDots;
               tmpOffsets = ebmGlue.reading.offset.dotCount - atResetsOffsets;
               
               checkU32.parameter++;
               if (checkU32.parameter >= (EBMMEM_UINT32_SIZE - 1)) checkU32.parameter = 0;
               //if (localCopyU32CheckEnable[checkU32.parameter])
               if (p_ebmMemHandle->address == checkU32.parameter && localCopyU32CheckEnable[checkU32.parameter])
               {
                  checkU32.data = *((unsigned int *)p_data);
                  if (localCopyU32[checkU32.parameter] != checkU32.data) {
                     checkU32.ok = false;
                     udDeb1 = checkU32.parameter;
                     udDeb2 = checkU32.data;
                  }
                  else checkU32.ok = true;
               }
               else checkU32.ok = true;
               
               
               if (ebmGlue.command.resetCnt)
               {
                  atResetGlueDots = ebmGlue.reading.glueGun.dotCount;
                  atResetsOffsets = ebmGlue.reading.offset.dotCount;
                  ebmGlue.command.resetCnt = false;
                  histHandler(0.0,ebmGlue.setting.glueGun.minMarkSize,ebmGlue.setting.glueGun.minMarkSize,true); 
               }   
               
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE1_IN_QUEUE) {
                  ebmGlue.reading.glueGun.inQueue = *((unsigned int *)p_data);
               }
               
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE2_IN_QUEUE) {
                  ebmGlue.reading.offset.inQueue = *((unsigned int *)p_data);
               }
               
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE2_OFFSET) {
                  readbackOffsetOffset = *((unsigned int *)p_data);
               }
               
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE2_MIN_MARK_SIZE) {
                  readbackMinMarkSize = *((unsigned int *)p_data);
                  if (readbackMinMarkSize != 112) readbackMinTrap = readbackMinMarkSize; 
               }
               
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE2_MAX_MARK_SIZE) {
                  readbackMaxMarkSize = *((unsigned int *)p_data);
                  if (readbackMaxMarkSize != 245) readbackMaxTrap = readbackMaxMarkSize; 
               }
               
               if (p_ebmMemHandle->address == EBMMEM_UINT32_GLUE2_PATTERN) {
                  readbackPattern = *((unsigned int *)p_data);
                  if (readbackPattern != 1) readbackPattern = readbackPattern; 
               }
               

               //if (p_ebmMemHandle->address == EBMMEM_UINT32_ENCODER_1)  ebmGlue.reading.input.encoder = *((unsigned int *)p_data);
               break;
            case MEM_TYPE_INT32:
               //shmWriteIntByHandle(shmHandle, *((int *)p_data)); 
               break;
            case MEM_TYPE_FLOAT:
               f1 = *((float *)p_data);
               //					printf("group:%d, adress:%d, float:%f\n",group, p_ebmMemHandle->address,f1);
               //shmWriteRealByHandle(shmHandle, f1); 
               break;
            case MEM_TYPE_UINTx5:
               break;
            case MEM_TYPE_FIFO_STRING: break;
            case MEM_TYPE_FIFO_BIT: break;
               break;
            case MEM_TYPE_FIFO_UINT8:
               //shmCpynByteToFifoByHandle(p_data, shmHandle, *p_dataSize);
               break;
            case MEM_TYPE_FIFO_INT8: break;
            case MEM_TYPE_FIFO_UINT16: 
               if (p_ebmMemHandle->address == EBMMEM_FIFO_UINT16_GLUE1_MARK_SIZE) {
                  
                  markReadRaw = *((unsigned short *)p_data);
                  /*rawValueQueue[rawValueQueueP] = markReadRaw;
                  rawValueQueueP++;
                  if (rawValueQueueP >= 40) rawValueQueueP = 0;
                  markReadCnt++;*/
                  ebmGlue.reading.markSize = (REAL)(markReadRaw) / ebmGlue.setting.scale;
                  histHandler(ebmGlue.reading.markSize,ebmGlue.setting.glueGun.minMarkSize,ebmGlue.setting.glueGun.minMarkSize,false);   
                  sprintf(ULog.text[ULog.ix++], "egms: %f;%f", ebmGlue.reading.markSize, ebmGlue.reading.pulseSpeed);
               }
               /*if (p_ebmMemHandle->address == EBMMEM_FIFO_UINT16_GLUE2_MARK_SIZE) {
                  
                  markReadRaw = *((unsigned int *)p_data);
                  rawValueQueue2[rawValueQueueP2] = markReadRaw;
                  rawValueQueueP2++;
                  if (rawValueQueueP2 >= 40) rawValueQueueP2 = 0;
               }*/
               break;
            case MEM_TYPE_FIFO_INT16: break;
            case MEM_TYPE_FIFO_UINT32: break;
            case MEM_TYPE_FIFO_INT32: break;
            case MEM_TYPE_FIFO_FLOAT: break;
            case MEM_TYPE_FIFO_UINTx5:
               //shmAddDataFifoByHandle(shmHandle, (int *)p_data, &errCode);				
               break;
         }
         //shmResetChangeBit(shmHandle);		
      }
   }
   //shmWriteBoolByHandle(ebmShmHandles[EBM_SHM(ebmConnection->id,CONNECTED)],true);

   //ebmConnection->ebmCommUdp.charReceived = 0;
   return true;
}

REAL rabs(REAL a)
{
   if (a<0) return a*-1.0;
   else return a;
}


void histHandler (REAL value, REAL min, REAL max, BOOL res)
{
   //uses histVal1 
   static USINT lastQVal[3];
   if (res) //reset
   {
      for (int i = 0; i < sizeof(histVal1) ; i += 4)
      {
         histVal1[i/4] = 0;
      }
      for (int z = 0; z < 100 ; z++)
      {
         markSizes[z] = 0;
      }
      msp = 0;
      marksRead = 0;
      validMarksRead = 0;
      invalidValidRatio = 100.0;
      return;
   }
   
   marksRead++;
   //if (value < 25 && value > 0.35) markSizes[msp] = ebmGlue.reading.markSize;
   if (value < 25 && value > ((ebmGlue.setting.offset.maxMarkSize - ebmGlue.setting.offset.minMarkSize)/5)) {
      markSizes[msp] = ebmGlue.reading.markSize;
      msp++;
      if (msp==5)
      {
         lastQVal[0] = lastQVal[1]; 
         lastQVal[1] = lastQVal[2];
         lastQVal[2] = (markQuality > 80 ? 2 : 1);
         if (lastQVal[0] == 0) mq = 0;
         else if ((lastQVal[0]+lastQVal[1]+lastQVal[2]) == 3) mq = 1; //BAD
         else if ((lastQVal[0]+lastQVal[1]+lastQVal[2]) == 4 && markQuality < 70) mq = 1; //BAD
         else mq = 2; //OK
      }
   }
   
   
   calc_Avg_Var_stdDev(markSizes);
   if (msp >= 100) msp = 0;
   if (value >= max) return;
   if (value <= min) return;
   
   validMarksRead++;

   invalidValidRatio = (REAL)(validMarksRead*100) / (REAL)(marksRead);
   
   float resStp;
   int seg;
   resStp = (max - min)/(REAL)(sizeof(histVal1)/4);
    
   seg = (int)(value/resStp) - 1;
   histVal1[seg]++;
   return;
}

void calc_Avg_Var_stdDev (REAL x[100])
{
   INT i,n;
   REAL sum = 0, sum1 = 0;  
   /*  Compute the sum of all elements */
   maxMarkRead = 0;
   minMarkRead = 100.0;
   for (i = 0; i < 100; i++)
   {
      sum = sum + x[i];
      if (x[i] > maxMarkRead) maxMarkRead = x[i];
      if (x[i] < minMarkRead) minMarkRead = x[i];
   }
   average = sum / 100.0;
   for (i = 0; i < 100; i++)
   {
      sum1 = sum1 + pow((x[i] - average), 2);
   }
   variance = sum1 / 100.0;
   std_deviation = pow(variance,0.5);
   if (minMarkRead>0.0) markQuality = compenstationCurve(maxMarkRead + std_deviation - minMarkRead);
   if (markQuality<0) markQuality = 0;
}

REAL compenstationCurve (REAL x)
{
   REAL xPoints[6] = {0.0 , 0.88, 1.0, 1.5, 2.0, 7}; //infeedTransport.actualSpeed speed m/s
   REAL yPoints[6] = {100 , 96.5 , 93 ,  86,  50 , 0 };  //return value,, ms
   
   int  i;
    
   for (i=0;i<6;i++)
   {
      if (x<xPoints[i]) break;
   }
   if (i == 0) return yPoints[0];
   
   return rmap(x,xPoints[i-1],xPoints[i],yPoints[i-1],yPoints[i]);
}

REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max)
{
   return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

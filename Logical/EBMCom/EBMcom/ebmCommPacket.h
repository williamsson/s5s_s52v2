#ifndef _EBM_PACKET_H_
#define _EBM_PACKET_H_

#include <stdbool.h>

#define MAX_PACKETDATA_SIZE 1200

#define COMMON_PACKET_KEY	'C'
#define PACKET_VERSION 1
//Packet types
#define PACKET_TYPE_RESET 		0 //Start counting from zero
#define PACKET_TYPE_DATA		1 //Data packet

#pragma pack(1)
typedef struct
{
   struct
   {
      unsigned char key;			// Must be a known value;
      unsigned char version;		// Version of header structure
      unsigned char type;			// Type of packet
      unsigned short length;		// Total length of packet (including header)
      unsigned short maxLength   : 15;	// Max allowed length of packet
      unsigned short useMaxLength: 1;	// Always send max length packets
      unsigned int seqNo;			// Incremented sequence number to detect missing packets
      unsigned char checksum;		// Checksum of data
   } header;
   char data[MAX_PACKETDATA_SIZE];	//Holds data
} ecPacket_t;

typedef struct 
{
   unsigned short length	  :15;	// length of block
   unsigned short serialData:1;	// 1 = only one handle in beginning of data, used for fifo
   unsigned short dataSize;		// size of each data element
   unsigned short count;			// number of elements
   char data;						// entry point to following data
} ecBlock_t;


typedef union
{
   unsigned short all;
   struct
   {
      unsigned short address:	10;
      unsigned short type:	6;
   };
} ebmMemHandle_t;
/*
typedef struct
{
	unsigned int value;
	unsigned int mask;
} bit32ValueMask_t;*/
#pragma pack()


// examples
/*
bool ecPacket_AddBits(ecPacket_t *ecPacket,unsigned short handle32,unsigned int value, unsigned int mask);
bool ecPacket_AddUInt(ecPacket_t *ecPacket,unsigned short handle,unsigned int value);
bool ecPacket_AddFloat(ecPacket_t *ecPacket,unsigned short handle,float value);
bool ecPacket_AddString(ecPacket_t *ecPacket,unsigned short handle, char *data);
bool ecPacket_AddBytes(ecPacket_t *ecPacket,unsigned short handle, char *data, unsigned short dataLength);
bool ecPacket_AddUIntx5(ecPacket_t *ecPacket,unsigned short handle, unsigned int uintx5[5]);
*/

#endif

/*#ifndef _EBM_PACKET_H_
#define _EBM_PACKET_H_

#include <stdbool.h>

#define MAX_PACKETDATA_SIZE 1200

#define PACKET_KEY	'E'
#define PACKET_VERSION 1
//Packet types
#define PACKET_TYPE_RESET 		0 //Start counting from zero
#define PACKET_TYPE_DATA		1 //Data packet

#pragma pack(1)
typedef struct
{
   struct
   {
      unsigned char key;			// Must be a known value;
      unsigned char version;		// Version of header structure
      unsigned char type;			// Type of packet
      unsigned short length;		// Total length of packet (including header)
      unsigned short maxLength   : 15;	// Max allowed length of packet
      unsigned short useMaxLength: 1;	// Always send max length packets
      unsigned int seqNo;			// Incremented sequence number to detect missing packets
      unsigned char checksum;		// Checksum of data
   } header;
   char data[MAX_PACKETDATA_SIZE];	//Holds data
} ecPacket_t;

typedef struct 
{
   unsigned short length;	// length of block
   unsigned short dataSize;// size of each data element
   unsigned short count;	// number of elements
   char data;				// entry point to following data
} ecBlock_t;


typedef union
{
   unsigned short all;
   struct
   {
      unsigned short address:	10;
      unsigned short type:	6;
   };
} ebmMemHandle_t;

typedef struct
{
   unsigned int value;
   unsigned int mask;
} bit32ValueMask_t;
#pragma pack()


#endif

*/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "Library.h"
#ifdef __cplusplus
	};
#endif

unsigned long oldPos = 0;
unsigned long wrapAround_i = 0;
unsigned long overFlow = 0;
UINT wrapped = 0;

float getFloatPos(DINT pos)
{
   unsigned long adjustPos = 0;
   unsigned uIntPos = 0;     

   if((unsigned long)pos < oldPos)
      wrapped++;
   
   adjustPos = (wrapped * overFlow) % wrapAround_i;
   uIntPos = (unsigned long)pos;   
   if(uIntPos < (wrapAround_i + adjustPos))
      uIntPos += adjustPos;
   else
      uIntPos -= (wrapAround_i - adjustPos);
   oldPos = (unsigned long)pos;
   return ((float)(uIntPos % wrapAround_i)) / 100.0;   
}

void DINT_TO_PERIODIC_REAL(struct DINT_TO_PERIODIC_REAL* inst)
{
   wrapAround_i  = (unsigned long)(inst->period * 100);
   overFlow = (1 + (0xFFFFFFFF % wrapAround_i)) % wrapAround_i;
   if(inst->reset)
   {
      wrapped = inst->wrapCount = 0;
      oldPos = 0;
      inst->reset = 0;
   }
   inst->posPeriodicREAL = getFloatPos(inst->posDINT);
   inst->wrapCount = wrapped;
}


#include <bur/plctypes.h>
#include <string.h>
#include <stdio.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF; 

#define SECONDS_UNIT  			              0.0096
#define COLOR_OFF                           0,0,0,0
#define COLOR_RED                           0,1,0,0
#define COLOR_YELLOW                        0,0,1,0
#define COLOR_GREEN                         1,0,0,0
#define COLOR_ORANGE                        0,1,1,0
//only button 1,2
#define COLOR_WHITE                         0,0,0,1
#define COLOR_PINK                          1,0,0,1
//only button 3
#define COLOR_BLUE                          0,0,0,1
#define COLOR_MAGENTA                       1,0,0,1
#define COLOR_CYAN                          0,0,1,1

#define TRUE                                1
#define FALSE                               0
#define BEEP_POSITION                       93.0

#define WEB_WATCH                           1
#define E_STOP                              2

INT buttonLedTest = 0;
void quarterSecondCycle (void);
void buttonCommand (void);
void setInfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
void setOutfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL isInfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL isOutfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL firstTimeInState(void); //returns true if we enter state from anoter state
//void setOutput (struct output_typ *x, BOOL val);
void checkEstop(void);
void setInfeedErrorSignal12(BOOL onOff);
void setInfeedErrorSignal34(BOOL onOff);
void setOutfeedErrorSignal12(BOOL onOff);
void setOutfeedErrorSignal34(BOOL onOff);
void startupLight(void);

void addError(USINT errorNo, UINT subType)
{
	machineError.active = TRUE;
	machineError.errType = (UINT)(errorNo) + 3000;
	machineError.errSubType = (UINT)(subType);	
}

void _INIT ProgramInit(void)
{
   lastState = errorState;
   state = initState;
   buttonLedTest = 0;
}

void setOutput(void)
{
	//physical.folder.analog.out1 = output.setValvePressure;
	if (folder.setting.forceOutputEnable.beeper)
		physical.folder.digital.out1 = folder.reading.output.beeper = folder.setting.forceOutputValue.beeper;
	else physical.folder.digital.out1 = folder.reading.output.beeper = output.beeper;
}

void getInputs(void)
{
	folder.reading.input.webTensionInside = physical.folder.analog.in1;
	folder.reading.input.webTensionInfeed = physical.folder.analog.in2;
	folder.reading.input.feedAllButton = physical.folder.digital.in2;
    folder.reading.input.feedAllButtonInfeed = physical.folder.digital.in7;
	folder.reading.input.webWatch1 = physical.folder.digital.in1;
	folder.reading.input.webWatch2 = physical.folder.digital.in3;
	folder.reading.input.webWatch3 = physical.folder.digital.in5;
    folder.reading.input.eStopInfeed = physical.folder.digital.in13;//in13;
    folder.reading.input.eStopOutfeed = physical.folder.digital.in15;//in14;
    folder.reading.input.rightFrontDoor = physical.folder.digital.in9;//in9;
    folder.reading.input.infeedSideDoor = physical.folder.digital.in10;//in10;
    folder.reading.input.infeedTopCover = physical.folder.digital.in14;//in15;
    folder.reading.input.outfeedTopCover = physical.folder.digital.in16;//in16;
   
}

void _CYCLIC ProgramCyclic(void)
{
	static LREAL lastSecondTick;
	static int jogInfStartStep = 0;
	static int jogInfStopStep = 0;
	static BOOL infeedWasConnected = false;
	static int jogOutfStartStep = 0;
	static int jogOutfStopStep = 0;
	static BOOL outfeedWasConnected = false;
	static REAL sourceB50usageLast;
	static REAL beepTime;
	static BOOL captureWebWatchSensorState = true;
	static BOOL ww1,ww2,ww3; //captured sensor state
	static USINT cwwssCnt = 200; //counter, 200-50 idle, 50-1 measure, 0 error detect
	static USINT ww1ON,ww1OFF,ww2ON,ww2OFF,ww3ON,ww3OFF; //measure sensor state
   
   if(!lineController.setup.folder.installed) return; // not installed, don't run this code
   
	getInputs();
	folderTick += SECONDS_UNIT;
	buttonCommand (); //check button commands
	if ((folderTick-lastSecondTick) > 0.25){
		lastSecondTick = folderTick;
		quarterSecondCycle(); //handle button lights
	} 
	if (folder.command.beep) beepTime = 0.6;
	if (folder.command.shortBeep) beepTime = 0.045;
   
	folder.command.beep = FALSE;
	folder.command.shortBeep = FALSE;
   
	if (FALSE == folder.reading.input.infeedTopCover || FALSE == folder.reading.input.outfeedTopCover) folder.reading.coversClosed = 0;  //readingsF50.coversClosed = 0;
	else folder.reading.coversClosed = 1;

	if (beepTime>0) 
	{
		//setOutput(&beeper,TRUE);// = TRUE;
		//setOutput(&folderBeeper,TRUE);
		//folder.command.beep = TRUE;
		output.beeper = TRUE;
		beepTime -= SECONDS_UNIT;
		if (beepTime<0) beepTime = 0.0;
	}
	else output.beeper = FALSE; //put(&folderBeeper,FALSE); //setOutput(&beeper,FALSE); //beeper = FALSE;
	checkEstop();
   
   folder.reading.status.stopped = FALSE;
   if      (initState == state)   folder.reading.status.state = Status_NotLoaded;
   else if (manualState == state) folder.reading.status.state = Status_ManualLoad;
   else if (runState == state)    {
      folder.reading.status.state = Status_Ready;
      if (buttonStopped) folder.reading.status.stopped = TRUE;
   }
   else if (errorState == state)  folder.reading.status.state = Status_Error;
   
	//Safety_Misc.folderInAuto = FALSE; //sets TRUE in runState
	switch (state)
	{
		case initState:
			if (firstTimeInState())
			{
				strcpy(ULog.text[ULog.ix++], "*F50 enter initState");
			}
			folder.reading.maximumSpeed = 0.0; //demand max speed to cutter
			folder.reading.ready = FALSE; //printer interface infromation
			folder.reading.error = FALSE; //printer interface infromation
			lastState = state;
			//beepTime = 0;
         
			break;
		case manualState:
			if (firstTimeInState())
			{
				strcpy(ULog.text[ULog.ix++], "*F50 enter manualState");
			}
			folder.reading.maximumSpeed = 0.0; //demand max speed to cutter
			folder.reading.ready = FALSE; //printer interface infromation
			folder.reading.error = FALSE; //printer interface infromation
			lastState = state;
		   
			if (buffer.reading.infeedSpeed>0.1)
			{
				if (buffer.reading.infeedSpeed<0.5)
				{
					if (buffer.reading.usage>BEEP_POSITION && sourceB50usageLast<BEEP_POSITION && beepTime == 0.0)  beepTime = 0.5;
				}
				else
				{ 	 
					if (buffer.reading.usage>(BEEP_POSITION-3) && sourceB50usageLast<(BEEP_POSITION-3) && beepTime == 0.0)  beepTime = 1;
				}
			}   
		 
			if (buffer.reading.usage>BEEP_POSITION && sourceB50usageLast<BEEP_POSITION && buffer.reading.infeedSpeed>0.1 && beepTime == 0.0)  beepTime = 0.5;
         
			if(buffer.reading.inDecurl && (folder.reading.feedInfeed || folder.reading.feedOutfeed))
				buffer.command.releaseDecurl = TRUE;
			if(buffer.reading.usage < 2.0)
				folder.reading.feedInfeed = folder.reading.feedOutfeed = false;
         
			/*********************   JOG INFEED   *********************/
			if(folder.reading.feedInfeed)
			{
				if(jogInfStartStep == 0)
				{
					strcpy(ULog.text[ULog.ix++], "*F50 Infeed start");
					infeedWasConnected = Slave[SLAVE_PLOW_IN_INDEX].AxisState.SynchronizedMotion;
					if(infeedWasConnected)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Halt slave (infeed)");
						Slave[SLAVE_PLOW_IN_INDEX].Command.Halt = true;
						//   Slave[SLAVE_BUF_OUT_INDEX].Command.Halt = true;
					}
					jogInfStartStep = 1;
				}
				if(jogInfStartStep == 1)
				{
					if(Slave[SLAVE_PLOW_IN_INDEX].AxisState.StandStill)
					{
						//bufMasterPF = true; // connect buffer master to plowfolder infeed 
						Slave[SLAVE_BUF_OUT_INDEX].Command.StartSlave = true;
						strcpy(ULog.text[ULog.ix++], "*F50 Connect buffer to PF in");
						jogInfStartStep = 2;
					}
				}
				if(jogInfStartStep == 2)
				{
					if(Slave[SLAVE_BUF_OUT_INDEX].AxisState.SynchronizedMotion)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Start moving (infeed)");
						Slave[SLAVE_PLOW_IN_INDEX].Parameter.Acceleration = 500;
						Slave[SLAVE_PLOW_IN_INDEX].Parameter.Deceleration = 500;
						//if (folder.reading.runLocal && readingsF50.coversClosed) Slave[SLAVE_PLOW_IN_INDEX].Parameter.Velocity = 24000;
						//else Slave[SLAVE_PLOW_IN_INDEX].Parameter.Velocity = 12000;
						Slave[SLAVE_PLOW_IN_INDEX].Command.MoveVelocity = true;
						jogInfStartStep = 3;
					}
				}
				if (folder.reading.runLocal && folder.reading.coversClosed) Slave[SLAVE_PLOW_IN_INDEX].Parameter.Velocity = 240;
				else Slave[SLAVE_PLOW_IN_INDEX].Parameter.Velocity = 120 + (feedFast?150:0);
			}
			else
			{
				if(jogInfStartStep == 3 || jogInfStartStep == 2 || jogInfStartStep == 1)
				{
					strcpy(ULog.text[ULog.ix++], "*F50 Stop moving (infeed)");
					Slave[SLAVE_PLOW_IN_INDEX].Command.Halt = true;
					//Slave[SLAVE_BUF_OUT_INDEX].Command.Halt = true;
					jogInfStopStep = 1;
					jogInfStartStep = 0;
				}
				if(jogInfStopStep == 1)
				{
					if(Slave[SLAVE_PLOW_IN_INDEX].AxisState.StandStill)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Stopped (infeed)");
						//bufMasterPF = false;
						Slave[SLAVE_PLOW_IN_INDEX].Command.StartSlave = true;
						if(infeedWasConnected)
						{
							strcpy(ULog.text[ULog.ix++], "*F50 Connect slave (infeed)");
							//   Slave[SLAVE_BUF_OUT_INDEX].Command.StartSlave = true;
							jogInfStopStep = 2;
						}
						else
						{
							strcpy(ULog.text[ULog.ix++], "*F50 Infeed finished (not connected)");
							jogInfStopStep = 0;
						}   
					}
				}
				if(jogInfStopStep == 2)
				{
					if(Slave[SLAVE_PLOW_IN_INDEX].AxisState.SynchronizedMotion && Slave[SLAVE_BUF_OUT_INDEX].AxisState.SynchronizedMotion)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Infeed finished (connected)");
						jogInfStopStep = 0;
					}
				}            
			}
			/**********************************************************/

			/*********************   JOG OUTFEED   *********************/
			if(folder.reading.feedOutfeed)
			{
				if(jogOutfStartStep == 0)
				{
					strcpy(ULog.text[ULog.ix++], "*F50 Outfeed start");
					outfeedWasConnected = Slave[SLAVE_PLOW_OUT_INDEX].AxisState.SynchronizedMotion;
					if(outfeedWasConnected)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Halt slave (outfeed)");
						Slave[SLAVE_PLOW_OUT_INDEX].Command.Halt = true;
					}
					jogOutfStartStep = 1;
				}
				if(jogOutfStartStep == 1)
				{
					if(Slave[SLAVE_PLOW_OUT_INDEX].AxisState.StandStill)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Start moving (outfeed)");
						Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Acceleration = 500;
						Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Deceleration = 500;
						//if (folder.reading.runLocal && readingsF50.coversClosed) Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Velocity = 24000;
						//else Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Velocity = 12000;
						Slave[SLAVE_PLOW_OUT_INDEX].Command.MoveVelocity = true;
						jogOutfStartStep = 2;
					}
				}
				if (folder.reading.runLocal && folder.reading.coversClosed) Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Velocity = 240;
				else Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Velocity = 120 + (feedFast?150:0);
			}
			else
			{
				if(jogOutfStartStep == 2 || jogOutfStartStep == 1)
				{
					strcpy(ULog.text[ULog.ix++], "*F50 Stop moving (outfeed)");
					Slave[SLAVE_PLOW_OUT_INDEX].Command.Halt = true;
					jogOutfStopStep = 1;
					jogOutfStartStep = 0;
				}
				if(jogOutfStopStep == 1)
				{
					if(Slave[SLAVE_PLOW_OUT_INDEX].AxisState.StandStill)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Stopped (outfeed)");
						Slave[SLAVE_PLOW_OUT_INDEX].Command.StartSlave = true;
						if(outfeedWasConnected)
						{
							strcpy(ULog.text[ULog.ix++], "*F50 Connect slave (outfeed)");
							jogOutfStopStep = 2;
						}
						else
						{
							strcpy(ULog.text[ULog.ix++], "*F50 Outfeed finished (not connected)");
							jogOutfStopStep = 0;
						}   
					}
				}
				if(jogOutfStopStep == 2)
				{
					if(Slave[SLAVE_PLOW_OUT_INDEX].AxisState.SynchronizedMotion)
					{
						strcpy(ULog.text[ULog.ix++], "*F50 Outfeed finished (connected)");
						jogOutfStopStep = 0;
					}
				}            
			}
			break;
		case runState:
			// crash check
         Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Acceleration = cutter.setting.masterAcc*10; //5000;
         Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Deceleration = cutter.setting.masterDec*5;
         Slave[SLAVE_PLOW_IN_INDEX].Parameter.Acceleration = cutter.setting.masterAcc*10;
         Slave[SLAVE_PLOW_IN_INDEX].Parameter.Deceleration = cutter.setting.masterDec*5;
			if (firstTimeInState())
			{
				strcpy(ULog.text[ULog.ix++], "*F50 enter runState");
				captureWebWatchSensorState = TRUE;
				cwwssCnt = 200;
				ww1ON=0;
				ww1OFF=0;
				ww2ON=0;
				ww2OFF=0;
				ww3ON=0;
				ww3OFF=0;
			}
			if (buttonStopped) {
			   folder.reading.maximumSpeed = 0.0;
			   captureWebWatchSensorState = TRUE;
			   cwwssCnt = 200;
			}
			else {
			    folder.reading.maximumSpeed = 5.0; //demand max speed to cutter
			}
			
			if (captureWebWatchSensorState && GUI.readings.C50_CurrentSpeed > 5 && cwwssCnt) cwwssCnt--;
			
			if (cwwssCnt < 50 && cwwssCnt > 0 && GUI.readings.C50_CurrentSpeed > 5)
			{
				if (folder.reading.input.webWatch1) ww1ON++;
				else ww1OFF++;
				if (folder.reading.input.webWatch2) ww2ON++;
				else ww2OFF++;
				if (folder.reading.input.webWatch3) ww3ON++;
				else ww3OFF++;
				
				if (cwwssCnt==1) //capture
				{
					ww1=FALSE;
					ww2=FALSE;
					ww3=FALSE;
					if (ww1ON>ww1OFF) ww1=TRUE;
					if (ww2ON>ww2OFF) ww2=TRUE;
					if (ww3ON>ww3OFF) ww3=TRUE;
					ww1ON=0;
					ww2ON=0;
					ww3ON=0;
				}
			}
			
			if (cwwssCnt==0)
			{
				if (ww1 != folder.reading.input.webWatch1) ww1ON++;
				else ww1ON = 0;
				if (ww2 != folder.reading.input.webWatch2) ww2ON++;
				else ww2ON = 0;
				if (ww3 != folder.reading.input.webWatch3) ww3ON++;
				else ww3ON = 0;
				
				if (ww1ON>100) {
					addError(WEB_WATCH,1);
					ww1ON = 0;
				}
				if (ww2ON>100) {
					addError(WEB_WATCH,2);
					ww2ON = 0;
				}
				if (ww3ON>100) {
					addError(WEB_WATCH,3);
					ww3ON = 0;
				}
			}
			folder.reading.ready = TRUE; //printer interface infromation
			folder.reading.error = FALSE; //printer interface infromation
			//Safety_Misc.folderInAuto = TRUE; //stop movement if covers are open
			lastState = state;
			//beepTime = 0;
			break;
		case errorState:
			if (firstTimeInState())
			{
				strcpy(ULog.text[ULog.ix++], "*F50 enter errorState");
			}
			folder.reading.maximumSpeed = 0.0; //demand max speed to cutter
			folder.reading.ready = FALSE; //printer interface infromation
			folder.reading.error = TRUE; //printer interface infromation
			lastState = state;
			beepTime = 0;
			if (errAck) {
				if (e_stop_error_by_f50_in) {
					if (safety.reading.resetPossible && folder.reading.input.eStopInfeed)	{
						state = initState;
						safety.command.eStopReset = TRUE;
						e_stop_error_by_f50_in = FALSE;
						strcpy(ULog.text[ULog.ix++], "*F50 reset E-stop (1 in)");
					}  
				}
				else if (e_stop_error_by_f50_out){
					if (safety.reading.resetPossible && folder.reading.input.eStopOutfeed){
						state = initState;
						safety.command.eStopReset = TRUE;
						e_stop_error_by_f50_out = FALSE;
						strcpy(ULog.text[ULog.ix++], "*F50 reset E-stop (2 out)");
					}  
				}
				else {
					state = initState;
				} 
			}
			break;
	}
	//folder.reading.runState.speedLimit = folder.reading.maximumSpeed; //printer interface infromation
	sourceB50usageLast = buffer.reading.usage;
	errAck = FALSE;
	setOutput();
}

void _EXIT ProgramExit(void)
{
   // Insert code here 
}

void checkEstop(void)
{
   if (!safety.reading.initialized) return;
   if (!folder.reading.input.eStopInfeed  /*eStop.F50_1_Active*/)
   {
      strcpy(ULog.text[ULog.ix++], "*F50 ERROR e-stop (in)");
      //addError(B50_E_STOP,0);
      state = errorState;
      if (!e_stop_error_by_f50_in) state = errorState;
      e_stop_error_by_f50_in = TRUE;
	  addError(E_STOP,1);
   }
   else if (!folder.reading.input.eStopOutfeed /*eStop.F50_2_Active*/)
   {
      strcpy(ULog.text[ULog.ix++], "*F50 ERROR e-stop (out)");
      //addError(B50_E_STOP,0);
      if (!e_stop_error_by_f50_out) state = errorState;
      e_stop_error_by_f50_out = TRUE;
	   addError(E_STOP,2);
   }
   else if (safety.reading.eStop)
   {
      state = initState;
   }
   else
   {
      e_stop_error_by_f50_in = FALSE;
      e_stop_error_by_f50_out = FALSE;  
   }
}

void buttonCommand (void) //controls button push
{
   if (!GlobalCommand.Output.Status.AllDrivesEnable) return;
   static BOOL waitButtonRelease =  true;
   
   BOOL inBtn1;
   BOOL inBtn2;
   BOOL inBtn3;
   BOOL inBtn4;
   BOOL inBtn5;
   BOOL outBtn1;
   BOOL outBtn2;
   BOOL outBtn3;
   BOOL outBtn4;
   BOOL outBtn5;
   
   if (infeedButton.push1) inBtn1 = TRUE;
   else if (folder.command.pressInfeedButton1) inBtn1 = TRUE;
   else inBtn1 = FALSE;
   folder.command.pressInfeedButton1 = FALSE;
   
   if (infeedButton.push2) inBtn2 = TRUE;
   else if (folder.command.pressInfeedButton2) inBtn2 = TRUE;
   else inBtn2 = FALSE;
   folder.command.pressInfeedButton2 = FALSE;
   
   if (infeedButton.push3) inBtn3 = TRUE;
   else if (folder.command.pressInfeedButton3) inBtn3 = TRUE;
   else inBtn3 = FALSE;
   folder.command.pressInfeedButton3 = FALSE;
   
   if (infeedButton.push4) inBtn4 = TRUE;
   else if (folder.command.pressInfeedButton4) inBtn4 = TRUE;
   else inBtn4 = FALSE;
   folder.command.pressInfeedButton4 = FALSE;
   
   if (infeedButton.push5) inBtn5 = TRUE;
   else if (folder.command.pressInfeedButton5) inBtn5 = TRUE;
   else inBtn5 = FALSE;
   folder.command.pressInfeedButton5 = FALSE;
   
   
   if (outfeedButton.push1) outBtn1 = TRUE;
   else if (folder.command.pressOutfeedButton1) outBtn1 = TRUE;
   else outBtn1 = FALSE;
   folder.command.pressOutfeedButton1 = FALSE;
   
   if (outfeedButton.push2) outBtn2 = TRUE;
   else if (folder.command.pressOutfeedButton2) outBtn2 = TRUE;
   else outBtn2 = FALSE;
   folder.command.pressOutfeedButton2 = FALSE;
   
   if (outfeedButton.push3) outBtn3 = TRUE;
   else if (folder.command.pressOutfeedButton3) outBtn3 = TRUE;
   else outBtn3 = FALSE;
   folder.command.pressOutfeedButton3 = FALSE;
   
   if (outfeedButton.push4) outBtn4 = TRUE;
   else if (folder.command.pressOutfeedButton4) outBtn4 = TRUE;
   else outBtn4 = FALSE;
   folder.command.pressOutfeedButton4 = FALSE;
   
   if (outfeedButton.push5) outBtn5 = TRUE;
   else if (folder.command.pressOutfeedButton5) outBtn5 = TRUE;
   else outBtn5 = FALSE;
   folder.command.pressOutfeedButton5 = FALSE;
   
   switch (state)
   {
      case initState:
         //doublePressTimer = 10.0;
         if ((!infeedButton.lastPush1 && inBtn1 ) || (!outfeedButton.lastPush1 && outBtn1))  {
            state = runState;
            //TODO
            //enable drives
         }		 
         if ((!infeedButton.lastPush2 && inBtn2) || (!outfeedButton.lastPush2 && outBtn2)) {
            state = manualState;
            waitButtonRelease = true;
            //TODO
            //enable drives
         }
         /*if ((!infeedButton.lastPush5 && infeedButton.push5)|| (!outfeedButton.lastPush5 && outfeedButton.push5))
         {
            //TODO
            //motors off, disable drives..
            state = errorState;
         }*/
         folder.reading.runLocal = FALSE;
		 folder.reading.inManualLoad = FALSE;	
         break;
      case manualState:
		 folder.reading.inManualLoad = TRUE;
         if(waitButtonRelease)
         {
            folder.reading.runLocal = FALSE;
            if(!inBtn1 && !inBtn2 && !inBtn3 && !inBtn4)
               waitButtonRelease = false;
         }
         else
         {
            if (!infeedButton.lastPush1 && inBtn1 && !folder.reading.runLocal)   state = runState;
            if (!outfeedButton.lastPush1 && outBtn1 && !folder.reading.runLocal) state = runState;
            if (folder.reading.runLocal)
            {
               if ((!outfeedButton.lastPush5 && outBtn5) ||
                  (!infeedButton.lastPush5 && inBtn5)   ||
                  (!outfeedButton.lastPush4 && outBtn4) ||
                  (!infeedButton.lastPush4 && inBtn4)   ||
                  (!outfeedButton.lastPush3 && outBtn3) ||
                  (!infeedButton.lastPush3 && inBtn3)   ||
                  (!outfeedButton.lastPush2 && outBtn2) ||
                  (!infeedButton.lastPush2 && inBtn2)   ||
                  (!outfeedButton.lastPush1 && outBtn1) ||
                  (!infeedButton.lastPush1 && inBtn1)   )
                  folder.reading.runLocal = FALSE;
            }
            else
            {
               if ((!outfeedButton.lastPush5 && outBtn5) ||
                  (!infeedButton.lastPush5 && inBtn5 ))
                  folder.reading.runLocal = TRUE;
            }
            folder.reading.feedInfeed = folder.reading.feedOutfeed = FALSE;
            if (inBtn2 || outBtn2) folder.reading.feedInfeed = TRUE;
            if (inBtn3 || outBtn3) folder.reading.feedOutfeed = TRUE;
            if (inBtn4 || outBtn4 || folder.reading.runLocal || folder.reading.input.feedAllButton || folder.reading.input.feedAllButtonInfeed) folder.reading.feedInfeed = folder.reading.feedOutfeed = TRUE;
            if (state != manualState) folder.reading.feedInfeed = folder.reading.feedOutfeed = FALSE;
            
            if ((!infeedButton.lastPush4 && inBtn4) || (!outfeedButton.lastPush4 && outBtn4))
            {
               //if (doublePressTimer<1.5) feedFast = true;
               //   doublePressTimer = 0.0;
               if (buffer.reading.usage < 20)
               {
                  if (buffer.reading.button5 == _ORANGE)
                  {
                     buffer.command.pressButton5 = true;
                  }
               }
            }
            if ((inBtn4 && inBtn3) || (outBtn4 && outBtn3)) {
               if (!feedFast)
               {
                  Slave[SLAVE_PLOW_IN_INDEX].Command.MoveVelocity = true;
                  Slave[SLAVE_PLOW_OUT_INDEX].Command.MoveVelocity = true;
               }
               feedFast = true;
            }
            else if ((inBtn4 && !inBtn3) || (outBtn4 && !outBtn3))
            {
               if (feedFast)
               {
                  Slave[SLAVE_PLOW_IN_INDEX].Command.MoveVelocity = true;
                  Slave[SLAVE_PLOW_OUT_INDEX].Command.MoveVelocity = true;
               }
               feedFast = false;
            }
            else
            {
               feedFast = false;
            }
            //doublePressTimer += SECONDS_UNIT;
         }
         break;
      case runState:
         //doublePressTimer = 10.0;
         folder.reading.inManualLoad = FALSE;
         if (!infeedButton.lastPush1 && inBtn1 && isInfeedButton(1,COLOR_RED))
         {
            buttonStopped = 1; //sets maximum speed to 0
         }
         if (!outfeedButton.lastPush1 && outBtn1 && isOutfeedButton(1,COLOR_RED))
         {
            buttonStopped = 1; //sets maximum speed to 0
         }
         if (FALSE == folder.reading.input.infeedTopCover || FALSE == folder.reading.input.outfeedTopCover)
         {
            buttonStopped = 1; //sets maximum speed to 0
         }
         if (!infeedButton.lastPush1 && inBtn1 && isInfeedButton(1,COLOR_WHITE))
         {
            buttonStopped = 0; //sets maximum speed to 5.0 m/s
         }
         if (!outfeedButton.lastPush1 && outBtn1 && isOutfeedButton(1,COLOR_WHITE))
         {
            buttonStopped = 0; //sets maximum speed to 5.0 m/s
         }
         if (!infeedButton.lastPush3 && inBtn3 && (isInfeedButton(3,COLOR_YELLOW)||isInfeedButton(3,COLOR_RED)))
         {
            state = initState; //go back
         }
         if (!outfeedButton.lastPush3 && outBtn3 && (isOutfeedButton(3,COLOR_YELLOW)||isOutfeedButton(3,COLOR_RED)))
         {
            state = initState; //go back
         }
         if (!outfeedButton.lastPush4 && outBtn4 && isOutfeedButton(4,COLOR_GREEN))
         {
            folder.setting.tensionIdealPerc -= 5.0;
            if (folder.setting.tensionIdealPerc<0.0) folder.setting.tensionIdealPerc = 0.0;
            folder.command.shortBeep = true;
         }
         if (!outfeedButton.lastPush5 && outBtn5 && isOutfeedButton(5,COLOR_YELLOW))
         {
            folder.setting.tensionIdealPerc += 5.0;
            if (folder.setting.tensionIdealPerc>100) folder.setting.tensionIdealPerc = 100.0;
            folder.command.shortBeep = true;
         }
         if (!infeedButton.lastPush4 && inBtn4 && isInfeedButton(4,COLOR_GREEN))
         {
            folder.setting.tensionIdealPerc -= 5.0;
            if (folder.setting.tensionIdealPerc<0.0) folder.setting.tensionIdealPerc = 0.0;
            folder.command.shortBeep = true;
         }
         if (!infeedButton.lastPush5 && inBtn5 && isInfeedButton(5,COLOR_YELLOW))
         {
            folder.setting.tensionIdealPerc += 5.0;
            if (folder.setting.tensionIdealPerc>100) folder.setting.tensionIdealPerc = 100.0;
            folder.command.shortBeep = true;
         }
         
         folder.reading.runLocal = FALSE;
         break;
      case errorState:
         //doublePressTimer = 10.0;
         folder.reading.inManualLoad = FALSE;
         if ((!infeedButton.lastPush5 && inBtn5)||(!outfeedButton.lastPush5 && outBtn5) )
         {
            errAck = TRUE;           
         }
         folder.reading.runLocal = FALSE;
         break;
   }
   infeedButton.lastPush1 = inBtn1;
   infeedButton.lastPush2 = inBtn2;
   infeedButton.lastPush3 = inBtn3;
   infeedButton.lastPush4 = inBtn4;
   infeedButton.lastPush5 = inBtn5;
   
   outfeedButton.lastPush1 = outBtn1;
   outfeedButton.lastPush2 = outBtn2;
   outfeedButton.lastPush3 = outBtn3;
   outfeedButton.lastPush4 = outBtn4;
   outfeedButton.lastPush5 = outBtn5;
}

void quarterSecondCycle (void) //controls light and blink..
{
   if (buttonLedTest <= 350) {
      startupLight();
      return;
   }
   folder.reading.startUpSeq = 15;
   
   if (!GlobalCommand.Output.Status.AllDrivesEnable) {
      setInfeedButton(1,COLOR_ORANGE);
      setInfeedButton(2,COLOR_ORANGE);
      setInfeedButton(3,COLOR_ORANGE);
      setInfeedButton(4,COLOR_ORANGE);
      setInfeedButton(5,COLOR_ORANGE);
      setOutfeedButton(1,COLOR_ORANGE);
      setOutfeedButton(2,COLOR_ORANGE);
      setOutfeedButton(3,COLOR_ORANGE);
      setOutfeedButton(4,COLOR_ORANGE);
      setOutfeedButton(5,COLOR_ORANGE);
      return; 
   }
    
   switch (state)
   {
      case initState:
         folder.reading.btn2Func = 2;
         folder.reading.btn3Func = 0;
         buttonStopped = 0;
         setInfeedButton(1,COLOR_WHITE);
         setOutfeedButton(1,COLOR_WHITE);

         if (isInfeedButton(2,COLOR_WHITE)) setInfeedButton(2,COLOR_OFF);
         else setInfeedButton(2,COLOR_WHITE);
         if (isOutfeedButton(2,COLOR_WHITE)) setOutfeedButton(2,COLOR_OFF);
         else setOutfeedButton(2,COLOR_WHITE);
			
         setInfeedButton(3,COLOR_OFF);
         setOutfeedButton(3,COLOR_OFF);
  
         setInfeedButton(4,COLOR_OFF);
         setOutfeedButton(4,COLOR_OFF);
			
         setInfeedButton(5,COLOR_OFF);
         setOutfeedButton(5,COLOR_OFF);
			
			/*if (isOutfeedButton(5,COLOR_RED)) setOutfeedButton(5,COLOR_OFF);
		    else setOutfeedButton(5,COLOR_RED);
			if (isInfeedButton(5,COLOR_RED)) setInfeedButton(5,COLOR_OFF);
		    else setInfeedButton(5,COLOR_RED);*/
			
         break;
      case manualState:
         buttonStopped = 0;
         folder.reading.btn2Func = 1;
         folder.reading.btn3Func = 1;
         
         setInfeedButton(1,COLOR_WHITE);
         setInfeedButton(2,COLOR_YELLOW);
         setInfeedButton(3,COLOR_YELLOW);
         setInfeedButton(4,COLOR_YELLOW);
			 
         setOutfeedButton(1,COLOR_WHITE);
         setOutfeedButton(2,COLOR_YELLOW);
         setOutfeedButton(3,COLOR_YELLOW);
         setOutfeedButton(4,COLOR_YELLOW);	 

         if (folder.reading.runLocal)
         {
            if (isInfeedButton(5,COLOR_GREEN))
            {
               setOutfeedButton(5,COLOR_OFF);
               setInfeedButton(5,COLOR_OFF);
            }
            else
            {
               setOutfeedButton(5,COLOR_GREEN);
               setInfeedButton(5,COLOR_GREEN);
            }
         }
         else
         {
            setOutfeedButton(5,COLOR_GREEN);
            setInfeedButton(5,COLOR_GREEN);
         }

			
         //TODO show green button if there is paper in the buffer..(run local)
			
         break;
      case runState:
         folder.reading.btn2Func = 0;
         folder.reading.btn3Func = 0;
         if (!buttonStopped){
            setInfeedButton(1,COLOR_RED);
            setOutfeedButton(1,COLOR_RED);	   
            setInfeedButton(3,COLOR_OFF);
            setOutfeedButton(3,COLOR_OFF);	
         }
         else
         {
            setInfeedButton(1,COLOR_WHITE);
            setOutfeedButton(1,COLOR_WHITE);
            if (isInfeedButton(3,COLOR_RED)) setInfeedButton(3,COLOR_YELLOW);	   
            else setInfeedButton(3,COLOR_RED);
            if (isOutfeedButton(3,COLOR_RED)) setOutfeedButton(3,COLOR_YELLOW);	   
            else setOutfeedButton(3,COLOR_RED);	
         }
         setInfeedButton(2,COLOR_OFF);
         setOutfeedButton(2,COLOR_OFF);
         if (!buttonStopped && cutter.reading.webSpeed > 0.1 && lineController.reading.guiControlLevel > 1) {
            if (folder.setting.tensionIdealPerc > 5.0) {
               setInfeedButton(4,COLOR_GREEN);
               setOutfeedButton(4,COLOR_GREEN);
            }
            else {
               setInfeedButton(4,COLOR_OFF);
               setOutfeedButton(4,COLOR_OFF);  
            }
            if (folder.setting.tensionIdealPerc < 95.0) {
               setInfeedButton(5,COLOR_YELLOW);
               setOutfeedButton(5,COLOR_YELLOW);
            }
            else {
               setInfeedButton(5,COLOR_OFF);
               setOutfeedButton(5,COLOR_OFF);
            }   
         }
         else {
            setInfeedButton(4,COLOR_OFF);
            setOutfeedButton(4,COLOR_OFF);
            setInfeedButton(5,COLOR_OFF);
            setOutfeedButton(5,COLOR_OFF);
         }
         break;
      case errorState:
         buttonStopped = 0;
         folder.reading.btn2Func = 0;
         folder.reading.btn3Func = 0;
		    
         if (e_stop_error_by_f50_out && e_stop_error_by_f50_in)
         {
            if (isInfeedButton(1,COLOR_RED))
            {
               setOutfeedErrorSignal12(0);
               setOutfeedErrorSignal34(1);
               setInfeedErrorSignal12(0);
               setInfeedErrorSignal34(1);
            }
            else
            {
               setOutfeedErrorSignal12(1);
               setOutfeedErrorSignal34(0);
               setInfeedErrorSignal12(1);
               setInfeedErrorSignal34(0);
            }
         }
         else if (e_stop_error_by_f50_out)
         {
            if (isInfeedButton(1,COLOR_RED))
            {
               setOutfeedErrorSignal12(0);
               setOutfeedErrorSignal34(1);
               setInfeedErrorSignal12(0);
               setInfeedErrorSignal34(0);
            }
            else
            {
               setOutfeedErrorSignal12(1);
               setOutfeedErrorSignal34(0);
               setInfeedErrorSignal12(1);
               setInfeedErrorSignal34(1);
            }
         }
         else if (e_stop_error_by_f50_in)
         {
            if (isInfeedButton(1,COLOR_RED))
            {
               setOutfeedErrorSignal12(1);
               setOutfeedErrorSignal34(1);
               setInfeedErrorSignal12(0);
               setInfeedErrorSignal34(1);
            }
            else
            {
               setOutfeedErrorSignal12(0);
               setOutfeedErrorSignal34(0);
               setInfeedErrorSignal12(1);
               setInfeedErrorSignal34(0);
            }
         }
         else
         {
            if (isInfeedButton(1,COLOR_RED))
            {
               setOutfeedErrorSignal12(0);
               setOutfeedErrorSignal34(0);
               setInfeedErrorSignal12(0);
               setInfeedErrorSignal34(0);
            }
            else
            {
               setOutfeedErrorSignal12(1);
               setOutfeedErrorSignal34(1);
               setInfeedErrorSignal12(1);
               setInfeedErrorSignal34(1);
            }
         }
         setInfeedButton(5,COLOR_BLUE);
         setOutfeedButton(5,COLOR_BLUE);
         break;
   }
}

void setInfeedErrorSignal12(BOOL onOff)
{
   if (onOff)
   {
      setInfeedButton(1,COLOR_RED);
      setInfeedButton(2,COLOR_RED);
   }
   else
   {
      setInfeedButton(1,COLOR_OFF);
      setInfeedButton(2,COLOR_OFF);
   }
}

void setInfeedErrorSignal34(BOOL onOff)
{
   if (onOff)
   {
      setInfeedButton(3,COLOR_RED);
      setInfeedButton(4,COLOR_RED);
   }
   else
   {
      setInfeedButton(3,COLOR_OFF);
      setInfeedButton(4,COLOR_OFF);
   }
}

void setOutfeedErrorSignal12(BOOL onOff)
{
   if (onOff)
   {
      setOutfeedButton(1,COLOR_RED);
      setOutfeedButton(2,COLOR_RED);
   }
   else
   {
      setOutfeedButton(1,COLOR_OFF);
      setOutfeedButton(2,COLOR_OFF);
   }
}

void setOutfeedErrorSignal34(BOOL onOff)
{
   if (onOff)
   {
      setOutfeedButton(3,COLOR_RED);
      setOutfeedButton(4,COLOR_RED);
   }
   else
   {
      setOutfeedButton(3,COLOR_OFF);
      setOutfeedButton(4,COLOR_OFF);
   }
}

BOOL firstTimeInState(void)
{
   if (lastState != state) return 1;
   return 0;
}

BOOL isInfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
   if (btnNo == 1)
   {
      if (   grn == infeedButton.color1.green 
         && red == infeedButton.color1.red 
         && yel == infeedButton.color1.yellow
         && whtBlu == infeedButton.color1.white) return 1;
      if (   grn && red && yel && whtBlu 
         && (infeedButton.color1.green || infeedButton.color1.red || infeedButton.color1.yellow || infeedButton.color1.white)) return 1;
      return 0;
   }
   if (btnNo == 2)
   {
      if (   grn == infeedButton.color2.green 
         && red == infeedButton.color2.red 
         && yel == infeedButton.color2.yellow
         && whtBlu == infeedButton.color2.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (infeedButton.color2.green || infeedButton.color2.red || infeedButton.color2.yellow || infeedButton.color2.white)) return 1;
      return 0;
   }
   if (btnNo == 3)
   {
      if (   grn == infeedButton.color3.green 
         && red == infeedButton.color3.red 
         && yel == infeedButton.color3.yellow
         && whtBlu == infeedButton.color3.white) return 1;
      if (   grn && red && yel && whtBlu 
         && (infeedButton.color3.green || infeedButton.color3.red || infeedButton.color3.yellow || infeedButton.color3.white)) return 1;
      return 0;
   }
   if (btnNo == 4)
   {
      if (   grn == infeedButton.color4.green 
         && red == infeedButton.color4.red 
         && yel == infeedButton.color4.yellow
         && whtBlu == infeedButton.color4.white) return 1;
      if (   grn && red && yel && whtBlu 
         && (infeedButton.color4.green || infeedButton.color4.red || infeedButton.color4.yellow || infeedButton.color4.white)) return 1;
      return 0;
   }
   if (btnNo == 5)
   {
      if (   grn == infeedButton.color5.green 
         && red == infeedButton.color5.red 
         && yel == infeedButton.color5.yellow
         && whtBlu == infeedButton.color5.blue) return 1;
      if (   grn && red && yel && whtBlu 
         && (infeedButton.color5.green || infeedButton.color5.red || infeedButton.color5.yellow || infeedButton.color5.blue)) return 1;
      return 0;
   }
   return 0;
}
BOOL isOutfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
   if (btnNo == 1)
   {
      if (   grn == outfeedButton.color1.green 
         && red == outfeedButton.color1.red 
         && yel == outfeedButton.color1.yellow
         && whtBlu == outfeedButton.color1.white) return 1;
      if (   grn && red && yel && whtBlu 
         && (outfeedButton.color1.green || outfeedButton.color1.red || outfeedButton.color1.yellow || outfeedButton.color1.white)) return 1;
      return 0;
   }
   if (btnNo == 2)
   {
      if (   grn == outfeedButton.color2.green 
         && red == outfeedButton.color2.red 
         && yel == outfeedButton.color2.yellow
         && whtBlu == outfeedButton.color2.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (outfeedButton.color2.green || outfeedButton.color2.red || outfeedButton.color2.yellow || outfeedButton.color2.white)) return 1;
      return 0;
   }
   if (btnNo == 3)
   {
      if (   grn == outfeedButton.color3.green 
         && red == outfeedButton.color3.red 
         && yel == outfeedButton.color3.yellow
         && whtBlu == outfeedButton.color3.white) return 1;
      if (   grn && red && yel && whtBlu 
         && (outfeedButton.color3.green || outfeedButton.color3.red || outfeedButton.color3.yellow || outfeedButton.color3.white)) return 1;
      return 0;
   }
   if (btnNo == 4)
   {
      if (   grn == outfeedButton.color4.green 
         && red == outfeedButton.color4.red 
         && yel == outfeedButton.color4.yellow
         && whtBlu == outfeedButton.color4.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (outfeedButton.color4.green || outfeedButton.color4.red || outfeedButton.color4.yellow || outfeedButton.color4.white)) return 1;
      return 0;
   }
   if (btnNo == 5)
   {
      if (   grn == outfeedButton.color5.green 
         && red == outfeedButton.color5.red 
         && yel == outfeedButton.color5.yellow
         && whtBlu == outfeedButton.color5.blue) return 1;
      if (   grn && red && yel && whtBlu 
         && (outfeedButton.color5.green || outfeedButton.color5.red || outfeedButton.color5.yellow || outfeedButton.color5.blue)) return 1;
      return 0;
   }
   return 0;
}

void setInfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{

   if (btnNo == 1)
   {
      infeedButton.color1.green 	= grn;
      infeedButton.color1.red   	= red;
      infeedButton.color1.yellow = yel;
      infeedButton.color1.white 	= whtBlu;
      if      (infeedButton.color1.yellow && infeedButton.color2.red) folder.reading.infeedButton1 = _ORANGE;
      else if (infeedButton.color1.yellow)                         folder.reading.infeedButton1    = _YELLOW;
      else if (infeedButton.color1.red)                            folder.reading.infeedButton1    = _RED;
      else if (infeedButton.color1.green)                          folder.reading.infeedButton1    = _GREEN;
      else if (infeedButton.color1.white)                          folder.reading.infeedButton1    = _WHITE;
      else                                                         folder.reading.infeedButton1    = _OFF;
   }
   if (btnNo == 2)
   {
      infeedButton.color2.green 	= grn;
      infeedButton.color2.red   	= red;
      infeedButton.color2.yellow = yel;
      infeedButton.color2.white 	= whtBlu;
      if      (infeedButton.color2.yellow && infeedButton.color2.red) folder.reading.infeedButton2 = _ORANGE;
      else if (infeedButton.color2.yellow)                         folder.reading.infeedButton2    = _YELLOW;
      else if (infeedButton.color2.red)                            folder.reading.infeedButton2    = _RED;
      else if (infeedButton.color2.green)                          folder.reading.infeedButton2    = _GREEN;
      else if (infeedButton.color2.white)                          folder.reading.infeedButton2    = _WHITE;
      else                                                         folder.reading.infeedButton2    = _OFF;
   }
   if (btnNo == 3)
   {
      infeedButton.color3.green 	= grn;
      infeedButton.color3.red   	= red;
      infeedButton.color3.yellow   = yel;
      infeedButton.color3.white 	= whtBlu;
      if      (infeedButton.color3.yellow && infeedButton.color2.red) folder.reading.infeedButton3 = _ORANGE;
      else if (infeedButton.color3.yellow)                         folder.reading.infeedButton3    = _YELLOW;
      else if (infeedButton.color3.red)                            folder.reading.infeedButton3    = _RED;
      else if (infeedButton.color3.green)                          folder.reading.infeedButton3    = _GREEN;
      else if (infeedButton.color3.white)                          folder.reading.infeedButton3    = _WHITE;
      else                                                         folder.reading.infeedButton3    = _OFF;
   }
   if (btnNo == 4)
   {
      infeedButton.color4.green 	= grn;
      infeedButton.color4.red   	= red;
      infeedButton.color4.yellow   = yel;
      infeedButton.color4.white 	= whtBlu;
      if      (infeedButton.color4.yellow && infeedButton.color2.red) folder.reading.infeedButton4 = _ORANGE;
      else if (infeedButton.color4.yellow)                         folder.reading.infeedButton4    = _YELLOW;
      else if (infeedButton.color4.red)                            folder.reading.infeedButton4    = _RED;
      else if (infeedButton.color4.green)                          folder.reading.infeedButton4    = _GREEN;
      else if (infeedButton.color4.white)                          folder.reading.infeedButton4    = _WHITE;
      else                                                         folder.reading.infeedButton4    = _OFF;
   }
   if (btnNo == 5)
   {
      infeedButton.color5.green 	= grn;
      infeedButton.color5.red   	= red;
      infeedButton.color5.yellow   = yel;
      infeedButton.color5.blue 	= whtBlu;
      if      (infeedButton.color5.yellow && infeedButton.color2.red) folder.reading.infeedButton5 = _ORANGE;
      else if (infeedButton.color5.yellow)                         folder.reading.infeedButton5    = _YELLOW;
      else if (infeedButton.color5.red)                            folder.reading.infeedButton5    = _RED;
      else if (infeedButton.color5.green)                          folder.reading.infeedButton5    = _GREEN;
      else if (infeedButton.color5.blue)                          folder.reading.infeedButton5    = _BLUE;
      else                                                         folder.reading.infeedButton5    = _OFF;
   }
}

void setOutfeedButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
   if (btnNo == 1)
   {
      outfeedButton.color1.green 	= grn;
      outfeedButton.color1.red   	= red;
      outfeedButton.color1.yellow  = yel;
      outfeedButton.color1.white 	= whtBlu;
      if      (outfeedButton.color1.yellow && b50button.color2.red) folder.reading.outfeedButton1 = _ORANGE;
      else if (outfeedButton.color1.yellow)                         folder.reading.outfeedButton1 = _YELLOW;
      else if (outfeedButton.color1.red)                            folder.reading.outfeedButton1 = _RED;
      else if (outfeedButton.color1.green)                          folder.reading.outfeedButton1 = _GREEN;
      else if (outfeedButton.color1.white)                          folder.reading.outfeedButton1 = _WHITE;
      else                                                          folder.reading.outfeedButton1 = _OFF;
   }
   if (btnNo == 2)
   {
      outfeedButton.color2.green 	= grn;
      outfeedButton.color2.red   	= red;
      outfeedButton.color2.yellow  = yel;
      outfeedButton.color2.white 	= whtBlu;
      if      (outfeedButton.color2.yellow && b50button.color2.red) folder.reading.outfeedButton2 = _ORANGE;
      else if (outfeedButton.color2.yellow)                         folder.reading.outfeedButton2 = _YELLOW;
      else if (outfeedButton.color2.red)                            folder.reading.outfeedButton2 = _RED;
      else if (outfeedButton.color2.green)                          folder.reading.outfeedButton2 = _GREEN;
      else if (outfeedButton.color2.white)                          folder.reading.outfeedButton2 = _WHITE;
      else                                                          folder.reading.outfeedButton2 = _OFF;
   }
   if (btnNo == 3)
   {
      outfeedButton.color3.green 	= grn;
      outfeedButton.color3.red   	= red;
      outfeedButton.color3.yellow  = yel;
      outfeedButton.color3.white 	= whtBlu;
      if      (outfeedButton.color3.yellow && b50button.color2.red) folder.reading.outfeedButton3 = _ORANGE;
      else if (outfeedButton.color3.yellow)                         folder.reading.outfeedButton3 = _YELLOW;
      else if (outfeedButton.color3.red)                            folder.reading.outfeedButton3 = _RED;
      else if (outfeedButton.color3.green)                          folder.reading.outfeedButton3 = _GREEN;
      else if (outfeedButton.color3.white)                          folder.reading.outfeedButton3 = _WHITE;
      else                                                          folder.reading.outfeedButton3 = _OFF;
   }
   if (btnNo == 4)
   {
      outfeedButton.color4.green 	= grn;
      outfeedButton.color4.red   	= red;
      outfeedButton.color4.yellow  = yel;
      outfeedButton.color4.white 	= whtBlu;
      if      (outfeedButton.color4.yellow && b50button.color2.red) folder.reading.outfeedButton4 = _ORANGE;
      else if (outfeedButton.color4.yellow)                         folder.reading.outfeedButton4 = _YELLOW;
      else if (outfeedButton.color4.red)                            folder.reading.outfeedButton4 = _RED;
      else if (outfeedButton.color4.green)                          folder.reading.outfeedButton4 = _GREEN;
      else if (outfeedButton.color4.white)                          folder.reading.outfeedButton4 = _WHITE;
      else                                                          folder.reading.outfeedButton4 = _OFF;
   }
   if (btnNo == 5)
   {
      outfeedButton.color5.green 	= grn;
      outfeedButton.color5.red   	= red;
      outfeedButton.color5.yellow  = yel;
      outfeedButton.color5.blue 	= whtBlu;
      if      (outfeedButton.color5.yellow && b50button.color2.red) folder.reading.outfeedButton5 = _ORANGE;
      else if (outfeedButton.color5.yellow)                         folder.reading.outfeedButton5 = _YELLOW;
      else if (outfeedButton.color5.red)                            folder.reading.outfeedButton5 = _RED;
      else if (outfeedButton.color5.green)                          folder.reading.outfeedButton5 = _GREEN;
      else if (outfeedButton.color5.blue)                          folder.reading.outfeedButton5 = _BLUE;
      else                                                          folder.reading.outfeedButton5 = _OFF;
   }
}

void setOutput (struct output_typ *x, BOOL val)
{
   x->pVal = val;
   if (!x->force)
   {
      x->oVal = val;
      return;
   }  
   x->oVal = x->fVal;
}

void startupLight(void)
{
   if (buttonLedTest < 10) {
      folder.reading.startUpSeq = 1;
      if (buttonLedTest == 0) {
         folder.command.shortBeep = TRUE;
      }
      setInfeedButton(1, COLOR_OFF);
      setInfeedButton(2, COLOR_OFF);
      setInfeedButton(3, COLOR_OFF);
      setInfeedButton(4, COLOR_OFF);
      setInfeedButton(5,COLOR_OFF);
      setOutfeedButton(1,COLOR_OFF);
      setOutfeedButton(2,COLOR_OFF);
      setOutfeedButton(3,COLOR_OFF);
      setOutfeedButton(4,COLOR_OFF);
      setOutfeedButton(5,COLOR_OFF);
   }
   else if (buttonLedTest<20) {
      setInfeedButton(1,COLOR_GREEN);
   }
   else if (buttonLedTest<30) {
      if (isInfeedButton(2,COLOR_GREEN)) setInfeedButton(2,COLOR_OFF);
      else setInfeedButton(2,COLOR_GREEN);
      folder.reading.startUpSeq = 2;
   }
   else if (buttonLedTest<40) {
      setInfeedButton(2,COLOR_GREEN);
      folder.reading.startUpSeq = 3;
   }
   else if (buttonLedTest<50) {
      if (isInfeedButton(3,COLOR_GREEN)) setInfeedButton(3,COLOR_OFF);
      else setInfeedButton(3,COLOR_GREEN);
      folder.reading.startUpSeq = 4;
   }
   else if (buttonLedTest<60) {
      setInfeedButton(3,COLOR_GREEN);
   }
   else if (buttonLedTest<70) {
      if (isInfeedButton(4,COLOR_GREEN)) setInfeedButton(4,COLOR_OFF);
      else setInfeedButton(4,COLOR_GREEN);
      folder.reading.startUpSeq = 5;
   }
   else if (buttonLedTest<80) {
      setInfeedButton(4,COLOR_GREEN);
      folder.reading.startUpSeq = 6;
   }
   else if (buttonLedTest<90) {
      if (isInfeedButton(5,COLOR_GREEN)) setInfeedButton(5,COLOR_OFF);
      else setInfeedButton(5,COLOR_GREEN);
      folder.reading.startUpSeq = 7;
   }
   else if (buttonLedTest<100) {
      setInfeedButton(5,COLOR_GREEN);
      folder.reading.startUpSeq = 8;
   }
   else if (buttonLedTest<110) {
      if (isOutfeedButton(1,COLOR_GREEN)) setOutfeedButton(1,COLOR_OFF);
      else setOutfeedButton(1,COLOR_GREEN);
      folder.reading.startUpSeq = 9;
   }
   else if (buttonLedTest<125) {
      setOutfeedButton(1,COLOR_GREEN);
      folder.reading.startUpSeq = 10;
   }
   else if (buttonLedTest<140) {
      if (isOutfeedButton(2,COLOR_GREEN)) setOutfeedButton(2,COLOR_OFF);
      else setOutfeedButton(2,COLOR_GREEN);
      folder.reading.startUpSeq = 11;
   }
   else if (buttonLedTest<155) {
      setOutfeedButton(2,COLOR_GREEN);
      folder.reading.startUpSeq = 12;
   }
   else if (buttonLedTest<170) {
      if (isOutfeedButton(3,COLOR_GREEN)) setOutfeedButton(3,COLOR_OFF);
      else setOutfeedButton(3,COLOR_GREEN);
      folder.reading.startUpSeq = 13;
   }
   else if (buttonLedTest<185) {
      setOutfeedButton(3,COLOR_GREEN);
      folder.reading.startUpSeq = 14;
   }
   else if (buttonLedTest<200) {
      if (isOutfeedButton(4,COLOR_GREEN)) setOutfeedButton(4,COLOR_OFF);
      else setOutfeedButton(4,COLOR_GREEN);
   }
   else if (buttonLedTest<210) {
      setOutfeedButton(4,COLOR_GREEN);
      folder.reading.startUpSeq = 15;
   }
   else if (buttonLedTest<250) {
      if (isOutfeedButton(5,COLOR_GREEN)) setOutfeedButton(5,COLOR_OFF);
      else setOutfeedButton(5,COLOR_GREEN);
   }
   else if (buttonLedTest<300) {
      setOutfeedButton(5,COLOR_GREEN);
   }
   if (GlobalCommand.Output.Status.AllDrivesEnable) {
      folder.command.shortBeep = TRUE;
      setInfeedButton(1,COLOR_GREEN);
      setInfeedButton(2,COLOR_GREEN);
      setInfeedButton(3,COLOR_GREEN);
      setInfeedButton(4,COLOR_GREEN);
      setInfeedButton(5,COLOR_GREEN);
      setOutfeedButton(1,COLOR_GREEN);
      setOutfeedButton(2,COLOR_GREEN);
      setOutfeedButton(3,COLOR_GREEN);
      setOutfeedButton(4,COLOR_GREEN);
      setOutfeedButton(5,COLOR_GREEN);
      buttonLedTest = 351;
   }
   buttonLedTest++;
}


TYPE
	merger_ext_command_typ : 	STRUCT 
		dummy : BOOL; (*deliver next (over min stack)*)
	END_STRUCT;
	merger_readings_typ : 	STRUCT 
		input : physical_merger_inputs; (*current state of input*)
		output : physical_merger_outputs; (*current state of output*)
		startUpSeq : UINT := 0; (*0 -> 15*)
		ioBusModuleOk : BOOL;
		status : machineStatus_typ;
	END_STRUCT;
	merger_settings_typ : 	STRUCT 
		forceOutputEnable : physical_merger_outputs;
		forceOutputValue : physical_merger_outputs;
		enabled : BOOL := FALSE;
		punchUnitEnabled : BOOL := FALSE;
		sensorPunchDist : REAL := 1000; (*mm*)
		infeedOverSpeed : DINT := 0;
		tensionIdealPerc : DINT := 30;
		tensionLowPos : DINT := 24000;
		tensionHighPos : DINT := 4000;
	END_STRUCT;
	merger_typ : 	STRUCT 
		command : merger_ext_command_typ;
		reading : merger_readings_typ;
		setting : merger_settings_typ;
	END_STRUCT;
	physical_merger_inputs : 	STRUCT 
		frontDoor : BOOL;
		eStop : BOOL;
		feedButton : BOOL;
		webBreak : BOOL;
		webTensionInfeed : INT;
	END_STRUCT;
	physical_merger_outputs : 	STRUCT 
		dummy : BOOL;
	END_STRUCT;
END_TYPE

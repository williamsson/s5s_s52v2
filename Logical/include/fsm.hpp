/********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program: mainControl
 * File: fsm.hpp
 * Author: Mats Henriksson
 * Created: March 17, 2014
 *******************************************************************/

#ifndef _FSM_HPP
#define _FSM_HPP

class FSM
{
   private:   
      bool entry;
      unsigned int runTime;
   protected:
      int currentState;
      int currentStateTimer;   
      int timer;

      FSM(int initialState)
      {
         timer = 0;
         switchState(initialState, true);
         entry = true;
         runTime = 0;
      }
      virtual ~FSM()
      {
         // no dynamic content to free
      }

      bool switchState(int newState, bool condition)
      {
         int oldState = currentState;
         if(condition && (newState != currentState))
         {      
            if(newState != oldState)
            {
               currentState = newState;
               currentStateTimer = 0;
               return true;
            }
         }      
         return false;
      }
      bool switchState(int newState)
      {
         return switchState(newState, true);
      }

      bool entered(void)
      {
         return entry;
      }
   public:
      virtual int update(void)
      {
         if(!currentStateTimer)
            entry = true;
         else
            entry = false;
         if(timer)
            timer--;
         currentStateTimer++;
         runTime++;
         return 0;
      }
      int getState(void)
      {
         return currentState;
      }
      int getCurrentStateTime(void)
      {
         return currentStateTimer;
      }
      int getTime(void)
      {
         return runTime;
      }
};

#endif // _FSM_HPP


TYPE
	lightTowerSegmentColor_typ : 
		(
		lt_off,
		lt_green,
		lt_red,
		lt_yellow,
		lt_blue,
		lt_orange,
		lt_user,
		lt_white,
		lt_frontColor
		);
	check_typ : 	STRUCT 
		active : USINT;
		node : USINT;
		station : USINT;
		moduleID : UINT;
	END_STRUCT;
	lineLog_typ : 	STRUCT 
		Reboots : USINT;
		TimeSinceStart : UDINT;
		Sub : UINT;
		ErrId : UINT;
		AvgRunSpeed : UINT;
		Distance : UDINT;
	END_STRUCT;
END_TYPE

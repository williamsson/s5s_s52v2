
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#define VERSION                    5049

/******************************************************************
192.168.110 + line number.xxx :start at 111 (default)
Machine    Unit IP (xxx)
T50        Controller 021   
S5x        Camera 051, Gripper 041, EBM 040                      
C5x        Controller 020, Display 030, Computer 001, Camera 050  
F50        Display 031

R20        Controller 081, Display 080
U20        Controller 071, Display 070
      
Printer interface 016,017
*******************************************************************/

#define STARTUP_TIME               100

#define SHOW                       0
#define HIDE                       1
#define LOCK                       2
#define UNLOCK                     0

#define COLOR_OFF_MASK_1_3_5       0x00
#define COLOR_GREEN_MASK_1_3_5     0x01
#define COLOR_RED_MASK_1_3_5       0x02
#define COLOR_YELLOW_MASK_1_3_5    0x03
#define COLOR_BLUE_MASK_1_3_5      0x04
#define COLOR_ORANGE_MASK_1_3_5    0x05
#define COLOR_USER_MASK_1_3_5      0x06
#define COLOR_WHITE_MASK_1_3_5     0x07

#define COLOR_OFF_MASK_2_4         0x00
#define COLOR_GREEN_MASK_2_4       0x10
#define COLOR_RED_MASK_2_4         0x20
#define COLOR_YELLOW_MASK_2_4      0x30
#define COLOR_BLUE_MASK_2_4        0x40
#define COLOR_ORANGE_MASK_2_4      0x50
#define COLOR_USER_MASK_2_4        0x60
#define COLOR_WHITE_MASK_2_4       0x70

#define SECONDS_UNIT               0.1008
#define MAX_SLOW_SPEED             0.50
#define MAX_MEDIUM_SPEED           1.15



// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF; 
//void setOutput (struct output_typ *x, BOOL val);
//BOOL slowRequestFlag=0;

USINT test[8];

#define LIGHT_OFF               0
#define LIGHT_GREEN             1
#define LIGHT_RED               2
#define LIGHT_YELLOW            3
#define LIGHT_BLUE              4
#define LIGHT_ORANGE            5
#define LIGHT_USER              6
#define LIGHT_WHITE             7
#define LIGHT_FORWARD           8

void lightTower(void);
void readMachineStates(void);
void uwMachineState(void);
REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max);
int stopPlateLightTower(float rV, float sP);
int streamRollerLightTower(float rV, float sP);
void handleLineLog(void);
USINT startup;
USINT beep;
USINT signalNoPaperSeq=0;

USINT rl[21];

USINT userILevel=0;
USINT userIcolor=0;
REAL userLimit;
BOOL  userI_LT_control=0;
LREAL secondCnt;

//USINT segmentColor[5];
//USINT segmentBkgColor[5];

void nodeCheck(void)
{
   static USINT nodes;
   static UINT mf,nf;
   int i,newNode;
   if (!nodes) mf=nf=0;
   newNode = 1;
   for (i=0;i<13;i++) {
      if (node[nodes].station[i]) {
         mf++;
         if (newNode) nf++;
         newNode = 0;
      }
   }
   nodes++;
   if (nodes>49) {
      lineController.reading.modulesFound = mf;
      lineController.reading.nodesFound = nf;
      nodes=0;
   }
}

/*
union userFunctionReturnTyp
{
   USINT all;
   struct 
   {
      unsigned ltControl:            1;
      unsigned pause:    	    	    1;
      unsigned slow:                 1;
      unsigned stop:                 1;
		
      unsigned limit:                1;
      unsigned dummy2:               1;
      unsigned dummy3:               1;
      unsigned dummy4:               1;          
   }bits;
};*/

union userFunctionReturnTyp
{
   UINT all;
   struct 
   {
      unsigned ltControl:            1;
      unsigned pause:    	       	 1;
      unsigned slow:                 1;
      unsigned stop:                 1;
		
      unsigned limit:                1;
      unsigned dummy8:               1;
      unsigned dummy9:               1;
      unsigned stacker1:             1;   
      
      unsigned stacker2:             1;
      unsigned dummy1:    	       	 1;
      unsigned dummy2:               1;
      unsigned dummy3:               1;
		
      unsigned dummy4:               1;
      unsigned dummy5:               1;
      unsigned dummy6:               1;
      unsigned dummy7:               1; 
   }bits;
};



BOOL triggerCheck(struct trigger_typ *x,REAL *time, REAL *release, BOOL currentState)
{
   bool tmp=0;

   switch (x->source)
   {
      case 8: //speed
         if (cutter.reading.webSpeed > x->parameter)
            tmp = 1;
         break;
      case 9: //printer speed
         if (buffer.reading.infeedSpeed > x->parameter)
            tmp = 1;
         break;
      case 10: //buffer usage
         if (buffer.reading.usage > x->parameter)
            tmp = 1;
         break;
      case 11: //deliver request stacker
         if (stacker.reading.deliverPageReceived)
            tmp = 1;
         break;
      case 12: //processing white sheets
         if (cutter.reading.processingWhite)
            tmp = 1;
         break;
      case 13: //slow speed
         if (lineController.reading.slowDown)
            tmp = 1;
         break;       
      case 14: //missing marks
         if (cutter.reading.missingMarks)
            tmp = 1;
         break; 
      case 15: //delivery table full
         if (stacker.reading.input.seqUnitNotReady)
            tmp = 1;
         break;
      case 16: //stack delivery
         if (stacker.reading.stackExit)
            tmp = 1;
         break;
      default: //digital input
         tmp = false;
         break;
   }
   
   
   
   if (tmp == x->contactState) {
      *time += SECONDS_UNIT;
      if (*time > x->delay) *release = x->release;
   }
   else {
      *time = 0;
      if (*release > 0) *release -= SECONDS_UNIT;
   }
   
   if (*time > x->delay || *release > 0) return 1;
   return 0;
}


UINT userFunction(struct userFunction_typ *x, UINT func)
{
   BOOL A,B,Result=0;
   union userFunctionReturnTyp retVal;
   retVal.all = 0;
   if (func == 1) lineController.reading.function1 = 0;
   if (func == 2) lineController.reading.function2 = 0;
   if (func == 3) lineController.reading.function3 = 0;
   if (func == 4) lineController.reading.function4 = 0;
   
   if (!x->activate) return retVal.all;
   
   if (0 == x->inputA) A = 1;
   if (1 == x->inputA) A = lineController.reading.trigger1;
   if (2 == x->inputA) A = lineController.reading.trigger2;
   if (3 == x->inputA) A = lineController.reading.trigger3;
   if (4 == x->inputA) A = lineController.reading.trigger4;
   if (5 == x->inputA) A = lineController.reading.trigger5;
   if (6 == x->inputA) A = lineController.reading.trigger6;
   
   if (0 == x->inputB) B = 1;
   if (1 == x->inputB) B = lineController.reading.trigger1;
   if (2 == x->inputB) B = lineController.reading.trigger2;
   if (3 == x->inputB) B = lineController.reading.trigger3;
   if (4 == x->inputB) B = lineController.reading.trigger4;
   if (5 == x->inputB) B = lineController.reading.trigger5;
   if (6 == x->inputB) B = lineController.reading.trigger6;
   
   if (x->logic)
      Result = (A && B);
   else
      Result = (A || B);
   
   if (!Result) return retVal.all; 
   
   if (func == 1) lineController.reading.function1 = Result;
   if (func == 2) lineController.reading.function2 = Result;
   if (func == 3) lineController.reading.function3 = Result;
   if (func == 4) lineController.reading.function4 = Result;

   switch (x->typ)
   {
      case 0: //speed limit
         retVal.bits.limit = 1;
         if (x->parameter1 < userLimit) userLimit = x->parameter1;
         break;
      case 1: //audio alarm signal
         lineController.command.beep = 1;
         break;
      case 2: //light tower color
         retVal.bits.ltControl = 1;
         userIcolor = x->setting1;
         userILevel = x->setting2;
         break;
      case 3: //pause printer
         retVal.bits.pause = 1;
         break;
      case 4: //hardStop printer
         retVal.bits.stop = 1;
         break;
      case 5: //slowDown printer
         retVal.bits.slow = 1;
         break;
      case 6: //modify step 
         //retVal.bits.stepModify = 1;
         //stepModifyValue = (REAL)(x->parameter1);   //not implemented in s52
         break;
      case 7:
         //retVal.bits.warning1 = 1;   //not implemented in s52
         break;
      case 8:
         retVal.bits.stacker1 = 1;  
         break;
      case 9:
         retVal.bits.stacker2 = 1;  
         break;
   }
   return retVal.all;
   
}

void controlMedSlowSpeed (void)
{
   static REAL slowTime;  
   
   if (!lastMed && cutter.reading.wastePages > 0 && !lineController.command.slowSpeed)
   {
      lineController.command.mediumSpeed = true;
      midSpeedByWaste = true;
      slowTime = 5.0;
   }
   
   if (midSpeedByWaste && cutter.reading.wastePages == 0)
   {
      slowTime -= SECONDS_UNIT;
      if (slowTime <= 0)
      {
         lineController.command.mediumSpeed = false;
         midSpeedByWaste = false;
      }
   }
   
   if (lineController.command.slowSpeed) 
   {
      if (!lastSlow) lineController.command.mediumSpeed = false;
      if (MAX_SLOW_SPEED < lineController.reading.maximumSpeed) lineController.reading.maximumSpeed = MAX_SLOW_SPEED;
   }
   
   if (lineController.command.mediumSpeed) 
   {
      if (lastMed) lineController.command.slowSpeed = false;
      if (cutter.reading.wastePages > 0 && cutter.setting.noMarksSlowSpeed < lineController.reading.maximumSpeed) 
         lineController.reading.maximumSpeed = cutter.setting.noMarksSlowSpeed;
      else if  (MAX_MEDIUM_SPEED < lineController.reading.maximumSpeed) 
         lineController.reading.maximumSpeed = MAX_MEDIUM_SPEED;
   }

   lastSlow = lineController.command.slowSpeed;
   lastMed = lineController.command.mediumSpeed;
}

USINT userInput(void)
{
   static REAL t1;
   static REAL t2;
   static REAL t3;
   static REAL t4;
   static REAL t5;
   static REAL t6;
   
   static REAL r1;
   static REAL r2;
   static REAL r3;
   static REAL r4;
   static REAL r5;
   static REAL r6;
   
   userLimit = 10.0;
   union userFunctionReturnTyp userInputResult;
   userInputResult.all = 0;
   
   lineController.reading.trigger1 = triggerCheck(&lineController.setting.trigger1,&t1,&r1,stacker.reading.input.userInput1);
   lineController.reading.trigger2 = triggerCheck(&lineController.setting.trigger2,&t2,&r2,stacker.reading.input.userInput2);
   lineController.reading.trigger3 = triggerCheck(&lineController.setting.trigger3,&t3,&r3,stacker.reading.input.userInput2);
   lineController.reading.trigger4 = triggerCheck(&lineController.setting.trigger4,&t4,&r4,stacker.reading.input.userInput2);
   lineController.reading.trigger5 = triggerCheck(&lineController.setting.trigger5,&t5,&r5,stacker.reading.input.userInput2);
   lineController.reading.trigger6 = triggerCheck(&lineController.setting.trigger6,&t6,&r6,stacker.reading.input.userInput2);
   
   userInputResult.all = userInputResult.all | userFunction(&lineController.setting.function1,1);
   userInputResult.all = userInputResult.all | userFunction(&lineController.setting.function2,2);
   userInputResult.all = userInputResult.all | userFunction(&lineController.setting.function3,3);
   userInputResult.all = userInputResult.all | userFunction(&lineController.setting.function4,4);
   
   lineController.reading.funcResult_Dev = userInputResult.all;
   
   if (userInputResult.bits.ltControl) {
      ltRunMode = 10;
   }
   
   lineController.reading.userOutput.stacker._1.functionControlled = false;
   lineController.reading.userOutput.stacker._2.functionControlled = false;
   
   if (  (lineController.setting.function1.typ == 8 && lineController.setting.function1.activate)
      || (lineController.setting.function2.typ == 8 && lineController.setting.function2.activate)
      || (lineController.setting.function3.typ == 8 && lineController.setting.function3.activate)
      || (lineController.setting.function4.typ == 8 && lineController.setting.function4.activate)) 
   {
      lineController.reading.userOutput.stacker._1.functionControlled = true;
   }
   if (  (lineController.setting.function1.typ == 9 && lineController.setting.function1.activate)
      || (lineController.setting.function2.typ == 9 && lineController.setting.function2.activate)
      || (lineController.setting.function3.typ == 9 && lineController.setting.function3.activate)
      || (lineController.setting.function4.typ == 9 && lineController.setting.function4.activate)) 
   {
      lineController.reading.userOutput.stacker._2.functionControlled = true;
   }
   
   lineController.reading.userOutput.stacker._1.status = userInputResult.bits.stacker1;
   lineController.reading.userOutput.stacker._2.status = userInputResult.bits.stacker2; 
   return userInputResult.all;
}

void _INIT ProgramInit(void)
{
   // Insert code here 
   startup = 0;
   rl[0] = 0;
   rl[1] = 12;
   rl[2] = 25;
   rl[3] = 38;
   rl[4] = 51;
   rl[5] = 63;
   rl[6] = 76;
   rl[7] = 89;
   rl[8] = 102;
   rl[9] = 114;
   rl[10] = 127;
   rl[11] = 140;
   rl[12] = 153;
   rl[13] = 165;
   rl[14] = 178;
   rl[15] = 191;
   rl[16] = 204;
   rl[17] = 216;
   rl[18] = 229;
   rl[19] = 242;
   rl[20] = 255;
   lineController.reading.timeSinceStart=0;
   secondCnt=0;
   int i,j;
   for (i=0;i<50;i++) {
      for (j=0;j<12;j++)
         node[i].moduleID[j]=0;
   }
   if (lineController.setup.stacker.electricalVersion == 1) {
      check[0].active = true;
      check[0].node = 10;
      check[0].station = 2;
   }
   lineController.reading.guiControlLevel = 1;
   lineController.reading.guiControlLevel2Show = 1;
   lineController.reading.guiControlLevel3Show = 1;
   lineController.reading.guiControlLevel4Show = 1;
   lineController.reading.guiControlLevel5Show = 1;
   lineController.reading.guiControlLevel2Unlock = 2;
   lineController.reading.guiControlLevel3Unlock = 2;
   lineController.reading.guiControlLevel4Unlock = 2;
   lineController.reading.guiControlLevel5Unlock = 2;
   lineController.reading.guiControShowWhenStopped = 0;
   lineController.reading.version = VERSION;
   lineController.reading.guiControlShowMerger = HIDE;
   lineController.reading.guiControlShowFolder = HIDE;
   bootUps++;
}



void _CYCLIC ProgramCyclic(void)
{
   static UDINT lastCode;
   static USINT monkeyTime;
   static BOOL lastStackerCover;
   static BOOL wasError;
   static UDINT curErr,curSub;
   union userFunctionReturnTyp userInputResult;
   secondCnt += SECONDS_UNIT;
   handleLineLog();
   lineController.reading.maximumSpeed = 10.0;
   
   if (u20.connected)
   {
      if (u20.started) {
         unwinder.reading.status.state = Status_Ready;
         unwinder.reading.status.stopped = false;
      }
      else {
         unwinder.reading.status.state = Status_Ready;
         unwinder.reading.status.stopped = true;
      }
      unwinder.reading.rollDiameter = u20.diameter*25.4;
   }
   else
   {
      unwinder.reading.status.state = Status_NotLoaded;
      unwinder.reading.status.stopped = false;
   }
      
   
   if (lineController.setting.ImperialUnits)
   {
      lineController.reading.displayUnits.Speed_Big = cutter.reading.webSpeed_fpm;
      lineController.reading.displayUnits.Speed_Small = cutter.reading.webSpeed_mm;
      lineController.reading.displayUnits.infeedSpeed_Big = buffer.reading.infeedSpeed_fpm;
      lineController.reading.displayUnits.infeedSpeed_Small = buffer.reading.infeedSpeed_mm;
      lineController.reading.displayUnits.statAvgSpeed = stacker.reading.tripCounter.avgSpeed * 3.28084;
      lineController.reading.displayUnits.statMeters = (LREAL)stacker.reading.tripCounter.meters * 3.28084;
      lineController.reading.displayUnits.statDtotal = dTotal * 3.280899;
   }
   else
   {
      lineController.reading.displayUnits.Speed_Big = cutter.reading.webSpeed_mm;
      lineController.reading.displayUnits.Speed_Small = cutter.reading.webSpeed_fpm;
      lineController.reading.displayUnits.infeedSpeed_Big = buffer.reading.infeedSpeed_mm;
      lineController.reading.displayUnits.infeedSpeed_Small = buffer.reading.infeedSpeed_fpm;
      lineController.reading.displayUnits.statAvgSpeed = stacker.reading.tripCounter.avgSpeed;
      lineController.reading.displayUnits.statMeters = (LREAL)stacker.reading.tripCounter.meters;
      lineController.reading.displayUnits.statDtotal = dTotal;
   }
    
   if (lineController.command.remoteControlled) lineController.reading.guiControlRemoteControlled = SHOW;
   else lineController.reading.guiControlRemoteControlled = HIDE;
   
   if (!lineController.setup.merger.installed && !lineController.setup.folder.installed) lineController.reading.guiControlShowMergerFolder = HIDE;
   else lineController.reading.guiControlShowMergerFolder = SHOW;
   
   if (0 == lineController.setting.buzzerVolume) lineController.setting.lightTowerSegmentSetup.buzzerVolume = 1;
   else if (1 == lineController.setting.buzzerVolume) lineController.setting.lightTowerSegmentSetup.buzzerVolume = 30;
   else if (2 == lineController.setting.buzzerVolume) lineController.setting.lightTowerSegmentSetup.buzzerVolume = 90;
   else lineController.setting.lightTowerSegmentSetup.buzzerVolume = 255;
   
   if (lineController.setup.merger.installed) lineController.reading.m50f50 = 0;
   if (lineController.setup.folder.installed) lineController.reading.m50f50 = 1;
   lineController.reading.guiContolShowFolderInstalled = HIDE;
   lineController.reading.guiContolShowMergerInstalled = HIDE;
   if (machineError.errType == 3002 ) {// eStop folder
      lineController.reading.guiContolShowFolderInstalled = SHOW;
   }
   if (machineError.errType == 4001 ) {// eStop merger
      lineController.reading.guiContolShowFolderInstalled = SHOW;
   }
   
   if (machineError.active && machineError.errType>0){
      curErr = machineError.errType;
      curSub = machineError.errSubType;
   }
   
    
   if (lineController.command.logPress1)
   {
      lineController.command.logPress1 = false;
      lineController.reading.guiControlLog.showDeb = HIDE;
      lineController.reading.guiControlLog.showStreamer = SHOW;
   }
   if (lineController.command.logPress2)
   {
      lineController.command.logPress2 = false;
      lineController.reading.guiControlLog.showDeb = SHOW;
      lineController.reading.guiControlLog.showStreamer = HIDE;
   }
    
    
   if (buffer.reading.input.coverClosed) lineController.reading.guiControlShowCoverOpenBuffer = HIDE;
   else lineController.reading.guiControlShowCoverOpenBuffer = SHOW;
    
   if (folder.reading.coversClosed) lineController.reading.guiControlShowCoverOpenFolder = HIDE;
   else lineController.reading.guiControlShowCoverOpenFolder = SHOW;
   
   if (!machineError.active && wasError) {
      //logIt
      lineController.reading.lastErrors.minutes[11] = lineController.reading.lastErrors.minutes[10];
      lineController.reading.lastErrors.minutes[10] = lineController.reading.lastErrors.minutes[9];
      lineController.reading.lastErrors.minutes[9] = lineController.reading.lastErrors.minutes[8];
      lineController.reading.lastErrors.minutes[8] = lineController.reading.lastErrors.minutes[7];
      lineController.reading.lastErrors.minutes[7] = lineController.reading.lastErrors.minutes[6];
      lineController.reading.lastErrors.minutes[6] = lineController.reading.lastErrors.minutes[5];
      lineController.reading.lastErrors.minutes[5] = lineController.reading.lastErrors.minutes[4];
      lineController.reading.lastErrors.minutes[4] = lineController.reading.lastErrors.minutes[3];
      lineController.reading.lastErrors.minutes[3] = lineController.reading.lastErrors.minutes[2];
      lineController.reading.lastErrors.minutes[2] = lineController.reading.lastErrors.minutes[1];
      lineController.reading.lastErrors.minutes[1] = lineController.reading.lastErrors.minutes[0];
      lineController.reading.lastErrors.sub[11] = lineController.reading.lastErrors.sub[10];
      lineController.reading.lastErrors.sub[10] = lineController.reading.lastErrors.sub[9];
      lineController.reading.lastErrors.sub[9] = lineController.reading.lastErrors.sub[8];
      lineController.reading.lastErrors.sub[8] = lineController.reading.lastErrors.sub[7];
      lineController.reading.lastErrors.sub[7] = lineController.reading.lastErrors.sub[6];
      lineController.reading.lastErrors.sub[6] = lineController.reading.lastErrors.sub[5];
      lineController.reading.lastErrors.sub[5] = lineController.reading.lastErrors.sub[4];
      lineController.reading.lastErrors.sub[4] = lineController.reading.lastErrors.sub[3];
      lineController.reading.lastErrors.sub[3] = lineController.reading.lastErrors.sub[2];
      lineController.reading.lastErrors.sub[2] = lineController.reading.lastErrors.sub[1];
      lineController.reading.lastErrors.sub[1] = lineController.reading.lastErrors.sub[0];
      lineController.reading.lastErrors.error[11] = lineController.reading.lastErrors.error[10];
      lineController.reading.lastErrors.error[10] = lineController.reading.lastErrors.error[9];
      lineController.reading.lastErrors.error[9] = lineController.reading.lastErrors.error[8];
      lineController.reading.lastErrors.error[8] = lineController.reading.lastErrors.error[7];
      lineController.reading.lastErrors.error[7] = lineController.reading.lastErrors.error[6];
      lineController.reading.lastErrors.error[6] = lineController.reading.lastErrors.error[5];
      lineController.reading.lastErrors.error[5] = lineController.reading.lastErrors.error[4];
      lineController.reading.lastErrors.error[4] = lineController.reading.lastErrors.error[3];
      lineController.reading.lastErrors.error[3] = lineController.reading.lastErrors.error[2];
      lineController.reading.lastErrors.error[2] = lineController.reading.lastErrors.error[1];
      lineController.reading.lastErrors.error[1] = lineController.reading.lastErrors.error[0];
      lineController.reading.lastErrors.error[0] = curErr;
      lineController.reading.lastErrors.sub[0] = curSub;
      lineController.reading.lastErrors.minutes[0] = 1;
   }
   wasError = machineError.active;
   
   
   if (secondCnt>1.0) {
      secondCnt -=1.0; 
      lineController.reading.timeSinceStart++;
      if (lineController.reading.timeSinceStart%60 == 0)
      {
         if (lineController.reading.lastErrors.minutes[0]>0) lineController.reading.lastErrors.minutes[0]++;
         if (lineController.reading.lastErrors.minutes[1]>0) lineController.reading.lastErrors.minutes[1]++;
         if (lineController.reading.lastErrors.minutes[2]>0) lineController.reading.lastErrors.minutes[2]++;
         if (lineController.reading.lastErrors.minutes[3]>0) lineController.reading.lastErrors.minutes[3]++;
         if (lineController.reading.lastErrors.minutes[4]>0) lineController.reading.lastErrors.minutes[4]++;
         if (lineController.reading.lastErrors.minutes[5]>0) lineController.reading.lastErrors.minutes[5]++;
         if (lineController.reading.lastErrors.minutes[6]>0) lineController.reading.lastErrors.minutes[6]++;
         if (lineController.reading.lastErrors.minutes[7]>0) lineController.reading.lastErrors.minutes[7]++;
         if (lineController.reading.lastErrors.minutes[8]>0) lineController.reading.lastErrors.minutes[8]++;
         if (lineController.reading.lastErrors.minutes[9]>0) lineController.reading.lastErrors.minutes[9]++;
         if (lineController.reading.lastErrors.minutes[10]>0) lineController.reading.lastErrors.minutes[10]++;
         if (lineController.reading.lastErrors.minutes[11]>0) lineController.reading.lastErrors.minutes[11]++;
      }
   }
   if (seg1<15 || seg2<15 || seg3<15 || seg4<15 || seg5<15) {
      ltRunMode = 5;
      lineController.reading.initDone = 0;
      //segment 1 (top)
      if (lineController.setup.stacker.installed || lineController.setup.streamer.installed)
         seg1 = stacker.reading.startUpSeq;
      else {
         if (cutter.reading.startUpSeq==15)
            seg1 = 15;
         else
            seg1 = 0;
      }
      //segment 2 
      seg2 = cutter.reading.startUpSeq;
      //segment 3 (middle)
      if (lineController.setup.folder.installed)
         seg3 = folder.reading.startUpSeq;
      else if (lineController.setup.merger.installed)
         seg3 = merger.reading.startUpSeq;
      else {
         if (cutter.reading.startUpSeq==15)
            seg3 = 15;
         else
            seg3 = 0;
      }
      //segment 4
      if (lineController.setup.buffer.installed)
         seg4 = buffer.reading.startUpSeq;
      else {
         if (cutter.reading.startUpSeq==15)
            seg4 = 15;
         else
            seg4 = 0;
      }
      //segment 5 (bottom)
      if (lineController.reading.timeSinceStart > safety.setting.initializingTime) {
         if (seg5 != 15) safety.command.eStopReset = 1;
         seg5 = 15;
         
         if (secondCnt<0.107 && lineController.reading.timeSinceStart%2 == 0) {
            if (safety.reading.resetPossible) safety.command.eStopReset = 1;
            if (!cutter.reading.input.eStopExt) beep = 9;
            else beep = 0;
         }
         
      }
      else {
         seg5 = (USINT)(((lineController.reading.timeSinceStart)*14)/safety.setting.initializingTime);
         if (seg5>14) seg5 = 14;
         if (seg1 == 15 && seg2 == 15 && seg3 == 15 && seg4 == 15 && safety.reading.resetPossible) 
         {
            lineController.reading.timeSinceStart++;
         }
      }
      /* Set Add Info to Initializing */
      lineController.reading.cutterStatusAddInfo = 6;
      if (seg1 < 15) lineController.reading.stackerStatusAddInfo = 6;
      else lineController.reading.stackerStatusAddInfo = 0;
      if (seg3 < 15) lineController.reading.mergerStatusAddInfo = 6;
      else lineController.reading.stackerStatusAddInfo = 0;
      if (seg4 < 15) lineController.reading.bufferStatusAddInfo = 6;
      else lineController.reading.stackerStatusAddInfo = 0;

      if (lineController.reading.timeSinceStart < STARTUP_TIME) {
         lineController.reading.timeToStartSeconds = (STARTUP_TIME - lineController.reading.timeSinceStart);
         lineController.reading.timeToStartMinutes = lineController.reading.timeToStartSeconds/60;
         lineController.reading.timeToStartSeconds = lineController.reading.timeToStartSeconds%60;
      }
      else {
         lineController.reading.timeToStartMinutes = 0;
         lineController.reading.timeToStartSeconds = 0;
      }  
      lineController.reading.guiControlShowDuringInit = SHOW;
   }
   else if (!lineController.reading.initDone && seg1>=15 && seg2>=15 && seg3>=15 && seg4>=15 && seg5>=15) {
      lineController.reading.initDone = 1;
      lineController.reading.timeToStartMinutes = 0;
      lineController.reading.timeToStartSeconds = 0;
      lineController.reading.guiControlShowDuringInit = HIDE;
      beep = 9;
   }
   else {
      uwMachineState();
      readMachineStates();
      nodeCheck();
      
      if (lastStopNext != cutter.command.stopNext && cutter.command.stopNext)
      {
         lineController.command.beep = true;
         folder.command.beep = true;
      }
      lastStopNext = cutter.command.stopNext;
      
      lineController.reading.guiControlShowDuringInit = HIDE;
      if (lineController.reading.logOutTime>0 && lineController.reading.timeSinceStart % 60 == 0 && secondCnt<0.107) lineController.reading.logOutTime--;
      
      if (cutter.reading.setSpeed>0 || cutter.reading.webSpeed) lineController.reading.guiControShowWhenStopped = 1;
      else lineController.reading.guiControShowWhenStopped = 0;
      
      
      if (stacker.reading.input.topCoverClosed) lineController.reading.guiControShowWhenStackCoverCl = 0;
      else lineController.reading.guiControShowWhenStackCoverCl = 1;

      if (lineController.setting.loginCode == 1861) {
         lineController.reading.logOutTime = 10;
         lineController.reading.guiControlLevel = 2;
         lineController.setting.loginCode = 2;
      }
      else if (lineController.setting.loginCode == 911) {
         lineController.reading.logOutTime = 10;
         lineController.reading.guiControlLevel = 3;
         lineController.setting.loginCode = 3;
      }
      else if (lineController.setting.loginCode == 1492) {
         lineController.reading.logOutTime = 10;
         lineController.reading.guiControlLevel = 4;
         lineController.setting.loginCode = 4;
      }
      else if (lineController.setting.loginCode == 420) { //reset tot counter
         dTotal = 0;
         lineController.setting.loginCode = 0;
         lineController.reading.guiControlLevel = 1;
      }
      else if (lineController.setting.loginCode == 262524) {
         lineController.reading.logOutTime = 60;
         lineController.reading.guiControlLevel = 5;
         lineController.setting.loginCode = 5;
      }
      else if (lineController.setting.loginCode == 0) {
         lineController.reading.guiControlLevel = 1;
         lineController.setting.loginCode = 0;
      } 
      else {
         if (lastCode == 911) {
            monkeyTime = 70;
         }
         //lineController.reading.guiControlLevel = 1;
      }
      
      if (monkeyTime) {
         monkeyTime--;
         if (lastStackerCover != stacker.reading.input.topCoverClosed) lineController.reading.guiControShowMonkey = 0;    
      }
      else lineController.reading.guiControShowMonkey = 1;    
      
      lastCode = lineController.reading.guiControlLevel;   
      lastStackerCover = stacker.reading.input.topCoverClosed;
      
      if (0 == lineController.reading.logOutTime && 1 != lineController.reading.guiControlLevel) {
         lineController.reading.guiControlLevel = 1;
         lineController.setting.loginCode = 0;
      }
      
      
      if (lineController.setup.cutter.secondKnife)
         lineController.reading.guiControlHideC51 = 0;
      else 
         lineController.reading.guiControlHideC51 = 1;
      

      switch (lineController.reading.guiControlLevel)
      {
         case 2:
            lineController.reading.guiControlLevel2Show = SHOW;
            lineController.reading.guiControlLevel3Show = HIDE;
            lineController.reading.guiControlLevel4Show = HIDE;
            lineController.reading.guiControlLevel5Show = HIDE;
            lineController.reading.guiControlLevel4Hide = 0;
            lineController.reading.guiControlLevel2Unlock = UNLOCK;
            lineController.reading.guiControlLevel3Unlock = LOCK;
            lineController.reading.guiControlLevel4Unlock = LOCK;
            lineController.reading.guiControlLevel5Unlock = LOCK;
            break;
         case 3:
            lineController.reading.guiControlLevel2Show = SHOW;
            lineController.reading.guiControlLevel3Show = SHOW;
            lineController.reading.guiControlLevel4Show = HIDE;
            lineController.reading.guiControlLevel5Show = HIDE;
            lineController.reading.guiControlLevel4Hide = 0;
            lineController.reading.guiControlLevel2Unlock = UNLOCK;
            lineController.reading.guiControlLevel3Unlock = UNLOCK;
            lineController.reading.guiControlLevel4Unlock = LOCK;
            lineController.reading.guiControlLevel5Unlock = LOCK;
            break;
         case 4:
            lineController.reading.guiControlLevel2Show = SHOW;
            lineController.reading.guiControlLevel3Show = SHOW;
            lineController.reading.guiControlLevel4Show = SHOW;
            lineController.reading.guiControlLevel5Show = HIDE;
            lineController.reading.guiControlLevel4Hide = HIDE;
            lineController.reading.guiControlLevel2Unlock = UNLOCK;
            lineController.reading.guiControlLevel3Unlock = UNLOCK;
            lineController.reading.guiControlLevel4Unlock = UNLOCK;
            lineController.reading.guiControlLevel5Unlock = LOCK;
            break;
         case 5:
            lineController.reading.guiControlLevel2Show = SHOW;
            lineController.reading.guiControlLevel3Show = SHOW;
            lineController.reading.guiControlLevel4Show = SHOW;
            lineController.reading.guiControlLevel5Show = SHOW;
            lineController.reading.guiControlLevel4Hide = 1;
            lineController.reading.guiControlLevel2Unlock = UNLOCK;
            lineController.reading.guiControlLevel3Unlock = UNLOCK;
            lineController.reading.guiControlLevel4Unlock = UNLOCK;
            lineController.reading.guiControlLevel5Unlock = UNLOCK;
            break;
         default:
            lineController.reading.guiControlLevel2Show = HIDE;
            lineController.reading.guiControlLevel3Show = HIDE;
            lineController.reading.guiControlLevel4Show = HIDE;
            lineController.reading.guiControlLevel5Show = HIDE;
            lineController.reading.guiControlLevel2Unlock = LOCK;
            lineController.reading.guiControlLevel3Unlock = LOCK;
            lineController.reading.guiControlLevel4Unlock = LOCK;
            lineController.reading.guiControlLevel5Unlock = LOCK;
            break;
      }
   }
   
   if (lineController.setup.unwinder.installed)
   {
      lineController.reading.guiControlUnwinder.Level1Show = SHOW;
      lineController.reading.guiControlUnwinder.Level2Show = lineController.reading.guiControlLevel2Show;
      lineController.reading.guiControlUnwinder.Level3Show = lineController.reading.guiControlLevel3Show;
      lineController.reading.guiControlUnwinder.Level4Show = lineController.reading.guiControlLevel4Show;
      lineController.reading.guiControlUnwinder.Level5Show = lineController.reading.guiControlLevel5Show;
      lineController.reading.guiControlUnwinder.Level2Unlock = lineController.reading.guiControlLevel2Unlock;
      lineController.reading.guiControlUnwinder.Level3Unlock = lineController.reading.guiControlLevel3Unlock;
      lineController.reading.guiControlUnwinder.Level4Unlock = lineController.reading.guiControlLevel4Unlock;
      lineController.reading.guiControlUnwinder.Level5Unlock = lineController.reading.guiControlLevel5Unlock;
   }
   else
   {
      lineController.reading.guiControlUnwinder.Level1Show = HIDE;
      lineController.reading.guiControlUnwinder.Level2Show = HIDE;
      lineController.reading.guiControlUnwinder.Level3Show = HIDE;
      lineController.reading.guiControlUnwinder.Level4Show = HIDE;
      lineController.reading.guiControlUnwinder.Level5Show = HIDE;
      lineController.reading.guiControlUnwinder.Level2Unlock = LOCK;
      lineController.reading.guiControlUnwinder.Level3Unlock = LOCK;
      lineController.reading.guiControlUnwinder.Level4Unlock = LOCK;
      lineController.reading.guiControlUnwinder.Level5Unlock = LOCK;
   }
   
   if (lineController.setup.stacker.installed)
   {
      lineController.reading.guiControlPostCut.stacker.Level2Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level2Show = lineController.reading.guiControlLevel2Show;
      lineController.reading.guiControlPostCut.stacker.Level3Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level3Show = lineController.reading.guiControlLevel3Show;
      lineController.reading.guiControlPostCut.stacker.Level4Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level4Show = lineController.reading.guiControlLevel4Show;
      lineController.reading.guiControlPostCut.stacker.Level5Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level5Show = lineController.reading.guiControlLevel5Show;
      lineController.reading.guiControlPostCut.stacker.Level2Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level2Unlock = lineController.reading.guiControlLevel2Unlock;
      lineController.reading.guiControlPostCut.stacker.Level3Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level3Unlock = lineController.reading.guiControlLevel3Unlock;
      lineController.reading.guiControlPostCut.stacker.Level4Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level4Unlock = lineController.reading.guiControlLevel4Unlock;
      lineController.reading.guiControlPostCut.stacker.Level5Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level5Unlock = lineController.reading.guiControlLevel5Unlock;
      lineController.reading.guiControlPostCut.streamer.Level2Show = HIDE;
      lineController.reading.guiControlPostCut.streamer.Level3Show = HIDE;
      lineController.reading.guiControlPostCut.streamer.Level4Show = HIDE;
      lineController.reading.guiControlPostCut.streamer.Level5Show = HIDE;
      lineController.reading.guiControlPostCut.streamer.Level2Unlock = LOCK;
      lineController.reading.guiControlPostCut.streamer.Level3Unlock = LOCK;
      lineController.reading.guiControlPostCut.streamer.Level4Unlock = LOCK;
      lineController.reading.guiControlPostCut.streamer.Level5Unlock = LOCK;
      lineController.reading.guiControlPostCut.streamer.Level1Show = HIDE;
      lineController.reading.guiControlPostCut.stacker.Level1Show = SHOW;
   }
   else
   {
      lineController.reading.guiControlPostCut.streamer.Level1Show = SHOW;
      lineController.reading.guiControlPostCut.stacker.Level1Show = HIDE;
      lineController.reading.guiControlPostCut.streamer.Level2Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level2Show = lineController.reading.guiControlLevel2Show;
      lineController.reading.guiControlPostCut.streamer.Level3Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level3Show = lineController.reading.guiControlLevel3Show;
      lineController.reading.guiControlPostCut.streamer.Level4Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level4Show = lineController.reading.guiControlLevel4Show;
      lineController.reading.guiControlPostCut.streamer.Level5Show = lineController.reading.guiControlPostCut.stackerAndStreamer.Level5Show = lineController.reading.guiControlLevel5Show;
      lineController.reading.guiControlPostCut.streamer.Level2Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level2Unlock = lineController.reading.guiControlLevel2Unlock;
      lineController.reading.guiControlPostCut.streamer.Level3Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level3Unlock = lineController.reading.guiControlLevel3Unlock;
      lineController.reading.guiControlPostCut.streamer.Level4Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level4Unlock = lineController.reading.guiControlLevel4Unlock;
      lineController.reading.guiControlPostCut.streamer.Level5Unlock = lineController.reading.guiControlPostCut.stackerAndStreamer.Level5Unlock = lineController.reading.guiControlLevel5Unlock;
      lineController.reading.guiControlPostCut.stacker.Level2Show = HIDE;
      lineController.reading.guiControlPostCut.stacker.Level3Show = HIDE;
      lineController.reading.guiControlPostCut.stacker.Level4Show = HIDE;
      lineController.reading.guiControlPostCut.stacker.Level5Show = HIDE;
      lineController.reading.guiControlPostCut.stacker.Level2Unlock = LOCK;
      lineController.reading.guiControlPostCut.stacker.Level3Unlock = LOCK;
      lineController.reading.guiControlPostCut.stacker.Level4Unlock = LOCK;
      lineController.reading.guiControlPostCut.stacker.Level5Unlock = LOCK;
   }
   if (lineController.reading.guiControlPostCut.stMenu3Tab == 2)
   {
      lineController.reading.guiControlPostCut.showAdvanced = lineController.reading.guiControlPostCut.streamer.Level3Show;
      lineController.reading.guiControlPostCut.showGlue = lineController.reading.guiControlPostCut.showOffset = HIDE;
   }
   if (lineController.reading.guiControlPostCut.stMenu3Tab == 0)
   {
      lineController.reading.guiControlPostCut.showGlue = lineController.reading.guiControlPostCut.streamer.Level3Show;
      lineController.reading.guiControlPostCut.showAdvanced = lineController.reading.guiControlPostCut.showOffset = HIDE;
   }
   if (lineController.reading.guiControlPostCut.stMenu3Tab == 1)
   {
      lineController.reading.guiControlPostCut.showOffset = lineController.reading.guiControlPostCut.streamer.Level3Show;
      lineController.reading.guiControlPostCut.showAdvanced = lineController.reading.guiControlPostCut.showGlue = HIDE;
   }
   
   
   
   lineController.reading.guiControlShowMerger = lineController.reading.guiControlShowFolder = HIDE;
   lineController.reading.guiControlShowNoFolder = lineController.reading.guiControlShowNoMerger = SHOW;
   if (lineController.setup.merger.installed) {
      lineController.reading.guiControlShowMerger = SHOW;
      lineController.reading.guiControlShowNoMerger = HIDE;
   }
   if (lineController.setup.folder.installed) {
      lineController.reading.guiControlShowFolder = SHOW;
      lineController.reading.guiControlShowNoFolder = HIDE;
   }
   
   userInputResult.all = userInput();
   if (userLimit < lineController.reading.maximumSpeed) lineController.reading.maximumSpeed = userLimit;
   lightTower();
   controlMedSlowSpeed();
  
   pif.machine.currentSpeed = (cutter.reading.webSpeed*1000);
	/*if (stacker.reading.error || buffer.reading.error || cutter.reading.error || folder.reading.error)
	{
		pif.machine.ready = 0;
		pif.machine.speedLimit = 0;
	}
	*/	
   if (stacker.reading.status.state == Status_Error
      || buffer.reading.status.state == Status_Error 
      || cutter.reading.status.state == Status_Error 
      || merger.reading.status.state == Status_Error)
   {
      pif.machine.ready = 0;
      pif.machine.speedLimit = 0;
   }
      //else if ((cutter.reading.output.reject || stacker.reading.ready)&& buffer.reading.ready && cutter.reading.ready)
   else if ((cutter.reading.output.reject || (stacker.reading.status.state == Status_Ready && !stacker.reading.status.stopped))
      && (buffer.reading.status.state == Status_Ready && !buffer.reading.status.stopped)
      && (cutter.reading.status.state == Status_Ready && !cutter.reading.status.stopped))
   {
      //pif.machine.ready = 1;
      //pif.machine.pause = 0;
      //pif.machine.slowRequest = 0;
      pif.machine.speedLimit = 0;
      if (pif.machine.ready && !pif.machine.pause) {
         pif.machine.speedLimit = 3000;
         if (buffer.setting.slowLevel)
            pif.machine.speedLimit = 1000;
      }
      
      //set
      if (buffer.reading.accumulated>(buffer.setting.stopLevel*buffer.reading.maxAccumulated/100))
         pif.machine.ready = 0;
      if (buffer.reading.accumulated>(buffer.setting.pauseLevel*buffer.reading.maxAccumulated/100))
         pif.machine.pause = 1;
      if (buffer.reading.accumulated>(buffer.setting.slowLevel*buffer.reading.maxAccumulated/100))
         pif.machine.slowRequest = 1;	
      //reset
      if (0==pif.machine.ready && (buffer.reading.accumulated)<((buffer.setting.stopLevel*(buffer.reading.maxAccumulated - 1.4))/100))
         pif.machine.ready = 1;
      if (pif.machine.slowRequest && (buffer.reading.accumulated) < ((buffer.setting.slowLevel*(buffer.reading.maxAccumulated - 1.4))/100))
         pif.machine.slowRequest = 0;
      if (pif.machine.pause && (buffer.reading.accumulated)< ((buffer.setting.pauseLevel*(buffer.reading.maxAccumulated - 1.4))/100))
         pif.machine.pause = 0;
   }
   else  {
      pif.machine.pause = 0;
      pif.machine.slowRequest = 0;
      pif.machine.ready = 0;
   }
   
   if (cutter.setting.slowSpeedLoad && buffer.reading.infeedSpeed > 0.1) {
      //if (/*cutter.reading.processingWhite || */(cutter.reading.output.reject && (cutter.reading.setSpeed>0 || cutter.reading.webSpeed>10)))
      if (cutter.reading.processingWhite || (cutter.reading.output.reject && (cutter.reading.setSpeed>0 || cutter.reading.webSpeed>10)))
         pif.machine.slowRequest = 1;    
   }
   /*if (lineController.setup.streamer.installed && streamer.setting.overMinStackSlowDown)
   {
      if (stacker.reading.stackUsage > 100.0)
      {
          pif.machine.slowRequest = 1; 
      }
   }*/
	
   if (userInputResult.bits.pause) pif.machine.pause = 1;
   if (userInputResult.bits.slow)  pif.machine.slowRequest = 1;
   if (userInputResult.bits.stop)  pif.machine.ready = 0;
   
   if (buffer.reading.fillBufferByReady) {
      pif.machine.slowRequest = 1;
      pif.machine.ready = 1;
   }
   
   if (lineController.setting.onlySoftStop)
   {
      if (lineController.setting.invertReady)
         lineController.reading.ready    = 1;
      else
         lineController.reading.ready    = 0;
      lineController.reading.pause = pif.machine.ready;
   }
   else
   {
      if (lineController.setting.invertReady)
         lineController.reading.ready    = pif.machine.ready;
      else
         lineController.reading.ready    = !pif.machine.ready;
      lineController.reading.pause    = pif.machine.pause;
   }
   lineController.reading.slowDown = pif.machine.slowRequest;
   
}

void _EXIT ProgramExit(void)
{

}

void lightTower(void)
{
   static USINT tick;
   static USINT beepCnt;
   static USINT doubleBeepState;
   static USINT trippleBeepState;
   static USINT errorCnt;
   static USINT animateSegementSpeed;
   tick ++;
   processData0 = 0;
   processData1 = 0;
   processData2 = 0;
   if (beepCnt) beepCnt--;
   if (lineController.command.beep) {
      beepCnt=5;
      lineController.command.beep = false;
   }
    
   if (lineController.command.shortBeep) {
      beepCnt=1;
      lineController.command.shortBeep = false;
   }
   
   if (lineController.command.doubleBeep) {
      beepCnt=2;
      doubleBeepState = 2;
      lineController.command.doubleBeep = false;
   }

   if (doubleBeepState && !beepCnt)
   {
      doubleBeepState--;
      if (!doubleBeepState) lineController.command.shortBeep = true;
   }
    
   if (lineController.command.trippleBeep) {
      beepCnt=1;
      trippleBeepState = 5;
      lineController.command.trippleBeep = false;
   }
    
   if (trippleBeepState && !beepCnt)
   {
      trippleBeepState--;
      if (trippleBeepState%2 == 1) lineController.command.shortBeep = true;
   }
	
   if (lineController.command.remoteControlled) ltRunMode = 11;
   
	
   if (ltRunMode == 0)
   {
      processData3 = 1;
      processData4 = 5;
      processData5 = 0;
      processData6 = 1;
      processData5 = 0;
      if (tick%8 < 4)
      {
         if      (segmentColor[0] == LIGHT_OFF)   processData0 = processData0  | COLOR_OFF_MASK_1_3_5; 
         else if (segmentColor[0] == LIGHT_GREEN) processData0 = processData0  | COLOR_GREEN_MASK_1_3_5; 
         else if (segmentColor[0] == LIGHT_RED) processData0 = processData0    | COLOR_RED_MASK_1_3_5; 
         else if (segmentColor[0] == LIGHT_YELLOW) processData0 = processData0 | COLOR_YELLOW_MASK_1_3_5; 
         else if (segmentColor[0] == LIGHT_BLUE) processData0 = processData0   | COLOR_BLUE_MASK_1_3_5; 
         else if (segmentColor[0] == LIGHT_ORANGE) processData0 = processData0 | COLOR_ORANGE_MASK_1_3_5; 
         else if (segmentColor[0] == LIGHT_USER) processData0 = processData0   | COLOR_USER_MASK_1_3_5; 
         else if (segmentColor[0] == LIGHT_WHITE) processData0 = processData0  | COLOR_WHITE_MASK_1_3_5; 
      }
      else 
      {
         if        (segmentBkgColor[0] == LIGHT_FORWARD)   processData0 = processData0; 
         else if   (segmentBkgColor[0] == LIGHT_OFF)   processData0 = processData0  | COLOR_OFF_MASK_1_3_5; 
         else if   (segmentBkgColor[0] == LIGHT_GREEN) processData0 = processData0  | COLOR_GREEN_MASK_1_3_5; 
         else if   (segmentBkgColor[0] == LIGHT_RED) processData0 = processData0    | COLOR_RED_MASK_1_3_5; 
         else if   (segmentBkgColor[0] == LIGHT_YELLOW) processData0 = processData0 | COLOR_YELLOW_MASK_1_3_5; 
         else if   (segmentBkgColor[0] == LIGHT_BLUE) processData0 = processData0   | COLOR_BLUE_MASK_1_3_5; 
         else if   (segmentBkgColor[0] == LIGHT_ORANGE) processData0 = processData0 | COLOR_ORANGE_MASK_1_3_5; 
         else if   (segmentBkgColor[0] == LIGHT_USER) processData0 = processData0   | COLOR_USER_MASK_1_3_5; 
         else if   (segmentBkgColor[0] == LIGHT_WHITE) processData0 = processData0  | COLOR_WHITE_MASK_1_3_5; 
      }
	
      if (tick%8 < 4)
      {
         if      (segmentColor[1] == LIGHT_OFF)   processData0 = processData0  | COLOR_OFF_MASK_2_4; 
         else if (segmentColor[1] == LIGHT_GREEN) processData0 = processData0  | COLOR_GREEN_MASK_2_4; 
         else if (segmentColor[1] == LIGHT_RED) processData0 = processData0    | COLOR_RED_MASK_2_4; 
         else if (segmentColor[1] == LIGHT_YELLOW) processData0 = processData0 | COLOR_YELLOW_MASK_2_4; 
         else if (segmentColor[1] == LIGHT_BLUE) processData0 = processData0   | COLOR_BLUE_MASK_2_4; 
         else if (segmentColor[1] == LIGHT_ORANGE) processData0 = processData0 | COLOR_ORANGE_MASK_2_4; 
         else if (segmentColor[1] == LIGHT_USER) processData0 = processData0   | COLOR_USER_MASK_2_4; 
         else if (segmentColor[1] == LIGHT_WHITE) processData0 = processData0  | COLOR_WHITE_MASK_2_4; 
      }
      else 
      {
         if        (segmentBkgColor[1] == LIGHT_FORWARD)   processData0 = processData0; 
         else if   (segmentBkgColor[1] == LIGHT_OFF)   processData0 = processData0  | COLOR_OFF_MASK_2_4; 
         else if   (segmentBkgColor[1] == LIGHT_GREEN) processData0 = processData0  | COLOR_GREEN_MASK_2_4; 
         else if   (segmentBkgColor[1] == LIGHT_RED) processData0 = processData0    | COLOR_RED_MASK_2_4; 
         else if   (segmentBkgColor[1] == LIGHT_YELLOW) processData0 = processData0 | COLOR_YELLOW_MASK_2_4; 
         else if   (segmentBkgColor[1] == LIGHT_BLUE) processData0 = processData0   | COLOR_BLUE_MASK_2_4; 
         else if   (segmentBkgColor[1] == LIGHT_ORANGE) processData0 = processData0 | COLOR_ORANGE_MASK_2_4; 
         else if   (segmentBkgColor[1] == LIGHT_USER) processData0 = processData0   | COLOR_USER_MASK_2_4; 
         else if   (segmentBkgColor[1] == LIGHT_WHITE) processData0 = processData0  | COLOR_WHITE_MASK_2_4; 
      }

      if (tick%8 < 4)
      {
         if      (segmentColor[2] == LIGHT_OFF)   processData1 = processData1  | COLOR_OFF_MASK_1_3_5; 
         else if (segmentColor[2] == LIGHT_GREEN) processData1 = processData1  | COLOR_GREEN_MASK_1_3_5; 
         else if (segmentColor[2] == LIGHT_RED) processData1 = processData1    | COLOR_RED_MASK_1_3_5; 
         else if (segmentColor[2] == LIGHT_YELLOW) processData1 = processData1 | COLOR_YELLOW_MASK_1_3_5; 
         else if (segmentColor[2] == LIGHT_BLUE) processData1 = processData1   | COLOR_BLUE_MASK_1_3_5; 
         else if (segmentColor[2] == LIGHT_ORANGE) processData1 = processData1 | COLOR_ORANGE_MASK_1_3_5; 
         else if (segmentColor[2] == LIGHT_USER) processData1 = processData1   | COLOR_USER_MASK_1_3_5; 
         else if (segmentColor[2] == LIGHT_WHITE) processData1 = processData1  | COLOR_WHITE_MASK_1_3_5; 
      }
      else 
      {
         if        (segmentBkgColor[2] == LIGHT_FORWARD)   processData1 = processData1; 
         else if   (segmentBkgColor[2] == LIGHT_OFF)   processData1 = processData1  | COLOR_OFF_MASK_1_3_5; 
         else if   (segmentBkgColor[2] == LIGHT_GREEN) processData1 = processData1  | COLOR_GREEN_MASK_1_3_5; 
         else if   (segmentBkgColor[2] == LIGHT_RED) processData1 = processData1    | COLOR_RED_MASK_1_3_5; 
         else if   (segmentBkgColor[2] == LIGHT_YELLOW) processData1 = processData1 | COLOR_YELLOW_MASK_1_3_5; 
         else if   (segmentBkgColor[2] == LIGHT_BLUE) processData1 = processData1   | COLOR_BLUE_MASK_1_3_5; 
         else if   (segmentBkgColor[2] == LIGHT_ORANGE) processData1 = processData1 | COLOR_ORANGE_MASK_1_3_5; 
         else if   (segmentBkgColor[2] == LIGHT_USER) processData1 = processData1   | COLOR_USER_MASK_1_3_5; 
         else if   (segmentBkgColor[2] == LIGHT_WHITE) processData1 = processData1  | COLOR_WHITE_MASK_1_3_5; 
      }
	
      if (tick%8 < 4)
      {
         if      (segmentColor[3] == LIGHT_OFF)   processData1 = processData1  | COLOR_OFF_MASK_2_4; 
         else if (segmentColor[3] == LIGHT_GREEN) processData1 = processData1  | COLOR_GREEN_MASK_2_4; 
         else if (segmentColor[3] == LIGHT_RED) processData1 = processData1    | COLOR_RED_MASK_2_4; 
         else if (segmentColor[3] == LIGHT_YELLOW) processData1 = processData1 | COLOR_YELLOW_MASK_2_4; 
         else if (segmentColor[3] == LIGHT_BLUE) processData1 = processData1   | COLOR_BLUE_MASK_2_4; 
         else if (segmentColor[3] == LIGHT_ORANGE) processData1 = processData1 | COLOR_ORANGE_MASK_2_4; 
         else if (segmentColor[3] == LIGHT_USER) processData1 = processData1   | COLOR_USER_MASK_2_4; 
         else if (segmentColor[3] == LIGHT_WHITE) processData1 = processData1  | COLOR_WHITE_MASK_2_4; 
      }
      else 
      {
         if        (segmentBkgColor[3] == LIGHT_FORWARD)   processData1 = processData1; 
         else if   (segmentBkgColor[3] == LIGHT_OFF)   processData1 = processData1  | COLOR_OFF_MASK_2_4; 
         else if   (segmentBkgColor[3] == LIGHT_GREEN) processData1 = processData1  | COLOR_GREEN_MASK_2_4; 
         else if   (segmentBkgColor[3] == LIGHT_RED) processData1 = processData1    | COLOR_RED_MASK_2_4; 
         else if   (segmentBkgColor[3] == LIGHT_YELLOW) processData1 = processData1 | COLOR_YELLOW_MASK_2_4; 
         else if   (segmentBkgColor[3] == LIGHT_BLUE) processData1 = processData1   | COLOR_BLUE_MASK_2_4; 
         else if   (segmentBkgColor[3] == LIGHT_ORANGE) processData1 = processData1 | COLOR_ORANGE_MASK_2_4; 
         else if   (segmentBkgColor[3] == LIGHT_USER) processData1 = processData1   | COLOR_USER_MASK_2_4; 
         else if   (segmentBkgColor[3] == LIGHT_WHITE) processData1 = processData1  | COLOR_WHITE_MASK_2_4; 
      }
	
      if (tick%8 < 4)
      {
         if      (segmentColor[4] == LIGHT_OFF)   processData2 = processData2  | COLOR_OFF_MASK_1_3_5; 
         else if (segmentColor[4] == LIGHT_GREEN) processData2 = processData2  | COLOR_GREEN_MASK_1_3_5; 
         else if (segmentColor[4] == LIGHT_RED) processData2 = processData2    | COLOR_RED_MASK_1_3_5; 
         else if (segmentColor[4] == LIGHT_YELLOW) processData2 = processData2 | COLOR_YELLOW_MASK_1_3_5; 
         else if (segmentColor[4] == LIGHT_BLUE) processData2 = processData2   | COLOR_BLUE_MASK_1_3_5; 
         else if (segmentColor[4] == LIGHT_ORANGE) processData2 = processData2 | COLOR_ORANGE_MASK_1_3_5; 
         else if (segmentColor[4] == LIGHT_USER) processData2 = processData2   | COLOR_USER_MASK_1_3_5; 
         else if (segmentColor[4] == LIGHT_WHITE) processData2 = processData2  | COLOR_WHITE_MASK_1_3_5; 
      }
      else 
      {
         if        (segmentBkgColor[4] == LIGHT_FORWARD)   processData2 = processData2; 
         else if   (segmentBkgColor[4] == LIGHT_OFF)   processData2 = processData2  | COLOR_OFF_MASK_1_3_5; 
         else if   (segmentBkgColor[4] == LIGHT_GREEN) processData2 = processData2  | COLOR_GREEN_MASK_1_3_5; 
         else if   (segmentBkgColor[4] == LIGHT_RED) processData2 = processData2    | COLOR_RED_MASK_1_3_5; 
         else if   (segmentBkgColor[4] == LIGHT_YELLOW) processData2 = processData2 | COLOR_YELLOW_MASK_1_3_5; 
         else if   (segmentBkgColor[4] == LIGHT_BLUE) processData2 = processData2   | COLOR_BLUE_MASK_1_3_5; 
         else if   (segmentBkgColor[4] == LIGHT_ORANGE) processData2 = processData2 | COLOR_ORANGE_MASK_1_3_5; 
         else if   (segmentBkgColor[4] == LIGHT_USER) processData2 = processData2   | COLOR_USER_MASK_1_3_5; 
         else if   (segmentBkgColor[4] == LIGHT_WHITE) processData2 = processData2  | COLOR_WHITE_MASK_1_3_5; 
      }
		
			
      lineController.reading.segmentColor1 = _OFF;
      if ((processData0 & 0x0f) & COLOR_GREEN_MASK_1_3_5) lineController.reading.segmentColor1 = _GREEN;
      if ((processData0 & 0x0f) & COLOR_RED_MASK_1_3_5) lineController.reading.segmentColor1 = _RED;
      if ((processData0 & 0x0f) & COLOR_YELLOW_MASK_1_3_5) lineController.reading.segmentColor1 = _YELLOW;
      if ((processData0 & 0x0f) & COLOR_BLUE_MASK_1_3_5) lineController.reading.segmentColor1 = _BLUE;
      if ((processData0 & 0x0f) & COLOR_ORANGE_MASK_1_3_5) lineController.reading.segmentColor1 = _ORANGE;
      if ((processData0 & 0x0f) & COLOR_USER_MASK_1_3_5) lineController.reading.segmentColor1 = _TURQUOISE;
      if ((processData0 & 0x0f) & COLOR_WHITE_MASK_1_3_5) lineController.reading.segmentColor1 = _WHITE;
		
      lineController.reading.segmentColor2 = _OFF;
      if ((processData0 & 0xf0) & COLOR_GREEN_MASK_2_4) lineController.reading.segmentColor2 = _GREEN;
      if ((processData0 & 0xf0) & COLOR_RED_MASK_2_4) lineController.reading.segmentColor2 = _RED;
      if ((processData0 & 0xf0) & COLOR_YELLOW_MASK_2_4) lineController.reading.segmentColor2 = _YELLOW;
      if ((processData0 & 0xf0) & COLOR_BLUE_MASK_2_4) lineController.reading.segmentColor2 = _BLUE;
      if ((processData0 & 0xf0) & COLOR_ORANGE_MASK_2_4) lineController.reading.segmentColor2 = _ORANGE;
      if ((processData0 & 0xf0) & COLOR_USER_MASK_2_4) lineController.reading.segmentColor2 = _TURQUOISE;
      if ((processData0 & 0xf0) & COLOR_WHITE_MASK_2_4) lineController.reading.segmentColor2 = _WHITE;
		
      lineController.reading.segmentColor3 = _OFF;
      if ((processData1 & 0x0f) & COLOR_GREEN_MASK_1_3_5) lineController.reading.segmentColor3 = _GREEN;
      if ((processData1 & 0x0f) & COLOR_RED_MASK_1_3_5) lineController.reading.segmentColor3 = _RED;
      if ((processData1 & 0x0f) & COLOR_YELLOW_MASK_1_3_5) lineController.reading.segmentColor3 = _YELLOW;
      if ((processData1 & 0x0f) & COLOR_BLUE_MASK_1_3_5) lineController.reading.segmentColor3 = _BLUE;
      if ((processData1 & 0x0f) & COLOR_ORANGE_MASK_1_3_5) lineController.reading.segmentColor3 = _ORANGE;
      if ((processData1 & 0x0f) & COLOR_USER_MASK_1_3_5) lineController.reading.segmentColor3 = _TURQUOISE;
      if ((processData1 & 0x0f) & COLOR_WHITE_MASK_1_3_5) lineController.reading.segmentColor3 = _WHITE;
		
      lineController.reading.segmentColor4 = _OFF;
      if ((processData1 & 0xf0) & COLOR_GREEN_MASK_2_4) lineController.reading.segmentColor4 = _GREEN;
      if ((processData1 & 0xf0) & COLOR_RED_MASK_2_4) lineController.reading.segmentColor4 = _RED;
      if ((processData1 & 0xf0) & COLOR_YELLOW_MASK_2_4) lineController.reading.segmentColor4 = _YELLOW;
      if ((processData1 & 0xf0) & COLOR_BLUE_MASK_2_4) lineController.reading.segmentColor4 = _BLUE;
      if ((processData1 & 0xf0) & COLOR_ORANGE_MASK_2_4) lineController.reading.segmentColor4 = _ORANGE;
      if ((processData1 & 0xf0) & COLOR_USER_MASK_2_4) lineController.reading.segmentColor4 = _TURQUOISE;
      if ((processData1 & 0xf0) & COLOR_WHITE_MASK_2_4) lineController.reading.segmentColor4 = _WHITE;
		
      lineController.reading.segmentColor5 = _OFF;
      if ((processData2 & 0x0f) & COLOR_GREEN_MASK_1_3_5) lineController.reading.segmentColor5 = _GREEN;
      if ((processData2 & 0x0f) & COLOR_RED_MASK_1_3_5) lineController.reading.segmentColor5 = _RED;
      if ((processData2 & 0x0f) & COLOR_YELLOW_MASK_1_3_5) lineController.reading.segmentColor5 = _YELLOW;
      if ((processData2 & 0x0f) & COLOR_BLUE_MASK_1_3_5) lineController.reading.segmentColor5 = _BLUE;
      if ((processData2 & 0x0f) & COLOR_ORANGE_MASK_1_3_5) lineController.reading.segmentColor5 = _ORANGE;
      if ((processData2 & 0x0f) & COLOR_USER_MASK_1_3_5) lineController.reading.segmentColor5 = _TURQUOISE;
      if ((processData2 & 0x0f) & COLOR_WHITE_MASK_1_3_5) lineController.reading.segmentColor5 = _WHITE;
		
      if (beepCnt)
      {
         processData2 = processData2 | 0x80;
      }
		
      processData7 = lineController.setting.lightTowerSegmentSetup.buzzerVolume;
   }
   if (ltRunMode == 2) //level mode (running)
   {
		
      if (u20.diameter < 9 && lineController.setup.unwinder.installed)
      {
         if (lineController.reading.timeSinceStart % 2 == 0)
         {
            processData0 = 85;
            processData1 = 85;
            if (beepCnt)
               processData2 = 133;
            else
               processData2 = 5;
            processData3 = 2;
            processData4 = 0;
            processData5 = 0; 
         }
      }
      else
      {   
         processData0 = 17;
         processData1 = 17;
         if (beepCnt)
            processData2 = 129;
         else
            processData2 = 1;
         processData3 = 2;
         processData4 = 0;
         processData5 = 0;
      }
      
      
      
      
      if (animateSegementSpeed > (segmentSpeed+8)) animateSegementSpeed -=8;
      else animateSegementSpeed = segmentSpeed;
      processData6 = animateSegementSpeed;
      
      if (((REAL)lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10)>255)
         processData7 = 255;
      else
         processData7 = (USINT)(lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10);
   }
   if (ltRunMode == 6) //level mode (runningW) //yellow
   {
      processData0 = 51;
      processData1 = 51;
      if (lineController.setting.lightTowerSegmentSetup.beepOnWarning)
      {
         processData2 = 147;
      }
      else
      {
         if (beepCnt)
            processData2 = 131;
         else
            processData2 = 3;
      }
      processData3 = 2;
      processData4 = 0;
      processData5 = 0;
      processData6 = segmentSpeed;
      if (((REAL)lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10)>255)
         processData7 = 255;
      else
         processData7 = (USINT)(lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10);
		
      if (lineController.command.flash && tick%4 < 2)
      {
         processData0 = 0;
         processData1 = 0;
         if (lineController.setting.lightTowerSegmentSetup.beepOnWarning)
         {
            processData2 = 144;
         }
         else
         {
            if (beepCnt)
               processData2 = 128;
            else
               processData2 = 0;
         }
      }
   }
   if (ltRunMode == 7) //level mode (running) //orange
   {
      processData0 = 85;
      processData1 = 85;
      if (beepCnt)
         processData2 = 133;
      else
         processData2 = 5;
      processData3 = 2;
      processData4 = 0;
      processData5 = 0;
      processData6 = segmentSpeed;
      if (((REAL)lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10)>255)
         processData7 = 255;
      else
         processData7 = (USINT)(lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10);
		
   }
   if (ltRunMode == 8) //level mode upside down (buffer level) //user color
   {
      processData0 = 102;
      processData1 = 102;
      if (beepCnt)
         processData2 = 134;
      else
         processData2 = 6;
      processData3 = 2;
      processData4 = 1;
      processData5 = 0;
      processData6 = segmentSpeed;
      if (((REAL)lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?50:0) + cutter.reading.webSpeed*10)>255)
         processData7 = 255;
      else
         processData7 = (USINT)(lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?50:0) + cutter.reading.webSpeed*10);
		
   }
   
   if (ltRunMode == 10) //user input control
   {     
      if (userIcolor == 0)
      {
         processData0 = 34;
         processData1 = 34;
         if (beepCnt)
            processData2 = 130;
         else
            processData2 = 2;
      }
      
      if (userIcolor == 1)
      {
         processData0 = 17;
         processData1 = 17;
         if (beepCnt)
            processData2 = 129;
         else
            processData2 = 1;
      }
      if (userIcolor == 2)
      {
         processData0 = 85;
         processData1 = 85;
         if (beepCnt)
            processData2 = 133;
         else
            processData2 = 5;
      }
      
      if (userIcolor == 3)
      {
         processData0 = 68;
         processData1 = 68;
         if (beepCnt)
            processData2 = 132;
         else
            processData2 = 4;
      }
      processData3 = 2;
      processData4 = 1;
      processData5 = 0;
      processData6 = userILevel;
      if (((REAL)lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10)>255)
         processData7 = 255;
      else
         processData7 = (USINT)(lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10);
   }
   
   
   
   if (ltRunMode == 9) //level mode upside down (stack size) //white color
   {
      processData0 = 119;
      processData1 = 119;
      if (beepCnt)
         processData2 = 135;
      else
         processData2 = 7;
      processData3 = 2;
      processData4 = 1;
      processData5 = 0;
      processData6 = segmentSpeed;
      if (((REAL)lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10)>255)
         processData7 = 255;
      else
         processData7 = (USINT)(lineController.setting.lightTowerSegmentSetup.buzzerVolume + (cutter.reading.output.vacuum==1?(lineController.setting.buzzerVolume*20):0) + cutter.reading.webSpeed*10);
   }
   if (ltRunMode == 3) //level mode (error)
   {
      if (errorCnt) errorCnt--;
      /*
      if (buffer.reading.status.state == Status_Error)       processData0 = 0x42;
      else                                                   processData0 = 0x22;
		
      if (cutter.reading.status.state == Status_Error 
         && merger.reading.status.state == Status_Error)     processData1 = 0x44;
      else if (cutter.reading.status.state == Status_Error)  processData1 = 0x42;
      else if (merger.reading.status.state == Status_Error)  processData1 = 0x24;
      else                                                   processData1 = 0x22;
		
      if (stacker.reading.status.state == Status_Error)      processData2 = 164;
      else                                                   processData2 = 162;
		*/
      if (buffer.reading.status.state == Status_Error)       processData0 = 0x24;
      else                                                   processData0 = 0x44;
		
      if (cutter.reading.status.state == Status_Error 
         && merger.reading.status.state == Status_Error)     processData1 = 0x22;
      else if (cutter.reading.status.state == Status_Error)  processData1 = 0x24;
      else if ((merger.reading.status.state == Status_Error && lineController.setup.merger.installed) || (folderConjugateStatus.state == Status_Error && lineController.setup.folder.installed))  processData1 = 0x42;
      else                                                   processData1 = 0x44;
		
      if (stacker.reading.status.state == Status_Error)      processData2 = 162;
      else                                                   processData2 = 164;
		
      
      
      
      processData3 = 2;
      processData4 = 1;
      processData5 = 0;
      processData6 = segmentSpeed;
      if (errorCnt>248)
      {
         if (lineController.setting.buzzerVolume == 0) processData7 = 50; 
         if (lineController.setting.buzzerVolume == 1) processData7 = 100;
         if (lineController.setting.buzzerVolume == 2) processData7 = 150;
         if (lineController.setting.buzzerVolume == 3) processData7 = 255;
      }
      else if (errorCnt>170)
         processData7 = lineController.setting.lightTowerSegmentSetup.buzzerVolume;
      else 
         processData7 = 0;
   }
   else errorCnt = 255;
   if (ltRunMode == 4) //level mode (startup)
   {
      processData3 = 8;
      processData4 = 0;
      processData5 = 0;
      processData6 = 0;
      processData7 = lineController.setting.lightTowerSegmentSetup.buzzerVolume;
      switch (segmentSpeed)
      {
         case 0:
            processData0 = 0;
            processData1 = 0;
            processData2 = 0;
            break;
         case 1: 
            processData0 = 1;
            processData1 = 0;
            processData2 = 0;
            break;
         case 2: 
            processData0 = 2;
            processData1 = 0;
            processData2 = 0;
            break;
         case 3: 
            processData0 = 4;
            processData1 = 0;
            processData2 = 0;
            break;
         case 4: 
            processData0 = 8;
            processData1 = 0;
            processData2 = 0;
            break;
         case 5: 
            processData0 = 16;
            processData1 = 0;
            processData2 = 0;
            break;
         case 6: 
            processData0 = 32;
            processData1 = 0;
            processData2 = 0;
            break;
         case 7: 
            processData0 = 64;
            processData1 = 0;
            processData2 = 0;
            break;
         case 8: 
            processData0 = 128;
            processData1 = 0;
            processData2 = 0;
            break;
         case 9: 
            processData0 = 0;
            processData1 = 1;
            processData2 = 0;
            break;
         case 10: 
            processData0 = 0;
            processData1 = 2;
            processData2 = 0;
            break;
         case 11: 
            processData0 = 0;
            processData1 = 4;
            processData2 = 0;
            break;
         case 12: 
            processData0 = 0;
            processData1 = 8;
            processData2 = 0;
            break;
         case 13: 
            processData0 = 0;
            processData1 = 16;
            processData2 = 0;
            break;
         case 14: 
            processData0 = 0;
            processData1 = 32;
            processData2 = 0;
            break;
         case 15: 
            processData0 = 0;
            processData1 = 64;
            processData2 = 0;
            break;
         case 16: 
            processData0 = 0;
            processData1 = 128;
            processData2 = 0;
            break;
         case 17: 
            processData0 = 0;
            processData1 = 0;
            processData2 = 1;
            break;
         case 18: 
            processData0 = 0;
            processData1 = 0;
            processData2 = 2;
            break;
         case 19: 
            processData0 = 0;
            processData1 = 0;
            processData2 = 4;
            break;
         case 20: 
            processData0 = 0;
            processData1 = 0;
            processData2 = 8;
            break;
      }
   }
   if (ltRunMode == 5) //level mode (startup2)
   {
		
      processData3 = 8;
      processData4 = 0;
      processData5 = 0;
      processData6 = 0;
      processData7 = lineController.setting.lightTowerSegmentSetup.buzzerVolume;
		
      processData0 = seg2;
      processData0 = seg1 | (processData0<<4);
      processData1 = seg4;
      processData1 = seg3 | (processData1<<4);
      processData2 = seg5 | (beep<<4);
   }
   if (ltRunMode == 11) //remote control
   {
      if (tick%20 < 10)
      {
         processData0 = 54;
         processData1 = 54;
         if (beepCnt)
            processData2 = 134;
         else
            processData2 = 6;
      }
      else
      {
         processData0 = 99;
         processData1 = 99;
         if (beepCnt)
            processData2 = 131;
         else
            processData2 = 3;
      }
      processData3 = 2;
      processData4 = 1;
      processData5 = 0;
      processData6 = 255; 

   }
   
   if (streamer.reading.showStopPlateLT && ltRunMode != 3 && ltRunMode != 4 && ltRunMode != 5)
   {
      USINT lr[21] = {0,12,25,38,51,63,76,89,102,114,127,140,153,165,178,191,204,216,229,242,255};
      /*processData0 = 0x32;
      processData1 = 0x31;
      processData2 = 0x02;
      if (beepCnt) processData2 = 0x80;
      processData3 = 2;
      processData4 = 0;
      processData5 = 0;
      processData6 = lr[stopPlateLightTower(stacker.reading.input.streamerStopPlatePos,cutter.reading.sheetSize)];*/
      
      
      processData0 = COLOR_YELLOW_MASK_1_3_5 | COLOR_YELLOW_MASK_2_4;
      processData1 = COLOR_GREEN_MASK_1_3_5 | COLOR_YELLOW_MASK_2_4;
      processData2 = COLOR_YELLOW_MASK_1_3_5;
      if (beepCnt) processData2 = 0x80;
      processData3 = 2;
      processData4 = 0;
      processData5 = 0;
      processData6 = lr[stopPlateLightTower(stacker.reading.input.streamerStopPlatePos,cutter.reading.sheetSize)];
      
      
      
   }
   
   if (streamer.reading.showStreamerRollerPlateLT && ltRunMode != 3 && ltRunMode != 4 && ltRunMode != 5)
   {
      USINT lr[21] = {0,12,25,38,51,63,76,89,102,114,127,140,153,165,178,191,204,216,229,242,255};
      processData0 = COLOR_BLUE_MASK_1_3_5 | COLOR_BLUE_MASK_2_4;
      processData1 = COLOR_GREEN_MASK_1_3_5 | COLOR_BLUE_MASK_2_4;
      processData2 = COLOR_BLUE_MASK_1_3_5;
      if (beepCnt) processData2 = 0x80;
      processData3 = 2;
      processData4 = 0;
      processData5 = 0;
      processData6 = lr[streamRollerLightTower(stacker.reading.input.streamerStrRollerPos,cutter.reading.sheetSize)];
   }
   
}

void uwMachineState(void)
{
   if (!lineController.setup.unwinder.installed)
   {
      unwinder.reading.status.state = Status_Ready;
      unwinder.reading.status.stopped = false;
      unwinder.reading.status.warning = false;
      return;
   }
   unwinder.reading.status.stopped = true;
   unwinder.reading.status.warning = false;
   if (!u20.connected)
   {
      unwinder.reading.status.state = Status_NotLoaded;
   }
   else
   {
      unwinder.reading.status.state = Status_Ready;
      if (u20.started)
      {
         unwinder.reading.status.stopped = false;
         if (u20.slowMode || u20.diameter<10) unwinder.reading.status.warning = true;
      }
   }
}

void readMachineStates(void)
{
   BOOL anyMachineInError = false;
   BOOL stackerRunning = false;
   BOOL cutterRunning = false;
   BOOL bufferRunning = false;
   BOOL mergerRunning = false;
   BOOL folderRunning = false;
   BOOL unwinderRunning = false;
   BOOL stackerRunningW = false;
   BOOL cutterRunningW = false;
   BOOL bufferRunningW = false;
   BOOL mergerRunningW = false;
   BOOL folderRunningW = false;
   BOOL unwinderRunningW = false;
   BOOL webIsRunning = true;
   USINT stackerLtPosition = 0;
   USINT cutterLtPosition = 0;
   USINT folderLtPosition = 0;
   USINT mergerLtPosition = 0;
   USINT bufferLtPosition = 0;
   USINT unwinderLtPosition = 0;
   static REAL lastUsage;
   static USINT signalFull;
   static BOOL goingDown;
   static USINT lightRoller;
   //struct machineStatus_typ tmpStatus;
   
   lineController.reading.lineReady = false;

   if (cutter.reading.webSpeed<0.05 && cutter.reading.setSpeed<0.05) webIsRunning = false;
   /*if (stacker.reading.status.state == Status_Error) anyMachineInError = true;
   if (buffer.reading.status.state == Status_Error) anyMachineInError = true;
   if (folder.reading.status.state == Status_Error) anyMachineInError = true;*/
	
   if (lineController.setting.lightTowerSegmentSetup.machineSegment1 == MachineStacker) stackerLtPosition = 0; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment2 == MachineStacker) stackerLtPosition = 1; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment3 == MachineStacker) stackerLtPosition = 2; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment4 == MachineStacker) stackerLtPosition = 3; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment5 == MachineStacker) stackerLtPosition = 4; 
	
   if (lineController.setting.lightTowerSegmentSetup.machineSegment1 == MachineCutter) cutterLtPosition = 0; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment2 == MachineCutter) cutterLtPosition = 1; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment3 == MachineCutter) cutterLtPosition = 2; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment4 == MachineCutter) cutterLtPosition = 3; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment5 == MachineCutter) cutterLtPosition = 4;
	
   if (lineController.setting.lightTowerSegmentSetup.machineSegment1 == MachineBuffer) bufferLtPosition = 0; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment2 == MachineBuffer) bufferLtPosition = 1; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment3 == MachineBuffer) bufferLtPosition = 2; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment4 == MachineBuffer) bufferLtPosition = 3; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment5 == MachineBuffer) bufferLtPosition = 4;
	
   if (lineController.setting.lightTowerSegmentSetup.machineSegment1 == MachineMerger) mergerLtPosition = 0; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment2 == MachineMerger) mergerLtPosition = 1; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment3 == MachineMerger) mergerLtPosition = 2; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment4 == MachineMerger) mergerLtPosition = 3; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment5 == MachineMerger) mergerLtPosition = 4;
	
   if (lineController.setting.lightTowerSegmentSetup.machineSegment1 == MachineFolder) folderLtPosition = 0; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment2 == MachineFolder) folderLtPosition = 1; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment3 == MachineFolder) folderLtPosition = 2; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment4 == MachineFolder) folderLtPosition = 3; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment5 == MachineFolder) folderLtPosition = 4;
   
   if (lineController.setting.lightTowerSegmentSetup.machineSegment1 == MachineUnwinder) unwinderLtPosition = 0; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment2 == MachineUnwinder) unwinderLtPosition = 1; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment3 == MachineUnwinder) unwinderLtPosition = 2; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment4 == MachineUnwinder) unwinderLtPosition = 3; 
   if (lineController.setting.lightTowerSegmentSetup.machineSegment5 == MachineUnwinder) unwinderLtPosition = 4;
	
   switch (stacker.reading.status.state) {
      case Status_NotLoaded:
         segmentBkgColor[stackerLtPosition] = lt_white;
         segmentColor[stackerLtPosition] = lt_white;
         lineController.reading.stackerStatus = 0;
         if (anyMachineInError) segmentBkgColor[stackerLtPosition] = segmentColor[stackerLtPosition] = lt_red;
         break;
      case Status_Loading:
         segmentBkgColor[stackerLtPosition] = lt_off;
         segmentColor[stackerLtPosition] = lt_white;
         lineController.reading.stackerStatus = 1;
         if (anyMachineInError) segmentBkgColor[stackerLtPosition] = segmentColor[stackerLtPosition] = lt_red;	
         break;
      case Status_ManualLoad:
         segmentBkgColor[stackerLtPosition] = lt_off;
         segmentColor[stackerLtPosition] = lt_yellow;
         lineController.reading.stackerStatus = 1;
         if (anyMachineInError) segmentBkgColor[stackerLtPosition] = segmentColor[stackerLtPosition] = lt_red;
         break;
      case Status_Ready:
         if (anyMachineInError) segmentBkgColor[stackerLtPosition] = segmentColor[stackerLtPosition] = lt_blue;
         
         if (lineController.setting.lightTowerSegmentSetup.simpleMode) {//TODO remove not simple mode option,, it's not used
            if (stacker.reading.status.stopped) {
               segmentColor[stackerLtPosition] = lt_blue;
               segmentBkgColor[stackerLtPosition] = lt_blue;
               lineController.reading.stackerStatus = 2;
            }
            else {
               segmentColor[stackerLtPosition] = lt_off;
               segmentBkgColor[stackerLtPosition] = lt_off;
               lineController.reading.stackerStatus = 3;
               stackerRunning = true;
            }
            
         }
         break;
      case Status_Error:
         segmentBkgColor[stackerLtPosition] = lt_blue;
         segmentColor[stackerLtPosition] = lt_red;
         lineController.reading.stackerStatus = 5;
         anyMachineInError = true;
         break;
   }
   
   if (lineController.setup.unwinder.installed) {
      switch (unwinder.reading.status.state) {
         case Status_NotLoaded:
            segmentBkgColor[unwinderLtPosition] = lt_white;
            segmentColor[unwinderLtPosition] = lt_white;
            if (anyMachineInError) segmentBkgColor[unwinderLtPosition] = segmentColor[unwinderLtPosition] = lt_red;
            break;
         case Status_Loading:
            segmentBkgColor[unwinderLtPosition] = lt_off;
            segmentColor[unwinderLtPosition] = lt_white;
            if (anyMachineInError) segmentBkgColor[unwinderLtPosition] = segmentColor[unwinderLtPosition] = lt_red;	
            break;
         case Status_ManualLoad:
            segmentBkgColor[unwinderLtPosition] = lt_off;
            segmentColor[unwinderLtPosition] = lt_yellow;
            if (anyMachineInError) segmentBkgColor[unwinderLtPosition] = segmentColor[unwinderLtPosition] = lt_red;
            break;
         case Status_Ready:
            if (lineController.setting.lightTowerSegmentSetup.simpleMode)
            {
               if (unwinder.reading.status.stopped) {
                  segmentColor[unwinderLtPosition] = lt_blue;
                  segmentBkgColor[unwinderLtPosition] = lt_blue;
               }
               else {
                  segmentColor[unwinderLtPosition] = lt_off;
                  segmentBkgColor[unwinderLtPosition] = lt_off;
                  unwinderRunning = true;
               }
            }
            if (anyMachineInError) segmentBkgColor[unwinderLtPosition] = segmentColor[unwinderLtPosition] = lt_blue;
            break;
         case Status_Error:
            segmentBkgColor[unwinderLtPosition] = lt_blue;
            segmentColor[unwinderLtPosition] = lt_red;
            anyMachineInError = true;
            break;
      }
      if (unwinder.reading.status.warning) unwinderRunningW = true;
   }
   
   switch (cutter.reading.status.state) {
      case Status_NotLoaded:
         segmentBkgColor[cutterLtPosition] = lt_white;
         segmentColor[cutterLtPosition] = lt_white;
         lineController.reading.cutterStatus = 0;
         if (anyMachineInError) segmentBkgColor[cutterLtPosition] = segmentColor[cutterLtPosition] = lt_red;
         break;
      case Status_Loading:
         segmentBkgColor[cutterLtPosition] = lt_off;
         segmentColor[cutterLtPosition] = lt_white;
         lineController.reading.cutterStatus = 1;
         if (anyMachineInError) segmentBkgColor[cutterLtPosition] = segmentColor[cutterLtPosition] = lt_red;	
         break;
      case Status_ManualLoad:
         segmentBkgColor[cutterLtPosition] = lt_off;
         segmentColor[cutterLtPosition] = lt_yellow;
         lineController.reading.cutterStatus = 1;
         if (anyMachineInError) segmentBkgColor[cutterLtPosition] = segmentColor[cutterLtPosition] = lt_red;
         break;
      case Status_Ready:
         if (lineController.setting.lightTowerSegmentSetup.simpleMode)
         {
            if (cutter.reading.status.stopped) {
               segmentColor[cutterLtPosition] = lt_blue;
               segmentBkgColor[cutterLtPosition] = lt_blue;
               lineController.reading.cutterStatus = 2;
            }
            else {
               segmentColor[cutterLtPosition] = lt_off;
               segmentBkgColor[cutterLtPosition] = lt_off;
               lineController.reading.cutterStatus = 3;
               cutterRunning = true;
            }
         }
         if (anyMachineInError) segmentBkgColor[cutterLtPosition] = segmentColor[cutterLtPosition] = lt_blue;
         break;
      case Status_Error:
         segmentBkgColor[cutterLtPosition] = lt_blue;
         segmentColor[cutterLtPosition] = lt_red;
         lineController.reading.cutterStatus = 5;
         anyMachineInError = true;
         break;
   }
   if (lineController.setup.merger.installed) {
      switch (merger.reading.status.state) {
         case Status_NotLoaded:
            segmentBkgColor[mergerLtPosition] = lt_white;
            segmentColor[mergerLtPosition] = lt_white;
            lineController.reading.mergerStatus = 0;
            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_red;
            break;
         case Status_Loading:
            segmentBkgColor[mergerLtPosition] = lt_off;
            segmentColor[mergerLtPosition] = lt_white;
            lineController.reading.mergerStatus = 1;
            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_red;	
            break;
         case Status_ManualLoad:
            segmentBkgColor[mergerLtPosition] = lt_off;
            segmentColor[mergerLtPosition] = lt_yellow;
            lineController.reading.mergerStatus = 1;
            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_red;
            break;
         case Status_Ready:
            if (lineController.setting.lightTowerSegmentSetup.simpleMode)
            {
               if (merger.reading.status.stopped) {
                  segmentColor[mergerLtPosition] = lt_blue;
                  segmentBkgColor[mergerLtPosition] = lt_blue;
                  lineController.reading.mergerStatus = 2;
               }
               else {
                  segmentColor[mergerLtPosition] = lt_off;
                  segmentBkgColor[mergerLtPosition] = lt_off;
                  lineController.reading.mergerStatus = 3;
                  mergerRunning = true;	
               }
            }

            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_blue;
            break;
         case Status_Error:
            segmentBkgColor[mergerLtPosition] = lt_blue;
            segmentColor[mergerLtPosition] = lt_red;
            lineController.reading.mergerStatus = 5;
            anyMachineInError = true;
            break;
      }
   }
   else if (lineController.setup.folder.installed)
   { 
      if (lineController.setup.folder.automationInstalled)
      {
         folderConjugateStatus.stopped = false;
         folderConjugateStatus.warning = false;
         
         if (folderAutomation.reading.status.state == folder.reading.status.state) folderConjugateStatus.state = folder.reading.status.state;
         else if (folderAutomation.reading.status.state == Status_Error || folder.reading.status.state == Status_Error) folderConjugateStatus.state = Status_Error;
         else if (folderAutomation.reading.status.state == Status_ManualLoad || folder.reading.status.state == Status_ManualLoad) folderConjugateStatus.state = Status_ManualLoad;
         else if (folderAutomation.reading.status.state == Status_Loading || folder.reading.status.state == Status_Loading) folderConjugateStatus.state = Status_Loading;
         else folderConjugateStatus.state = Status_NotLoaded;
         
         if (folder.reading.status.stopped || folderAutomation.reading.status.stopped) folderConjugateStatus.stopped = true;
         if (folder.reading.status.warning || folderAutomation.reading.status.warning) folderConjugateStatus.warning = true;
         
      }
      else
      {
         folderConjugateStatus.state = folder.reading.status.state;
         folderConjugateStatus.stopped = folder.reading.status.stopped;
         folderConjugateStatus.warning = folder.reading.status.warning;
      }   

      folderRunningW = folderConjugateStatus.warning;
      
      switch (folderConjugateStatus.state) {
         case Status_NotLoaded:
            segmentBkgColor[mergerLtPosition] = lt_white;
            segmentColor[mergerLtPosition] = lt_white;
            lineController.reading.mergerStatus = 0;
            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_red;
            break;
         case Status_Loading:
            segmentBkgColor[mergerLtPosition] = lt_off;
            segmentColor[mergerLtPosition] = lt_white;
            lineController.reading.mergerStatus = 1;
            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_red;	
            break;
         case Status_ManualLoad:
            segmentBkgColor[mergerLtPosition] = lt_off;
            segmentColor[mergerLtPosition] = lt_yellow;
            lineController.reading.mergerStatus = 1;
            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_red;
            break;
         case Status_Ready:
            if (lineController.setting.lightTowerSegmentSetup.simpleMode)
            {
               if (folderConjugateStatus.stopped) {
                  segmentColor[mergerLtPosition] = lt_blue;
                  segmentBkgColor[mergerLtPosition] = lt_blue;
                  lineController.reading.mergerStatus = 2;
               }
               else {
                  segmentColor[mergerLtPosition] = lt_off;
                  segmentBkgColor[mergerLtPosition] = lt_off;
                  lineController.reading.mergerStatus = 3;
                  folderRunning = true;	
               }
            }
            if (anyMachineInError) segmentBkgColor[mergerLtPosition] = segmentColor[mergerLtPosition] = lt_blue;
            break;
         case Status_Error:
            segmentBkgColor[mergerLtPosition] = lt_blue;
            segmentColor[mergerLtPosition] = lt_red;
            lineController.reading.mergerStatus = 5;
            anyMachineInError = true;
            break;
      }
   }
	
   switch (buffer.reading.status.state) {
      case Status_NotLoaded:
         segmentBkgColor[bufferLtPosition] = lt_white;
         segmentColor[bufferLtPosition] = lt_white;
         lineController.reading.bufferStatus = 0;
         if (anyMachineInError) segmentBkgColor[bufferLtPosition] = segmentColor[bufferLtPosition] = lt_red;
         break;
      case Status_Loading:
         segmentBkgColor[bufferLtPosition] = lt_off;
         segmentColor[bufferLtPosition] = lt_white;
         lineController.reading.bufferStatus = 1;
         if (anyMachineInError) segmentBkgColor[bufferLtPosition] = segmentColor[bufferLtPosition] = lt_red;
         break;
      case Status_ManualLoad:
         segmentBkgColor[bufferLtPosition] = lt_off;
         segmentColor[bufferLtPosition] = lt_yellow;
         lineController.reading.bufferStatus = 1;
         if (anyMachineInError) segmentBkgColor[bufferLtPosition] = segmentColor[bufferLtPosition] = lt_red;
         break;
      case Status_Ready:
         if (lineController.setting.lightTowerSegmentSetup.simpleMode)
         {
            if (buffer.reading.status.stopped) {
               segmentColor[bufferLtPosition] = lt_blue;
               segmentBkgColor[bufferLtPosition] = lt_blue;
               lineController.reading.bufferStatus = 2;
               signalNoPaperSeq = 0;
            }
            else {
               segmentColor[bufferLtPosition] = lt_off;
               segmentBkgColor[bufferLtPosition] = lt_off;
               lineController.reading.bufferStatus = 3;
               bufferRunning = true;
               
               if (buffer.reading.usage>90.0 && lastUsage<90) {
                  signalFull = 10;   
               }
               lastUsage = buffer.reading.usage;
               if (signalFull) {
                  signalFull--;
                  segmentColor[bufferLtPosition] = lt_orange;
                  segmentBkgColor[bufferLtPosition] = lt_orange;
               }
               
               
            }
         }
         if (anyMachineInError) segmentBkgColor[bufferLtPosition] = segmentColor[bufferLtPosition] = lt_blue;
         break;
      case Status_Error:
         segmentBkgColor[bufferLtPosition] = lt_blue;
         segmentColor[bufferLtPosition] = lt_red;
         lineController.reading.bufferStatus = 5;
         anyMachineInError = true;
         break;
   }
   
   /*
     0 - no text
     1 - wastebox full (stacker)
     2 - sub seq not ready (stacker)
     3 - in decurl (buffer)
     4 - waste gate open (cutter)
     5 - no power (all)
     6 - initializing system (all)
     7 - cover open (all)
     8 - door open (all)
     9 - no paper (buffer (manual load))
   
   
     20 - Low Glue Level
   */
   lineController.reading.mergerStatusAddInfo = 0;   
   if (stacker.reading.status.warning) {
      stackerRunningW = true;
      if (stacker.reading.warningCode == 1) lineController.reading.stackerStatusAddInfo = 1;
      else if (stacker.reading.warningCode == 2) lineController.reading.stackerStatusAddInfo = 2;
      else if (stacker.reading.warningCode == 7) lineController.reading.stackerStatusAddInfo = 7;
      else if (stacker.reading.warningCode == 20) lineController.reading.stackerStatusAddInfo = 20;
      else if (stacker.reading.warningCode == 21) lineController.reading.stackerStatusAddInfo = 21;
      else lineController.reading.stackerStatusAddInfo = 0;
      /*
      if (lineController.setup.streamer.installed)
      {
         if (!stacker.reading.input.alignerTopCoverClosed || !stacker.reading.input.topCoverClosed) lineController.reading.stackerStatusAddInfo = 7;
      }*/
   }
   else { 
      lineController.reading.stackerStatusAddInfo = 0;
   }
   if (buffer.reading.inDecurl) {
      lineController.reading.bufferStatusAddInfo = 3;
      bufferRunningW = true;
   }
   else if (buffer.reading.status.state == Status_ManualLoad) {
      if (buffer.reading.button2 != _YELLOW && buffer.reading.button3 != _YELLOW && buffer.reading.button4 != _YELLOW)
         lineController.reading.bufferStatusAddInfo = 9;
      else 
         lineController.reading.bufferStatusAddInfo = 0;
   }   
   else lineController.reading.bufferStatusAddInfo = 0; 
   
   if (GUI_status.statusBarInit == 0) {
      lineController.reading.cutterStatusAddInfo = 6;
      lineController.reading.mergerStatusAddInfo = 6;
   }
   else if (GUI_status.statusBarDrivesDisabled == 0) {
      lineController.reading.cutterStatusAddInfo  = 5;
      lineController.reading.mergerStatusAddInfo  = 5;
      lineController.reading.stackerStatusAddInfo = 5;
      if (lineController.reading.timeSinceStart % 2 == 1) {
         if (!merger.reading.input.frontDoor && lineController.setup.merger.installed) lineController.reading.mergerStatusAddInfo = 8;
         if (!cutter.reading.input.frontDoor) lineController.reading.cutterStatusAddInfo = 8;
         if (!cutter.reading.input.frontDoorKnife) lineController.reading.cutterStatusAddInfo = 8;
      }
   }
   else {
      if ((cutter.reading.status.state == Status_Ready && !cutter.reading.status.stopped)) {
         mergerRunningW = true;
         cutterRunningW = true;
         if (cutter.reading.output.reject) {
            lineController.reading.cutterStatusAddInfo = 4;
            cutterRunningW = false;
         }
         else if (!cutter.reading.input.frontDoor) lineController.reading.cutterStatusAddInfo = 8;
         else if (!cutter.reading.input.frontDoorKnife) lineController.reading.cutterStatusAddInfo = 8;
         else if (!cutter.reading.input.topCover) lineController.reading.cutterStatusAddInfo = 7;
         else {
            lineController.reading.cutterStatusAddInfo = 0;
            cutterRunningW = false;
         }
         
         if (!merger.reading.input.frontDoor && lineController.setup.merger.installed) lineController.reading.mergerStatusAddInfo = 8;
         else {
            lineController.reading.mergerStatusAddInfo = 0;
            mergerRunningW = false;
         }
      }
      else {
         lineController.reading.cutterStatusAddInfo = 0;
         lineController.reading.mergerStatusAddInfo = 0;
         if (cutter.reading.status.state == Status_Ready) {
            if (!cutter.reading.input.topCover) lineController.reading.cutterStatusAddInfo = 7;
            if (!merger.reading.input.frontDoor && lineController.setup.merger.installed) lineController.reading.mergerStatusAddInfo = 8;
            if (!cutter.reading.input.frontDoor) lineController.reading.cutterStatusAddInfo = 8;
            if (!cutter.reading.input.frontDoorKnife) lineController.reading.cutterStatusAddInfo = 8;
         }
      }
      if (!lineController.setup.merger.installed) mergerRunningW = false;
   }
   
   if (Status_Ready == folder.reading.status.state && lineController.setup.folder.installed) { //folder is hijacking on the mergerStatus
      if (!folder.reading.input.infeedSideDoor || !folder.reading.input.rightFrontDoor) {
         mergerRunningW = true;
         lineController.reading.mergerStatusAddInfo = 8;
      }
      else if (!folder.reading.input.infeedTopCover || !folder.reading.input.outfeedTopCover) {
         mergerRunningW = true;
         lineController.reading.mergerStatusAddInfo = 7;
      }
      
   }

   if (anyMachineInError)
   {
      ltRunMode = 3;
      if (goingDown)
      {
         if (lightRoller<20) lightRoller++;
         else goingDown = 0;
      }
      else
      {
         if (lightRoller>0) lightRoller--;
         else goingDown = 1;
      }
      //if (lightRoller>20) lightRoller = 20;
      //if (lightRoller<0) lightRoller = 0;
		
      segmentSpeed = rl[lightRoller];
   }
   else if (bufferRunning  && 
            stackerRunning && 
            cutterRunning  && 
           (mergerRunning || !lineController.setup.merger.installed) && 
           (unwinderRunning || !lineController.setup.unwinder.installed) && 
           (folderRunning || !lineController.setup.folder.installed)) {
      if (bufferRunningW || stackerRunningW || cutterRunningW || mergerRunningW || folderRunningW || unwinderRunningW)
         ltRunMode = 6; //orange level
      else 
      {
         if (1 == lineController.setting.lightTowerSegmentSetup.runningMode)
            ltRunMode = 8; //light blue reverse level
         else if (2 == lineController.setting.lightTowerSegmentSetup.runningMode)
            ltRunMode = 9; //light blue reverse level
         else if (3 == lineController.setting.lightTowerSegmentSetup.runningMode)
            ltRunMode = 2; //steady green
         else
            ltRunMode = 2; //green level
      }
      if (1 == lineController.setting.lightTowerSegmentSetup.runningMode)
         segmentSpeed = (unsigned char)(rmap(buffer.reading.usage,0,100,7,255));
      else if (2 == lineController.setting.lightTowerSegmentSetup.runningMode)
         segmentSpeed = (unsigned char)(rmap(stacker.reading.stackSize,0,stacker.reading.maxStackSize,7,255));
      else if (3 == lineController.setting.lightTowerSegmentSetup.runningMode)
      {
         if (segmentSpeed<255) segmentSpeed++;
      }
      else
         segmentSpeed = (unsigned char)(rmap(cutter.reading.webSpeed,0,cutter.setting.infeedRunSpeed*0.001,7,255));
      lineController.reading.lineReady = true;
   }
   else
   {
      ltRunMode = 0;
      segmentSpeed = 0;
   }

   if (lineController.setup.unwinder.installed)
   {
      if (  (cutter.reading.status.state   == Status_Ready && !cutter.reading.status.stopped) 
         && (stacker.reading.status.state  == Status_Ready && !stacker.reading.status.stopped)
         && (folderConjugateStatus.state   == Status_Ready && !folder.reading.status.stopped)
         && (buffer.reading.status.state   == Status_Ready && !buffer.reading.status.stopped)) //everyone is ready except the unwinder (roll change ??)
      {
         if (unwinder.reading.status.state == Status_Ready && unwinder.reading.status.stopped) idleTime += SECONDS_UNIT;
         else idleTime = 0;
         
         if (idleTime > 60.0)
         {
            lineController.command.trippleBeep = true;
            cutter.command.stop = true;
            idleTime = 0;
         }
         
      }
      else idleTime = 0;
   }
   
   
   /*if (ltRunMode == 2 || ltRunMode == 6) //line ready
   {
      if (cutter.reading.setSpeed == 0) //idle
         idleTime += SECONDS_UNIT;
      else
         idleTime = 0;
      
      if (idleTime > 120.0)
      {
         cutter.command.stop = true;
         idleTime = 0;
      }
   }
   else 
   {
       idleTime = 0;
   }*/
  
   if (lineController.command.remoteControlled) ltRunMode = 11;
   
   
}


REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max)
{
   REAL tmp;
   tmp = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
   if (tmp > out_max) return out_max;
   if (tmp < out_min) return out_min;
   return tmp;
}

int stopPlateLightTower(float rV, float sP)
{
   float x;
   float a = 0.275;
   float b = 1.6;
   float c = 4.75;
   float offset = 0.10;
   x = rV-sP + offset; //ReadingValue - setPoint
    
   if      (x < (-a*4 - b*3 - c*4))   return 1;
   else if (x < (-a*4 - b*3 - c*3))   return 2;
   else if (x < (-a*4 - b*3 - c*2))   return 3;
   else if (x < (-a*4 - b*3 - c))     return 4;  
    
   else if (x < (-a*4 - b*3))         return 5;  
   else if (x < (-a*4 - b*2))         return 6;  
   else if (x < (-a*4 - b))           return 7;  
   else if (x < -a*4)                 return 8;  
    
   else if (x < (-a))                 return 9;  
   else if (x < (a))                  return 10; //<- we are golden  
   else if (x < (a*3))                return 11;  
   else if (x < (a*5))                return 12;  
    
   else if (x < (a*5 + b))            return 13;  
   else if (x < (a*5 + b*2))          return 14;  
   else if (x < (a*5 + b*3))          return 15;  
   else if (x < (a*5 + b*4))          return 16;  
    
   else if (x < (a*5 + b*4 + c))      return 17;  
   else if (x < (a*5 + b*4 + c*2))    return 18;  
   else if (x < (a*5 + b*4 + c*3))    return 19;  
   else                               return 20;  
}


int streamRollerLightTower(float rV, float sP)
{
   float x;
   float a = 0.6;
   float b = 3.0;
   float c = 9;
   float offset = 0.10;
   x = rV-sP + offset; //ReadingValue - setPoint
    
   if      (x < (-a*4 - b*3 - c*4))   return 1;
   else if (x < (-a*4 - b*3 - c*3))   return 2;
   else if (x < (-a*4 - b*3 - c*2))   return 3;
   else if (x < (-a*4 - b*3 - c))     return 4;  
    
   else if (x < (-a*4 - b*3))         return 5;  
   else if (x < (-a*4 - b*2))         return 6;  
   else if (x < (-a*4 - b))           return 7;  
   else if (x < -a*4)                 return 8;  
    
   else if (x < (-a))                 return 9;  
   else if (x < (a))                  return 10; //<- we are golden  
   else if (x < (a*3))                return 11;  
   else if (x < (a*5))                return 12;  
    
   else if (x < (a*5 + b))            return 13;  
   else if (x < (a*5 + b*2))          return 14;  
   else if (x < (a*5 + b*3))          return 15;  
   else if (x < (a*5 + b*4))          return 16;  
    
   else if (x < (a*5 + b*4 + c))      return 17;  
   else if (x < (a*5 + b*4 + c*2))    return 18;  
   else if (x < (a*5 + b*4 + c*3))    return 19;  
   else                               return 20;  
}

void handleLineLog(void)
{
   static REAL meter;
   static BOOL lastErr;
   meter += cutter.reading.webSpeed * SECONDS_UNIT; 
   if (lastErr != machineError.active && machineError.active)
   {
      lineLogP++;
      if (lineLogP>=50) lineLogP = 0;
      lineLog[lineLogP].Distance += (UDINT)meter;
      lineLog[lineLogP].AvgRunSpeed = (UINT)stacker.reading.tripCounter.avgSpeed;
      lineLog[lineLogP].ErrId = machineError.errType;
      lineLog[lineLogP].Sub = machineError.errSubType;
      lineLog[lineLogP].TimeSinceStart = lineController.reading.timeSinceStart;
      lineLog[lineLogP].Reboots = bootUps;
      meter = 0.0;
   }
   lastErr = machineError.active;
}


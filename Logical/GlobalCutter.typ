
TYPE
	cutter_ext_command_typ : 	STRUCT 
		stopNext : BOOL := FALSE;
		cutMinus : BOOL := FALSE;
		cutPlus : BOOL := FALSE;
		decSpeed : BOOL := FALSE;
		deliver : BOOL := FALSE;
		feed : BOOL := FALSE;
		home : BOOL := FALSE;
		incSpeed : BOOL := FALSE;
		reload : BOOL := FALSE;
		rejectSheets : BOOL := FALSE;
		start : BOOL := FALSE;
		stopRequest : BOOL := FALSE;
		stop : BOOL := FALSE;
		deliverNextRequest : BOOL := FALSE; (*deliver next (over min stack)*)
		deliverNowRequestToggle : BOOL := FALSE; (*deliver on max stack*)
		stopDeliverRequest : BOOL := FALSE;
		skipToPrint : BOOL := FALSE;
		feedToCut : BOOL := FALSE;
	END_STRUCT;
	cutter_readings_typ : 	STRUCT 
		currentPage : sheet;
		sheetSignal : BOOL; (*Toggles when a new page is cut*)
		pendingDeliverToggle : BOOL; (*Toggles when cutters starts to go down in speed to send a deliver page*)
		sheetSize : REAL; (*mm*)
		formatImperial : REAL; (*inches*)
		webSpeed_mm : REAL; (*m/m*)
		webSpeed_fpm : REAL; (*fpm*)
		webSpeed : REAL; (*m/s*)
		input : physical_cutter_inputs; (*current state of input*)
		output : physical_cutter_outputs; (*current state of output*)
		tempFront : REAL; (*C*)
		tempRear : REAL; (*C*)
		tempLeft : REAL; (*C*)
		tempRight : REAL; (*C*)
		maximumSpeed : REAL; (*m/s*)
		ioBusModuleOk : BOOL := FALSE;
		setSpeed : REAL; (*m/s*)
		startUpSeq : UINT := 0; (*0 -> 15*)
		status : machineStatus_typ;
		fastFeed : BOOL := FALSE;
		limiter : USINT;
		correctionValueAvg : REAL := 0;
		correctionValue : REAL := 0;
		missingMarks : BOOL := FALSE;
		pagesToSkip : UDINT := 0;
		processingWhite : BOOL := FALSE;
		highPosMax : REAL;
		lowPosMin : REAL;
		goodPages : UDINT := 0;
		wastePages : UDINT := 0;
	END_STRUCT;
	cutter_settings_typ : 	STRUCT 
		forceVacuum : BOOL := FALSE;
		regMarkOffset : REAL := 0; (*mm*)
		format : REAL := 305.0; (*mm*)
		reloadOnWhite : BOOL := FALSE;
		outfeedCrash1Enable : BOOL := FALSE;
		outfeedCrash2Enable : BOOL := FALSE;
		outfeedCrash3Enable : BOOL := FALSE;
		simulateOffset : DINT := 0; (*number of sheets*)
		stopOffset : REAL := 90; (*mm*)
		masterDec : REAL := 6074; (*mm/s^2*)
		masterAcc : REAL := 300; (*mm/s^2*)
		infeedRunSpeed : REAL := 500;
		sensorKnifeDist : REAL := 2600; (*mm*)
		knifeDiff : REAL := 123;
		stripSize : REAL := 8;
		stripCut : BOOL := FALSE;
		mainInfeedOverSpeed : DINT := 0;
		supportOverSpeed : DINT := 50;
		forceOutputEnable : physical_cutter_outputs;
		forceOutputValue : physical_cutter_outputs;
		mainKnifeHomeOffset : REAL := 0.0;
		deliverStopTime : DINT := 400;
		markRegistration : BOOL := FALSE;
		regMarkMaxWidth : REAL := 5.0;
		regMarkMinWidth : REAL := 0.5;
		jobMarkSeparation : BOOL := FALSE;
		jobMarkDelay : DINT := 3;
		jobMarkMaxWidth : REAL := 17.5;
		jobMarkMinWidth : REAL := 12.5;
		skipToPrintLength : REAL := 20.0;
		whitePagesLength : REAL := 10.0;
		rejectSkipToPrint : BOOL := FALSE;
		slowSpeedLoad : BOOL := TRUE;
		stackWhitePages : BOOL := FALSE;
		tensionIdealPerc : DINT := 50;
		tensionLowPos : DINT := 24000;
		tensionHighPos : DINT := 4000;
		offsetOnMissedMark : DINT := 0;
		maxMissedMarks : DINT := 1;
		markBeep : BOOL := FALSE;
		cutMarkAbove0Below1 : DINT := 0;
		rejectDelay : REAL := 100.0;
		rejectBannerPages : DINT := 0;
		jobMarkAbove0Below1 : DINT := 0;
		confName : STRING[50] := '<Default configuration>';
		maxPrinterOverSpeed : REAL := 0.0;
		stopWhenDelivering : BOOL := TRUE;
		deliverDecStartSpeed : REAL := 4.0;
		deliverDecMinSpeed : REAL := 0.2;
		bufferLowPos : REAL;
		bufferHighPos : REAL;
		noMarksSlowSpeed : REAL := 1.25;
		spitterTorqueLimit : REAL := 10.0;
	END_STRUCT;
	cutter_typ : 	STRUCT 
		command : cutter_ext_command_typ;
		reading : cutter_readings_typ;
		setting : cutter_settings_typ;
	END_STRUCT;
	physical_cutter_inputs : 	STRUCT 
		eStop : BOOL;
		topCover : BOOL;
		frontDoorKnife : BOOL;
		frontDoor : BOOL;
		knifeModule : BOOL;
		feedButton : BOOL;
		printerTensionControl : BOOL;
		markReader1 : BOOL;
		markReader2 : BOOL;
		printerBlowerOn : BOOL;
		knifeOutfeedCrash1 : BOOL;
		knifeOutfeedCrash2 : BOOL;
		knifeOutfeedCrash3 : BOOL;
		eStopExt : BOOL;
		homePosMainKnife : BOOL;
		extractionCrash : BOOL;
		knifeOutCrashCount1 : UINT;
		knifeOutCrashCount2 : UINT;
		knifeOutCrashCount3 : UINT;
		webTensionInfeed : INT;
	END_STRUCT;
	physical_cutter_outputs : 	STRUCT 
		airStripEject : BOOL;
		slowDown : BOOL;
		pause : BOOL;
		ready : BOOL;
		reject : BOOL;
		fanOutput : BOOL;
		heaterRight : BOOL;
		heaterRear : BOOL;
		heaterFront : BOOL;
		heaterLeft : BOOL;
		granulatorStart : BOOL;
		antistatic : BOOL;
		cutMarkAbove0Below1 : BOOL;
		jobMarkAbove0Below1 : BOOL;
		vacuum : BOOL;
	END_STRUCT;
END_TYPE

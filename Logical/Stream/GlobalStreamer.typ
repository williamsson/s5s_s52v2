
TYPE
	streamer_typ : 	STRUCT 
		command : streamer_ext_command_typ;
		reading : streamer_readings_typ;
		setting : streamer_settings_typ;
	END_STRUCT;
	buttonFunct : 	STRUCT 
		btn1 : USINT;
		btn2 : USINT;
		btn3 : USINT;
		btn4 : USINT;
		btn5 : USINT;
	END_STRUCT;
	streamer_settings_typ : 	STRUCT 
		jogSpeed : REAL := 1;
		overlap : REAL := 29;
		lastSheetDist : REAL := 120;
		maxSpeedDeliveryInMaxSpeed : REAL := 0.75;
		maxSpeedDeliveryInSpeed : REAL := 1.06;
		offsetBlindDistance : REAL := 0.0;
		lastSheetStepPosition : REAL := 1.0;
		streamerDeliverySpeed : REAL := 0.46;
		sheetsUp : DINT := 2;
		simulateOffsetPage_Dev : DINT := 0;
		forkBackTapPos : REAL := 2.0;
		streamerSeq2Crash : REAL := 2.0;
		tagliaTorqueCrash : REAL := 2.0;
		tableDropDistance : REAL := 25; (*mm*)
		stackJoggerSpeed : REAL := 1.05;
		gapExtraFeed : REAL := 100;
		forkBackTap : BOOL := FALSE;
		offsetOnDeliver : BOOL := FALSE;
		stepSensorNo : DINT := 0;
		enableOffset : BOOL := TRUE;
		inputFilterStreamer : REAL;
		inputFilterAligner : REAL;
		jogger2Position : REAL;
		deliverOnlyInSouth : BOOL;
	END_STRUCT;
	streamer_readings_typ : 	STRUCT 
		button1 : colorOfButton;
		button2 : colorOfButton;
		button3 : colorOfButton;
		button4 : colorOfButton;
		button5 : colorOfButton;
		status : machineStatus_typ;
		lastSheetInStreamerSection2Pos : REAL;
		lastSheetInStreamerSection1Pos : REAL;
		deliverSheetAlignerExitPosToStr : REAL;
		overLapInStack : REAL;
		southRightAvg : REAL;
		southLeftAvg : REAL;
		northRightAvg : REAL;
		northLeftAvg : REAL;
		newOverLapDistToStack : REAL;
		deliverSheetToAlignerExitPos : REAL;
		deliverSheetPos : USINT;
		mainState : USINT;
		alignerSpeedAtDeliver : REAL;
		feedToStack : USINT;
		pagesSinceDeliver : UDINT;
		sheetsAfterEmptyStack : UDINT;
		tableStepWarning : BOOL;
		strSec2CurrentRaw : REAL;
		strSec2TorqueRaw : REAL;
		strSec2Torque : REAL;
		deliverStack : BOOL;
		forkTableTransaction : BOOL;
		lastRecivedOffset : BOOL; (*1 = south 0 = north*)
		showStreamerRollerPlateLT : BOOL;
		showStopPlateLT : BOOL;
		stackingOnForks : BOOL;
		lastSheetGap : REAL;
		instantDeliverOn : BOOL;
		tagliaCurrentRaw : REAL;
		tagliaTorqueRaw : REAL;
		tagliaTorque : REAL;
		lastSheetSpeed : REAL;
		glueConfirmButton : UINT;
		deliverOnTheFly : USINT;
		sheetsInStreamQueue : USINT;
		function : buttonFunct;
		southStartCnt : UDINT;
		northStartCnt : UDINT;
	END_STRUCT;
	streamer_ext_command_typ : 	STRUCT 
		pressButton1 : BOOL;
		pressButton2 : BOOL;
		pressButton3 : BOOL;
		pressButton4 : BOOL;
		pressButton5 : BOOL;
		stop : BOOL;
		errAck : BOOL;
		unLoad : BOOL;
		feedToStack : BOOL;
		confirmGlue : BOOL;
		manFeed : BOOL;
		offsetNorth : BOOL;
		offsetToggleNextMark : BOOL;
		offsetToggle : BOOL;
		offsetSouth : BOOL;
		load : BOOL;
	END_STRUCT;
END_TYPE

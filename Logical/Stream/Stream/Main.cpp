
#include <bur/plctypes.h>
#include <strMain.h>
#include <string.h> 

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

struct  sheet dummySheet;

unsigned long bur_heap_size = 0xFFFF; 

void    flyDeliver(void);
void    getCutterSpeed(void);
void    quarterSecondCycle(void);
void    buttonCommand(void);
void    setButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL    isButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
REAL    connectStreamer(BOOL reset, BOOL bInputSensor, REAL sensorDist, REAL alignerSpeed, REAL format, REAL overlapDistance, REAL cycleTime, REAL alignerOverSpeed);
BOOL    ready(void);
USINT   checkPendingError(void);
REAL    alignerSpeedCalculation(void);
void    runPaperTransport(void);
void    sheetSignal(void);
void    alignerEnter(void);
void    alignerExit(void);
void    cutterRunning(void);
REAL    overSpeedSimple(REAL v,REAL i,REAL o, BOOL s);
USINT   directHomeServos(void);
BOOL    realEqual (REAL valA, REAL valB, REAL tollerance);
REAL    stackerStep (REAL tblStp, REAL sensVal, REAL strSpd, REAL ovrLap, BOOL gapFeed);
void    deliverStack(void);
void    powerOffAll(void);
USINT   absoluteHomeServos(BOOL rst);
USINT   pageInTransportQueue (USINT a, struct sheet b);
BOOL    risingEdgeDebounce (BOOL input);
void    runStreamerSection2 (void);
void    runStreamerSection1 (void);
enum    sgqRetVal streamerGlueQueue(enum sgqFunc func);
void    runStackerY(void);
void    runFingerY(void); 
void    runFingerX(void);
REAL    stepAccDec(REAL m, REAL v, BOOL ff, REAL boost, REAL max, REAL min);
BOOL    load(void);
void    absoluteGoHome(void);
void    controlTableStepsButton(void);
void    runJoggers(void);
REAL    rabs(REAL a);
REAL    setMaxSpeed(void);
void    addError (USINT errorNo, USINT stopScenario, BOOL unload, UINT subType);
void    clearAllErrors(void); //TODO clear displayed error only..
void    runTaglia(void);
void    handleMaxStack(void);
void    resetAxisErrors(void);
REAL    highest(REAL a, REAL b, REAL c);
REAL    lowest(REAL a, REAL b, REAL c);
//void    stackerMarkReader(void); 
void    keepCoverLocked(void);
void    keepCoverUnLocked(void);
void    tablePressUnitControl(void);
void    manFeed(void);
BOOL    axisRunning(void);
void    debugger(USINT type, REAL r1, REAL r2, UDINT i1);
void    checkWasteBox (void);
void    handleGlueAir(void);
void    streamerOffsetSequence (void);
void    strSec2Crash(void);
void    tagliaCrash(void);
void    dbgTxt (char * str);
void    torqueCalculation (void);
REAL    mm(USINT axis); //returns distance last cycle, (mm)
void    histHandler1 (REAL value, REAL min, float REAL, BOOL res);
void    handleWarnings(void);
void    goToOffsetStartPos(void);
void    setStackExit(REAL pos,REAL start);
REAL    subSeqSlowSpeed (void);
REAL    subSeqMediumSpeed (void);
REAL    rmod (REAL in,REAL w_min, REAL w_max);

void setOutputs(void)
{
   ///
   static BOOL LastOffsetValve1; //stop plate (south plate) 0 down 1 up
   static BOOL LastOffsetValve2; //fingers (north plate)    1 down 0 up
   
   
   if (!streamer.setting.enableOffset)
   {
      output.offsetValve1 = 1; //up down stop plate  , 0 = down        1 = up
      output.offsetValve2 = 0; //up down hold fingers, 0 = up          1 = down
      output.offsetValve3 = 1; //in out hold fingers,  0 = extracted   1 = retracted
      output.offsetValve4 = 0; //io out streamer tbl,  0 = shorter     1 = longer
   }
   
   if (stacker.setting.forceOutputEnable.mainAirValve)
      physical.streamer.digital.out1 = stacker.reading.output.mainAirValve  = stacker.setting.forceOutputValue.mainAirValve;
   else physical.streamer.digital.out1 = stacker.reading.output.mainAirValve = output.mainAirValve;
   
   /*if (stacker.setting.forceOutputEnable.glueMarkReaderEnable)
      physical.streamer.digital.out3 = stacker.reading.output.glueMarkReaderEnable  = stacker.setting.forceOutputValue.glueMarkReaderEnable;
   else physical.streamer.digital.out3 = stacker.reading.output.glueMarkReaderEnable = output.glueMarkReaderEnable;*/
   
   if (stacker.setting.forceOutputEnable.antistatic)
      physical.streamer.digital.out5 = stacker.reading.output.antistatic  = stacker.setting.forceOutputValue.antistatic;
   else physical.streamer.digital.out5 = stacker.reading.output.offsetValveSpaghettiBelt2 = output.antistatic;
   
   if (stacker.setting.forceOutputEnable.enableAirGlue)
      physical.streamer.digital.out7 = stacker.reading.output.enableAirGlue  = stacker.setting.forceOutputValue.enableAirGlue;
   else physical.streamer.digital.out7 = stacker.reading.output.enableAirGlue = output.enableAirGlue;
   
   if (stacker.setting.forceOutputEnable.pressUnitStart)
      physical.streamer.digital.out9 = stacker.reading.output.pressUnitStart  = stacker.setting.forceOutputValue.pressUnitStart;
   else physical.streamer.digital.out9 = stacker.reading.output.pressUnitStart = output.pressUnitStart;
   
   if (stacker.setting.forceOutputEnable.offsetValve1)
      physical.streamer.digital.out11 = stacker.reading.output.offsetValve1  = stacker.setting.forceOutputValue.offsetValve1;
   else physical.streamer.digital.out11 = stacker.reading.output.offsetValve1 = output.offsetValve1;
   
   if (stacker.setting.forceOutputEnable.offsetValve2)
      physical.streamer.digital.out13 = stacker.reading.output.offsetValve2  = stacker.setting.forceOutputValue.offsetValve2;
   else physical.streamer.digital.out13 = stacker.reading.output.offsetValve2 = output.offsetValve2;
   
   if (stacker.setting.forceOutputEnable.offsetValve3)
      physical.streamer.digital.out15 = stacker.reading.output.offsetValve3  = stacker.setting.forceOutputValue.offsetValve3;
   else physical.streamer.digital.out15 = stacker.reading.output.offsetValve3 = output.offsetValve3;
   

   if (stacker.setting.forceOutputEnable.offsetValve4)
      physical.streamer.digital.out2 = stacker.reading.output.offsetValve4  = stacker.setting.forceOutputValue.offsetValve4;
   else physical.streamer.digital.out2 = stacker.reading.output.offsetValve4 = output.offsetValve4;
   
   if (stacker.setting.forceOutputEnable.offsetValve5)
      physical.streamer.digital.out4 = stacker.reading.output.offsetValve5  = stacker.setting.forceOutputValue.offsetValve5;
   else physical.streamer.digital.out4 = stacker.reading.output.offsetValve5 = output.offsetValve5;
   
   if (stacker.setting.forceOutputEnable.userOut1)
      physical.streamer.digital.out12 = stacker.reading.output.userOut1  = stacker.setting.forceOutputValue.userOut1;
   else physical.streamer.digital.out12 = stacker.reading.output.userOut1 = output.userOut1;
   
   if (stacker.setting.forceOutputEnable.userOut2)
      physical.streamer.digital.out14 = stacker.reading.output.userOut2  = stacker.setting.forceOutputValue.userOut2;
   else physical.streamer.digital.out14 = stacker.reading.output.userOut2 = output.userOut2;
   
   if (stacker.setting.forceOutputEnable.userOut3)
      physical.streamer.digital.out16 = stacker.reading.output.userOut3  = stacker.setting.forceOutputValue.userOut3;
   else physical.streamer.digital.out16 = stacker.reading.output.userOut3 = output.userOut3;
   
   /*if (stacker.setting.forceOutputEnable.userOut3)
      ebmGlue.setting.forceOutputValue. = stacker.reading.output.userOut3  = stacker.setting.forceOutputValue.userOut3;
   else physical.streamer.digital.out16 = stacker.reading.output.userOut3 = output.userOut3;
   
   teachGlueOffsetMarkSensor*/
   oldalignerOutfeedCnt1 = stacker.reading.input.alignerOutfeedCnt1;
   
   
   if (LastOffsetValve1 != stacker.reading.output.offsetValve1 && !stacker.reading.output.offsetValve1) //start time measurement
   {
      streamer.reading.southStartCnt++;
      southTimeDownRight = 0;
      southTimeDownLeft = 0;
      startSouthTimer = true;
   }
        
   if (LastOffsetValve2 != stacker.reading.output.offsetValve2 && stacker.reading.output.offsetValve2) //start time measurement
   {
      streamer.reading.northStartCnt++;
      northTimeDownRight = 0;
      northTimeDownLeft = 0;
      startNorthTimer = true;
   }

   LastOffsetValve1 = stacker.reading.output.offsetValve1;
   LastOffsetValve2 = stacker.reading.output.offsetValve2;
   
   
   
   
}


void inputFilter(BOOL * out, BOOL in, USINT inputNo, USINT flt, BOOL reverse)
{
   BOOL tIn;
   static USINT inOnCnt[50];
   static USINT inOffCnt[50];
   
   if (reverse) tIn = !in;
   else tIn = in;
   
   if (tIn && inOnCnt[inputNo] < 250) {
      inOnCnt[inputNo]++;
      inOffCnt[inputNo] = 0;
   }
   if (!tIn && inOffCnt[inputNo] < 250) {
      inOffCnt[inputNo]++;
      inOnCnt[inputNo] = 0;
   }
 
   if (inOnCnt[inputNo] > flt) *out = true;
   else if (inOffCnt[inputNo] > flt) *out = false;
}

void getInputs(void)
{
   
   static USINT gapon,gapoff;
   static USINT exton,extoff;
   
   static USINT hsmP;
   static REAL historySouthLeftMed[3],SouthLeftMed,SouthRightMed,historySouthRightMed[3];
   
   static USINT hnmP;
   static REAL historyNorthLeftMed[3],NorthLeftMed,NorthRightMed,historyNorthRightMed[3];
   
   //stacker.reading.input.enterTransport = inputFilter2(physical.streamer.digital.in9, 9, USINT(streamer.setting.inputFilterAligner/2.4), true);
   //inputFilter(&stacker.reading.input.enterTransport, physical.streamer.digital.in9, 9, 1, true);
      
   //inputFilter(&stacker.reading.input.alignerOutfeedDigital1, physical.streamer.digital.in41, 41, 2, true);
   //inputFilter(&stacker.reading.input.alignerOutfeedDigital2, physical.streamer.digital.in42, 42, 2, true);
   //inputFilter(&stacker.reading.input.alignerOutfeedDigital3, physical.streamer.digital.in43, 43, 2, true);
      
   //inputFilter(&stacker.reading.input.streamerGap1, physical.streamer.digital.in16, 16, 6, true);
   //inputFilter(&stacker.reading.input.streamerGap2, physical.streamer.digital.in16, 18, 6, true);
   //inputFilter(&stacker.reading.input.streamerGap3, physical.streamer.digital.in16, 20, 6, true);
      
   //inputFilter(&stacker.reading.input.exitStreamerSection1, physical.streamer.digital.in39, 39, 4, true);   
   //inputFilter(&stacker.reading.input.exitStreamerSection2, physical.streamer.digital.in26, 26, 4, true);   
   //inputFilter(&stacker.reading.input.exitStreamerSection3, physical.streamer.digital.in28, 28, 4, true);  
   
   
   if (physical.streamer.digital.in16 && gapon<250) {
      gapon ++;
      gapoff = 0;
   }
   if (!physical.streamer.digital.in16 && gapoff<250) {
      gapoff ++;
      gapon = 0;
   } 
   
   
   
   if (physical.streamer.digital.in39 && exton<250) {
      exton ++;
      extoff = 0;
   }
   if (!physical.streamer.digital.in39 && extoff<250) {
      extoff ++;
      exton = 0;
   }
   
   
   if (exton > 3) stacker.reading.input.exitStreamerSection1 = true;
   else if (extoff > 3) stacker.reading.input.exitStreamerSection1 = false;
   
   if (gapon > 5) stacker.reading.input.streamerGap1 = true;
   else if (gapoff > 5) stacker.reading.input.streamerGap1 = false;
   
   stacker.reading.input.enterTransport         = physical.streamer.digital.in9;  //s5x
   stacker.reading.input.alignerOutfeedDigital1 = physical.streamer.digital.in41; //s52 Streamer Only
   
   /*
   if (exton > 3) stacker.reading.input.exitStreamerSection1 = false;
   else if (extoff > 3) stacker.reading.input.exitStreamerSection1 = true;
   
   if (gapon > 5) stacker.reading.input.streamerGap1 = false;
   else if (gapoff > 5) stacker.reading.input.streamerGap1 = true;
   
   stacker.reading.input.enterTransport         = !physical.streamer.digital.in9;  //s5x
   stacker.reading.input.alignerOutfeedDigital1 = !physical.streamer.digital.in41; //s52 Streamer Only
   */
   //digital
   
   stacker.reading.input.wasteBoxWarning        = physical.streamer.digital.in11; //s5x
   stacker.reading.input.wasteBoxFull           = physical.streamer.digital.in13; //s5x
   stacker.reading.input.rackGate               = physical.streamer.digital.in15; //s5x
   stacker.reading.input.antistaticOk           = physical.streamer.digital.in17; //s5x
   stacker.reading.input.homePosJogger1         = physical.streamer.digital.in19; //s52 Streamer Only
   stacker.reading.input.homePosJogger2         = physical.streamer.digital.in21; //s52 Streamer Only
   stacker.reading.input.joggerStackClosed      = physical.streamer.digital.in23; // spare
   
   //stacker.reading.input.homeForkPush         = physical.streamer.digital.in10; // sets in HomeSetupStreamer
   //stacker.reading.input.homeForkLift         = physical.streamer.digital.in12; // sets in HomeSetupStreamer
   //stacker.reading.input.homeElevator         = physical.streamer.digital.in14; // sets in HomeSetupStreamer
   //stacker.reading.input.streamerGap1           = !physical.streamer.digital.in16; // s52 Streamer Only
   //stacker.reading.input.streamerGap2           = physical.streamer.digital.in18; // s52 Streamer Only
   //stacker.reading.input.streamerGap3           = physical.streamer.digital.in20; // s52 Streamer Only
   stacker.reading.input.homePosJoggerStack     = physical.streamer.digital.in22; // spare
   //                                           = physical.streamer.digital.in24; // place holder for home pos stop plate
   
   stacker.reading.input.tableClearSensor1      = physical.streamer.digital.in25; //s5x
   stacker.reading.input.tableClearSensor2      = physical.streamer.digital.in27; //s5x
   stacker.reading.input.tableClearSensor3      = physical.streamer.digital.in29; //s5x  
   stacker.reading.input.tableClearSensor4      = physical.streamer.digital.in31; //s5x
   stacker.reading.input.seqUnitNotReady        = physical.streamer.digital.in33; //s5x
   //                                           = physical.streamer.digital.in35; // spare
   //                                           = physical.streamer.digital.in37; // spare
   //stacker.reading.input.exitStreamerSection1   = !physical.streamer.digital.in39; //s52 Streamer Only
   
   //stacker.reading.input.exitStreamerSection2   = physical.streamer.digital.in26; //s52 Streamer Only
   //stacker.reading.input.exitStreamerSection3   = physical.streamer.digital.in28; //s52 Streamer Only
   stacker.reading.input.streamerCrashRight     = physical.streamer.digital.in30; //s52 Streamer Only
   stacker.reading.input.streamerCrashLeft      = physical.streamer.digital.in32; //s52 Streamer Only
   stacker.reading.input.manFeed                = physical.streamer.digital.in34; //s52 Streamer Only
   stacker.reading.input.userInput1             = physical.streamer.digital.in36; //s5x
   stacker.reading.input.userInput2             = physical.streamer.digital.in38; //s5x
   stacker.reading.input.userInput3             = physical.streamer.digital.in40; //s5x
   
   stacker.reading.input.streamerGate           = physical.streamer.digital.in45; //s52 Streamer Only
   stacker.reading.input.NorthStopPlateRight    = physical.streamer.digital.in47; // spare
   stacker.reading.input.NorthStopPlateLeft     = physical.streamer.digital.in49; // spare
   stacker.reading.input.SouthStopPlateRight    = physical.streamer.digital.in51; // spare
   stacker.reading.input.SouthStopPlateLeft     = physical.streamer.digital.in53; // spare
   //                                           = physical.streamer.digital.in55; // spare
   //                                           = physical.streamer.digital.in57; // spare
   //                                           = physical.streamer.digital.in59; // spare
   
   stacker.reading.input.pressUnitSensor        = physical.streamer.digital.in46; //s52 Streamer Only
   //                                           = physical.streamer.digital.in48; // spare
   //                                           = physical.streamer.digital.in50; // spare
   //                                           = physical.streamer.digital.in52; // spare
   //                                           = physical.streamer.digital.in54; // spare
   //                                           = physical.streamer.digital.in56; // spare
   //                                           = physical.streamer.digital.in58; // spare
   //                                           = physical.streamer.digital.in60; // spare
   
   
   
   
   
   
   
   //stacker.reading.input.alignerOutfeedDigital2 = physical.streamer.digital.in42; //s52 Streamer Only
   //stacker.reading.input.alignerOutfeedDigital3 = physical.streamer.digital.in43; //s52 Streamer Only
   stacker.reading.input.offsetMarkReader       = physical.streamer.digital.in44; //s52 Streamer Only Offset signal from EBM
   stacker.reading.input.alignerOutfeedCnt1     = physical.streamer.counter.cnt1; //s52 Streamer Only
   stacker.reading.input.alignerOutfeedCnt2     = physical.streamer.counter.cnt2; //s52 Streamer Only
   stacker.reading.input.alignerOutfeedCnt3     = physical.streamer.counter.cnt3; //s52 Streamer Only
   stacker.reading.input.offsetMarkReaderCnt    = physical.streamer.counter.cnt4;

   //safety
   stacker.reading.input.lightCurtain           = physical.streamer.digital.in7 && safety.reading.lightCurtainStatus; //2.s56	, safety software  
   stacker.reading.input.eStop                  = physical.streamer.digital.in5 && physical.streamer.digital.in2; //2.s12 , 1.s34
   stacker.reading.input.topCoverClosed         = physical.streamer.digital.in6; //2.s34
   stacker.reading.input.eStopExt               = physical.streamer.digital.in8; //2.s78
   //stacker.reading.input.alignerTopCoverClosed  = physical.streamer.digital.in1; //1.s12
   stacker.reading.input.stackerHatch           = physical.streamer.digital.in3; //1.s56
   
   stacker.reading.input.ebmEncoder             = ebmGlue.reading.input.in1;
   stacker.reading.input.glueOffsetMarkSensor   = ebmGlue.reading.input.in2;
   stacker.reading.input.pressureGuard          = ebmGlue.reading.input.in9;
  
   
   
   
   //analog
   //stacker.reading.input.AnalogStepSensor       = physical.streamer.analog.in1;
   
   if (streamer.setting.stepSensorNo == 0)  {
      stacker.reading.input.AnalogStepSensor       = physical.streamer.analog.in1;
   }
   if (streamer.setting.stepSensorNo == 1)  {
      stacker.reading.input.AnalogStepSensor       = physical.streamer.analog.in2;
   }
   if (streamer.setting.stepSensorNo == 2)  {
      stacker.reading.input.AnalogStepSensor       = (physical.streamer.analog.in1 + physical.streamer.analog.in2)/2;
   }
   
   
   if (magneticStopPlateSensor[0] == 0)
   {
      magneticStopPlateSensorTot  =  167060;// - (streamer.setting.enableOffset?5000:0); //164160
      magneticStopPlateSensorTot +=  (UDINT)(magneticStopPlateSensor[1] << 16);
      magneticStopPlateSensorTot +=  (UDINT)(magneticStopPlateSensor[2] << 8 );
      magneticStopPlateSensorTot +=  (UDINT)(magneticStopPlateSensor[3]);
      stacker.reading.input.streamerStopPlatePos = (REAL)(magneticStopPlateSensorTot)/1000.0;
      if (!streamer.setting.enableOffset) stacker.reading.input.streamerStopPlatePos += 10.0;
      else stacker.reading.input.streamerStopPlatePos -= 10.0;
   }
   else stacker.reading.input.streamerStopPlatePos = 999.9;
   
   magneticStrRollerConnected = false;
   if (magneticStrRollerStatus == 5) magneticStrRollerConnected = true;
   
   if (magneticStrRollerConnected)
   {
      magneticStrRollerSensorTot  =  0;// - (streamer.setting.enableOffset?5000:0); //164160
      magneticStrRollerSensorTot +=  (UDINT)(magneticStrRollerSensor[1] << 16);
      magneticStrRollerSensorTot +=  (UDINT)(magneticStrRollerSensor[2] << 8 );
      magneticStrRollerSensorTot +=  (UDINT)(magneticStrRollerSensor[3]);
      stacker.reading.input.streamerStrRollerPos = 334.75 - ((REAL)(magneticStrRollerSensorTot)/1000.0); //350
   }
   else stacker.reading.input.streamerStrRollerPos = 999.9;
   
   
   if (startNorthTimer) //add bounce check
   {
      if (!stacker.reading.input.NorthStopPlateRight) northTimeDownRight += MILLISECONDS_UNIT;
      if (!stacker.reading.input.NorthStopPlateLeft)  northTimeDownLeft += MILLISECONDS_UNIT;
      
      if (stacker.reading.input.NorthStopPlateLeft && stacker.reading.input.NorthStopPlateRight) {
         startNorthTimer = false;

         hnmP++;
         if (hnmP >= 3 ) hsmP=0; 
         historyNorthLeftMed[hnmP] = northTimeDownLeft;
         historyNorthRightMed[hnmP] = northTimeDownRight;
         
         NorthLeftMed = historyNorthLeftMed[0] + historyNorthLeftMed[1] + historyNorthLeftMed[2] -
            highest(historyNorthLeftMed[0],historyNorthLeftMed[1], historyNorthLeftMed[2]) - 
            lowest(historyNorthLeftMed[0],historyNorthLeftMed[1], historyNorthLeftMed[2]);
         
         NorthRightMed = historyNorthRightMed[0] + historyNorthRightMed[1] + historyNorthRightMed[2] -
            highest(historyNorthRightMed[0],historyNorthRightMed[1], historyNorthRightMed[2]) - 
            lowest(historyNorthRightMed[0],historyNorthRightMed[1], historyNorthRightMed[2]);
         
         historyNorthLeft[historyNorthP] = NorthLeftMed;
         historyNorthRight[historyNorthP] = NorthRightMed;
         
         streamer.reading.northLeftAvg = ((historyNorthLeft[0] + historyNorthLeft[1] + historyNorthLeft[2] + 
            historyNorthLeft[3] + historyNorthLeft[4] + historyNorthLeft[5] + 
            historyNorthLeft[6] + historyNorthLeft[7] + historyNorthLeft[8] + historyNorthLeft[9]) / 10) - 3.0;
         
         streamer.reading.northRightAvg = ((historyNorthRight[0] + historyNorthRight[1] + historyNorthRight[2] + 
            historyNorthRight[3] + historyNorthRight[4] + historyNorthRight[5] + 
            historyNorthRight[6] + historyNorthRight[7] + historyNorthRight[8] + historyNorthRight[9]) / 10) - 3.0;

         historyNorthP++;
         if (historyNorthP >= 10) historyNorthP = 0;
      }
   }
   
   if (startSouthTimer)
   {
      if (!stacker.reading.input.SouthStopPlateRight) southTimeDownRight += MILLISECONDS_UNIT;
      if (!stacker.reading.input.SouthStopPlateLeft)  southTimeDownLeft += MILLISECONDS_UNIT;
      
      if (stacker.reading.input.SouthStopPlateLeft && stacker.reading.input.SouthStopPlateRight) {
         startSouthTimer = false;
         hsmP++;
         if (hsmP >= 3 ) hsmP=0; 
         historySouthLeftMed[hsmP] = southTimeDownLeft;
         historySouthRightMed[hsmP] = southTimeDownRight;
         
         SouthLeftMed = (historySouthLeftMed[0] + historySouthLeftMed[1] + historySouthLeftMed[2] -
            highest(historySouthLeftMed[0],historySouthLeftMed[1], historySouthLeftMed[2]) - 
            lowest(historySouthLeftMed[0],historySouthLeftMed[1], historySouthLeftMed[2]));
         
         SouthRightMed = (historySouthRightMed[0] + historySouthRightMed[1] + historySouthRightMed[2] -
            highest(historySouthRightMed[0],historySouthRightMed[1], historySouthRightMed[2]) - 
            lowest(historySouthRightMed[0],historySouthRightMed[1], historySouthRightMed[2]) );
         
         historySouthLeft[historySouthP] = SouthLeftMed;
         historySouthRight[historySouthP] = SouthRightMed;
         
         streamer.reading.southLeftAvg = ((historySouthLeft[0] + historySouthLeft[1] + historySouthLeft[2] + 
            historySouthLeft[3] + historySouthLeft[4] + historySouthLeft[5] + 
            historySouthLeft[6] + historySouthLeft[7] + historySouthLeft[8] + historySouthLeft[9]) / 10) - 3.0;
         
         streamer.reading.southRightAvg = ((historySouthRight[0] + historySouthRight[1] + historySouthRight[2] + 
            historySouthRight[3] + historySouthRight[4] + historySouthRight[5] + 
            historySouthRight[6] + historySouthRight[7] + historySouthRight[8] + historySouthRight[9]) / 10) - 3.0;
         
         historySouthP++;

         
         if (historySouthP >= 10) historySouthP = 0;
      }
      
   }   
}

void _INIT ProgramInit(void)
{
   historySouthP = 0;
   historyNorthP = 0;
   mainTick = lastSecondTick = 0.0;
   stacker.reading.startUpSeq = 0;
   streamer.reading.mainState = 0;
   paperTransport[ALIGNER].acceleration = 10;
   paperTransport[ALIGNER].deceleration = 6;
   paperTransport[JOGGER1].acceleration = 5;
   paperTransport[JOGGER1].deceleration = 5;
   paperTransport[JOGGER2].acceleration = 5;
   paperTransport[JOGGER2].deceleration = 5;
   paperTransport[JOGGER3].acceleration = 5;
   paperTransport[JOGGER3].deceleration = 5;
   paperTransport[STREAMER_SECTION_1].acceleration = 5;
   paperTransport[STREAMER_SECTION_1].deceleration = 5;
   paperTransport[STREAMER_SECTION_2].acceleration = 5;
   paperTransport[STREAMER_SECTION_2].deceleration = 5;
   paperTransport[TAGLIA].acceleration = 5;
   paperTransport[TAGLIA].deceleration = 5;
   paperTransport[DELIVER].acceleration = 0.35;//0.5;//0.7;
   paperTransport[DELIVER].deceleration = 4.5;
   aligner.Parameter.Acceleration = paperTransport[ALIGNER].acceleration * 1000;
   aligner.Parameter.Deceleration = paperTransport[ALIGNER].deceleration * 1000;
   jogger1.Parameter.Acceleration = paperTransport[JOGGER1].acceleration * 1000;
   jogger1.Parameter.Deceleration = paperTransport[JOGGER1].deceleration * 1000;
   jogger2.Parameter.Acceleration = paperTransport[JOGGER2].acceleration * 1000;
   jogger2.Parameter.Deceleration = paperTransport[JOGGER2].deceleration * 1000;
   strSection1.Parameter.Acceleration = paperTransport[STREAMER_SECTION_1].acceleration * 1000;
   strSection1.Parameter.Deceleration = paperTransport[STREAMER_SECTION_1].deceleration * 1000;
   strSection2.Parameter.Acceleration = paperTransport[STREAMER_SECTION_2].acceleration * 1000;
   strSection2.Parameter.Deceleration = paperTransport[STREAMER_SECTION_2].deceleration * 1000;
   taglia.Parameter.Acceleration = paperTransport[TAGLIA].acceleration * 1000;
   taglia.Parameter.Deceleration = paperTransport[TAGLIA].deceleration * 1000;
   deliver.Parameter.Acceleration = paperTransport[DELIVER].acceleration * 1000;
   deliver.Parameter.Deceleration = paperTransport[DELIVER].deceleration * 1000;
   jogger1.Command.Power = FALSE;
   jogger2.Command.Power = FALSE;
   jogger3.Command.Power = FALSE;
   jogger2.Command.StopSlave = FALSE;
   strSection1.Command.Power = FALSE;
   strSection2.Command.Power = FALSE;
   aligner.Command.Power = FALSE;
   taglia.Command.Power = FALSE;
   deliver.Command.Power = FALSE;
   strFingerPusher.Parameter.Acceleration = 2000;
   strFingerPusher.Parameter.Deceleration = 2000;   
   strFingerPusher.Parameter.Velocity = 1500;
   strFingerPusher.Parameter.JogVelocity = 400;
   stackerY.acceleration = 3.0; //2.0
   stackerY.deceleration = 2.0; //2.0
   stackerY.setSpeed = 0.5;
   forkY.acceleration = 2.0;//1.8; //2.0
   forkY.deceleration = 2.0;//2.0
   forkY.setSpeed = 0.5;
   forkX.acceleration = FORKX_ACC;
   forkX.deceleration = FORKX_DEC;
   forkX.setSpeed = FORKX_SPD;
   joggers.masterClosePos = 44;
   joggers.masterOpenPos = 6.0;
   streamer.reading.sheetsAfterEmptyStack = 0;
   manFeedStreamSpeed = 0.15;
   global_dbg.real2 = 0;
   globalTimeTick = 0.0;
   streamerGlueQueue(resetQueue);
   streamerOffset.waitToRetractSheets = 2.0; 
   streamerOffset.waitToExtendStream = 0.5;
   streamerOffset.timeToLiftFingers = 200;
   streamer.setting.streamerSeq2Crash = 2.0;
   stacker.reading.stackExit = false;
   reHomeAbsolute = false;
   cleanGlueStandStillTime = 1000000;
   streamer.reading.glueConfirmButton = HIDE;
}

void automaticSettings(void)
{
   streamer.setting.streamerDeliverySpeed = 0.28 + buffer.reading.infeedSpeed*0.53;
   constrain(&streamer.setting.streamerDeliverySpeed,0.35,0.55);   
}

void _CYCLIC ProgramCyclic(void)
{
   static BOOL newSheet;
   static REAL meter;
   static REAL decimeters;
   static BOOL stopSlave;
   static UDINT lastMinute;
   static REAL rejectDistance;
   static BOOL lastStreamerGap;
   
   //
   //static REAL secTick;
   //secTick += SECONDS_UNIT;
   /*rstCnt4 = false;
   if (secTick >= 1.0)
   {
   secTick -= 1.0;
   pulseFreq = ((REAL)(physical.streamer.counter.cnt4) + pulseFreq*1.5)/2.5;
   rstCnt4 = true;
   }*/
   //
   
   
   stacker.reading.currentStackSize = 0;
   
   lineController.setup.streamer.jogger3 = STACK_JOGGER_PRESSENT;
   
   if (triggerTime>0) 
   {
      triggerTime -= SECONDS_UNIT;
      triggerScope = true;
   }
   else triggerScope = false;
   
   output.userOut1 = lineController.reading.userOutput.stacker._1.status;
   output.userOut2 = lineController.reading.userOutput.stacker._2.status;
   
   if (!stacker.reading.input.eStop && lineController.reading.timeSinceStart > 180) addError(E_STOP,NORMAL_STOP,TRUE,0);
   
   if (paperTransport[DELIVER].setSpeed == 0.0) stacker.reading.stackExit = false;
   
   if (globalPower)
   {
      output.mainAirValve = true;
      if ((globalTimeTick - airOnTime) > 10000 && !stacker.reading.input.pressureGuard) //check for low air pressure
      {
         addError(LOW_AIR_PRESSURE,NORMAL_STOP,TRUE,0);
         airOnTime = globalTimeTick;
      }
   }
   else {
      output.mainAirValve = false;
      airOnTime = globalTimeTick;
   }
  

   if (FALSE == lineController.setup.streamer.installed) return;
   
   tick++;
   globalTimeTick += MILLISECONDS_UNIT;
   
   if (streamer.reading.deliverSheetPos != 0)
      paperTransport[ALIGNER].acceleration = 5.0;
   else 
      paperTransport[ALIGNER].acceleration = cutter.setting.masterAcc/700;
   
  

   stacker.reading.status.warning = false;
   //stacker.reading.stepSensorValue = ((REAL)(stacker.reading.input.AnalogStepSensor) - 16383)/5461;
   stacker.reading.stepSensorValue = ((REAL)(stacker.reading.input.AnalogStepSensor) - 16383)/3650;
   
   
   getCutterSpeed();
   buttonCommand();
   getInputs();
   checkWasteBox();
   handleWarnings();
  
   if (magneticStrRollerConnected) //
   {
      
      if (!stacker.reading.input.topCoverClosed)
      {
         if (rabs(stacker.reading.input.streamerStopPlatePos - cutter.reading.sheetSize) > 0.8)
         {
            if (!streamer.reading.showStopPlateLT) lineController.command.doubleBeep = true;
            streamer.reading.showStopPlateLT = true;
            streamer.reading.showStreamerRollerPlateLT = false;
            wasStopPlateLT = 6.0;
         }
         else if (wasStopPlateLT>0)
         {
            wasStopPlateLT -= SECONDS_UNIT;
            streamer.reading.showStopPlateLT = true;
            streamer.reading.showStreamerRollerPlateLT = false;
         }
         else if (rabs(stacker.reading.input.streamerStrRollerPos - cutter.reading.sheetSize) > 1.75)
         {
            if (!streamer.reading.showStreamerRollerPlateLT) lineController.command.doubleBeep = true;
            streamer.reading.showStreamerRollerPlateLT = true;
            streamer.reading.showStopPlateLT = false;
            wasStreamRollLT = 10.0;
         }
         else if (wasStreamRollLT > 0)
         {
            wasStreamRollLT -= SECONDS_UNIT;
            streamer.reading.showStreamerRollerPlateLT = true;
            streamer.reading.showStopPlateLT = false;
         }
         else
         {
            streamer.reading.showStreamerRollerPlateLT = false;
            streamer.reading.showStopPlateLT = false;
         }
      }
      else {
         wasStreamRollLT = 0.60;
         wasStopPlateLT = 0.12;
         streamer.reading.showStreamerRollerPlateLT = false;
         streamer.reading.showStopPlateLT = false;
      }
   }
   else
   {   
      streamer.reading.showStreamerRollerPlateLT = false;
      if (rabs(stacker.reading.input.streamerStopPlatePos - cutter.reading.sheetSize)>1.0 && !stacker.reading.input.topCoverClosed/* && lastTopCoverClosed*/)
      {
         if (!streamer.reading.showStopPlateLT) lineController.command.doubleBeep = true;
         streamer.reading.showStopPlateLT = true;
      }
      if (stacker.reading.input.topCoverClosed) streamer.reading.showStopPlateLT = false;
   }
  
   
   
   
   stacker.reading.transportPages = pageInTransportQueue(GET_NO_PAGES_IN_QUEUE);
   stacker.reading.axisStatus.forkElevator.pos = forkY.position;
   stacker.reading.axisStatus.stackerElevator.pos = stackerY.position;
   stacker.reading.axisStatus.transport.actualSpeed = aligner.Status.ActVelocity;
   
   if (measureGapTime) streamer.reading.lastSheetGap += SECONDS_UNIT;
   
   if (!lastStreamerGap && stacker.reading.input.streamerGap1) stacker.reading.gapTime = 0.0;
   if (stacker.reading.input.streamerGap1) stacker.reading.gapTime += paperTransport[STREAMER_SECTION_2].isSpeed * SECONDS_UNIT;
   
   lastStreamerGap = stacker.reading.input.streamerGap1;
   
   meter += cutter.reading.webSpeed * SECONDS_UNIT;
   decimeters += cutter.reading.webSpeed * SECONDS_UNIT;
   
   if (!stacker.reading.input.stackerHatch && STARTED) {
      addError(STACKER_HATCH,NORMAL_STOP,TRUE,0);
   }
   
   if (decimeters > 0.1)
   {
      dTotal += 0.1;
      decimeters -= 0.1;
   }
   
   if (meter>1.0)
   {
      stacker.reading.tripCounter.meters++;
      meter -= 1.0;
   }
   
   if (tick%500 == 0)
   {
      if (stacker.reading.tripCounter.minutes>0) {
         if (lastMinute != stacker.reading.tripCounter.minutes) // new minute
         {
            stacker.reading.tripCounter.avgSpeed = (REAL)(stacker.reading.tripCounter.meters) / (REAL)(stacker.reading.tripCounter.minutes);
            if (cutter.reading.setSpeed < 0.05 && cutter.reading.webSpeed < 0.1 && paperTransport[STREAMER_SECTION_2].setSpeed < 0.025)
               ebmGlue.command.sendAll = TRUE;
         }
      }
      else {
         stacker.reading.tripCounter.avgSpeed = buffer.reading.infeedSpeed * 60;
      }
      lastMinute = stacker.reading.tripCounter.minutes;
   }

   if (stacker.reading.transportPages > (INFEED_TO_END_SENSOR / (DINT)(cutter.reading.currentPage.sheetLength)))
   {
      if (pageInTransportQueue(FORWARD_PEAK_PAGE) != DELIVER_PAGE && pageInTransportQueue(PEEK_PAGE) != DELIVER_PAGE)
      {
         //lineController.command.trippleBeep = true;
         //repair queue
         global_dbg.real2 = global_dbg.real2 + 1.0; 
         pageInTransportQueue(REMOVE_LAST_PAGE);
         debugger(5,cutter.reading.webSpeed,global_dbg.real2,stacker.reading.transportPages);
      }
   }
   if (stacker.reading.transportPages > 0 && inSpeed.is > 0 && cutter.reading.output.reject)
   {
      rejectDistance += paperTransport[ALIGNER].isSpeed * SECONDS_UNIT;
      if (rejectDistance>10.0) {
         pageInTransportQueue(INIT_QUEUE);
         lineController.command.trippleBeep = true;
      }
   }
   else rejectDistance = 0;
   
   stacker.reading.stackSize = stacker.setting.topPosOffset - stackerY.position;
   
   if (streamer.reading.stackingOnForks) {
      stacker.reading.stackUsage = ((stacker.setting.fingerHightAdjust - forkY.position)*100) / stacker.setting.minStackSize;
   }
   else if (0 == streamer.reading.deliverOnTheFly && 3 != streamer.reading.feedToStack) {
      stacker.reading.stackUsage = (stacker.reading.stackSize * 100) / stacker.setting.minStackSize;
   }
   
   mainStateTime += SECONDS_UNIT;
   if (lastMainState != streamer.reading.mainState)
      mainStateTime = 0;
   lastMainState = streamer.reading.mainState;
   mainTick += SECONDS_UNIT;
   if ((mainTick-lastSecondTick) > 0.25){
      lastSecondTick = mainTick;
      quarterSecondCycle();
   }
   
   if (!stacker.reading.input.eStop || !stacker.reading.input.eStopExt)
   {
      if (!safety.reading.initialized) safety.command.eStopReset = TRUE;
      else
      {
         streamer.reading.mainState = ERROR;
      }
   }
   
   if (stacker.reading.startUpSeq < 15) streamer.reading.mainState = INIT;
   
   if (stacker.command.resetCounters) {
      stacker.reading.tripCounter.jobs = 0;
      stacker.reading.tripCounter.pages = 0;
      stacker.reading.tripCounter.meters = 0;
      stacker.reading.tripCounter.minutes = 0;
      stacker.reading.tripCounter.avgSpeed = 0;
      minute = 0;
      ebmGlue.command.resetCnt = TRUE;
      stacker.command.resetCounters = FALSE;
   }
  
   if (streamer.reading.mainState == RUN && paperTransport[ALIGNER].isSpeed < (stacker.setting.transportIdleSpeed-0.01) && inSpeed.is == 0.0)
   {
      if (checkPendingError()) streamer.reading.mainState = ERROR;
   }
   else if (streamer.reading.mainState != ERROR)
   {
      if (checkPendingError()) streamer.reading.mainState = ERROR;
   }
   
   if (streamer.reading.mainState == RUN) {
      
      output.antistatic = true;
   }
   else {
      //output.glueMarkReaderEnable = false;
      output.antistatic = false;
      enterAlignerCrash = 0;
      exitAlignerCrash = 0;
      if (streamer.reading.mainState != STOPPED) {
         if (cutter.command.deliverNextRequest) dbgTxt("1 delNext FALSE");
         cutter.command.deliverNextRequest = false;
         deliverRequest.state = afterDelivery;
      }
      stacker.command.instantDeliver = false;
      
   }
   
   if (streamer.reading.mainState == RUN || streamer.reading.mainState == STOPPED) maxSpeed.ready = MAX_SPEED;
   else if (mainStateTime > 2.0 && streamer.reading.mainState == INIT){
      maxSpeed.ready = 0.0;  
      maxSpeed.makeGap = MAX_SPEED;
   }
   
   if (streamer.reading.mainState != STOPPED) maxSpeed.stopped = MAX_SPEED;
   
   if (streamer.reading.mainState != ERROR) {
      maxSpeed.error = MAX_SPEED;
      stopImmediately = FALSE;
      ErroResetCnt = 0;
   }
   
   maxSpeed.alignerTopCover = MAX_SPEED;
   /*
   if (stacker.reading.input.alignerTopCoverClosed)
      maxSpeed.alignerTopCover = MAX_SPEED;
   else {
      stacker.reading.status.warning = true;
      maxSpeed.alignerTopCover = 0.8;
   }
   */
   if (stacker.reading.warningCode != 0)
      stacker.reading.status.warning = true;
   
   if (!globalPower && streamer.reading.mainState != INIT) noPower += SECONDS_UNIT;
   else noPower = 0;
   
   if (noPower > 2.0 && streamer.reading.mainState != ERROR) streamer.reading.mainState = INIT;
   
   
   switch (streamer.reading.mainState)
   {
      case 0: //init
         keepCoverUnLocked();
         goClosed = FALSE;
         waitForCutterToStop = FALSE;
         goBackToStopped = FALSE;
         stacker.reading.status.state = Status_NotLoaded;
         stacker.reading.status.stopped = false;
         if (stacker.reading.input.manFeed == 0) SET_SERVOSPEED_TO_ZERO;
         runPaperTransport();
         dirHomeErrorCnt[0] = dirHomeErrorCnt[1] = dirHomeErrorCnt[2] = dirHomeErrorCnt[3] = 0;
         dirHomeErrorCnt[4] = dirHomeErrorCnt[5] = dirHomeErrorCnt[6] = dirHomeErrorCnt[7] = 0;
         //if (axisRunning() && mainStateTime > 2.0) runPaperTransport(); //untested
         if (streamer.command.load) streamer.reading.mainState = STATE_LOAD;
         break;
      case 1: //load
         keepCoverLocked();
         goClosed = FALSE;
         if (mainStateTime == 0.0) homingState = 0;
         stacker.reading.status.state = Status_Loading;
         stacker.reading.status.stopped = false;
         if (load()) streamer.reading.mainState = RUN;
         if (stacker.reading.transportPages>0) pageInTransportQueue(INIT_QUEUE);
         break;
      case 2: //run
         subSeqSlowSpeed(); //sets the machine to slow speed if the table is full and a deliver sheet is in the stream
         subSeqMediumSpeed(); //sets the machine to medium speed if the table is full for more than 12 seconds
         if (streamer.reading.deliverOnTheFly == 0)
            stacker.reading.currentStackSize = stacker.setting.topPosOffset - stackerY.position;
         
         keepCoverLocked();
         stacker.reading.status.state = Status_Ready;
         stacker.reading.status.stopped = false;
         goClosed = FALSE;
         cutterStandStill += SECONDS_UNIT;
         if (inSpeed.is>0.0) cutterStandStill = 0;
         
         if (!ready()) streamer.reading.mainState = ERROR;
         
         if (streamer.command.unLoad) {
            streamer.reading.mainState = INIT;
         }
         if (streamer.command.stop) {
            waitForCutterToStop = TRUE;
            mainStateTime = 1.0;
            cutter.command.stop = TRUE;
         }
         if (waitForCutterToStop)
         {
            if (/*inSpeed.is == 0.0*/ cutterStandStill > 2.0) {
               if (streamer.reading.deliverOnTheFly != 0) {
                  streamer.reading.deliverOnTheFly = 0;
                  streamer.reading.mainState = INIT;
               }
               else   
                  streamer.reading.mainState = STOPPED;
               dbgTxt("wait for cutter to stop inSpeed 0");
               
            }
            if (mainStateTime>4.5) {
               if (streamer.reading.deliverOnTheFly != 0) {
                  streamer.reading.deliverOnTheFly = 0;
                  streamer.reading.mainState = INIT;
               }
               else   
                  streamer.reading.mainState = STOPPED;
               dbgTxt("wait for cutter to stop state time 3.5 s");
            }
            
 
            
         }
         
         break;
      case 3: //error
         keepCoverUnLocked();
         waitForCutterToStop = FALSE;
         goBackToStopped = FALSE;
         goClosed = FALSE;
         stacker.reading.status.state = Status_Error;
         stacker.reading.status.stopped = false;
 
         //if (mainStateTime>3.5) {
            SET_SERVOSPEED_TO_ZERO;
         //}
         
         if (checkPendingError() <= NORMAL_STOP && mainStateTime < 3.5) {
            paperTransport[ALIGNER].setSpeed = (highest(cutter.reading.webSpeed,cutter.reading.setSpeed,0.0) * 1.3) + stacker.setting.transportIdleSpeed;
         }
         
         runPaperTransport();
         maxSpeed.error = 0;

         if (streamer.command.errAck && lastErrAck != streamer.command.errAck) ErroResetCnt = 100;
         if (ErroResetCnt) {
            if (100 == ErroResetCnt) {
               streamer.reading.deliverOnTheFly = 0;
               streamer.reading.deliverSheetPos = 0;
               if (machineError.errType == JOGGER2_HOMEFAULT) {
                  jogger2.Command.Stop = true;
                  stopSlave = true;
               }
               clearAllErrors();
            }
            
            if (0 == ErroResetCnt%10) {
               safety.command.resetStackerLightCurtain = true;
               if (safety.reading.resetPossible) safety.command.eStopReset = true;
               resetAxisErrors();
            }
            if (1 == ErroResetCnt)
            {
               streamer.reading.mainState = INIT;
               streamer.command.errAck = FALSE;
               if (!strFingerPusher.AxisState.StandStill) strFingerPusher.Command.Power = true;
               if (stopSlave) jogger2.Command.StopSlave = true;
               stopSlave = false;
            }
            ErroResetCnt--;
         }
         break;
      case 4: //stopped
         keepCoverUnLocked();
         waitForCutterToStop = FALSE;
         stacker.reading.status.state = Status_Ready;
         stacker.reading.status.stopped = true;
         streamer.reading.deliverSheetPos = 0; //reset delivery
         maxSpeed.makeGap = MAX_SPEED;
         
         if (stacker.reading.transportPages>0 && stacker.reading.input.alignerOutfeedDigital1 && stacker.reading.input.enterTransport) pageInTransportQueue(INIT_QUEUE);
         if (streamer.command.unLoad) {
            streamer.reading.mainState = INIT;
            absoluteGoHome();
         }
         if (mainStateTime>1.5) {
            maxSpeed.stopped = 0;
            
            if (unLoadWhenCoverCloses && stacker.reading.input.topCoverClosed) {
               streamer.reading.mainState = INIT;
               unLoadWhenCoverCloses = false;
            }
            
            if  (!streamer.command.manFeed) feedSlowTime = 2.2;
            
            if (streamer.command.manFeed) {
               if (feedSlowTime > 0) {
                  paperTransport[STREAMER_SECTION_2].setSpeed = 0.015;
                  paperTransport[STREAMER_SECTION_1].setSpeed = 0.015;
                  feedSlowTime -= SECONDS_UNIT;
               }
               else {
                  paperTransport[STREAMER_SECTION_2].setSpeed = manFeedStreamSpeed;
                  paperTransport[STREAMER_SECTION_1].setSpeed = manFeedStreamSpeed;
               }
              
               if (!stacker.reading.input.topCoverClosed) {
                  goClosed = true;
                  unLoadWhenCoverCloses = true;
               }
            }
            else if (!stacker.reading.input.topCoverClosed && goClosed) 
            {
               if ((jogger1.Status.ActPosition < (joggers.masterClosePos-0.2)) || (jogger1.Status.ActPosition > (joggers.masterClosePos+0.2)))
               {
                  paperTransport[STREAMER_SECTION_2].setSpeed = 0.010;
                  paperTransport[STREAMER_SECTION_1].setSpeed = 0.010;
               }
               else 
               {
                  paperTransport[STREAMER_SECTION_2].setSpeed = 0.00;
                  paperTransport[STREAMER_SECTION_1].setSpeed = 0.00;
                  goClosed = false;
               }   
            }
            else if (feedTaglia && !stacker.reading.input.topCoverClosed)
            {
               paperTransport[TAGLIA].setSpeed = 0.1;
            }
            else if (streamer.command.feedToStack && stacker.reading.input.topCoverClosed)
            {
               //keepCoverLocked();
               //if (!ready()) streamer.reading.mainState = ERROR;
               streamer.reading.mainState = RUN;
            }
            else SET_SERVOSPEED_TO_ZERO;
         }         
         runPaperTransport();
         break;
   }
      
   streamer.command.stop = FALSE;
   streamer.command.unLoad = FALSE;
   streamer.command.load = FALSE;
   
   if (streamer.reading.mainState == INIT || streamer.reading.mainState == STATE_LOAD || streamer.reading.mainState == ERROR) streamer.command.feedToStack = FALSE;
   else if (cutter.reading.setSpeed > 0 && !cutter.reading.output.reject) streamer.command.feedToStack = FALSE;
   
   stacker.reading.maximumSpeed = setMaxSpeed();
   
   joggers.speed = paperTransport[STREAMER_SECTION_2].setSpeed * streamer.setting.jogSpeed;
   if (streamer.reading.mainState == ERROR || streamer.reading.mainState == INIT) {
      joggers.run = false;
   }
   else if (joggers.ready) {
      joggers.run = true;
      joggers.close = false;
      joggers.open = false;
   }
   
   runStackerY();
   runFingerY(); 
   runFingerX();
   runJoggers();
   setOutputs();
   controlTableStepsButton();
   
   lastErrAck = streamer.command.errAck;
   newSheet = cutter.reading.sheetSignal;
   //lastMarkReader = stacker.reading.input.glueMarkReader;
   lastSheetSignal = cutter.reading.sheetSignal; 
   if (cutter.reading.webSpeed > 0.05 && !cutter.reading.output.reject && paperTransport[ALIGNER].setSpeed>0 && streamer.reading.mainState == RUN)
      stackerStreaming = true; //this flag is used by deliverNext button
   else
      stackerStreaming = false;
   if (cutter.reading.output.reject) pagesAfterReject = 0;
   tablePressUnitControl();  
   lastTopCoverClosed = stacker.reading.input.topCoverClosed;
   handleGlueAir();
   streamerOffsetSequence();
}


void handleGlueAir(void)
{
   static REAL turnOff1;
   static REAL airPressureFaultTimer;
   static REAL glueEmptyTimer;
 
   /* open and close the glue guns */
   if (ebmGlue.setting.glueGun.enable && paperTransport[STREAMER_SECTION_2].isSpeed>0.005 && !stacker.reading.input.streamerGap1)
   {
      turnOff1 = 3.0;
   }
   
   if (turnOff1 > 0.0) {
      turnOff1 -= SECONDS_UNIT;
      output.enableAirGlue = true;
   }
   else output.enableAirGlue = false;
   
   /* check the pressure */
   /*
   if (!ebmGlue.reading.input.in6 && globalPower) {
   airPressureFaultTimer -= SECONDS_UNIT;
   if (airPressureFaultTimer < 0.0) {
   addError(GLUE_AIR_PRESSURE,NORMAL_STOP,TRUE,0);
   airPressureFaultTimer = 120.0;
   }
   }
   else airPressureFaultTimer = 5.0; 
   */
   
   if (paperTransport[STREAMER_SECTION_2].setSpeed < 0.02)
      cleanGlueStandStillTime += SECONDS_UNIT;
   else if (cleanGlueStandStillTime < GLUE_WARNING_TIMEOUT)
      cleanGlueStandStillTime = 0;
   
   /* check glue tank */
   if (ebmGlue.setting.glueGun.pattern != 0)
   {
      if (ebmGlue.reading.input.in8) glueEmptyTimer += SECONDS_UNIT * (cutter.reading.webSpeed > 0.1 ? 1.0:0.25);
      else glueEmptyTimer = 0;
      if (glueEmptyTimer > 300) {
         addError(GLUE_EMPTY,NORMAL_STOP,TRUE,0);
         glueEmptyTimer = 0;
      }
      if (!globalPower) glueEmptyTimer = 0;
   }
   else glueEmptyTimer = 0;
   
}

void resetAxisErrors (void)
{
   if (jogger1.Status.ErrorID>0) jogger1.Command.ErrorAcknowledge = true;
   if (jogger3.Status.ErrorID>0) jogger3.Command.ErrorAcknowledge = true;
   if (jogger2.Status.ErrorID>0) jogger2.Command.ErrorAcknowledge = true;
   if (strSection1.Status.ErrorID>0) strSection1.Command.ErrorAcknowledge = true;
   if (strSection2.Status.ErrorID>0) strSection2.Command.ErrorAcknowledge = true;
   if (aligner.Status.ErrorID>0) aligner.Command.ErrorAcknowledge = true;
   if (taglia.Status.ErrorID>0) taglia.Command.ErrorAcknowledge = true;
   if (deliver.Status.ErrorID>0) deliver.Command.ErrorAcknowledge = true;
   if (strFingerLift.Status.ErrorID >0) strFingerLift.Command.ErrorAcknowledge = true;
   if (strStackerLift.Status.ErrorID >0) strStackerLift.Command.ErrorAcknowledge = true;
   if (strFingerPusher.Status.ErrorID >0) strFingerPusher.Command.ErrorAcknowledge = true;
   jogger2.Command.StopSlave = false;
   jogger2.Command.MoveJogNeg = false;
   jogger2.Command.MoveJogPos = false;
   jogger1.Command.MoveJogNeg = false;
   jogger1.Command.MoveJogPos = false;
   jogger2.Command.StartSlave = false;
   strFingerPusher.Command.Power = false;
   strFingerPusher.Command.MoveAbsolute = false;
}

REAL setMaxSpeed(void)
{
   REAL tmp;
   
   tmp = MAX_SPEED;
   if (maxSpeed.stopped < tmp)  {
      tmp = maxSpeed.stopped;
      stacker.reading.lowestID=0;
   }
   if (maxSpeed.error < tmp)   {
      tmp = maxSpeed.error;
      stacker.reading.lowestID=1;
   }
   if (maxSpeed.ready < tmp)    {
      tmp = maxSpeed.ready;
      stacker.reading.lowestID=2;
   }
   if (maxSpeed.standStillDeliver < tmp) {
      tmp = maxSpeed.standStillDeliver;
      stacker.reading.lowestID=3;
   }
   if (maxSpeed.makeGap < tmp) {
      tmp = maxSpeed.makeGap;
      stacker.reading.lowestID=4;
   }
   if (maxSpeed.alignerTopCover < tmp) {
      tmp = maxSpeed.alignerTopCover;
      stacker.reading.lowestID=5;
   }
   return tmp;
}

void powerOffAll(void)
{
   strSection1.Command.Power = FALSE;
   strSection2.Command.Power = FALSE;
   aligner.Command.Power = FALSE;
   taglia.Command.Power = FALSE;
   deliver.Command.Power = FALSE;
}

void _EXIT ProgramExit(void)
{
   USINT x;
   jogger2.Command.StopSlave = TRUE;
   for (x=0;x<200;x++);
   powerOffAll();
}

void deliverStack(void)
{
   static REAL timer;
   static REAL freeTime;
   
   if (streamer.reading.deliverOnTheFly != 0) streamer.command.feedToStack = FALSE;
   
   
   switch (streamer.reading.feedToStack)
   {
      case 0:
         //idle
         if (streamer.command.feedToStack)
         {
            deliverRequest.state = afterDelivery;
            streamer.reading.feedToStack = 1;
            streamer.command.feedToStack = FALSE;
            streamer.reading.deliverSheetPos = 0;
            pagesSinceDeliver = 10;
         }
         timer = 0;
         break;
      case 1:
         paperTransport[DELIVER].setSpeed = 0;
         timer += SECONDS_UNIT;
         StoppedDelivEmptyStrTries = 0;
         if (timer > 2.0) {
            streamer.reading.feedToStack = 2;
            timer = 0.0;
         }
         break;
      case 2:
         paperTransport[DELIVER].setSpeed = 0;
         if (stacker.reading.input.streamerGap1) timer += SECONDS_UNIT*2;
         else timer += SECONDS_UNIT;
         
         if (timer > 6.0) {
            if (!stacker.reading.input.streamerGap1 && StoppedDelivEmptyStrTries<5) {
               StoppedDelivEmptyStrTries++; 
               timer = 5.0;
            }
            else if (!stacker.reading.input.streamerGap1 && StoppedDelivEmptyStrTries == 5) {
               addError(ENTER_STACK_GAP_SENSOR,NORMAL_STOP,TRUE,0);
            }
            else
            {
               streamer.reading.feedToStack = 3;
               if (stacker.setting.deliveryPosition == 0) stackerY.goHome = true;
               else stackerY.absMove = stacker.setting.deliveryPosition;
               timer = 0.0;
            }
         }
         deliverStartPostion = paperTransport[DELIVER].position;
         freeTime = 0.0;
         break;
      case 3:
         timer += SECONDS_UNIT;
         stackToPress = true;
         goToOffsetStartPos();
         if (timer > 0.04) paperTransport[DELIVER].setSpeed = stacker.setting.deliverTransportSpeed;
         if (timer > (1.000/paperTransport[DELIVER].setSpeed))
         {
            if (stacker.reading.input.tableClearSensor1 && stacker.reading.input.tableClearSensor2 && stacker.reading.input.tableClearSensor3 && stacker.reading.input.tableClearSensor4)
            {
               freeTime += SECONDS_UNIT; 
               if (stacker.reading.input.lightCurtain && freeTime>1.0)
               {
                  streamer.reading.feedToStack = 4;
                  paperTransport[DELIVER].setSpeed = 0;
                  stackerY.absMove = stacker.setting.topPosOffset;
                  forkY.absMove = stacker.setting.fingerHightAdjust;
                  forkX.goHome = true;
                  //if (goBackToStopped) {
                  //   streamer.command.stop = FALSE;
                  //   goBackToStopped = FALSE;
                  //   streamer.reading.mainState = STOPPED;
                  //}
               }
            }  
         }
         if (timer>10.0) {
            addError(TABLE_CLEAR,NORMAL_STOP,TRUE,100);
            streamer.reading.feedToStack = 0;
         }
         setStackExit(paperTransport[DELIVER].position,deliverStartPostion);
         pageInTransportQueue(INIT_QUEUE);
         break;
      case 4:
         timer += SECONDS_UNIT;
         if (timer > 2.0 || stackerY.position > (stacker.setting.topPosOffset - 3)) { 
            streamer.reading.feedToStack = 0;
            mainStateTime = 3.5;
            if (goBackToStopped) {
               streamer.command.stop = true;
               goBackToStopped = false;
            }
         }
         break;
   }
   if (streamer.reading.feedToStack != 0) maxSpeed.standStillDeliver = 0.0;
   else  maxSpeed.standStillDeliver = MAX_SPEED;
}

void handleDeliverNextAndMaxStack (void)
{
   if (deliverRequest.lastState != deliverRequest.state) {
      if (deliverRequest.state == 0) dbgTxt("2 delNext FALSE");
      deliverRequest.sheetsInState = 0;
      deliverRequest.lastState = deliverRequest.state;
   }
   
   if (deliverRequest.deliverNowPressed)  {
      deliverRequest.state = deliverNow;
      deliverRequest.deliverNowPressed = false;
      deliverRequest.deliverNowPresses++;
   }
   
   switch (deliverRequest.state)
   {
      case afterDelivery: //0
         cutter.command.deliverNextRequest = false;
         //if (deliverRequest.sheetsInState > (stacker.setting.minStackSize/(stacker.setting.tableSteps*2.5))) deliverRequest.state = sleeping; 
         if (deliverRequest.sheetsInState > 30) deliverRequest.state = sleeping;
         break;
      case sleeping://1
         
         
         if (streamer.setting.deliverOnlyInSouth)
         {
            //deliver only when last recived offset was a south offset.
            if (stacker.reading.stackUsage > 100 &&  streamer.reading.lastRecivedOffset) deliverRequest.state = deliverNext;
         }
         else
         {
            if (stacker.reading.stackUsage > 100)  deliverRequest.state = deliverNext;
         }

         if (deliverRequest.deliverNextPressed)  {
            deliverRequest.state = deliverNext;
            deliverRequest.deliverNextPresses++;
         }
         break;
      case deliverNext: //2 
         cutter.command.deliverNextRequest = true;
         dbgTxt("1 delNext TRUE");
         deliverRequest.state = afterDeliverNext;
         break;
      case deliverNow: //3
         cutter.command.deliverNextRequest = false;
         dbgTxt("3 delNext FALSE");
         if (true == cutter.command.deliverNowRequestToggle) cutter.command.deliverNowRequestToggle = false;
         else cutter.command.deliverNowRequestToggle = true;
         
         lineController.command.beep = TRUE;
         
         /*if (((stacker.reading.stackSize > (stacker.setting.maxStackSize + 25)) || (stacker.reading.stackUsage > 215)) && streamer.reading.deliverOnTheFly == 0) {
            deliverRequest.state = afterDeliverNowX2;
         }*/
         if ((stacker.reading.stackSize > (stacker.setting.maxStackSize + 25))|| (stacker.reading.stackUsage > 215)) {
            deliverRequest.state = afterDeliverNowX2;
         }   
            
         else                                                                 deliverRequest.state = afterDeliverNow;
         break;
      case afterDeliverNext: //4 (wait on deliver next)
         if (stacker.reading.stackSize > stacker.setting.maxStackSize) deliverRequest.state = deliverNow; 
         break;
      case afterDeliverNow: //5 (wait on deliver now)
         //if (deliverRequest.sheetsInState > 20 && streamer.reading.deliverOnTheFly == 0) deliverRequest.state = deliverNow; //one more time 
         if (deliverRequest.sheetsInState > 20) deliverRequest.state = deliverNow; //one more time 
         break;
      case afterDeliverNowX2: //6 (some type of error state)
         if (deliverRequest.sheetsInState > 15)
            addError(OVER_MAX_STACK,NORMAL_STOP,TRUE,0); 
         break;
   }
   deliverRequest.deliverNextPressed = false;
}

void checkAxisError (void)
{   
   if (aligner.Status.ErrorID != 0) addError(AXIS_ALIGNER,NORMAL_STOP,TRUE,aligner.Status.ErrorID);
   if (strSection1.Status.ErrorID != 0) addError(AXIS_STREAMER_SECTION_1,NORMAL_STOP,TRUE,strSection1.Status.ErrorID);
   //if (strSection2.Status.ErrorID == 1) strSection2.Command.ErrorAcknowledge = true;
   if (strSection2.Status.ErrorID != 0) addError(AXIS_STREAMER_SECTION_2,NORMAL_STOP,TRUE,strSection2.Status.ErrorID);
   if (jogger1.Status.ErrorID != 0) addError(AXIS_JOGGER1,NORMAL_STOP,TRUE,jogger1.Status.ErrorID);
   if (jogger2.Status.ErrorID != 0) addError(AXIS_JOGGER2,NORMAL_STOP,TRUE,jogger2.Status.ErrorID);
   if (jogger3.Status.ErrorID != 0 && lineController.setup.streamer.jogger3) addError(AXIS_JOGGER3,NORMAL_STOP,TRUE,jogger3.Status.ErrorID);
   if (taglia.Status.ErrorID != 0) addError(AXIS_TAGLIA,NORMAL_STOP,TRUE,taglia.Status.ErrorID);
   if (deliver.Status.ErrorID != 0) addError(AXIS_DELIVER,NORMAL_STOP,TRUE,deliver.Status.ErrorID); 
}

BOOL ready (void)
{
   USINT errorStopTyp;
   errorStopTyp = checkPendingError();
   if (errorStopTyp > NORMAL_STOP)  {
      SET_SERVOSPEED_TO_ZERO;   
      runPaperTransport();
      return 0;
   }
   else if (NORMAL_STOP == errorStopTyp && inSpeed.is == 0) return 0;
   else
   {
      if (errorStopTyp == NORMAL_STOP) {
         cutter.command.stop = true;
         errorWaitForStop += SECONDS_UNIT;
      }
      sheetSignal();   //0,4 SHEET_SIGNAL,DELIVERY
      alignerEnter();  //1 ALIGNER_ENTER
      alignerExit();   //2 ALIGNER_EXIT
      cutterRunning(); //3 CUTTER_RUNNING
      deliverStack(); //setsMaximumSpeed to 0
      paperTransport[ALIGNER].setSpeed = alignerSpeedCalculation();
      runStreamerSection1(); //make gap, overlap
      runStreamerSection2(); //increase gap, starts delivery, tableSteps, deliverBelt
      flyDeliver();
      runTaglia();
      tagliaCrash();
      strSec2Crash();
      //handleMaxStack();
      handleDeliverNextAndMaxStack();
      //stackerMarkReader();
      checkAxisError();
      if (streamer.reading.mainState == STOPPED) {
         SET_SERVOSPEED_TO_ZERO;
      }
      if (0 == streamer.reading.feedToStack && 0 == streamer.reading.deliverOnTheFly)
      {
         //if (runDeliverBeltFirstSheet == false) runBeltExtra
         
         if (runDeliverBeltFirstSheet) paperTransport[DELIVER].setSpeed = paperTransport[STREAMER_SECTION_2].isSpeed*1.02;
         else paperTransport[DELIVER].setSpeed = 0;   
      }
      
      if (feedStreamer && inSpeed.is == 0.2) 
      {
         paperTransport[STREAMER_SECTION_2].setSpeed = manFeedStreamSpeed;
         paperTransport[STREAMER_SECTION_1].setSpeed = manFeedStreamSpeed;
      }
      
      runPaperTransport();

      if (streamer.reading.deliverSheetPos || streamer.reading.deliverOnTheFly) {
         maxSpeed.makeGap = highest(buffer.reading.infeedSpeed * streamer.setting.maxSpeedDeliveryInSpeed,(cutter.setting.infeedRunSpeed/1000)*streamer.setting.maxSpeedDeliveryInMaxSpeed,2.0);
         if (streamer.reading.deliverOnTheFly == 4 || streamer.reading.deliverOnTheFly == 5) maxSpeed.makeGap += sheetsOnForks * 0.0020;
         else if (sheetsOnForks > 5) maxSpeed.makeGap += sheetsOnForks * 0.0055;         
         
         
         
         //maxSpeed.makeGap = buffer.reading.infeedSpeed * streamer.setting.maxSpeedDeliveryInSpeed;
         if ((streamer.reading.deliverOnTheFly <= 6) && (streamer.reading.deliverOnTheFly >= 4))
         {
            lowest(buffer.reading.infeedSpeed * streamer.setting.maxSpeedDeliveryInSpeed,(cutter.setting.infeedRunSpeed/1000)*streamer.setting.maxSpeedDeliveryInMaxSpeed,buffer.reading.infeedSpeed);
         }
      }
      else maxSpeed.makeGap = MAX_SPEED;  //done, speed up
   }

   if (rabs(stacker.reading.input.streamerStopPlatePos - cutter.reading.sheetSize) > 1.2)
   {
      addError(STOP_PLATE_MOVED,NORMAL_STOP,TRUE,0);
   }
   
   if (rabs(stacker.reading.input.streamerStrRollerPos - cutter.reading.sheetSize) > 2.5 && magneticStrRollerConnected)
   {
      addError(STREAMER_ROLLER_MOVED,NORMAL_STOP,TRUE,0);
   }
   
   if (stacker.reading.input.topCoverClosed && !stacker.reading.input.joggerStackClosed && lineController.setup.streamer.jogger3) 
   {
      addError(STACK_JOGGERS_NOT_CLOSED,NORMAL_STOP,TRUE,0);
   }
   /*
   if (streamer.reading.northLeftAvg > 60 || streamer.reading.northRightAvg > 60 || northTimeDownRight > 80 || northTimeDownLeft > 80 )
   {
      addError(HOLD_FINGERS_TIMING_ERROR,NORMAL_STOP,TRUE,0);
   }
   
   if (streamer.reading.southLeftAvg > 60 || streamer.reading.southRightAvg > 60 || southTimeDownRight > 80 || southTimeDownLeft > 80 )
   {
      addError(SOUTH_STOPPLATE_TIMING_ERROR,NORMAL_STOP,TRUE,0);
   }
   */
   return 1;
}
/*
void stackerMarkReader(void) 
{
   if (streamer.setting.simulateOffsetPage_Dev>0)
   {
      if (lastMarkReader != stacker.reading.input.glueMarkReader && !stacker.reading.input.glueMarkReader) //falling edge
      {
         if (pgCntGMR) pgCntGMR--;
         else {
            goBlindDist = (streamer.setting.overlap + 7.0);
            pgCntGMR = streamer.setting.simulateOffsetPage_Dev;
         }
      }
      if (goBlindDist>0.0)
      {
         goBlindDist -= paperTransport[STREAMER_SECTION_2].isSpeed * MILLISECONDS_UNIT;
         output.glueMarkReaderEnable = false;
         //ebmGlue.command.mute = true; 
      }
      else
      {

      }
      
   }
   else {
      if (dontGlue[0] || dontGlue[1] || dontGlue[2])
      {
         output.glueMarkReaderEnable = false;
         //ebmGlue.command.mute = true;
      }
      else 
      {
         output.glueMarkReaderEnable = true;
         //ebmGlue.command.mute = false;
      }
   }
}
*/

void setStackExit(REAL pos,REAL start)
{
  if (pos > (start + (400.0 - cutter.reading.sheetSize))) stacker.reading.stackExit = true;
}

void runTaglia(void)
{
   if (streamer.reading.deliverOnTheFly == 1 || streamer.reading.deliverOnTheFly == 2 || deliverFly.startDeliverDist > 0)
      paperTransport[TAGLIA].setSpeed = (paperTransport[STREAMER_SECTION_2].setSpeed * stacker.setting.spaghettiOverSpeed * 1.05) + 0.05;
   else if (streamer.reading.deliverSheetPos)   
      paperTransport[TAGLIA].setSpeed = (paperTransport[STREAMER_SECTION_2].setSpeed * stacker.setting.spaghettiOverSpeed * 1.025) + 0.03;
   else
      paperTransport[TAGLIA].setSpeed = paperTransport[STREAMER_SECTION_2].setSpeed * stacker.setting.spaghettiOverSpeed*0.98 + 0.03;
   
   if (paperTransport[STREAMER_SECTION_2].setSpeed == 0) paperTransport[TAGLIA].setSpeed = 0; 

}

void flyDeliver (void)
{
   static USINT lastState;
   static REAL tableClearCnt;
   deliverFly.doFlyStateTime += SECONDS_UNIT;
   if (lastState != streamer.reading.deliverOnTheFly) deliverFly.doFlyStateTime = 0;
   
  /* if (streamer.reading.deliverOnTheFly > 1 && stacker.reading.input.streamerGap1) dontGlue[1] = true;
   else dontGlue[1] = false;
 */
   if (streamer.reading.deliverOnTheFly != 0 && streamer.reading.deliverOnTheFly<5) deliverRequest.state = afterDelivery;
   
   switch (streamer.reading.deliverOnTheFly)
   {
      case 0: //idle
         streamer.reading.deliverStack = false;
         streamer.reading.forkTableTransaction = false;
         streamer.reading.stackingOnForks = false;
         break;
      case 1: //table down
         streamer.reading.instantDeliverOn = false;
         stacker.reading.lastStackSize = stacker.setting.topPosOffset - stackerY.position;
         if (streamer.setting.forkBackTapPos > 0)
            forkX.absMove = -4.0;//streamer.setting.forkBackTapPos-4.0;
         stackerY.acceleration = 1.0; //1.5
         if (stacker.setting.deliveryPosition == 0) stackerY.goHome = true;
         else stackerY.absMove = stacker.setting.deliveryPosition;
         if (stacker.reading.input.seqUnitNotReady) {
            streamer.reading.deliverOnTheFly = 2;
            stackerCounter.onTheFlyDeliveries++;
         }
         if (deliverFly.doFlyStateTime > 3.0) addError(SUB_SEQ_NOT_READY,NORMAL_STOP,TRUE,0);
         lineController.command.beep = true;
         deliverStartPostion = paperTransport[DELIVER].position;
         forkY.absMove = stacker.setting.fingerHightAdjust; //should be here but prevents odd situations..
         sheetsOnForks = 0;
         break;
      case 2: //finger out,start delivery table
         streamer.reading.forkTableTransaction = false;
         streamer.reading.deliverStack = false;
         /********  Do Offset *******/
         if (streamer.setting.offsetOnDeliver) streamer.command.offsetToggle = true; 
         /***************************/
         //if (deliverFly.doFlyStateTime > 0.060) //0.6
         if (deliverFly.doFlyStateTime < 0.04) { //move forks in
            if (streamer.setting.forkBackTapPos != 0) forkX.absMove = -4.0; //streamer.setting.forkBackTapPos-4.0;
         }
         if (deliverFly.doFlyStateTime > 0.08) //set stacking on forks flag
         {
            forkY.acceleration = 1.5;
            forkY.deceleration = 1.0;
            streamer.reading.stackingOnForks = true;
         }
         if (deliverFly.doFlyStateTime > 0.17) //start delivery table belts (test "leaning stack cure" was 0.16)
            paperTransport[DELIVER].setSpeed = stacker.setting.deliverTransportSpeed;
         if (deliverFly.doFlyStateTime > 0.18) //setup and leave state (test "leaning stack cure" was 0.17)
         {
            //forkX.acceleration = 10.0 + paperTransport[STREAMER_SECTION_2].setSpeed*2.0;
            forkX.acceleration = 1.0 + paperTransport[STREAMER_SECTION_2].setSpeed;
            forkX.deceleration = 10.0 + paperTransport[STREAMER_SECTION_2].setSpeed*2.0;
            //forkX.setSpeed = 0.1 + (inSpeed.is*0.025) + paperTransport[STREAMER_SECTION_2].setSpeed*1.05;
            forkX.setSpeed = highest(paperTransport[STREAMER_SECTION_2].setSpeed*1.01,0.5,0.0);
            
            //forkX.absMove = (cutter.reading.currentPage.sheetLength - 20);
            forkX.absMove = (cutter.reading.currentPage.sheetLength + 17.0);
            streamer.reading.deliverOnTheFly = 3;
            stackerY.acceleration = 3.0;
            tableClearCnt = 0.8/stacker.setting.deliverTransportSpeed + 0.5;
            //if (streamer.setting.enableOffset) goToOffsetStartPos();
         }
         break;
      case 3: //stacking on fingers, deliver stack
         
         if ((forkX.position > 130.0) && liftFrontPageFlag)
         {
            liftFrontPageFlag = false;
            forkY.absMove = stacker.setting.fingerHightAdjust;
         } 
         //if (paperTransport[DELIVER].position > deliverStartPostion + (400 - cutter.reading.sheetSize)) stacker.reading.stackExit = true;
         setStackExit(paperTransport[DELIVER].position,deliverStartPostion);
         streamer.reading.deliverStack = true;
         streamer.reading.forkTableTransaction = false;
         streamer.reading.stackingOnForks = true;
         stackToPress = true;
         if (!stacker.reading.input.tableClearSensor1 && tableClearCnt<1.0) tableClearCnt = 1.0;
         if (!stacker.reading.input.tableClearSensor2 && tableClearCnt<1.0) tableClearCnt = 1.0;
         if (!stacker.reading.input.tableClearSensor3 && tableClearCnt<1.0) tableClearCnt = 1.0;
         if (!stacker.reading.input.tableClearSensor4 && tableClearCnt<1.0) tableClearCnt = 1.0;
         if (!stacker.reading.input.lightCurtain && tableClearCnt<0.5) tableClearCnt = 0.5;
         tableClearCnt -= SECONDS_UNIT;
         if (tableClearCnt < 0.0) {
            streamer.reading.deliverOnTheFly = 4;
            paperTransport[DELIVER].setSpeed = 0;
         }
         break;
      case 4: //transaction start move
         streamer.reading.deliverStack = false;
         streamer.reading.forkTableTransaction = true;
         streamer.reading.stackingOnForks = true;
         deliverFly.targetPos = stackerY.absMove = stacker.setting.topPosOffset 
            - (stacker.setting.fingerHightAdjust - forkY.position) 
            + stacker.setting.StackerFingerZeroAdjust 
            - stacker.setting.tableSteps * 2 * paperTransport[STREAMER_SECTION_2].isSpeed;
         streamer.reading.deliverOnTheFly = 5;
         if (deliverFly.targetPos > stacker.setting.topPosOffset) deliverFly.targetPos = stackerY.absMove = stacker.setting.topPosOffset;
         break;
      case 5: //transaction moving
         streamer.reading.deliverStack = false;
         streamer.reading.forkTableTransaction = true;
         streamer.reading.stackingOnForks = true;
         if (deliverFly.targetPos > (stackerY.position-1.5) && deliverFly.targetPos < (stackerY.position + 1.0)) {
            streamer.reading.deliverOnTheFly = 6;
            streamer.reading.stackingOnForks = false;
         }
         forkX.acceleration = FORKX_ACC;
         forkX.deceleration = FORKX_DEC;
         forkX.setSpeed     = FORKX_SPD;
         forkY.acceleration = 2.2;
         forkY.deceleration = 2.0;
         break;
      case 6: //move done, retract fingers
         if (deliverRequest.state == afterDelivery) deliverRequest.state=sleeping;
         streamer.reading.deliverStack = false;
         streamer.reading.forkTableTransaction = true;
         streamer.reading.stackingOnForks = false;
         forkY.move = -6.0 - stacker.setting.StackerFingerZeroAdjust;
         streamer.reading.deliverOnTheFly = 7;
         break;
      case 7: //move done, retract fingers
         streamer.reading.deliverStack = false;
         streamer.reading.forkTableTransaction = true;
         streamer.reading.stackingOnForks = false;
         if (deliverFly.doFlyStateTime > 0.1)
         {
            forkY.move = - stacker.setting.tableSteps;
            forkX.goHome = true;
            streamer.reading.deliverOnTheFly = 8;
         }
         strDistDelFlyState8 = 0.0;
         break;
      case 8: //move done, retract fingers
         streamer.reading.deliverStack = false;
         streamer.reading.forkTableTransaction = true;
         streamer.reading.stackingOnForks = false;
         if (!stacker.reading.input.streamerGap1)
         strDistDelFlyState8 += paperTransport[STREAMER_SECTION_2].isSpeed * MILLISECONDS_UNIT;
         if (strDistDelFlyState8 > streamer.setting.overlap*0.9)
         {
            strDistDelFlyState8 = 0.0;
            forkY.move = -stacker.setting.tableSteps;
         }  
         /*if (deliverFly.doFlyStateTime > 0.05 && (deliverFly.doFlyStateTime > 0.05 + SECONDS_UNIT)) forkY.move = -stacker.setting.tableSteps;
         if (deliverFly.doFlyStateTime > 0.10 && (deliverFly.doFlyStateTime > 0.10 + SECONDS_UNIT)) forkY.move = -stacker.setting.tableSteps;
         if (deliverFly.doFlyStateTime > 0.15 && (deliverFly.doFlyStateTime > 0.15 + SECONDS_UNIT)) forkY.move = -stacker.setting.tableSteps;
         if (deliverFly.doFlyStateTime > 0.20 && (deliverFly.doFlyStateTime > 0.20 + SECONDS_UNIT)) forkY.move = -stacker.setting.tableSteps;
         if (deliverFly.doFlyStateTime > 0.25 && (deliverFly.doFlyStateTime > 0.25 + SECONDS_UNIT)) forkY.move = -stacker.setting.tableSteps;*/
         if (forkX.position < 3.0 && forkX.position > -10.0) streamer.reading.deliverOnTheFly = 9;
         break;
      case 9: //move fingers back
         forkY.absMove = stacker.setting.fingerHightAdjust;
         streamer.reading.deliverOnTheFly = 0;
         streamer.reading.forkTableTransaction = false;
         streamer.reading.stackingOnForks = false;
         sheetsOnForks = 0;
         break;
   }
   lastState = streamer.reading.deliverOnTheFly;
}

REAL adjustStartupPages (void)
{
   REAL retVal = 0;
   if (pagesSinceDeliver < 3) {
      retVal = 15;
   }
   else if (pagesSinceDeliver < 5)
   {
      retVal = 10;
   }
   else if (pagesSinceDeliver < 7)
   {
      retVal = 7.5;
   }
   else if (pagesSinceDeliver < 10)
   {
      retVal = 5.0;
   }
   else if (pagesSinceDeliver < 20)
   {
      retVal = 2.5;
   }
   else if (pagesSinceDeliver < 30)
   {
      retVal = 1.25;
   }
   else
     retVal = 0.0;
   //guard
   if (retVal > streamer.setting.overlap * 0.5) retVal = streamer.setting.overlap * 0.5;
   
   return retVal;
}

void runStreamerSection1 (void)
{
   static USINT crashCnt;
   static USINT crashCode;
   //cutter.reading.sheetSize;
   
   if (streamer.reading.feedToStack == 2 && cutter.reading.sheetSize > 220)
   {
      paperTransport[STREAMER_SECTION_1].setSpeed = 0.25; // this is just an temporary fix.  something that works all the time needs to be done here, this was done to optimize LSI quickly.
   }
   else
   { 
      /*paperTransport[STREAMER_SECTION_1].setSpeed = connectStreamer(0, 
         stacker.reading.input.alignerOutfeedDigital1, 
         END_SENSOR_TO_STREAMINPUT, 
         (paperTransport[ALIGNER].isSpeed*4 + paperTransport[ALIGNER].setSpeed)/5, 
         cutter.reading.sheetSize, 
         streamer.setting.overlap * (inSpeed.is<stacker.setting.transportIdleSpeed?0.8:1) - (pagesSinceDeliver<10 ? (pagesSinceDeliver<3? 10.0 : 5.0):0.0) , 
         MILLISECONDS_UNIT, 
         stacker.setting.transportTableSpeed);*/
      
      paperTransport[STREAMER_SECTION_1].setSpeed = connectStreamer(0, 
         stacker.reading.input.alignerOutfeedDigital1, 
         END_SENSOR_TO_STREAMINPUT, 
         (paperTransport[ALIGNER].isSpeed*4 + paperTransport[ALIGNER].setSpeed)/5, 
         cutter.reading.sheetSize, 
         streamer.setting.overlap * (inSpeed.is<stacker.setting.transportIdleSpeed?0.8:1) - adjustStartupPages(), 
         MILLISECONDS_UNIT, 
         stacker.setting.transportTableSpeed);
      
      
      if (extraGap > 0.0)
      {
         if (!stacker.reading.input.alignerOutfeedDigital1) extraGap = 0.0;
         else extraGap -= paperTransport[STREAMER_SECTION_1].isSpeed * MILLISECONDS_UNIT;
         
         paperTransport[STREAMER_SECTION_1].setSpeed = (streamer.setting.streamerDeliverySpeed + paperTransport[STREAMER_SECTION_1].setSpeed) / 2;
      }
      
   }
   if (streamer.reading.deliverSheetPos == 2 &&  streamer.reading.deliverSheetAlignerExitPosToStr>(END_SENSOR_TO_STREAMINPUT+20)) //make gap
   {
      paperTransport[STREAMER_SECTION_1].setSpeed = streamer.setting.streamerDeliverySpeed;
      streamer.reading.lastSheetInStreamerSection1Pos +=  paperTransport[STREAMER_SECTION_1].isSpeed * MILLISECONDS_UNIT; 
      extraGap = streamer.setting.gapExtraFeed;
      if (!streamer.reading.instantDeliverOn)
      {
         if (streamer.reading.lastSheetInStreamerSection1Pos > cutter.reading.currentPage.sheetLength )
            streamer.reading.deliverSheetPos = 3;
      }
      else
      {
         if (streamer.reading.lastSheetInStreamerSection1Pos < 55.0 && paperTransport[STREAMER_SECTION_1].setSpeed > (streamer.setting.streamerDeliverySpeed*0.4)) paperTransport[STREAMER_SECTION_1].setSpeed = streamer.setting.streamerDeliverySpeed*0.4;
         if (streamer.reading.lastSheetInStreamerSection1Pos > 600.0 )  //empty section 1  
            streamer.reading.deliverSheetPos = 3;
      }
   
      
      
      /*
      if (streamer.reading.lastSheetInStreamerSection1Pos > (streamer.reading.instantDeliverOn ? 600:cutter.reading.currentPage.sheetLength) )   
         streamer.reading.deliverSheetPos = 3;
      */
   }
   else {
      streamer.reading.lastSheetInStreamerSection1Pos = 0;
      //maxSpeed.makeGap = MAX_SPEED;
   }
   
   
   
   
   if (!stacker.reading.input.streamerCrashLeft && !stacker.reading.input.streamerCrashRight) crashCnt = 0;
   if (stacker.reading.input.streamerCrashLeft) crashCnt++;
   if (stacker.reading.input.streamerCrashRight) crashCnt++;
   if (!stacker.reading.input.streamerGate) {
      crashCnt = 0;
      addError(STREAMER_GATE,NORMAL_STOP,1,0);
   }
   if (crashCnt>10) {
      if (stacker.reading.input.streamerCrashLeft) {
         crashCode = 1;
         if (stacker.reading.input.streamerCrashRight) crashCode = 3;
      }
      else crashCode = 2;
      addError(STREAMER_CRASH,NORMAL_STOP,1,crashCode);
      //streamer.command.stop = true;
   }
   //cutter.reading.output.reject;
   if (inSpeed.is > 0.1 && paperTransport[STREAMER_SECTION_1].setSpeed < streamer.setting.streamerDeliverySpeed/5 && pagesSinceDeliver < 4) {
      if (paperTransport[STREAMER_SECTION_1].setSpeed == 0 && stacker.reading.input.alignerOutfeedDigital1 && stacker.reading.input.exitStreamerSection1)
         paperTransport[STREAMER_SECTION_1].setSpeed = 0.0075;
      else
      {
         paperTransport[STREAMER_SECTION_1].setSpeed = 0.045 + buffer.reading.infeedSpeed*0.0535;
         constrain(&paperTransport[STREAMER_SECTION_1].setSpeed,0.015,0.075);
      }
   }
   //was like this.. worked fine, except that it somtimes runned to far after the white sheets..
   /*if (inSpeed.is > 0.1 && paperTransport[STREAMER_SECTION_1].setSpeed < streamer.setting.streamerDeliverySpeed/5 && pagesSinceDeliver < 4) {
   paperTransport[STREAMER_SECTION_1].setSpeed = 0.045 + buffer.reading.infeedSpeed*0.0535;
   constrain(&paperTransport[STREAMER_SECTION_1].setSpeed,0.015,0.075);
   }*/
   
   //if (pagesSinceDeliver < 3) lineController.command.beep = true;
   if (tick%2==0) torqueCalculation();
}

void runStreamerSection2 (void)
{
   static BOOL lastSensor;
   static REAL paperEdgeToAnalogStepSensor;
   static REAL paperAtAnalogStepSensor;
   static REAL lastOverLap;
   static REAL currentOverlap;
   static REAL stepSensorError;
   static REAL maxRunDistance;
   //static REAL startDeliverDist = -1.0;
   
   if (maxRunDistance>0) maxRunDistance -= paperTransport[STREAMER_SECTION_2].setSpeed * MILLISECONDS_UNIT;;
   
   if (streamer.reading.newOverLapDistToStack<10000) streamer.reading.newOverLapDistToStack += paperTransport[STREAMER_SECTION_2].setSpeed * MILLISECONDS_UNIT;
   if (lastOverLap != streamer.setting.overlap) {
      currentOverlap = lastOverLap;
      streamer.reading.newOverLapDistToStack = 0;
   }
   if (streamer.reading.sheetsAfterEmptyStack == 0) streamer.reading.newOverLapDistToStack = 1000;
   
   if (paperTransport[STREAMER_SECTION_1].setSpeed == streamer.setting.streamerDeliverySpeed) fastFeed = true;
   else if (streamer.reading.deliverSheetPos == 3 && stacker.reading.input.exitStreamerSection1) fastFeed = true; 
   else fastFeed = false;
   
   if (stacker.reading.input.streamerGap1) {//paper enter stream
      paperEdgeToAnalogStepSensor = cutter.reading.currentPage.sheetLength - 26;
      if (streamer.reading.deliverSheetPos != 3) {
         runDeliverBeltFirstSheet = true;
         maxRunDistance = stacker.reading.input.streamerStopPlatePos+15.0;
      }
   }
   if (paperEdgeToAnalogStepSensor>0) {
      paperEdgeToAnalogStepSensor -= paperTransport[STREAMER_SECTION_2].setSpeed * MILLISECONDS_UNIT;
      paperAtAnalogStepSensor = 20 + (streamer.setting.overlap * 4);
   }
   else {
      if (paperAtAnalogStepSensor>0) {
         paperAtAnalogStepSensor -= paperTransport[STREAMER_SECTION_2].setSpeed * MILLISECONDS_UNIT;
         if (stacker.reading.stepSensorValue > (stacker.setting.tableSteps/4)) paperAtAnalogStepSensor=-20.0;
      }
      else {
         if (streamer.reading.newOverLapDistToStack < 1300)
            streamer.reading.overLapInStack = currentOverlap;
         else 
            streamer.reading.overLapInStack = streamer.setting.overlap;
         if (streamer.reading.overLapInStack < 5) streamer.reading.overLapInStack = streamer.setting.overlap;
         if ((stacker.setting.topPosOffset-stackerY.position)>(stacker.setting.tableSteps*0.25)) {
            //if (streamerOffset.state == StreamerOffsetInNorth) runBeltExtra = 25.0;
            //else runBeltExtra = 10.0;
            if (rabs(stacker.reading.stepSensorValue) < 4.3) runDeliverBeltFirstSheet = false; //over 4.3 = out of range
         }
         if (maxRunDistance <= 0 && runDeliverBeltFirstSheet) runDeliverBeltFirstSheet = false;
         
         //Try this
         //if (stacker.reading.stepSensorValue > ((stacker.setting.tableSteps * -5.0) - stacker.reading.stackSize/100 - (streamer.setting.offsetBlindDistance<20 ? -1.0:0))  ) {
         //Old 
         // if (stacker.reading.stepSensorValue > (stacker.setting.tableSteps * -5.0))
         if (stacker.reading.stepSensorValue > ((stacker.setting.tableSteps * -5.0) - stacker.reading.stackSize/100 - (streamer.setting.offsetBlindDistance<20 ? -1.0:0))  ) {
            stackerStep(stacker.setting.tableSteps,stacker.reading.stepSensorValue,paperTransport[STREAMER_SECTION_2].isSpeed,streamer.reading.overLapInStack,fastFeed);
            stepSensorError = 0;
         }
         else if (!stacker.reading.input.streamerGap1) {
            stepSensorError += paperTransport[STREAMER_SECTION_2].isSpeed * MILLISECONDS_UNIT;
            if (stepSensorError > (300.0 + streamer.setting.overlap * 20)) {
               addError(STEP_SENSOR,NORMAL_STOP,TRUE,50);
               stepSensorError = 0;
            }
         }
      }
   }
   
   if (streamer.reading.deliverSheetPos == 3)   
   {
      if (stacker.reading.input.exitStreamerSection1) //increase gap
      {
         paperTransport[STREAMER_SECTION_2].setSpeed = (paperTransport[STREAMER_SECTION_1].setSpeed*1.15)+0.1;
         if (paperTransport[STREAMER_SECTION_2].setSpeed < streamer.setting.streamerDeliverySpeed) //we can feed faster...
            paperTransport[STREAMER_SECTION_2].setSpeed = streamer.setting.streamerDeliverySpeed;
         if (paperTransport[STREAMER_SECTION_1].setSpeed == 0) paperTransport[STREAMER_SECTION_2].setSpeed = 0;
         //test
         if (streamer.reading.instantDeliverOn && paperTransport[STREAMER_SECTION_1].setSpeed == 0 && paperTransport[STREAMER_SECTION_2].setSpeed < streamer.setting.streamerDeliverySpeed*0.5)
            paperTransport[STREAMER_SECTION_2].setSpeed = streamer.setting.streamerDeliverySpeed*0.5;
         //test
         distanceAfterGapSection2 = - 0.5;
      }
      else {
         
         if (streamer.reading.instantDeliverOn && paperTransport[STREAMER_SECTION_1].setSpeed < streamer.setting.streamerDeliverySpeed*0.5)
            paperTransport[STREAMER_SECTION_1].setSpeed = streamer.setting.streamerDeliverySpeed*0.5;

         paperTransport[STREAMER_SECTION_2].setSpeed = paperTransport[STREAMER_SECTION_1].setSpeed;
         if (distanceAfterGapSection2<10000) distanceAfterGapSection2 += paperTransport[STREAMER_SECTION_2].isSpeed * MILLISECONDS_UNIT;
      }
      
      if (stacker.reading.input.streamerGap1 && !lastSensor && deliverFly.startDeliverDist < 0)
      {
         deliverFly.startDeliverDist = 0.1; //start counting distance,, wait 20 mm
         //
      }
      if (deliverFly.startDeliverDist >= 0.0)
      {
         dontGlue[0] = true;
         deliverFly.startDeliverDist += paperTransport[STREAMER_SECTION_2].isSpeed * MILLISECONDS_UNIT;
         if (deliverFly.startDeliverDist > streamer.setting.lastSheetDist)
         {
            streamer.reading.deliverSheetPos = 0; //make gap idle
            streamer.reading.deliverOnTheFly = 1; //start delivering
            deliverFly.startDeliverDist = -1.0;
            streamer.reading.instantDeliverOn = false;
         }
      }
      else 
         dontGlue[0] = false;
   }
   else
   {
      dontGlue[0] = false;
      paperTransport[STREAMER_SECTION_2].setSpeed = paperTransport[STREAMER_SECTION_1].setSpeed;  
      deliverFly.startDeliverDist = -1.0;
      if (distanceAfterGapSection2<10000) distanceAfterGapSection2 += paperTransport[STREAMER_SECTION_2].isSpeed * MILLISECONDS_UNIT;
   }
   lastSensor = stacker.reading.input.streamerGap1; 
   lastOverLap = streamer.setting.overlap;
   
   
   if ((ebmGlue.setting.glueGun.pattern >= 1 || ebmGlue.setting.offset.enable) && paperTransport[STREAMER_SECTION_2].isSpeed > 0.1)
   {
      if (((paperTransport[STREAMER_SECTION_2].isSpeed + paperTransport[STREAMER_SECTION_2].setSpeed) * 0.375) > ebmGlue.reading.pulseSpeed * 0.001) encErrorCnt++;
      if ((paperTransport[STREAMER_SECTION_2].setSpeed) * 0.90 <= ebmGlue.reading.pulseSpeed * 0.001) encErrorCnt = 0;
      if (encErrorCnt > 1500) {
         addError (ENCODER_WHEEL,NORMAL_STOP,TRUE,(UINT)(ebmGlue.reading.pulseSpeed));
         encErrorCnt = 0;
      }
   }
   else encErrorCnt = 0;
}

void sheetSignal(void)
{
   static UDINT lastPagePostTick;
   if (cutter.reading.sheetSignal != lastSheetSignal)
   {
      endPageInStream = false;
      streamer.reading.pagesSinceDeliver++;
      if (stacker.reading.input.exitStreamerSection1) noPaperExitStreamerSensor++;
      else noPaperExitStreamerSensor = 0;
      
      if ((1000/streamer.setting.overlap) < noPaperExitStreamerSensor) {
         addError(EXIT_STREAMER_SECTION_1,NORMAL_STOP,TRUE,1);
         noPaperExitStreamerSensor = 0;
      }
      pagesReceived++;
      deliverRequest.sheetsInState++;
      if (measureGapTime) measureGapTime = false;
      debug.sheetSignalCnt++;
      alignerFloorSpeed[SHEET_SIGNAL].speed = overSpeedSimple (inSpeed.follow,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_ZERO);
      if (alignerFloorSpeed[SHEET_SIGNAL].speed) alignerFloorSpeed[SHEET_SIGNAL].timer = ((TOTAL_ALIGNER_LENGTH-cutter.reading.sheetSize)/1000)/alignerFloorSpeed[SHEET_SIGNAL].speed;
      else alignerFloorSpeed[SHEET_SIGNAL].timer = 0.5;
      
      /*   TEMPORARLY REMOVED for LSI to Determine if this causes missed delvieries. v1017
      if (streamer.reading.deliverSheetPos && cutter.reading.currentPage.type == DELIVER_PAGE) {
         cutter.reading.currentPage.type = NORMAL_PAGE;
         //cutter.command.deliverNextRequest = false;
         //lineController.command.trippleBeep = true;
      }*/
      
      //
      
      
      
      if (cutter.reading.currentPage.type == END_PAGE) endPageInStream = true;
      
      
      if ( cutter.reading.currentPage.type == DELIVER_PAGE) {
         debugger(4,cutter.reading.webSpeed,(REAL)(lastPagePostTick),stacker.reading.transportPages);
         dbgTxt("DELIVER_PAGE");
         streamer.reading.deliverSheetPos = 1;
         paperTransport[ALIGNER].acceleration = 5.0;
         pagesSinceDeliver = 0;
         alignerLastSheetDistance = 0;
         global_dbg.real4 = cutter.reading.webSpeed;
         streamer.reading.lastSheetSpeed = cutter.reading.webSpeed;
         cutter.command.deliverNextRequest = false;
         dbgTxt("4 delNext FALSE");
         deliverRequest.state = afterDelivery;
         stacker.reading.tripCounter.jobs++;
         streamer.reading.lastSheetGap = 0.0;
         measureGapTime	= true;
         streamer.reading.lastRecivedOffset = 1; //reset to south 
         stacker.reading.deliverPageReceived = true;
         streamer.reading.instantDeliverOn = false;
         streamer.reading.pagesSinceDeliver = 0;
         
         
         
         //neverStopStream = 5;
         ///ignoreDeliverPage = 50;
      }
      else 
      {
         pagesSinceDeliver++;
         stacker.reading.deliverPageReceived = false;
      }
      if (cutter.reading.currentPage.type == OFFSET_PAGE) {
         stacker.reading.tripCounter.jobs++;
         streamer.reading.lastRecivedOffset = !streamer.reading.lastRecivedOffset;
      }
      
      pageInTransportQueue(POST_PAGE,cutter.reading.currentPage);
      lastPagePostTick = tick;
      streamer.reading.sheetsAfterEmptyStack++;
      stacker.reading.tripCounter.pages++;
      pagesAfterReject++;
      enterAlignerCrash++;
      exitAlignerCrash++;
   } 
   
   if (!stacker.reading.input.rackGate)
   {
      addError(ALIGNER_GATE,NORMAL_STOP,true,0);
      enterAlignerCrash = 0;
      exitAlignerCrash = 0;
   }
   
   
   if (alignerLastSheetDistance>(INFEED_TO_FIRST_SENSOR-50))
   {
      alignerFloorSpeed[DELIVERY].speed = 2.5; //1.0
      alignerFloorSpeed[DELIVERY].timer = 0.2;
   }
   
   
   lastSheetSignal = cutter.reading.sheetSignal;     
}

void alignerEnter(void)
{
   static BOOL lastSensor;
   struct sheet dummySheet;
   
   if (enterAlignerCrash>5) addError(ENTER_TRANSPORT,NORMAL_STOP,false,(USINT)(stacker.reading.input.enterTransport));

   if (streamer.reading.deliverSheetPos == 1)
   {
      alignerLastSheetDistance += mm(ALIGNER);
   }
   if (!stacker.reading.input.enterTransport && lastSensor) //sheet enter sensor
   {
      alignerFloorSpeed[ALIGNER_ENTER].speed = overSpeedSimple (inSpeed.follow,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
      if (alignerFloorSpeed[ALIGNER_ENTER].speed) alignerFloorSpeed[ALIGNER_ENTER].timer = ((FIRST_SENSOR_TO_SECOND_SENSOR)/1000)/alignerFloorSpeed[ALIGNER_ENTER].speed;
      else alignerFloorSpeed[ALIGNER_ENTER].timer = 0.5;
      
      if (pagesAfterReject == 0 && stacker.reading.transportPages == 0) { //this is a band aid.. TODO fix it correctly
         dummySheet.sheetLength = cutter.reading.sheetSize;
         dummySheet.type = NORMAL_PAGE;
         pageInTransportQueue(POST_PAGE,dummySheet); 
         debugger(6,cutter.reading.webSpeed,0,stacker.reading.transportPages);
         //lineController.command.beep = true;
      }
      if (enterAlignerCrash) enterAlignerCrash = 0;
   }
   if (stacker.reading.input.enterTransport && !lastSensor) //sheet leaving sensor
   {
      if (stacker.reading.input.rackGate)
         alignerFloorSpeed[ALIGNER_ENTER].speed = overSpeedSimple (inSpeed.follow,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
      else 
         alignerFloorSpeed[ALIGNER_ENTER].speed = 0;
      if (alignerFloorSpeed[ALIGNER_ENTER].speed) alignerFloorSpeed[ALIGNER_ENTER].timer = ((FIRST_SENSOR_TO_STREAMINPUT-cutter.reading.sheetSize)/1000)/alignerFloorSpeed[ALIGNER_ENTER].speed;
      else alignerFloorSpeed[ALIGNER_ENTER].timer = 0.5;
   }
   if (streamer.reading.feedToStack == 1)
   {
      alignerFloorSpeed[ALIGNER_ENTER].speed = 0.4;
      alignerFloorSpeed[ALIGNER_ENTER].timer = 2.5;
   }
   
   lastSensor = stacker.reading.input.enterTransport;

}


void alignerExit(void)
{
   static BOOL lastSensor;
   //static UINT oldalignerOutfeedCnt1;
   static UDINT lastPageRemTick;
   static REAL blindDistance;
  
   USINT pgType; 
   
   if (blindDistance >= 0) {
      blindDistance -= mm(ALIGNER);
      oldalignerOutfeedCnt1 = stacker.reading.input.alignerOutfeedCnt1;
   }
   
   pgType = pageInTransportQueue (PEEK_PAGE);
   
   if (exitAlignerCrash>8) addError(EXIT_TRANSPORT,NORMAL_STOP,false,(USINT)(stacker.reading.input.alignerOutfeedCnt1));

   if (streamer.reading.deliverSheetPos == 2) streamer.reading.deliverSheetAlignerExitPosToStr += paperTransport[ALIGNER].isSpeed * MILLISECONDS_UNIT;
   
   if (stacker.reading.input.alignerOutfeedCnt1 != oldalignerOutfeedCnt1)
   {
      
      blindDistance = (cutter.reading.currentPage.sheetLength * 0.95) - 25;
         
      if (/* pgType == DELIVER_PAGE */ alignerLastSheetDistance > (680.0 + (stacker.reading.transportPages > 1 ? 50 : 0)) ) { //680
      //if (alignerLastSheetDistance > (630.0) && !stacker.reading.input.alignerOutfeedDigital1) {    
         debugger(3,alignerLastSheetDistance,(REAL)(streamer.reading.instantDeliverOn),stacker.reading.transportPages);
         streamer.reading.deliverSheetPos = 2;
         global_dbg.real3 = alignerLastSheetDistance; 
         streamer.reading.deliverSheetAlignerExitPosToStr = 990.0-alignerLastSheetDistance;
         alignerLastSheetDistance = 0;
         
      }
      
      //if (pageOnExit>(cutter.reading.currentPage.sheetLength-70)) {
      if (DELIVER_PAGE == pgType || OFFSET_PAGE == pgType)  streamerGlueQueue(addNoGlue);
      else streamerGlueQueue(addGlue);
      pageInTransportQueue(REMOVE_LAST_PAGE); 
      lastPageRemTick = tick;
      streamerGlueQueue(dbg);
      //}    
      if (exitAlignerCrash) exitAlignerCrash--;
   }
   
   if (pagesReceived == 0 && stacker.command.instantDeliver)
   {
      stacker.command.instantDeliver = false;
   }
   
   if (streamer.reading.deliverSheetPos > 0 && stacker.command.instantDeliver)
   {
      stacker.command.instantDeliver = false;
   }
   
   if (stacker.command.instantDeliver && stacker.reading.transportPages == 0)
   {
      debugger(1,alignerLastSheetDistance,(REAL)(lastPageRemTick),0);
      streamer.reading.deliverSheetPos = 2;
      global_dbg.real3 = alignerLastSheetDistance; 
      streamer.reading.deliverSheetAlignerExitPosToStr = 15;//990.0-alignerLastSheetDistance;
      alignerLastSheetDistance = 0;
      stacker.command.instantDeliver = false;
      lineController.command.beep = true;
      streamer.reading.instantDeliverOn = true;
      /* this stuff happend on an normal DELIVER_PAGE
         debugger(4,cutter.reading.webSpeed,(REAL)(lastPagePostTick),stacker.reading.transportPages);
         streamer.reading.deliverSheetPos = 1;
         paperTransport[ALIGNER].acceleration = 5.0;
         pagesSinceDeliver = 0;
         alignerLastSheetDistance = 0;
         global_dbg.real4 = cutter.reading.webSpeed;
         streamer.reading.lastSheetSpeed = cutter.reading.webSpeed;
         cutter.command.deliverNextRequest = false;
         deliverRequest.state = afterDelivery;
         stacker.reading.tripCounter.jobs++;
         streamer.reading.lastSheetGap = 0.0;
         measureGapTime	= true;
         streamer.reading.lastRecivedOffset = 1; //reset to south 
      */
   }
   else if (stacker.command.instantDeliver)
   {
      debugger(2,alignerLastSheetDistance,(REAL)(lastPageRemTick),stacker.reading.transportPages);
   }
   
   
   if (pgType == DELIVER_PAGE && alignerLastSheetDistance>1050 && streamer.reading.deliverSheetPos == 1) { //guard, queue is one page off
      streamer.reading.deliverSheetPos = 2;
      //pageInTransportQueue(INIT_QUEUE); // this causes problems, when running fast. sometimes the cutter has already sent a page
      //if (pageInTransportQueue(GET_NO_PAGES_IN_QUEUE) > 1) pageInTransportQueue(REMOVE_LAST_PAGE); // try this one might help if we are out of sync.
      global_dbg.real3 = alignerLastSheetDistance;
      streamer.reading.deliverSheetAlignerExitPosToStr = 0.0;
      alignerLastSheetDistance = 0;
   }

   if (!stacker.reading.input.alignerOutfeedDigital1) 
   {
      if (stacker.reading.input.rackGate)
      {
         if (streamer.reading.deliverSheetPos == 2)
            alignerFloorSpeed[ALIGNER_EXIT].speed = overSpeedSimple ((inSpeed.follow<1.5?1.5:inSpeed.follow),stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
         else
            alignerFloorSpeed[ALIGNER_EXIT].speed = overSpeedSimple (inSpeed.follow,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
      }
      else
         alignerFloorSpeed[ALIGNER_EXIT].speed = 0;
   
      REAL adjDist;
      if (alignerFloorSpeed[ALIGNER_EXIT].speed<0.3) adjDist = 15;
      else if (alignerFloorSpeed[ALIGNER_EXIT].speed<1.0) adjDist = 0;
      else adjDist = -10;
   
      if (alignerFloorSpeed[ALIGNER_EXIT].speed) alignerFloorSpeed[ALIGNER_EXIT].timer = (((END_SENSOR_TO_STREAMINPUT + adjDist )/1000))/alignerFloorSpeed[ALIGNER_EXIT].speed;
      if (alignerFloorSpeed[ALIGNER_EXIT].timer>5.0) alignerFloorSpeed[ALIGNER_EXIT].timer = 5.0;
   }  
   if (stacker.reading.input.alignerOutfeedDigital1) {
      gapOnExit += paperTransport[ALIGNER].isSpeed * MILLISECONDS_UNIT;
      if (gapOnExit>15.0) pageOnExit = 0;
   }
   else  {
      pageOnExit += paperTransport[ALIGNER].isSpeed * MILLISECONDS_UNIT;
      if (pageOnExit>30.0) gapOnExit = 0;
   }
   lastSensor = stacker.reading.input.alignerOutfeedDigital1;
   //oldalignerOutfeedCnt1 = stacker.reading.input.alignerOutfeedCnt1;
}



void cutterRunning(void)
{
   
   //static USINT pgLast;
   if (inSpeed.follow > 0.015)
   {
      alignerFloorSpeed[CUTTER_RUNNING].speed = overSpeedSimple (inSpeed.follow,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_ZERO);
      alignerFloorSpeed[CUTTER_RUNNING].timer = 0.75 + (inSpeed.is<0.05?1:0);
      
   }
   /*if (cutter.reading.currentPage.type == NORMAL_PAGE && pgLast == DELIVER_PAGE && inSpeed.is < 0.25 && inSpeed.is > 0.005) //fix first sheet after gap, in high acceleration
   {
      alignerFloorSpeed[CUTTER_RUNNING].speed = overSpeedSimple (inSpeed.follow * (cutter.setting.masterAcc < 760 ? 1 : (cutter.setting.masterAcc < 1500 ? 1.2 : 1.5)) ,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_ZERO);
      alignerFloorSpeed[CUTTER_RUNNING].timer = 1.5;
   }
   pgLast = cutter.reading.currentPage.type;*/
   if (pagesSinceDeliver < 5) // speed up first sheet
   {
      alignerFloorSpeed[CUTTER_RUNNING].speed = alignerFloorSpeed[CUTTER_RUNNING].speed * (1 + (cutter.setting.masterAcc / (7500 * ((REAL)pagesSinceDeliver + 1))));
      if (alignerFloorSpeed[CUTTER_RUNNING].speed > (cutter.setting.infeedRunSpeed*stacker.setting.transportTableSpeed)/1000) alignerFloorSpeed[CUTTER_RUNNING].speed = (cutter.setting.infeedRunSpeed*stacker.setting.transportTableSpeed)/1000.0;
      if (alignerFloorSpeed[CUTTER_RUNNING].speed > inSpeed.follow*10) alignerFloorSpeed[CUTTER_RUNNING].speed = inSpeed.follow*10;
      if (alignerFloorSpeed[CUTTER_RUNNING].speed < stacker.setting.transportIdleSpeed*2) alignerFloorSpeed[CUTTER_RUNNING].speed = stacker.setting.transportIdleSpeed*2;
   }
   
}



void runPaperTransport(void)
{
   REAL oldSetSpeedAligner;
   REAL oldSetSpeedStrSec1;
   REAL oldSetSpeedStrSec2;
   REAL oldSetSpeedTaglia;
   REAL oldSetSpeedDeliver;
   static BOOL wasAlignerFeeding;
   static USINT tick;
   tick++;
   paperTransport[ALIGNER].isSpeed            = aligner.Status.ActVelocity / 1000;
   paperTransport[STREAMER_SECTION_1].isSpeed = strSection1.Status.ActVelocity / 1000;
   paperTransport[STREAMER_SECTION_2].isSpeed = strSection2.Status.ActVelocity / 1000;
   paperTransport[JOGGER1].isSpeed            = jogger1.Status.ActVelocity / 1000;
   paperTransport[JOGGER2].isSpeed            = jogger2.Status.ActVelocity / 1000;
   paperTransport[DELIVER].isSpeed            = deliver.Status.ActVelocity / 1000;
   paperTransport[TAGLIA].isSpeed             = taglia.Status.ActVelocity / 1000;
   
   
   
   paperTransport[ALIGNER].moveing = !aligner.AxisState.StandStill;
   paperTransport[STREAMER_SECTION_1].moveing = !strSection1.AxisState.StandStill;
   paperTransport[STREAMER_SECTION_2].moveing = !strSection2.AxisState.StandStill;
   paperTransport[JOGGER1].moveing = !jogger1.AxisState.StandStill;
   paperTransport[JOGGER2].moveing = !jogger2.AxisState.StandStill;
   paperTransport[DELIVER].moveing = !deliver.AxisState.StandStill;
   paperTransport[TAGLIA].moveing = !taglia.AxisState.StandStill;
   
   
   
   paperTransport[ALIGNER].position = aligner.Status.ActPosition;
   paperTransport[STREAMER_SECTION_1].position = strSection1.Status.ActPosition;
   paperTransport[STREAMER_SECTION_2].position = strSection2.Status.ActPosition;
   paperTransport[JOGGER1].position = jogger1.Status.ActPosition;
   paperTransport[JOGGER2].position = jogger2.Status.ActPosition;
   paperTransport[DELIVER].position = deliver.Status.ActPosition;
   paperTransport[TAGLIA].position = taglia.Status.ActPosition;
   
   paperTransport[ALIGNER].homed              =aligner.Status.DriveStatus.HomingOk;
   paperTransport[STREAMER_SECTION_1].homed   =strSection1.Status.DriveStatus.HomingOk;
   paperTransport[STREAMER_SECTION_2].homed   =strSection2.Status.DriveStatus.HomingOk;
   paperTransport[JOGGER1].homed              =jogger1.Status.DriveStatus.HomingOk;
   paperTransport[JOGGER2].homed              =jogger2.Status.DriveStatus.HomingOk;
   paperTransport[DELIVER].homed              =deliver.Status.DriveStatus.HomingOk;
   paperTransport[TAGLIA].homed               =taglia.Status.DriveStatus.HomingOk;
   
   paperTransport[ALIGNER].error              =(aligner.Status.ErrorID > 0 ? TRUE : FALSE);
   paperTransport[STREAMER_SECTION_1].error   =(strSection1.Status.ErrorID > 0 ? TRUE : FALSE);
   paperTransport[STREAMER_SECTION_2].error   =(strSection2.Status.ErrorID > 0 ? TRUE : FALSE);
   paperTransport[JOGGER1].error              =(jogger1.Status.ErrorID > 0 ? TRUE : FALSE);
   paperTransport[JOGGER2].error              =(jogger2.Status.ErrorID > 0 ? TRUE : FALSE);
   paperTransport[DELIVER].error              =(deliver.Status.ErrorID > 0 ? TRUE : FALSE);
   paperTransport[TAGLIA].error               =(taglia.Status.ErrorID > 0 ? TRUE : FALSE);
   
   aligner.Parameter.Acceleration = paperTransport[ALIGNER].acceleration * 1000; 
   aligner.Parameter.Deceleration = paperTransport[ALIGNER].deceleration * 1000;
   jogger1.Parameter.Acceleration = paperTransport[JOGGER1].acceleration * 1000;
   jogger1.Parameter.Deceleration = paperTransport[JOGGER1].deceleration * 1000;
   jogger2.Parameter.Acceleration = paperTransport[JOGGER2].acceleration * 1000;
   jogger2.Parameter.Deceleration = paperTransport[JOGGER2].deceleration * 1000;
   strSection1.Parameter.Acceleration = paperTransport[STREAMER_SECTION_1].acceleration * 1000;
   strSection1.Parameter.Deceleration = paperTransport[STREAMER_SECTION_1].deceleration * 1000;
   strSection2.Parameter.Acceleration = paperTransport[STREAMER_SECTION_2].acceleration * 1000;
   strSection2.Parameter.Deceleration = paperTransport[STREAMER_SECTION_2].deceleration * 1000;
   taglia.Parameter.Acceleration = paperTransport[TAGLIA].acceleration * 1000;
   taglia.Parameter.Deceleration = paperTransport[TAGLIA].deceleration * 1000;
   deliver.Parameter.Acceleration = paperTransport[DELIVER].acceleration * 1000;
   deliver.Parameter.Deceleration = paperTransport[DELIVER].deceleration * 1000;
   
   if (lineController.setup.streamer.jogger3) {
      paperTransport[JOGGER3].isSpeed = jogger3.Status.ActVelocity / 1000;
      paperTransport[JOGGER3].moveing = !jogger3.AxisState.StandStill;
      paperTransport[JOGGER3].position = jogger3.Status.ActPosition;
      paperTransport[JOGGER3].homed              =jogger3.Status.DriveStatus.HomingOk;
      paperTransport[JOGGER3].error              =(jogger3.Status.ErrorID > 0 ? TRUE : FALSE);
      jogger3.Parameter.Acceleration = paperTransport[JOGGER3].acceleration * 1000;
      jogger3.Parameter.Deceleration = paperTransport[JOGGER3].deceleration * 1000;
   }
   
   if (paperTransport[TAGLIA].setSpeed == 0)
   {
      taglia.Parameter.Velocity = 0.0;
      if (tagliaCnt>0) tagliaCnt=0;
      if (tagliaCnt>-1000)
         tagliaCnt--;
      if (tagliaCnt == -1) taglia.Command.MoveVelocity = TRUE;
      else if (tagliaCnt == -3) taglia.Command.MoveVelocity = TRUE;
      else if (tagliaCnt == -5) taglia.Command.MoveVelocity = TRUE;
      else if (tagliaCnt == -10 && paperTransport[TAGLIA].isSpeed>0.1 && taglia.Command.MoveVelocity == TRUE)
      {
         taglia.Command.Halt = TRUE;
      }
      else if (tagliaCnt == -20 && paperTransport[TAGLIA].isSpeed>0.05 && taglia.Command.Halt == TRUE)
      {
         taglia.Command.Stop = TRUE;
      }
      else if (tagliaCnt == -30 && taglia.Command.Stop) taglia.Command.Stop = false;
   }
   else
   {
      if (tagliaCnt<0) tagliaCnt=0;
      if (tagliaCnt<1000) tagliaCnt++;
      if (oldSetSpeedTaglia != paperTransport[TAGLIA].setSpeed || !realEqual(taglia.Parameter.Velocity,taglia.Status.ActVelocity,100)) {
         tagliaCnt=1;
         taglia.Parameter.Velocity = paperTransport[TAGLIA].setSpeed*1000;
         oldSetSpeedTaglia = paperTransport[TAGLIA].setSpeed;
      }
      if (tagliaCnt == 1) taglia.Command.MoveVelocity = TRUE;
      if (tagliaCnt == 3) taglia.Command.MoveVelocity = TRUE;
      if (taglia.Command.MoveVelocity == TRUE && tagliaCnt == 5)
      {
         taglia.Command.MoveVelocity = FALSE;
      }
      if (tagliaCnt == 7) taglia.Command.MoveVelocity = TRUE;
      if (tagliaCnt == 10) taglia.Command.MoveVelocity = FALSE;
   }
   
   
   if (paperTransport[DELIVER].setSpeed == 0)
   {
      deliver.Parameter.Velocity = 0.0;
      if (deliverCnt>0) deliverCnt=0;
      if (deliverCnt>-1000)
         deliverCnt--;
      if (deliverCnt == -1) deliver.Command.MoveVelocity = TRUE;
      else if (deliverCnt == -3) deliver.Command.MoveVelocity = TRUE;
      else if (deliverCnt == -5) deliver.Command.MoveVelocity = TRUE;
      else if (deliverCnt == -10 && paperTransport[DELIVER].isSpeed>0.1 && deliver.Command.MoveVelocity == TRUE)
      {
         deliver.Command.Halt = TRUE;
      }
      else if (deliverCnt == -20 && paperTransport[DELIVER].isSpeed>0.05 && deliver.Command.Halt == TRUE)
      {
         deliver.Command.Stop = TRUE;
      }
      else if (deliverCnt == -30 && deliver.Command.Stop) deliver.Command.Stop = false;
      else if (deliverCnt == -999) deliver.Command.Power = false;
   }
   else
   {
      if (deliverCnt<0) {
         deliverCnt=0;
         deliver.Command.Power = true;
      }
      if (oldSetSpeedDeliver != paperTransport[DELIVER].setSpeed || !realEqual(deliver.Parameter.Velocity,deliver.Status.ActVelocity,100)) {
         deliverCnt=1;
         deliver.Parameter.Velocity = paperTransport[DELIVER].setSpeed*1000;
         oldSetSpeedDeliver = paperTransport[DELIVER].setSpeed;
      }
      if (deliverCnt == 1) deliver.Command.MoveVelocity = TRUE;
      if (deliverCnt == 3) deliver.Command.MoveVelocity = TRUE;
      if (deliver.Command.MoveVelocity == TRUE && deliverCnt == 5)
      {
         deliver.Command.MoveVelocity = FALSE;
      }
      if (deliverCnt == 7) deliver.Command.MoveVelocity = TRUE;
      if (deliverCnt == 10) deliver.Command.MoveVelocity = FALSE;
      if (deliverCnt<1000) deliverCnt++;
   }
   
   ///feeding not loaded construction >
   if (feedAligner) {
      wasAlignerFeeding = true;
      if (cutter.reading.setSpeed || cutter.reading.webSpeed) paperTransport[ALIGNER].setSpeed = highest(cutter.reading.setSpeed,cutter.reading.webSpeed,0.0) * 1.2;
      else paperTransport[ALIGNER].setSpeed = 0.07;
   }
   if (wasAlignerFeeding && !feedAligner)
   {
      paperTransport[ALIGNER].setSpeed = 0;
      wasAlignerFeeding = false;
   }
   ///feeding not loaded construction ^

   if (paperTransport[ALIGNER].setSpeed == 0)
   {
      aligner.Parameter.Velocity = 0.0;
      if (alignerCnt>0) alignerCnt=0;
      if (alignerCnt>-1000)
         alignerCnt--;
      if (alignerCnt == -1) aligner.Command.MoveVelocity = TRUE;
      else if (alignerCnt == -3) aligner.Command.MoveVelocity = TRUE;
      else if (alignerCnt == -5) aligner.Command.MoveVelocity = TRUE;
      else if (alignerCnt == -10 && paperTransport[ALIGNER].isSpeed>0.1 && aligner.Command.MoveVelocity == TRUE)
      {
         aligner.Command.Halt = TRUE;
      }
      else if (alignerCnt == -20 && paperTransport[ALIGNER].isSpeed>0.05 && aligner.Command.Halt == TRUE)
      {
         aligner.Command.Stop = TRUE;
      }
      else if (alignerCnt == -30 && aligner.Command.Stop) aligner.Command.Stop = false;
   }
   else
   {
      if (alignerCnt<0) alignerCnt=0;
      if (alignerCnt<1000) alignerCnt++;
      if (oldSetSpeedAligner != paperTransport[ALIGNER].setSpeed || !realEqual(aligner.Parameter.Velocity,aligner.Status.ActVelocity,500)) {
         alignerCnt=1;
         aligner.Parameter.Velocity = paperTransport[ALIGNER].setSpeed*1000;
         oldSetSpeedAligner = paperTransport[ALIGNER].setSpeed;
      }
      if (alignerCnt == 1) aligner.Command.MoveVelocity = TRUE;
      if (alignerCnt == 3) aligner.Command.MoveVelocity = TRUE;
      if (aligner.Command.MoveVelocity == TRUE && alignerCnt == 5)
      {
         aligner.Command.MoveVelocity = FALSE;
      }
      if (alignerCnt == 7) aligner.Command.MoveVelocity = TRUE;
      if (alignerCnt == 10) aligner.Command.MoveVelocity = FALSE;
   }

   if (paperTransport[STREAMER_SECTION_1].setSpeed == 0)
   {
      strSection1.Parameter.Velocity = 0.0;
      if (streamerSection1Cnt>0) streamerSection1Cnt=0;
      if (streamerSection1Cnt>-1000) streamerSection1Cnt--;
      if (streamerSection1Cnt == -1) strSection1.Command.MoveVelocity = TRUE;
      else if (streamerSection1Cnt == -3) strSection1.Command.MoveVelocity = TRUE;
      else if (streamerSection1Cnt == -5) strSection1.Command.MoveVelocity = TRUE;
      else if (streamerSection1Cnt == -10 && paperTransport[STREAMER_SECTION_1].isSpeed>0.1 && strSection1.Command.MoveVelocity == TRUE)
      {
         strSection1.Command.Halt = TRUE;
      }
      else if (streamerSection1Cnt == -20 && paperTransport[STREAMER_SECTION_1].isSpeed>0.05 && strSection1.Command.Halt == TRUE)
      {
         strSection1.Command.Stop = TRUE;
      }
      else if (streamerSection1Cnt == -30 && strSection1.Command.Stop) strSection1.Command.Stop = false;
   }
   else
   {
      if (streamerSection1Cnt<0) streamerSection1Cnt=0;
      if (streamerSection1Cnt<1000) streamerSection1Cnt++;
      if (oldSetSpeedStrSec1 != paperTransport[STREAMER_SECTION_1].setSpeed || !realEqual(strSection1.Parameter.Velocity,strSection1.Status.ActVelocity,100)) {
         streamerSection1Cnt=1;
         strSection1.Parameter.Velocity = paperTransport[STREAMER_SECTION_1].setSpeed*1000;
         oldSetSpeedStrSec1 = paperTransport[STREAMER_SECTION_1].setSpeed;
      }
      if (streamerSection1Cnt == 1) strSection1.Command.MoveVelocity = TRUE;
      if (streamerSection1Cnt == 3) strSection1.Command.MoveVelocity = TRUE;
      if (strSection1.Command.MoveVelocity == TRUE && streamerSection1Cnt == 5)
      {
         strSection1.Command.MoveVelocity = FALSE;
      }
      if (streamerSection1Cnt == 7) strSection1.Command.MoveVelocity = TRUE;
      if (streamerSection1Cnt == 10) strSection1.Command.MoveVelocity = FALSE;
   }

   if (paperTransport[STREAMER_SECTION_2].setSpeed == 0)
   {
      strSection2.Parameter.Velocity = 0.0;
      if (streamerSection2Cnt>0) streamerSection2Cnt=0;
      if (streamerSection2Cnt>-1000) streamerSection2Cnt--;
      if (streamerSection2Cnt == -1) strSection2.Command.MoveVelocity = TRUE;
      else if (streamerSection2Cnt == -3) strSection2.Command.MoveVelocity = TRUE;
      else if (streamerSection2Cnt == -5) strSection2.Command.MoveVelocity = TRUE;
      else if (streamerSection2Cnt == -20 && paperTransport[STREAMER_SECTION_2].isSpeed>0.1 && strSection2.Command.MoveVelocity == TRUE)
      {
         strSection2.Command.Halt = TRUE;
      }
      else if (streamerSection2Cnt == -20 && paperTransport[STREAMER_SECTION_2].isSpeed>0.05 && strSection2.Command.Halt == TRUE)
      {
         strSection2.Command.Stop = TRUE;
      }
      else if (streamerSection2Cnt == -30 && strSection2.Command.Stop) strSection2.Command.Stop = false;
   }
   else
   {
      if (streamerSection2Cnt<0) streamerSection2Cnt=0;
      if (streamerSection2Cnt<1000) streamerSection2Cnt++;
      if (oldSetSpeedStrSec2 != paperTransport[STREAMER_SECTION_2].setSpeed || !realEqual(strSection2.Parameter.Velocity,strSection2.Status.ActVelocity,1000)) {
         streamerSection2Cnt=1;
         strSection2.Parameter.Velocity = paperTransport[STREAMER_SECTION_2].setSpeed*1000;
         oldSetSpeedStrSec2 = paperTransport[STREAMER_SECTION_2].setSpeed;
      }
      if (!realEqual (strSection2.Parameter.Velocity, paperTransport[STREAMER_SECTION_2].setSpeed*1000, 2)) 
      {
         streamerSection2Cnt=1;
         strSection2.Parameter.Velocity = paperTransport[STREAMER_SECTION_2].setSpeed*1000;
      }
      if (streamerSection2Cnt == 1) strSection2.Command.MoveVelocity = TRUE;
      if (streamerSection2Cnt == 3) strSection2.Command.MoveVelocity = TRUE;
      if (strSection2.Command.MoveVelocity == TRUE && streamerSection2Cnt == 5)
      {
         strSection2.Command.MoveVelocity = FALSE;
      }
      if (streamerSection2Cnt == 7) strSection2.Command.MoveVelocity = TRUE;
      if (streamerSection2Cnt == 20) strSection2.Command.MoveVelocity = FALSE;
      if (strSection2.Parameter.Velocity<0) strSection2.Parameter.Velocity = 0;
   }
      
}

void runJoggers(void)
{
   if (!joggers.ready) {
      //if (jogger2.AxisState.SynchronizedMotion && jogger1.Command.Power && jogger1.Command.Power && jogger1.Status.ErrorID != 0 && jogger2.Status.ErrorID != 0) joggers.ready = true;
      return;
   }
   if (!fixedJogger2) {
      if (!jogger2.AxisState.SynchronizedMotion) {
         if ( joggers.wait ) {
            joggers.wait--;
            return;
         }
         if (jogger2.Command.StartSlave)
         {
            jogger2.Command.StartSlave = false;
            jogger2.Command.Halt = true;
            joggers.wait = 20;
         }
         else {
            jogger2.Command.StartSlave = true;
            joggers.wait = 20;
         }
         return;
      }
   }
   else
   {
      if (streamer.reading.mainState == RUN || streamer.reading.mainState == STOPPED) {
         if (jogger2.Status.ActPosition != rmod(streamer.setting.jogger2Position,0,62.9) && jogger2.AxisState.StandStill && !jogger2.Command.MoveAbsolute && streamer.setting.jogger2Position != 0.0)
         {
            jogger2.Parameter.Position = rmod(streamer.setting.jogger2Position,0,62.9);
            jogger2.Command.MoveAbsolute = true;
            jogger2Pos = rmod(streamer.setting.jogger2Position,0,62.9);
         }
      }
   }
   
   
   switch (joggers.state)
   {
      case 0:
         //idle
         break;
      case 1:
         if (!joggers.wait) joggers.state = 2;
         break;
      case 2:
         jogger1.Parameter.Velocity = 200;
         jogger1.Parameter.Position = joggers.masterOpenPos;
         jogger1.Command.MoveAbsolute = true;
         joggers.state = 12;
         joggers.wait = 200;
         break;
      case 3:
         if (!joggers.wait) joggers.state = 4;
         break;
      case 4:
         jogger1.Parameter.Velocity = 200;
         jogger1.Parameter.Position = joggers.masterClosePos;
         jogger1.Command.MoveAbsolute = true;
         joggers.state = 12;
         joggers.wait = 200;
         break;
      case 5:
         if (!joggers.wait) joggers.state = 6;
         break;
      case 6:
         jogger1.Command.MoveVelocity = TRUE;
         joggers.state = 7;
         joggers.wait = 9;
         break;
      case 7:
         if (!joggers.wait) joggers.state = 8;
         if (joggers.wait<4) jogger1.Command.MoveVelocity = FALSE;
         break;
      case 8:
         jogger1.Command.MoveVelocity = TRUE; 
         if (jogger1.Parameter.Velocity == 0) {
            if (jogger1.AxisState.StandStill) joggers.state = 0;
            else {
               joggers.state = 9;
               joggers.wait = 200;
            }
         }
         else joggers.state = 0;
         break;
      case 9:
         if (!joggers.wait) joggers.state = 10;
         if (joggers.wait == 100) jogger1.Command.MoveVelocity = FALSE;
         if (joggers.wait == 150) jogger1.Command.MoveVelocity = TRUE;
         if (joggers.wait == 180) jogger1.Command.MoveVelocity = FALSE;
         break;
      case 10:
         if (jogger1.AxisState.StandStill) joggers.state = 0;
         else jogger1.Command.Halt = true;
         break;
      case 11:
         if (jogger1.AxisState.StandStill) joggers.state = 0;
         else jogger1.Command.Halt = true;
         break;
      case 12: //wait state
         if (!joggers.wait)
         {
            joggers.state = 0;
            joggers.close = false;
            joggers.open = false;
         }
         break;
   
   }
   if (joggers.open && joggers.state != 1 && joggers.state != 2 && joggers.state != 12)
   {
      joggers.state = 1;
      joggers.wait = 20;
      jogger1.Command.Halt = true;
      joggers.speed = 0.0;
      joggers.run = false;
      
   }
   else if (joggers.close && joggers.state != 3 && joggers.state != 4 && joggers.state != 12)
   {
      joggers.state = 3;
      joggers.wait = 20;
      jogger1.Command.Halt = true;
      joggers.speed = 0.0;
      joggers.run = false;
      
   }
   else if (joggers.run)
   {
      if (joggers.test) joggers.speed = joggers.testSpeed;
      if ((jogger1.Parameter.Velocity) != (joggers.speed*1000))
      {
         jogger1.Parameter.Velocity = joggers.speed*1000;
         jogger1.Command.MoveVelocity = TRUE;
         joggers.state = 5;
         joggers.wait = 8;
      }
   }
   else if (false == joggers.run)
   {
      if (jogger1.Parameter.Velocity>0)
      {
         jogger1.Parameter.Velocity = 0;
         if (jogger1.Status.ActVelocity )
            jogger1.Command.MoveVelocity = TRUE;
         joggers.state = 5;
         joggers.wait = 8;
         
         
         
      }
   }
   if (joggers.wait) joggers.wait--;

   if (lineController.setup.streamer.jogger3) {
      //jogger3.Parameter.Velocity = jogger1.Parameter.Velocity * 2.0;
      jogger3.Parameter.Velocity = jogger1.Parameter.Velocity * (streamer.setting.stackJoggerSpeed / 0.55);
      jogger3.Command.MoveVelocity = jogger1.Command.MoveVelocity;
      jogger3.Parameter.Direction = false;
      
      if (!stacker.reading.input.topCoverClosed && stacker.reading.input.joggerStackClosed)
      {
         moveJog3 = 0.0;
         switch (goInState)
         {
            case 0:
               if (stacker.reading.input.manFeed) goInState = 1;
               break;
            case 1: 
               if (!stacker.reading.input.homePosJoggerStack)
                  goInState = 2;
               else
                  moveJog3 = -150;
               break;
            case 2:
               moveJog3 = 50;
               if (stacker.reading.input.homePosJoggerStack) moveJog3 = 0;
               break;
         }

         if (globalPower && jogger3.Status.DriveStatus.HomingOk && jogger3.Command.Power)
         {
            if (moveJog3<0) {
               jogger3.Parameter.Direction = true;
               moveJog3 = moveJog3 * -1.0;
            }
            jogger3.Parameter.Velocity = moveJog3;
            jogger3.Command.MoveVelocity = TRUE;
         }
      }
      else {
         goInState = 0;
      }
      
      
      if (jogger3.Status.ActVelocity > 1 && jogger3.Parameter.Velocity == 0 && !jogger3.Command.MoveVelocity )
      {
         jogger3.Command.MoveVelocity = TRUE;
      }
      
   }
}


USINT checkPendingError(void)
{   
   USINT i,stopTyp;
   stopTyp = 0;
   for (i=0;i<=LAST_ERROR;i++)
   {
      if (error[i].active) {
     
         if ((error[i].stopType)>stopTyp) stopTyp = error[i].stopType;
      }
   }
   return stopTyp; //returns the most severe stop scenario
}

void startUpMachine(void)
{

   switch (stacker.reading.startUpSeq)
   {
      case 0:
         if (node[40].station[0] == TRUE) stacker.reading.startUpSeq++;
         break;
      case 1:
         if (node[41].station[0] == TRUE) stacker.reading.startUpSeq++;
         break;
      case 2:
         if (node[42].station[0] == TRUE) stacker.reading.startUpSeq++;
         break;
      case 3:
         if (node[43].station[0] == TRUE) stacker.reading.startUpSeq++;
         break;
      case 4:
         if (node[44].station[0] == TRUE) stacker.reading.startUpSeq++;
         break;
      case 5:
         if (stacker.reading.input.lightCurtain || stacker.reading.input.eStop || stacker.reading.input.eStopExt) stacker.reading.startUpSeq++;
         break;
      case 6:
         if (strSection1.Status.DriveStatus.ControllerReady || strSection2.Status.DriveStatus.DriveEnable || strSection2.Status.DriveStatus.ControllerReady || strSection1.Status.DriveStatus.DriveEnable) stacker.reading.startUpSeq++;
         break;
      case 7:
         if (aligner.Status.DriveStatus.ControllerReady || aligner.Status.DriveStatus.DriveEnable) stacker.reading.startUpSeq++;
         break;
      case 8:
         stacker.reading.startUpSeq++;
         break;
      case 9:
         stacker.reading.startUpSeq++;
         break;
      case 10:
         stacker.reading.startUpSeq++;
         break;
      case 11:
         stacker.reading.startUpSeq++;
         break;
      case 12:
         stacker.reading.startUpSeq++;
         break;
      case 13:
         stacker.reading.startUpSeq++;
         break;
      case 14:
         stacker.reading.startUpSeq++;
         break;
   }

}


void quarterSecondCycle(void)
{
   //static UDINT qTick;
   //static REAL minute;
   qTick++;
   if (qTick <= 1000000)
   {
      startUpMachine();
      if (qTick < 10) {
         setButton(1,COLOR_OFF);
         setButton(2,COLOR_OFF);
         setButton(3,COLOR_OFF);
         setButton(4,COLOR_OFF);
         setButton(5,COLOR_OFF);
      }
      else if (qTick<10) {
         if (isButton(1,COLOR_RED)) setButton(1,COLOR_OFF);
         else setButton(1,COLOR_RED);
      }
      else if (qTick<35) {
         setButton(1,COLOR_RED);
      }
      else if (qTick<60) {
         if (isButton(2,COLOR_RED)) setButton(2,COLOR_OFF);
         else setButton(2,COLOR_RED);
      }
      else if (qTick<85) {
         setButton(2,COLOR_RED);
      }
      else if (qTick<110) {
         if (isButton(3,COLOR_RED)) setButton(3,COLOR_OFF);
         else setButton(3,COLOR_RED);
      }
      else if (qTick<135) {
         setButton(3,COLOR_RED);
      }
      else if (qTick<160) {
         if (isButton(4,COLOR_RED)) setButton(4,COLOR_OFF);
         else setButton(4,COLOR_RED);
      }
      else if (qTick<180) {
         setButton(4,COLOR_RED);
      }
      else if (qTick<200) {
         if (isButton(5,COLOR_RED)) setButton(5,COLOR_OFF);
         else setButton(5,COLOR_RED);
      }
      else if (qTick<220) {
         setButton(5,COLOR_RED);
      }
      if (GlobalCommand.Output.Status.AllDrivesEnable && stacker.reading.startUpSeq == 15) {
         qTick = 1000001; 
         clearAllErrors();
      }
      lastPendingDeliverToggle = cutter.reading.pendingDeliverToggle;
      return;
   }

   if (!GlobalCommand.Output.Status.AllDrivesEnable) {
      return;
      streamer.reading.mainState = 0;
      setButton(1,COLOR_OFF);
      setButton(2,COLOR_OFF);
      setButton(3,COLOR_OFF);
      setButton(4,COLOR_OFF);
      setButton(5,COLOR_OFF);
   }
   
   minute += 0.00416666667;
   if (minute>1) {
      stacker.reading.tripCounter.minutes++;
      minute -= 1;
   }
   
   streamer.reading.function.btn1 = 0;
   streamer.reading.function.btn2 = 0;
   streamer.reading.function.btn3 = 0;
   streamer.reading.function.btn4 = 0;
   streamer.reading.function.btn5 = 0;

   switch (streamer.reading.mainState)
   {
      case INIT:
         if (stacker.reading.input.topCoverClosed) setButton(1,COLOR_WHITE); //only show if covers are closed
         else setButton(1,COLOR_OFF);
         setButton(2,COLOR_OFF);
         if (stackerY.homed && stacker.reading.input.topCoverClosed) {
            setButton(3,COLOR_YELLOW);
            streamer.reading.function.btn3 = 7; //table down
         }
         else {
            setButton(3,COLOR_OFF);
            streamer.reading.function.btn3 = 0;
         }
         setButton(4,COLOR_OFF);
         setButton(5,COLOR_OFF);
         //show the correct text in GUI
         streamer.reading.function.btn1 = 1; //load
         streamer.reading.function.btn2 = 0;
         streamer.reading.function.btn3 = 0;
         streamer.reading.function.btn4 = 0;
         streamer.reading.function.btn5 = 0;
         deliverNextState = 0;
         break;
      case STATE_LOAD:
         setButton(1,COLOR_OFF);
         setButton(2,COLOR_OFF);
         setButton(3,COLOR_OFF);
         setButton(4,COLOR_OFF);
         setButton(5,COLOR_OFF);
         //show the correct text in GUI
         streamer.reading.function.btn1 = 0; 
         streamer.reading.function.btn2 = 0;
         streamer.reading.function.btn3 = 0;
         streamer.reading.function.btn4 = 0;
         streamer.reading.function.btn5 = 0;
         deliverNextState = 0;
         break;
      case RUN:
         setButton(1,COLOR_RED); //stop
         if (cutter.reading.setSpeed == 0) setButton(2,COLOR_YELLOW); 
         else if (fixedJogger2) {
                setButton(2,COLOR_WHITE);
         }
         else setButton(2,COLOR_OFF);
   
         if (cutter.reading.setSpeed > 0 && fixedJogger2) 
            setButton(3,COLOR_WHITE); 
         else
            setButton(3,COLOR_OFF);
         
         if (streamer.reading.feedToStack != 0 || streamer.reading.deliverOnTheFly != 0 || streamer.reading.deliverSheetPos != 0) deliverNextState = 0;
         
         if (streamer.reading.feedToStack == 0 && streamer.reading.deliverOnTheFly == 0 && deliverRequest.state == 1 )
            setButton(4,COLOR_GREEN);
         else 
            setButton(4,COLOR_OFF);

         if (streamer.reading.feedToStack) setButton(5,COLOR_OFF); 
         else if (buffer.reading.usage<30 || inSpeed.is < 0.1) setButton(5,COLOR_MAGENTA); //deliver now
         else setButton(5,COLOR_OFF); 
         
         
         
         
         
         
         
            //show the correct text in GUI
            streamer.reading.function.btn1 = 2; //stop
         streamer.reading.function.btn2 = 0;
         streamer.reading.function.btn3 = 0;
         streamer.reading.function.btn4 = 0;
         streamer.reading.function.btn5 = 0;
         if (isButton(2,COLOR_WHITE)) streamer.reading.function.btn2 = 10; //jogger 2 ccw
         if (isButton(2,COLOR_YELLOW)) streamer.reading.function.btn2 = 3; //feed
         if (isButton(3,COLOR_WHITE)) streamer.reading.function.btn3 = 11; //jogger 2 cw
         if (isButton(4,COLOR_GREEN)) streamer.reading.function.btn4 = 4; //next
         if (isButton(5,COLOR_MAGENTA)) streamer.reading.function.btn5 = 5; //deliver
         break;
      case STOPPED:
         if (stacker.reading.input.topCoverClosed) {
            setButton(1,COLOR_WHITE);        
            setButton(2,COLOR_YELLOW);
            if (isButton(3,COLOR_RED)) setButton(3,COLOR_YELLOW);
            else setButton(3,COLOR_RED);
            setButton(4,COLOR_OFF);
            setButton(5,COLOR_MAGENTA);
         
            //show the correct text in GUI
            streamer.reading.function.btn1 = 6; //start
            streamer.reading.function.btn2 = 3; //feed
            streamer.reading.function.btn3 = 7; //table down
            streamer.reading.function.btn4 = 0; 
            streamer.reading.function.btn5 = 5; //deliver
         }
         else
         {
            setButton(1,COLOR_OFF);
            setButton(2,COLOR_OFF);
            setButton(3,COLOR_OFF);
            setButton(4,COLOR_YELLOW); //feed taglia
            setButton(5,COLOR_OFF);
            if (!streamer.reading.forkTableTransaction && !streamer.reading.stackingOnForks && streamer.setting.forkBackTapPos > 0)
            {
               if (forkX.position != streamer.setting.forkBackTapPos-4.0)
               {
                  forkX.absMove = streamer.setting.forkBackTapPos-4.0;
                  if (forkX.absMove == 0) forkX.goHome = true;
               
               }
            } 
            streamer.reading.function.btn4 = 9;
         }
         break;
      case ERROR:
         if (isButton(1,COLOR_RED))
         {
            setButton(1,COLOR_OFF);
            setButton(2,COLOR_OFF);
            setButton(3,COLOR_OFF);
            setButton(4,COLOR_OFF);
         }   
         else
         {
            setButton(1,COLOR_RED);
            setButton(2,COLOR_RED);
            setButton(3,COLOR_RED);
            setButton(4,COLOR_RED);
         }
         setButton(5,COLOR_BLUE);
         streamer.reading.function.btn1 = 0;
         streamer.reading.function.btn2 = 0;
         streamer.reading.function.btn3 = 0;
         streamer.reading.function.btn4 = 0;
         streamer.reading.function.btn5 = 8; //error ack
         break;
   }
}
void buttonCommand(void)
{
   BOOL btn1;
   BOOL btn2;
   BOOL btn3;
   BOOL btn4;
   BOOL btn5;
   static BOOL lastFeedBtn;
   static REAL timer;

   if (s52button.push1) btn1 = TRUE;
   else if (streamer.command.pressButton1) btn1 = TRUE;
   else btn1 = FALSE;
   streamer.command.pressButton1 = FALSE;

   if (s52button.push2) btn2 = TRUE;
   else if (streamer.command.pressButton2) btn2 = TRUE;
   else btn2 = FALSE;
   streamer.command.pressButton2 = FALSE;

   if (s52button.push3) btn3 = TRUE;
   else if (streamer.command.pressButton3) btn3 = TRUE;
   else btn3 = FALSE;
   streamer.command.pressButton3 = FALSE;

   if (s52button.push4) btn4 = TRUE;
   else if (streamer.command.pressButton4) btn4 = TRUE;
   else btn4 = FALSE;
   streamer.command.pressButton4 = FALSE;

   if (s52button.push5) btn5 = TRUE;
   else if (streamer.command.pressButton5) btn5 = TRUE;
   else btn5 = FALSE;
   streamer.command.pressButton5 = FALSE;
   
   if (streamer.command.confirmGlue)
   {
      streamer.reading.glueConfirmButton = HIDE;
      streamer.command.confirmGlue = false;
      streamer.reading.glueConfirmButton = HIDE;
      cleanGlueStandStillTime = 0;
   }


   switch (streamer.reading.mainState)
   {
      case INIT:
         streamer.command.manFeed = FALSE;
         feedAligner = false;
         if (stacker.reading.input.manFeed) {
         cutter.command.feed = TRUE;
         if (!stacker.reading.input.enterTransport && aligner.Status.DriveStatus.HomingOk/* && stacker.reading.input.topCoverClosed*/) feedAligner = true;
         //if (aligner.Status.DriveStatus.HomingOk && stacker.reading.input.alignerTopCoverClosed) paperTransport[ALIGNER].setSpeed = 0.2;
         }
         if (!stacker.reading.input.manFeed && lastFeedBtn && cutter.command.feed) {
            cutter.command.feed = FALSE;
            paperTransport[ALIGNER].setSpeed = 0.0;
         }
         
         if (isButton(1,COLOR_WHITE) && btn1) {
            timer += SECONDS_UNIT;
            
            if (timer > 0.25 && timer < (0.25 + SECONDS_UNIT)) globalPower = TRUE;
            if (timer > 0.30 && timer < (0.30 + SECONDS_UNIT)) ebmGlue.command.sendAll = true;
            if ((timer > 0.35 && timer < (0.35 + SECONDS_UNIT)) && buffer.reading.status.state == Status_NotLoaded && buffer.reading.input.coverClosed) buffer.command.load = TRUE; 
            if ((timer > 0.45 && timer < (0.45 + SECONDS_UNIT)) && folder.reading.status.state == Status_NotLoaded && folder.reading.coversClosed) folder.command.pressInfeedButton1  = TRUE;
            if (timer > 1.7 && timer < (1.7 + SECONDS_UNIT)) {
               if (safety.reading.allCoversClosed) cutter.command.home = TRUE;
               lineController.command.beep = TRUE;
            } 
         }
         
         if (isButton(3,COLOR_YELLOW) && btn3 && !s52button.lastPush3)
         {
            if (stackerY.homed && stackerY.position>0)
            {
               stackerY.move = -10;
            }
         }
         
         
         if (isButton(1,COLOR_WHITE) && !btn1 && timer>0) {
            timer = 0;
            streamer.command.load = TRUE;
            //ful fix
            ebmGlue.setting.forceOutputEnable.out1 = FALSE;
            ebmGlue.setting.forceOutputEnable.out2 = FALSE;
            ebmGlue.setting.forceOutputValue.out1 = FALSE;
            ebmGlue.setting.forceOutputValue.out1 = FALSE;
            if (ebmGlue.setting.glueGun.pattern > 0)
            {
               ebmGlue.setting.glueGun.enable = TRUE;
               ebmGlue.command.glueGun.start = TRUE;
            }
            if (ebmGlue.setting.offset.enable)
            {
               ebmGlue.command.offset.start = TRUE;
            }
            ebmGlue.command.sendAll = TRUE;
         }

         break;
      case STATE_LOAD:
         feedAligner = false;
         streamer.command.manFeed = FALSE;
         timer = 0;
         break;
      case RUN:
         timer = 0;
         
         if (endPageInStream && inSpeed.is == 0/* && paperTransport[ALIGNER].setSpeed < stacker.setting.transportIdleSpeed*/) {
            streamer.command.feedToStack = TRUE;
            endPageInStream = false;
            if (!lineController.command.slowSpeed) lineController.command.slowSpeed = true;
         }
         
         
         if ((stacker.reading.input.manFeed && cutter.reading.setSpeed == 0) || (isButton(2,COLOR_YELLOW) && btn2)) streamer.command.manFeed = TRUE;
         else streamer.command.manFeed = FALSE;
         
         if (isButton(1,COLOR_RED) && btn1 && !s52button.lastPush1) streamer.command.stop = TRUE;
         if (isButton(2,COLOR_YELLOW) && btn2) streamer.command.manFeed = TRUE;
         if (fixedJogger2) {
            if (isButton(2,COLOR_WHITE) && btn2 && !s52button.lastPush2) {
               streamer.setting.jogger2Position += 0.5;
               if (streamer.setting.jogger2Position == 0.0) streamer.setting.jogger2Position = 0.1;
            }
            if (isButton(3,COLOR_WHITE) && btn3 && !s52button.lastPush3) {
               streamer.setting.jogger2Position -= 0.5;
               if (streamer.setting.jogger2Position == 0.0) streamer.setting.jogger2Position = -0.1;
            }
         }
         
         if (isButton(4,COLOR_GREEN) && btn4 && !s52button.lastPush4 ) {
            deliverRequest.deliverNextPressed = true;  //cutter.command.deliverNextRequest = TRUE;
            dbgTxt("5 delNext TRUE btn");
         }
         if (isButton(5,COLOR_MAGENTA) && btn5 && !s52button.lastPush5 )
         {
            if (inSpeed.is == 0) streamer.command.feedToStack = TRUE;
            else deliverRequest.deliverNowPressed = TRUE;
            
            triggerTime = 10.0;
            
            
         }
         break;
      case STOPPED:
         timer = 0;
         
         if (isButton(1,COLOR_WHITE) && btn1 && !s52button.lastPush1) streamer.reading.mainState = RUN;
         
         if ((stacker.reading.input.manFeed && cutter.reading.setSpeed == 0) || (isButton(2,COLOR_YELLOW) && btn2)) streamer.command.manFeed = TRUE;
         else streamer.command.manFeed = FALSE;
         
         if ((isButton(3,COLOR_RED) || isButton(3,COLOR_YELLOW)) && btn3 && !s52button.lastPush3) streamer.command.unLoad = TRUE;
         if (isButton(4,COLOR_YELLOW) && btn4) //feed taglia
         {
            feedTaglia = true;
         }
         else feedTaglia = false;
         if (isButton(5,COLOR_MAGENTA) && btn5 && !s52button.lastPush5 )
         {
            streamer.command.feedToStack = TRUE;
            goBackToStopped = TRUE;
         }
         break;
      case ERROR:
         timer = 0;
         if (isButton(5,COLOR_BLUE) && btn5 && !s52button.lastPush5) {
            
            machineError.active = FALSE;
         }
         if (machineError.active == FALSE) {//some one cleared the error from GUI
            streamer.command.errAck = TRUE;
         }
         break;
   }

   s52button.lastPush1 = btn1;
   s52button.lastPush2 = btn2;
   s52button.lastPush3 = btn3;
   s52button.lastPush4 = btn4;
   s52button.lastPush5 = btn5;
   lastFeedBtn = stacker.reading.input.manFeed;

}

void getCutterSpeed(void)
{
   inSpeed.decelerate = FALSE;
   inSpeed.accelerate = FALSE;
   inSpeed.is = cutter.reading.webSpeed;
   
   if (streamer.command.manFeed) inSpeed.is = 0.2;
   
   if (inSpeed.is < 0.02) inSpeed.is = 0.0;

   inSpeed.set = cutter.reading.setSpeed;

   if (inSpeed.is > inSpeed.set*1.015) inSpeed.decelerate = TRUE;
   else if (inSpeed.is < inSpeed.set*0.985) inSpeed.accelerate = TRUE;

   if (inSpeed.accelerate) {
      if (inSpeed.is<0.05 && cutter.setting.masterAcc>2600)
         inSpeed.predict = inSpeed.is + (3.12 * SECONDS_UNIT);
      else
         inSpeed.predict = inSpeed.is + (cutter.setting.masterAcc * 0.0012 * SECONDS_UNIT);
   
      //inSpeed.follow = (inSpeed.set + inSpeed.predict) / 2;
      inSpeed.follow = (inSpeed.set + inSpeed.predict + inSpeed.is*3) / 5;
      if (inSpeed.follow > inSpeed.predict*1.05) inSpeed.follow = inSpeed.predict*1.05;
      
      inSpeed.follow = (inSpeed.is * 2 + inSpeed.follow) / 3;
   }
   else if (inSpeed.decelerate) {
      inSpeed.predict = inSpeed.is - (cutter.setting.masterDec * 0.0012 * SECONDS_UNIT);
      //inSpeed.follow = inSpeed.is > inSpeed.set ? inSpeed.is:(inSpeed.set+inSpeed.is*2)/3;
      inSpeed.follow = inSpeed.is > inSpeed.set ? inSpeed.is:(inSpeed.set+inSpeed.is*7)/8;
   }
   else {
      inSpeed.follow = inSpeed.predict = (inSpeed.set + inSpeed.is*3)/4;
   }
}

void setButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
   if (btnNo == 1) {
      s52button.color1.green 	= grn;
      s52button.color1.red   	= red;
      s52button.color1.yellow  = yel;
      s52button.color1.white 	= whtBlu;
      if      (s52button.color1.yellow && s52button.color2.red) streamer.reading.button1 = _ORANGE;
      else if (s52button.color1.yellow)                         streamer.reading.button1 = _YELLOW;
      else if (s52button.color1.red)                            streamer.reading.button1 = _RED;
      else if (s52button.color1.green)                          streamer.reading.button1 = _GREEN;
      else if (s52button.color1.white)                          streamer.reading.button1 = _WHITE;
      else                                                      streamer.reading.button1 = _OFF;
   }
   if (btnNo == 2) {
      s52button.color2.green 	= grn;
      s52button.color2.red   	= red;
      s52button.color2.yellow  = yel;
      s52button.color2.white 	= whtBlu;
      if      (s52button.color2.yellow && s52button.color2.red) streamer.reading.button2 = _ORANGE;
      else if (s52button.color2.yellow)                         streamer.reading.button2 = _YELLOW;
      else if (s52button.color2.red)                            streamer.reading.button2 = _RED;
      else if (s52button.color2.green)                          streamer.reading.button2 = _GREEN;
      else if (s52button.color2.white)                          streamer.reading.button2 = _WHITE;
      else                                                      streamer.reading.button2 = _OFF;
   }
   if (btnNo == 3) {
      s52button.color3.green 	= grn;
      s52button.color3.red   	= red;
      s52button.color3.yellow  = yel;
      s52button.color3.white 	= whtBlu;
      if      (s52button.color3.yellow && s52button.color2.red) streamer.reading.button3 = _ORANGE;
      else if (s52button.color3.yellow)                         streamer.reading.button3 = _YELLOW;
      else if (s52button.color3.red)                            streamer.reading.button3 = _RED;
      else if (s52button.color3.green)                          streamer.reading.button3 = _GREEN;
      else if (s52button.color3.white)                          streamer.reading.button3 = _WHITE;
      else                                                      streamer.reading.button3 = _OFF;
   }
   if (btnNo == 4) {
      s52button.color4.green 	= grn;
      s52button.color4.red   	= red;
      s52button.color4.yellow  = yel;
      s52button.color4.white 	= whtBlu;
      if      (s52button.color4.yellow && s52button.color2.red) streamer.reading.button4 = _ORANGE;
      else if (s52button.color4.yellow)                         streamer.reading.button4 = _YELLOW;
      else if (s52button.color4.red)                            streamer.reading.button4 = _RED;
      else if (s52button.color4.green)                          streamer.reading.button4 = _GREEN;
      else if (s52button.color4.white)                          streamer.reading.button4 = _WHITE;
      else                                                      streamer.reading.button4 = _OFF;
   }
   if (btnNo == 5) {
      s52button.color5.green 	= grn;
      s52button.color5.red   	= red;
      s52button.color5.yellow  = yel;
      s52button.color5.blue 	= whtBlu;
      if (s52button.color5.yellow && s52button.color5.red)      streamer.reading.button5 = _ORANGE;
      else if (s52button.color5.blue && s52button.color5.red)   streamer.reading.button5 = _MAGENTA;
      else if (s52button.color5.green && s52button.color5.blue) streamer.reading.button5 = _TURQUOISE;
      else if (s52button.color5.yellow)                         streamer.reading.button5 = _YELLOW;
      else if (s52button.color5.red)                            streamer.reading.button5 = _RED;
      else if (s52button.color5.green)                          streamer.reading.button5 = _GREEN;
      else if (s52button.color5.blue)                           streamer.reading.button5 = _BLUE;
      else                                                      streamer.reading.button5 = _OFF;
   }	
}

BOOL isButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
   if (btnNo == 1) {
      if (   grn == s52button.color1.green 
         && red == s52button.color1.red 
         && yel == s52button.color1.yellow
         && whtBlu == s52button.color1.white) return 1;
      if (   grn && red && yel && whtBlu 
         && (s52button.color1.green || s52button.color1.red || s52button.color1.yellow || s52button.color1.white)) return 1;
      return 0;
   }
   if (btnNo == 2)	{
      if (   grn == s52button.color2.green 
         && red == s52button.color2.red 
         && yel == s52button.color2.yellow
         && whtBlu == s52button.color2.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (s52button.color2.green || s52button.color2.red || s52button.color2.yellow || s52button.color2.white)) return 1;
      return 0;
   }
   if (btnNo == 3)	{
      if (   grn == s52button.color3.green 
         && red == s52button.color3.red 
         && yel == s52button.color3.yellow
         && whtBlu == s52button.color3.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (s52button.color3.green || s52button.color3.red || s52button.color3.yellow || s52button.color3.white)) return 1;
      return 0;
   }
   if (btnNo == 4)	{
      if (   grn == s52button.color4.green 
         && red == s52button.color4.red 
         && yel == s52button.color4.yellow
         && whtBlu == s52button.color4.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (s52button.color4.green || s52button.color4.red || s52button.color4.yellow || s52button.color4.white)) return 1;
      return 0;
   }
   if (btnNo == 5)	{
      if (   grn == s52button.color5.green 
         && red == s52button.color5.red 
         && yel == s52button.color5.yellow
         && whtBlu == s52button.color5.blue) return 1;
      if (   grn && red && yel && whtBlu 
         && (s52button.color5.green || s52button.color5.red || s52button.color5.yellow || s52button.color5.blue)) return 1;
      return 0;
   }
   return 0;
}

REAL alignerSpeedCalculation (void)
{
   REAL speed = 0.0;
   USINT i;
   for (i=0;i<ALIGN_SPEED_QUEUE_SIZE;i++) {
      if (alignerFloorSpeed[i].timer >= 0.0) alignerFloorSpeed[i].timer -= SECONDS_UNIT;
      if (alignerFloorSpeed[i].timer > 0.0 && alignerFloorSpeed[i].speed>speed) {
         speed = alignerFloorSpeed[i].speed;
      }
   }
   return speed;
}


REAL connectStreamer(BOOL reset, BOOL bInputSensor, REAL sensorDist, REAL alignerSpeed, REAL format, REAL overlapDistance, REAL cycleTime, REAL alignerOverSpeed)
{
   static REAL alignerDist;

   if (reset) {
      alignerDist = 0;
      return 0;
   }
 
   if (!bInputSensor) alignerDist = sensorDist;
 
   if (alignerDist > 0) 
   {
      alignerDist -= alignerSpeed*cycleTime;
      
      return ((alignerSpeed * overlapDistance)/format)/alignerOverSpeed;
   }
   return 0;
}

REAL stackerStep (REAL tblStp, REAL sensVal, REAL strSpd, REAL ovrLap, BOOL gapFeed)
{
   static REAL lastI;
   static BOOL gf2step;

   tblStep.stpCnt += strSpd * 2.4;
   tblStep.timer -= 2.4;
   if (tblStep.lastTblStep != tblStp) tblStep.I = 0; //table steps changed reset the I part
   tblStep.lastTblStep = tblStp;
   if (strSpd>0 && tblStep.timer<=0) //collect measurements
   {
      tblStep.measurements[tblStep.p] = sensVal; 
      tblStep.p++;
      if (tblStep.p==25) {
         tblStep.p = 0;
         tblStep.w = true;
      }   
      tblStep.timer = 1/strSpd;
      if (tblStep.timer>100) tblStep.timer = 100;
   }
   

   while (tblStep.stpCnt>ovrLap)
   {
      REAL meas = 0;
      REAL measMax = -4.5;
      REAL measMin = 4.5;
      USINT i;
      USINT errCnt;
      
      //if (streamerGlueQueue(peek) == noGlue) lineController.command.doubleBeep;
      streamerGlueQueue(removeLast);  
      
      if (tblStep.w) tblStep.p = 25;
      if (tblStep.p>2)
      {
         for (i=0;i<(tblStep.p-1);i++)
         {
            meas += tblStep.measurements[i];
            if (tblStep.measurements[i]>measMax) measMax = tblStep.measurements[i];
            if (tblStep.measurements[i]<measMin) measMin = tblStep.measurements[i];
         }
         //meas = (meas-measMin+sensVal)/(p-1);
         //meas = (meas + measMax)/2;    
         meas = (meas-measMax+sensVal)/(tblStep.p-1);
         meas = (meas + measMin * 2 + lowest(sensVal,tblStep.MV,meas))/4;    
      }
      else if (tblStep.p == 0) meas = sensVal;
      else if (tblStep.p == 1) meas = (tblStep.measurements[0] + sensVal)/2;
      else if (tblStep.p == 2) meas = (tblStep.measurements[0] + tblStep.measurements[1] + sensVal)/3;
      
      meas -= (stacker.setting.holdingPositionGradient*stacker.reading.stackSize)/100; 
      meas -= stacker.setting.holdPostition;
      stacker.reading.stepSensorSetPoint = (stacker.setting.holdingPositionGradient*stacker.reading.stackSize)/100;
      stacker.reading.stepSensorSetPoint += stacker.setting.holdPostition;
      static REAL lE;
      REAL E,D;
      //regulator settings
      static REAL KI;
      static REAL KD;
      static REAL KP;

      
      
      
      
      if (gapFeed) KP = 0.80;
      else KP = 0.953;
      
      /*
      if (streamer.setting.stepSensorNo == 2) //hopefully more accurate value
         KP += 0.017;
      */
      
      KI = 0.051;
      KD = 0.0012;
      
      tblStep.NM = tblStep.p;
      tblStep.MV = meas;

      REAL MaxAdj = tblStp *  (0.2 + tblStep.limitAdj);
      REAL MinAdj = tblStp * (-0.2 + tblStep.limitAdj);
       
      //PID
      E = meas; //-
      if (distanceAfterGapSection2 > 1.0 && distanceAfterGapSection2 < 1000) E = meas - streamer.setting.lastSheetStepPosition;  //adjust stack hold pos the last sheets
      
      tblStep.I = tblStep.I + E*(gapFeed?0.05:1);
      D = (E - lE);
      tblStep.adj = KP*E + KI*tblStep.I + KD*D;
      lE = E;
      //Cap it
      if (tblStep.adj > MaxAdj) {
         tblStep.adj = MaxAdj;
         if (tblStep.limitAdj<0.10) tblStep.limitAdj += 0.025;
      }
      if (tblStep.adj < MinAdj) {
         tblStep.adj = MinAdj;
         if (tblStep.limitAdj>-0.10) tblStep.limitAdj -= 0.025;
      }
      
      if (tblStep.I > 8.0)  tblStep.I = 8.0; //4
      if (tblStep.I < -8.0) tblStep.I = -8.0; //4
      //handle step sensor error
      /*if (rabs(tblStep.I) > 7.95) { //4.95
      errCnt++;
      if (rabs(meas) > 1.0) errCnt++;
      if (rabs(meas) > 2.0) errCnt++;
      if (rabs(meas) >= 2.5) errCnt++;
      if (errCnt > 200 && rabs(meas) > 1.5)
      {
      tblStep.I = 0;
      errCnt = 0;
      addError(STEP_SENSOR,NORMAL_STOP,CONTINUE_LOADED,0);
      }
      }*/
      if (rabs(tblStep.I) < 0.4 &&  errCnt) errCnt--;
      if (rabs(tblStep.I) < 1.0 &&  errCnt) errCnt--; 
      if (rabs(tblStep.I) < 2.0 &&  errCnt) errCnt--; 
      if (rabs(meas) < 0.15 && errCnt) errCnt--;
      //
      if (errCnt > 50) streamer.reading.tableStepWarning = true;
      else streamer.reading.tableStepWarning = false;
       
      //do stacker step tblStp+adj
      // when running on table
      
      if (!streamer.reading.forkTableTransaction && !streamer.reading.stackingOnForks && streamer.reading.deliverOnTheFly != 1 && streamer.reading.deliverOnTheFly != 2)
      {
         if (streamer.setting.forkBackTapPos > 0)
         {
            if (streamer.setting.forkBackTapPos > 25)
            {
               if (forkX.position != 17) forkX.absMove = 17;
               else {
                  forkX.absMove = streamer.setting.forkBackTapPos-4.0;
                  if (forkX.absMove == 0) forkX.goHome = true;
               }
            }
            else if (streamer.setting.forkBackTapPos > 15)
            {
               if (forkX.position != 5) forkX.absMove = 5;
               else {
                  forkX.absMove = streamer.setting.forkBackTapPos-4.0;
                  if (forkX.absMove == 0) forkX.goHome = true;
               }
            }
            else
            {
               if (forkX.position != -4.0) forkX.absMove = -4.0;
               else {
                  forkX.absMove = streamer.setting.forkBackTapPos-4.0;
                  if (forkX.absMove == 0) forkX.goHome = true;
               }
            }
            
         }
      }
      //retract fingers before table down
      if (streamer.reading.deliverOnTheFly == 1 || (streamer.reading.deliverOnTheFly == 2 && !streamer.reading.stackingOnForks))
      {
         if (streamer.setting.forkBackTapPos > 0)
            forkX.absMove = streamer.setting.forkBackTapPos-4.0;
      }
      //retract fingers before table down ^
      if (streamer.reading.deliverOnTheFly == 0)
      {
         if (stackerY.position < (stacker.setting.deliveryPosition + 20.0)) {
            addError(OVER_MAX_STACK,NORMAL_STOP,TRUE,(cutter.command.deliverNextRequest * 10000 + deliverRequest.state * 1000 + deliverRequest.sheetsInState));
         }
      }
      if (streamer.reading.stackingOnForks) sheetsOnForks++;
      
      if (streamer.reading.deliverOnTheFly == 0 || streamer.reading.deliverOnTheFly > 5) {
         lastI = tblStep.I;
         if (gapFeed)
         {
            if (gf2step)
            {
               stackerY.move = (tblStp + tblStep.adj) * -2;
               gf2step=false;
            }
            else gf2step=true;
         }
         else  { 
            if ((distanceAfterGapSection2 > 1.0 && distanceAfterGapSection2 < 1000) || (streamer.reading.feedToStack == 1 && stacker.reading.input.exitStreamerSection1)) { //press harder/looser the last sheets before table down
               //fly deliver ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^     stopped deliver ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
               REAL s;
               s = streamer.setting.lastSheetStepPosition - sensVal;
               if (s > 0.1) s = 0.1;
               if (s < -1.0) s = -1.0;
               stackerY.move = s;
            }
            else   {
               if (distanceSinceOffset < streamer.setting.offsetBlindDistance)
               {
                  stackerY.move = -(tblStp + tblStep.adj*0.5);
                  tblStep.I *= 0.9;
               }
               else
                  stackerY.move = -(tblStp + tblStep.adj);
            }
         }
      }
      
      if (streamer.reading.deliverOnTheFly == 5)
      {
         tblStep.I = lastI;             //slightly increased + small sensor adjust
         stackerY.move = forkY.move = - (tblStp * 1.025 + tblStep.adj * 0.1);         
      }
      
      if (streamer.reading.deliverOnTheFly == 3 || streamer.reading.deliverOnTheFly == 4) {
         tblStep.I = lastI;
         if (sheetsOnForks > 20)
            forkY.move = - (tblStp + tblStep.adj * 0.50);
         else if (sheetsOnForks > 10)
            forkY.move = - (tblStp + tblStep.adj * 0.25);
         else if (sheetsOnForks > 5)
            forkY.move = - (tblStp + tblStep.adj * 0.1);
         else
            forkY.move = -tblStp;
         //if (forkY.position < -19.0 && stacker.reading.input.homeForkLift) addError(FORK_OVERFULL,NORMAL_STOP,TRUE,0); //limit was -12.0, and no homing sensor check
         if (forkY.position < -20.0 && stacker.reading.input.homeForkLift) addError(FORK_OVERFULL,NORMAL_STOP,TRUE,0); //limit was -12.0, and no homing sensor check
      }
      
      tblStep.lastSteps[tblStep.lsP] = (tblStp + tblStep.adj);
      tblStep.lsP++;
      if (tblStep.lsP>=25) tblStep.lsP = 0;
      stacker.reading.stepAvg = 0;
      for (i=0;i<25;i++) stacker.reading.stepAvg += tblStep.lastSteps[i];
      stacker.reading.stepAvg = stacker.reading.stepAvg/25;

      tblStep.stpCnt -= ovrLap;
      if (strSpd>0.025)
         tblStep.timer = 2.5/strSpd; //wait 2.5 milimeter till next meaurement 
      else tblStep.timer = 100;

      tblStep.p = 0;
      tblStep.w = false;
   }
   return tblStep.adj;
}


USINT directHomeServos(void)
{
   USINT retVal = 0;
   static BOOL deliverCmd;
   static BOOL jogger1Cmd;
   static BOOL jogger2Cmd;
   static BOOL jogger3Cmd;
   static BOOL tagliaCmd;
   static BOOL alignerCmd;
   static BOOL strSection1Cmd;
   static BOOL strSection2Cmd;
   
   //////////////////////
   if (deliver.Status.ErrorID > 0)
   {
      if (!deliverCmd)
      {
         if (deliver.Command.Power == TRUE)
            deliver.Command.Power = FALSE;
         else
            deliver.Command.ErrorAcknowledge = TRUE;
         deliverCmd = TRUE;
      }
      else deliverCmd = FALSE;
      retVal = 1;
      dirHomeErrorCnt[DELIVER]++;
   }
   else if (deliver.Command.Power == FALSE)
   {
      deliver.Command.Power = TRUE;
      deliverCmd = TRUE;
      retVal = 2;
   }
   else if (/*deliver.Status.ActPosition != 0 && */!deliver.Status.DriveStatus.HomingOk) 
   {
      if (!deliverCmd)
      {
         if (!deliver.AxisState.Homing) {
            deliver.Command.Home = TRUE;
         }
         deliverCmd = TRUE;
      }
      else deliverCmd = FALSE; 
      retVal = 3;
   }
   else
   {
      if (deliverCmd)
      {
         deliverCmd = FALSE;
         retVal = 4;
      }
   }

   if (jogger1.Status.ErrorID > 0)
   {
      if (!jogger1Cmd)
      {
         if (jogger1.Command.Power == TRUE)
            jogger1.Command.Power = FALSE;
         else
            jogger1.Command.ErrorAcknowledge = TRUE;
         jogger1Cmd = TRUE;
      }
      else jogger1Cmd = FALSE;
      retVal = 5;
      dirHomeErrorCnt[JOGGER1]++;
   }
   else if (jogger1.Command.Power == FALSE)
   {
      jogger1.Command.Power = TRUE;
      jogger1Cmd = TRUE;
      retVal = 6;
   }
   else if (!jogger1.Status.DriveStatus.HomingOk) 
   {
      if (!jogger1Cmd)
      {
         if (!jogger1.AxisState.Homing) {
            jogger1.Command.Home = TRUE;
         }
         jogger1Cmd = TRUE;
      }
      else jogger1Cmd = FALSE; 
      retVal = 7;
   }
   else
   {
      if (jogger1Cmd)
      {
         jogger1Cmd = FALSE;
         retVal = 8;
      }
   }
   
   if (jogger2.Status.ErrorID > 0)
   {
      if (!jogger2Cmd)
      {
         if (jogger2.Command.Power == TRUE)
            jogger2.Command.Power = FALSE;
         else
            jogger2.Command.ErrorAcknowledge = TRUE;
         jogger2Cmd = TRUE;
      }
      else jogger2Cmd = FALSE;
      retVal = 5;
      dirHomeErrorCnt[JOGGER2]++;
   }
   else if (jogger2.Command.Power == FALSE)
   {
      jogger2.Command.Power = TRUE;
      jogger2Cmd = TRUE;
      retVal = 6;
   }
   else if (!jogger2.Status.DriveStatus.HomingOk) 
   {
      if (!jogger2Cmd)
      {
         if (!jogger2.AxisState.Homing) {
            jogger2.Command.Home = TRUE;
         }
         jogger2Cmd = TRUE;
      }
      else jogger2Cmd = FALSE; 
      retVal = 7;
   }
   else
   {
      if (jogger2Cmd)
      {
         jogger2Cmd = FALSE;
         retVal = 8;
      }
   }
   
   if (aligner.Status.ErrorID > 0)
   {
      if (!alignerCmd)
      {
         if (aligner.Command.Power == TRUE)
            aligner.Command.Power = FALSE;
         else
            aligner.Command.ErrorAcknowledge = TRUE;
         alignerCmd = TRUE;
      }
      else alignerCmd = FALSE;
      retVal = 15;
      dirHomeErrorCnt[ALIGNER]++;
   }
   else if (aligner.Command.Power == FALSE)
   {
      aligner.Command.Power = TRUE;
      alignerCmd = TRUE;
      retVal = 16;
   }
   else if (/*aligner.Status.ActPosition != 0 && */!aligner.Status.DriveStatus.HomingOk) 
   {
      if (!alignerCmd)
      {
         if (!aligner.AxisState.Homing) {
            aligner.Command.Home = TRUE;
         }
         alignerCmd = TRUE;
      }
      else alignerCmd = FALSE; 
      retVal = 17;
   }
   else
   {
      if (alignerCmd)
      {
         alignerCmd = FALSE;
         retVal = 18;
      }
   }
   //////////////////////
   if (strSection1.Status.ErrorID > 0)
   {
      if (!strSection1Cmd)
      {
         if (strSection1.Command.Power == TRUE)
            strSection1.Command.Power = FALSE;
         else
            strSection1.Command.ErrorAcknowledge = TRUE;
         strSection1Cmd = TRUE;
      }
      else strSection1Cmd = FALSE;
      retVal = 19;
      dirHomeErrorCnt[STREAMER_SECTION_1]++;
   }
   else if (strSection1.Command.Power == FALSE)
   {
      strSection1.Command.Power = TRUE;
      strSection1Cmd = TRUE;
      retVal = 20;
   }
   else if (/*strSection1.Status.ActPosition != 0 && */!strSection1.Status.DriveStatus.HomingOk) 
   {
      if (!strSection1Cmd)
      {
         if (!strSection1.AxisState.Homing) {
            strSection1.Command.Home = TRUE;
         }
         strSection1Cmd = TRUE;
      }
      else strSection1Cmd = FALSE; 
      retVal = 21;
   }
   else
   {
      if (strSection1Cmd)
      {
         strSection1Cmd = FALSE;
         retVal = 22;
      }
   }
   //////////////////////
   if (strSection2.Status.ErrorID > 0)
   {
      if (!strSection2Cmd)
      {
         if (strSection2.Status.ErrorID == 1) 
            strSection2.Command.ErrorAcknowledge = TRUE;
         else if (strSection2.Command.Power == TRUE)
            strSection2.Command.Power = FALSE;
         else
            strSection2.Command.ErrorAcknowledge = TRUE;
         strSection2Cmd = TRUE;
      }
      else strSection2Cmd = FALSE;
      retVal = 23;
      dirHomeErrorCnt[STREAMER_SECTION_2]++;
   }
   else if (strSection2.Command.Power == FALSE)
   {
      strSection2.Command.Power = TRUE;
      strSection2Cmd = TRUE;
      retVal = 24;
   }
   else if (/*strSection2.Status.ActPosition != 0 && */!strSection2.Status.DriveStatus.HomingOk) 
   {
      if (!strSection2Cmd)
      {
         if (!strSection2.AxisState.Homing) {
            strSection2.Command.Home = TRUE;
         }
         strSection2Cmd = TRUE;
      }
      else strSection2Cmd = FALSE; 
      retVal = 25;
   }
   else
   {
      if (strSection2Cmd)
      {
         strSection2Cmd = FALSE;
         retVal = 26;
      }
   }
   
   //////////////////////
   if (taglia.Status.ErrorID > 0)
   {
      if (!tagliaCmd)
      {
         if (taglia.Command.Power == TRUE)
            taglia.Command.Power = FALSE;
         else
            taglia.Command.ErrorAcknowledge = TRUE;
         tagliaCmd = TRUE;
      }
      else tagliaCmd = FALSE;
      retVal = 27;
      dirHomeErrorCnt[TAGLIA]++;
   }
   else if (taglia.Command.Power == FALSE)
   {
      taglia.Command.Power = TRUE;
      tagliaCmd = TRUE;
      retVal = 28;
      
   }
   else if (/*taglia.Status.ActPosition != 0 && */!taglia.Status.DriveStatus.HomingOk) 
   {
      if (!tagliaCmd)
      {
         if (!taglia.AxisState.Homing) {
            taglia.Command.Home = TRUE;
         }
         //else if (taglia.Status.ErrorID > 0)
         //{
         //   addError(AXIS_TAGLIA ,NORMAL_STOP,TRUE,taglia.Status.ErrorID);
         //}
         tagliaCmd = TRUE;
      }
      else tagliaCmd = FALSE; 
      retVal = 29;
   }
   else
   {
      if (tagliaCmd)
      {
         tagliaCmd = FALSE;
         retVal = 30;
      }
   }
   
   if (lineController.setup.streamer.jogger3) {
      if (jogger3.Status.ErrorID > 0)
      {
         if (!jogger3Cmd)
         {
            if (jogger3.Command.Power == TRUE)
               jogger3.Command.Power = FALSE;
            else
               jogger3.Command.ErrorAcknowledge = TRUE;
            jogger3Cmd = TRUE;
         }
         else jogger3Cmd = FALSE;
         retVal = 65;
         dirHomeErrorCnt[JOGGER3]++;
      }
      else if (jogger3.Command.Power == FALSE)
      {
         jogger3.Command.Power = TRUE;
         jogger3Cmd = TRUE;
         retVal = 66;
      }
      else if (!jogger3.Status.DriveStatus.HomingOk) 
      {
         if (!jogger3Cmd)
         {
            if (!jogger3.AxisState.Homing) {
               jogger3.Command.Home = TRUE;
            }
            jogger3Cmd = TRUE;
         }
         else jogger3Cmd = FALSE; 
         retVal = 67;
      }
      else
      {
         if (jogger3Cmd)
         {
            jogger3Cmd = FALSE;
            retVal = 68;
         }
      }
   }
   
   return retVal;
}

USINT absoluteHomeServos(BOOL rst)
{
   static BOOL waitOne;
   static REAL homeingTimer;
   static USINT failCnt;
   if (rst) {
      absHomeStep = 0;
      failCnt = 0;
      homeingTimer = 0;
      return 99;
   }
   

   switch (absHomeStep)
   {
      case 0:
         if (strFingerPusher.Status.DriveStatus.HomingOk && strFingerLift.Status.DriveStatus.HomingOk && strStackerLift.Status.DriveStatus.HomingOk /*&& !reHomeAbsolute*/)
         {
            if (waitOne) waitOne = false;
            else if (strFingerPusher.Status.ErrorID !=0) waitOne = strFingerPusher.Command.ErrorAcknowledge = true;
            else if (strFingerLift.Status.ErrorID !=0) waitOne = strFingerLift.Command.ErrorAcknowledge = true;
            else if (strStackerLift.Status.ErrorID !=0) waitOne = strStackerLift.Command.ErrorAcknowledge = true;
            else {
               strFingerPusher.Parameter.Position = 0;
               strFingerLift.Parameter.Position = 0;
               strStackerLift.Parameter.Position = 0;
               strFingerPusher.Command.MoveAbsolute = true;
               strFingerLift.Command.MoveAbsolute = true;
               strStackerLift.Command.MoveAbsolute = true;
               absHomeStep = 6;
            }
         }   
         else {
            if (waitOne) {
               waitOne = false;
               absHomeStep = 1;
            }
            else {
               strFingerPusher.Command.Stop = true;
               strFingerLift.Command.Stop = true;
               strStackerLift.Command.Stop = true;
               waitOne = true;
            }  
         }
         break;
      case 1:
         if (waitOne) {
            waitOne = false;
            absHomeStep = 2;
         }
         else {
            strFingerPusher.Command.Stop = false;
            strFingerLift.Command.Stop = false;
            strStackerLift.Command.Stop = false;
            strFingerPusher.Command.Power = false;
            strFingerLift.Command.Power = false;
            strStackerLift.Command.Power = false;
            strFingerPusher.Command.ErrorAcknowledge = false;
            strFingerLift.Command.ErrorAcknowledge = false;
            strStackerLift.Command.ErrorAcknowledge = false;
            waitOne = true;
         }  
         break;
      case 2:
         if (waitOne) {
            waitOne = false;
            absHomeStep = 3;
         }
         else {
            strFingerPusher.Command.ErrorAcknowledge = true;
            strFingerLift.Command.ErrorAcknowledge = true;
            strStackerLift.Command.ErrorAcknowledge = true;
            waitOne = true;
         }  
         break;
      case 3:
         if (waitOne) {
            waitOne = false;
            absHomeStep = 4;
         }
         else {
            if ((strFingerPusher.Status.ErrorID !=0) ||
               (strFingerLift.Status.ErrorID !=0)    ||
               (strStackerLift.Status.ErrorID !=0)) {
               absHomeStep = 1;
            }
            else {
               strFingerPusher.Command.Power = true;
               strFingerLift.Command.Power = true;
               strStackerLift.Command.Power = true;
               waitOne = true;
            }
            
         }  
         break;
      case 4:
         if (waitOne) {
            waitOne = false;
            absHomeStep = 5;
         }
         else {
            strFingerPusher.Command.Home = true;
            strFingerLift.Command.Home = true;
            strStackerLift.Command.Home = true;
            waitOne = true;
         }  
         break;
      case 5:
         if (strFingerPusher.Status.DriveStatus.HomingOk && strFingerLift.Status.DriveStatus.HomingOk && strStackerLift.Status.DriveStatus.HomingOk /*&& !reHomeAbsolute*/) absHomeStep = 6;
         else {
            if (homeingTimer>7)
            {
               if (failCnt>5) absHomeStep = 1;
               else
               {
                  if (!strFingerPusher.Status.DriveStatus.HomingOk /*|| reHomeAbsolute*/) strFingerPusher.Command.Home = true;
                  if (!strFingerLift.Status.DriveStatus.HomingOk /*|| reHomeAbsolute*/) strFingerLift.Command.Home = true;
                  if (!strStackerLift.Status.DriveStatus.HomingOk /*|| reHomeAbsolute*/) strStackerLift.Command.Home  = true;
                  failCnt++;
                  homeingTimer=0;
                  lineController.command.shortBeep = true;
               }
            }
            else
            {
               homeingTimer += SECONDS_UNIT;
            }
         }
         break;
      case 6:
         if (strFingerPusher.AxisState.StandStill && strFingerLift.AxisState.StandStill && strStackerLift.AxisState.StandStill) absHomeStep = 7;
         break;   
      case 7:
         //reHomeAbsolute = false;
         //done
         break;
      
      
   }
   
   if (((strFingerPusher.Status.ErrorID !=0) || (strFingerLift.Status.ErrorID !=0) || (strStackerLift.Status.ErrorID !=0)) && absHomeStep>5) {
      absHomeStep = 0;
   }
   

   if (strFingerPusher.Status.DriveStatus.HomingOk && 
       /*strFingerLift.Status.DriveStatus.HomingOk   && 
       strStackerLift.Status.DriveStatus.HomingOk  && */
      strFingerPusher.Command.Power               &&
      strFingerLift.Command.Power                 &&
      strStackerLift.Command.Power                &&  
      (0 ==strFingerPusher.Status.ErrorID)        &&
      (0 == strFingerLift.Status.ErrorID)          &&
      (0 == strStackerLift.Status.ErrorID)         &&
   absHomeStep >= 7) {
      
      //lineController.command.trippleBeep = TRUE;
      return 0;
      
   }
   
   return absHomeStep+1;
}

USINT pageInTransportQueue (USINT a, struct sheet b)
{
   static USINT lastPageTypeAdded;
	
   if (0 == a) { //POST_PAGE
      transportQueue.pgQueue[transportQueue.pIn++] = b;
      lastPageTypeAdded = b.type;
      if (transportQueue.pIn>=TRANSPORT_QUEUE_SIZE) transportQueue.pIn = 0;
      return 0;
   }
   if (1 == a) { //PEEK_PAGE
      return transportQueue.pgQueue[transportQueue.pOut].type;
   }
   if (2 == a) { //INIT_QUEUE
      transportQueue.pIn = transportQueue.pOut = 0;
      return 0;
   }
   if (3 == a) { //GET_NO_PAGES_IN_QUEUE
      return ((transportQueue.pIn+TRANSPORT_QUEUE_SIZE)-transportQueue.pOut)%TRANSPORT_QUEUE_SIZE;
   }
   if (4 == a) { //REMOVE_LAST_PAGE
      if (transportQueue.pIn == transportQueue.pOut) return -1;
      transportQueue.pOut++;
      if (transportQueue.pOut>=TRANSPORT_QUEUE_SIZE) transportQueue.pOut = 0;
      return 0;
   }	
	/*if (5 == a) { //PEEK_PAGE_SIZE
		return transportQueue.pgQueue[transportQueue.pOut].size;
	}*/
   if (6 == a) { //FIRST_PAGE
      return lastPageTypeAdded;
   }
   if (7 == a) { //FORWARD_PEAK_PAGE
      USINT tmp;
      tmp = transportQueue.pOut;
      if (0 == tmp) tmp = TRANSPORT_QUEUE_SIZE;
      else tmp--;
      if (0 == pageInTransportQueue(GET_NO_PAGES_IN_QUEUE)) return NORMAL_PAGE;
      return transportQueue.pgQueue[tmp].type;
   }
   return 0;
}

enum sgqRetVal streamerGlueQueue(enum sgqFunc func)
{
   static BOOL glQueue[GQ_SIZE]; //bool
   static USINT inP;
   static USINT outP;
   int tmp;
    
   switch (func)
   {
      case addNoGlue:
         glQueue[inP++] = 0;
         if ((GQ_SIZE) == inP) inP = 0;
         break;
      case addGlue:
         glQueue[inP++] = 1;
         if ((GQ_SIZE) == inP) inP = 0;
         break;
      case peek:
         if (outP == inP) return err;
         else if (glQueue[outP] == 1) return glue;
         else return noGlue;
         break;
      case peekNext:
         tmp = outP+1;
         if (tmp >= GQ_SIZE) tmp = 0;
         if (tmp == inP) return err;
         if (outP == inP) return err;
         else if (glQueue[tmp] == 1) return glue;
         else return noGlue;
         break;
      case removeLast: 
         if (outP == inP) return err;
         outP++;
         if (outP == GQ_SIZE) outP = 0;
         break;
      case resetQueue:
         outP = inP = 0;
         break;
      case goNoGlue:
         do 
            streamerGlueQueue(removeLast);
         while (streamerGlueQueue(peekNext) == glue);
         streamerGlueQueue(removeLast);
         break;
      case dbg:
         
         if (inP>outP) streamer.reading.sheetsInStreamQueue = (inP-outP);
         else if (inP == outP) streamer.reading.sheetsInStreamQueue = 0;
         else {
            tmp = GQ_SIZE - outP;
            streamer.reading.sheetsInStreamQueue = tmp+inP;
         }
         break;
   }
   return err;
}

void runStackerY(void) 
{
   stackerY.homed = strStackerLift.Status.DriveStatus.HomingOk;
   stackerY.isSpeed = strStackerLift.Status.ActVelocity/1000;
   stackerY.position = strStackerLift.Status.ActPosition;
   
   if (strStackerLift.Status.ErrorID == 35201 || strStackerLift.Status.ErrorID == 35200) { 
      addError(LIGHT_CURTAIN,NORMAL_STOP,UNLOAD,1);
      return;
   }
   else if (streamer.reading.mainState == STATE_LOAD && homingState == 15) {
      return;
   }
   else if (strStackerLift.Status.ErrorID > 0)
   {
      if (strStackerLift.Command.ErrorAcknowledge == false) strStackerLift.Command.ErrorAcknowledge = true;
      return;
   }
   
   if (strStackerLift.Status.ErrorID != 0) stackerY.error = true;
   else stackerY.error = false;
   
   static USINT wait;
   if (wait)
   {
      wait--;
      return;
   }
   
   if (safety.reading.initialized)
   {
      if (strStackerLift.Status.ActVelocity > 0.35) //moving up
      {
         if (!stacker.reading.input.lightCurtain) {
            strStackerLift.Command.Halt = true;
            addError(LIGHT_CURTAIN,NORMAL_STOP,UNLOAD,0);
         }
      }
   }
   
   strStackerLift.Parameter.Acceleration = stackerY.acceleration * 1000;
   strStackerLift.Parameter.Deceleration = stackerY.deceleration * 1000;
   strStackerLift.Parameter.Velocity = stackerY.setSpeed * 1000;
   
   if (stackerY.moveing && strStackerLift.AxisState.StandStill) stackerY.moveing = false;
   
   if (!stackerY.homed) {
      stackerY.moveing = false;
   }
   else if (stackerY.error) {
      stackerY.moveing = false;
   }
   else if (!stackerY.moveing) {
      if (stackerY.move != 0) {
         
         REAL fac;
         fac = stepAccDec (stackerY.move,paperTransport[STREAMER_SECTION_2].setSpeed,0.0,fastFeed,0.1,3.5);
         strStackerLift.Parameter.Acceleration = strStackerLift.Parameter.Deceleration = fac * rabs(stackerY.move) * 1000;
         
         //strStackerLift.Parameter.Acceleration = 0.75 * rabs(stackerY.move) * 1000;
         //strStackerLift.Parameter.Deceleration = 0.75 * rabs(stackerY.move) * 1000;         
         if (strStackerLift.Parameter.Acceleration > (stackerY.acceleration * 1000)) strStackerLift.Parameter.Acceleration = stackerY.acceleration * 1000;
         if (strStackerLift.Parameter.Deceleration > (stackerY.deceleration * 1000)) strStackerLift.Parameter.Deceleration = stackerY.deceleration * 1000;
         if (strStackerLift.Parameter.Acceleration < ((0.1 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strStackerLift.Parameter.Acceleration = 0.1 * 1000;
         if (strStackerLift.Parameter.Deceleration < ((0.1 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strStackerLift.Parameter.Deceleration = 0.1 * 1000;
         
         sYmoveAcc = strStackerLift.Parameter.Acceleration;
         sYmoveCnt++;
         
         strStackerLift.Parameter.Distance = stackerY.move;
         strStackerLift.Command.MoveAdditive = true;
         stackerY.move = 0;
         stackerY.moveing = true;
         wait = 8;
      }
   
      if (stackerY.absMove != 0) { //you can go to all positions except to position 0. the use the command stackerY.goHome;
         stackerY.move = 0;
         strStackerLift.Parameter.Position = stackerY.absMove;
         strStackerLift.Command.MoveAbsolute = true;
         stackerY.absMove = 0;
         stackerY.moveing = true;
         wait = 8;
      }
      
      if (stackerY.goHome) { //you can go to all positions except to position 0. the use the command stackerY.goHome;
         strStackerLift.Parameter.Position = 0.0;
         strStackerLift.Command.MoveAbsolute = true;
         stackerY.absMove = 0;
         stackerY.goHome = false;
         stackerY.moveing = true;
         stackerY.move = 0;
         wait = 8;
      }
   }
}

void runFingerY(void) 
{
   #define NEVER_TIME_OUT 255
   forkY.homed = strFingerLift.Status.DriveStatus.HomingOk;
   forkY.isSpeed = strFingerLift.Status.ActVelocity/1000;
   forkY.position = strFingerLift.Status.ActPosition;

   if (strFingerLift.Status.ErrorID != 0) forkY.error = true;
   else forkY.error = false;
   
   static USINT wait;
   static USINT standStillTimeOut;
   
   if (wait)
   {
      wait--;
      return;
   }
   
   strFingerLift.Parameter.Acceleration = forkY.acceleration * 1000;
   strFingerLift.Parameter.Deceleration = forkY.deceleration * 1000;
   strFingerLift.Parameter.Velocity = forkY.setSpeed * 1000;
   
   if (forkY.moveing)
   {
      if (strFingerLift.AxisState.StandStill) forkY.moveing = false;
      else {
         if (standStillTimeOut == NEVER_TIME_OUT) {
             forkY.moveing = true;
         }
         if (standStillTimeOut) {
            standStillTimeOut--;
         }
         else
            forkY.moveing = false;
      }
   }
   
   
   if (forkY.moveing && strFingerLift.AxisState.StandStill) forkY.moveing = false;
   
   if (!forkY.homed) {
      forkY.moveing = false;
   }
   else if (forkY.error) {
      forkY.moveing = false;
   }
   else if (!forkY.moveing) {
      if (forkY.move != 0) {
         
         REAL fac;
         fac = stepAccDec (stackerY.move,paperTransport[STREAMER_SECTION_2].setSpeed,1.5,fastFeed,0.25,4.0);
         strFingerLift.Parameter.Acceleration = strFingerLift.Parameter.Deceleration = fac * rabs(stackerY.move) * 1000;
  
         if (strFingerLift.Parameter.Acceleration < ((0.15 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strFingerLift.Parameter.Acceleration = 0.15 * 1000;
         if (strFingerLift.Parameter.Deceleration < ((0.15 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strFingerLift.Parameter.Deceleration = 0.15 * 1000;
         
         if (streamer.reading.deliverOnTheFly == 4)
         {
            strFingerLift.Parameter.Acceleration = strFingerLift.Parameter.Acceleration * 1.1;
            strFingerLift.Parameter.Deceleration = strFingerLift.Parameter.Deceleration * 1.05;
            if (strFingerLift.Parameter.Acceleration < ((0.3 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strFingerLift.Parameter.Acceleration = 0.50 * 1000;
            if (strFingerLift.Parameter.Deceleration < ((0.3 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strFingerLift.Parameter.Deceleration = 0.50 * 1000;
         }
         
         if (streamer.reading.deliverOnTheFly > 4 && streamer.reading.deliverOnTheFly <=8)
         {
            strFingerLift.Parameter.Acceleration = strFingerLift.Parameter.Acceleration * 1.15;
            strFingerLift.Parameter.Deceleration = strFingerLift.Parameter.Deceleration * 1.15;
            if (strFingerLift.Parameter.Acceleration < ((0.5 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strFingerLift.Parameter.Acceleration = 0.75 * 1000;
            if (strFingerLift.Parameter.Deceleration < ((0.5 + paperTransport[STREAMER_SECTION_2].isSpeed) * 1000)) strFingerLift.Parameter.Deceleration = 0.75 * 1000;
         }
         
         if (strFingerLift.Parameter.Acceleration > (forkY.acceleration * 1000)) strFingerLift.Parameter.Acceleration = forkY.acceleration * 1000;
         if (strFingerLift.Parameter.Deceleration > (forkY.deceleration * 1000)) strFingerLift.Parameter.Deceleration = forkY.deceleration * 1000;

         fYmoveAcc = strStackerLift.Parameter.Acceleration;
         fYmoveCnt++;
         
         strFingerLift.Parameter.Distance = forkY.move;
         if (rabs(forkY.move)>100)
            standStillTimeOut = 250;
         else
            standStillTimeOut = 15;
         strFingerLift.Command.MoveAdditive = true;
         forkY.move = 0;
         forkY.moveing = true;
         wait = 8;
      }
   
      if (forkY.absMove != 0) { //you can go to all positions except to position 0. the use the command forkY.goHome;
         strFingerLift.Parameter.Position = forkY.absMove;
         strFingerLift.Command.MoveAbsolute = true;
         standStillTimeOut = 20;
         forkY.absMove = 0;
         forkY.moveing = true;
         wait = 8;
      }
      
      if (forkY.goHome) { //you can go to all positions except to position 0. the use the command forkY.goHome;
         strFingerLift.Parameter.Position = 0.0;
         strFingerLift.Command.MoveAbsolute = true;
         standStillTimeOut = 20;
         forkY.absMove = 0;
         forkY.goHome = false;
         forkY.moveing = true;
         wait = 8;
      }
   }
}

void runFingerX(void) 
{
   forkX.homed = strFingerPusher.Status.DriveStatus.HomingOk;
   forkX.isSpeed = strFingerPusher.Status.ActVelocity/1000;
   forkX.position = strFingerPusher.Status.ActPosition;

   if (strFingerPusher.Status.ErrorID != 0) forkX.error = true;
   else forkX.error = false;
   
   static USINT wait;
   if (wait)
   {
      wait--;
      return;
   }
   
   strFingerPusher.Parameter.Acceleration = forkX.acceleration * 1000;
   strFingerPusher.Parameter.Deceleration = forkX.deceleration * 1000;
   strFingerPusher.Parameter.Velocity = forkX.setSpeed * 1000;
   
   if (forkX.moveing && strFingerPusher.AxisState.StandStill) forkX.moveing = false;
   
   if (!forkX.homed) {
      forkX.moveing = false;
   }
   else if (forkX.error) {
      forkX.moveing = false;
   }
   else if (!forkX.moveing) {
      if (forkX.move != 0) {
         strFingerPusher.Parameter.Distance = forkX.move;
         strFingerPusher.Command.MoveAdditive = true;
         forkX.move = 0;
         forkX.moveing = true;
         wait = 8;
      }
   
      if (forkX.absMove != 0) { //you can go to all positions except to position 0. the use the command forkX.goHome;
         strFingerPusher.Parameter.Position = forkX.absMove;
         strFingerPusher.Command.MoveAbsolute = true;
         forkX.absMove = 0;
         forkX.moveing = true;
         wait = 8;
      }
      
      if (forkX.goHome) { //you can go to all positions except to position 0. the use the command forkX.goHome;
         strFingerPusher.Parameter.Position = 0.0;
         strFingerPusher.Command.MoveAbsolute = true;
         forkX.absMove = 0;
         forkX.goHome = false;
         forkX.moveing = true;
         wait = 8;
      }
   }
}

void loadError(REAL timeOut, USINT funcError)
{
   static REAL timer;
   if (!funcError) {
      timer = 0.0;
      return;
   }
   else timer += SECONDS_UNIT;
      
   if (timer > timeOut)
   {
      if (funcError >= 2 && funcError <= 4)
      {
         addError(JOGGER1_HOMEFAULT,NORMAL_STOP,TRUE,funcError);
      }
      else if (funcError >= 5 && funcError <= 9)
      {
         addError(JOGGER2_HOMEFAULT,NORMAL_STOP,TRUE,funcError);
         jogger2.Command.StopSlave = true;
      }  
      else if (funcError >= 65 && funcError <= 69)
      {
         addError(JOGGER3_HOMEFAULT,NORMAL_STOP,TRUE,funcError);
      }   
      else if (funcError == 16)
      {
         if (!stacker.reading.input.enterTransport)
            addError(ENTER_TRANSPORT,NORMAL_STOP,TRUE,funcError);
         else if (!stacker.reading.input.alignerOutfeedDigital1) 
            addError(EXIT_TRANSPORT,NORMAL_STOP,TRUE,funcError);//addError(LOAD_ERROR,NORMAL_STOP,TRUE,funcError);
         else
            addError(JOGGER2_HOMEFAULT,NORMAL_STOP,TRUE,funcError);
      }
      else if (funcError == 17)
      {
         if (!stacker.reading.input.exitStreamerSection1)
            addError(EXIT_STREAMER_SECTION_1,NORMAL_STOP,TRUE,funcError);
         else if (!stacker.reading.input.streamerGap1)
            addError(ENTER_STACK_GAP_SENSOR,NORMAL_STOP,TRUE,funcError);
         else
            addError(LOAD_ERROR,NORMAL_STOP,TRUE,funcError);
      }
      else if (funcError == 20)
      {
         if (!stacker.reading.input.tableClearSensor1 || !stacker.reading.input.tableClearSensor2 || !stacker.reading.input.tableClearSensor3 || !stacker.reading.input.tableClearSensor4)
            addError(TABLE_CLEAR,NORMAL_STOP,TRUE,funcError);
         else if (!stacker.reading.input.lightCurtain)
            addError(LIGHT_CURTAIN,NORMAL_STOP,TRUE,funcError);
         else
            addError(LOAD_ERROR,NORMAL_STOP,TRUE,funcError);
      }
      else if (funcError == 41)
      {
         if (!stacker.reading.input.lightCurtain)
            addError(LIGHT_CURTAIN,NORMAL_STOP,TRUE,funcError);
         else
            addError(LOAD_ERROR,NORMAL_STOP,TRUE,funcError);
      }
      else
         addError(LOAD_ERROR,NORMAL_STOP,TRUE,funcError);
      homingState = 0;
   }
}

BOOL load (void)
{
   static BOOL wasFixed;
  
   if (!globalPower) {
      addError(NO_POWER,NORMAL_STOP,TRUE,0);
      return 0;
   }
      
   endPageInStream = false;
   
   if (streamer.setting.jogger2Position == 0) fixedJogger2 = false;
   else fixedJogger2 = true;
   
   if (fixedJogger2) {
      reStartJoggers = true;
      wasFixed = true;
   }
   else if (wasFixed)
   {
      reStartJoggers = true;
      wasFixed = false;
   }

   pagesSinceDeliver = 10;
   if (stateFeedToCutLoadState>0) // handle sheet that is sticking out into the alignerTransport
   {
      feedToCutTimer += SECONDS_UNIT;
      switch (stateFeedToCutLoadState) {
         case 1:
            cutter.command.feedToCut = true;
            stateFeedToCutLoadState = 2;
            paperTransport[ALIGNER].setSpeed = stacker.setting.transportIdleSpeed;
            feedToCutTimer = 0;
            break;
         case 2:
            if (cutter.reading.webSpeed > 0.05) {
               stateFeedToCutLoadState = 3;
               feedToCutTimer = 0;
            }
            paperTransport[ALIGNER].setSpeed = highest(stacker.setting.transportIdleSpeed,cutter.reading.webSpeed*1.3,cutter.reading.setSpeed * 0.9);
            break;
         case 3:
            if (stacker.reading.input.enterTransport) {
               stateFeedToCutLoadState = 4;
               feedToCutTimer = 0;
            }
            paperTransport[ALIGNER].setSpeed = highest(stacker.setting.transportIdleSpeed,cutter.reading.webSpeed*1.3,cutter.reading.setSpeed * 0.9);
            break;
         case 4:
            paperTransport[ALIGNER].setSpeed = highest(stacker.setting.transportIdleSpeed*1.5,0.5,0);
            paperTransport[STREAMER_SECTION_1].setSpeed = paperTransport[ALIGNER].setSpeed/30;
            if (feedToCutTimer>4.0 && stacker.reading.input.enterTransport) {
               stateFeedToCutLoadState = 0;
               feedToCutTimer = 0;
               paperTransport[ALIGNER].setSpeed = 0.0;
               paperTransport[STREAMER_SECTION_1].setSpeed = 0.0;
            }
            break;
      }
      
      if (feedToCutTimer > (2.5 + (stateFeedToCutLoadState == 4 ? 3.5:0)) ) { //could not clear it
         addError(ENTER_TRANSPORT,NORMAL_STOP,TRUE,stateFeedToCutLoadState);
         stateFeedToCutLoadState = 0;
         paperTransport[ALIGNER].setSpeed = 0.0;
         paperTransport[STREAMER_SECTION_1].setSpeed = 0.0;
         feedToCutTimer = 0;
         return 0;
      }
      runPaperTransport();
      return 0;
   }
   
   if (!stacker.reading.input.enterTransport) {
      if (cutter.reading.status.state == Status_Ready && !cutter.reading.output.reject)
      {
         stateFeedToCutLoadState = 1;
         feedToCutTimer = 0;
      }
      else
      {
         addError(ENTER_TRANSPORT,NORMAL_STOP,TRUE,0);
         stateFeedToCutLoadState = 0;
         feedToCutTimer = 0;
         return 0;
      }
   }
   
   stacker.reading.lastStackSize = 0;
   pagesReceived = 0;
   streamer.reading.instantDeliverOn = false;
   switch (homingState)
   {
      case 0:
         goToOffsetStartPos();
         homeServoResult = directHomeServos();
         streamer.reading.feedToStack = 0;
         streamer.reading.deliverSheetPos = 0;
         streamer.reading.deliverOnTheFly = 0;
         alignerLastSheetDistance = 0.0;
         alignerFloorSpeed[0].timer = 0.0;
         alignerFloorSpeed[1].timer = 0.0;
         alignerFloorSpeed[2].timer = 0.0;
         alignerFloorSpeed[3].timer = 0.0;
         alignerFloorSpeed[4].timer = 0.0;
         alignerFloorSpeed[5].timer = 0.0;
         alignerFloorSpeed[6].timer = 0.0;
         alignerFloorSpeed[7].timer = 0.0;
         
         forkX.acceleration = FORKX_ACC;
         forkX.deceleration = FORKX_DEC;
         forkX.setSpeed = FORKX_SPD;
         
         jogger1.Command.MoveJogNeg = false;
         jogger1.Command.MoveJogPos = false;
         jogger2.Command.MoveJogNeg = false;
         jogger2.Command.MoveJogPos = false;
         if (lineController.setup.streamer.jogger3) {
            jogger3.Command.MoveJogNeg = false;
            jogger3.Command.MoveJogPos = false;
         }   
          
         
         if (dirHomeErrorCnt[ALIGNER]>10) addError(AXIS_ALIGNER,NORMAL_STOP,TRUE,aligner.Status.ErrorID);
         if (dirHomeErrorCnt[STREAMER_SECTION_1]>10) addError(AXIS_STREAMER_SECTION_1,NORMAL_STOP,TRUE,strSection1.Status.ErrorID);
         if (dirHomeErrorCnt[STREAMER_SECTION_2]>50) addError(AXIS_STREAMER_SECTION_2,NORMAL_STOP,TRUE,strSection2.Status.ErrorID);
         if (dirHomeErrorCnt[JOGGER1]>10) addError(AXIS_JOGGER1,NORMAL_STOP,TRUE,jogger1.Status.ErrorID);
         if (dirHomeErrorCnt[JOGGER2]>10) addError(AXIS_JOGGER2,NORMAL_STOP,TRUE,jogger2.Status.ErrorID);
         if (dirHomeErrorCnt[JOGGER3]>10 && lineController.setup.streamer.jogger3) addError(AXIS_JOGGER3,NORMAL_STOP,TRUE,jogger3.Status.ErrorID);
         if (dirHomeErrorCnt[TAGLIA]>10) addError(AXIS_TAGLIA,NORMAL_STOP,TRUE,taglia.Status.ErrorID);
         if (dirHomeErrorCnt[DELIVER]>10) addError(AXIS_DELIVER,NORMAL_STOP,TRUE,deliver.Status.ErrorID);
         
         if (homeServoResult == 0) {
            homingState = 1;
            absoluteHomeServos(1);
            loadError(LOAD_ERROR_RESET);
         }
         reHomeStatus = 0;
         break;
      case 1: //home joggers start
         if (jogger2.AxisState.SynchronizedMotion)
         {
            if (reStartJoggers) {
               jogger2.Command.StopSlave = TRUE;
               joggers.ready = false;
            }
            else homingState = 15;
         }
         else if (stacker.reading.input.homePosJogger1) 
            homingState = 3;
         else
         {
            jogger1.Parameter.JogVelocity = 20;
            jogger1.Command.MoveJogPos = true;
            homingState = 2;
         }
         loadError(LOAD_ERROR_RESET);
         break;
      case 2:
         if (stacker.reading.input.homePosJogger1)
         {
            jogger1.Command.MoveJogPos = false;
            homingState = 3;
            loadError(LOAD_ERROR_RESET);
         }
         loadError(8,homingState);
         break;
      case 3:
         reStartJoggers = false;
         joggers.ready = false;
         jogger1.Parameter.JogVelocity = 1;
         jogger1.Command.MoveJogNeg = true;
         homingState = 4;
         loadError(LOAD_ERROR_RESET);
         break;
      case 4:
         if (!stacker.reading.input.homePosJogger1)
         {
            joggers.homePos1 = jogger1.Status.ActPosition;
            jogger1.Command.MoveJogNeg = false;
            homingState = 5;
            loadError(LOAD_ERROR_RESET);
         }
         loadError(8,homingState);
         break;
      case 5:
         if (stacker.reading.input.homePosJogger2) {
            homingState = 7;
            loadError(LOAD_ERROR_RESET);
         }
         else
         {
            jogger2.Parameter.JogVelocity = 20;
            jogger2.Command.MoveJogNeg = true;
            homingState = 6;
            loadError(LOAD_ERROR_RESET);
         }
         if (jogger2.Status.ErrorID>0) {
            jogger2.Command.ErrorAcknowledge = true;
            jogger2.Command.MoveJogNeg = false;
            homingState = 1;
            loadError(LOAD_ERROR_RESET);
         }
         loadError(8.0,homingState);
         break;
      case 6:
         if (stacker.reading.input.homePosJogger2)
         {
            jogger2.Command.MoveJogNeg = false;
            homingState = 7;
            loadError(LOAD_ERROR_RESET);
         }
         loadError(8.0,homingState);
         break; 
      case 7:
         jogger2.Parameter.JogVelocity = 1;
         jogger2.Command.MoveJogPos = true;
         homingState = 8;
         loadError(LOAD_ERROR_RESET);
         break;
      case 8:
         if (!stacker.reading.input.homePosJogger2)
         {
            joggers.homePos2 = jogger2.Status.ActPosition;
            if (!jogger2.Command.MoveJogPos && joggers.homePos2 != 0) jogger2.Command.Home = true;
            jogger2.Command.MoveJogPos = false;
            if (!jogger2.Command.Home && joggers.homePos2 == 0) homingState = 9;
            loadError(LOAD_ERROR_RESET);
         }
         if (jogger2.Status.ErrorID>0) {
            jogger2.Command.ErrorAcknowledge = true;
            jogger2.Command.MoveJogNeg = false;
            homingState = 1;
            loadError(LOAD_ERROR_RESET);
         }
         loadError(5.0,homingState);
         break;
      case 9:
         if (fixedJogger2) {
            jogger2.Parameter.Position = rmod(streamer.setting.jogger2Position,0,62.9);
            jogger2.Command.MoveAbsolute = true;
            jogger2.Parameter.Velocity = 40.0;
         }
         else
            jogger2.Command.StartSlave = true;
         joggers.run = false;
         joggers.open = false;
         joggers.close = false;
         joggers.speed = 0.0;
         homingState = 10;
         loadError(LOAD_ERROR_RESET);
         break;
      case 10: //Home joggers end
         joggers.ready = true;
         homingState = 15;
         loadError(LOAD_ERROR_RESET);
         
         break;
      case 15:
         //cat door
         if (!strFingerPusher.Status.DriveStatus.HomingOk) reHomeAbsolute = true;
         if (!strFingerLift.Status.DriveStatus.HomingOk) reHomeAbsolute = true;
         if (!strStackerLift.Status.DriveStatus.HomingOk) reHomeAbsolute = true;
         
         
         if (reHomeAbsolute)
         {
            switch (reHomeStatus)
            {
               case 0:
                  strFingerPusher.Command.Power = true;
                  strFingerLift.Command.Power = true;
                  strStackerLift.Command.Power = true;
                  reHomeStatus = 1;
               case 1: //clear stop
                  strFingerPusher.Command.Stop = false;
                  strFingerLift.Command.Stop = false;
                  strStackerLift.Command.Stop = false;
                  reHomeStatus = 2;
                  break;
               case 2: //start home
                  strFingerPusher.Command.Home = true;
                  strFingerLift.Command.Home = true;
                  strStackerLift.Command.Home  = true;
                  reHomeStatus = 3;
                  break;
               case 3: //homeing has started
                  if (!strFingerPusher.Status.DriveStatus.HomingOk || !strFingerLift.Status.DriveStatus.HomingOk || !strStackerLift.Status.DriveStatus.HomingOk) reHomeStatus = 4;
                  break;
               case 4: //homeing done
                  if (strFingerPusher.Status.DriveStatus.HomingOk && strFingerLift.Status.DriveStatus.HomingOk && strStackerLift.Status.DriveStatus.HomingOk) reHomeAbsolute = false;
                  break;
            }
         }
         else
         {
            homeServoResult = absoluteHomeServos(0);
            if (homeServoResult == 0) {
               homingState = 16;
               homingStateTimer = 0.5;
               loadError(LOAD_ERROR_RESET);
            }
         }
         loadError(7.0,homingState);
         break;
      case 16:
         if (!stacker.reading.input.enterTransport) homingStateTimer = 2.0;
         if (!stacker.reading.input.alignerOutfeedDigital1) homingStateTimer = 1.0;
         if (homingStateTimer>0) {
            paperTransport[ALIGNER].setSpeed = 0.5;
            homingStateTimer -=SECONDS_UNIT;
            paperTransport[STREAMER_SECTION_1].setSpeed = paperTransport[ALIGNER].setSpeed/12;
            paperTransport[STREAMER_SECTION_2].setSpeed = paperTransport[ALIGNER].setSpeed/12;
            paperTransport[TAGLIA].setSpeed = paperTransport[ALIGNER].setSpeed/10;
            /* test2 */
            
            
            runPaperTransport();
         }
         else {
            homingState = 17;
            homingStateTimer = 3.0;
            loadError(LOAD_ERROR_RESET);
         }
         loadError(7.0,homingState);
         break;
      case 17:
         if (!stacker.reading.input.exitStreamerSection1) homingStateTimer = 4.0;
         if (!stacker.reading.input.streamerGap1) homingStateTimer = 0.5;
         if (homingStateTimer>0) {
            paperTransport[ALIGNER].setSpeed = 0.0;
            homingStateTimer -=SECONDS_UNIT;
            paperTransport[STREAMER_SECTION_1].setSpeed = 0.25;
            paperTransport[STREAMER_SECTION_2].setSpeed = 0.25;
            paperTransport[TAGLIA].setSpeed = 0.3;
            paperTransport[DELIVER].setSpeed = 0.025;
            runPaperTransport();
         }
         else {
            homingState = 18;
            loadError(LOAD_ERROR_RESET);
         }
         loadError(7.0,homingState);
         break;
      case 18:
         paperTransport[ALIGNER].setSpeed = 0.0;
         paperTransport[STREAMER_SECTION_1].setSpeed = 0.0;
         paperTransport[STREAMER_SECTION_2].setSpeed = 0.0;
         paperTransport[TAGLIA].setSpeed = 0.0;
         paperTransport[DELIVER].setSpeed = 0.0;
         runPaperTransport();
         homingStateTimer = 4.0;
         if (stacker.reading.input.seqUnitNotReady) homingState = 20;
         else addError(SUB_SEQ_NOT_READY,NORMAL_STOP,TRUE,1);
         if (stacker.setting.deliveryPosition == 0) stackerY.goHome = true;
         else stackerY.absMove = stacker.setting.deliveryPosition; 
         loadError(LOAD_ERROR_RESET);
         deliverStartPostion = paperTransport[DELIVER].position;
         break;
      case 20:
         //deliverStack(); 
         paperTransport[DELIVER].setSpeed = stacker.setting.deliverTransportSpeed;
         if (!stacker.reading.input.tableClearSensor1) homingStateTimer = 2.0 + (1-stacker.setting.deliverTransportSpeed);
         if (!stacker.reading.input.tableClearSensor2) homingStateTimer = 2.0 + (1-stacker.setting.deliverTransportSpeed);
         if (!stacker.reading.input.tableClearSensor3) homingStateTimer = 2.0 + (1-stacker.setting.deliverTransportSpeed);
         if (!stacker.reading.input.tableClearSensor4) homingStateTimer = 2.0 + (1-stacker.setting.deliverTransportSpeed);
         if (!stacker.reading.input.lightCurtain) homingStateTimer = 1.0;
         if (homingStateTimer>0) {
            paperTransport[DELIVER].setSpeed = stacker.setting.deliverTransportSpeed;
            homingStateTimer -=SECONDS_UNIT;
         }
         else {
            paperTransport[DELIVER].setSpeed = 0.0;
            homingState = 30;
            loadError(LOAD_ERROR_RESET);
         }
         setStackExit(paperTransport[DELIVER].position,deliverStartPostion);
         loadError(10.0,homingState);
         runPaperTransport();
         break;
      case 30:
         paperTransport[ALIGNER].setSpeed = 0.0;
         paperTransport[STREAMER_SECTION_1].setSpeed = 0.0;
         paperTransport[STREAMER_SECTION_2].setSpeed = 0.0;
         paperTransport[TAGLIA].setSpeed = 0.0;
         paperTransport[DELIVER].setSpeed = 0.0;
         forkX.absMove = -5;
         homingState = 35;
         loadError(LOAD_ERROR_RESET);
         break;
      case 35:
         forkY.absMove = stacker.setting.fingerHightAdjust;
         homingState = 40;
         loadError(LOAD_ERROR_RESET);
         break;
      case 40:
         stackerY.absMove = stacker.setting.topPosOffset;
         homingState = 41;  
         loadError(LOAD_ERROR_RESET);
         break;
      case 41:
         if (stackerY.position > (stacker.setting.topPosOffset - stacker.setting.minStackSize)) {
            homingState = 99;  
            loadError(LOAD_ERROR_RESET);
         }
         loadError(4.0,homingState);
         distanceAfterGapSection2 = 50.0;
         joggers.masterClosePos = joggers.homePos1 + 18.7;
         if (joggers.masterClosePos>63) joggers.masterClosePos -= 63;
         break;
   }
   if (homingState == 99) {
      streamer.reading.sheetsAfterEmptyStack = 0;
      streamer.reading.deliverOnTheFly = 0;
      streamer.reading.deliverSheetPos = 0;
      cutter.command.deliverNextRequest = false;
      dbgTxt("6 delNext FALSE");
      homingState = 0;
      //dontGlue[2] = false;
      //ebmGlue.command.mute = false;
      ebmGlue.command.glueGun.start = true;
      return 1;
   }
   else {
      return 0;
      //dontGlue[2] = true;
      //ebmGlue.command.mute = true;
      ebmGlue.command.glueGun.stop = true;
   }
}

void absoluteGoHome(void)
{
   stackerY.goHome = true;
   forkY.goHome = true;
   forkX.goHome = true;
}

void controlTableStepsButton(void)
{
   if (stacker.command.tableStepsAvg) {
      stacker.setting.tableSteps = stacker.reading.stepAvg;
   }
   stacker.command.tableStepsAvg = 0;
}

void clearAllErrors(void)
{
   USINT i;
   lineController.command.beep = true;
   for (i=0;i<=LAST_ERROR;i++)
   {
      error[i].active = 0;
      error[i].stopType = 0;
      error[i].unload = 0;
      error[i].subType = 0;
   }
}

void addError(USINT errorNo,USINT stopScenario,BOOL unload, UINT subType)
{
   if (errorNo == LIGHT_CURTAIN || errorNo == STACKER_HATCH || errorNo == E_STOP) reStartJoggers = TRUE;
   if (errorNo == STACKER_HATCH || errorNo == E_STOP) reHomeAbsolute = TRUE;
            

   //if (errorNo == WASTE_BOX_FULL) wasteBoxFullErrorTriggerd = TRUE;
   
   error[errorNo].active   = TRUE;
   //0 Stop After Next Job //Not implemented, place holder
   //1 Normal Stop
   //2 Stop Immediately
   //3 Stop Immediately, And Power Off //Not implemented, place holder
   error[errorNo].stopType = stopScenario;
   //0 continue loaded
   //1 unload
   error[errorNo].unload   = unload;
   error[errorNo].subType  = subType;
   error[errorNo].cnt++;
	
   machineError.active = TRUE;
   machineError.errType = (UINT)(errorNo) + 1500;
   machineError.errSubType = (UINT)(subType);	
}

void keepCoverLocked(void)
{
   if (!safety.reading.stackerTopCoverLocked) safety.command.lockStackerTopCover = TRUE;
}

void keepCoverUnLocked(void)
{
   if (safety.reading.stackerTopCoverLocked) safety.command.unlockStackerTopCover = TRUE;
}

void tablePressUnitControl(void)
{
   
   switch (stpState)
   {
      case 0:
         if (stackToPress && !lastStackToPress) stpState = 1;
         pressUnitTimer = 13.0; //10.0
         output.pressUnitStart = false;
         break;
      case 1:
         pressUnitTimer -= SECONDS_UNIT;
         stackToPress = false;
         output.pressUnitStart = false;
         if (pressUnitTimer <= 0.0) stpState = 0;
         if (!stacker.reading.input.pressUnitSensor && lastPressUnitSensor) {
            stpState = 2;
            pressUnitTimer = 4.0; //3.5
            lineController.command.doubleBeep = true;
         }
         break;
      case 2:
         if (pressUnitTimer < 3.5) output.pressUnitStart = true;
         pressUnitTimer -= SECONDS_UNIT;
         stackToPress = false;
         if (pressUnitTimer <= 0.0) stpState = 0;
         break;
   }
   
   lastPressUnitSensor = stacker.reading.input.pressUnitSensor;
   lastStackToPress = stackToPress;
}

void manFeed(void)
{
   static REAL manFeedTime;
   
   if (streamer.command.manFeed)
   {
      manFeedTime += SECONDS_UNIT;
      if (manFeedTime>3.0) feedStreamer = true;
   }
   else 
   {
      feedStreamer = false;
      manFeedTime = 0.0;
   }
}

BOOL axisRunning(void)
{
   if (aligner.Status.ActVelocity > 25)     return true; 
   if (strSection1.Status.ActVelocity > 25) return true;
   if (strSection2.Status.ActVelocity > 25) return true;
   if (jogger1.Status.ActVelocity > 25)     return true;
   if (jogger2.Status.ActVelocity > 25)     return true;
   if (jogger3.Status.ActVelocity > 25 && STACK_JOGGER_PRESSENT)     return true;
   if (deliver.Status.ActVelocity > 25)     return true;
   if (taglia.Status.ActVelocity > 25)      return true;
   return false;
}

void debugger(USINT type, REAL r1, REAL r2, UDINT i1)
{
   if (seqDbgHalt) return;
   seqDbgP++;
   if (seqDbgP>49) seqDbgP = 0;
   
   seqDbg[seqDbgP].typ = type;
   seqDbg[seqDbgP].i1 = i1;
   seqDbg[seqDbgP].i2 = tick;
   seqDbg[seqDbgP].r1 = r1;
   seqDbg[seqDbgP].r2 = r2;
   return;
}

void checkWasteBox (void)
{
   if (!stacker.reading.input.wasteBoxFull && !stacker.reading.input.wasteBoxWarning) wasteBoxCnt += 10;
   else if (!stacker.reading.input.wasteBoxFull) wasteBoxCnt++;
   else if (!stacker.reading.input.wasteBoxWarning) wasteBoxCnt++;
   else wasteBoxCnt=0;

   if (wasteBoxCnt>10000) wasteBoxCnt = 10000;     
   
   if (cutter.reading.output.reject && inSpeed.is > 0 && wasteBoxCnt>4000)
   {
      addError(WASTEBOX_FULL,NORMAL_STOP,FALSE,0);
   }
   else if (wasteBoxCnt>500)
   {
      if (stacker.reading.warningCode == 0) stacker.reading.warningCode = 1;
   }
   else if (stacker.reading.warningCode == 1) stacker.reading.warningCode = 0;
}

void goToOffsetStartPos(void)
{
   //swap this//
   //streamer.command.offsetNorth = true;
   //streamer.command.offsetSouth = false;
   
   streamer.command.offsetNorth = false; //test2
   streamer.command.offsetSouth = true; //test2
   
}

void streamerOffsetSequence (void) //handles the 5 offset valves
{

   if (!streamer.setting.enableOffset) 
   {
      output.offsetValve1 = 0;
      output.offsetValve2 = 0;
      output.offsetValve3 = 0;
      output.offsetValve4 = 0;
      output.offsetValve5 = 1;
      streamerOffset.markState = 0;
      return;
   }
   else if (stacker.reading.stackSize > streamer.setting.tableDropDistance)
   {
      output.offsetValve5 = 0;
   }
   else output.offsetValve5 = 1; 
   
   static UINT lastOffsetMarkReader;
   static BOOL nextNoMark;
   static BOOL newOffsetSent;
   static BOOL waitStart;
   static REAL wait;
   
   if (stacker.reading.input.streamerGap1) waitToNext = cutter.reading.sheetSize; //ignore offsets after the gap is seen.
      
   if (stacker.reading.input.offsetMarkReaderCnt != lastOffsetMarkReader && /* !stacker.reading.input.streamerGap1 &&*/ waitToNext <= 0)
   {
      streamer.command.offsetToggle = true;
      waitToNext = 25; //mm  //not more often than 25 ms.
   }
   
   if (waitToNext > 0)
   {
      waitToNext -= mm(STREAMER_SECTION_2);
   }
   
   if (streamerOffset.waitToExtendStream > 0) streamerOffset.enableExtendStreamer = true;
   
   if (streamer.command.offsetNorth) {
      waitStart = false;
      wait = 10 + (streamer.setting.overlap/10); //mm
      streamerOffset.state = StreamerOffsetInNorth;
   }
   if (streamer.command.offsetSouth) streamerOffset.state = StreamerOffsetInSouth;
   
   if (streamer.command.offsetNorth) streamerOffset.state = StreamerOffsetInNorth; //test2
   
   switch (streamerOffset.state)
   {
      case StreamerOffsetInit:
         output.offsetValve1 = 0; //up down stop plate  , 0 = down        1 = up
         output.offsetValve2 = 0; //up down hold fingers, 0 = up          1 = down
         output.offsetValve3 = 0; //in out hold fingers,  0 = extracted   1 = retracted
         output.offsetValve4 = 0; //io out streamer tbl,  0 = shorter     1 = longer
         //output.offsetValve5 = 1; //stacker up stacker    0 = table drop  1 = table up
         streamerOffset.state = StreamerOffsetInSouth;
         distanceSinceOffset = 0.0;
         break;
      case StreamerOffsetInSouth: 
         //in south, extend hold fingers (south = towards unwinder)
         output.offsetValve1 = 0; //up down stop plate, down
         output.offsetValve2 = 0; //up down hold fingers, up
         output.offsetValve3 = 0; //in out hold fingers, extracted
         output.offsetValve4 = 0; //io out streamer tbl, shorter
         //output.offsetValve5 = 1;
         
         if (streamer.command.offsetToggle)
            streamerOffset.state = StreamerOffsetInSouthToNorth1;
         break;
      case StreamerOffsetInSouthToNorth1:
         //in south -> north seq 1, down hold fingers
         streamerOffset.distance = streamer.setting.overlap*streamerOffset.waitToExtendStream;
         output.offsetValve1 = 0;
         output.offsetValve2 = 1;
         output.offsetValve3 = 0;
         output.offsetValve4 = 0;
         //output.offsetValve5 = 0; //drop stacker
         streamerOffset.state = StreamerOffsetInSouthToNorth2;
         break;
      case StreamerOffsetInSouthToNorth2:
         //in south -> north seq 2, up stop plate,
         streamerOffset.distance -= mm(STREAMER_SECTION_2);
         output.offsetValve1 = 1;
         output.offsetValve2 = 1;
         output.offsetValve3 = 0;
         output.offsetValve4 = 0;
         if (streamerOffset.distance < 0.0) {
            streamerOffset.state = StreamerOffsetInSouthToNorth3;
            streamerOffset.distance = streamer.setting.overlap*streamerOffset.waitToRetractSheets;
         }
         break;
      case StreamerOffsetInSouthToNorth3:
         //in south -> north seq 2, extend streamer
         streamerOffset.distance -= mm(STREAMER_SECTION_2);
         output.offsetValve1 = 1;
         output.offsetValve2 = 1;
         output.offsetValve3 = 0;
         output.offsetValve4 = 1;
         if (streamerOffset.distance < 0.0) {
            streamerOffset.state = StreamerOffsetInSouthToNorth4;
            streamerOffset.time = streamerOffset.timeToLiftFingers;
         }
         break;
      case StreamerOffsetInSouthToNorth4:
         //in south -> north seq 3 (offset done), retract hold fingers
         streamerOffset.time -= MILLISECONDS_UNIT;
         output.offsetValve1 = 1;
         output.offsetValve2 = 1;
         output.offsetValve3 = 1;//0
         output.offsetValve4 = 1;
         if (streamerOffset.time < 0.0) {
            streamerOffset.state = StreamerOffsetInNorth;
         }
         //wait = 15; //mm
         wait = 10 + (streamer.setting.overlap/10); //mm
         waitStart = false;
         break;
      case StreamerOffsetInNorth:
         //in north  finger up (towards delivery tbl)
         output.offsetValve1 = 1; //up down stop plate, up
         output.offsetValve2 = 0; //up down hold fingers, up
         output.offsetValve3 = 1; //in out hold fingers, retracted
         output.offsetValve4 = 1; //io out streamer tbl, longer
         
         if (streamer.command.offsetToggle) {
            waitStart = true;
            /*
            if (streamer.reading.northStartCnt > 30 && streamer.reading.southStartCnt > 30 && streamer.reading.southStartCnt % 25 == 0) {
               streamer.reading.deltaStopPlateCompensation = ((streamer.reading.southLeftAvg + streamer.reading.southRightAvg 
                  - streamer.reading.northLeftAvg - streamer.reading.northRightAvg + 3) / 2.5);
               wait -=  streamer.reading.deltaStopPlateCompensation * paperTransport[STREAMER_SECTION_2].isSpeed; //2 old assembly, 6 new assembly 15 mm stroke
            }
            else
            {
               wait -= 6 * paperTransport[STREAMER_SECTION_2].isSpeed; //2 old assembly, 6 new assembly 15 mm stroke
            } */  
            
            wait -= 6 * paperTransport[STREAMER_SECTION_2].isSpeed; //2 old assembly, 6 new assembly 15 mm stroke
            
         }

         if (waitStart) wait -= mm(STREAMER_SECTION_2);
         
         if (wait < 0) {
            streamerOffset.state = StreamerOffsetInSouth;
            wait = 10;
            waitStart = false;
         }
         break;
   }
   if (streamer.command.offsetToggle || streamer.command.offsetNorth || streamer.command.offsetSouth) stacker.reading.offsetCounter++;
   distanceSinceOffset += paperTransport[STREAMER_SECTION_2].isSpeed * MILLISECONDS_UNIT;
   streamer.command.offsetNorth = false;
   streamer.command.offsetSouth = false;
   if (streamer.command.offsetToggle) distanceSinceOffset = 0.0;
   
   
   streamer.command.offsetToggle = false;
   streamer.command.offsetToggleNextMark = false;
   
   if (cutter.setting.simulateOffset == 0)
   {
      if (streamerOffset.state == StreamerOffsetInNorth || streamerOffset.state == StreamerOffsetInSouth && streamerOffset.markState == 0) streamer.command.offsetToggleNextMark = true;
   }
   
   if (!streamerOffset.enableExtendStreamer) output.offsetValve4 = 0;
   lastOffsetMarkReader = stacker.reading.input.offsetMarkReaderCnt;
}

void dbgTxt (char * str)
{
   //static int p;
   strcpy(debugStringOut[dsop],str);
   dsop++;
   if (dsop>9) dsop = 0;
}

void tagliaCrash(void)
{
   static REAL tagliaTorqueAvg;
   tagliaTorqueAvg = (tagliaTorqueAvg * 3 + streamer.reading.tagliaTorque)/4; 
   if (tagliaTorqueAvg > streamer.setting.tagliaTorqueCrash)
   {
      addError(SPAGHETTI_TORQUE_CRASH,NORMAL_STOP,TRUE,0);
      tagliaTorqueAvg = 0;
   }  
}

void torqueCalculation (void)
{
   cmpTtorque = (rabs(streamer.reading.strSec2CurrentRaw) + rabs(streamer.reading.strSec2TorqueRaw)*2)/3;
   cmpTtorque = cmpTtorque * (paperTransport[STREAMER_SECTION_2].setSpeed*0.95 > paperTransport[STREAMER_SECTION_2].isSpeed ? 0.9:1) - paperTransport[STREAMER_SECTION_2].setSpeed*0.1 - (square(paperTransport[STREAMER_SECTION_2].isSpeed)*0.5);
   constrain(&cmpTtorque,0.0,5.0);
   streamer.reading.strSec2Torque = (streamer.reading.strSec2Torque*3 + cmpTtorque)/4;
   streamer.reading.tagliaTorque = (streamer.reading.tagliaTorque*7 + rabs(streamer.reading.tagliaCurrentRaw) + rabs(streamer.reading.tagliaTorqueRaw)*2)/10;
   if (paperTransport[TAGLIA].setSpeed == 0) streamer.reading.tagliaTorque = streamer.reading.tagliaTorque * 0.96;
   if (paperTransport[STREAMER_SECTION_2].setSpeed == 0) streamer.reading.tagliaTorque = streamer.reading.tagliaTorque * 0.96;
}

void strSec2Crash(void)
{
   static REAL strSec2TorqueAvg;
   strSec2TorqueAvg = (strSec2TorqueAvg * 3 + streamer.reading.strSec2Torque)/4;
   if (strSec2TorqueAvg > streamer.setting.streamerSeq2Crash)
   {
      addError(STREAMER_TORQUE_CRASH,NORMAL_STOP,TRUE,0);
      strSec2TorqueAvg = 0;
   } 
}

REAL mm(USINT axis)
{
   return paperTransport[axis].isSpeed * MILLISECONDS_UNIT;
}

void histHandler1 (REAL value, REAL min, REAL max, BOOL res)
{
   //uses histVal1 
   if (res == TRUE) //reset
   {
      for (int i = 0; i < sizeof(histVal1) ; i += 2)
      {
         histVal1[i/2] = 0;
      }
      return;
   }
   if (value >= max) return;
   if (value <= min) return;
   float resStp;
   int seg;
   resStp = (max - min)/(float)(sizeof(histVal1)/2);
    
   seg = (int)(value/resStp) - 1;
   histVal1[seg]++;
   return;
}

void handleWarnings(void)
{
   static REAL subSeqWarningTimer;
   static INT beepTick;
   static USINT beeps;
   
   stacker.reading.status.warning = FALSE;
   stacker.reading.warningCode = 0;
   if (!stacker.reading.input.seqUnitNotReady) subSeqWarningTimer += SECONDS_UNIT;
   else subSeqWarningTimer = 0.0;
   
   if (subSeqWarningTimer > 2.0)
   {
      stacker.reading.status.warning = TRUE;
      stacker.reading.warningCode = 2;
   }
   
   if (!stacker.reading.input.topCoverClosed)
   {
      stacker.reading.status.warning = TRUE;
      stacker.reading.warningCode = 7;
   }
   
   if (ebmGlue.reading.input.in8 && ebmGlue.setting.glueGun.pattern != 0 && globalPower)
   {
      stacker.reading.status.warning = TRUE;
      stacker.reading.warningCode = 20;
   
   }
   
   if (cleanGlueStandStillTime > GLUE_WARNING_TIMEOUT && ebmGlue.setting.glueGun.enable)
   {
      stacker.reading.status.warning = TRUE;
      stacker.reading.warningCode = 21;
      streamer.reading.glueConfirmButton = SHOW;
      beepTick++;
      
      if (beepTick > 500)
      {
         beepTick = 0;
         if (beeps < 250) beeps++;
         if (streamer.reading.mainState == RUN || streamer.reading.mainState == STATE_LOAD || streamer.reading.mainState == STOPPED)
         {   
            if ((!stacker.reading.input.exitStreamerSection1 || !stacker.reading.input.streamerGap1) && paperTransport[STREAMER_SECTION_2].setSpeed) folder.command.beep = true;
            if (beeps > 100) lineController.command.doubleBeep = true;
         }
      }
   }
   else beeps = 0;
}

REAL stepAccDec(REAL m, REAL v, BOOL ff, REAL boost, REAL max, REAL min)
{
   REAL x = 6.0;
   REAL retVal;
   if (ff) x += 1.5;
   x += boost;
   x = x/6;
   m = rabs(m);
    
   retVal = (m * x) + ((v * v) * (x * 6)) + ((m * m) * (x * 8));
    
   if (retVal > max) retVal = max;
   if (retVal < min) retVal = min;
   return retVal;
}

REAL subSeqSlowSpeed (void)
{
   static BOOL slowSetByMe;
   if (!stacker.reading.input.seqUnitNotReady)
   {
      if (streamer.reading.deliverSheetPos == 1 ||  streamer.reading.deliverSheetPos == 2 || streamer.reading.deliverSheetPos == 3 || streamer.reading.deliverOnTheFly == 1 || streamer.reading.deliverOnTheFly == 2) 
      {
         if (!lineController.command.slowSpeed)
         {
             slowSetByMe = true;
             lineController.command.slowSpeed = true;
             lineController.command.flash = true;
         }
      }
   }
   else if (slowSetByMe)
   {
      slowSetByMe = false;
      lineController.command.slowSpeed = false;
      lineController.command.flash = true;
   }
}

REAL subSeqMediumSpeed (void)
{
   static BOOL medSetByMe;
   static REAL timer;
   if (!stacker.reading.input.seqUnitNotReady) {
      timer += SECONDS_UNIT;
   }
   else
      timer = 0.0;
   if (timer > 12.0)
   {  
      if (!lineController.command.mediumSpeed)
      {
         medSetByMe = true;
         lineController.command.mediumSpeed= true;
         lineController.command.flash = true;
      }
   }
   else if (medSetByMe)
   {
      medSetByMe = false;
      lineController.command.mediumSpeed = false;
      lineController.command.flash = false;
   }
}
/*
#define PI 3.14159
#define TERMS 7
REAL power(REAL base, DINT exp) {
   if(exp < 0) {
      if(base == 0)
         return -0; // Error!!
      return 1 / (base * power(base, (-exp) - 1));
   }
   if(exp == 0)
      return 1;
   if(exp == 1)
      return base;
   return base * power(base, exp - 1);
}

INT fact(DINT n) {
   return n <= 0 ? 1 : n * fact(n-1);
}

REAL sine(int deg) {
   deg %= 360; // make it less than 360
   REAL rad = deg * PI / 180;
   REAL sin = 0;

   DINT i;
   for(i = 0; i < TERMS; i++) { // Taylor series
      sin += power(-1, i) * power(rad, 2 * i + 1) / fact(2 * i + 1);
   }
   return sin;
}
*/

REAL rmod (REAL in,REAL w_min, REAL w_max)
{
   REAL tmp,rep;
   rep = w_max - w_min;
   while (in < w_min) in += w_max;
   tmp = in-w_min;
   while (tmp > (rep)) tmp -= rep;
   tmp += w_min;
   return tmp;
}

   
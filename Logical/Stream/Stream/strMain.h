/*------ ERRORS ------*/
#define STREAMER_CRASH                      0
#define LIGHT_CURTAIN                       1
#define STEP_SENSOR                         2
#define LOAD_ERROR                          3
#define JOGGER1_HOMEFAULT                   4
#define JOGGER2_HOMEFAULT                   5
#define ENTER_TRANSPORT                     6
#define EXIT_TRANSPORT                      7
#define ENTER_STACK_GAP_SENSOR              8
#define EXIT_STREAMER_SECTION_1             9
#define TABLE_CLEAR                         10
#define STREAMER_GATE                       11
#define ALIGNER_GATE                        12
#define WASTEBOX_FULL                       13
#define FORK_OVERFULL                       14
#define AXIS_ALIGNER                        15
#define AXIS_STREAMER_SECTION_1             16
#define AXIS_STREAMER_SECTION_2             17
#define AXIS_JOGGER1                        18
#define AXIS_JOGGER2                        19
#define AXIS_JOGGER3                        20
#define AXIS_TAGLIA                         21
#define AXIS_DELIVER                        22
#define OVER_MAX_STACK                      23
#define SPAGHETTI_TORQUE_CRASH              24
#define STACKER_HATCH                       25
#define STREAMER_TORQUE_CRASH               26
#define NO_POWER                            27
#define ENCODER_WHEEL                       28
#define SUB_SEQ_NOT_READY                   29
#define LOW_AIR_PRESSURE                    30
#define STOP_PLATE_MOVED                    31
#define E_STOP                              32
#define JOGGER3_HOMEFAULT                   33
#define GLUE_EMPTY                          34
#define STREAMER_ROLLER_MOVED               35
#define STACK_JOGGERS_NOT_CLOSED            36
#define SOUTH_STOPPLATE_TIMING_ERROR        37
#define HOLD_FINGERS_TIMING_ERROR           38
#define GLUE_AIR_PRESSURE                   39

#define LAST_ERROR                          50

#define SOUTH                               0
#define NORTH                               1

#define SHOW                                0
#define HIDE                                1

#define GLUE_WARNING_TIMEOUT                450   // 7 min 30 seconds

#define STACK_JOGGER_PRESSENT               node[44].station[3]

/************ERRORS*************/
#define NO_ERROR                            0 //Don't use
#define STOP_NEXT                           1 //Not implemented, place holder
#define NORMAL_STOP                         2 //sets cutter.command.stop, and then wait for stop
#define STOP_IMMEDIATELY                    3 //stopps all motors
#define PANIC                               4 //Stop Immediately, And Power Off //Not implemented, place holder
/*-----------*/
#define CONTINUE_LOADED                     0
#define UNLOAD                              1

#define FORKX_ACC                           2.0
#define FORKX_DEC                           2.0
#define FORKX_SPD                           1.5

#define MAX_SPEED                           5.0
#define SECONDS_UNIT                        0.0024
#define MILLISECONDS_UNIT                   2.4
#define FALSE                               0
#define TRUE                                1
#define COLOR_OFF                           0,0,0,0
#define COLOR_RED                           0,1,0,0
#define COLOR_YELLOW                        0,0,1,0
#define COLOR_GREEN                         1,0,0,0
#define COLOR_ORANGE                        0,1,1,0
#define COLOR_WHITE                         0,0,0,1
#define COLOR_PINK                          0,1,0,1
//only button 3
#define COLOR_BLUE                          0,0,0,1
#define COLOR_MAGENTA                       0,1,0,1
#define LIT                                 1,1,1,1

#define INIT                                0
#define STATE_LOAD                          1
#define RUN                                 2
#define ERROR                               3
#define STOPPED                             4

#define ALIGNER                             0
#define STREAMER_SECTION_1                  1
#define STREAMER_SECTION_2                  2
#define JOGGER1                             3
#define JOGGER2                             4
#define JOGGER3                             5
#define TAGLIA                              6
#define DELIVER                             7

#define ALIGN_SPEED_QUEUE_SIZE              8

#define INFEED_TO_FIRST_SENSOR              140  //65.0
#define INFEED_TO_END_SENSOR                1000 //765.0
#define END_SENSOR_TO_STREAMINPUT           140 //240.0
#define STREAMER_SECTION2_LENGNTH           630.0
#define TOTAL_ALIGNER_LENGTH                INFEED_TO_END_SENSOR+END_SENSOR_TO_STREAMINPUT
#define FIRST_SENSOR_TO_STREAMINPUT         TOTAL_ALIGNER_LENGTH-INFEED_TO_FIRST_SENSOR
#define FIRST_SENSOR_TO_SECOND_SENSOR       INFEED_TO_END_SENSOR-INFEED_TO_FIRST_SENSOR

#define GO_TO_ZERO                          0
#define GO_TO_MINSPEED                      1

#define SHEET_SIGNAL                        0
#define ALIGNER_ENTER                       1
#define ALIGNER_EXIT                        2
#define CUTTER_RUNNING                      3
#define DELIVERY                            4

#define NORMAL_PAGE                         0
#define OFFSET_PAGE                         1
#define DELIVER_PAGE                        2
#define END_PAGE                            3
//#define FAKE_DELIVER_PAGE                   3

#define WASTEBOXWARNING                     0
#define SECUNITNOTREADY                     1
#define DELIVERING                          2
#define STOPPEDDELIVERING                   3
#define STEPSENSORWARNING                   4
#define EMPTY_SLOT2                         5
#define MAX_WARNINGID                       6
#define CHK                                 0                                        
#define RST                                 1

#define SET_SERVOSPEED_TO_ZERO              for (int ll = 0; ll<8 ; ll++) paperTransport[ll].setSpeed = 0
#define SET_TO_ZERO_EXCEPT_ALIGNER          for (int ll = 1; ll<8 ; ll++) paperTransport[ll].setSpeed = 0
#define STARTED                             stacker.reading.startUpSeq >= 15

#define POST_PAGE                           0
#define PEEK_PAGE                           1,dummySheet
#define INIT_QUEUE                          2,dummySheet
#define GET_NO_PAGES_IN_QUEUE               3,dummySheet
#define REMOVE_LAST_PAGE                    4,dummySheet
#define PEEK_PAGE_SIZE                      5,dummySheet
#define FIRST_PAGE                          6,dummySheet
#define FORWARD_PEAK_PAGE                   7,dummySheet
#define TRANSPORT_QUEUE_SIZE                32

#define GQ_SIZE                             200

#define LOAD_ERROR_RESET                    0.0,0

void constrain(REAL * val, REAL min, REAL max)
{
   if (*val < min) *val = min;
   if (*val > max) *val = max;
}

REAL square(REAL v)
{
   return v*v;
}

DINT map(DINT x, DINT in_min, DINT in_max, DINT out_min, DINT out_max)
{
   return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max)
{
   return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

BOOL realEqual (REAL valA, REAL valB, REAL tollerance) 
{
   if (valA > (valB-tollerance) && valB < (valB+tollerance))    return TRUE;
   return FALSE;
}

REAL lowest(REAL a, REAL b, REAL c)
{
   if (a<b) {
      if (a<c) return a;
      return c;
   }
   else {
      if (b<c) return b;
      return c;
   }
}

REAL highest(REAL a, REAL b, REAL c)
{
   if (a>b) {
      if (a>c) return a;
      return c;
   }
   else {
      if (b>c) return b;
      return c;
   }
}


REAL rabs(REAL a)
{
   if (a<0) return a*-1.0;
   else return a;
}


BOOL risingEdgeDebounce (BOOL input)
{
   static BOOL s[4];
   static USINT p;
   USINT e,c=0;
    
   s[p++] = input;
   if (p>=4) p = 0;
   e=p;
   do {
      e--;
      if (255 == e) e=3;
      if (0 == c && !s[e]) break;
      if (1 == c && !s[e]) break;
      if (2 == c && s[e]) break;
      if (3 == c && !s[e]) return 1;
      c++;  
   } while (e!=p);
   return 0;
}

REAL overSpeedSimple(REAL v,REAL i,REAL o, BOOL s)
{
   REAL x;
   x = v*o;
   if (GO_TO_ZERO == s) x = (x<i?(v<0.015?0:i):x);
   else x = x<i?i:x;
   return x;
}

REAL rmed5 (REAL v[5])
{
   USINT i; 
   REAL min = 99999999.9;
   REAL max = -99999999.9;
   USINT minNo = 0;
   USINT maxNo = 0;
   REAL sum = 0;
   for (i = 0 ; i < 5; i++)
   {
      if (v[i] < min) {
         min = v[i];
         minNo = i;
      }
      if (v[i] > max) {
         max = v[i];
         maxNo = i;
      }
      sum += v[i];
        
   }
   sum = sum - max - min;
   min = 99999999.9;
   max = -99999999.9;
   for (i = 0 ; i < 5; i++)
   {
      if (i != maxNo && i != minNo)
      {
         if (v[i] < min) min = v[i];
         if (v[i] > max) max = v[i];
      }
        
   }
   sum = sum - max - min;
   return sum;
}


TYPE
	delState_enum : 
		(
		afterDelivery,
		sleeping,
		deliverNext,
		deliverNow,
		afterDeliverNext,
		afterDeliverNow,
		afterDeliverNowX2
		);
	debugStr_typ : 	STRUCT 
		typ : UDINT;
		r1 : REAL;
		r2 : REAL;
		i1 : UDINT;
		i2 : UDINT;
	END_STRUCT;
	errorStruct : 	STRUCT 
		stopType : USINT := 0; (*0 no_error, 1 stop after next job, 2 normalStop, 3 stop and make place for paper, 4 panic*)
		cnt : UDINT := 0;
		subType : USINT;
		active : BOOL := FALSE;
		unload : BOOL := FALSE;
	END_STRUCT;
	joggers_typ : 	STRUCT 
		homePos1 : REAL;
		homePos2 : REAL;
		homePos3 : REAL;
		run : BOOL;
		close : BOOL;
		open : BOOL;
		test : BOOL;
		ready : BOOL;
		masterClosePos : REAL;
		masterOpenPos : REAL;
		wait : USINT;
		state : USINT;
		testSpeed : REAL;
		speed : REAL;
	END_STRUCT;
	stackerStep_typ : 	STRUCT 
		measurements : ARRAY[0..24]OF REAL;
		adj : REAL;
		lastSteps : ARRAY[0..24]OF REAL;
		timer : REAL;
		limitAdj : REAL;
		stpCnt : REAL;
		w : BOOL;
		lsP : USINT;
		lastTblStep : REAL;
		I : REAL;
		MV : REAL;
		NM : USINT;
		p : USINT;
	END_STRUCT;
	sgqFunc : 
		(
		resetQueue,
		addNoGlue,
		addGlue,
		peek,
		removeLast,
		dbg,
		peekNext,
		goNoGlue
		);
	tbTable : 	STRUCT 
		deliverPageRecived : BOOL := FALSE;
		pIn : USINT := 0;
		pOut : USINT := 0;
		pgQueue : ARRAY[0..31]OF sheet;
		pgSequence0 : UDINT := 111111; (*new page registred, wait for input sensor rising edge*)
		pgSequence1 : UDINT := 222222; (*increments transportInfeedCrash*)
		infeedSequenceCrashCnt : USINT := 0; (*transportInfeedCrashCnt	USINT	false	false	false	true	0	should not be greater than 1, two pages sent by cutter none seen.	 	 	19*)
	END_STRUCT;
	asSpeeStruct : 	STRUCT 
		timer : REAL := 0; (*s*)
		speed : REAL := 0; (*m/s*)
		distance : REAL := 0; (*mm*)
	END_STRUCT;
	inSpeed_typ : 	STRUCT 
		is : REAL;
		set : REAL;
		predict : REAL;
		follow : REAL;
		accelerate : BOOL;
		decelerate : BOOL;
	END_STRUCT;
	motor_typ : 	STRUCT 
		cmdReset : REAL; (*m/s*)
		cmdStop : REAL; (*m/s*)
		cmdErrAck : REAL; (*m/s*)
		acceleration : REAL; (*m/s*)
		deceleration : REAL; (*m/s*)
		homed : BOOL; (*m/s*)
		error : BOOL; (*m/s*)
		setSpeed : REAL; (*m/s*)
		move : REAL; (*mm*)
		absMove : REAL; (*mm*)
		goHome : BOOL; (*abs move to 0.0*)
		moveing : BOOL; (*m/s*)
		position : REAL; (*m/s*)
		isSpeed : REAL; (*m/s*)
	END_STRUCT;
	debug_typ : 	STRUCT 
		sheetSignalCnt : UINT;
	END_STRUCT;
	maxSpeed_typ : 	STRUCT 
		standStillDeliver : REAL;
		error : REAL;
		stopped : REAL;
		makeGap : REAL;
		ready : REAL;
		alignerTopCover : REAL;
	END_STRUCT;
	sgqRetVal : 
		(
		glue,
		noGlue,
		err
		);
	deliverFly_typ : 	STRUCT 
		doFlyStateTime : REAL;
		startDeliverDist : REAL;
		targetPos : REAL;
	END_STRUCT;
	deliverRequest_typ : 	STRUCT 
		deliverNowPresses : UINT := 0;
		deliverNextPresses : UINT := 0;
		deliverNextPressed : BOOL;
		deliverNowPressed : BOOL;
		sheetsInState : UDINT := 0;
		lastState : delState_enum := sleeping;
		state : delState_enum := afterDelivery;
	END_STRUCT;
	streamerOffset_typ : 	STRUCT 
		state : soState_enum;
		timeToLiftFingers : REAL := 200; (*ms*)
		waitToExtendStream : REAL := 0.5; (*overlaps*)
		waitToRetractSheets : REAL := 1.5; (*overlaps*)
		markState : USINT := 0;
		time : REAL;
		enableExtendStreamer : BOOL;
		distanceToOffset : REAL;
		simulateOffsetMarkCnt : DINT;
		distance : REAL;
	END_STRUCT;
	soState_enum : 
		(
		StreamerOffsetInit, (*all off*)
		StreamerOffsetInSouth, (*extend hold fingers, plate down*)
		StreamerOffsetInSouthToNorth1, (*down hold fingers*)
		StreamerOffsetInSouthToNorth2, (*down plate*)
		StreamerOffsetInSouthToNorth3, (*retract hold fingers*)
		StreamerOffsetInSouthToNorth4, (*retract hold fingers*)
		StreamerOffsetInNorth (*hold fingers up*)
		);
END_TYPE


PROGRAM _INIT
	//Stacker
   AxisFingerPushObjStr := ADR(gAxStrFPU);
   AxisFingerLiftObjStr := ADR(gAxStrFLI);
   AxisStackerLiftObjStr := ADR(gAxStrSLI);
   //AxisJog1ObjStr := ADR(gAxStrJ1);
   //AxisJog2ObjStr := ADR(gAxStrJ2);
END_PROGRAM

PROGRAM _CYCLIC
   IF (lineController.setup.streamer.installed) THEN
      
      stacker.reading.input.homeForkPush   := physical.streamer.digital.in10;
      stacker.reading.input.homeForkLift   := physical.streamer.digital.in12;
      stacker.reading.input.homeElevator   := physical.streamer.digital.in14;
      

   	(********************MC_BR_SetHardwareInputs***********************)
   	FB_SetHardwareInput_FingerPush.Enable := (NOT(FB_SetHardwareInput_FingerPush.Error));
   	FB_SetHardwareInput_FingerPush.Axis := AxisFingerPushObjStr;
      FB_SetHardwareInput_FingerPush.HomeSwitch := physical.streamer.digital.in10;
      FB_SetHardwareInput_FingerPush();	
   
   	FB_SetHardwareInput_FingerLift.Enable := (NOT(FB_SetHardwareInput_FingerLift.Error));
   	FB_SetHardwareInput_FingerLift.Axis := AxisFingerLiftObjStr;
      FB_SetHardwareInput_FingerLift.HomeSwitch := physical.streamer.digital.in12;
   	FB_SetHardwareInput_FingerLift();	
   	
      FB_SetHardwareInput_StackerLift.Enable := (NOT(FB_SetHardwareInput_StackerLift.Error));
   	FB_SetHardwareInput_StackerLift.Axis := AxisStackerLiftObjStr;
      FB_SetHardwareInput_StackerLift.HomeSwitch := physical.streamer.digital.in14;
      FB_SetHardwareInput_StackerLift();	
      (*
      FB_SetHardwareInput_Jogger1.Enable := (NOT(FB_SetHardwareInput_Jogger1.Error));
      FB_SetHardwareInput_Jogger1.Axis := AxisJog1ObjStr;
      FB_SetHardwareInput_Jogger1.HomeSwitch := physical.streamer.digital.in19;
      FB_SetHardwareInput_Jogger1();	
      
      FB_SetHardwareInput_Jogger2.Enable := (NOT(FB_SetHardwareInput_Jogger2.Error));
      FB_SetHardwareInput_Jogger2.Axis := AxisJog2ObjStr;
      FB_SetHardwareInput_Jogger2.HomeSwitch := physical.streamer.digital.in21;
      FB_SetHardwareInput_Jogger2();
      *)
      (************************** MC_BR_ReadParID *****************************)

      
         
         
         
         CASE parIDStep OF
            0:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrSt1);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 1;
            1:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrSt2);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 2;
            2:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrFPU);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 3;
            3:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrFLI);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 4;
            4:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrDEL);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 5;
            5:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrSLI);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 6;
            6:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrSPA);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 7;
            7:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrJ1);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 8;
            8:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrJ2);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 9;
            9:
               MC_BR_ReadParID_0.Axis := ADR(gAxStrAli);
               MC_BR_ReadParID_0.ParID := 381;
               MC_BR_ReadParID_0.DataAddress := ADR(temperatures[parIDStep]);
               MC_BR_ReadParID_0.DataType := ncPAR_TYP_REAL;
               MC_BR_ReadParID_0.Execute := TRUE;
               parIDStep := 100;
               parIDNextStep := 0;
           100:
           IF MC_BR_ReadParID_0.Done OR MC_BR_ReadParID_0.Error THEN
               MC_BR_ReadParID_0.Execute := FALSE;
            END_IF
               IF MC_BR_ReadParID_0.Execute = FALSE THEN
                  IF split = 5 THEN
                     parIDStep := parIDNextStep;
                     split := split + 1;
                  END_IF
            END_IF
         END_CASE   
      
      
      
      
      CASE parIDStep1 OF
         0:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrSt1);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 1;
         1:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrSt2);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 2;
         2:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrFPU);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 3;
         3:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrFLI);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 4;
         4:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrDEL);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 5;
         5:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrSLI);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 6;
         6:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrSPA);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 7;
         7:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrJ1);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 8;
         8:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrJ2);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 9;
         9:
            MC_BR_ReadParID_1.Axis := ADR(gAxStrAli);
            MC_BR_ReadParID_1.ParID := 214;
            MC_BR_ReadParID_1.DataAddress := ADR(current[parIDStep1]);
            MC_BR_ReadParID_1.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_1.Execute := TRUE;
            parIDStep1 := 100;
            parIDNextStep1 := 0;
         100:
            IF MC_BR_ReadParID_1.Done OR MC_BR_ReadParID_1.Error THEN
               MC_BR_ReadParID_1.Execute := FALSE;
            END_IF     
            IF MC_BR_ReadParID_1.Execute = FALSE THEN
               IF split = 1 OR split = 3 OR split = 7 OR split = 9 THEN
                  parIDStep1 := parIDNextStep1;
                  split := split + 1;
               END_IF
            END_IF
      END_CASE   
      
      CASE parIDStep2 OF
         0:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrSt1);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 1;
         1:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrSt2);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 2;
         2:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrFPU);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 3;
         3:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrFLI);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 4;
         4:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrDEL);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 5;
         5:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrSLI);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 6;
         6:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrSPA);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 7;
         7:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrJ1);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 8;
         8:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrJ2);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 9;
         9:
            MC_BR_ReadParID_2.Axis := ADR(gAxStrAli);
            MC_BR_ReadParID_2.ParID := 277;
            MC_BR_ReadParID_2.DataAddress := ADR(torque[parIDStep2]);
            MC_BR_ReadParID_2.DataType := ncPAR_TYP_REAL;
            MC_BR_ReadParID_2.Execute := TRUE;
            parIDStep2 := 100;
            parIDNextStep2 := 0;
         100:
            IF MC_BR_ReadParID_2.Done OR MC_BR_ReadParID_2.Error THEN
               MC_BR_ReadParID_2.Execute := FALSE;
               
            END_IF     
            IF MC_BR_ReadParID_2.Execute = FALSE THEN
               IF split = 0 OR split = 2 OR split = 4 OR split = 6 OR split = 8 THEN
                  parIDStep2 := parIDNextStep2;
                  split := split + 1;
               END_IF
            END_IF
      END_CASE   
      
      IF split>9 THEN 
         split := 0;
      END_IF
      //global_dbg.real1 := tempTaglia;
      MC_BR_ReadParID_0();	
      MC_BR_ReadParID_1();
      MC_BR_ReadParID_2();
      streamer.reading.tagliaTorqueRaw := torque[6];
      streamer.reading.tagliaCurrentRaw := current[6];
      streamer.reading.strSec2TorqueRaw := torque[1];
      streamer.reading.strSec2CurrentRaw := current[1];
   END_IF
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM
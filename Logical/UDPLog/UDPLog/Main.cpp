
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <AsUDP.h>
#include <fsm.hpp>
#include <ctype.h>

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFFF; 


#define LOG_BUF_SIZE 256

#define LOG_CYCLIC    0
#define LOG_TERMINATE 1
#define LOG_ADDTEXT   2

unsigned char UDPLogIP[] = "192.168.111.242";
UINT UDPLogPort = 4647;

void UDPLogger(int logAction, char *txt)
{
   //static unsigned char logBuffer[LOG_BUF_SIZE][255];
   //static unsigned int logIn = 0;
   static unsigned char logOut = 0;
   static int fState = 0;
   static int stateTimer = 0;
   static unsigned long currentIdent = 0;
   static UdpSend_typ udpS;
   static UdpOpen_typ udpO;
   static UdpClose_typ udpC;
   static UdpRecv_typ udpR;
   static UdpConnect_typ udpCnct;
   
   switch(logAction)
   {
      case LOG_CYCLIC:
         if(fState == 0) // Initialize
         {
            memset((void *)&udpS, 0, sizeof(UdpSend_typ));
            memset((void *)&udpO, 0, sizeof(UdpOpen_typ));
            memset((void *)&udpC, 0, sizeof(UdpClose_typ));
            memset((void *)&udpR, 0, sizeof(UdpRecv_typ));
            memset((void *)&udpCnct, 0, sizeof(UdpConnect_typ));
            fState = 1;
         }
         if(fState == 1) // Open comm
         {
            udpO.pIfAddr = (UDINT)0;
            udpO.port = UDPLogPort;
            udpO.options = 0;
            udpO.enable = 1;
            UdpOpen(&udpO);
            stateTimer = 0;
            fState = 2;
         }
         if(fState == 2) // Wait for UDP open to complete
         {
            if(udpO.status == ERR_FUB_BUSY)
            {
               if(stateTimer > 100)
                  fState = 100;
               else
                  UdpOpen(&udpO);   
            }
            else if(udpO.status != 0)
            {
               fState = 101;
            }
            else
            {
               currentIdent = udpO.ident;
               fState = 3;
            }
         }
         if(fState == 3) // Transmit data
         {
            if(ULog.ix != logOut)
            {
               udpS.pData = (DINT)ULog.text[logOut];
               udpS.datalen = strlen((const char*)ULog.text[logOut]);
               udpS.ident = currentIdent;
               udpS.pHost = (DINT)UDPLogIP;
               udpS.port = UDPLogPort;
               udpS.flags = 0;
               udpS.enable = 1;
               UdpSend(&udpS);
               stateTimer = 0;
               fState = 4;
               logOut++;// = (logOut + 1) % LOG_BUF_SIZE;
               
            }   
         }
         if(fState == 4) // Wait for UDP send to complete
         {
            if(udpS.status == ERR_FUB_BUSY)
            {
               if(stateTimer > 100)
                  fState = 102;
               else
                  UdpSend(&udpS);   
            }
            else if(udpS.status != 0)
            {
               fState = 103;
            }
            else
            {
               fState = 3;
            }
         }
         if(fState == 100) // error: 
         {      
            strcpy(UDPDebug, "UDP open: timeout");
         }
         if(fState == 101) // error: 
         {      
            sprintf(UDPDebug, "%s %d", "UDP open", udpO.status);
         }
         if(fState == 102) // error: 
         {      
            strcpy(UDPDebug, "UDP send: timeout");
         }
         if(fState == 103) // error: 
         {      
            sprintf(UDPDebug, "%s %d", "UDP send", udpS.status);
         }
         stateTimer++;
         break;
      
      case LOG_TERMINATE:
         if(currentIdent)
         {
            udpC.enable = 1;
            udpC.ident = currentIdent;
            UdpClose(&udpC);
         }
         break;
      
      case LOG_ADDTEXT:
         strcpy((char *)ULog.text[ULog.ix], txt);
         ULog.ix++;// = (logIn + 1) % LOG_BUF_SIZE;
         break;
      
      default:
         break;
   }   
}


void _INIT ProgramInit(void)
{
	// Insert code here 

}

void _CYCLIC ProgramCyclic(void)
{
	// Insert code here 
   UDPLogger(LOG_CYCLIC, "");
}

void _EXIT ProgramExit(void)
{
	// Insert code here 

}

(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Slave_00
 * File: slaveInit.st
 * Author: Bernecker + Rainer
 * Created: March 01, 2011
 ********************************************************************
 * Implementation of program Slave_00
 ********************************************************************)

PROGRAM _INIT
   SlaveStep := STATE_WAIT; (* start step *)
END_PROGRAM


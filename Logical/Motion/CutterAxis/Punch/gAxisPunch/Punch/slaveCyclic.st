(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: Slave_00
 * File: slaveCyclic.st
 * Author: Bernecker + Rainer
 * Created: March 01, 2011
 ********************************************************************
 * Implementation OF PROGRAM Slave_00
 ********************************************************************)

PROGRAM _CYCLIC
   IF lineController.setup.merger.punchUnitInstalled THEN
      MC_BR_ReadAxisError_0.Enable := TRUE;
      MC_BR_ReadAxisError_0.Mode := mcACKNOWLEDGE_ALL;
      MC_BR_ReadAxisError_0.Acknowledge := FALSE;
      IF (GlobalCommand.Output.Command.ErrorAcknowledge) THEN
         IF (Slave[SLAVE_INDEX].AxisState.ErrorStop) THEN
            MC_Reset_0.Execute := TRUE;
         ELSIF (Slave[SLAVE_INDEX].Status.DriveStatus.AxisError) THEN
            MC_BR_ReadAxisError_0.Acknowledge := TRUE;
         END_IF	
      END_IF
      IF (MC_Reset_0.Done) THEN
         MC_Reset_0.Execute := FALSE;
      END_IF		
      IF (MC_BR_ReadAxisError_0.Valid) THEN
         Slave[SLAVE_INDEX].Error.AxisErrorCount := MC_BR_ReadAxisError_0.AxisErrorCount;
         Slave[SLAVE_INDEX].Error.AxisWarningCount := MC_BR_ReadAxisError_0.AxisWarningCount;
         Slave[SLAVE_INDEX].Error.FunctionBlockErrorCount := MC_BR_ReadAxisError_0.FunctionBlockErrorCount;
         Slave[SLAVE_INDEX].Error.ErrorRecord := MC_BR_ReadAxisError_0.ErrorRecord;
      END_IF
		
      IF (oldCutCount <> MC_BR_CrossCutterControl_0.CutCount) THEN
         iot_mergerPunchCount := iot_mergerPunchCount +1;
         oldCutCount := MC_BR_CrossCutterControl_0.CutCount;
         //IF (MC_BR_CrossCutterControl_0.CutCount > 1) THEN
         //   sheetCounter2 := sheetCounter2 + 1;
         //END_IF   
      END_IF
	
      //cutPosition2 := MC_BR_CrossCutterControl_0.CutMasterPosition;
      //regMarkPos2 := MC_BR_RegMarkCapture_0.IS.LatchPos;
	
      IF (GlobalCommand.Output.Command.EnableRegMarkControlPunch) THEN
         MC_BR_RegMarkCapture_0.InitData := TRUE;
	      
         IF(searchForMark_punch) THEN
            //Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := 0;
            //Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := 0;
            MC_BR_RegMarkCapture_0.SearchRM := TRUE;
            searchForMark_punch := FALSE;
            waitForFirstMark := TRUE;
            MC_BR_RegMarkCapture_0.InitData := FALSE;         
         END_IF
         IF(MC_BR_RegMarkCapture_0.SearchDone) THEN
            MC_BR_RegMarkCapture_0.SearchRM := FALSE;      
            MC_BR_RegMarkCapture_0.InitData := FALSE;         
         END_IF
	      
         IF (MC_BR_RegMarkCapture_0.ProductsWithoutRM <> productsWithoutRM) THEN
            missedMarks := missedMarks + 1;         
            productsWithoutRM := MC_BR_RegMarkCapture_0.ProductsWithoutRM;
         END_IF
	
         IF ( MC_BR_RegMarkCapture_0.ValidRMs <> validRegMarks ) THEN
            missedMarks := 0;
            expectOMR := TRUE;
            IF (waitForFirstMark) THEN
               waitForFirstMark := FALSE;
               //Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := 20;
               //Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := 20;
               //MC_BR_RegMarkCapture_0.InitData := FALSE;
            END_IF
         END_IF
	            
      ELSE
         missedMarks := 0;
         productsWithoutRM := MC_BR_RegMarkCapture_0.ProductsWithoutRM;
         //Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := 0;
         //Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := 0;
         //MC_BR_RegMarkCapture_0.InitData := FALSE;
      END_IF
      validRegMarks := MC_BR_RegMarkCapture_0.ValidRMs;
	   
	  
      (************CHECK IF POWER SHOULD BE OFF ***************************)
      IF ((GlobalCommand.Output.Command.Power = FALSE) AND (Slave[SLAVE_INDEX].Command.Power = FALSE)) THEN
         MC_BR_CrossCutterControl_0.Enable := FALSE;
         MC_BR_RegMarkCapture_0.Enable := FALSE;
         //MC_BR_TouchProbe_0.Enable := FALSE;
         //MC_BR_TouchProbe_1.Enable := FALSE;
         SlaveStep := STATE_WAIT;
      END_IF
	
      (* central monitoring of the stop command allows a shorter reaction time *)
      IF ((GlobalCommand.Output.Command.E_Stop = TRUE) OR (Slave[SLAVE_INDEX].Command.Stop = TRUE)) THEN
         IF ((GlobalCommand.Output.Command.Power = TRUE) OR (Slave[SLAVE_INDEX].Command.Power = TRUE)) THEN
            SlaveStep := STATE_STOP;
            (* reset all FB execute inputs we use *)
            MC_Home_0.Execute := FALSE;
            MC_Halt_0.Execute := FALSE;
            MC_MoveAbsolute_0.Execute := FALSE;
            MC_MoveAdditive_0.Execute := FALSE;
            MC_MoveVelocity_0.Execute := FALSE;
            MC_BR_CrossCutterControl_0.Enable := FALSE;
            MC_BR_RegMarkCapture_0.Enable := FALSE;
            //MC_BR_TouchProbe_0.Enable := FALSE;
            //MC_BR_TouchProbe_1.Enable := FALSE;
	
				
            Slave[SLAVE_INDEX].Command.MoveJogPos := FALSE;
            Slave[SLAVE_INDEX].Command.MoveJogNeg := FALSE;
         END_IF
      END_IF
	
      (************************** MC_READSTATUS ***************************)
      MC_ReadStatus_0.Enable := NOT(MC_ReadStatus_0.Error);
      MC_ReadStatus_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_ReadStatus_0();
      Slave[SLAVE_INDEX].AxisState.Disabled             := MC_ReadStatus_0.Disabled;
      Slave[SLAVE_INDEX].AxisState.StandStill           := MC_ReadStatus_0.StandStill;
      Slave[SLAVE_INDEX].AxisState.Stopping             := MC_ReadStatus_0.Stopping;
      Slave[SLAVE_INDEX].AxisState.Homing               := MC_ReadStatus_0.Homing;
      Slave[SLAVE_INDEX].AxisState.DiscreteMotion       := MC_ReadStatus_0.DiscreteMotion;
      Slave[SLAVE_INDEX].AxisState.ContinuousMotion     := MC_ReadStatus_0.ContinuousMotion;
      Slave[SLAVE_INDEX].AxisState.SynchronizedMotion   := MC_ReadStatus_0.SynchronizedMotion;
      Slave[SLAVE_INDEX].AxisState.ErrorStop            := MC_ReadStatus_0.Errorstop;
	
      (********************** MC_BR_READDRIVESTATUS ***********************)
      MC_BR_ReadDriveStatus_0.Enable := NOT(MC_BR_ReadDriveStatus_0.Error);
      MC_BR_ReadDriveStatus_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_BR_ReadDriveStatus_0.AdrDriveStatus := ADR(Slave[SLAVE_INDEX].Status.DriveStatus);
      MC_BR_ReadDriveStatus_0();
	
      (********************* MC_READACTUALPOSITION ************************)
      MC_ReadActualPosition_0.Enable := NOT(MC_ReadActualPosition_0.Error);
      MC_ReadActualPosition_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_ReadActualPosition_0();
      IF(MC_ReadActualPosition_0.Valid = TRUE) THEN
         Slave[SLAVE_INDEX].Status.ActPosition := MC_ReadActualPosition_0.Position;
      END_IF
	
      (********************* MC_READACTUALVELOCITY ************************)
      MC_ReadActualVelocity_0.Enable := NOT(MC_ReadActualVelocity_0.Error);
      MC_ReadActualVelocity_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_ReadActualVelocity_0();
      IF(MC_ReadActualVelocity_0.Valid = TRUE) THEN
         Slave[SLAVE_INDEX].Status.ActVelocity := MC_ReadActualVelocity_0.Velocity;
      END_IF
			
      CASE SlaveStep OF
         (****************************** WAIT ********************************)
         STATE_WAIT: (* STATE: Wait *)
            IF ((GlobalCommand.Output.Command.Power = TRUE) OR (Slave[SLAVE_INDEX].Command.Power = TRUE)) THEN
               SlaveStep := STATE_POWER_ON;
            ELSE
               MC_Power_0.Enable := FALSE;
            END_IF
	
            (* reset all FB execute inputs we use *)
            MC_Home_0.Execute := FALSE;
            MC_Halt_0.Execute := FALSE;
            MC_Stop_0.Execute := FALSE;
            MC_MoveAbsolute_0.Execute := FALSE;
            MC_MoveAdditive_0.Execute := FALSE;
            MC_MoveVelocity_0.Execute := FALSE;
            MC_BR_CrossCutterControl_0.Enable := FALSE;
            MC_BR_RegMarkCapture_0.Enable := FALSE;
            //MC_BR_TouchProbe_0.Enable := FALSE;
            //MC_BR_TouchProbe_1.Enable := FALSE;
	
            (* reset user commands *)
            Slave[SLAVE_INDEX].Command.Stop := FALSE;
            Slave[SLAVE_INDEX].Command.MoveJogPos := FALSE;
            Slave[SLAVE_INDEX].Command.MoveJogNeg := FALSE;
            Slave[SLAVE_INDEX].Command.MoveVelocity := FALSE;
            Slave[SLAVE_INDEX].Command.MoveAbsolute := FALSE;
            Slave[SLAVE_INDEX].Command.MoveAdditive := FALSE;
            Slave[SLAVE_INDEX].Command.StartCrossCutterControl := FALSE;
            Slave[SLAVE_INDEX].Command.StopCrossCutterControl := FALSE;
	
            (* End of STATE_WAIT *)
	
            (****************************** POWER ON ****************************)
         STATE_POWER_ON: (* STATE: Power on *)
            IF (PowerSupplyOn = TRUE) THEN
               MC_Power_0.Enable := TRUE;
            END_IF
            IF (MC_Power_0.Status = TRUE) THEN
               SlaveStep := STATE_READY;
            END_IF
	
            (* if a power error occured switch off controller *)
            IF (MC_Power_0.Error = TRUE) THEN
               MC_Power_0.Enable := FALSE;
            END_IF
	
            (* End of STATE_POWER_ON *)
	
            (****************************** READY *******************************)
         STATE_READY: (* STATE: Waiting for Commands *)
            IF ((Slave[SLAVE_INDEX].Command.Home = TRUE) OR (GlobalCommand.Output.Command.Home = TRUE)) THEN
               Slave[SLAVE_INDEX].Command.Home := FALSE;
               SlaveStep := STATE_HOME;
            ELSIF ((Slave[SLAVE_INDEX].Command.Stop = TRUE) OR (GlobalCommand.Output.Command.E_Stop = TRUE)) THEN
               SlaveStep := STATE_STOP;
            ELSIF (Slave[SLAVE_INDEX].Command.MoveJogPos = TRUE) THEN
               SlaveStep := STATE_JOG_POSITIVE;
            ELSIF (Slave[SLAVE_INDEX].Command.MoveJogNeg = TRUE) THEN
               SlaveStep := STATE_JOG_NEGATIVE;
            ELSIF (Slave[SLAVE_INDEX].Command.MoveAbsolute = TRUE) THEN
               Slave[SLAVE_INDEX].Command.MoveAbsolute := FALSE;
               SlaveStep := STATE_MOVE_ABSOLUTE;
            ELSIF (Slave[SLAVE_INDEX].Command.MoveAdditive = TRUE) THEN
               Slave[SLAVE_INDEX].Command.MoveAdditive := FALSE; 
               SlaveStep := STATE_MOVE_ADDITIVE;
            ELSIF (Slave[SLAVE_INDEX].Command.MoveVelocity = TRUE) THEN
               Slave[SLAVE_INDEX].Command.MoveVelocity := FALSE;
               SlaveStep := STATE_MOVE_VELOCITY;
            ELSIF (Slave[SLAVE_INDEX].Command.Halt = TRUE) THEN
               Slave[SLAVE_INDEX].Command.Halt := FALSE;
               SlaveStep := STATE_HALT;
	
            ELSIF ((Slave[SLAVE_INDEX].Command.StartCrossCutterControl = TRUE) OR (GlobalCommand.Output.Command.ConnectSlavesToMaster = TRUE)) THEN
               MC_BR_CrossCutterControl_0.Enable := FALSE;
               Slave[SLAVE_INDEX].Command.StartCrossCutterControl := FALSE;
               MC_BR_RegMarkCapture_0.SearchRM := FALSE;
               SlaveStep := STATE_CROSSCUTTER;
	
            END_IF
	
            (* End of STATE_READY *)
	
            (****************************** HOME ********************************)
         STATE_HOME: (* STATE: start homing process *)
            MC_Home_0.Position := Slave[SLAVE_INDEX].Parameter.HomePosition;
            MC_Home_0.HomingMode := Slave[SLAVE_INDEX].Parameter.HomeMode;
            MC_Home_0.Execute := TRUE;
            IF (MC_Home_0.Done = TRUE) THEN
               MC_Home_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
		
            (* if a homing error occured go to ready state *)
            IF ((MC_Home_0.Error = TRUE) OR (MC_Home_0.CommandAborted = TRUE)) THEN
               MC_Home_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
            (* End of STATE_HOME *)
	
            (*************************** HALT MOVEMENT **************************)
         STATE_HALT: (* STATE: Stop the active movement *)
            MC_Halt_0.Deceleration := Slave[SLAVE_INDEX].Parameter.Deceleration;
            MC_Halt_0.Execute := TRUE;
            IF (MC_Halt_0.Done = TRUE) THEN
               MC_Halt_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* check if error occured *)
            IF ((MC_Halt_0.Error = TRUE) OR (MC_Halt_0.CommandAborted = TRUE)) THEN
               MC_Halt_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* End of STATE_HALT *)
	
            (*************************** STOP MOVEMENT **************************)
         STATE_STOP: (* STATE: Stop movement as long as command is set *)
            MC_Stop_0.Deceleration := Slave[SLAVE_INDEX].Parameter.Deceleration;
            MC_Stop_0.Execute := TRUE;
	
            (* if axis is stopped go to ready state *)
            IF ((MC_Stop_0.Done = TRUE) AND (Slave[SLAVE_INDEX].Command.Stop = FALSE) AND (GlobalCommand.Output.Command.E_Stop = FALSE)) THEN
               MC_Stop_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* check if error occured *)
            IF (((MC_Stop_0.Error = TRUE) OR (MC_Stop_0.CommandAborted = TRUE)) AND
               (Slave[SLAVE_INDEX].Command.Stop = FALSE) AND
               (GlobalCommand.Output.Command.E_Stop = FALSE)) THEN
               MC_Stop_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* End of STATE_STOP *)
	
            (******************* START JOG MOVEMENT POSITIVE ********************)
         STATE_JOG_POSITIVE: (* STATE: Start jog movement in positive direction *)
            MC_MoveVelocity_0.Velocity      := Slave[SLAVE_INDEX].Parameter.JogVelocity;
            MC_MoveVelocity_0.Acceleration  := Slave[SLAVE_INDEX].Parameter.Acceleration;
            MC_MoveVelocity_0.Deceleration  := Slave[SLAVE_INDEX].Parameter.Deceleration;
            MC_MoveVelocity_0.Direction     := mcPOSITIVE_DIR;
            MC_MoveVelocity_0.Execute := 1;
	
            (* check if jog movement should be stopped *)
            IF (Slave[SLAVE_INDEX].Command.MoveJogPos = FALSE) THEN
               MC_MoveVelocity_0.Execute := FALSE;
               SlaveStep := STATE_HALT;
            END_IF
	
            IF ((MC_MoveVelocity_0.Error = TRUE) OR (MC_MoveVelocity_0.CommandAborted = TRUE)) THEN
               MC_MoveVelocity_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* End of STATE_JOG_POSITIVE *)
	
            (********************START JOG MOVEMENT NEGATIVE ********************)
         STATE_JOG_NEGATIVE: (* STATE: Start jog movement in negative direction *)
            MC_MoveVelocity_0.Velocity      := Slave[SLAVE_INDEX].Parameter.JogVelocity;
            MC_MoveVelocity_0.Acceleration  := Slave[SLAVE_INDEX].Parameter.Acceleration;
            MC_MoveVelocity_0.Deceleration  := Slave[SLAVE_INDEX].Parameter.Deceleration;
            MC_MoveVelocity_0.Direction     := mcNEGATIVE_DIR;
            MC_MoveVelocity_0.Execute := 1;
	
            (* check if jog movement should be stopped *)
            IF (Slave[SLAVE_INDEX].Command.MoveJogNeg = FALSE) THEN
               MC_MoveVelocity_0.Execute := FALSE;
               SlaveStep := STATE_HALT;
            END_IF
	
            IF ((MC_MoveVelocity_0.Error = TRUE) OR (MC_MoveVelocity_0.CommandAborted = TRUE)) THEN
               MC_MoveVelocity_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* End of STATE_JOG_NEGATIVE *)
	
            (********************* START ABSOLUTE MOVEMENT **********************)
         STATE_MOVE_ABSOLUTE: (* STATE: Start absolute movement *)
            MC_MoveAbsolute_0.Position      := Slave[SLAVE_INDEX].Parameter.Position;
            MC_MoveAbsolute_0.Velocity      := Slave[SLAVE_INDEX].Parameter.Velocity;
            MC_MoveAbsolute_0.Acceleration  := Slave[SLAVE_INDEX].Parameter.Acceleration;
            MC_MoveAbsolute_0.Deceleration  := Slave[SLAVE_INDEX].Parameter.Deceleration;
            MC_MoveAbsolute_0.Direction     := Slave[SLAVE_INDEX].Parameter.Direction;
            MC_MoveAbsolute_0.Execute := 1;
	
            IF (Slave[SLAVE_INDEX].Command.Halt = TRUE) THEN
               Slave[SLAVE_INDEX].Command.Halt := 0;
               MC_MoveAbsolute_0.Execute := 0;
               SlaveStep := STATE_HALT;
            ELSIF (MC_MoveAbsolute_0.Done = TRUE) THEN
               MC_MoveAbsolute_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            IF ((MC_MoveAbsolute_0.Error = TRUE) OR (MC_MoveAbsolute_0.CommandAborted = TRUE)) THEN
               MC_MoveAbsolute_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* End of STATE_MOVE_ABSOLUTE *)
	
            (********************* START ADDITIVE MOVEMENT **********************)
         STATE_MOVE_ADDITIVE: (* STATE: Start additive movement *)
            MC_MoveAdditive_0.Distance      := Slave[SLAVE_INDEX].Parameter.Distance;
            MC_MoveAdditive_0.Velocity      := Slave[SLAVE_INDEX].Parameter.Velocity;
            MC_MoveAdditive_0.Acceleration  := Slave[SLAVE_INDEX].Parameter.Acceleration;
            MC_MoveAdditive_0.Deceleration  := Slave[SLAVE_INDEX].Parameter.Deceleration;
            MC_MoveAdditive_0.Execute := 1;
            IF (Slave[SLAVE_INDEX].Command.Halt = TRUE) THEN
               Slave[SLAVE_INDEX].Command.Halt := FALSE;
               MC_MoveAdditive_0.Execute := FALSE;
               SlaveStep := STATE_HALT;
            ELSIF (MC_MoveAdditive_0.Done = TRUE) THEN
               MC_MoveAdditive_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* check IF error occured *)
            IF ((MC_MoveAdditive_0.Error = TRUE) OR (MC_MoveAdditive_0.CommandAborted = TRUE)) THEN
               MC_MoveAdditive_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* End of STATE_MOVE_ADDITIVE *)
	
            (********************* START VELOCITY MOVEMENT **********************)
         STATE_MOVE_VELOCITY: (* STATE: Start velocity movement *)
            MC_MoveVelocity_0.Velocity      := Slave[SLAVE_INDEX].Parameter.Velocity;
            MC_MoveVelocity_0.Acceleration  := Slave[SLAVE_INDEX].Parameter.Acceleration;
            MC_MoveVelocity_0.Deceleration  := Slave[SLAVE_INDEX].Parameter.Deceleration;
            MC_MoveVelocity_0.Direction     := Slave[SLAVE_INDEX].Parameter.Direction;
            MC_MoveVelocity_0.Execute := 1;
	
            IF (Slave[SLAVE_INDEX].Command.Halt = TRUE) THEN
               Slave[SLAVE_INDEX].Command.Halt := FALSE;
               MC_MoveVelocity_0.Execute := FALSE;
               SlaveStep := STATE_HALT;
            ELSIF (MC_MoveVelocity_0.InVelocity = TRUE) THEN
               MC_MoveVelocity_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
            (* check if error occured *)
            IF ((MC_MoveVelocity_0.Error = TRUE) OR (MC_MoveVelocity_0.CommandAborted = TRUE)) THEN
               MC_MoveVelocity_0.Execute := FALSE;
               SlaveStep := STATE_READY;
            END_IF
	
            (* End of STATE_MOVE_ADDITIVE *)
			
            (***********************STATE_CROSSCUTTER***************************)
         STATE_CROSSCUTTER:   (* STATE:  *)
		
				
            IF GlobalCommand.Output.Command.EnableRegMarkControlPunch = TRUE THEN
               MC_BR_CrossCutterControl_0.InitData := MC_BR_RegMarkCapture_0.Valid;
               MC_BR_CrossCutterControl_0.EnableCut := GlobalCommand.Output.Command.EnableCutPunch;
               MC_BR_CrossCutterControl_0.CutRangeMaster := Slave[SLAVE_INDEX].Parameter.CutRangeMaster;
               MC_BR_CrossCutterControl_0.CutRangeSlave := Slave[SLAVE_INDEX].Parameter.CutRangeSlave;
               MC_BR_CrossCutterControl_0.ProductLength := Slave[SLAVE_INDEX].Parameter.ProductLength;
               //IF -MC_BR_RegMarkCapture_0.AdditionalInfo.ActualCorrectionValue > Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitNeg THEN
               //   MC_BR_CrossCutterControl_0.ProductLengthCorrection := format + MC_BR_RegMarkCapture_0.AdditionalInfo.ActualCorrectionValue; 
               //ELSE
               MC_BR_CrossCutterControl_0.ProductLengthCorrection := MC_BR_RegMarkCapture_0.CorrectionValue;
               //END_IF
               MC_BR_CrossCutterControl_0.AdvancedParameters := Slave[SLAVE_INDEX].Parameter.AdvancedParameter;
               MC_BR_CrossCutterControl_0.Enable := TRUE;
						 
               MC_BR_RegMarkCapture_0.Enable := MC_BR_CrossCutterControl_0.Active;
	            
               //MC_BR_TouchProbe_0.Enable := MC_BR_CrossCutterControl_0.Active;
               //MC_BR_TouchProbe_1.Enable := MC_BR_CrossCutterControl_0.Active;
	            
               MC_BR_RegMarkCapture_0.SearchRM; (* Starts or re-initiates the search for a registration mark manually *)
               //MC_BR_RegMarkCapture_0.InitData := GlobalCommand.Output.Command.InitDataPunch; (* Initializes input data on a rising edge (online change of function block input data) *)
               MC_BR_RegMarkCapture_0.CutPosition := MC_BR_CrossCutterControl_0.CutMasterPosition; (* "Frozen" position value where the cut will be performed [axis units of master] *)
               MC_BR_RegMarkCapture_0.Configuration := Slave[SLAVE_INDEX].Parameter.RegMarkConfiguration; (* Required configuration parameters *)
               MC_BR_RegMarkCapture_0.AdvancedParameters := Slave[SLAVE_INDEX].Parameter.RegMarkAdvancedParameter; (* Structure for using advanced functions *)
            ELSE
               MC_BR_RegMarkCapture_0.Enable := FALSE;
               //MC_BR_TouchProbe_0.Enable := FALSE;
               //MC_BR_TouchProbe_1.Enable := FALSE;
               MC_BR_CrossCutterControl_0.InitData := GlobalCommand.Output.Command.InitDataPunch;
               MC_BR_CrossCutterControl_0.EnableCut := GlobalCommand.Output.Command.EnableCutPunch;
               MC_BR_CrossCutterControl_0.CutRangeMaster := Slave[SLAVE_INDEX].Parameter.CutRangeMaster; 
               MC_BR_CrossCutterControl_0.CutRangeSlave := Slave[SLAVE_INDEX].Parameter.CutRangeSlave;
               MC_BR_CrossCutterControl_0.ProductLength := Slave[SLAVE_INDEX].Parameter.ProductLength;
               MC_BR_CrossCutterControl_0.ProductLengthCorrection := Slave[SLAVE_INDEX].Parameter.ProductLengthCorrection;
               MC_BR_CrossCutterControl_0.AdvancedParameters := Slave[SLAVE_INDEX].Parameter.AdvancedParameter;
               MC_BR_CrossCutterControl_0.Enable := TRUE;
            END_IF
			
		
            IF ((Slave[SLAVE_INDEX].Command.StopCrossCutterControl = TRUE) OR (GlobalCommand.Output.Command.DisconnectSlavesFromMaster = TRUE)) THEN
               Slave[SLAVE_INDEX].Command.StopCrossCutterControl := 0;
               MC_BR_CrossCutterControl_0.Enable := FALSE;
               MC_BR_RegMarkCapture_0.Enable := FALSE;
               //MC_BR_TouchProbe_0.Enable := FALSE;
               //MC_BR_TouchProbe_1.Enable := FALSE;
               SlaveStep := STATE_READY;
            END_IF
            (* check if error occured *)
            IF (MC_BR_CrossCutterControl_0.Error = TRUE) THEN
               MC_BR_CrossCutterControl_0.Enable := FALSE;
               MC_BR_RegMarkCapture_0.Enable := FALSE;
               //MC_BR_TouchProbe_0.Enable := FALSE;
               //MC_BR_TouchProbe_1.Enable := FALSE;
               SlaveStep := STATE_READY;
            END_IF                                         
	
	
	
         (*********************** SEQUENCE END *******************************)
      END_CASE
	
	(*********************************************************************
	        FUNCTION Block Calls
	*********************************************************************)
	
	(*************************** MC_POWER *******************************)
      MC_Power_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_Power_0();
	
      (**************************** MC_HOME *******************************)
      MC_Home_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_Home_0();
	
      (************************ MC_MOVEABSOLUTE ***************************)
      MC_MoveAbsolute_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_MoveAbsolute_0();
	
      (************************ MC_MOVEADDITIVE ***************************)
      MC_MoveAdditive_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_MoveAdditive_0();
	
      (************************ MC_MOVEVELOCITY ***************************)
      MC_MoveVelocity_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_MoveVelocity_0();
	
      (**************************** MC_STOP *******************************)
      MC_Stop_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_Stop_0();
	
      (*************************** MC_HALT ********************************)
      MC_Halt_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_Halt_0();
	
      (************************** MC_REGMARKCAPTURE *****************************)
      MC_BR_RegMarkCapture_0.Master := MasterRef[0];
      MC_BR_RegMarkCapture_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_BR_RegMarkCapture_0();
	
      (************************** MC_TOUCHPROBE *****************************)
      //MC_BR_TouchProbe_0.Axis := SlaveRef[SLAVE_INDEX];
      //MC_BR_TouchProbe_0.TriggerInput.EventSourceParID := ACP10PAR_STAT_TRIGGER1;
      //MC_BR_TouchProbe_0.TriggerInput.Edge := mcP_EDGE; 
      //MC_BR_TouchProbe_0.TriggerInput.ProbeParID := 542; //was 542
      //MC_BR_TouchProbe_0.Mode := mcWITHOUT_PERIOD;
      //MC_BR_TouchProbe_0();
	
      //MC_BR_TouchProbe_1.Axis := SlaveRef[SLAVE_INDEX];
      //MC_BR_TouchProbe_1.TriggerInput.EventSourceParID := ACP10PAR_STAT_TRIGGER1;
      //MC_BR_TouchProbe_1.TriggerInput.Edge := mcN_EDGE; 
      //MC_BR_TouchProbe_1.TriggerInput.ProbeParID := 542;
      //MC_BR_TouchProbe_1.Mode := mcWITHOUT_PERIOD;
      //MC_BR_TouchProbe_1();
	   
      //IF (MC_BR_TouchProbe_1.RecordedValue - MC_BR_TouchProbe_0.RecordedValue > 500) THEN
      //IF (MC_BR_TouchProbe_1.RecordedValue - MC_BR_TouchProbe_0.RecordedValue > 50 AND MC_BR_TouchProbe_1.RecordedValue - MC_BR_TouchProbe_0.RecordedValue < 400) THEN
      //   markWidth := MC_BR_TouchProbe_1.RecordedValue - MC_BR_TouchProbe_0.RecordedValue; 
      //END_IF
      //markWidth := MC_BR_TouchProbe_0.RecordedValue - MC_BR_RegMarkCapture_0.IS.LatchPos;
	   
      (************************** MC_CROSSCUTTER *****************************)
      MC_BR_CrossCutterControl_0.Master := MasterRef[0];
      MC_BR_CrossCutterControl_0.Slave := SlaveRef[SLAVE_INDEX];
      MC_BR_CrossCutterControl_0();
      GlobalCommand.Input.Command.InitDataPunch := FALSE;
	
	
      (************************** MC_BR_SetHardwareInputs ********************)
      MC_BR_SetHardwareInputs_0.Enable := (NOT(MC_BR_SetHardwareInputs_0.Error));
      MC_BR_SetHardwareInputs_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_BR_SetHardwareInputs_0.HomeSwitch := physical.merger.digital.in5;//  homePosPunch;
      MC_BR_SetHardwareInputs_0();	
	
      (************************** MC_RESET *****************************)
      MC_Reset_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_Reset_0();		
	
      (************************** MC_BR_ReadAxisError *****************************)
      MC_BR_ReadAxisError_0.Axis := SlaveRef[SLAVE_INDEX];
      iot_mergerPunchError := MC_BR_ReadAxisError_0.ErrorID;
      MC_BR_ReadAxisError_0();		

      (************************** MC_BR_ReadAxisError *****************************)
      MC_BR_ReadAxisError_0.Axis := SlaveRef[SLAVE_INDEX];
      MC_BR_ReadAxisError_0();		
      iot_mergerPunchError := MC_BR_ReadAxisError_0.ErrorRecord.Number;
   
      fbReadTemp(
      Axis		:= SlaveRef[SLAVE_INDEX], 
      Enable		:= NOT fbReadTemp.Error, 
      ParID		:= ACP10PAR_TEMP_MOTOR, 
      DataAddress	:= ADR(iot_mergerPunchTemp), 
      DataType	:= ncPAR_TYP_REAL, 
      Mode		:= mcONE_RECORD);
      fbReadTorque(
      Axis		:= SlaveRef[SLAVE_INDEX], 
      Enable		:= NOT fbReadTorque.Error, 
      ParID		:= ACP10PAR_TORQUE_ACT, 
      DataAddress	:= ADR(iot_mergerPunchTorque), 
      DataType	:= ncPAR_TYP_REAL, 
      Mode		:= mcONE_RECORD);
      fbReadCurr(
      Axis		:= SlaveRef[SLAVE_INDEX], 
      Enable		:= NOT fbReadCurr.Error, 
      ParID		:= ACP10PAR_ICTRL_ISQ_ACT, 
      DataAddress	:= ADR(iot_mergerPunchCurrent_Q), 
      DataType	:= ncPAR_TYP_REAL, 
      Mode		:= mcONE_RECORD);		
   ELSE
      //merger.reading.correctionValue := 0.0;
   END_IF 
END_PROGRAM
﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.7.54 SP?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Initialization code">axisConfigurationInit.st</File>
    <File Description="Cyclic code">axisControlCyclic.st</File>
    <File Description="Local variables" Private="true">axisControl.var</File>
  </Files>
</Program>
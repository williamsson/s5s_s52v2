(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AxisControl
 * File: axisConfigurationInit.st
 * Author: Bernecker + Rainer
 * Created: March 01, 2011
 ********************************************************************
 * Implementation of program AxisControl
 ********************************************************************)

PROGRAM _INIT
	Safety_ESPEActive;
	Safety_ESPEStatus;
	Safety_SPEActive_Ok;
	Safety_SignalFeedback;
	//SafetyStatusDrives;
	
	PsmRef[POWER_SUPPLY_INDEX] := ADR(gAxPower); (* axis reference of the power supply module *)
	MasterRef[MASTER_AXIS_INDEX] := ADR(gAxVirtMa); (* axis reference of first master axis *)
   SlaveRef[SLAVE_MAININFEED_INDEX] := ADR(gAxMainInfeed); (* axis reference of 2 slave axis *)
   SlaveRef[SLAVE_MAINKNIFE_INDEX] := ADR(gAxMainKnife); (* axis reference of 3 slave axis *)
	SlaveRef[SLAVE_SECONDARYKNIFE_INDEX] := ADR(gAxSecondaryKnife); (* axis reference of 3 slave axis *)
   SlaveRef[SLAVE_PUNCH_INDEX] := ADR(gAxPunch); (* axis reference of 3 slave axis *)
   SlaveRef[SLAVE_SUPPORT_INDEX] := ADR(gAxSupp); (* axis reference of 6 slave axis *)
   SlaveRef[SLAVE_REJECT_INDEX] := ADR(gAxReject); (* axis reference of 7 slave axis *)
   //.90 lsi demo maskin
   SlaveRef[SLAVE_BUF_OUT_INDEX] := ADR(gABufOut); (* axis reference of 4 slave axis (gAxBufOut (1-phase))(gABufOut (3-phase))*)
   //.98, h�kans lina
   //SlaveRef[SLAVE_BUF_OUT_INDEX] := ADR(gAxBufOut); (* axis reference of 4 slave axis (gAxBufOut (1-phase))(gABufOut (3-phase))*)
   SlaveRef[SLAVE_PLOW_IN_INDEX] := ADR(gAxPFIn); (* axis reference of 5 slave axis *)
   SlaveRef[SLAVE_PLOW_OUT_INDEX] := ADR(gAxPFOut); (* axis reference of 6 slave axis *)
	SlaveRef[SLAVE_MERGE_INFEED_INDEX] := ADR(gAxMergeInfeed); (* axis reference of 6 slave axis *)
	ExtraRef := ADR(gAxAngle);

	PowerSupplyOn := TRUE; (* if a power supply is used, this variable is reset in the according task *)
	
	GlobalCommand.Output.Command.Power := FALSE;
	GlobalCommand.Output.Command.Home := FALSE;
	GlobalCommand.Output.Command.E_Stop := FALSE;
	GlobalCommand.Output.Command.ConnectSlavesToMaster := FALSE;
	GlobalCommand.Output.Command.DisconnectSlavesFromMaster := FALSE;
	GlobalCommand.Output.Command.MoveVelocity := FALSE;
	GlobalCommand.Output.Command.EnableCutMain := FALSE;
	GlobalCommand.Output.Command.EnableCutSecondary := FALSE;
	GlobalCommand.Output.Command.EnableCutPunch := FALSE;
	GlobalCommand.Output.Command.InitDataMain := FALSE;
	GlobalCommand.Output.Command.InitDataSecondary := FALSE;
	GlobalCommand.Output.Command.InitDataPunch := FALSE;
	GlobalCommand.Output.Command.EnableRegMarkControlMain := FALSE;
	GlobalCommand.Output.Command.EnableRegMarkControlSecondary := FALSE;
	GlobalCommand.Output.Command.EnableRegMarkControlPunch := FALSE;
	
	GlobalCommand.Input.Parameter.Velocity := 1000;
	
	Master[MASTER_AXIS_INDEX].Parameter.Velocity       := 1000; (* velocity for movement *)
	Master[MASTER_AXIS_INDEX].Parameter.Acceleration   := 300; (* 750 acceleration for movement *)
	Master[MASTER_AXIS_INDEX].Parameter.Deceleration   := 6074; (* deceleration for movement *)
	Master[MASTER_AXIS_INDEX].Parameter.JogVelocity    := 400;  (* velocity for jogging *)

   Slave[SLAVE_MAININFEED_INDEX].Parameter.Velocity       := 1000; (* velocity for movement *)
   Slave[SLAVE_MAININFEED_INDEX].Parameter.Acceleration   := 5000; (* acceleration for movement *)
   Slave[SLAVE_MAININFEED_INDEX].Parameter.Deceleration   := 5000; (* deceleration for movement *)
   Slave[SLAVE_MAININFEED_INDEX].Parameter.JogVelocity    := 400;  (* velocity for jogging *)
   Slave[SLAVE_MAININFEED_INDEX].Parameter.RatioNumerator       := 10000; (*ratio numerator for coupling *)
   Slave[SLAVE_MAININFEED_INDEX].Parameter.RatioDenominator     := 10000; (*ratio denominator for coupling *)

	Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.Velocity       := 1000; (* velocity for movement *)
	Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.Acceleration   := 500000; (* acceleration for movement *)
	Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.Deceleration   := 500000; (* deceleration for movement *)
	Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.JogVelocity    := 400;  (* velocity for jogging *)
	Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.RatioNumerator       := 10000; (*ratio numerator for coupling *)
	Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.RatioDenominator     := 10000; (*ratio denominator for coupling *)
		
	Slave[SLAVE_SUPPORT_INDEX].Parameter.Velocity       := 1000; (* velocity for movement *)
	Slave[SLAVE_SUPPORT_INDEX].Parameter.Acceleration   := 5000; (* acceleration for movement *)
	Slave[SLAVE_SUPPORT_INDEX].Parameter.Deceleration   := 5000; (* deceleration for movement *)
	Slave[SLAVE_SUPPORT_INDEX].Parameter.JogVelocity    := 400;  (* velocity for jogging *)
	Slave[SLAVE_SUPPORT_INDEX].Parameter.RatioNumerator       := 10600; (*ratio numerator for coupling *) 
	Slave[SLAVE_SUPPORT_INDEX].Parameter.RatioDenominator     := 10000; (*ratio denominator for coupling *)

   Slave[SLAVE_REJECT_INDEX].Parameter.Velocity       := 1000; (* velocity for movement *)
   Slave[SLAVE_REJECT_INDEX].Parameter.Acceleration   := 5000; (* acceleration for movement *)
   Slave[SLAVE_REJECT_INDEX].Parameter.Deceleration   := 5000; (* deceleration for movement *)
   Slave[SLAVE_REJECT_INDEX].Parameter.JogVelocity    := 400;  (* velocity for jogging *)
   Slave[SLAVE_REJECT_INDEX].Parameter.RatioNumerator       := 16000; (* was 1200 ratio numerator for coupling *) 
   Slave[SLAVE_REJECT_INDEX].Parameter.RatioDenominator     := 10000; (*ratio denominator for coupling *)

	Slave[SLAVE_BUF_OUT_INDEX].Parameter.Velocity       := 1000; (* velocity for movement *)
	Slave[SLAVE_BUF_OUT_INDEX].Parameter.Acceleration   := 500000; (* acceleration for movement *)
	Slave[SLAVE_BUF_OUT_INDEX].Parameter.Deceleration   := 500000; (* deceleration for movement *)
	Slave[SLAVE_BUF_OUT_INDEX].Parameter.JogVelocity    := 400;  (* velocity for jogging *)
	Slave[SLAVE_BUF_OUT_INDEX].Parameter.RatioNumerator       := 10000; (*ratio numerator for coupling *)
	Slave[SLAVE_BUF_OUT_INDEX].Parameter.RatioDenominator     := 10000; (*ratio denominator for coupling *)

   Slave[SLAVE_PLOW_IN_INDEX].Parameter.Velocity           := 1000; (* velocity for movement *)
   Slave[SLAVE_PLOW_IN_INDEX].Parameter.Acceleration       := 500000; (* acceleration for movement *)
   Slave[SLAVE_PLOW_IN_INDEX].Parameter.Deceleration       := 500000; (* deceleration for movement *)
   Slave[SLAVE_PLOW_IN_INDEX].Parameter.JogVelocity        := 400;  (* velocity for jogging *)
   Slave[SLAVE_PLOW_IN_INDEX].Parameter.RatioNumerator     := 10000; (*ratio numerator for coupling *)
   Slave[SLAVE_PLOW_IN_INDEX].Parameter.RatioDenominator   := 10000; (*ratio denominator for coupling *)

   Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Velocity           := 1000; (* velocity for movement *)
   Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Acceleration       := 500000; (* acceleration for movement *)
   Slave[SLAVE_PLOW_OUT_INDEX].Parameter.Deceleration       := 500000; (* deceleration for movement *)
   Slave[SLAVE_PLOW_OUT_INDEX].Parameter.JogVelocity        := 400;  (* velocity for jogging *)
   Slave[SLAVE_PLOW_OUT_INDEX].Parameter.RatioNumerator     := 10000; (*ratio numerator for coupling *)
   Slave[SLAVE_PLOW_OUT_INDEX].Parameter.RatioDenominator   := 10000; (*ratio denominator for coupling *)
   
   (* SETTINGS FOR KNIFES *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.Velocity := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.Velocity       := 10000; (* velocity for movement *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.Acceleration := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.Acceleration   := 50000; (* acceleration for movement *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.Deceleration := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.Deceleration   := 50000; (* deceleration for movement *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.JogVelocity := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.JogVelocity    := 4000;  (* velocity for jogging *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.HomePosition := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.HomePosition	  := 26500; (* Position after homing is done *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.HomeMode := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.HomeMode       := mcHOME_SWITCH_GATE; (* Homeing mode *)
	(* Parameters for crosscutter *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := 12.7; (* Cutting area of master axis within which the cross cutter ("slave") should run synchronously to the "master" *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeSlave := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave  := 3000; (* Cutting area of slave axis within which the cross cutter ("slave") should run synchronously to the "master"  *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.ProductLength := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.ProductLength  := 305.0; (* Distance between two cuts (product cycle) *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.ProductLengthCorrection := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.ProductLengthCorrection    := 0; (* This value is added to the "ProductLength" input before "ProductLength" is transferred to the drive. This makes it possible to correct a cut using a value determined from a higher-level registration mark control process. *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.StartMode := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.StartMode := mcMASTER_POSITION; (* Mode for the initial movement after the function block is enabled *)
   Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.FirstCutPosition := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.FirstCutPosition := 94330; (* Position within the master axis period or absolute position for a non-periodic master axis where the first cut should take place  *)
   Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.ProductLength / 2; (* Distance of the master axis in which the cross cutter should be synchronized to the first cut position *)
   Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.StartInterval := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.StartInterval := 0; (* Interval for generating a starting event (a multiple of "StartInterval" is added to "FirstCutPosition" so that the movement can be started in synchronization) *)
	(* Parameters for registration mark *)	
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.ProductLength := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.ProductLength := 305.0; (* Product length setpoint (user setting) from registration mark TO registration mark *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkPosition := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkPosition := 0; (* Position setpoint OF the registration mark in the product interval based on the cut position *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := 0; (* Immediate position setpoint correction OF the registration mark in the product interval based on the cut position *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := 700; (* Distance between cut position and registration mark sensor *)
	
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.Mode := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.Mode := mcFIRST_RM; (* This operating mode can be used to define how the first registration mark is detected *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.EventSourceParID := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.EventSourceParID := ACP10PAR_STAT_TRIGGER2; (* ACOPOS ParID as an event source *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.Edge := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.Edge := mcP_EDGE; (* Edge selection for the registration mark signal from the sensor *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MinWidth := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MinWidth := 0.5; (* Minimum width of a valid registration mark *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MaxWidth := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MaxWidth := 4; (* Maximum width of a valid registration mark *) 
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := 20; (* Window before the expected position *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := 20; (* Window after the expected position *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.SensorDelay := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.SensorDelay := 450; (* Registration mark sensor delay [�s] *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectCurrentCycle := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectCurrentCycle := FALSE; (* Applies the correction in the current movement cycle (e.g. via MC_Phasing and notMC_BR_CrossCutterControl) *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitNeg := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitNeg := 30; (* Negative limit down to which the "CorrectionValue" is permitted to be modified *)
	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitPos := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitPos := 30; (* sitive limit up to which "CorrectionValue" is permitted to be modified *)

   (* SETTINGS FOR PUNCH *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.Velocity       := 10000; (* velocity for movement *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.Acceleration   := 50000; (* acceleration for movement *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.Deceleration   := 50000; (* deceleration for movement *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.JogVelocity    := 4000;  (* velocity for jogging *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.HomePosition	  := 18000; (* Position after homing is done *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.HomeMode       := mcHOME_ABS_SWITCH;; // was mcHOME_ABS_SWITCH; (* Homing mode *)
   (* Parameters for crosscutter *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.CutRangeMaster := 15.7; (* was 12.7 Cutting area of master axis within which the cross cutter ("slave") should run synchronously to the "master" *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.CutRangeSlave  := 3000; // 2599; (* Cutting area of slave axis within which the cross cutter ("slave") should run synchronously to the "master"  *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.ProductLength  := 305.0; (* Distance between two cuts (product cycle) *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.ProductLengthCorrection    := 0; (* This value is added to the "ProductLength" input before "ProductLength" is transferred to the drive. This makes it possible to correct a cut using a value determined from a higher-level registration mark control process. *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.StartMode := mcMASTER_POSITION; (* Mode for the initial movement after the function block is enabled *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.FirstCutPosition := 13000; (* Position within the master axis period or absolute position for a non-periodic master axis where the first cut should take place  *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.MasterStartDistance := Slave[SLAVE_PUNCH_INDEX].Parameter.ProductLength; (* Distance of the master axis in which the cross cutter should be synchronized to the first cut position *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.StartInterval := 0; (* Interval for generating a starting event (a multiple of "StartInterval" is added to "FirstCutPosition" so that the movement can be started in synchronization) *)
   (* Parameters for registration mark *)	
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.ProductLength := 305.0; (* Product length setpoint (user setting) from registration mark TO registration mark *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.RegMarkPosition := 0; (* Position setpoint OF the registration mark in the product interval based on the cut position *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := 0; (* Immediate position setpoint correction OF the registration mark in the product interval based on the cut position *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := 300; (* Distance between cut position and registration mark sensor *)	
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.Mode := mcFIRST_RM; (* This operating mode can be used to define how the first registration mark is detected *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.EventSourceParID := ACP10PAR_STAT_TRIGGER2; (* ACOPOS ParID as an event source *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.Edge := mcP_EDGE; (* Edge selection for the registration mark signal from the sensor *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.MinWidth := 0.5; (* Minimum width of a valid registration mark *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.MaxWidth := 4; (* Maximum width of a valid registration mark *) 
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := 20; (* Window before the expected position *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := 20; (* Window after the expected position *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.SensorDelay := 50; (* Registration mark sensor delay [�s] *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.CorrectCurrentCycle := FALSE; (* Applies the correction in the current movement cycle (e.g. via MC_Phasing and notMC_BR_CrossCutterControl) *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitNeg := 30; (* Negative limit down to which the "CorrectionValue" is permitted to be modified *)
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitPos := 30; (* sitive limit up to which "CorrectionValue" is permitted to be modified *)
   
   
   GlobalError; (* Call to get access in watch window *) 
	
END_PROGRAM



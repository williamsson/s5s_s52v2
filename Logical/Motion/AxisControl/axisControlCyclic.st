(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: AxisControl
 * File: axisControlCyclic.st
 * Author: Magnus Jeppsson
 * Created: February 02, 2017
 ********************************************************************
 * Implementation OF PROGRAM AxisControl
 *******************************************************************)


PROGRAM _CYCLIC
   
   
   (*IF startAcpBasic = FALSE THEN
      RETURN;
   END_IF
   
   
   IF node[23].station[0] = TRUE THEN
      SlaveRef[SLAVE_BUF_OUT_INDEX] := ADR(gABufOut); //3-phase drive
   ELSE 
      SlaveRef[SLAVE_BUF_OUT_INDEX] := ADR(gAxBufOut); //1-phase drive
   END_IF *)
   
   Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := cutter.setting.regMarkOffset;	   
   Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := cutter.setting.regMarkOffset;	   
   Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := cutter.setting.regMarkOffset;	   

   (* E-stop *)
   GlobalCommand.Output.Command.E_Stop := GlobalCommand.Input.Command.E_Stop;
	
	IF(Slave[SLAVE_MAININFEED_INDEX].AxisState.ErrorStop) THEN
      GlobalCommand.Input.Command.Power := FALSE;
      globalPower := FALSE;
	END_IF
	
   (************CHECK IF POWER SHOULD BE OFF ***************************)
   IF (GlobalCommand.Input.Command.Power = FALSE) THEN
      GlobalCommand.Output.Command.Home := FALSE;
      GlobalCommand.Output.Command.ConnectSlavesToMaster := FALSE;
      GlobalCommand.Output.Command.DisconnectSlavesFromMaster := FALSE;
      GlobalCommand.Output.Command.MoveVelocity := FALSE;
      GlobalCommand.Output.Command.EnableCutMain := FALSE;
      GlobalCommand.Output.Command.EnableCutSecondary := FALSE;
      GlobalCommand.Output.Command.EnableCutPunch := FALSE;
      GlobalCommand.Output.Command.InitDataMain := FALSE;
      GlobalCommand.Output.Command.InitDataSecondary := FALSE;
      GlobalCommand.Output.Command.InitDataPunch := FALSE;
      GlobalCommand.Output.Command.EnableRegMarkControlMain := FALSE;
      GlobalCommand.Output.Command.EnableRegMarkControlSecondary := FALSE;
      GlobalCommand.Output.Command.EnableRegMarkControlPunch := FALSE;
      GlobalCommand.Output.Command.E_Stop := FALSE;

      GlobalCommand.Input.Command.Home := FALSE;
      GlobalCommand.Input.Command.ConnectSlavesToMaster := FALSE;
      GlobalCommand.Input.Command.DisconnectSlavesFromMaster := FALSE;
      GlobalCommand.Input.Command.MoveVelocity := FALSE;
      GlobalCommand.Input.Command.EnableCutMain := FALSE;
      GlobalCommand.Input.Command.EnableCutSecondary := FALSE;
      GlobalCommand.Input.Command.InitDataMain := FALSE;
      GlobalCommand.Input.Command.InitDataSecondary := FALSE;
      GlobalCommand.Output.Command.EnableRegMarkControlMain := FALSE;
      GlobalCommand.Output.Command.EnableRegMarkControlSecondary := FALSE;
      GlobalCommand.Output.Command.EnableRegMarkControlPunch := FALSE;
      GlobalCommand.Input.Command.E_Stop := FALSE;
      GlobalCommand.Input.Command.StopMovement := FALSE;

      GlobalCommandStep := STATE_WAIT;
   ELSIF (GlobalCommand.Input.Command.E_Stop = TRUE) THEN
      GlobalCommand.Output.Command.Home := FALSE;
      GlobalCommand.Output.Command.ConnectSlavesToMaster := FALSE;
      GlobalCommand.Output.Command.DisconnectSlavesFromMaster := FALSE;
      GlobalCommand.Output.Command.MoveVelocity := FALSE;
      GlobalCommand.Output.Command.EnableCutMain := FALSE;
      GlobalCommand.Output.Command.EnableCutSecondary := FALSE;
      GlobalCommand.Output.Command.EnableCutPunch := FALSE;
      GlobalCommand.Output.Command.InitDataMain := FALSE;
      GlobalCommand.Output.Command.InitDataSecondary := FALSE;
      GlobalCommand.Output.Command.InitDataPunch := FALSE;

      GlobalCommand.Input.Command.Home := FALSE;
      GlobalCommand.Input.Command.ConnectSlavesToMaster := FALSE;
      GlobalCommand.Input.Command.DisconnectSlavesFromMaster := FALSE;
      GlobalCommand.Input.Command.MoveVelocity := FALSE;
      GlobalCommand.Input.Command.EnableCutMain := FALSE;
      GlobalCommand.Input.Command.EnableCutSecondary := FALSE;
      GlobalCommand.Input.Command.InitDataMain := FALSE;
      GlobalCommand.Input.Command.InitDataSecondary := FALSE;
		GlobalCommand.Input.Command.InitDataPunch := FALSE;
		GlobalCommand.Input.Command.StopMovement := FALSE;

      GlobalCommandStep := STATE_READY;
   END_IF
	
   (* Forward command to axes *)
   GlobalCommand.Output.Command.InitDataMain := GlobalCommand.Input.Command.InitDataMain;
   GlobalCommand.Output.Command.InitDataSecondary := GlobalCommand.Input.Command.InitDataSecondary;
   GlobalCommand.Output.Command.InitDataPunch := GlobalCommand.Input.Command.InitDataPunch;
   GlobalCommand.Output.Command.EnableCutMain := GlobalCommand.Input.Command.EnableCutMain;
   GlobalCommand.Output.Command.EnableCutSecondary := GlobalCommand.Input.Command.EnableCutSecondary;
   GlobalCommand.Output.Command.EnableCutPunch := GlobalCommand.Input.Command.EnableCutPunch;
   GlobalCommand.Output.Command.EnableRegMarkControlMain := GlobalCommand.Input.Command.EnableRegMarkControlMain;
   GlobalCommand.Output.Command.EnableRegMarkControlSecondary := GlobalCommand.Input.Command.EnableRegMarkControlSecondary;
   GlobalCommand.Output.Command.EnableRegMarkControlPunch := GlobalCommand.Input.Command.EnableRegMarkControlPunch;
   GlobalCommand.Output.Command.ErrorAcknowledge := GlobalCommand.Input.Command.ErrorAcknowledge;

   Master[MASTER_AXIS_INDEX].Parameter.Velocity := GlobalCommand.Input.Parameter.Velocity;
	
   (* Read ststus from all axes and copy to global status *)
   GlobalCommand.Output.Status.AllDrivesEnable := PowerSupply[POWER_SUPPLY_INDEX].Status.DriveStatus.DriveEnable AND Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.DriveEnable AND Slave[SLAVE_MAINKNIFE_INDEX].Status.DriveStatus.DriveEnable AND Slave[SLAVE_SUPPORT_INDEX].Status.DriveStatus.DriveEnable AND Slave[SLAVE_REJECT_INDEX].Status.DriveStatus.DriveEnable AND (Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.DriveEnable OR NOT lineController.setup.merger.installed) AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.DriveStatus.DriveEnable OR NOT lineController.setup.cutter.secondKnife)  AND (Slave[SLAVE_PUNCH_INDEX].Status.DriveStatus.DriveEnable OR NOT lineController.setup.merger.punchUnitInstalled);

   GlobalCommand.Output.Status.AllControllersReady := PowerSupply[POWER_SUPPLY_INDEX].Status.DriveStatus.ControllerReady AND Master[0].Status.DriveStatus.ControllerReady AND Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.ControllerReady AND Slave[SLAVE_MAINKNIFE_INDEX].Status.DriveStatus.ControllerReady AND Slave[SLAVE_SUPPORT_INDEX].Status.DriveStatus.ControllerReady AND Slave[SLAVE_REJECT_INDEX].Status.DriveStatus.ControllerReady AND (Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.ControllerReady OR NOT lineController.setup.merger.installed) AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.DriveStatus.ControllerReady OR NOT lineController.setup.cutter.secondKnife) AND (Slave[SLAVE_PUNCH_INDEX].Status.DriveStatus.ControllerReady OR NOT lineController.setup.merger.punchUnitInstalled);

   GlobalCommand.Output.Status.AllControllersStatus := PowerSupply[POWER_SUPPLY_INDEX].Status.DriveStatus.ControllerStatus AND Master[0].Status.DriveStatus.ControllerStatus AND Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.ControllerStatus AND Slave[SLAVE_MAINKNIFE_INDEX].Status.DriveStatus.ControllerStatus AND Slave[SLAVE_SUPPORT_INDEX].Status.DriveStatus.ControllerStatus AND Slave[SLAVE_REJECT_INDEX].Status.DriveStatus.ControllerStatus AND (Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.ControllerStatus OR NOT lineController.setup.merger.installed) AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.DriveStatus.ControllerStatus OR NOT lineController.setup.cutter.secondKnife) AND (Slave[SLAVE_PUNCH_INDEX].Status.DriveStatus.ControllerStatus OR NOT lineController.setup.merger.punchUnitInstalled);

   GlobalCommand.Output.Status.AllHomingsOk := Master[0].Status.DriveStatus.HomingOk AND Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.HomingOk AND Slave[SLAVE_MAINKNIFE_INDEX].Status.DriveStatus.HomingOk AND Slave[SLAVE_SUPPORT_INDEX].Status.DriveStatus.HomingOk AND Slave[SLAVE_REJECT_INDEX].Status.DriveStatus.HomingOk AND (Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.HomingOk OR NOT lineController.setup.merger.installed) AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.DriveStatus.HomingOk OR NOT lineController.setup.cutter.secondKnife) AND (Slave[SLAVE_PUNCH_INDEX].Status.DriveStatus.HomingOk OR NOT lineController.setup.merger.punchUnitInstalled);
	
   //GlobalCommand.Output.Status.AllSlavesSynchronizedMotion := Slave[SLAVE_MAININFEED_INDEX].AxisState.SynchronizedMotion AND Slave[SLAVE_MAINKNIFE_INDEX].AxisState.SynchronizedMotion AND Slave[SLAVE_SUPPORT_INDEX].AxisState.SynchronizedMotion AND Slave[SLAVE_REJECT_INDEX].AxisState.SynchronizedMotion AND Slave[SLAVE_MERGE_INFEED_INDEX].AxisState.SynchronizedMotion AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].AxisState.SynchronizedMotion OR NOT lineController.setup.cutter.secondKnife) AND (Slave[SLAVE_PUNCH_INDEX].AxisState.SynchronizedMotion OR NOT lineController.setup.merger.punchUnitInstalled);

   //GlobalCommand.Output.Status.AllSlavesNotSynchronizedMotion := (NOT Slave[SLAVE_MAININFEED_INDEX].AxisState.SynchronizedMotion) AND (NOT Slave[SLAVE_MAINKNIFE_INDEX].AxisState.SynchronizedMotion) AND (NOT Slave[SLAVE_SUPPORT_INDEX].AxisState.SynchronizedMotion) AND (NOT Slave[SLAVE_REJECT_INDEX].AxisState.SynchronizedMotion) AND (NOT Slave[SLAVE_MERGE_INFEED_INDEX].AxisState.SynchronizedMotion AND (NOT (Slave[SLAVE_SECONDARYKNIFE_INDEX].AxisState.SynchronizedMotion AND lineController.setup.cutter.secondKnife)) AND (NOT (Slave[SLAVE_PUNCH_INDEX].AxisState.SynchronizedMotion AND lineController.setup.merger.punchUnitInstalled)));
	
   GlobalCommand.Output.Status.AllSlavesSynchronizedMotion := Slave[SLAVE_MAININFEED_INDEX].AxisState.SynchronizedMotion AND Slave[SLAVE_MAINKNIFE_INDEX].AxisState.SynchronizedMotion AND Slave[SLAVE_SUPPORT_INDEX].AxisState.SynchronizedMotion AND (Slave[SLAVE_MERGE_INFEED_INDEX].AxisState.SynchronizedMotion OR NOT lineController.setup.merger.installed) AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].AxisState.SynchronizedMotion OR NOT lineController.setup.cutter.secondKnife) AND (Slave[SLAVE_PUNCH_INDEX].AxisState.SynchronizedMotion OR NOT lineController.setup.merger.punchUnitInstalled);

   GlobalCommand.Output.Status.AllSlavesNotSynchronizedMotion := (NOT Slave[SLAVE_MAININFEED_INDEX].AxisState.SynchronizedMotion) AND (NOT Slave[SLAVE_MAINKNIFE_INDEX].AxisState.SynchronizedMotion) AND (NOT Slave[SLAVE_SUPPORT_INDEX].AxisState.SynchronizedMotion) AND (NOT (Slave[SLAVE_MERGE_INFEED_INDEX].AxisState.SynchronizedMotion AND lineController.setup.merger.installed)) AND (NOT (Slave[SLAVE_SECONDARYKNIFE_INDEX].AxisState.SynchronizedMotion AND lineController.setup.cutter.secondKnife)) AND (NOT (Slave[SLAVE_PUNCH_INDEX].AxisState.SynchronizedMotion AND lineController.setup.merger.punchUnitInstalled));
	
	
   CASE GlobalCommandStep OF
      (***************************** WAIT *******************************)
      STATE_WAIT: (* STATE: Wait *)
         IF ((GlobalCommand.Input.Command.Power = TRUE)) THEN
            GlobalCommand.Output.Command.Power := TRUE;
            GlobalCommandStep := STATE_POWER_ON;
         ELSE
            GlobalCommand.Output.Command.Power := FALSE;
         END_IF

         (* End of STATE_WAIT *)

         (****************************** POWER ON ****************************)
      STATE_POWER_ON: (* STATE: Power on *)
         IF (GlobalCommand.Output.Status.AllControllersStatus = TRUE) THEN
            GlobalCommandStep := STATE_READY;
         END_IF

         (* End of STATE_POWER_ON *)

         (****************************** READY *******************************)
      STATE_READY: (* STATE: Waiting for Commands *)
         IF (GlobalCommand.Input.Command.Home = TRUE) THEN
            GlobalCommand.Output.Command.Home := TRUE;
            GlobalCommandStep := STATE_HOME;
         ELSIF (GlobalCommand.Input.Command.ConnectSlavesToMaster = TRUE) THEN
            GlobalCommand.Output.Command.ConnectSlavesToMaster := TRUE;
            GlobalCommandStep := STATE_CONNECT_SLAVES;
         END_IF

         (* End of STATE_READY *)

         (****************************** HOME ********************************)
      STATE_HOME: (* STATE: start homing process *)
         GlobalCommand.Output.Command.Home := FALSE;
         IF (GlobalCommand.Output.Status.AllHomingsOk) THEN 
            GlobalCommandStep := STATE_HOME_START_RESET;
            startMove := 0;
         END_IF
						
      STATE_HOME_START_RESET: (* STATE: Start to move main knife to it's zero positions *)
			Slave[SLAVE_PUNCH_INDEX].Parameter.ProductLength := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.ProductLength := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.ProductLength := cutter.setting.format * 305 / 307;
			Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.ProductLength := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.ProductLength := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.ProductLength := cutter.setting.format;
			//IF (cutter.setting.format < 355.6) THEN
			//	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := cutter.setting.format / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
			//ELSE	
			//	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := 355.6 / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
			//END_IF
            IF (cutter.setting.format < 254.0) THEN
            	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := cutter.setting.format / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
            ELSE	
            	Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := 254.0 / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
            END_IF
						
			//Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.StartMode := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.StartMode := mcIMMEDIATE; (* Mode for the initial movement after the function block is enabled *)
         //Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.StartInterval := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.StartInterval := 1;
         //Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := format / 2; //305 + 452 + 133 + cutter.setting.stripSize;
         //Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.FirstCutPosition := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.FirstCutPosition := 0; // was 305

			Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.StartMode := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.StartMode := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.StartMode := mcIMMEDIATE; //mcMASTER_POSITION; (* Mode for the initial movement after the function block is enabled *)
			Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.StartInterval := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.StartInterval := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.StartInterval := 1;
         IF(cutter.setting.stripCut) THEN
            Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := 500 + cutter.setting.stripSize;
         ELSE
            Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := 500;
         END_IF
         Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := 500 - cutter.setting.knifeDiff;
         Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.MasterStartDistance := 500 + cutter.setting.stripSize / 2.0;
			Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.FirstCutPosition := 0; // was 305
			Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.FirstCutPosition := 0;
         Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.FirstCutPosition := 0;
			
         rejectLoadPos := 0.0;

         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.SensorDelay := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.SensorDelay := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.SensorDelay := 20; (* was 20 Registration mark sensor delay [�s] *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.RegMarkPosition := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkPosition := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkPosition := 0; (* Position setpoint OF the registration mark in the product interval based on the cut position *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.RegMarkOffset := cutter.setting.regMarkOffset; (* Immediate position setpoint correction OF the registration mark in the product interval based on the cut position *)
         IF(cutter.setting.stripCut) THEN
            Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := cutter.setting.sensorKnifeDist + cutter.setting.stripSize; // was 888.5 (* Distance between cut position and registration mark sensor *)	
			ELSE
            Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := cutter.setting.sensorKnifeDist; // was 888.5 (* Distance between cut position and registration mark sensor *)	
         END_IF
         Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := cutter.setting.sensorKnifeDist - cutter.setting.knifeDiff; // was 888.5 (* Distance between cut position and registration mark sensor *)	
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := merger.setting.sensorPunchDist + cutter.setting.stripSize / 2.0; // was 888.5 (* Distance between cut position and registration mark sensor *)	
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.Mode := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.Mode := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.Mode := mcFIRST_RM; (* This operating mode can be used to define how the first registration mark is detected *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.MinWidth := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MinWidth := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MinWidth := cutter.setting.regMarkMinWidth; (* Minimum width of a valid registration mark *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.MaxWidth := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MaxWidth := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.MaxWidth := cutter.setting.regMarkMaxWidth; (* Maximum width of a valid registration mark *) 
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowNeg := 20; (* Window before the expected position *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.WindowPos := 20; (* Window after the expected position *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.CorrectCurrentCycle := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectCurrentCycle := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectCurrentCycle := FALSE; (* Applies the correction in the current movement cycle (e.g. via MC_Phasing and notMC_BR_CrossCutterControl) *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitNeg := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitNeg := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitNeg := 0; //30; (* Negative limit down to which the "CorrectionValue" is permitted to be modified *)
         Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitPos := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitPos := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkAdvancedParameter.CorrectionValueLimitPos := 0; //format / 2; (* Positive limit up to which "CorrectionValue" is permitted to be modified *)
                 
         Slave[SLAVE_MAINKNIFE_INDEX].Parameter.Position := 32000 + (cutter.setting.mainKnifeHomeOffset * 100.0); // 180 degrees from cutposition
			Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.Position := 14750;
         Slave[SLAVE_PUNCH_INDEX].Parameter.Position := 2200;//32000;

         Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.CutPosition := (REAL_TO_UDINT(Slave[SLAVE_MAINKNIFE_INDEX].Parameter.Position) + 18000) MOD 36000; // 14000;
         Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.CutPosition := (REAL_TO_UDINT(Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.Position) + 18000) MOD 36000; // 32750;
         Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.CutPosition := (REAL_TO_UDINT(Slave[SLAVE_PUNCH_INDEX].Parameter.Position) + 18000) MOD 36000; // 14000;
         //Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.CutPosition := 14000;
         //Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.AdvancedParameter.CutPosition := 32750;
         //Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.CutPosition := 14000;

         IF startMove = 0 THEN
            Slave[SLAVE_MAINKNIFE_INDEX].Command.MoveAbsolute := TRUE;
            Slave[SLAVE_SECONDARYKNIFE_INDEX].Command.MoveAbsolute := TRUE;
            Slave[SLAVE_PUNCH_INDEX].Command.MoveAbsolute := TRUE;
         END_IF
         startMove := startMove + 1;
         //IF (Slave[SLAVE_MAINKNIFE_INDEX].AxisState.DiscreteMotion = TRUE AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].AxisState.DiscreteMotion OR NOT lineController.setup.cutter.secondKnife) = TRUE AND (Slave[SLAVE_PUNCH_INDEX].AxisState.DiscreteMotion OR NOT lineController.setup.merger.punchUnitInstalled) = TRUE) THEN
         IF startMove >= 400 THEN 
            GlobalCommandStep := STATE_HOME_ZERO;
         END_IF
			
      STATE_HOME_ZERO: (* STATE: Wait for cutters to stop in their zero positions *)
         IF (Slave[SLAVE_MAINKNIFE_INDEX].AxisState.StandStill = TRUE AND (Slave[SLAVE_SECONDARYKNIFE_INDEX].AxisState.StandStill OR NOT lineController.setup.cutter.secondKnife) = TRUE AND (Slave[SLAVE_PUNCH_INDEX].AxisState.StandStill OR NOT lineController.setup.merger.punchUnitInstalled) = TRUE) THEN
            GlobalCommand.Input.Command.Home := FALSE;
            GlobalCommandStep := STATE_READY;
         END_IF

         (* End of STATE_HOME *)


         (****************************** CONNECT_SLAVES ********************************)
      STATE_CONNECT_SLAVES: (* STATE: connect all slaves to master *)
         IF (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion = TRUE) THEN
            GlobalCommand.Output.Command.ConnectSlavesToMaster := FALSE;
            GlobalCommand.Input.Command.ConnectSlavesToMaster := FALSE;
            GlobalCommandStep := STATE_SYNC_STANDSTILL;
         END_IF

         (* End of STATE_CONNECT_SLAVES *)
		
         (****************************** STATE_SYNCRONIZED ********************************)
      STATE_SYNC_STANDSTILL: (* STATE: *)
         IF (GlobalCommand.Input.Command.MoveVelocity = TRUE) THEN
            GlobalCommand.Output.Command.MoveVelocity := TRUE;
            GlobalCommandStep := STATE_SYNC_MOVE_VEL_SPEEDUP;
         ELSIF (GlobalCommand.Input.Command.DisconnectSlavesFromMaster = TRUE) THEN
            GlobalCommand.Output.Command.DisconnectSlavesFromMaster := TRUE;
            GlobalCommandStep := STATE_DISCONNECT_SLAVES;
         END_IF

      STATE_SYNC_MOVE_VEL_SPEEDUP: (* STATE: *)
         IF ((GlobalCommand.Output.Command.MoveVelocity = FALSE) AND (Master[MASTER_AXIS_INDEX].AxisState.ContinuousMotion = TRUE)) THEN
            GlobalCommand.Input.Command.MoveVelocity := FALSE;
            GlobalCommandStep := STATE_SYNC_MOVE_VEL_CONST;
         END_IF
         
      STATE_SYNC_MOVE_VEL_CONST: (* STATE: *)
         IF (GlobalCommand.Input.Command.StopMovement = TRUE) THEN
            Master[MASTER_AXIS_INDEX].Command.Stop := TRUE;
            GlobalCommandStep := STATE_SYNC_MOVE_STOP;
         //ELSIF (GlobalCommand.Input.Parameter.Velocity <> oldVelocity) THEN
         ELSE
            //IF(NOT stopUpdateSpeed) THEN
               GlobalCommand.Output.Command.MoveVelocity := TRUE;
               GlobalCommandStep := STATE_SYNC_MOVE_VEL_SPEEDUP;
               oldVelocity := GlobalCommand.Input.Parameter.Velocity;
            //END_IF
         END_IF

      STATE_SYNC_MOVE_STOP: (* STATE: *)
         Master[MASTER_AXIS_INDEX].Command.Stop := FALSE;
         IF (Master[MASTER_AXIS_INDEX].AxisState.StandStill = TRUE) THEN
            GlobalCommand.Input.Command.StopMovement := FALSE;
            GlobalCommandStep := STATE_SYNC_STANDSTILL;
         END_IF

         (* End of STATE_SYNCRONIZED *)

         (****************************** DISCONNECT_SLAVES ********************************)
      STATE_DISCONNECT_SLAVES: (* STATE: disconnect all slaves to master *)
         IF (GlobalCommand.Output.Status.AllSlavesNotSynchronizedMotion = TRUE) THEN
            GlobalCommand.Output.Command.DisconnectSlavesFromMaster := FALSE;
            GlobalCommand.Input.Command.DisconnectSlavesFromMaster := FALSE;
            GlobalCommandStep := STATE_READY;
         END_IF

      (* End of STATE_CONNECT_SLAVES *)		
      (*********************** SEQUENCE END *******************************)
   END_CASE

(*********************************************************************
        FUNCTION Block Calls
*********************************************************************)
	
   oldOutput := GlobalCommand.Output;
	 
END_PROGRAM


TYPE
	physical_ebmGlue_outputs : 	STRUCT 
		out1 : BOOL;
		out2 : BOOL;
		out3 : BOOL;
		out4 : BOOL;
		out5 : BOOL;
		out6 : BOOL;
		out7 : BOOL;
		out8 : BOOL;
		out9 : BOOL;
		out10 : BOOL;
		out11 : BOOL;
		out12 : BOOL;
		out13 : BOOL;
		out14 : BOOL;
		out15 : BOOL;
		out16 : BOOL;
		analogOut : REAL;
	END_STRUCT;
	physical_ebmGlue_inputs : 	STRUCT 
		encoder : INT;
		in1 : BOOL;
		in2 : BOOL;
		in3 : BOOL;
		in4 : BOOL;
		in5 : BOOL;
		in6 : BOOL;
		in7 : BOOL;
		in8 : BOOL;
		in9 : BOOL;
		in10 : BOOL;
		in11 : BOOL;
		in12 : BOOL;
		in13 : BOOL;
		in14 : BOOL;
		in15 : BOOL;
		in16 : BOOL;
		in17 : BOOL;
		in18 : BOOL;
		in19 : BOOL;
		in20 : BOOL;
		in21 : BOOL;
		in22 : BOOL;
		in23 : BOOL;
		in24 : BOOL;
		in25 : BOOL;
		in26 : BOOL;
		in27 : BOOL;
		in28 : BOOL;
		analogIn : REAL;
	END_STRUCT;
	ebmGlue_typ : 	STRUCT 
		reading : ebmGlue_readings_typ;
		setting : ebmGlue_settings_typ;
		command : ebmGlue_ext_command_typ;
	END_STRUCT;
	ebmGlue_settings_typ : 	STRUCT 
		forceOutputEnable : physical_ebmGlue_outputs;
		forceOutputValue : physical_ebmGlue_outputs;
		offset : glueGunSettings_typ;
		aimLight3 : BOOL;
		aimLight4 : BOOL;
		aimLight2 : BOOL;
		aimLight1 : BOOL;
		glueGun : glueGunSettings_typ;
		testMode : DINT := 0; (*pulses per mm //5.0*)
		scale : REAL := 14.46; (*pulses per mm //5.0*)
	END_STRUCT;
	enabledGlueGuns_typ : 	STRUCT 
		glue1 : BOOL;
		glue2 : BOOL;
		glue3 : BOOL;
		glue4 : BOOL;
		glue5 : BOOL;
		glue6 : BOOL;
		glue7 : BOOL;
		glue8 : BOOL;
	END_STRUCT;
	ebmGlue_readings_typ : 	STRUCT 
		ip : STRING[16];
		connected : BOOL := FALSE;
		offset : glueGunReadings_typ;
		glueGun : glueGunReadings_typ;
		pulseSpeed : REAL; (*Hz*)
		offsetDistance : REAL; (*mm*)
		markSize : REAL; (*mm*)
		sMsg : UDINT;
		rMsg : UDINT;
		input : physical_ebmGlue_inputs; (*current state of input*)
		output : physical_ebmGlue_outputs; (*current state of output*)
		sendAllCnt : UDINT := 0;
		status : INT;
	END_STRUCT;
	ebmGlue_ext_command_typ : 	STRUCT 
		glueGun : glueGunCommand_typ;
		offset : glueGunCommand_typ;
		readAll : BOOL;
		sendAll : BOOL;
		resetCnt : BOOL;
	END_STRUCT;
	glueGunCommand_typ : 	STRUCT 
		start : BOOL := TRUE;
		stop : BOOL := FALSE;
		clearQueue : BOOL := FALSE;
	END_STRUCT;
	glueGunReadings_typ : 	STRUCT 
		active : BOOL := FALSE; (*#*)
		inQueue : UDINT;
		dotCount : DINT := 0; (*#*)
	END_STRUCT;
	glueGunSettings_typ : 	STRUCT 
		nozzleDelay : DINT := 15; (*ms*)
		size : REAL := 5; (*ms*)
		separation : REAL := 2.5; (*mm*)
		markMode : REAL := 0; (*not used*)
		maxMarkSize : REAL := 5.0; (*mm*)
		minMarkSize : REAL := 2.0; (*mm*)
		offset : REAL := 20; (*mm*)
		enable : BOOL := TRUE;
		signalLevel : BOOL := FALSE; (*Signal level from mark sensor when reading a mark, 0 = inactive, 1 = active*)
		pattern : DINT := 1; (*0 = disable*)
	END_STRUCT;
END_TYPE

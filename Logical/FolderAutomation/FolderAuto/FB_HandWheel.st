
(* Make absolute encoder from angular sensor *)
FUNCTION_BLOCK FB_HandWheel
   IF init THEN
      rotCount := 0;
      prevPos := newPos := inputSignal;
      countValue := INT_TO_REAL(prevPos);
      init := FALSE;
   ELSE
      tempPos := inputSignal;
      IF tempPos<0 THEN
         tempPos := 0;
      END_IF;
   
      IF ABS(tempPos-prevPos)>DEAD_BAND THEN  // if not, no move at all
         newPos := tempPos;
       IF prevPos<MAX_POS/4 THEN	// started on lower quarter
          IF newPos>MAX_POS*3/4 THEN // roll over backwards
             rotCount := rotCount-1;
           END_IF;
         ELSIF prevPos>MAX_POS*3/4 THEN	// started on top quarter
          IF newPos<MAX_POS/4 THEN	// roll over forwards
             rotCount := rotCount+1;
          END_IF;
       END_IF;
       prevPos := newPos;
     END_IF;
      countValue := DINT_TO_REAL(rotCount)*MAX_POS+INT_TO_REAL(newPos);
   END_IF;
END_FUNCTION_BLOCK

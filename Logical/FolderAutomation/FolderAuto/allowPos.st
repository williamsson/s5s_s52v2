
(* Returns allowed position to move to *)
FUNCTION allowPos
   IF wantPos<lowLim THEN
      allowPos := lowLim;
      errCode := LEFT_EDGE_FAIL;
   ELSIF wantPos>highLim THEN
      allowPos := highLim;
      errCode := RIGHT_EDGE_FAIL;
   ELSE
      allowPos := wantPos;
      errCode := 0;
   END_IF;
END_FUNCTION

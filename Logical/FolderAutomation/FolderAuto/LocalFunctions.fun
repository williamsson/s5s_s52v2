
{REDUND_ERROR} {REDUND_UNREPLICABLE} FUNCTION_BLOCK FB_HandWheel (*Make absolute encoder from angular sensor*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		inputSignal : {REDUND_UNREPLICABLE} INT;
		init : {REDUND_UNREPLICABLE} BOOL;
	END_VAR
	VAR_OUTPUT
		countValue : {REDUND_UNREPLICABLE} REAL;
	END_VAR
	VAR
		prevPos : {REDUND_UNREPLICABLE} INT;
		newPos : {REDUND_UNREPLICABLE} INT;
		tempPos : {REDUND_UNREPLICABLE} INT;
	END_VAR
	VAR CONSTANT
		DEAD_BAND : INT := 15;
		MAX_POS : INT := 32767;
	END_VAR
	VAR
		rotCount : {REDUND_UNREPLICABLE} DINT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION allowPos : REAL (*Returns allowed position to move to*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		wantPos : REAL;
		lowLim : REAL;
		highLim : REAL;
	END_VAR
	VAR_IN_OUT
		errCode : UINT; (*Returns error code if outside limits*)
	END_VAR
END_FUNCTION

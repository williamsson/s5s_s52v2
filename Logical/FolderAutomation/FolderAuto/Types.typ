
TYPE
	axis_position_typ : 	STRUCT 
		homePos : REAL; (*Offset from zero line when at home position*)
		offsetPos : REAL; (*Physical offset from actual position zero. Photo cell offset for perfs.*)
		limitLow : REAL; (*Software limit negative*)
		limitHigh : REAL; (*Software limit positive*)
		extraLimit : REAL; (*To be added to negative limit in special cases*)
		useExtra : BOOL; (*Flag: Use extraLimit*)
		currentPos : REAL; (*Actual position for axis. Not updated all the time*)
		gotoPos : REAL; (*Where to go when recipe/auto measure is applied*)
		posColor : UINT; (*Color for positon data*)
		adjust : ARRAY[0..3]OF BOOL; (*Adjust key press*)
	END_STRUCT;
	perf_typ : 	STRUCT 
		usePerfLeft : BOOL; (*Perf used for current format*)
		usePerfCenter : BOOL; (*Perf used for current format*)
		usePerfRight : BOOL; (*Perf used for current format*)
		applyPerf : BOOL; (*Applies perfs, if enabled*)
		enablePerf : BOOL; (*Enables perf action*)
		perfLColor : INT;
		perfRColor : INT;
		perfCColor : INT;
		buttonColor : INT; (*Color for perf enable button*)
	END_STRUCT;
	foldAuto_typ : 	STRUCT 
		edgeLeft : REAL; (*Physical paper edge*)
		edgeRight : REAL; (*Physical paper edge*)
		paperWidth : REAL; (*Physical paper width*)
		virtEdgeLeft : REAL; (*Virtual paper edge*)
		virtEdgeRight : REAL; (*Virtual paper edge*)
		virtPaperWidth : REAL; (*Virtual paper width*)
		paperFound : BOOL; (*Both edges found in last search*)
		autoButtonColor : INT;
		moveButtonColor : INT;
		perfCylinder : perf_typ;
	END_STRUCT;
	axisLink_typ : 	STRUCT 
		pointer : REFERENCE TO basic_typ; (*Points to axis structure for basic control functions*)
		rawPointer : UDINT; (*Points directly to axis structure*)
		installed : BOOL; (*Axis exists*)
		dispControl : UINT; (*Display control bit mask*)
		linkActive : BOOL; (*Axis is following handwheel*)
		buttonColor : INT; (*Handwheel link button color*)
		hasError : BOOL; (*Axis error found*)
		hasAbsolute : BOOL; (*Axis has absolute encoder*)
		safeID : INT; (*Safety relation to axis*)
		positions : axis_position_typ;
	END_STRUCT;
END_TYPE


TYPE
	foldType : 
		(
		BYPASS := 0,
		TWO_UP_1 := 1,
		TWO_UP_2 := 2,
		THREE_UP := 3,
		FOUR_UP := 4
		);
	cmdButton_typ : 	STRUCT 
		cmdButton_1 : BOOL; (*General button pressed signals*)
		cmdButton_2 : BOOL;
		cmdButton_3 : BOOL;
		cmdButton_4 : BOOL;
		cmdButton_5 : BOOL;
		cmdButton_6 : BOOL;
		cmdButton_7 : BOOL;
		cmdButton_8 : BOOL;
		cmdButton_11 : BOOL;
		cmdButton_12 : BOOL;
		cmdButton_13 : BOOL;
		cmdButton_9 : BOOL;
	END_STRUCT;
	offset_typ : 	STRUCT 
		plowH1HomePos : REAL; (*Home position offsets (from absolute position)*)
		plowV1HomePos : REAL; (*Home position offsets (from absolute position)*)
		plowH2HomePos : REAL; (*Home position offsets (from absolute position)*)
		plowV2HomePos : REAL; (*Home position offsets (from absolute position)*)
		plowPLHomePos : REAL; (*Home position offsets (from machine zero)*)
		plowPRHomePos : REAL; (*Home position offsets (from machine zero)*)
		plowH1Offset : REAL; (*Tool offset from machine zero*)
		plowV1Offset : REAL; (*Tool offset from machine zero*)
		plowH2Offset : REAL; (*Tool offset from machine zero*)
		plowV2Offset : REAL; (*Tool offset from machine zero*)
		plowPLOffset : REAL; (*Photo cell offset from host perf disc*)
		plowPROffset : REAL; (*Photo cell offset from host perf disc*)
		plowH1LimLow : REAL; (*Movement limits*)
		plowV1LimLow : REAL; (*Movement limits*)
		plowH2LimLow : REAL; (*Movement limits*)
		plowV2LimLow : REAL; (*Movement limits*)
		plowPLLimLow : REAL; (*Movement limits*)
		plowPRLimLow : REAL; (*Movement limits*)
		plowH1LimHigh : REAL; (*Movement limits*)
		plowV1LimHigh : REAL; (*Movement limits*)
		plowH2LimHigh : REAL; (*Movement limits*)
		plowV2LimHigh : REAL; (*Movement limits*)
		plowPLLimHigh : REAL; (*Movement limits*)
		plowPRLimHigh : REAL; (*Movement limits*)
		plowHead2VerExtra : REAL; (*Extra limit used only in certain folding modes, to avoid hitting paper*)
		plowH2Act : REAL; (*Actual position. Stored in config to allow return to manually set position.*)
		plowV2Act : REAL; (*Actual position. Stored in config to allow return to manually set position.*)
		plowH1Act : REAL; (*Actual position. Stored in config to allow return to manually set position.*)
		plowV1Act : REAL; (*Actual position. Stored in config to allow return to manually set position.*)
		plowPLAct : REAL; (*Actual position. Stored in config to allow return to manually set position.*)
		plowPRAct : REAL; (*Actual position. Stored in config to allow return to manually set position.*)
	END_STRUCT;
	folderAutomation_ext_command_typ : 	STRUCT 
		zeroPoint : BOOL; (*Move heads to zero position (manually)*)
		zeroAbs : BOOL; (*Set zero for absolute encoders*)
		localPower : BOOL; (*Motor power in folder*)
		load : BOOL; (*Power and homing*)
		autoSet : BOOL; (*Measure web*)
		moveNow : BOOL; (*Go to calculated position*)
		perfLift : BOOL; (*Release perforation*)
		perfLoad : BOOL; (*Apply perforation*)
		handwheelHorizontal1 : BOOL; (*Link to handwheel*)
		button4_2show : BOOL;
		button4_1show : BOOL;
		handwheelHorizontal2 : BOOL; (*Link to handwheel*)
		handwheelVertical1 : BOOL; (*Link to handwheel*)
		handwheelVertical2 : BOOL; (*Link to handwheel*)
		buttons : cmdButton_typ;
	END_STRUCT;
	folderAutomation_readings_typ : 	STRUCT 
		input : physical_folderAutomation_inputs; (*current state of input*)
		output : physical_folderAutomation_output; (*current state of output*)
		startUpSeq : UINT := 0; (*0 -> 15*)
		status : machineStatus_typ;
		page4_1show : INT; (*button control layout 1*)
		page4_2show : INT; (*button control layout 2*)
	END_STRUCT;
	folderAutomation_settings_typ : 	STRUCT 
		forceOutputEnable : physical_folderAutomation_output;
		forceOutputValue : physical_folderAutomation_output;
		forceAxis_dev : BOOL := FALSE;
		position : offset_typ;
		marginLeft : REAL; (*When folding with a tab*)
		marginRight : REAL; (*When folding with a tab*)
		usePerfLeft : BOOL; (*Perf active*)
		usePerfCenter : BOOL; (*Perf active*)
		usePerfRight : BOOL; (*Perf active*)
	END_STRUCT;
	folderAutomation_typ : 	STRUCT 
		command : folderAutomation_ext_command_typ;
		reading : folderAutomation_readings_typ;
		setting : folderAutomation_settings_typ;
	END_STRUCT;
	physical_folderAutomation_inputs : 	STRUCT 
		homePosLeftPerf : BOOL; (*trigger1*)
		homePosRightPerf : BOOL; (*trigger1*)
		homePosLeftEdge : BOOL; (*trigger2 *)
		homePosRightEdge : BOOL; (*trigger2*)
		perfCover : BOOL;
		analogEncoderWheel1 : INT; (*Rotary sensor*)
		analogEncoderWheel2 : INT; (*Rotary sensor*)
	END_STRUCT;
	physical_folderAutomation_output : 	STRUCT 
		valve1 : BOOL;
		valve2 : BOOL;
	END_STRUCT;
END_TYPE

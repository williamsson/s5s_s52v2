
TYPE
	physical_folder_outputs : 	STRUCT 
		beeper : BOOL; (*analog out*)
	END_STRUCT;
	physical_folder_inputs : 	STRUCT 
		webTensionInfeed : INT; (*analog*)
		webTensionInside : INT; (*analog*)
		webWatch2 : BOOL;
		webWatch3 : BOOL;
		webWatch1 : BOOL;
		eStopInfeed : BOOL;
		eStopOutfeed : BOOL;
		rightFrontDoor : BOOL;
		infeedSideDoor : BOOL;
		infeedTopCover : BOOL;
		outfeedTopCover : BOOL;
		feedAllButtonInfeed : BOOL;
		feedAllButton : BOOL;
	END_STRUCT;
	folder_typ : 	STRUCT 
		command : folder_ext_command_typ;
		reading : folder_readings_typ;
		setting : folder_settings_typ;
	END_STRUCT;
	folder_settings_typ : 	STRUCT 
		tensionIdealPerc : DINT := 30;
		tensionLowPos : DINT := 24000;
		tensionHighPos : DINT := 4000;
		tensionInIdealPerc : DINT := 30;
		tensionInLowPos : DINT := 24000;
		foldingType : DINT := 2; (*0 = bypass, 1 = two up first head, 2 = two up second head, 3 = three up, 4 = four up*)
		tensionInHighPos : DINT := 4000;
		forceOutputEnable : physical_folder_outputs;
		forceOutputValue : physical_folder_outputs;
	END_STRUCT;
	folder_readings_typ : 	STRUCT 
		maximumSpeed : REAL := 0.0; (*m/s*)
		feedOutfeed : BOOL := FALSE;
		feedInfeed : BOOL := FALSE;
		inManualLoad : BOOL := FALSE;
		runLocal : BOOL := FALSE;
		ready : BOOL;
		error : BOOL;
		coversClosed : BOOL;
		infeedButton1 : colorOfButton;
		infeedButton2 : colorOfButton;
		infeedButton3 : colorOfButton;
		infeedButton4 : colorOfButton;
		infeedButton5 : colorOfButton;
		outfeedButton1 : colorOfButton;
		outfeedButton2 : colorOfButton;
		outfeedButton3 : colorOfButton;
		outfeedButton4 : colorOfButton;
		outfeedButton5 : colorOfButton;
		input : physical_folder_inputs; (*current state of input*)
		output : physical_folder_outputs; (*current state of output*)
		btn3Func : USINT;
		btn2Func : USINT;
		startUpSeq : USINT;
		status : machineStatus_typ;
	END_STRUCT;
	folder_ext_command_typ : 	STRUCT 
		shortBeep : BOOL; (*deliver next (over min stack)*)
		beep : BOOL; (*deliver next (over min stack)*)
		pressOutfeedButton1 : BOOL;
		pressOutfeedButton2 : BOOL;
		pressOutfeedButton3 : BOOL;
		pressOutfeedButton4 : BOOL;
		pressOutfeedButton5 : BOOL;
		pressInfeedButton1 : BOOL;
		pressInfeedButton2 : BOOL;
		pressInfeedButton3 : BOOL;
		pressInfeedButton4 : BOOL;
		pressInfeedButton5 : BOOL;
	END_STRUCT;
END_TYPE

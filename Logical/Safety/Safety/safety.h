
union stackerToSafe_typ
{
	UINT all;
	struct 
	{
		unsigned typ:     6;
		unsigned lock:    1;
		unsigned lcReset: 1;
		unsigned fReset:  1;
		unsigned dummy:   7;
	}bits;
}stackerToSafe;


union stackerFromSafe_typ
{
	UINT all;
	struct 
	{
		unsigned machinePressent:      1;
		unsigned lock:    	    	   1;
		unsigned eStop:                1;
		unsigned eStopExt:             1;
		
		unsigned SafeTorqueOff:        1;
		unsigned lightCurtain:         1;
		unsigned SafeStop:             1;
		unsigned Ax1_1run:             1;           
		
		unsigned Ax1_2run:             1;
		unsigned Ax1_3run:             1;
		unsigned Ax2_1run:             1;
		unsigned Ax2_2run:             1;
		
		unsigned Ax2_3run:             1;
		unsigned Ax3_1run:             1;
		unsigned Ax3_2run:             1;
		unsigned Ax3_3run:             1;
	}bits;
}stackerFromSafe;

union stackerFromSafe2_typ
{
    UINT all;
    struct 
    {
        unsigned coverFault:           1;
        unsigned dummy:                15;
    }bits;
}stackerFromSafe2;

union bufferToSafe_typ
{
	UINT all;
	struct 
	{
		unsigned typ:     6;
		unsigned lock:    1;
		unsigned dummy:   9;
	}bits;
}bufferToSafe;

union bufferFromSafe_typ
{
	UINT all;
	struct 
	{
		unsigned machinePressent:      1;
		unsigned lock:    	    	   1;
		unsigned eStop:                1;
		unsigned nc3:                  1;
		
		unsigned SafeTorqueOff:        1;
		unsigned SlsFbStatus:          1;
		unsigned SafeLimitSpeed:       1;
		unsigned Ax1_1run:             1;           
		
		unsigned Ax1_2run:             1;
        unsigned coverFault:           1;
		unsigned dummy:                6;
		
	}bits;
}bufferFromSafe;

union mergerToSafe_typ
{
	UINT all;
	struct 
	{
		unsigned typ:     6;
		unsigned dummy:   10;
	}bits;
}mergerToSafe;

union mergerFromSafe_typ
{
	UINT all;
	struct 
	{
		unsigned machinePressent:      1;
		unsigned nc1:    	    	   1;
		unsigned eStop:                1;
		unsigned nc3:                  1;
		
		unsigned SafeTorqueOff:        1;
		unsigned nc5:                  1;
		unsigned SafeStop:             1;
		unsigned Ax1_1run:             1;           
		
		unsigned Ax1_2run:             1;
		unsigned dummy:                7;
		
	}bits;
}mergerFromSafe;

union cutterToSafe_typ
{
	UINT all;
	struct 
	{
		unsigned typ:     6;
      unsigned lock:    1;
		unsigned dummy:   9;
	}bits;
}cutterToSafe;

union cutterFromSafe_typ
{
	UINT all;
	struct 
	{
		unsigned machinePressent:      1;
		unsigned nc1:    	    	   1;
		unsigned eStop:                1;
		unsigned eStopExt:             1;
		
		unsigned SafeTorqueOff:        1;
		unsigned SafeLimitSpeed:       1;
		unsigned SafeStop:             1;
		unsigned nc7:                  1;
		
		unsigned dummy:                8;
	}bits;
}cutterFromSafe;

union folderToSafe_typ
{
	UINT all;
	struct 
	{
		unsigned typ:     6;
		unsigned dummy:   10;
	}bits;
}folderToSafe;

union folderFromSafe_typ
{
	UINT all;
	struct 
	{
		unsigned machinePressent:      1;
		unsigned nc1:    	    	   1;
		unsigned eStopIn:              1;
		unsigned eStopOut:             1;
		
		unsigned SafeTorqueOff:        1;
		unsigned SafeLimitSpeed:       1;
		unsigned nc6:                  1;
		unsigned nc7:                  1;
		
		unsigned dummy:                8;
	}bits;
}folderFromSafe;

union GESFromSafe_typ
{
	UINT all;
	struct 
	{
		unsigned bufferEstop:                1;
		unsigned nc1:            		     1;
		unsigned nc2:                        1;
		unsigned nc3:                        1;
		
		unsigned nc4:                        1;
		unsigned nc5:                        1;
		unsigned stackerEStop:               1;
		unsigned stackerExtEStop:            1;           
		
		unsigned bufferEStopActivated:       1;
		unsigned nc9:                        1;
		unsigned nc10:                       1;
		unsigned nc11:                       1;
		
		unsigned nc12:                       1;
		unsigned nc13:                       1;
		unsigned stackerEStopActivated:      1;
		unsigned stackerExtEStopActivated:   1;
	}bits;
}gesStatusFromSafe;




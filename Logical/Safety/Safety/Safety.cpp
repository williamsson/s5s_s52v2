
#include <bur/plctypes.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <safety.h>
//#include <AsUDP.h>
//#include <fsm.hpp>
//#include <ctype.h>


#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#define FALSE 0
#define TRUE 1

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF; 

INT logPoint(void);
UINT rstCnt;
BOOL lastlockCover;
USINT slsUnit;
USINT mFrontDoorTime;
USINT cFrontDoorTime;

void checkAxisStatus(USINT func, struct safeFunc_typ * a)
{
	static struct safeFunc_typ x[20];
	char str[80];
	if (func == 0) //check
	{
		if (a->Operational != x[a->id].Operational)
		{
		   strcpy(str,"CAS event:");	
		   strcat(str,a->name);
		   if (a->Operational) strcat(str," ->Operational");	
		   else strcat(str," -> NOT Operational"); 	
		   strcpy(log[logPoint()],str);
		}
		if (a->SDI != x[a->id].SDI) 
		{
			strcpy(str,"CAS event:");	
			strcat(str,a->name);
			if (a->SDI) strcat(str," ->SDI");	
			else strcat(str," -> NOT SDI"); 	
			strcpy(log[logPoint()],str);
		}
		if (a->SLS != x[a->id].SLS)
		{
			strcpy(str,"CAS event:");	
			strcat(str,a->name);
			if (a->SLS) strcat(str," ->SLS");	
			else strcat(str," -> NOT SLS"); 	
			strcpy(log[logPoint()],str);
		}
		if (a->SS  != x[a->id].SS)
		{
			strcpy(str,"CAS event:");	
			strcat(str,a->name);
			if (a->SS) strcat(str," ->SS");	
			else strcat(str," -> NOT SS"); 	
			strcpy(log[logPoint()],str);
		}
		if (a->STO != x[a->id].STO)
		{
			strcpy(str,"CAS event:");	
			strcat(str,a->name);
			if (a->STO) strcat(str," ->STO");	
			else strcat(str," -> NOT STO"); 	
			strcpy(log[logPoint()],str);
		}
		x[a->id] = *a;
	}

	if (func == 1) //setup axis
	{
		x[a->id] = *a;
	}


}

void _INIT ProgramInit(void)
{
	strcpy(safety.reading.stackerServoP3_1.ax1.name ,"stacker aligner transport");
	strcpy(safety.reading.stackerServoP3_1.ax2.name ,"stacker finger push (X)");
	strcpy(safety.reading.stackerServoP3_1.ax3.name ,"stacker finger lift (Y)");
	strcpy(safety.reading.stackerServoP3_2.ax1.name ,"stacker stop plate");
	strcpy(safety.reading.stackerServoP3_2.ax2.name ,"stacker delivery table");
	strcpy(safety.reading.stackerServoP3_2.ax3.name ,"stacker elevator lift (Y)");
	strcpy(safety.reading.stackerServoP3_3.ax1.name ,"gripper module 1");
	strcpy(safety.reading.stackerServoP3_3.ax2.name ,"gripper module 2");
	strcpy(safety.reading.stackerServoP3_3.ax3.name ,"gripper module 3");
	strcpy(safety.reading.bufferServoP2.ax1.name    ,"buffer outfeed");
	strcpy(safety.reading.bufferServoP2.ax2.name    ,"buffer infeed");
	strcpy(safety.reading.mergerServoP2.ax1.name    ,"merger punch unit");
	strcpy(safety.reading.bufferServoP2.ax2.name    ,"merger infeed");
	strcpy(safety.reading.cutterAcposM2_1.ax1.name  ,"cutter support motor");
	strcpy(safety.reading.cutterAcposM2_2.ax1.name  ,"cutter main infeed");
	strcpy(safety.reading.cutterAcposM2_2.ax2.name  ,"cutter main knife");
	strcpy(safety.reading.cutterAcposM2_3.ax1.name  ,"cutter strip knife");
	strcpy(safety.reading.cutterAcposM2_3.ax2.name  ,"cutter reject drive");
	
	safety.reading.stackerServoP3_1.ax1.id=1;
	safety.reading.stackerServoP3_1.ax2.id=2;
	safety.reading.stackerServoP3_1.ax3.id=3;
	safety.reading.stackerServoP3_2.ax1.id=4;
	safety.reading.stackerServoP3_2.ax2.id=5;
	safety.reading.stackerServoP3_2.ax3.id=6;
   safety.reading.bufferServoP2.ax1.id   =7;
   safety.reading.bufferServoP2.ax2.id   =8;
	safety.reading.stackerServoP3_3.ax1.id=9;
	
	
	safety.reading.mergerServoP2.ax2.id=12;
	
	checkAxisStatus(1,&safety.reading.stackerServoP3_1.ax1);
	checkAxisStatus(1,&safety.reading.stackerServoP3_1.ax2);
	checkAxisStatus(1,&safety.reading.stackerServoP3_1.ax3);
	
	checkAxisStatus(1,&safety.reading.stackerServoP3_2.ax1);
	checkAxisStatus(1,&safety.reading.stackerServoP3_2.ax2);
	checkAxisStatus(1,&safety.reading.stackerServoP3_2.ax3);
	
	checkAxisStatus(1,&safety.reading.stackerServoP3_3.ax1);
	
	checkAxisStatus(1,&safety.reading.bufferServoP2.ax1);
	checkAxisStatus(1,&safety.reading.bufferServoP2.ax2);
	
	checkAxisStatus(1,&safety.reading.mergerServoP2.ax2);
	safety.reading.initialized = false;
}

void _CYCLIC ProgramCyclic(void)
{
   static USINT eStopResetCnt;
   static USINT lcResetCnt;
   static USINT fResetCnt;
   static BOOL oldToggler;
   static USINT safetyRunning;
   static USINT bypassStackerTopCoverCnt;
   
   if (oldToggler != toggler && safetyRunning<20)
   {
      safetyRunning++;
   }
   oldToggler = toggler;
   if (lineController.reading.timeSinceStart < safety.setting.initializingTime && (cutter.reading.input.eStop || stacker.reading.input.eStop)) safety.setting.initializingTime--;
   if (lineController.reading.timeSinceStart > safety.setting.initializingTime) safety.reading.initialized = true;
   if (lineController.reading.timeSinceStart == (safety.setting.initializingTime-1)) eStopResetCnt = 10;
   safety.reading.LClatch = dbgLCLatch;
   if (safety.command.resetLClatch)
   {
      safety.command.resetLClatch = FALSE;
      dbgLCLatchReset = TRUE;
   }
   else
   {
      dbgLCLatchReset = FALSE;
   }
   
   if (lineController.setup.merger.installed) safety.setting.mergerType = 1;
   else safety.setting.mergerType = 0;
   
   if (lineController.setup.folder.installed) safety.setting.folderType = 1;
   else safety.setting.folderType = 0;
   
   stackerFromSafe.all = stackerToCPU;
   stackerFromSafe2.all = stackerToCPU_2;
   bufferFromSafe.all = bufferToCPU;
   mergerFromSafe.all = mergerToCPU;
   cutterFromSafe.all = cutterToCPU;
   folderFromSafe.all = folderToCPU;
   bufferToSafe.bits.typ = safety.setting.bufferType;
   if (lineController.setup.stacker.installed)
     stackerToSafe.bits.typ = safety.setting.stackerType;
   else if (lineController.setup.streamer.installed) 
     stackerToSafe.bits.typ = 16;
   mergerToSafe.bits.typ = safety.setting.mergerType;
   cutterToSafe.bits.typ = safety.setting.cutterType;
   folderToSafe.bits.typ = safety.setting.folderType;
   safety.reading.lightCurtainStatus = stackerFromSafe.bits.lightCurtain;
   //gesStatusFromSafe.all = Safety_eStopStatus.monitorGlobalEStop;
   
   safety.reading.SafetyVersion = safetyVersion%256;
	
   checkAxisStatus(1,&safety.reading.stackerServoP3_1.ax1);
   checkAxisStatus(1,&safety.reading.stackerServoP3_1.ax2);
   checkAxisStatus(1,&safety.reading.stackerServoP3_1.ax3);
   checkAxisStatus(1,&safety.reading.stackerServoP3_2.ax1);
   checkAxisStatus(1,&safety.reading.stackerServoP3_2.ax2);
   checkAxisStatus(1,&safety.reading.stackerServoP3_2.ax3);
   checkAxisStatus(1,&safety.reading.bufferServoP2.ax1);
   checkAxisStatus(1,&safety.reading.bufferServoP2.ax2);
   
   
   
   if (bypassStackerTopCoverCnt)
   {
      safety.reading.stackerTopCoverBypassed = true;
      bypassStackerTopCoverCnt--;
   }
   else 
   {
      safety.reading.stackerTopCoverBypassed = false;
      bypassCoverForFingerMove = false;
   }
   
   if (safety.command.bypassStackerTopCover)
   {
      safety.command.bypassStackerTopCover = false;
      bypassCoverForFingerMove = true;
      bypassStackerTopCoverCnt = 15; //was 10, but got error 1004 once, fiserv
   }
   
   
	
   if (eStopResetCnt>0) {
      eStopReset = true;
      eStopResetCnt--;
   }
   else eStopReset = false;
	
   if (safety.command.resetStackerLightCurtain) {
      lcResetCnt = 10;
      strcpy(log[logPoint()],"reset light curtain");
   }
   safety.command.resetStackerLightCurtain = false;
	
   if (lcResetCnt>0)
   {
      lcResetCnt--;
      stackerToSafe.bits.lcReset = TRUE;
   }
   else stackerToSafe.bits.lcReset = FALSE;
	
   if (fResetCnt>0)
   {
      fResetCnt--;
      stackerToSafe.bits.fReset = TRUE;
   }
   else stackerToSafe.bits.fReset = FALSE;
	
   if (safety.command.resetStackerFork)
   {
      fResetCnt = 10;
      strcpy(log[logPoint()],"reset fork Axes");
   }
   safety.command.resetStackerFork = false;

   if (0 == bufferFromSafe.bits.coverFault) buffer.command.coverFault = TRUE;
   if (0 == stackerFromSafe2.bits.coverFault) stacker.command.coverFault = TRUE;
	/*
   if (!gesStatusFromSafe.bits.bufferEstop 
      || !gesStatusFromSafe.bits.stackerEStop 
   || !gesStatusFromSafe.bits.stackerExtEStopActivated) {
      safety.reading.eStop = true;
   }
   else 
      safety.reading.eStop = false;
   */
   
	
   if ((  stacker.reading.input.eStop       || !lineController.setup.merger.installed)
      && (stacker.reading.input.eStopExt    || !lineController.setup.merger.installed)
      && buffer.reading.input.eStop
      && (merger.reading.input.eStop        || !lineController.setup.merger.installed)
      && (folder.reading.input.eStopInfeed  || !lineController.setup.folder.installed)
      && (folder.reading.input.eStopOutfeed || !lineController.setup.folder.installed)
      && cutter.reading.input.eStop) {
      safety.reading.resetPossible = true;
      safety.reading.eStop = false;
      if (safety.command.eStopReset) {
         eStopResetCnt = 10;
         strcpy(log[logPoint()],"reset e-stop");
      }
   }
   else {
      safety.reading.resetPossible = false;
      safety.reading.eStop = true;
   }
   safety.command.eStopReset = false;
	
	
   if (safety.command.lockBufferTopCover)
   {
      bufferToSafe.bits.lock = 1;
      strcpy(log[logPoint()],"buffer lock top cover");
      safety.command.lockBufferTopCover = false;
   }
   else if (safety.command.unlockBufferTopCover)
   {
      bufferToSafe.bits.lock = 0;
      strcpy(log[logPoint()],"buffer unlock top cover");
      safety.command.unlockBufferTopCover = false;
   }
   safety.reading.bufferTopCoverLocked = bufferFromSafe.bits.lock;

   if (cutter.reading.status.state == Status_Loading || cutter.reading.status.state == Status_Ready)
   {
      cutterToSafe.bits.lock = 1;
   }
   else 
   {
      cutterToSafe.bits.lock = 0;
   }
   
   
   if (safety.command.lockStackerTopCover)
   {
      stackerToSafe.bits.lock = 1;
      strcpy(log[logPoint()],"stacker lock top cover");
      safety.command.lockStackerTopCover = false;
   }
   else if (safety.command.unlockStackerTopCover)
   {
      stackerToSafe.bits.lock = 0;
      strcpy(log[logPoint()],"stacker unlock top cover");
      safety.command.unlockStackerTopCover = false;
   }
   safety.reading.stackerTopCoverLocked = stackerFromSafe.bits.lock;

   safety.reading.allCoversClosed = FALSE;
   
   if (!lineController.setup.stacker.installed) safety.reading.allCoversClosed = TRUE;
   else if (stacker.reading.input.topCoverClosed) safety.reading.allCoversClosed = TRUE;
   else safety.reading.allCoversClosed = FALSE;

   if (safety.reading.allCoversClosed)
   {
      if (!lineController.setup.buffer.installed) safety.reading.allCoversClosed = TRUE;
      else if (buffer.reading.input.coverClosed) safety.reading.allCoversClosed = TRUE;
      else safety.reading.allCoversClosed = FALSE;
   }
   
   if (safety.reading.allCoversClosed)
   {
      if (!lineController.setup.merger.installed) safety.reading.allCoversClosed = TRUE;
      else if (merger.reading.input.frontDoor) safety.reading.allCoversClosed = TRUE;
      else safety.reading.allCoversClosed = FALSE;
   }
   
   if (safety.reading.allCoversClosed)
   {
      if (!lineController.setup.folder.installed) safety.reading.allCoversClosed = TRUE;
      else safety.reading.allCoversClosed = FALSE;
   }
	
   if (safety.reading.allCoversClosed)
   {
      if (cutter.reading.input.frontDoor && 
         cutter.reading.input.frontDoorKnife &&
         cutter.reading.input.knifeModule &&
         cutter.reading.input.topCover) safety.reading.allCoversClosed = TRUE;
      else safety.reading.allCoversClosed = FALSE;
   }
	
   //B50coverClosed  = B50_SafetyIO.S2ch_input._34;
   
   /*safetySpeedLimit = 5.0;
   
   if (!C50frontDoor1) safetySpeedLimit = 0.0;
   else if (!C50knifeModule) safetySpeedLimit = 0.0;
   else if (!C50topCover) safetySpeedLimit = 0.15;
   else if (!C50frontDoor2) safetySpeedLimit = 0.20;
   else safetySpeedLimit = 5.0;*/
   
   //F50infeedCover  = F50_SafetyIO.S2ch_input._56;
   //F50outfeedCover = F50_SafetyIO.S2ch_input._78;
   //copy safety data	
   /*eStop.S50_Active    =   (Safety_eStopStatus.monitor & 0x1); //bit 0
   eStop.B50_Active    =   (Safety_eStopStatus.monitor & 0x2); //bit 1
   eStop.F50_1_Active  =   (Safety_eStopStatus.monitor & 0x4); //bit 2
   eStop.F50_2_Active  =   (Safety_eStopStatus.monitor & 0x8); //bit 3
   eStop.C50_Active    =   (Safety_eStopStatus.monitor & 0x10); //bit 4
   eStop.M50_Active    =   (Safety_eStopStatus.monitor & 0x20); //bit 5
   eStop.S50_Pressed   =   !(Safety_eStopStatus.monitor & 0x100);
   eStop.B50_Pressed   =   !(Safety_eStopStatus.monitor & 0x200);
   eStop.F50_1_Pressed =   !(Safety_eStopStatus.monitor & 0x400);
   eStop.F50_2_Pressed =   !(Safety_eStopStatus.monitor & 0x800);
   eStop.C50_Pressed   =   !(Safety_eStopStatus.monitor & 0x1000);
   eStop.M50_Pressed   =   !(Safety_eStopStatus.monitor & 0x2000);*/
   
   //eStop.ResetPossible = Safety_eStopStatus.ResetPossible;
   //if (eStop.Any_Pressed != Safety_eStopStatus.Pressed && Safety_eStopStatus.Pressed)
   //		strcpy(ULog.text[ULog.ix++], "Safety: E-stop pressed");
   //	eStop.Any_Pressed = Safety_eStopStatus.Pressed;
   
   //Let's control the reset of E-stop
   /*if (TRUE == eStop.ResetEstop)
   {
   strcpy(ULog.text[ULog.ix++], "Safety: Reset E-stop Request");
   if (eStop.S50_Pressed)
   {
   Safety_eStopCommand.ResetRequest = FALSE;
   eStop.Any_Pressed = TRUE;
   eStop.ResetEstop = FALSE;
   strcpy(ULog.text[ULog.ix++], "Safety: Not possible! S50 E-stop still pressed");
   }
   else if (eStop.B50_Pressed)
   {
   Safety_eStopCommand.ResetRequest = FALSE;
   eStop.Any_Pressed = TRUE;
   eStop.ResetEstop = FALSE;
   strcpy(ULog.text[ULog.ix++], "Safety: Not possible! B50 E-stop still pressed");
   }
   else if (eStop.F50_1_Pressed)
   {
   Safety_eStopCommand.ResetRequest = FALSE;
   eStop.Any_Pressed = TRUE;
   eStop.ResetEstop = FALSE;
   strcpy(ULog.text[ULog.ix++], "Safety: Not possible! F50_1 E-stop still pressed");
   }
   else if (eStop.F50_2_Pressed)
   {
   Safety_eStopCommand.ResetRequest = FALSE;
   eStop.Any_Pressed = TRUE;
   eStop.ResetEstop = FALSE;
   strcpy(ULog.text[ULog.ix++], "Safety: Not possible! F50_2 E-stop still pressed");
   }
   else if (Safety_eStopStatus.ResetPossible)
   {
   Safety_eStopCommand.ResetRequest = TRUE;
   eStop.Any_Pressed = FALSE;
   eStop.ResetEstop = FALSE;
   rstCnt = 0;
   strcpy(ULog.text[ULog.ix++], "Safety: Reset Request ON");
   }
   }
   else
   {
   if (TRUE == Safety_eStopCommand.ResetRequest)
   {
   rstCnt++;
   strcpy(ULog.text[ULog.ix++], "Safety: !");
   if (rstCnt>5) {
   Safety_eStopCommand.ResetRequest = FALSE;
   strcpy(ULog.text[ULog.ix++], "Safety: Reset Request OFF");
   }
   if (eStop.Any_Pressed) Safety_eStopCommand.ResetRequest = FALSE;
   }
   }*/
   //B50 stuff
   //

   //if (buffer.reading.stateRunning || buffer.reading.stateLoad) Safety_B50.lockCover = TRUE;
   if ((buffer.reading.status.state == Status_Ready && !buffer.reading.status.stopped) 
      || buffer.reading.status.state == Status_Loading) Safety_B50.lockCover = TRUE;
   else Safety_B50.lockCover = FALSE;
   if (lastlockCover != Safety_B50.lockCover)
   {
      if (Safety_B50.lockCover) strcpy(ULog.text[ULog.ix++], "Safety: B50 lock");
      else strcpy(ULog.text[ULog.ix++], "Safety: B50 unlock");
   }
   lastlockCover = Safety_B50.lockCover;
   //B50_coverOpen = FALSE; //TODO
   //S50 stuff
   //Safety_Misc.resetStackerLift = FALSE;
   /*if (lightCurtainResetCnt) {
   lightCurtainResetCnt--;
   Safety_Misc.resetStackerLift = TRUE;
   }*/
   /*Safety_Misc.StackerInDeliver = FALSE;
   if (stackerInDeliver)
   {
   Safety_Misc.StackerInDeliver = TRUE;
   }*/
	
			
   if (!buffer.reading.input.coverClosed)
   {
      safety.reading.maximumSpeed = 0.155;
      if (slsUnit != 1) strcpy(log[logPoint()],"buffer SLS (top cover)");
      slsUnit = 1;
   }
   else if (!cutter.reading.input.topCover)
   {
      if (cutter.reading.fastFeed && (cutter.reading.input.feedButton || merger.reading.input.feedButton))
         safety.reading.maximumSpeed = 0.2;
      else
         safety.reading.maximumSpeed = 0.155;
      if (slsUnit != 3) strcpy(log[logPoint()],"cutter SLS (top cover)");
      slsUnit = 3;
   }
   else if (!cutter.reading.input.frontDoorKnife)
   {
      safety.reading.maximumSpeed = 0.155;
      if (slsUnit != 4) strcpy(log[logPoint()],"cutter SLS (front door knife)");
      slsUnit = 4;
   }
   else if (!folder.reading.input.infeedSideDoor && lineController.setup.folder.installed)
   {
      safety.reading.maximumSpeed = 0.155;
      if (slsUnit != 5) strcpy(log[logPoint()],"folder SLS (infeed side door)");
      slsUnit = 5;
   }
   else if (!folder.reading.input.infeedTopCover && lineController.setup.folder.installed)
   {
      safety.reading.maximumSpeed = 0.155;
      if (slsUnit != 6) strcpy(log[logPoint()],"folder SLS (infeed top cover)");
      slsUnit = 6;
   }
   else if (!folder.reading.input.outfeedTopCover && lineController.setup.folder.installed)
   {
      safety.reading.maximumSpeed = 0.155;
      if (slsUnit != 7) strcpy(log[logPoint()],"folder SLS (outfeed top cover)");
      slsUnit = 7;
   }
   else if (!folder.reading.input.rightFrontDoor && lineController.setup.folder.installed)
   {
      safety.reading.maximumSpeed = 0.155;
      if (slsUnit != 8) strcpy(log[logPoint()],"folder SLS (right front door)");
      slsUnit = 8;
   }
   else {
      safety.reading.maximumSpeed = 10.0;
      slsUnit = 0;		
   }
   
   if (!cutter.reading.input.frontDoor) {
      if (0 == cFrontDoorTime) cutter.command.stop = TRUE;
      if (cFrontDoorTime<250)
          cFrontDoorTime++;
      if      (cFrontDoorTime>=20)                                    safety.reading.maximumSpeed = 0.0;
      else if (cFrontDoorTime>=17 && safety.reading.maximumSpeed>0.4) safety.reading.maximumSpeed = 0.4;
      else if (cFrontDoorTime>=14 && safety.reading.maximumSpeed>1.6) safety.reading.maximumSpeed = 1.6;
   }
   else if (cFrontDoorTime>12)
   {
      safety.reading.maximumSpeed = 0.0;
      cFrontDoorTime=0;
   }
   else cFrontDoorTime=0;
   
   if (!merger.reading.input.frontDoor && lineController.setup.merger.installed) {
      if (0 == mFrontDoorTime) cutter.command.stop = TRUE;
      if (mFrontDoorTime<250)
         mFrontDoorTime++;
      if      (mFrontDoorTime>=20)                                    safety.reading.maximumSpeed = 0.0;
      else if (mFrontDoorTime>=17 && safety.reading.maximumSpeed>0.4) safety.reading.maximumSpeed = 0.4;
      else if (mFrontDoorTime>=14 && safety.reading.maximumSpeed>1.6) safety.reading.maximumSpeed = 1.6;
   }
   else if (mFrontDoorTime>12)
   {
      safety.reading.maximumSpeed = 0.0;
      mFrontDoorTime=0;
   } 
   else mFrontDoorTime=0;
 
   
   
	
   stackerFromCPU = stackerToSafe.all;
   bufferFromCPU  = bufferToSafe.all;
   mergerFromCPU  = mergerToSafe.all;
   cutterFromCPU  = cutterToSafe.all;
   folderFromCPU  = folderToSafe.all;
}

INT logPoint(void)
{
	logP++;
	if (logP>19) logP = 0;
	return logP;
}

void _EXIT ProgramExit(void)
{
	// Insert code here 

   
}


TYPE
	S_2ch_io_typ : 	STRUCT 
		_12 : BOOL;
		_34 : BOOL;
		_56 : BOOL;
		_78 : BOOL;
	END_STRUCT;
	S_io_typ : 	STRUCT 
		_1 : BOOL;
		_2 : BOOL;
		_3 : BOOL;
		_4 : BOOL;
		_5 : BOOL;
		_6 : BOOL;
		_7 : BOOL;
		_8 : BOOL;
	END_STRUCT;
	Safety_Logic_E_stop_typ : 	STRUCT 
		InSTO : BOOL; (*Safe torque off (status)*)
		monitorGlobalEStop : UINT;
		ResetDone : BOOL; (*ResetRequest executed*)
	END_STRUCT;
	X20SC_typ : 	STRUCT 
		S_output : S_io_typ;
		S_input : S_io_typ;
		S2ch_input : S_2ch_io_typ;
		S2ch_inputOK : S_2ch_io_typ;
		S_inputOK : S_io_typ;
	END_STRUCT;
	Safety_Logic_E_stop_command_typ : 	STRUCT 
		ResetRequest : BOOL; (*Reset Em stop*)
	END_STRUCT;
	SafetyB50_typ : 	STRUCT 
		coverOpen : BOOL;
		lockCover : BOOL;
	END_STRUCT;
END_TYPE

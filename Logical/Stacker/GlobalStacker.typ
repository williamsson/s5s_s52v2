
TYPE
	stackerAxisStatus_typ : 	STRUCT 
		transport : readAxis_typ;
		deliver : readAxis_typ;
		forkPush : readAxis_typ;
		forkElevator : readAxis_typ;
		endStop : readAxis_typ;
		spaghettiBelt : readAxis_typ;
		stackerElevator : readAxis_typ;
	END_STRUCT;
	stackerError_typ : 	STRUCT 
		text : STRING[80];
		subTyp : UINT;
		timeStamp : UDINT;
		typ : USINT;
	END_STRUCT;
	stackerLastErrors_typ : 	STRUCT 
		pointer : USINT;
		error : ARRAY[0..9]OF stackerError_typ;
	END_STRUCT;
	basic_axisState_typ : 	STRUCT  (*axis state structure*)
		Disabled : BOOL; (*if set, axis is in state Disabled*)
		StandStill : BOOL; (*if set, axis is in state StandsStill*)
		Homing : BOOL; (*if set, axis is in state Homing*)
		Stopping : BOOL; (*if set, axis is in state Stopping*)
		DiscreteMotion : BOOL; (*if set, axis is in state DiscreteMotion*)
		ContinuousMotion : BOOL; (*if set, axis is in state ContinuousMotion*)
		SynchronizedMotion : BOOL; (*if set, axis is in state SynchronizedMotion*)
		ErrorStop : BOOL; (*if set, axis is in state ErrorStop*)
	END_STRUCT;
	basic_command_typ : 	STRUCT  (*command structure*)
		Power : BOOL; (*switch on the controller*)
		Home : BOOL; (*reference the axis*)
		MoveAbsolute : BOOL; (*move to an defined position*)
		MoveAdditive : BOOL; (*move a defiened distance*)
		MoveVelocity : BOOL; (*start a movement with a defiened velocity*)
		StartSlave : BOOL; (*start following a master axis*)
		StopSlave : BOOL; (*stop following a master axis*)
		Halt : BOOL; (*stop every active movement once*)
		Stop : BOOL; (*stop every active movement as long as set*)
		MoveJogPos : BOOL; (*move in positive direction as long as is set*)
		MoveJogNeg : BOOL; (*move in negative direction as long as is set*)
		ErrorAcknowledge : BOOL; (*reset active errors*)
	END_STRUCT;
	basic_parameter_typ : 	STRUCT  (*parameter structure*)
		Position : REAL; (*target-position for MoveAbsolute-Command*)
		Distance : REAL; (*distance for MoveAdditive-Command*)
		Velocity : REAL; (*velocity for MoveVelocity-Command*)
		Direction : USINT; (*direction for commanded movements*)
		Acceleration : REAL; (*acceleration for commanded movements*)
		Deceleration : REAL; (*deceleration for commanded movements*)
		HomePosition : REAL; (*target-position for referencing the axis*)
		HomeMode : USINT; (*homing mode*)
		JogVelocity : REAL; (*velocity for jogging movement*)
		ratioNumerator : INT;
		ratioDenominator : UINT;
		MasterAxis : UDINT; (*master axis reference*)
	END_STRUCT;
	basic_status_typ : 	STRUCT  (*status structure*)
		ErrorID : UINT; (*ErrorID of any occured error*)
		ErrorText : ARRAY[0..3]OF STRING[79]; (*Error Text*)
		ActPosition : REAL; (*actual position of the axis*)
		ActVelocity : REAL; (*actual velocity of the axis*)
		DriveStatus : MC_DRIVESTATUS_TYP; (*actual status of the axis*)
	END_STRUCT;
	basic_typ : 	STRUCT  (*control structure*)
		Command : basic_command_typ; (*command structure*)
		Parameter : basic_parameter_typ; (*parameter structure*)
		Status : basic_status_typ; (*status structure*)
		AxisState : basic_axisState_typ; (*axis state structure*)
	END_STRUCT;
	color : 
		(
		RED,
		MAGENTA,
		BLUE,
		CYAN,
		GREEN,
		LIME,
		YELLOW,
		ORANGE,
		WHITE
		);
	ExitJobControl : 	STRUCT 
		pg : sheet;
		isSpeed : REAL;
		setSpeed : REAL;
		sheetSize : REAL := 300.0; (*(mm) Sheet size, can only be changed when webspeed is zero*)
	END_STRUCT;
	InfoSheetHandler : 	STRUCT 
		maxStackingPageSize : REAL := 457.2; (*Maximum stacking page size in (mm)*)
		maxBypassPageSize : REAL := 1000.0; (*Maximum bypass page size (mm)*)
		maxSpeed : REAL := 0.0; (*current maximum speed of the sheet handler,is 0 if the stacker isn't ready to receive pages  (m/s)*)
		deliverPageSpeed : REAL := 1.0; (*Deliver page maximum speed (m/s)*)
		deliverWastePage : REAL := 1.0; (*Deliver waste page maximum speed (m/s)*)
		offsetPageSpeed : REAL := 3.0; (*Offset page maximum speed (m/s)*)
		normalPageSpeed : REAL := 3.0; (*Normal page maximum speed (m/s)*)
		changeFormatSpeed : REAL := 0.0; (*Page with new format (m/s)*)
		changeToStackSpeed : REAL := 0.0; (*going from bypass to stacking, page speed (m/s)*)
		changeToBypassSpeed : REAL := 0.0; (*going from stacking to bypass. page speed (m/s)*)
		deliverNextRequest : BOOL := FALSE; (*over min stack (flag)*)
		deliverRequest : BOOL := FALSE; (*over max stack (flag)*)
		canTakeDeliverPage : BOOL := TRUE; (*Can take a new deliver page. Delivery in progress (flag)*)
		st : stack; (*stack structure*)
	END_STRUCT;
	offsetPos_enum : 
		(
		inNorth,
		inSouth
		);
	physical_stacker_inputs : 	STRUCT 
		wasteBoxWarning : BOOL;
		wasteBoxFull : BOOL;
		homeForkPush : BOOL; (*home finger pusher in/out*)
		tableClearSensor2 : BOOL; (*cross stacker sensor*)
		tableClearSensor3 : BOOL; (*cross stacker sensor*)
		tableClearSensor4 : BOOL; (*cross stacker sensor*)
		tableClearSensor1 : BOOL; (*cross stacker sensor*)
		homeForkLift : BOOL; (*home finger up/down*)
		rejectCrash : BOOL;
		homeElevator : BOOL; (*home stacker lift*)
		homeEndStop : BOOL; (*home stop plate*)
		seqUnitNotReady : BOOL; (*convayer*)
		infeedGate : BOOL; (*ballrack infeed*)
		readyGripper : BOOL; (*only with Mitsubishi PLC gripper, Not Used*)
		exitTransport1 : BOOL;
		exitTransport2 : BOOL;
		exitTransport3 : BOOL;
		enterTransport : BOOL;
		lightCurtain : BOOL; (*lightCurtain*)
		topCoverClosed : BOOL;
		eStopExt : {REDUND_UNREPLICABLE} BOOL;
		eStop : BOOL;
		rackGate : BOOL;
		userInput1 : BOOL;
		userInput2 : BOOL;
		antistaticOk : BOOL;
		userInput3 : BOOL;
		alignerOutfeedCnt1 : UINT; (*Streamer only (s52)*)
		alignerOutfeedCnt2 : UINT; (*Streamer only (s52)*)
		alignerOutfeedCnt3 : UINT; (*Streamer only (s52)*)
		alignerOutfeedDigital2 : BOOL; (*Streamer only (s52)*)
		alignerOutfeedDigital3 : BOOL; (*Streamer only (s52)*)
		alignerOutfeedDigital1 : BOOL; (*Streamer only (s52)*)
		homePosJogger1 : BOOL; (*Streamer only (s52)*)
		homePosJogger2 : BOOL; (*Streamer only (s52)*)
		homePosJogger3 : BOOL; (*Streamer only (s52)*)
		streamerGap1 : BOOL; (*Streamer only (s52)*)
		streamerGap2 : BOOL; (*Streamer only (s52)*)
		streamerGap3 : BOOL; (*Streamer only (s52)*)
		exitStreamerSection1 : BOOL; (*Streamer only (s52)*)
		exitStreamerSection2 : BOOL; (*Streamer only (s52)*)
		exitStreamerSection3 : BOOL; (*Streamer only (s52)*)
		alignerTopCoverClosed : BOOL; (*Streamer only (s52)*)
		streamerCrashRight : BOOL; (*Streamer only (s52)*)
		streamerCrashLeft : BOOL; (*Streamer only (s52)*)
		manFeed : BOOL; (*Streamer only (s52)*)
		stackerHatch : BOOL; (*Streamer only (s52)*)
		streamerStrRollerPos : REAL; (*Streamer only (s52)*)
		streamerStopPlatePos : REAL; (*Streamer only (s52)*)
		streamerGate : BOOL; (*Streamer only (s52)*)
		pressUnitSensor : BOOL;
		offsetMarkReaderCnt : UINT;
		ebmEncoder : BOOL;
		glueOffsetMarkSensor : BOOL;
		pressureGuard : BOOL;
		joggerStackClosed : BOOL;
		homePosJoggerStack : BOOL;
		SouthStopPlateLeft : BOOL;
		SouthStopPlateRight : BOOL;
		NorthStopPlateLeft : BOOL;
		NorthStopPlateRight : BOOL;
		offsetMarkReader : BOOL;
		AnalogStepSensor : INT;
	END_STRUCT;
	physical_stacker_outputs : 	STRUCT 
		comPulseGripper : BOOL;
		offsetNorthGripper : BOOL;
		offsetSouthGripper : BOOL; (*home finger pusher in/out*)
		stackDeliverGripper : BOOL; (*cross stacker sensor*)
		offsetValveSpaghettiBelt1 : BOOL; (*offset up / down*)
		offsetValveSpaghettiBelt2 : BOOL; (*paper back holder*)
		secUnitControl : BOOL;
		antistatic : BOOL;
		userOut1 : BOOL;
		userOut2 : BOOL;
		userOut3 : BOOL;
		pressUnitStart : BOOL;
		offsetValve1 : BOOL; (*only streamer*)
		offsetValve2 : BOOL; (*only streamer*)
		offsetValve3 : BOOL; (*only streamer*)
		offsetValve4 : BOOL; (*only streamer*)
		offsetValve5 : BOOL; (*only streamer, Stack drop*)
		mainAirValve : BOOL; (*only streamer*)
		teachGlueOffsetMarkSensor : BOOL; (*only streamer*)
		enableAirGlue : BOOL; (*only streamer*)
	END_STRUCT;
	RT1382 : 	STRUCT 
		cto : BOOL;
		fileI : BOOL;
		funcI : BOOL;
		insI : BOOL;
		fnl : BOOL;
		cTime : UINT;
		cCnt : UINT;
		par01 : INT;
		engineRun : BOOL;
		res01 : INT;
	END_STRUCT;
	stack : 	STRUCT 
		continuous : BOOL; (*The stack was not delivered on a job or set end. i.e button press deliver, max stack deliver*)
		offsets : UINT; (*Offsets in stack*)
		sequence : UDINT; (*stack counter, changes on every stack*)
		stackSize : REAL; (*stack size in mm*)
		pagesInStack : UDINT; (*no of pages in stack*)
		pageSize : REAL; (*sheet size in mm*)
		stacks : USINT; (*1-5 up,, no of stacks in paralell*)
	END_STRUCT;
	stacker_ext_command_typ : 	STRUCT 
		load : BOOL; (*not implemented*)
		tableStepsLooser : BOOL;
		tableStepsAvg : BOOL;
		tableStepsTighter : BOOL;
		stoppedDeliveryRequest : BOOL; (*set to TRUE when a deliver is needed*)
		pressButton1 : BOOL;
		pressButton2 : BOOL;
		goToZeroPos : BOOL;
		pressButton3 : BOOL;
		instantDeliver : BOOL;
		coverFault : BOOL;
		stepFingers : BOOL;
		resetCounters : BOOL;
	END_STRUCT;
	counters_typ : 	STRUCT 
		loads : UDINT;
		sheets : UDINT;
		stackerElevatorTravelDistance : UDINT;
		transportTableTravelDistance : UDINT;
		deliveryTableTravelDistance : UDINT;
		stoppedDeliveries : UDINT;
		offsets : UDINT;
		errorAcknowlages : UDINT;
		onTheFlyDeliveries : UDINT;
	END_STRUCT;
	tripCounter_typ : 	STRUCT 
		jobs : UDINT;
		meters : UDINT;
		minutes : UDINT;
		avgSpeed : REAL; (*m/m*)
		pages : UDINT;
	END_STRUCT;
	stacker_reading_typ : 	STRUCT 
		offsetCounter : DINT;
		currentStackSize : REAL;
		lastStackSize : REAL;
		sheetSignalFrontEdge : BOOL := FALSE;
		stackExit : BOOL := FALSE; (*stack exit*)
		deliverPageReceived : BOOL := FALSE; (*true when a deliver page is seen by the stacker, *)
		input : physical_stacker_inputs;
		output : physical_stacker_outputs;
		gapTime : REAL; (*ms*)
		deliverGapTime : REAL; (*s*)
		forksInOffsetToNorthPos : BOOL := FALSE;
		startUpSeq : UINT := 0; (*0 -> 15*)
		maximumSpeed : REAL; (*m/s*)
		maxStackSize : REAL; (*mm*)
		stackSize : REAL; (*mm*)
		offsetPositionStr : STRING[14]; (*north/south (0/1)*)
		deliverFlyState : USINT; (*dotf state*)
		lowestID : USINT; (*limiter*)
		deliveriesSinceLoad : UINT; (*number of sets*)
		offsetsInStack : USINT; (*number of sets*)
		offsetPosition : offsetPos_enum; (*north/south (0/1)*)
		lastErrors : stackerLastErrors_typ;
		button1 : colorOfButton;
		button2 : colorOfButton;
		button3 : colorOfButton;
		btnFunc1 : USINT;
		btnFunc2 : USINT;
		btnFunc3 : USINT;
		feedPressed : BOOL; (*feed from the buttons*)
		status : machineStatus_typ;
		axisStatus : stackerAxisStatus_typ;
		stepSensorSetPoint : REAL; (*mm*)
		stepSensorValue : REAL; (*mm*)
		stepAvg : REAL; (*mm*)
		stackerPosition : REAL; (*mm*)
		forkPosition : REAL; (*mm*)
		stackUsage : REAL; (*%*)
		debug : note_type;
		stackPages2 : UDINT; (*pages in queue, times 2 if slit/merge checked*)
		transportPages : USINT; (*pages in queue*)
		stackPages : UDINT; (*pages in stack*)
		stackTableEmpty : BOOL;
		SFTransCalc : REAL; (*mm, debug infromation*)
		SFTransPos : REAL; (*mm, The position the stacker will go to when transfer the stack from the fingers to the stacker*)
		actualOverSpeed : REAL;
		buttonPushLog : buttonLog_typ;
		tripCounter : tripCounter_typ;
		timeToToggleHoldBack : REAL;
		timeToToggleStopPlate : REAL;
		warningCode : USINT;
		offsetStateTimer : REAL := 0.0;
		goBackPage : UDINT := 0;
		offsetState : USINT := 0;
		guiControlShowFingerStepButton : UINT := 0;
	END_STRUCT;
	stacker_setting_typ : 	STRUCT 
		minStackSize : REAL := 50; (*mm*)
		maxStackSize : REAL := 150.0; (*mm*)
		spaghettiOverSpeed : REAL := 1.05;
		deliveryRunDistance : REAL := 620; (*mm*)
		deliveryPosition : REAL := 25.0; (*mm*)
		startInSouth : BOOL;
		holdPostition : REAL := 0;
		StackerFingerZeroAdjust : REAL := 0; (*mm*)
		highSpeedDelay : REAL := 0; (*s*)
		fingerHightAdjust : REAL := 0; (*mm*)
		deliverTransportSpeed : REAL := 0.3; (*stacker belt speed (m/s)*)
		forceOutputValue : physical_stacker_outputs;
		forceOutputEnable : physical_stacker_outputs;
		tableSteps : REAL := 0.1; (*sheet thikness (mm)*)
		transportTableSpeed : REAL := 1.32; (*factor*)
		useStepSensor : DINT := 0; (*0, no step sensor.... 3 only step sensor*)
		automaticEndStop : BOOL := TRUE;
		firstStepFactor : REAL := 1.0; (*compensation on first stack step*)
		holdingPositionGradient : REAL := 0; (*mm*)
		backStop : REAL := 0; (*mm*)
		transportIdleSpeed : REAL := 0.25; (*m/s*)
		forkHoldAtOffsetAdjust : REAL := 0; (*mm*)
		forkDeliverPositionAdjust : REAL := 0; (*mm*)
		topPosOffset : REAL; (*mm*)
		ignoreError_Dev : USINT := 0; (*0x01 (global power) 0x02 (transport outfeed) 0x04 (wastebox)*)
		forceOffset_Dev : USINT := 0;
		forceFeed_Dev : REFERENCE TO BOOL;
		forceAxis_Dev : BOOL := FALSE;
		delayOffsetPage : DINT := 1;
	END_STRUCT;
	stacker_typ : 	STRUCT 
		setting : stacker_setting_typ;
		reading : stacker_reading_typ;
		command : stacker_ext_command_typ;
	END_STRUCT;
	stepStruct : 	STRUCT 
		stackPages : UDINT;
		useSensor : BOOL;
		newPageInStack : BOOL;
		measurement : ARRAY[0..4]OF REAL;
		sheets : USINT;
		adjustStep : REAL;
		tableSteps : REAL;
		avgStep : REAL;
	END_STRUCT;
	versionStruct : 	STRUCT 
		Program : USINT;
		RemoteComm : USINT;
	END_STRUCT;
	noteInfo_typ : 	STRUCT 
		typ : USINT;
		real1 : REAL;
		real2 : REAL;
		bool1 : BOOL;
		bool2 : BOOL;
		timeStamp : UDINT;
	END_STRUCT;
	note_type : 	STRUCT 
		infoP : USINT;
		info : ARRAY[0..30]OF noteInfo_typ;
	END_STRUCT;
	buttonLogData_typ : 	STRUCT 
		color : color;
		timeStamp : UDINT;
		state : USINT;
		button : USINT;
	END_STRUCT;
	readAxis_typ : 	STRUCT 
		temperature : REAL;
		current_D : REAL;
		current_Q : REAL;
		torque : REAL;
		speed : REAL;
		pos : REAL;
		actualSpeed : REAL;
		homeState : USINT;
		lastErrorID : UINT;
		errorID : UINT;
		synchronized : BOOL := FALSE;
	END_STRUCT;
	buttonLog_typ : 	STRUCT 
		data : ARRAY[0..19]OF buttonLogData_typ;
		p : USINT;
	END_STRUCT;
END_TYPE

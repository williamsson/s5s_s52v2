
#include <bur/plctypes.h>
//#include <brsystem.h>
#include <s50.h>
#include <stdio.h>
#include <string.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

void    setAxis(void);                                                                         // set all axis
void    getAxis(void);                                                                         // get all axis values
DWORD   homeAxis(DWORD ax);                                                                    // home axis
void    errorAckAll (BOOL halt);                                                               // clear axis errors										
BOOL    load (BOOL forceHome, BOOL deliverWaste, BOOL stopDel, BOOL findZero, BOOL homeOnly);  // load machine, or stopped delivery									
REAL    sheetHandlerLimiter (INT function, INT id, REAL speed);	                               // max speed 
USINT   pageInTransportQueue (USINT a, struct sheet b);                                        // transport queue
void    initSpeeds(void);
void    stackerStepSpaghetti (void);
BOOL    ready(void);	
void    transportExit(void);			
void    runTransport(void);		
void    stackerStep (void);
void    transportEnter(void);
void    quarterSecondCycle(void);
void    buttonCommand(void);
void    enforceLimits(void);
USINT   checkPendingError(void);
void    resetTransport(void);
void    sheetSignal (void);
BOOL    makeUnloaded(void);
BOOL    errorState(BOOL * continueLoaded);
void    clearErrors (void);
void    addError(USINT errorNo,USINT stopScenario,BOOL unload,UINT subType);
void    comSync(void);
void    retError (REAL *err, REAL *type);
void    debugeEvent (REAL p1, REAL p2);
void    deliverOnTheFly(void);
void    ballrack (void);
void    evalSoftStop();
void    offsetControl (USINT func);                                                            //0 do nothing, 1 north, 2 south, 3 toggle
void    holdBackControl (USINT func);                                                          //0 do nothing, 1 north, 2 south, 3 toggle
void    tableDown(void);
void    fingerMoveOut(REAL mm);
USINT   maxInQueue(void);
BOOL    hold (USINT);
BOOL    release (USINT);
BOOL    readStat (USINT);
BOOL    amIstopping(USINT);
BOOL    releaseAll(USINT);
void    drivePower(BOOL onOff);
void    addErrorToStackerLastError(USINT typ,UINT subTyp);
float   speedDelay(int f, float t, float s);
void    unHome(void);
void    setButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL    isButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL    (*sStop[])(USINT) = {hold, release, readStat, amIstopping, releaseAll};
REAL    overSpeedSimple(REAL v,REAL i,REAL o,BOOL s);
REAL    overSpeedSimpleInv(REAL v,REAL i);
void    accelerateSpeed(REAL vi, REAL vs, REAL a, REAL d, REAL t, REAL i, REAL f, REAL e, USINT tag);
char    *inttostr(long i, char b[]);
char    *uinttostr(unsigned long i, char b[]);
void    controlTableStepsButton(void);
void    checkGripperReady(void);
void    getInputs(void);
void    setOutputs(void);
void    checkWasteBox(void);
void    handleStartUpSeq(void);
void    addDebugNote(USINT typ, REAL realDebug1, REAL realDebug2,REAL realDebug3 ,BOOL boolDebug);
void    keepCoverLocked(void);
void    keepCoverUnLocked(void);
BOOL    stackerMoving (void);
void    checkAxis (void);
void    checkPower(void);
void    checkCover(void);
REAL    distTransportCalc(UINT func);
void    getCutterSpeed(void);
void    setWarningFlag(BOOL reset);
void    checkSheetSequence(void);
void    standStillTimer(void);

//continious Axis
void    setInfeedTransport (void);                                                             //called from set setAxis
void    setDeliverTableBelt (void);                                                            //called from set setAxis
void    getInfeedTransport(void);                                                              //called from set getAxis
void    getDeliverTableBelt (void);                                                            //called from set getAxis
//horizontal Axis	
void    setFingerX (void);                                                                     //called from set setAxis
void    getFingerX (void);                                                                     //called from set getAxis
void    setStopPlateX (void);                                                                  //called from set setAxis
void    getStopPlateX (void);                                                                  //called from set getAxis
//vertical Axis	
void    setFingerY (void);                                                                     //called from set setAxis
void    getFingerY (void);                                                                     //called from set getAxis
void    setStackerY (void);                                                                    //called from set setAxis
void    getStackerY (void);                                                                    //called from set getAxis

static  char dbgPrintStr[80];
static  char tBuf[20];

struct sheet dummySheet;

//#define MAX_EVENTS 20
#define FORCE_RELOAD         1
#define CONTINIUE_LOADED     0
//INT a1;
INT buttonLedTest = 0;
BOOL errorTableDown = 0;
USINT queueErrTimer = 0;
USINT errorJustCleared = 0;
USINT queueErrCnt;
USINT queueErrorIndication=0;
UDINT lastStackPages=0;
//BOOL bStop = FALSE;
REAL fingerYMaxPosInit;
BOOL stopDeliverToExit;
struct sheet pocketPage;
USINT pageInPocket = 0;
USINT speedUpOverSpeedCnt = 0;
BOOL lastSheetSignal;
//BOOL enterReady=0;
BOOL enterError=1;
BOOL enterNotLoaded = 1;
BOOL stackerDriveStartUp=0;
BOOL resetMoveVelocityFlagDeliver=0;
BOOL dbgTmpWasFeeding;
BOOL lastTransportTableExit;
static BOOL fakePage;
USINT delayIt;

#define north 0
#define south 1

//BOOL wasteBoxFullErrorTriggerd = 0;
//UINT unlockTimerCnt;

void getInputs(void)
{  
   //LSI
   //stacker.reading.input.enterTransport    = physical.stacker.digital.in27;
   //stacker.reading.input.exitTransport     = physical.stacker.digital.in25;
   //stacker.reading.input.infeedCrash       = physical.stacker.digital.in18;
   //stacker.reading.input.infeedGate        = physical.stacker.digital.in20;
   //stacker.reading.input.rackGate          = physical.stacker.digital.in31;
   //stacker.reading.input.readyGripper      = physical.stacker.digital.in21;
   //stacker.reading.input.seqUnitNotReady   = physical.stacker.digital.in16;
   //stacker.reading.input.stepSensor        = physical.stacker.digital.in23;
   //stacker.reading.input.tableClearSensor1 = physical.stacker.digital.in9;
   //stacker.reading.input.tableClearSensor2 = physical.stacker.digital.in11;
   //stacker.reading.input.tableClearSensor3 = physical.stacker.digital.in13;
   //stacker.reading.input.tableClearSensor4 = physical.stacker.digital.in15;
   //stacker.reading.input.wasteBoxFull      = physical.stacker.digital.in4;
   //stacker.reading.input.wasteBoxWarning   = physical.stacker.digital.in2;
   //stacker.reading.input.AnalogStepSensor  = physical.stacker.analog.in1;
	
   //digital
   stacker.reading.input.readyGripper      = 1; //not connected
   stacker.reading.input.enterTransport    = physical.stacker.digital.in1;    //1_1 ,1 
   stacker.reading.input.wasteBoxWarning   = physical.stacker.digital.in2;    //1_2 ,9
   //stacker.reading.input.infeedCrash1    = physical.stacker.digital.in3;    //1_3 ,2
   stacker.reading.input.wasteBoxFull      = physical.stacker.digital.in4;    //1_4 ,10
   //stacker.reading.input.infeedCrash2    = physical.stacker.digital.in5;    //1_5 ,3
   stacker.reading.input.antistaticOk      = physical.stacker.digital.in6;    //1_6 ,11
   //stacker.reading.input.infeedCrash3    = physical.stacker.digital.in7;    //1_7 ,4
   //stacker.reading.input.homeForkPush    = physical.stacker.digital.in8;    //1_8 ,12  in Stacker.Motion.AxisControl  (Stacker->Motion->AxisContol->Main.st)
   stacker.reading.input.tableClearSensor1 = physical.stacker.digital.in9;    //1_9 ,5 
   //stacker.reading.input.homeForkLift    = physical.stacker.digital.in10;   //1_10,13  in Stacker.Motion.AxisControl  (Stacker->Motion->AxisContol->Main.st)
   stacker.reading.input.tableClearSensor2 = physical.stacker.digital.in11;   //1_11,6
   //1_12,14  Available (in12)
   stacker.reading.input.tableClearSensor3 = physical.stacker.digital.in13;   //1_13,7
   //stacker.reading.input.homeElevator    = physical.stacker.digital.in14;   //1_14,15  in Stacker.Motion.AxisControl  (Stacker->Motion->AxisContol->Main.st)
   stacker.reading.input.tableClearSensor4 = physical.stacker.digital.in15;   //1_15,8
   stacker.reading.input.seqUnitNotReady   = physical.stacker.digital.in16;   //1_16,16  Available (in16)	
   //stacker.reading.input.homeEndStop     = physical.stacker.digital.in17;   //2_1 ,17  in Stacker.Motion.AxisControl  (Stacker->Motion->AxisContol->Main.st)
   //2_2 ,25  Available (in18)
   //2_3 ,18  Available (in19)
   stacker.reading.input.infeedGate        = physical.stacker.digital.in20;   //2_4 ,26
   //2_5 ,19  Available (in21)
   stacker.reading.input.userInput1        = physical.stacker.digital.in22;   //2_6 ,27 
   //2_7 ,20  Available (in23) 
   //gripper.reading.input.homeGripper1    = physical.stacker.digital.in24;   //2_8 ,28  in Gripper.Automat.automatCyclic.st
   stacker.reading.input.exitTransport1    = physical.stacker.digital.in25;   //2_9 ,21  shared input with gripper trigger
   //gripper.reading.input.homeGripper2    = physical.stacker.digital.in26;   //2_10,29  in Gripper.Automat.automatCyclic.st
   stacker.reading.input.exitTransport2    = physical.stacker.digital.in27;   //2_11,22
   //gripper.reading.input.homeGripper3    = physical.stacker.digital.in28;   //2_12,30  in Gripper.Automat.automatCyclic.st
   stacker.reading.input.exitTransport3    = physical.stacker.digital.in29;   //2_13,23
   stacker.reading.input.userInput2        = physical.stacker.digital.in30;   //2_14,31  Available (in30) 
   stacker.reading.input.rackGate          = physical.stacker.digital.in31;   //2_15,24
   stacker.reading.input.userInput3        = physical.stacker.digital.in32;   //2_16,32
	
   if (0 == safety.setting.stackerType) stacker.reading.input.lightCurtain = 0;
   if (1 == safety.setting.stackerType) stacker.reading.input.lightCurtain = physical.stacker.digital.in33 && safety.reading.lightCurtainStatus; //33  
   if (2 == safety.setting.stackerType) stacker.reading.input.lightCurtain = physical.stacker.digital.in37 && safety.reading.lightCurtainStatus; //33  
			  
   stacker.reading.input.eStop             = physical.stacker.digital.in35;
   stacker.reading.input.topCoverClosed    = physical.stacker.digital.in36;
   stacker.reading.input.eStopExt          = physical.stacker.digital.in38;
   
   
	
   //analog
   stacker.reading.input.AnalogStepSensor  = physical.stacker.analog.in1; 
}

void setOutputs(void)
{
	/*if (stacker.setting.forceOutputEnable.comPulseGripper)
		physical.stacker.digital.out8  = stacker.reading.output.comPulseGripper     = stacker.setting.forceOutputValue.comPulseGripper;
	else physical.stacker.digital.out8  = stacker.reading.output.comPulseGripper     = output.comPulseGripper;    
	
	if (stacker.setting.forceOutputEnable.offsetNorthGripper)
		physical.stacker.digital.out10 = stacker.reading.output.offsetNorthGripper   = stacker.setting.forceOutputValue.offsetNorthGripper;
	else physical.stacker.digital.out10 = stacker.reading.output.offsetNorthGripper  = output.offsetNorthGripper;
	
	if (stacker.setting.forceOutputEnable.offsetSouthGripper)
		physical.stacker.digital.out12 = stacker.reading.output.comPulseGripper      = stacker.setting.forceOutputValue.offsetSouthGripper;
	else physical.stacker.digital.out12 = stacker.reading.output.offsetSouthGripper  = output.offsetSouthGripper;
	
	if (stacker.setting.forceOutputEnable.stackDeliverGripper)
		physical.stacker.digital.out14 = stacker.reading.output.stackDeliverGripper  = stacker.setting.forceOutputValue.stackDeliverGripper;
	else physical.stacker.digital.out14 = stacker.reading.output.stackDeliverGripper = output.stackDeliverGripper;
   */
   if (stacker.setting.forceOutputEnable.offsetValveSpaghettiBelt2)
      physical.stacker.digital.out1 = stacker.reading.output.offsetValveSpaghettiBelt2  = stacker.setting.forceOutputValue.offsetValveSpaghettiBelt2;
   else physical.stacker.digital.out1 = stacker.reading.output.offsetValveSpaghettiBelt2 = output.offsetValveSpaghettiBelt2;
   
   if (stacker.setting.forceOutputEnable.offsetValveSpaghettiBelt1)
      physical.stacker.digital.out2 = stacker.reading.output.offsetValveSpaghettiBelt1  = stacker.setting.forceOutputValue.offsetValveSpaghettiBelt1;
   else physical.stacker.digital.out2 = stacker.reading.output.offsetValveSpaghettiBelt1 = output.offsetValveSpaghettiBelt1;

   if (stacker.setting.forceOutputEnable.secUnitControl)
      physical.stacker.digital.out3 = stacker.reading.output.secUnitControl  = stacker.setting.forceOutputValue.secUnitControl;
   else physical.stacker.digital.out3 = stacker.reading.output.secUnitControl = output.secUnitControl;
   
   if (stacker.setting.forceOutputEnable.antistatic)
      physical.stacker.digital.out4 = stacker.reading.output.antistatic  = stacker.setting.forceOutputValue.antistatic;
   else physical.stacker.digital.out4 = stacker.reading.output.antistatic = output.antistatic;
   
   
   
   
   if (stacker.setting.forceOutputEnable.userOut1)
      physical.stacker.digital.out15 = stacker.reading.output.userOut1  = stacker.setting.forceOutputValue.userOut1;
   else physical.stacker.digital.out15 = stacker.reading.output.userOut1 = output.userOut1;
   
   if (stacker.setting.forceOutputEnable.userOut2)
      physical.stacker.digital.out16 = stacker.reading.output.userOut2  = stacker.setting.forceOutputValue.userOut2;
   else physical.stacker.digital.out16 = stacker.reading.output.userOut2 = output.userOut2;

}

void setWarningFlag(BOOL reset)
{
   int i;
   static unsigned int tick;
   stacker.reading.status.warning = FALSE;
   if (reset) {
      for (i=0;i<MAX_WARNINGID;i++) {
         warnings[i] = FALSE;
         stacker.reading.warningCode = 0;
      }   
      return;
   }
   for (i=0;i<MAX_WARNINGID;i++) {
      if (warnings[i]) {
         stacker.reading.status.warning = TRUE;
         stacker.reading.warningCode = 0;
         tick++;
         if (i == WASTEBOXWARNING) {
            stacker.reading.warningCode = 1;
            if (tick%300 == 0) lineController.command.beep = 1;  
         }
         if (i == SECUNITNOTREADY) {
            stacker.reading.warningCode = 2;
            if (tick%300 == 0) lineController.command.beep = 1;
         }
         /*if (i == DELIVERING) stacker.reading.warningCode = 3;
         if (i == STOPPEDDELIVERING) stacker.reading.warningCode = 4;*/
         return;
      }
   }
}

void _CYCLIC ProgramCyclic(void)
{
   if (FALSE == lineController.setup.stacker.installed) return;
   getInputs();
   static LREAL lastSecondTick;
   static LREAL secUnitTime;
   static REAL minute;
     
   warnings[SECUNITNOTREADY] = FALSE;
   if (stacker.reading.input.seqUnitNotReady) secUnitTime = mainTick;
   if ((mainTick-secUnitTime)>2.0) warnings[SECUNITNOTREADY] = TRUE;
   if (stacker.reading.input.wasteBoxWarning && stacker.reading.input.wasteBoxFull)  warnings[WASTEBOXWARNING] = FALSE;

   if (0 == lineController.reading.initDone) stacker.command.coverFault = FALSE;
   if (stacker.command.coverFault && safety.reading.initialized) addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,6);
   else if (!safety.reading.initialized) stacker.command.coverFault = FALSE;
    
   setWarningFlag(CHK);
   getCutterSpeed();
   spaghettiStep.inputValue = ((REAL)(stacker.reading.input.AnalogStepSensor) - 16383)/5461;
   stacker.reading.deliverFlyState = (USINT)(deliverFly.state);
   stacker.reading.lowestID = sheetHandlerLimiter(GET_LOWEST_ID,0,0);
   stacker.reading.stackPages = step.stackPages;
   stacker.reading.stackPages2 = stacker.reading.stackPages * (merger.setting.enabled?2:1);
   stacker.reading.stepAvg = spaghettiStep.average;
   stacker.reading.stepSensorValue = spaghettiStep.inputValue;
   stacker.reading.forkPosition = stacker.setting.topPosOffset - stacker.setting.fingerHightAdjust + stacker.reading.axisStatus.forkElevator.pos - stacker.setting.deliveryPosition;
   stacker.reading.stackerPosition = stacker.reading.axisStatus.stackerElevator.pos - stacker.setting.deliveryPosition;
   if (state == NotLoadedMode)  strcpy(stacker.reading.offsetPositionStr,"Not Loaded");
   else if (stacker.reading.offsetPosition == inNorth) {
      if (deliverFly.pendingDeliver || cutter.command.deliverNextRequest) strcpy(stacker.reading.offsetPositionStr,"N. (Deliver)");
      else strcpy(stacker.reading.offsetPositionStr,"North"); 
   }
   else {
      if (deliverFly.pendingDeliver || cutter.command.deliverNextRequest) strcpy(stacker.reading.offsetPositionStr,"S. (Deliver)");
      else strcpy(stacker.reading.offsetPositionStr,"South"); 
   }
   
   if (stacker.command.resetCounters) {
      stacker.reading.tripCounter.jobs = 0;
      stacker.reading.tripCounter.pages = 0;
      stacker.reading.tripCounter.meters = 0;
      stacker.reading.tripCounter.minutes = 0;
      stacker.reading.tripCounter.avgSpeed = 0;
      stacker.command.resetCounters = FALSE;
   }
   
   if (fromInit) {
      initSpeeds();
      buttonLedTest = 0;
      fingerYMaxPosInit = fingerY.maxPos;
      speedDelay(SETUP,0.1,0);
      stacker.reading.startUpSeq = 0;
	   reHome = 1;
   }	
   fingerY.maxPos = stacker.setting.fingerHightAdjust;// + fingerYMaxPosInit;
   if (inSpeed.predict>0)
      stacker.reading.actualOverSpeed = stacker.reading.axisStatus.transport.actualSpeed/inSpeed.predict;
   else 
      stacker.reading.actualOverSpeed = 0;
   fromInit = 0;
   mainTick += SECONDS_UNIT;
   minute += SECONDS_UNIT;
   
   if (minute>60) {
      stacker.reading.tripCounter.minutes++;
      stacker.reading.tripCounter.avgSpeed = (REAL)(stacker.reading.tripCounter.meters)/(REAL)(stacker.reading.tripCounter.minutes);
      minute -= 60;
   }

   handleStartUpSeq();	
   comSync();

   if ((mainTick-lastSecondTick) > 0.25){
      lastSecondTick = mainTick;
      if (safety.reading.initialized && stacker.reading.startUpSeq>11) quarterSecondCycle();
   }

   if (safety.reading.initialized && stacker.reading.startUpSeq>14) buttonCommand();

   if (bStop == TRUE) {
      if (inSpeed.is == 0) {
         bStop = FALSE;
         (*sStop[holdSoftStop])(BTN_PRESS);
      }
   }
   
   output.antistatic = FALSE;
   if (state == ReadyMode) output.antistatic = stacker.reading.input.topCoverClosed;

   enforceLimits();
   inQueue = pageInTransportQueue(GET_NO_PAGES_IN_QUEUE);
   stacker.reading.transportPages = inQueue;
   inQueueMax = maxInQueue();
   if (inQueue > inQueueMax) queueErrCnt++;
   else queueErrCnt = 0;
   if (queueErrCnt>15) queueErrTimer = 250;
   if (step.stackPages%8 == 5 && queueErrTimer>100 && lastStackPages != step.stackPages) queueErrorIndication++;
   if (step.stackPages<8) queueErrorIndication = 0;
   if (queueErrorIndication==4) {
      strcpy(dbgPrintStr, "*S50 *WARNING* queue Error detected !!!");
      strcpy(ULog.text[ULog.ix++], dbgPrintStr);
      queueErrorIndication = 0;
      resetQueue = TRUE;
   }
   lastStackPages = step.stackPages;
   queueError = FALSE;
   if (queueErrTimer) {
      queueErrTimer--;
      queueError = TRUE;
   }
   if (resetQueue) {
      strcpy(dbgPrintStr, "*S50 queue Repair!!");
      strcpy(ULog.text[ULog.ix++], dbgPrintStr);
      if (inQueue && pageInTransportQueue(PEEK_PAGE)==NORMAL_PAGE) {
         pageInTransportQueue(REMOVE_LAST_PAGE);
      }
      resetQueue = FALSE;
      queueErrTimer = 0;
   }
   getAxis(); // also writes acc/dec to axis
   if (state != NotLoadedMode) {
      commandStatus.loading = 0;
   }
   if (state != ReadyMode) {
      if (state == StoppedDelivery && loadingState>=5) // pre-start 
         sheetHandlerLimiter(SET_SPEED,READY,MAX_SPEED);
      else
         sheetHandlerLimiter(SET_SPEED,READY,STOP);

      sheetHandlerLimiter(SET_SPEED,STOP_TIME,MAX_SPEED);
      distTransportCalc(SETUP);
      sheetHandlerLimiter(SET_SPEED,FIRST_SHEET,MAX_SPEED);
   }
   else {
      sheetHandlerLimiter(SET_SPEED,READY,MAX_SPEED);
   }	
   //if (mainTick - deliverFly.stopStart > 0.4/*setting2.deliverPageStopTime*/) { //moved to ZeroSpeedHold in mainCyclic.ST (167)
   //   REAL tmp;
   //   tmp = 0.6 + (deliverFly.holdSpeed-0.5)*0.4;
   //   if (tmp>1) tmp = 1;
   //   if (mainTick - deliverFly.stopStart > tmp) {
   //      sheetHandlerLimiter(SET_SPEED,STOP_TIME,MAX_SPEED); 
   //   }
   //}
   /*----------------------- Status and SET_SPEED(id:NOT_READY) -------------------------*/
   if (state != ErrorMode) {
      sheetHandlerLimiter(SET_SPEED,ERROR,MAX_SPEED);
      enterError = 1;
      if (stackerLift.Status.ErrorID == 35201) {
         addError(LIGHT_CURTAIN,NORMAL_STOP,FORCE_RELOAD,3);
		 reHome = 1;
         state = ErrorMode;
      }
   }
   if (state != NotLoadedMode) {
      enterNotLoaded = 1;
      stacker.command.goToZeroPos = FALSE;
   }
   if (state == ReadyMode) {
      sheetHandlerLimiter(SET_SPEED,NOT_READY,MAX_SPEED);
      stacker.reading.status.state = Status_Ready;
   }
   else if (state == ErrorMode) {
      sheetHandlerLimiter(SET_SPEED,NOT_READY,MAX_SPEED);
      stacker.reading.status.state = Status_Error;
   }
   else if (state == NotLoadedMode) {
      sheetHandlerLimiter(SET_SPEED,NOT_READY,0);
   }
   else if (state == StoppedDelivery) {
      cutter.command.stopDeliverRequest = FALSE;
      sheetHandlerLimiter(SET_SPEED,NOT_READY,MAX_SPEED);
   }
   else {
      sheetHandlerLimiter(SET_SPEED,NOT_READY,MAX_SPEED);
   }
   //stacker.reading.stackSize  = stackerY.maxPos + stacker.setting.topPosOffset - stackerY.pos;
   stacker.reading.stackSize  = stacker.setting.topPosOffset - stackerY.pos;
   //stacker.reading.maxStackSize = stackerY.maxPos + stacker.setting.topPosOffset - stacker.setting.deliveryPosition - 25;
   stacker.reading.maxStackSize = stacker.setting.topPosOffset - stacker.setting.deliveryPosition - 25;
   
   if (Status_Ready == stacker.reading.status.state) stacker.reading.stackUsage = (stacker.reading.stackSize * 100) / stacker.reading.maxStackSize;
   else stacker.reading.stackUsage = 0;
   
   /*-----------------------Handle E stop error--------------------------------*/
   if (FALSE == safety.reading.initialized)
   {
      state = NotLoadedMode;
   }
   else if (safety.reading.eStop && state != ErrorMode) {
      if (!stacker.reading.input.eStop) {
         addError(E_STOP_BY_ME,NORMAL_STOP,FORCE_RELOAD,0);
		 reHome = 1;
         errorByS50Estop = 1;
         state = ErrorMode;         
      }
      else {
         if (state == StoppedDelivery && state == ReadyMode) {
            addError(NO_POWER,NORMAL_STOP,FORCE_RELOAD,3);
			reHome = 1;
            state = ErrorMode;
         }
      }
   }
   //for the GUI
   if (STEP_SENSOR) stepDownColorMapControl = 0;
   else stepDownColorMapControl = 1;
   //
   switch (state) {
      case TestMode:/*----------------------------------------------------------------------------TestMode-------*/
			
         break; /*-----------------------------------------------------------------------------------------------*/
      case ReadyMode:/*--------------------------------------------------------------------------ReadyMode-------*/
         if (stackerMoving() || unlockTimerCnt) keepCoverLocked();
         else keepCoverUnLocked();
         if (unlockTimerCnt) unlockTimerCnt--;
							
         if (!ready()) {
            state = ErrorMode;								                                    //-->ErrorMode
         }
         else
         {
            if (command.stoppedDelivery) {
               if (!cutter.command.stopDeliverRequest) {// hold this speed...
                  transportFloorSpeed[STOPPED_DELIVERY_MINSPEED].speed = overSpeedSimple (inSpeed.set,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
                  transportFloorSpeed[STOPPED_DELIVERY_MINSPEED].timer = tRtime((TRANSPORT_TABLE_LENGTH + JobControlExit.sheetSize)*3,
                     transportFloorSpeed[STOPPED_DELIVERY_MINSPEED].speed,
                     6.0);
               }
               cutter.command.stopDeliverRequest = TRUE;
               if (inSpeed.is < 0.01) 
                  state = StoppedDelivery;					                                   //-->StoppedDelivery
            }
            else if (command.load) {
               sheetHandlerLimiter(SET_SPEED,READY,STOP);
               if (inSpeed.is < 0.01) {
                  state = NotLoadedMode;						                               //-->NotLoadedMode
                  addDebugNote(4,0,0,0,0);
               }
            }
         }
         break;/*------------------------------------------------------------------------------------------------*/
      case NotLoadedMode:/*--------------------------------------------------------------------NotLoadedMode-----*/
         delayIt = 0;
         if (safety.reading.stackerTopCoverBypassed) {
		     addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,7);
			 reHome = 1;
	     }
         if (enterNotLoaded) {
            if (unlockTimerCnt==0) safety.command.unlockStackerTopCover = TRUE;
            enterNotLoaded = 0;
         }
         
         if (unlockTimerCnt) {
            unlockTimerCnt--;
            if (unlockTimerCnt == 0) safety.command.unlockStackerTopCover = TRUE;
         }
         
         if (TRUE == stacker.command.goToZeroPos) { 
            stacker.command.goToZeroPos = FALSE;
            command.findZero = TRUE;
            addDebugNote(4,-10.0,0,1,0);
         }
         
         if (command.load || command.stoppedDelivery || command.findZero) {
            stacker.reading.status.state = Status_Loading;
            if (command.findZero) { //command.findZero is reset in load()
               commandStatus.findingZero = 1;
               stacker.reading.status.state = Status_ManualLoad;
            }
            else { //command.load is reset in load()
               commandStatus.loading = 1;
               stacker.reading.status.state = Status_Loading;
            }
            command.stoppedDelivery = 0;
            commandStatus.deliveringStopped = 0;
            deliverTableBelt.speed = 0.0;
            infeedTransport.speed = 0.0;
            safety.command.lockStackerTopCover = TRUE;
            addDebugNote(4,-5.0,0,1,0);
         }
 
         if (commandStatus.findingZero && load(FIND_ZERO)) {
            addDebugNote(4,0,0,1,0);
            if (checkPendingError()>NO_ERROR) state = ErrorMode;
            else state = NotLoadedMode;
            commandStatus.findingZero = 0;
         }	
         else if (commandStatus.loading && load(reHome,0,0,0,0)/*load(FORCED_HOME_LOAD)*/) { 
            state = ReadyMode;									                                //-->ReadyMode
            sheetHandlerLimiter(SET_SPEED,DELIVERY,MAX_SPEED);
            addDebugNote(1,0,0,command.load,command.stoppedDelivery);
         }
         else if (!commandStatus.loading && !commandStatus.findingZero) {
            stacker.reading.status.state = Status_NotLoaded;
         }
         if (!stacker.reading.input.topCoverClosed && !button.push1 /*&& machineError.active*/) infeedTransport.speed = 0;
         
         setAxis();
         break;/*-------------------------------------------------------------------------------------------------*/
      case ErrorMode:/*---------------------------------------------------------------------------ErrorMode-------*/
         if (enterError) {
            safety.command.unlockStackerTopCover = TRUE;
            enterError = 0;
         }
         sheetHandlerLimiter(SET_SPEED,ERROR,STOP);
         sheetHandlerLimiter(SET_SPEED,DELIVERY,MAX_SPEED);
         if (errorState(&continueLoaded)) {
            if (continueLoaded) state = ReadyMode;			                                   //-->ReadyMode
            else { 
               state = NotLoadedMode;					                                       //-->NotLoadedMode
               addDebugNote(4,0,0,0,1);
            }
         }
         break;/*------------------------------------------------------------------------------------------------*/
      case StoppedDelivery:/*-----------------------------------------------------------------StoppedDelivery----*/
         cutter.command.stopDeliverRequest = TRUE;
         keepCoverLocked();
         unlockTimerCnt=300;
         if (homeAx != 0) { //axis not homed
            command.stoppedDelivery = 0; //reset command, loading results in an delivery
            command.load = 1;
            sheetHandlerLimiter(SET_SPEED,DELIVERY,MAX_SPEED);
            state = NotLoadedMode;	 //-->NotLoadedMode
            addDebugNote(4,0,0,1,1);
         }
         else {
            if (!stacker.reading.input.seqUnitNotReady) {
               addError(SEC_UNIT_NOT_READY,NORMAL_STOP,CONTINIUE_LOADED,1);
               state = ReadyMode;
               bStop = TRUE; // go to stop fiserv
            }
            else { 
                if (command.stoppedDelivery) commandStatus.deliveringStopped = 1;
                if (commandStatus.deliveringStopped && load(STOPPED_DELIVER)) {
                   cutter.command.stopDeliverRequest = FALSE;
                   sheetHandlerLimiter(SET_SPEED,DELIVERY,MAX_SPEED);
                   state = ReadyMode;												            //-->ReadyMode
                }
            }
         }
         break;/*------------------------------------------------------------------------------------------------*/
      default:
         break;
   }
   lastSheetSignal = cutter.reading.sheetSignal;
   
   lastTransportTableExit = TRANSPORT_TABLE_EXIT_1;
   /*---------------------------------------------SPAGHETTI------------------------------------------------------*/
   if (lineController.setup.stacker.spaghettiBelts && stacker.reading.startUpSeq >= 15) {
      if (stacker.reading.status.state == Status_NotLoaded || stacker.reading.status.state == Status_Error) {
         spaghettiBelt.Parameter.Velocity = 0;
         spaghettiBelt.Parameter.Acceleration = 3000;
         spaghettiBelt.Parameter.Deceleration = 3000;
         spaghettiBelt.Command.Halt = TRUE;
         if (spaghettiBelt.Status.ErrorID != 0 && !spaghettiBelt.Command.ErrorAcknowledge) spaghettiBelt.Command.ErrorAcknowledge = TRUE;
         if (stacker.reading.feedPressed) spaghettiBelt.Parameter.Velocity = transport.Parameter.Velocity;
      }
      else {
         if (deliverFly.inProgress || deliverFly.stackingOnFingers) speedUpOverSpeedCnt=250;
         spaghettiBelt.Parameter.Velocity = transport.Parameter.Velocity * stacker.setting.spaghettiOverSpeed;
         if (speedUpOverSpeedCnt>60 && stacker.setting.spaghettiOverSpeed<0.8) spaghettiBelt.Parameter.Velocity = transport.Parameter.Velocity * 0.8; //it easily crashes on the fingers if the overSpeed is too low
         if ((cutter.reading.processingWhite || curlDistance>0.1) && stacker.setting.spaghettiOverSpeed<1.05) spaghettiBelt.Parameter.Velocity = transport.Parameter.Velocity * 1.05; //this is junk anyway, reduces crashes during curly paper

         spaghettiBelt.Parameter.Acceleration = transport.Parameter.Acceleration * 1.075;
         if (speedUpOverSpeedCnt) {
            spaghettiBelt.Parameter.Deceleration = transport.Parameter.Acceleration * 0.25; //slowly meet the speed
            speedUpOverSpeedCnt--;
         }   
         else
            spaghettiBelt.Parameter.Deceleration = transport.Parameter.Acceleration * 0.95;
         if (transport.Status.ActVelocity > 0.09) {
            if (moveStarted && spaghettiBelt.Command.MoveVelocity == FALSE) {
               spaghettiBelt.Command.MoveVelocity = TRUE; //do it twice..
               moveStarted = FALSE;
            }
            if (lastSpaghettiSpeed != spaghettiBelt.Parameter.Velocity) {
               spaghettiBelt.Command.MoveVelocity = TRUE;
               moveStarted = TRUE;
            }
            lastSpaghettiSpeed = spaghettiBelt.Parameter.Velocity;
         }
      }
      stacker.reading.axisStatus.spaghettiBelt.actualSpeed = spaghettiBelt.Status.ActVelocity/1000;
      stacker.reading.axisStatus.spaghettiBelt.errorID = spaghettiBelt.Status.ErrorID;
      if (spaghettiBelt.Status.ErrorID != 0) stacker.reading.axisStatus.spaghettiBelt.lastErrorID = spaghettiBelt.Status.ErrorID;
      stacker.reading.axisStatus.spaghettiBelt.speed = spaghettiBelt.Parameter.Velocity/1000;
      stacker.reading.axisStatus.spaghettiBelt.pos = spaghettiBelt.Status.ActPosition;
      stacker.reading.axisStatus.spaghettiBelt.synchronized = FALSE;
   }
   /*------------------------------------------------------------------------------------------------------------*/
   setOutputs();
}

void comSync(void)
{
	static BOOL lastPendingDeliverToggle;
	JobControlExit.isSpeed = cutter.reading.webSpeed;
	if (JobControlExit.isSpeed < 0.02) JobControlExit.isSpeed = 0.0;

	JobControlExit.setSpeed = cutter.reading.setSpeed;
	stacker.reading.maximumSpeed = sheetHandlerLimiter(SPEED_LIMIT);
	/*if (deliverFly.state > 0 && stacker.reading.maximumSpeed == 0)
    {
	    strcpy(dbgPrintStr, "*S50 stacker.reading.maximumSpeed = 0 stackerID:");
        strcat(dbgPrintStr, uinttostr((unsigned long)(sheetHandlerLimiter(GET_LOWEST_ID,0,0)),tBuf));
        strcat(dbgPrintStr, "\0");
        strcpy(ULog.text[ULog.ix++], dbgPrintStr);
	} */
	if (stacker.command.stoppedDeliveryRequest && state == NotLoadedMode) command.load=TRUE;
	else if (stacker.command.stoppedDeliveryRequest && JobControlExit.isSpeed == 0) command.stoppedDelivery = ON;
	stacker.command.stoppedDeliveryRequest = FALSE;
	
	if (JobControlExit.sheetSize != cutter.reading.sheetSize  /*rCommToStacker.sheetSize*/) {
		JobControlExit.sheetSize = cutter.reading.sheetSize;  //rCommToStacker.sheetSize;
		realMinMaxCheck(&JobControlExit.sheetSize,100.0,450.0);
	}
	if (cutter.reading.pendingDeliverToggle != lastPendingDeliverToggle || stacker.command.instantDeliver) {
	    /* HOLD SPEED DURING DELIVERY */                                                       //printerSpeed
		deliverFly.holdSpeed                                                                   = buffer.reading.infeedSpeed * stacker.setting.transportTableSpeed;  
                                                                                               //75% of maxSpeed 
		if (deliverFly.holdSpeed < (cutter.setting.infeedRunSpeed/1500)) deliverFly.holdSpeed  = (cutter.setting.infeedRunSpeed/1500);  
                                                                                               //current speed (current run speed)
		if (deliverFly.holdSpeed < infeedTransport.speed) deliverFly.holdSpeed                 = infeedTransport.speed; 
                                                                                               //idleSpeed (min speed of aligner transport)
		if (deliverFly.holdSpeed < stacker.setting.transportIdleSpeed) deliverFly.holdSpeed    = stacker.setting.transportIdleSpeed; 
        
		lastPendingDeliverToggle = cutter.reading.pendingDeliverToggle;
		deliverFly.pendingDeliver = TRUE;
		cutter.command.deliverNextRequest = FALSE;
        
        strcpy(dbgPrintStr, "*S50 pending deliver toggle pIT:");
        strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue (GET_NO_PAGES_IN_QUEUE)),tBuf));
        strcat(dbgPrintStr, "\0");
        strcpy(ULog.text[ULog.ix++], dbgPrintStr);
	}   
	JobControlExit.pg = cutter.reading.currentPage;
   if (cutter.reading.currentPage.type == DELIVER_PAGE) {
      if (!stacker.reading.deliverPageReceived) addDebugNote(50,0,0,0,0); 
      stacker.reading.deliverPageReceived = TRUE;
   }
   else stacker.reading.deliverPageReceived = FALSE;
}

USINT maxInQueue(void)
{
	REAL tmpOverSpeed;
	REAL usedLength;
	REAL isSpeedUsed;
	static REAL isSpeed[6];
	static USINT p;
	static USINT tD;
	isSpeedUsed = JobControlExit.isSpeed * 2 + isSpeed[0] + isSpeed[1] + isSpeed[2] + isSpeed[3] + isSpeed[4] + isSpeed[5];
	isSpeedUsed = isSpeedUsed / 8;
	if (isSpeedUsed<0.05)  
		tmpOverSpeed = 10;
	else
		tmpOverSpeed = infeedTransport.actualSpeed/isSpeedUsed;
	if (tmpOverSpeed>=10) 
		tmpOverSpeed = 10;   
	tmpOverSpeed = (tmpOverSpeed*2 + stacker.setting.transportTableSpeed*3)/5;
	if (tmpOverSpeed<0.9) tmpOverSpeed = 0.9;
	usedLength = (TRANSPORT_TABLE_LENGTH + 10) / tmpOverSpeed;  
	if ((++tD)%9==0) {
		isSpeed[p] = JobControlExit.isSpeed;
		if (++p>=6) p = 0;
	}
	return (1 + (USINT)(usedLength/JobControlExit.sheetSize)); 
}

void buttonCommand (void)
{
   if (FALSE == safety.reading.initialized) return;
   static REAL unloadTimer;
   static BOOL lastTopCoverClosed;
   static USINT loadPressed;
   static BOOL fingerXmove;
   static BOOL stackerXmove;
   static BOOL infeedSpeedSetByButton;
   static USINT doublePressTime;
   static DWORD ax;
   BOOL btn1;
   BOOL btn2;
   BOOL btn3;
   if (doublePressTime) doublePressTime--; //double press on feed should feed all.
   /* so we can push buttons from stacker.command (automation studio) */
   if (button.push1) btn1 = TRUE;
   else if (stacker.command.pressButton1) btn1 = TRUE;
   else if (state == ReadyMode && lastTopCoverClosed != stacker.reading.input.topCoverClosed && !stacker.reading.input.topCoverClosed) btn1 = TRUE;
   else btn1 = FALSE;
   stacker.command.pressButton1 = FALSE;
	
   if (button.push2) btn2 = TRUE;
   else if (stacker.command.pressButton2) btn2 = TRUE;
   else btn2 = FALSE;
   stacker.command.pressButton2 = FALSE;
	
   if (button.push3) btn3 = TRUE;
   else if (stacker.command.pressButton3) btn3 = TRUE;
   else btn3 = FALSE;
   
   stacker.command.pressButton3 = FALSE;
   stacker.reading.feedPressed  = FALSE;
    
    if ((*sStop[readSoftStopStatus])(BTN_PRESS) && state == ReadyMode && transport.Status.ActVelocity < 0.05 && lineController.reading.guiControlLevel>1 && stacker.reading.input.topCoverClosed)
       stacker.reading.guiControlShowFingerStepButton = SHOW;
    else
    {
        stacker.reading.guiControlShowFingerStepButton = HIDE; 
        if (fingerXmoved)
        {
            if (((0 == (*sStop[readSoftStopStatus])(BTN_PRESS)) || state != ReadyMode) && (transport.Status.ActVelocity > 0.1 || lineController.reading.guiControlLevel<=1))
            {
                fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                fingerXmoved = fingerXmove = FALSE;
                lineController.command.beep = TRUE;
            }
            if (lastTopCoverClosed != stacker.reading.input.topCoverClosed && stacker.reading.input.topCoverClosed)
            {
                fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                fingerXmoved = fingerXmove = FALSE;
                lineController.command.beep = TRUE;
            }
        }  
    }
    
    if (stacker.reading.input.topCoverClosed && fingerXmoved && fingerXmove && lineController.reading.guiControlLevel>1) {
        fingerX.absMove = fingerX.actualPositon + 10.0;
        fingerXmove = FALSE;
    }
    
    if (stacker.command.stepFingers) 
    {
        if ((*sStop[readSoftStopStatus])(BTN_PRESS) && state == ReadyMode && transport.Status.ActVelocity < 0.05 && stacker.reading.input.topCoverClosed)
        {
            fingerXmoved = TRUE;
            fingerXmove = TRUE; 
            lineController.command.shortBeep = TRUE;
            
        }
        stacker.command.stepFingers = FALSE;
    }
    	
   /************************************* BUTTON 1 ************************************** (RED,WHITE,YELLOW) */
   if (btn1 && !button.lastPush1)
   {
      if (stacker.reading.input.topCoverClosed)
      {
         if (state == NotLoadedMode) {
            loadPressed = 1;
         }
         if (isButton(1,COLOR_WHITE))
         {
            (*sStop[releaseSoftStop])(BTN_PRESS);
            bStop = FALSE;
         }
      }
      if (isButton(1,COLOR_RED) && state == ReadyMode)
      {
         bStop = TRUE;
         cutter.command.stop = TRUE;
      }
   }
   //handle load press (steady white button), long press should load line
   if (state == NotLoadedMode && isButton(1,COLOR_WHITE))
   {
      if (loadPressed && btn1) {
         loadPressed++;
         if (loadPressed >= 220) {
            if (stacker.reading.input.topCoverClosed) {
               if (loadPressed == 220) globalPower = TRUE;
               if (loadPressed == 230 && buffer.reading.input.coverClosed) {
                  buffer.command.load = TRUE;
                  if (Status_NotLoaded == folder.reading.status.state || Status_ManualLoad == folder.reading.status.state) folder.command.pressOutfeedButton1 = TRUE;
               }
               if (loadPressed == 235 && safety.reading.allCoversClosed) cutter.command.home = TRUE;
               if (loadPressed == 240) {
                  command.load = ON;
                  loadPressed = 0;
                  lineController.command.beep = TRUE;
               }
            }
            else if (stacker.reading.input.topCoverClosed && loadPressed > 240) {
               command.load = ON;
               loadPressed = 0;
               lineController.command.beep = TRUE;
            }
         }
      }
      else if (loadPressed && !btn1) {
         if (loadPressed < 220 && stacker.reading.input.topCoverClosed) {
            command.load = ON;
            ebmGlue.command.sendAll = TRUE;
         }
         loadPressed = 0;
      }
   }
   else loadPressed = 0;
   //stopped cover open..
   if (isButton(1,COLOR_YELLOW) && !stacker.reading.input.topCoverClosed) {
      if ((btn1 && button.lastPush1) || stacker.setting.forceFeed_Dev) {
         dbgTmpWasFeeding = 1;
         stacker.reading.feedPressed = TRUE;
         
         if (0 == infeedTransport.homed) {
            ax = homeAxis(ax);
            lineController.command.beep = TRUE;
         }
         else if (inSpeed.is) {
            if (state == NotLoadedMode) {
               infeedSpeedSetByButton = TRUE;
               infeedTransport.speed = inSpeed.predict * 1.32 + 0.1;
            } 
            else {
               transportFloorSpeed[FEED].speed = inSpeed.predict * 1.32 + 0.1;
               transportFloorSpeed[FEED].timer = 0.1;
            }
         }
         else {
            if (state == NotLoadedMode) {
               infeedSpeedSetByButton = TRUE;
               infeedTransport.speed = 0.2;        
            }   
            else {
               transportFloorSpeed[FEED].speed = 0.2;
               transportFloorSpeed[FEED].timer = 0.1;
            }
         }
      }
      else ax = TRH;
      if ( btn1 && !button.lastPush1) { //rising edge
         if (doublePressTime) cutter.command.feed = TRUE;
         doublePressTime = 150;
      }
      if (!btn1 && button.lastPush1) { //falling edge
         cutter.command.feed = FALSE;
         if (infeedSpeedSetByButton) {
            infeedTransport.speed = 0;
            infeedSpeedSetByButton = FALSE;
         }
      }
   }
   
   if (lastTopCoverClosed != stacker.reading.input.topCoverClosed && !stacker.reading.input.topCoverClosed && cutter.command.feed) { 
      cutter.command.feed = FALSE;
   }
   /************************************** BUTTON 2 ***(TOGGLING WHITE,TOGGLING RED-YELLOW, YELLOW, GREEN)*/	
   if (stacker.reading.input.topCoverClosed) {
      if (btn2 && !button.lastPush2) {
         if (isButton(2,COLOR_GREEN)) {
            if (stacker.setting.forceOffset_Dev == 255)
               makeOffset = TRUE;
            else
               cutter.command.deliverNextRequest = TRUE;
         }
      }
   
      if (btn2 && ((isButton(2,COLOR_YELLOW)||isButton(2,COLOR_RED)) && state != ErrorMode)) unloadTimer += SECONDS_UNIT;
      else if (unloadTimer > 1.3 && unloadTimer < 1.595) unloadTimer += SECONDS_UNIT;
      else unloadTimer = 0;
   
      if (!stacker.reading.input.topCoverClosed) unloadTimer = 0; 
  
      if (unloadTimer>1.3) {
         safety.command.lockStackerTopCover = TRUE;
         unlockTimerCnt = 350;
      }
      if (unloadTimer>1.6) {
         safety.command.lockStackerTopCover = TRUE;
         unlockTimerCnt = 350;
         stopPlateX.absMove = stopPlateX.actualPositon/3;
         tableDown();
         state = NotLoadedMode;
         reHome = 1;
         fromError = FALSE;
         unloadTimer = 0;
      }
   }
   else if (btn2 && !button.lastPush2 && isButton(2,COLOR_YELLOW) && state == NotLoadedMode && lineController.setup.stacker.spaghettiBelts) {
      stackerXmove = TRUE;
   }
   /**************************************** BUTTON 3 ******************(RED,MAGENTA,BLUE,YELLOW)*/
   if (btn3 && !button.lastPush3) {
      if (isButton(3,COLOR_MAGENTA) && stacker.reading.input.topCoverClosed) {
         if (inSpeed.set == 0 && inSpeed.is < 0.02) {
            command.stoppedDelivery = ON;
            fromError = FALSE;
         }
         else {
            cutter.command.deliverNowRequestToggle = !cutter.command.deliverNowRequestToggle;
            strcpy(dbgPrintStr, "*S50 DELIVER NOW REQUEST TOGGLE (by button)");
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
         }
      }
      else if (isButton(3,COLOR_BLUE)) {
         if (safety.reading.eStop && safety.reading.resetPossible) {
            safety.command.eStopReset = 1;
         }
         if (drivePowerStatus == 0) drivePower(1);
         command.errorAcknowledge = TRUE; 
         transportFloorSpeed[0].timer = transportFloorSpeed[1].timer = 0;
         transportFloorSpeed[2].timer = transportFloorSpeed[3].timer = 0;
         transportFloorSpeed[4].timer = transportFloorSpeed[5].timer = 0;
         transportFloorSpeed[6].timer = transportFloorSpeed[7].timer = 0;
      }
      else if (isButton(3,COLOR_YELLOW) && !stacker.reading.input.topCoverClosed) {
         
         //if (gripper.reading.ready && lineController.setup.stacker.stackerGripperUnit1)   
         //   gripper.command.index = TRUE;
         /*else if (lineController.setup.stacker.spaghettiBelts) {
            //safety.command.bypassStackerTopCover = TRUE;
            fingerXmoved = TRUE;
            fingerXmove = TRUE;
         }*/
      }
   }
   
   
   /* button push log */   
   if (button.lastPush1 != btn1 && btn1) {
      stacker.reading.buttonPushLog.p++;
      if (stacker.reading.buttonPushLog.p>=20) stacker.reading.buttonPushLog.p = 0;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].button = 1;
      if (isButton(1,COLOR_RED)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = RED;
      else if (isButton(1,COLOR_YELLOW)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = YELLOW;
      else if (isButton(1,COLOR_GREEN)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = GREEN;
      else if (isButton(1,COLOR_ORANGE)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = ORANGE;
      else if (isButton(1,COLOR_WHITE)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = WHITE;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].state = state;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].timeStamp = lineController.reading.timeSinceStart;
   }
   if (button.lastPush2 != btn2 && btn2) {
      stacker.reading.buttonPushLog.p++;
      if (stacker.reading.buttonPushLog.p>=20) stacker.reading.buttonPushLog.p = 0;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].button = 2;
      if (isButton(1,COLOR_RED)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = RED;
      else if (isButton(1,COLOR_YELLOW)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = YELLOW;
      else if (isButton(1,COLOR_GREEN)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = GREEN;
      else if (isButton(1,COLOR_ORANGE)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = ORANGE;
      else if (isButton(1,COLOR_WHITE)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = WHITE;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].state = state;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].timeStamp = lineController.reading.timeSinceStart;
   }
   if (button.lastPush3 != btn3 && btn3) {
      stacker.reading.buttonPushLog.p++;
      if (stacker.reading.buttonPushLog.p>=20) stacker.reading.buttonPushLog.p = 0;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].button = 3;
      if (isButton(1,COLOR_RED)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = RED;
      else if (isButton(1,COLOR_YELLOW)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = YELLOW;
      else if (isButton(1,COLOR_GREEN)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = GREEN;
      else if (isButton(1,COLOR_ORANGE)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = ORANGE;
      else if (isButton(1,COLOR_BLUE)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = BLUE;
      else if (isButton(1,COLOR_MAGENTA)) stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].color = MAGENTA;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].state = state;
      stacker.reading.buttonPushLog.data[stacker.reading.buttonPushLog.p].timeStamp = lineController.reading.timeSinceStart;
   }
	
   if (stackerXmove && state == NotLoadedMode && !commandStatus.loading && !commandStatus.findingZero) {
      if (stackerY.actualPositon>10) stackerY.absMove = stackerY.actualPositon - 10; 
      stackerXmove = FALSE;
   }
   
   button.lastPush1 = btn1;
   button.lastPush2 = btn2;
   button.lastPush3 = btn3;
   lastTopCoverClosed = stacker.reading.input.topCoverClosed;
}

void quarterSecondCycle (void)
{
	if (buttonLedTest <= 4) {
		stacker.reading.status.stopped = FALSE;
		if (buttonLedTest<1) {
			setButton(1,COLOR_WHITE);
			setButton(2,COLOR_WHITE);
			setButton(3,COLOR_BLUE);
		}
		else if (buttonLedTest<2) {
			setButton(1,COLOR_RED);
			setButton(2,COLOR_RED);
			setButton(3,COLOR_RED);
		}
		else if (buttonLedTest<3) {
			setButton(1,COLOR_YELLOW);
			setButton(2,COLOR_YELLOW);
			setButton(3,COLOR_YELLOW);
		}
		else {
			setButton(1,COLOR_GREEN);
			setButton(2,COLOR_GREEN);
			setButton(3,COLOR_GREEN);
			if (buttonLedTest == 4) lineController.command.beep = TRUE;
		}
		buttonLedTest++;
		return;
	}
	else if (FALSE == safety.reading.initialized) {
		setButton(1,COLOR_OFF);
		setButton(2,COLOR_OFF);
		setButton(3,COLOR_OFF);
		return;
	}

	if (state == ReadyMode) {
		if ((*sStop[readSoftStopStatus])(BTN_PRESS)) {
			if (!fromError) {
				if (stopDeliverToExit) setButton(1,COLOR_OFF);
				else setButton(1,COLOR_WHITE);
			}
			else {
				if (stackerIsClear) {
					if (stopDeliverToExit) setButton(1,COLOR_OFF);
					else setButton(1,COLOR_WHITE);
				}
				else setButton(1,COLOR_OFF);
			}	
            
			if (isButton(2,COLOR_RED)) setButton(2,COLOR_YELLOW);
			else setButton(2,COLOR_RED);
            
			setButton(3,COLOR_MAGENTA);	
			stacker.reading.status.stopped = TRUE;
            
			if (!stacker.reading.input.topCoverClosed) {
				setButton(1,COLOR_YELLOW);
				setButton(2,COLOR_OFF);
                setButton(3,COLOR_OFF);
            //if ((stacker.reading.stackSize < stacker.setting.tableSteps) && (lineController.reading.guiControlLevel > 1)) setButton(3,COLOR_YELLOW);
            //else setButton(3,COLOR_OFF);
			}
		}
		else {
			setButton(1,COLOR_RED);
         
			if (deliverFly.pendingDeliver || cutter.command.deliverNextRequest) setButton(2,COLOR_OFF);
			else setButton(2,COLOR_GREEN);
			//if ((stackerY.maxPos + stacker.setting.topPosOffset /*GUI.settings.S50_topPosition*/ - stackerY.pos) > stacker.setting.minStackSize) {
         if ((stacker.setting.topPosOffset - stackerY.pos) > stacker.setting.minStackSize) {
				if (isButton(2,COLOR_GREEN)) cutter.command.deliverNextRequest = TRUE;
				setButton(2,COLOR_OFF);
			}
			if (deliverFly.pendingDeliver || (lineController.setup.buffer.installed && (buffer.reading.usage > 25.0))) setButton(3,COLOR_OFF); 
			else setButton(3,COLOR_MAGENTA);	
			stacker.reading.status.stopped = FALSE;
			if (!stacker.reading.input.topCoverClosed)
			{
				setButton(2,COLOR_OFF);
				setButton(3,COLOR_OFF);
			}
		}
		buttonState = 1;
      
     
      
	}
	else if (state == StoppedDelivery) {
		buttonState = 2;
		setButton(1,COLOR_OFF);
		setButton(2,COLOR_OFF);
		setButton(3,COLOR_OFF);
	}
	else if (state == NotLoadedMode && (commandStatus.loading || commandStatus.findingZero)) {
		buttonState = 3;		
      //button2WasWhite = 0;
		setButton(1,COLOR_OFF);
		setButton(2,COLOR_OFF);
		setButton(3,COLOR_OFF);
		stacker.reading.status.stopped = FALSE;
	}
	else if (state == ErrorMode) {
		buttonState = 4;
      //button2WasWhite = 0;
		stacker.reading.status.stopped = FALSE;
		if (!stacker.reading.input.eStop /*eStop.S50_Active*/) {
			if (isButton(1,COLOR_RED)) {
				setButton(1,COLOR_OFF);
				setButton(2,COLOR_RED);
			}
			else {
				setButton(1,COLOR_RED);
				setButton(2,COLOR_OFF);
			}
		}
		else 
		{		
			if (isButton(1,COLOR_RED)) {
				setButton(1,COLOR_OFF);
				setButton(2,COLOR_OFF);
			}
			else {
				setButton(1,COLOR_RED);
				setButton(2,COLOR_RED);
			}
		}
		if (safety.reading.resetPossible) setButton(3,COLOR_BLUE);
		else setButton(3,COLOR_OFF);
	}
	
	if (state == NotLoadedMode && commandStatus.loading == OFF) { // not loaded, and green not pressed
		stacker.reading.status.stopped = FALSE;
		setButton(1,COLOR_WHITE);
        setButton(2,COLOR_OFF);
        setButton(3,COLOR_OFF);
        
		if (!stacker.reading.input.topCoverClosed) {
         if (globalPower != 0) setButton(1,COLOR_YELLOW);
         else setButton(1,COLOR_OFF);
         if (lineController.setup.stacker.spaghettiBelts /*&& stackerY.homed */&& !stacker.reading.input.homeElevator) setButton(2,COLOR_YELLOW);
         else setButton(2,COLOR_OFF);
			setButton(3,COLOR_OFF);
		}
	}
   
   if (isButton(1,COLOR_WHITE)) {
                                    
      if (state == NotLoadedMode)       stacker.reading.btnFunc1 = 1;        //load 
      else                              stacker.reading.btnFunc1 = 2;        //start
   }  
   else if (isButton(1,COLOR_RED)) {
      if (state != ErrorMode)           stacker.reading.btnFunc1 = 3;        //stop
      else                              stacker.reading.btnFunc1 = 0;        //nothing
   }
   else if (isButton(1,COLOR_YELLOW))   stacker.reading.btnFunc1 = 4;        //feed aligner
   else                                 stacker.reading.btnFunc1 = 0;        //nothing
   
   if (isButton(2,COLOR_GREEN))         stacker.reading.btnFunc2 = 1;        //deliver next
   else if (isButton(2,COLOR_RED) || isButton(2,COLOR_YELLOW)) {
      if (isButton(2,COLOR_YELLOW) && !stacker.reading.input.topCoverClosed)
                                        stacker.reading.btnFunc2 = 2;        //table step down
      else if (state != ErrorMode)      stacker.reading.btnFunc2 = 3;        //unload
      else                              stacker.reading.btnFunc2 = 0;        //nothing
   }
   else                                 stacker.reading.btnFunc2 = 0;        //nothing
   
   if (isButton(3,COLOR_MAGENTA))       stacker.reading.btnFunc3 = 1;        //deliver
   else if (isButton(3,COLOR_BLUE))     stacker.reading.btnFunc3 = 2;        //error ack.
   else if (isButton(3,COLOR_YELLOW))   stacker.reading.btnFunc3 = 3;        //fork step
   else                                 stacker.reading.btnFunc3 = 0;        //nothing   

   
}

void fingerMoveOut(REAL mm)
{
	fingerX.move = mm;
	setAxis();
}

void tableDown(void)
{
	if (deliverFly.stackingOnFingers) fingerY.absMove = 0;
	stackerY.absMove = stacker.setting.deliveryPosition;
	if (state != ReadyMode) setAxis();
}

BOOL errorState(BOOL * continueLoaded)
{
	//static USINT errAckCnt = ERR_ACK_CNT;
	// static BOOL enterErrorState = TRUE;
	command.load = 0;
	command.stoppedDelivery = 0;
	*continueLoaded = FALSE;
   
	//if (enterErrorState) { 
	//   if (makeUnloaded())  {
	if (errorTableDown) tableDown();
	errorTableDown = FALSE;
	//      enterErrorState = FALSE;
	//   }
	//}
	delayIt = 0;
	if (machineError.active == FALSE) //some one cleared the error from GUI
	{
		command.errorAcknowledge = TRUE;
	}
	if (command.errorAcknowledge)
	{
		deliverFly.state = 0; //reset deliver on fly
		deliverFly.pendingDeliver = 0;
		fromError = TRUE;
        deliverFly.inProgress = FALSE;
		pageInTransportQueue(INIT_QUEUE);
		if (errResetCnt%3 == 0) errorAckAll(0);
		if (errResetCnt%8 == 4 && errorByS50Estop) {
			safety.command.eStopReset = TRUE;
			safety.command.resetStackerFork = TRUE;
			safety.command.resetStackerLightCurtain = TRUE;
		}
		if (!safety.reading.eStop) errorByS50Estop = 0;
		if (stacker.reading.axisStatus.forkElevator.errorID>0) {
			safety.command.resetStackerFork = TRUE;
			stacker.reading.axisStatus.forkElevator.homeState = 0;
		}
		else if (stacker.reading.axisStatus.forkPush.errorID>0) {
			safety.command.resetStackerFork = TRUE;
			stacker.reading.axisStatus.forkPush.homeState = 0;
		}
		deliverFly.stackingOnFingers = FALSE;
		if (errResetCnt == 0) {
			if (makeUnloaded()) {
				loadingState = initial;
				state = NotLoadedMode;
			}
			else {
				*continueLoaded = TRUE; //TODO,, fix this so we can continiue loaded only wastebox full now gives continiue loaded.. test
				//*continueLoaded = FALSE;
			}   //go ready state = ReadyMode;
			clearErrors();
			// axisError
			errResetCnt = ERR_ACK_CNT;
			machineError.active = FALSE;
			command.errorAcknowledge = FALSE;
            stackerCounter.errorAcknowlages++;
			//lightCurtainResetCnt = 100;
			safety.command.resetStackerFork = TRUE; //Test
			safety.command.resetStackerLightCurtain = TRUE;	
			errorJustCleared = 100;
			if (stackerY.pos == stacker.setting.deliveryPosition) state = NotLoadedMode;
			return TRUE;
		}    
		errResetCnt--;
		infeedTransport.speed = 0;
        if (stacker.command.coverFault)
        {
            stacker.command.coverFault = FALSE;
            if (safety.reading.resetPossible) safety.command.eStopReset = TRUE; 
        }
		//return TRUE;
	}

	/*
	if (checkPendingError() <= NORMAL_STOP)
	{
		infeedTransport.speed = overSpeedSimple(,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_ZERO);
	}
	else
		infeedTransport.speed = 0;
	*/
	
	infeedTransport.speed = inSpeed.is * 2;
	
	deliverTableBelt.speed = 0;
	stackerY.move = 0;
	fingerY.move = 0;
	//STACK_DELIVER_GRIPPER = OFF;
	//setOutput(&stackDeliverGripper,OFF);
	output.stackDeliverGripper = FALSE;
	setAxis();
	return FALSE;
}

BOOL ready (void)
{
    setAxis();			            // set axis commands
	if (checkPendingError()>NO_ERROR)
	{
		resetTransport();	        // stop transport
		
		if (deliverFly.state == 5) deliverFly.debug_6 = deliverFly.debug_6+1; 
		return FALSE; 		         // leave ReadyMode -> ErrorMode
	}
	else {
      ballrack();                // check ballrack                                        generates error -> BALLRACK
      transportEnter(); 	      // check transport enter sensor                          generates error -> TRANSPORT_INFEED
      transportExit();           // check transport exit sensor                           generates error -> TRANSPORT_OUTFEED (Aligner outfeed crash)
      sheetSignal(); 		      // new page ? add to page queue, starts deliver on fly.  generates error -> QUEUE
      //offsetSignal();
      runTransport();            // run stacker infeed 										
      stackerStepSpaghetti();    // step down stacker                                     generates error -> STACKER
      deliverOnTheFly();	      // deliver on the fly,(waitOnStart = idle)               generates error -> FLY_DELIVER 
      //stackerStepSpaghetti();    // step down stacker                                     generates error -> STACKER
      evalSoftStop();            // evaluate soft stop, set incomming speed to 0 if it is
      checkWasteBox();           //                                                       generates error -> WASTE_BOX_FULL
		//checkInfeedCrash();      //                                                       generates error -> STACKER_INFEED_CRASH
      controlTableStepsButton(); //handels +/- tablesteps
      checkGripperReady();       //                                                       generates error -> GRIPPER_NOT_READY 
      checkAxis();               //                                                      
      checkPower();              //exit to Not Loaded
      checkCover();              //                                                       generates error -> COVER OPEN                     
      checkSheetSequence();      //                                                       generates error -> TRANSPORT_INFEED/OUTFEED (2)
      standStillTimer();
	}
	return TRUE;
}

void standStillTimer(void)
{
   static UDINT lastTime;
   static USINT standStillTimer;
   if (inSpeed.set < 0.01) {
      if (lineController.reading.timeSinceStart != lastTime) {
         if (standStillTimer<255) standStillTimer++;
         else curlDistance = 30; //30 meters of curl
      } 
   }
   else
      standStillTimer = 0;

   if (cutter.reading.processingWhite) curlDistance = 0;
   lastTime = lineController.reading.timeSinceStart;
}

void checkSheetSequence(void)
{
   if (sheetSeqErrorEnter> 2) {
      addError(TRANSPORT_INFEED,NORMAL_STOP,FORCE_RELOAD,3);
      sheetSeqErrorEnter = sheetSeqErrorExit = 0;
   }
   if (sheetSeqErrorExit > 3) {
      addError(TRANSPORT_OUTFEED,NORMAL_STOP,FORCE_RELOAD,3);
         sheetSeqErrorEnter = sheetSeqErrorExit = 0;
   }
}

void checkCover(void)
{
	if (!stacker.reading.input.topCoverClosed) {
      if (stacker.reading.feedPressed) {
         if (deliverFly.pendingDeliver) {
		    addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,5);
			reHome = 1;
	     }
      }
      else {
         if      (stacker.reading.axisStatus.deliver.actualSpeed > 0.1)   addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,1);
         else if (stacker.reading.axisStatus.transport.actualSpeed > 1.0) addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,2);      
         else if (stackerMoving() && !fingerXmoved)                       addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,3);
         else if (deliverFly.pendingDeliver)                              addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,4);
		 reHome = 1;
      }
	}
}

void checkPower(void)
{
	if (FALSE == globalPower && (!(stacker.setting.ignoreError_Dev & 0x01))) {
	   state = NotLoadedMode;
	   reHome = 1;
	}
}

void checkAxis (void)
{
   if (stacker.reading.axisStatus.stackerElevator.errorID>0) {
      if (!stacker.reading.input.lightCurtain) {
         addError(LIGHT_CURTAIN,NORMAL_STOP,FORCE_RELOAD,4);
		 reHome = 1;
       }
      else {
         addError(AXIS,NORMAL_STOP,FORCE_RELOAD,SLIFT);
		 reHome = 1;
	  }
   }
	if (stacker.reading.axisStatus.endStop.errorID>0 && stacker.setting.automaticEndStop) {
	   addError(AXIS,NORMAL_STOP,FORCE_RELOAD,STOPPL);
	   reHome = 1;
	}
	if (stacker.reading.axisStatus.forkElevator.errorID>0) {
	   addError(AXIS,NORMAL_STOP,FORCE_RELOAD,FLIFT);
	   reHome = 1;
	}
	if (stacker.reading.axisStatus.forkPush.errorID>0) {
	   addError(AXIS,NORMAL_STOP,FORCE_RELOAD,FPUSH);
	   reHome = 1;
	}
	if (stacker.reading.axisStatus.deliver.errorID>0) {
	   addError(AXIS,NORMAL_STOP,FORCE_RELOAD,DELIV);
	   reHome = 1;
	}
	if (stacker.reading.axisStatus.transport.errorID>0) {
	  addError(AXIS,NORMAL_STOP,FORCE_RELOAD,TRANS);
	  reHome = 1;
	}
}

void checkGripperReady(void)
{
	if (!stacker.reading.input.readyGripper) {
		addError(GRIPPER_NOT_READY,NORMAL_STOP,FORCE_RELOAD,0);
		reHome = 1;
	}
}

void controlTableStepsButton(void)
{
	if (stacker.command.tableStepsTighter) {
		stacker.setting.tableSteps -= 0.005;
		stackerY.move = (0.005*step.stackPages);
	}
	if (stacker.command.tableStepsLooser) {
		stacker.setting.tableSteps += 0.005;
		stackerY.move = -(0.005*step.stackPages);
	}
	if (stacker.command.tableStepsAvg) {
		//F50BeepS = TRUE;
		folder.command.shortBeep = TRUE;
      if (lineController.setup.stacker.spaghettiBelts) stacker.setting.tableSteps = spaghettiStep.average;
		//GUI.settings.S50_tableSteps = step.avgStep;
	}
	stacker.command.tableStepsAvg = 0;
	stacker.command.tableStepsLooser = 0;
	stacker.command.tableStepsTighter = 0;
}

void checkWasteBox(void)
{
   if (stacker.setting.ignoreError_Dev & 0x04) return;
	static UINT wasteBoxFullCnt;	
	//static UINT wasteBoxWarningCnt;	
	//static USINT warningBeep;
	//static USINT firstBeeps;
   
   if (!stacker.reading.input.wasteBoxFull)     wasteBoxFullCnt++;
   if (!stacker.reading.input.wasteBoxWarning)  wasteBoxFullCnt++;   
   if (stacker.reading.input.wasteBoxWarning && stacker.reading.input.wasteBoxFull) wasteBoxFullCnt = 0;
   
   if (wasteBoxFullCnt > 400) {
      warnings[WASTEBOXWARNING] = TRUE;
   }
   if (wasteBoxFullCnt>1000 && cutter.reading.output.reject && cutter.reading.webSpeed > 20) addError(WASTE_BOX_FULL,NORMAL_STOP,CONTINIUE_LOADED,2);
	
	/*if (!stacker.reading.input.wasteBoxFull && !stacker.reading.input.wasteBoxWarning) {
        if (cutter.reading.output.reject) wasteBoxFullCnt += 3;
        else wasteBoxFullCnt = 0;
		wasteBoxWarningCnt +=2;
	}
	if (stacker.reading.input.wasteBoxFull && stacker.reading.input.wasteBoxWarning) wasteBoxFullCnt = 0;
	if (!stacker.reading.input.wasteBoxFull && stacker.reading.input.wasteBoxWarning && wasteBoxFullCnt>0) wasteBoxFullCnt--;
	if (!stacker.reading.input.wasteBoxWarning) wasteBoxWarningCnt++;		
	else wasteBoxWarningCnt = 0;
	
	if (wasteBoxWarningCnt > 1200) {
		emptyWasteBox = 0x0000; //show message
		//stacker.reading.status.warning = TRUE;
        warnings[WASTEBOXWARNING] = TRUE;
	}
	else {
		emptyWasteBox = 0x0001; //hide message		
		//stacker.reading.status.warning = FALSE;
        warnings[WASTEBOXWARNING] = FALSE;
	}
	if (wasteBoxFullCnt>2400) addError(WASTE_BOX_FULL,NORMAL_STOP,CONTINIUE_LOADED,2);

	if (wasteBoxWarningCnt>1200 ) {
		if (warningBeep == 0 && !firstBeeps) {
			//F50BeepS = TRUE;
			folder.command.shortBeep = TRUE;
		}
		if (warningBeep == 0 && firstBeeps) {
			//F50Beep = TRUE;
			folder.command.beep = TRUE;
			firstBeeps--;
		}
		
		warningBeep++;
		if (warningBeep>200) warningBeep = 0;
	}
	else {
		firstBeeps = 2;
	}*/
}

void ballrack (void)
{
	/*if (!BALLRACK) (*sStop[holdSoftStop])(BRACK);
	else (*sStop[releaseSoftStop])(BRACK);*/
	
	if (!BALLRACK) {
		if (infeedTransport.speed > 0.01)
			addError(ALIGNER_ROLLER_GATE,NORMAL_STOP,FORCE_RELOAD,2);
		else if (bStop == FALSE) {
			bStop = TRUE;
			stopDeliverToExit = TRUE;
		}
	}
	if (!stacker.reading.input.infeedGate) {
		if (infeedTransport.speed > 0.01)
			addError(INFEED_ROLLER_GATE,NORMAL_STOP,FORCE_RELOAD,2);
		else if (bStop == FALSE)
			bStop = TRUE;
	}
}

void transportEnter(void)
{
	static BOOL lastTransportTableEnter;
	static REAL coverdTimeError;
	static USINT queueErrorCnt;
	
	realMinMaxCheck(&stacker.setting.transportTableSpeed,0.05,3.5);
	speedDelay(ADD,SECONDS_UNIT,max(inSpeed.is,inSpeed.set));
   
   sinceFrontEdgeTransportEnter += (infeedTransport.actualSpeed * SECONDS_UNIT)*1000; //fiserv
	
   if (!TRANSPORT_TABLE_ENTER_1) {
		if (lastTransportTableEnter) {
            sheetSequencCheck=0; //rising edge
            sheetSeqErrorEnter=0;
            //strcpy(dbgPrintStr, "*S50 new front edge seen pIT:");
            //strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue(GET_NO_PAGES_IN_QUEUE)),tBuf));
            //strcat(dbgPrintStr, " pip:");
            //strcat(dbgPrintStr, uinttostr((unsigned long)(pageInPocket),tBuf));
            //strcat(dbgPrintStr, "\0");
            //strcpy(ULog.text[ULog.ix++], dbgPrintStr);
			   //lastSpeed = transportFloorSpeed[ENTER_TRANSPORT].speed; 
			   if (pageInPocket) pageInPocket = 0;
            else {
               strcpy(dbgPrintStr, "*S50 unannounced page enter stacker pIT:");
               strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue(GET_NO_PAGES_IN_QUEUE)),tBuf));
               strcat(dbgPrintStr, "\0");
               strcpy(ULog.text[ULog.ix++], dbgPrintStr);
            }
         sinceFrontEdgeTransportEnter = 0;  //fiserv  
		}
		lastTransportTableEnter = 0;
		if (inSpeed.is > 0) coverdTimeError += SECONDS_UNIT;
		
		if (coverdTimeError > (7.0 - inSpeed.is)) {
			addError(TRANSPORT_INFEED,NORMAL_STOP,FORCE_RELOAD,0);
			errorTableDown = TRUE;
			coverdTimeError=0;
		}
		if (!(inSpeed.is == 0 && inSpeed.set == 0 && (buffer.reading.usage<20 || !machineStarted))) {
			if (stacker.setting.highSpeedDelay<0.1)
				transportFloorSpeed[ENTER_TRANSPORT].speed = overSpeedSimple (inSpeed.set>inSpeed.is ? ((inSpeed.set+inSpeed.is*2)/3):inSpeed.is,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
			else
				transportFloorSpeed[ENTER_TRANSPORT].speed = overSpeedSimple (speedDelay(FETCH,stacker.setting.highSpeedDelay,0),stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
			transportFloorSpeed[ENTER_TRANSPORT].timer = tRtime((TRANSPORT_TABLE_LENGTH + JobControlExit.sheetSize + 50),transportFloorSpeed[ENTER_TRANSPORT].speed,1.0);
		}	
	}
	else {
		if (firstTimeAfterLoad) queueErrorCnt = 0;
		lastTransportTableEnter = 1;
		if (coverdTimeError) pageCrashCnt = 0;
		coverdTimeError=0;
		//uncoverdTime += SECONDS_UNIT;
	}
	if (pageCrashCnt>3) {
		addError(TRANSPORT_INFEED,NORMAL_STOP,FORCE_RELOAD,1);
		errorTableDown = TRUE;
		pageCrashCnt = 0;
	}
	firstTimeAfterLoad = 0;
	//lastTransportTableEnter = TRANSPORT_TABLE_ENTER_1;
}

REAL overSpeedSimple(REAL v,REAL i,REAL o, BOOL s)
{
	REAL x;
	x = v*o;
	if (GO_TO_ZERO == s) x = (x<i?(v<0.015?0:i):x);
	else x = x<i?i:x;
	return x;
}

REAL overSpeedSimpleInv(REAL sV,REAL o)
{
	REAL x;
	x = sV/o;
	return x;
}

void transportExit(void)
{
    if (!BALLRACK) return;
    static REAL coverdTimeError;
    static REAL frontEdgePosition;
    static REAL backEdgePosition;
    static BOOL lastPageDeliver;
    static USINT standStillCnt;
    static USINT toggleOffsetCnt = 0;
    static REAL backEdgeInStack;
    static USINT gapTime;
    static USINT delayPage=0;
    //static UDINT goBack;
    //static BOOL offsetDone;
    

    if (infeedTransport.speed==0) {
        if (standStillCnt<250) standStillCnt++;
    }
    else standStillCnt = 0;
    if (standStillCnt>=250) return; //ignore the sensor if we are standing still
	
    if (FALSE == lineController.setup.stacker.spaghettiBelts) {//handle offset when using grippers
        if (toggleOffsetCnt == 1) {
            stackerCounter.offsets++;
            offsetControl(OFFSET_TOGGLE);	
        }
        if (toggleOffsetCnt > 0) toggleOffsetCnt--;
    }
   
    realMinMaxCheck(&stacker.setting.transportTableSpeed,0.05,3.0);

    if (!TRANSPORT_TABLE_EXIT_1) {
        //FRONT EDGE
        if (lastTransportTableExit != TRANSPORT_TABLE_EXIT_1) {//front edge of sheet
            frontEdgePosition = infeedTransport.actualPosition;
            sheetSeqErrorExit = 0;
            //coveredTime = 0;
            //measured gap
            exitTrans.measuredGap = frontEdgePosition - backEdgePosition;
            if (lastPageDeliver) {
                //sourceS50.deliverGapTime = mainTick - sourceS50.deliverGapTime;
                stacker.reading.deliverGapTime = mainTick - stacker.reading.deliverGapTime;
                strcpy(dbgPrintStr, "*S50 deliver sheet gap:");
                strcat(dbgPrintStr, uinttostr((unsigned long)(stacker.reading.deliverGapTime*1000),tBuf));
                strcat(dbgPrintStr, " ms\0");
                strcpy(ULog.text[ULog.ix++], dbgPrintStr);
                lastPageDeliver = 0;
            
                if (stacker.reading.deliverGapTime<0.80) {
                    delayIt = 1;
                }
            }
            
            if (pageInTransportQueue(PEEK_PAGE) == OFFSET_PAGE)
            {
                if (inSouth == stacker.reading.offsetPosition) stacker.reading.offsetState = DWELL_SOUTH;
                else stacker.reading.offsetState = DWELL_NORTH;
            }
            
            if (stacker.setting.delayOffsetPage > 0)
            {
                if (delayPage) {
                    delayPage--;   
                    if (0 == delayPage) toggleOffsetCnt = 8;
                }
                if (pageInTransportQueue(PEEK_PAGE) == OFFSET_PAGE) {
                    delayPage = stacker.setting.delayOffsetPage;
                    //delayPage = stacker.setting.delayOffsetPage + (inSouth == stacker.reading.offsetPosition?(stacker.setting.delayNorthOffset?1:0):0);
                    if (!deliverFly.stackingOnFingers && inSouth == stacker.reading.offsetPosition && lineController.setup.stacker.spaghettiBelts)
                    {
                        fingerX.acceleration              = 6.0; //speed up finger move
                        fingerX.deceleration              = 6.0;
                        fingerX.speed                     = 2.5;
                        getFingerX();
                        fingerX.absMove = FINGER_START_POS_SPAGHETTI+FORKS_TO_NORTH_POS;    
                    }
                }
            }
            else if (stacker.setting.delayOffsetPage==0) {
                if (pageInTransportQueue(PEEK_PAGE) == OFFSET_PAGE) {
                    toggleOffsetCnt = 8;
                    
                }
            }
        }
        else {
         
        }	
        if (infeedTransport.actualSpeed > 0.05) coverdTimeError += SECONDS_UNIT;
		
        if (coverdTimeError > ((3.2 + JobControlExit.sheetSize*0.002) - infeedTransport.speed/2) && !(stacker.setting.ignoreError_Dev & 0x02)) {
            addError(TRANSPORT_OUTFEED,CRASH_STOP,FORCE_RELOAD,0);
            errorTableDown = TRUE;
            coverdTimeError = 0;
        }
        if (stacker.setting.highSpeedDelay < 0.1) {
            transportFloorSpeed[EXIT_TRANSPORT].speed = overSpeedSimple (inSpeed.is,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
            transportFloorSpeed[EXIT_TRANSPORT].timer = tRtime((TRANSPORT_EXIT_TO_STACK + JobControlExit.sheetSize + 100),transportFloorSpeed[EXIT_TRANSPORT].speed,0.5);
        }
        else {
            transportFloorSpeed[EXIT_TRANSPORT].speed = (overSpeedSimple(speedDelay(FETCH,stacker.setting.highSpeedDelay,0),stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED)+infeedTransport.speed*3)/4;
            transportFloorSpeed[EXIT_TRANSPORT].timer = tRtime((TRANSPORT_EXIT_TO_STACK + JobControlExit.sheetSize + 100),transportFloorSpeed[EXIT_TRANSPORT].speed,0.5);
        }
        distTransportCalc(ADD);
        if (gapTime>0 && gapTime<3 && !(stacker.setting.ignoreError_Dev & 0x02)) addError(TRANSPORT_OUTFEED,CRASH_STOP,CONTINIUE_LOADED,2);
        gapTime = 0;
        //sheetTime++;
    }
    else {
        coverdTimeError = 0;
        if (lastTransportTableExit != TRANSPORT_TABLE_EXIT_1) { //back edge of sheet
            backEdgePosition = infeedTransport.actualPosition;
            exitTrans.measuredSize = backEdgePosition - frontEdgePosition;
            if ((exitTrans.measuredSize) > (JobControlExit.sheetSize * 3.0) && !(stacker.setting.ignoreError_Dev & 0x02)) {
                addError(TRANSPORT_OUTFEED,CRASH_STOP,FORCE_RELOAD,1);
                errorTableDown = TRUE;
                frontEdgePosition = infeedTransport.actualPosition;
            }
         /*else if (sheetTime>0 && sheetTime < (USINT)( 6 + (JobControlExit.sheetSize*0.015 - infeedTransport.speed)) )  {
            addError(TRANSPORT_OUTFEED,CRASH_STOP,FORCE_RELOAD,4);
         }*/
        }
        if (gapTime<255) gapTime++;
        //sheetTime = 0;
    }
    //BACK EDGE
    if (lastTransportTableExit != TRANSPORT_TABLE_EXIT_1 && TRANSPORT_TABLE_EXIT_1) { //rising edge (sheet back edge)
        distSLp++;
        if (distSLp>9) distSLp = 0;
        sheetLengts[distSLp] = distTransportCalc(FETCH);
        distTransportCalc(SETUP);
      
        if (pageInTransportQueue(PEEK_PAGE) == DELIVER_PAGE) {
            strcpy(dbgPrintStr, "*S50 next dp leaving exit sensor pIT:");
            strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue (GET_NO_PAGES_IN_QUEUE)),tBuf));
            strcat(dbgPrintStr, "\0");
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
            stacker.reading.deliverGapTime = mainTick;
            lastPageDeliver = 1;
        }
        if (pageInTransportQueue(PEEK_PAGE) == FAKE_DELIVER_PAGE) {
            strcpy(dbgPrintStr, "*S50 next fdp leaving exit sensor pIT:");
            strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue (GET_NO_PAGES_IN_QUEUE)),tBuf));
            strcat(dbgPrintStr, "\0");
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
            stacker.reading.deliverGapTime = mainTick;
            lastPageDeliver = 1;
        }
        if (inQueue == 0) {
            strcpy(dbgPrintStr, "*S50 queue error (nothing in queue)");
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
        }
        pageInTransportQueue(REMOVE_LAST_PAGE);
        //if (delayOffsetPage==1) {
        //if (pageInTransportQueue(PEEK_PAGE) == OFFSET_PAGE) toggleOffsetCnt = 1;
        //}
      
        if (step.stackPages==0) backEdgeInStack = 0.215 - (infeedTransport.actualSpeed * infeedTransport.actualSpeed / deliverTableBelt.deceleration);
        step.stackPages++;
        step.newPageInStack=1;

        //early start of finger out test
        if (1 == delayPage && !deliverFly.stackingOnFingers && inSouth == stacker.reading.offsetPosition && lineController.setup.stacker.spaghettiBelts)
        {
            fingerX.acceleration              = 6.0; //speed up finger move
            fingerX.deceleration              = 6.0;
            fingerX.speed                     = 2.5;
            getFingerX();
            fingerX.absMove = FINGER_START_POS_SPAGHETTI+FORKS_TO_NORTH_POS;
            
        }

    }
   
    if (step.stackPages == 0)
        stacker.reading.stackTableEmpty = TRUE;  
    else if (step.stackPages < 3) {
        backEdgeInStack -= (infeedTransport.actualSpeed * SECONDS_UNIT);
        if (backEdgeInStack<0) stacker.reading.stackTableEmpty = FALSE;
        else stacker.reading.stackTableEmpty = TRUE;
    }
    else 
        stacker.reading.stackTableEmpty = FALSE;  
   
    /*---------------------------------------------SPAGHETTI------------------------------------------------------*/    
    if (lineController.setup.stacker.spaghettiBelts) { //handle offset when using spaghetti belts

        switch (stacker.reading.offsetState)
        {
        case DWELL_SOUTH: //0
            //in south //idleing
            if (toggleOffsetCnt)
            {
                lineController.command.shortBeep = TRUE;
                stacker.reading.timeToToggleHoldBack = 150/(infeedTransport.actualSpeed) - 15;
                toggleOffsetCnt = 0;
                stacker.reading.offsetState = WAIT_HOLDBACK_FORK_IN_POS;
                stacker.reading.offsetStateTimer = stacker.reading.timeToToggleHoldBack;
                if (!deliverFly.stackingOnFingers) {
                    fingerX.acceleration              = 8.5; //speed up finger move
                    fingerX.deceleration              = 8.5;
                    fingerX.speed                     = 2.5;
                    getFingerX();
                    fingerX.absMove = FINGER_START_POS_SPAGHETTI+FORKS_TO_NORTH_POS;
                }
            }
            break;
        case WAIT_HOLDBACK_FORK_IN_POS: //1
            stacker.reading.offsetStateTimer -= MILLISECONDS_UNIT;
            if (stacker.reading.offsetStateTimer < 0)
            {
                holdBackControl(OFFSET_TOGGLE); 
                stacker.reading.timeToToggleStopPlate = 60/(infeedTransport.actualSpeed) - 30;
                stacker.reading.offsetState = WAIT_STOPPLATE_HOLDBACK_IN_POS;
                stacker.reading.offsetStateTimer = stacker.reading.timeToToggleStopPlate;
            }
            break;
        case WAIT_STOPPLATE_HOLDBACK_IN_POS: //2
            stacker.reading.offsetStateTimer -= MILLISECONDS_UNIT;
            fingerX.acceleration              = 2.0;
            fingerX.deceleration              = 2.0;
            fingerX.speed                     = 1.5;
            if (stacker.reading.offsetStateTimer < 0)
            {
                offsetControl(OFFSET_TOGGLE);
                stacker.reading.offsetState = WAIT_FORKBACK_STOPPLATE_IN_POS;
                stacker.reading.goBackPage = stacker.reading.stackPages + 3;
            }
            break;
        case WAIT_FORKBACK_STOPPLATE_IN_POS: //3
            if (stacker.reading.goBackPage == stacker.reading.stackPages)
            {
                if (!deliverFly.stackingOnFingers) {
                    fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                }
                stacker.reading.offsetState = DWELL_NORTH;
            }
            break;
        case DWELL_NORTH: //4
        if (toggleOffsetCnt)
            {
                lineController.command.doubleBeep = TRUE;
                holdBackControl(OFFSET_TOGGLE);
                toggleOffsetCnt = 0;
                stacker.reading.offsetState = MOVE_STOPPLATE_TO_SOUTH;
                stacker.reading.timeToToggleStopPlate = 20;
                stacker.reading.offsetStateTimer = stacker.reading.timeToToggleStopPlate;
            }
            break;
        case MOVE_STOPPLATE_TO_SOUTH: //5
            stacker.reading.offsetStateTimer -= MILLISECONDS_UNIT;    
            if (stacker.reading.offsetStateTimer < 0)
            {
                offsetControl(OFFSET_TOGGLE);
                stacker.reading.offsetState = DWELL_SOUTH;
            }
            break;
        case 6:
        break;
        case 7:
        break;
        }
        //
        
        /*llllllllllllllllllll
        
        if (toggleOffsetCnt) { //reset wait distance, load offset
            toggleOffsetCnt = 0;
            holdBackOffsetTimer=0.0;
            offsetTimer=0.0;
            holdBackDone = 0; //flag
            offsetDone = 0; //flag
            stackerCounter.offsets++;
            if (inNorth == stacker.reading.offsetPosition)
            {
                //move fingers back
                if (!deliverFly.stackingOnFingers)
                    fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                
                if (infeedTransport.actualSpeed != 0) {
                    stacker.reading.timeToToggleHoldBack = (stacker.setting.offset.inNorthDelayDistHoldBack/(infeedTransport.actualSpeed));
                    stacker.reading.timeToToggleStopPlate = (stacker.setting.offset.inNorthDelayDist/(infeedTransport.actualSpeed));
                }
                else stacker.reading.timeToToggleHoldBack = stacker.reading.timeToToggleStopPlate = 1000;
                
                stacker.reading.timeToToggleHoldBack += stacker.setting.offset.inNorthTimeOffsetHoldBack;
                stacker.reading.timeToToggleStopPlate += stacker.setting.offset.inNorthTimeOffset;
            }
            else
            {
                //move fingers out..
                if (!deliverFly.stackingOnFingers)
                {
                    
                    
                    fingerX.acceleration              = 8.5;
                    fingerX.deceleration              = 8.5;
                    fingerX.speed                     = 2.5;
                    getFingerX();
                    
                    fingerX.absMove = FINGER_START_POS_SPAGHETTI+14;
                }

                if (infeedTransport.actualSpeed != 0) {
                    stacker.reading.timeToToggleHoldBack = (stacker.setting.offset.inSouthDelayDistHoldBack/(infeedTransport.actualSpeed));
                    stacker.reading.timeToToggleStopPlate = (stacker.setting.offset.inSouthDelayDist/(infeedTransport.actualSpeed));
                }
                else stacker.reading.timeToToggleHoldBack = stacker.reading.timeToToggleStopPlate = 1000;
                
                stacker.reading.timeToToggleHoldBack += stacker.setting.offset.inSouthTimeOffsetHoldBack;
                stacker.reading.timeToToggleStopPlate += stacker.setting.offset.inSouthTimeOffset;
            }
            if (stacker.reading.timeToToggleHoldBack > stacker.reading.timeToToggleStopPlate) stacker.reading.timeToToggleHoldBack = stacker.reading.timeToToggleStopPlate;
        }  
        
        if (offsetDone && (stacker.reading.stackPages - goBack) >= 3 && lastOffset == stackerCounter.offsets)
        {
            if (!deliverFly.stackingOnFingers)
                fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                
        }
        
        if (lastOffset != stackerCounter.offsets) {
            holdBackOffsetTimer += MILLISECONDS_UNIT;
            offsetTimer += MILLISECONDS_UNIT;
            
            if (holdBackOffsetTimer > stacker.reading.timeToToggleHoldBack && !holdBackDone) {
                holdBackControl(OFFSET_TOGGLE); 
                holdBackDone = 1;
                fingerX.acceleration              = 2.0;
                fingerX.deceleration              = 2.0;
                fingerX.speed                     = 1.5;
            }
            if (offsetTimer > stacker.reading.timeToToggleStopPlate && !offsetDone)
            {
                offsetDone = 1;
                goBack = stacker.reading.stackPages;
                offsetControl(OFFSET_TOGGLE); //do the offset
                stacker.reading.offsetsInStack++;
                lastOffset = stackerCounter.offsets;
                fingerX.acceleration              = 2.0;
                fingerX.deceleration              = 2.0;
                fingerX.speed                     = 1.5;
            }
        } lllllllllllllllllll */
    }
    
    
    /*------------------------------------------------------------------------------------------------------------*/
}

void sheetSignal (void)
{
   struct sheet tmpPg;
   //static UDINT offsetPages;
   static UDINT lastSeq;
   //static BOOL sneakyPageJustRemoved;
   static REAL SpeedDistCnt;
   static REAL SpeedDistAlignerCnt;
   static USINT forceOffsetCnt;
   static REAL meterMeasure;
	
   realMinMaxCheck(&stacker.setting.transportTableSpeed,0.05,3.0);
   //tmpPg = JobControlExit.pg;
   tmpPg = cutter.reading.currentPage;
   SpeedDistCnt += cutter.reading.webSpeed * SECONDS_UNIT;
   SpeedDistAlignerCnt += stacker.reading.axisStatus.transport.actualSpeed * SECONDS_UNIT;
   
   if (curlDistance>0.1) curlDistance -= (tmpPg.sheetLength/1000);
	
   if (/*SHEET_SIGNAL sheetSignal*//*rCommToStacker.sheetSignal != lastSheetSignal*/ cutter.reading.sheetSignal != lastSheetSignal) {
      sheetSequencCheck++;
      sheetSeqErrorEnter++;
      sheetSeqErrorExit++;
      
      /* fiserv */
      if (0 == inQueue) { 
         stacker.reading.sheetSignalFrontEdge = stacker.reading.input.enterTransport;
      }
      else {
         stacker.reading.sheetSignalFrontEdge = FALSE;
         if (stacker.reading.input.enterTransport) stacker.reading.sheetSignalFrontEdge = TRUE;
         else if (sinceFrontEdgeTransportEnter >= (cutter.reading.currentPage.sheetLength+30)) stacker.reading.sheetSignalFrontEdge = TRUE;
      }
      /* fiserv */
      
      
      meterMeasure += cutter.reading.currentPage.sheetLength;
      
      if (merger.setting.enabled) stacker.reading.tripCounter.pages += 2;
      else stacker.reading.tripCounter.pages++;
      if (FAKE_DELIVER_PAGE == tmpPg.type) sheetSequencCheck = 0;
      
      if (meterMeasure>1000) {
         stacker.reading.tripCounter.meters++;
         meterMeasure -= 1000;
      }
      if (tmpPg.type == DELIVER_PAGE || tmpPg.type == OFFSET_PAGE) {
         stacker.reading.tripCounter.jobs++;
         //fiserv
         deliverPageSignals[3] = deliverPageSignals[2];
         deliverPageSignals[2] = deliverPageSignals[1];
         deliverPageSignals[1] = deliverPageSignals[0];
         deliverPageSignals[0] = stacker.reading.sheetSignalFrontEdge;
      }
      
      if (sheetSequencCheck>1 && errorJustCleared==0) {
         addError(QUEUE_ERROR,NORMAL_STOP,FORCE_RELOAD,2);
         //errorTableDown = TRUE;
         sheetSequencCheck = 0;
         pageInTransportQueue(INIT_QUEUE);
      }
      stackerIsClear = 0;
      lastSeq++;
      tmpPg.sequence = lastSeq;
      if (inSpeed.is > 0.05) pageCrashCnt++;
		/*if (sneakyPage && !sneakyPageJustRemoved) //this can not happend anymore. sneakyPage will never be set to true
		{
			strcpy(dbgPrintStr, "*S50 queue error sneakyPage, should not be added to queue");
			strcpy(ULog.text[ULog.ix++], dbgPrintStr);
			sneakyPage = FALSE;
			sneakyPageJustRemoved = TRUE; //prevent situation when no page is added to queue
		}
		else */
      if (inQueue < 10) {
         //if (setting.addToQueueOnSensor)
         //{
         distBSSp++;
         if (fakePage) {
            //pageInTransportQueue(INIT_QUEUE);
            strcpy(dbgPrintStr, "*S50 first page after fake page.. reset queue");
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
            fakePage = FALSE;
         }
         if (distBSSp>9) distBSSp = 0;
         distanceBetweenSheetSignals[distBSSp] = SpeedDistCnt;
         tmpPg.measuredSheetLength = SpeedDistCnt;
         tmpPg.sheetLength = JobControlExit.sheetSize;
         tmpPg.measuredGap = SpeedDistAlignerCnt-SpeedDistCnt;
         
         if (stacker.setting.forceOffset_Dev && stacker.setting.forceOffset_Dev != 255) {
            if (forceOffsetCnt) {
               if (forceOffsetCnt==1) tmpPg.type = OFFSET_PAGE;
               forceOffsetCnt--;
            }
            else {
               forceOffsetCnt = stacker.setting.forceOffset_Dev;
            }
         }
         else if (stacker.setting.forceOffset_Dev == 255 && makeOffset) {
            makeOffset = FALSE;
            tmpPg.type = OFFSET_PAGE;
         }
         
         if (inSpeed.predict != 0)
            tmpPg.estimatedGap = tmpPg.sheetLength * (1-(stacker.reading.axisStatus.transport.actualSpeed/inSpeed.predict));
         else
            tmpPg.estimatedGap = 100.0;
         //if (pageInPocket<1) pageInTransportQueue(POST_PAGE,tmpPg);
         
         pageInTransportQueue(POST_PAGE,tmpPg);
         
         pageInPocket++;
         SpeedDistCnt = 0;
         SpeedDistAlignerCnt = 0;
			
         if (tmpPg.type == OFFSET_PAGE) {	
            strcpy(dbgPrintStr, "*S50 offset page recived: ");
            strcat(dbgPrintStr, uinttostr((unsigned long)(mainTick*1000),tBuf));
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
         }
         if (tmpPg.type == DELIVER_PAGE) {
            strcpy(dbgPrintStr, "*S50 deliver page recived ");
            strcat(dbgPrintStr, uinttostr((unsigned long)(mainTick*1000),tBuf));
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
         }	
         //if (tmpPg.type == NORMAL_PAGE) {
         //   strcpy(dbgPrintStr, "*S50 normal page recived: ");
         //   strcat(dbgPrintStr, uinttostr((unsigned long)(mainTick*1000),tBuf));
         //   strcpy(ULog.text[ULog.ix++], dbgPrintStr);
         //}
         if (tmpPg.type == FAKE_DELIVER_PAGE) {
            strcpy(dbgPrintStr, "*S50 fake deliver page recived: ");
            strcat(dbgPrintStr, uinttostr((unsigned long)(mainTick*1000),tBuf));
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
         }	
      }

      if ((REAL)(inQueue) > ( 1 + (TRANSPORT_TABLE_LENGTH/stacker.setting.transportTableSpeed)/JobControlExit.sheetSize) && errorJustCleared==0) {
         addError(QUEUE_ERROR,NORMAL_STOP,FORCE_RELOAD,1);
         //errorTableDown = TRUE;
         pageInTransportQueue(INIT_QUEUE);
      }

      if (DELIVER_PAGE == tmpPg.type || FAKE_DELIVER_PAGE == tmpPg.type) {
         deliverFly.stopStart = mainTick;
         //sheetHandlerLimiter(SET_SPEED,STOP_TIME,STOP); ////TEST TEST TEST XXXXXXX
         //strcpy(dbgPrintStr, "*S50 set speed to 0 (deliver stop time)");
         //strcpy(ULog.text[ULog.ix++], dbgPrintStr);
         //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,JobControlExit.sheetSize>(deliverFly.holdSpeed/2)?(deliverFly.holdSpeed/2):JobControlExit.sheetSize);
      }

      transportFloorSpeed[KNIFE_ROUND].speed = overSpeedSimple (inSpeed.follow,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_MINSPEED);
      transportFloorSpeed[KNIFE_ROUND].timer = tRtime((JobControlExit.sheetSize + TRANSPORT_TABLE_LENGTH + 140.0),transportFloorSpeed[KNIFE_ROUND].speed,1.0);	
   }
   //lastSheetSignal=sheetSignal;
}

void holdBackControl (USINT func) //0 do nothing, 1 north, 2 south, 3 toggle
{
   if (lineController.setup.stacker.spaghettiBelts) {
      if (func == 1) output.offsetValveSpaghettiBelt2 = 1;
      if (func == 2) output.offsetValveSpaghettiBelt2 = 0;
      if (func == 3) {
         if (stacker.reading.output.offsetValveSpaghettiBelt2 == 0)
            output.offsetValveSpaghettiBelt2 = 1;
         else 
            output.offsetValveSpaghettiBelt2 = 0;
      }
   }
}

void offsetControl (USINT func) //0 do nothing, 1 north, 2 south, 3 toggle
{
   if (lineController.setup.stacker.spaghettiBelts) {
      if (func == 1) output.offsetValveSpaghettiBelt1 = north;
      if (func == 2) output.offsetValveSpaghettiBelt1 = south;
      if (func == 3) {
          if (stacker.reading.output.offsetValveSpaghettiBelt1 == north)
            output.offsetValveSpaghettiBelt1 = south;
          else 
            output.offsetValveSpaghettiBelt1 = north;
      }
      if (output.offsetValveSpaghettiBelt1 == south) stacker.reading.offsetPosition = inSouth;
      else stacker.reading.offsetPosition = inNorth;
      
      //output.offsetValveSpaghettiBelt2 = !output.offsetValveSpaghettiBelt1;
   }
}

void runTransport (void)
{
	static REAL lastSpeed;
	static REAL softStopTime;
	static REAL noPaperTimer;
	static USINT startCnt; 
	static REAL lastSetSpeed; 
	INT i;
	REAL tspeed;
	if (startCnt) startCnt--; 
	if (inSpeed.set > 0 && lastSetSpeed < 0.05) startCnt = 50; 
	lastSetSpeed = inSpeed.set; 
	transportFloorSpeed[WEB_SPEED].speed = overSpeedSimple (inSpeed.predict,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_ZERO);
	if (startCnt) { 
		if ((inSpeed.is*1.5) < stacker.setting.transportIdleSpeed) transportFloorSpeed[WEB_SPEED].speed = stacker.setting.transportIdleSpeed; //
	    else transportFloorSpeed[WEB_SPEED].speed = overSpeedSimple (JobControlExit.isSpeed,stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_ZERO); //
	} 
	transportFloorSpeed[WEB_SPEED].timer = tRtime((JobControlExit.sheetSize + TRANSPORT_TABLE_LENGTH + 140.0),transportFloorSpeed[WEB_SPEED].speed,0);	
	//if (setting.addToQueueOnSensor)
	//{
	if (transportFloorSpeed[WEB_SPEED].speed == 0 && inQueue) {
		transportFloorSpeed[WEB_SPEED].speed = stacker.setting.transportIdleSpeed;
		transportFloorSpeed[WEB_SPEED].timer = 0.5;
      if (TRANSPORT_TABLE_EXIT_1 && TRANSPORT_TABLE_ENTER_1) { //no paper
         noPaperTimer += SECONDS_UNIT;     
	   }
		else noPaperTimer = 0;
      
		if (noPaperTimer > 3.0) {
			//F50Beep = TRUE;
			folder.command.beep = TRUE;
			pageInTransportQueue(INIT_QUEUE);
		}
	}
	else noPaperTimer = 0;
	//}
	tspeed = 0.0;
	for (i=0;i<TRANS_SPEED_QUEUE_SIZE;i++) {
		if (transportFloorSpeed[i].timer >= 0.0) transportFloorSpeed[i].timer -= SECONDS_UNIT;
		if (transportFloorSpeed[i].timer > 0.0 && transportFloorSpeed[i].speed>tspeed) {
			tspeed = transportFloorSpeed[i].speed;
			if (lastTransportID != i) lastTransportID = i;
		}
	}
	if ((*sStop[readSoftStopStatus])(0)) softStopTime += SECONDS_UNIT;
	else softStopTime = 0.0;
	//stop nice and slow
	if (softStopTime<1.0 || stacker.reading.feedPressed)
		infeedTransport.speed = tspeed;
	else {
		infeedTransport.speed = infeedTransport.speed*0.95;
		if (infeedTransport.speed<0.15) infeedTransport.speed = 0;
	}
	lastSpeed = infeedTransport.speed;
   
   /*---------------------------------------------SPAGHETTI------------------------------------------------------*/
   if (lineController.setup.stacker.spaghettiBelts) {
      /*if (stacker.reading.stackTableEmpty) {
         deliverTableBelt.speed=spaghettiBelt.Status.ActVelocity/1000;  //infeedTransport.speed;
         if (deliverFly.pendingDeliver == TRUE || deliverFly.state>0)
            sheetHandlerLimiter(SET_SPEED,FIRST_SHEET,MAX_SPEED);
         else
            sheetHandlerLimiter(SET_SPEED,FIRST_SHEET,1.0); 
      }
      else if (deliverFly.state<3 || deliverFly.state>7) {
         sheetHandlerLimiter(SET_SPEED,FIRST_SHEET,MAX_SPEED); 
         deliverTableBelt.speed=0;
      } */
      sheetHandlerLimiter(SET_SPEED,FIRST_SHEET,MAX_SPEED);
   }
   /*------------------------------------------------------------------------------------------------------------*/
}


REAL cutterSpeed(REAL t,REAL spd,REAL f, REAL m)
{
	// t = gap between sheets (s)
	// spd = transportSpeed
	// f = format
	// m = min speed
	REAL tmp;
	if (spd < 0.1) spd = 0.1;
	if (f < 0.05) f = 0.05;
	tmp = 1/(t/f+1/spd);	
	if ((spd/tmp)<1.4) tmp = spd/1.4; 
	if (tmp<m) tmp = m;
	return tmp;
}

void deliverOnTheFly(void) 
{
    static REAL endOfSheet;
    static REAL distance;
    static USINT lastState;
    static REAL timeSinceGripperOff;
    static REAL gripperTime;
    static REAL usedHoldSpeed;
    static LREAL mainTickTmp;
    static USINT lightCurtainDelay;
    static REAL speedLimit;
    	
    if (lastState != deliverFly.state) {
        deliverFly.stateTime = 0.0;
    }
    else deliverFly.stateTime += SECONDS_UNIT;
	
    lastState = deliverFly.state;
	 
    if (deliverFly.stateTime > 10.0 && deliverFly.state != 0 /*&& deliverFly.debug.simulate_deliverOnFly==0*/) {
        addError(DELIVER,CRASH_STOP,FORCE_RELOAD,deliverFly.state+200);
        errorTableDown = TRUE;
        deliverFly.state = 0;
        deliverFly.pendingDeliver = FALSE;
        deliverFly.stackingOnFingers = FALSE; //remove this, and check in errorState if this is set before resetting. ask operator to remove the papers form fingers manually.
        //deliverFly.debug.simulate_deliverOnFly = 0;
    }
    if ( deliverFly.state != 0) { //speed control
        if (maxDeliverStateTime < deliverFly.stateTime)	maxDeliverStateTime = deliverFly.stateTime;	
        if (deliverFly.state == DELIVER_PAGE_WAITING) { //pending delivery
            usedHoldSpeed = deliverFly.holdSpeed; //capture this speed, incase it changes
            transportFloorSpeed[DELIVER_ON_FLY].speed = usedHoldSpeed; //my speed
            //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,cutterSpeed(0.5,deliverFly.holdSpeed,JobControlExit.sheetSize,0.25));
            if (buffer.reading.usage<55)
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY, (usedHoldSpeed/stacker.setting.transportTableSpeed) * rmap(buffer.reading.usage ,0 ,55 ,0.65 ,1.0));
            else        
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,usedHoldSpeed/stacker.setting.transportTableSpeed);    
        }
        else if (deliverFly.state == LAST_SHEET_INSTACK_WAITING) { //last sheet in stack, table down
            transportFloorSpeed[DELIVER_ON_FLY].speed = usedHoldSpeed; //my speed
            //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,cutterSpeed(0.5,deliverFly.holdSpeed,JobControlExit.sheetSize,0.25)); //limit
             
            if (buffer.reading.usage<55)
                speedLimit = (usedHoldSpeed/stacker.setting.transportTableSpeed) * rmap(buffer.reading.usage ,0 ,55 ,0.65 ,1.0);
            else        
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,usedHoldSpeed/stacker.setting.transportTableSpeed); 
            
        }
        else if (deliverFly.state == STACK_DROP_WAITING) { //wait for stack drop (0.3 s)
            if (fakePage) usedHoldSpeed = (deliverFly.holdSpeed + 0.5)/2;
            else usedHoldSpeed = deliverFly.holdSpeed;
            transportFloorSpeed[DELIVER_ON_FLY].speed = (usedHoldSpeed * 1.02 + inSpeed.predict)/2;//my speed
            //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,cutterSpeed(0.5,deliverFly.holdSpeed,JobControlExit.sheetSize,0.25)); //limit
            if (buffer.reading.usage<55)
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY, (usedHoldSpeed/stacker.setting.transportTableSpeed) * rmap(buffer.reading.usage ,0 ,55 ,0.65 ,1.02));
            else        
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,(usedHoldSpeed/stacker.setting.transportTableSpeed) * 1.02);
        }	
        else if (deliverFly.state > STACK_DROP_WAITING && deliverFly.state<STACKER_FINGER_TRANSITON) {//fingers are out
            if (fakePage) usedHoldSpeed = (deliverFly.holdSpeed + 0.5)/2;
            else usedHoldSpeed = deliverFly.holdSpeed;
            transportFloorSpeed[DELIVER_ON_FLY].speed = (usedHoldSpeed * 1.04 + inSpeed.is)/2; //my speed
            //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,overSpeedSimpleInv(transportFloorSpeed[DELIVER_ON_FLY].speed,stacker.setting.transportTableSpeed)); //limit
            if (buffer.reading.usage<55)
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY, (usedHoldSpeed/stacker.setting.transportTableSpeed) * rmap(buffer.reading.usage ,0 ,55 ,0.65 ,1.04));
            else        
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,(usedHoldSpeed/stacker.setting.transportTableSpeed) * 1.04);    
        }
        else if (deliverFly.state == STACKER_FINGER_TRANSITON) {
            if (fakePage) usedHoldSpeed = (deliverFly.holdSpeed + 0.5)/2;
            else usedHoldSpeed = deliverFly.holdSpeed;
            transportFloorSpeed[DELIVER_ON_FLY].speed = (usedHoldSpeed * 1.06 + inSpeed.is)/2; //my speed				
            //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,(usedHoldSpeed)); //limit
            if (buffer.reading.usage<55)
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY, (usedHoldSpeed/stacker.setting.transportTableSpeed) * rmap(buffer.reading.usage ,0 ,55 ,0.65 ,1.06));
            else        
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,(usedHoldSpeed/stacker.setting.transportTableSpeed) * 1.06);    
        } 
        else if (deliverFly.state == RETRACT_FINGERS) {
            usedHoldSpeed = deliverFly.holdSpeed;
            transportFloorSpeed[DELIVER_ON_FLY].speed = (usedHoldSpeed * 1.08 + inSpeed.is)/2; //my speed
            //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,MAX_SPEED); //limit
            if (buffer.reading.usage<55)
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY, (usedHoldSpeed/stacker.setting.transportTableSpeed) * rmap(buffer.reading.usage ,0 ,55 ,0.65 ,1.08));
            else        
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,(usedHoldSpeed/stacker.setting.transportTableSpeed) * 1.08);        
        }
        else if (deliverFly.state == EXIT) {
            usedHoldSpeed = deliverFly.holdSpeed;
            transportFloorSpeed[DELIVER_ON_FLY].speed = (usedHoldSpeed * 1.10 + inSpeed.is)/2; //my speed
            sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,MAX_SPEED); //no limit
        }
        if (transportFloorSpeed[DELIVER_ON_FLY].speed < (inSpeed.is+0.05)) {
            transportFloorSpeed[DELIVER_ON_FLY_MINSPEED].speed = overSpeedSimple (inSpeed.is*(inSpeed.set>inSpeed.is?1.1:1),stacker.setting.transportIdleSpeed,stacker.setting.transportTableSpeed,GO_TO_ZERO);
        }
        else {
            transportFloorSpeed[DELIVER_ON_FLY_MINSPEED].speed = 0.5;
        }
        transportFloorSpeed[DELIVER_ON_FLY_MINSPEED].timer = 1.25;//5.0;
        transportFloorSpeed[DELIVER_ON_FLY].timer = 3.0;
    }
    
    if (deliverFly.inProgress && stacker.command.instantDeliver) {
        addError(STACKER_BUSY,CRASH_STOP,FORCE_RELOAD,deliverFly.state);
        errorTableDown = TRUE;
        deliverFly.state = 0;
        deliverFly.pendingDeliver = FALSE;
        deliverFly.stackingOnFingers = FALSE; 
        stacker.command.instantDeliver = FALSE;
        deliverFly.inProgress = FALSE;
    }

    switch (deliverFly.state) {
        case 0:
            deliverFly.stateTime = 0;
            deliverFly.stackerFingerTransState=0;
            fakePage = FALSE;
            if (state == ReadyMode && deliverFly.pendingDeliver) {
                deliverFly.state = 1;
                maxDeliverStateTime = 0;
                timeSinceGripperOff = deliverFly.stateTime;
                stacker.reading.deliveriesSinceLoad++;
                
                strcpy(dbgPrintStr, "*S50 wait for DELIVER_PAGE pIT:");
                strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue (GET_NO_PAGES_IN_QUEUE)),tBuf));
                strcat(dbgPrintStr, "\0");
                strcpy(ULog.text[ULog.ix++], dbgPrintStr);
                
                //STACK_DELIVER_GRIPPER = OFF;
                output.stackDeliverGripper = FALSE;  //setOutput(&stackDeliverGripper,OFF);
                if (GUI.readings.S50_stackSize > 50) {
                    avgStepLastStack = step.avgStep;
                    autoAdjustState++;
                }
            }
            if (state == ReadyMode && cutter.reading.processingWhite && stacker.command.instantDeliver)
            {
                deliverFly.inProgress = TRUE;
                deliverFly.stackerFingerTransState=0;
                deliverFly.state = 2;
                output.stackDeliverGripper = FALSE;
                fakePage = TRUE;
                endOfSheet = infeedTransport.actualPosition + TRANSPORT_TABLE_LENGTH + JobControlExit.sheetSize + 500; //was 0.0, 150.0
                strcpy(dbgPrintStr, "*S50 instantDeliver:");
                strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue (GET_NO_PAGES_IN_QUEUE)),tBuf));
                strcat(dbgPrintStr, "\0");
                strcpy(ULog.text[ULog.ix++], dbgPrintStr);
                gripperTime = 0;
                mainTickTmp = mainTick;
                stacker.command.instantDeliver = FALSE;
            }
            //deliverFly.debug.tableDown=FALSE;
            break;
        case 1:
            //WAIT FOR LAST SHEET
            deliverFly.inProgress = TRUE;
            deliverFly.stackerFingerTransState=0;
            endOfSheet = infeedTransport.actualPosition + 10000;
            if (DELIVER_PAGE == pageInTransportQueue(PEEK_PAGE)) { 
                deliverFly.state = 2; 
                output.stackDeliverGripper = FALSE; //setOutput(&stackDeliverGripper,OFF);
                fakePage = FALSE;
                strcpy(dbgPrintStr, "*S50 next dp at exit sensor pIT:");
                strcat(dbgPrintStr, uinttostr((unsigned long)(pageInTransportQueue (GET_NO_PAGES_IN_QUEUE)),tBuf));
                strcat(dbgPrintStr, "\0");
                strcpy(ULog.text[ULog.ix++], dbgPrintStr);
            }
            gripperTime = 0;
            mainTickTmp = mainTick;
            break;
        case 2:
            if (!TRANSPORT_TABLE_EXIT_1 && !fakePage) {
                endOfSheet = infeedTransport.actualPosition; 
            }
            /*********************LAST SHEET IN STACK***170mm*********&&*****************300ms Gripper time****************/          
            if ((infeedTransport.actualPosition - endOfSheet) > deliverFly.lastSheetInStackDistFromSensor + delayIt?JobControlExit.sheetSize+100:0) {// delayIt is a patch REMOVE this an find out the real problem
                if (gripperTime == 0)
                {
                    strcpy(dbgPrintStr, "*S50 gripper pulse on:");
                    strcat(dbgPrintStr, uinttostr((unsigned int)((mainTick-mainTickTmp)*1000),tBuf));
                    strcat(dbgPrintStr, "\0");
                    strcpy(ULog.text[ULog.ix++], dbgPrintStr);
                    //gripper.command.index = TRUE;
                    warnings[DELIVERING] = TRUE;
                }
                gripperTime += SECONDS_UNIT;
                delayIt = 0;
            }
            if (gripperTime>0.350)
            {
                //deliverFly.debug.tableDown=TRUE;
                if (lineController.setup.stacker.spaghettiBelts) {
                    fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                    offsetControl(NORTH);
                    holdBackControl(NORTH);
                    stacker.reading.offsetState = DWELL_NORTH;
                }
                deliverFly.state = 3;
                stackerCounter.onTheFlyDeliveries++;
                tableDown();
                stacker.reading.offsetsInStack = 0;
                strcpy(dbgPrintStr, "*S50 table down:");
                strcat(dbgPrintStr, uinttostr((unsigned long)((mainTick-mainTickTmp)*1000),tBuf));
                strcat(dbgPrintStr, "\0");
                strcpy(ULog.text[ULog.ix++], dbgPrintStr);
                //deliverFly.state = 10; //testing
            }
            //debug1=endOfSheet;
            break;
        case 3:
            //STACK DOWN + SLOW DOWN TRANSPORT
            //transportFloorSpeed[DELIVER_ON_FLY].timer = 0;
            deliverFly.stackingOnFingers = FALSE;
            
            fingerX.acceleration              = 2.0;
            fingerX.deceleration              = 2.0;
            fingerX.speed                     = 1.5;
            getFingerX();
            
            if (deliverFly.stateTime > deliverFly.stackDropFngOutWait) {
                //STACK_DELIVER_GRIPPER = OFF;  //drop last sheet
                //setOutput(&stackDeliverGripper,OFF);
                output.stackDeliverGripper = FALSE;
                step.stackPages = 0;
                if (lineController.setup.stacker.spaghettiBelts)
                {
                    //fingerX.absMove = JobControlExit.sheetSize - 10; //infront of first stopplate, orginal fiserv
                    fingerX.absMove = JobControlExit.sheetSize - 3.5 + stacker.setting.forkDeliverPositionAdjust; //stop between the stopplates +5, -1.5
                    //fingerX.absMove = JobControlExit.sheetSize + 20; //passed the second stopplate
                }
                else
                    fingerX.absMove = JobControlExit.sheetSize + 5; //Finger out +10, -15
                deliverFly.stopEnd = mainTick; //stop the stop timer
                deliverFly.stackingOnFingers = TRUE; //ready to take paper at full speed
                distance = deliverTableBelt.actualPosition; //store the belt position
                clear(RESET_CLEAR); //clear the table clear timer 
                deliverFly.state = 5; //leave this state
                //deliverTableBelt.speed=0.1;// prestart deliverTable .................. JobControlExit.sheetSize
                deliverTableBelt.speed=((stacker.setting.deliveryRunDistance - JobControlExit.sheetSize)*0.001)/1.2;
                if (deliverTableBelt.speed>0.2) deliverTableBelt.speed = 0.2;
                deliverFly.debug_2 += deliverFly.stateTime;
                //deliverFly.state = 5; // leave room for fingers before they assert
                strcpy(dbgPrintStr, "*S50 finger out:");
                strcat(dbgPrintStr, uinttostr((unsigned long)((mainTick-mainTickTmp)*1000),tBuf));
                strcat(dbgPrintStr, "\0");
                strcpy(ULog.text[ULog.ix++], dbgPrintStr);
                warnings[DELIVERING] = FALSE;
                /*if (inSouth == stacker.reading.offsetPosition) {
                    offsetControl(NORTH);
                    holdBackControl(NORTH);
                    stacker.reading.offsetState = DWELL_NORTH;
                }*/
            }
            break;
        case 4:
            //FINGER OUT + START CUTTER
            break;
        case 5:
            //START TRANSPORT STACK
            //deliverTableBelt.speed=setting.deliverTransportSpeed;
            deliverTableBelt.speed=stacker.setting.deliverTransportSpeed<0.1?0.1:stacker.setting.deliverTransportSpeed;
            deliverFly.debug_1 += 1.0;
            /*
            //move stop plate down
            if (fingerX.actualPositon > (JobControlExit.sheetSize - 7.5) && DWELL_SOUTH != stacker.reading.offsetState) {
                offsetControl(SOUTH);
                holdBackControl(SOUTH);
                stacker.reading.offsetState = DWELL_SOUTH;
            }    
            */
            if (clear(DELIVERYTABLE_SENSORS,(ticksPerSecond * 0.75))) deliverFly.state = 6; //clear for 0.75 seconds	
            break;
        case 6:
            //WAIT FOR TRANSPORT STACK
            clear(RESET_CLEAR);
            deliverFly.stackerUpState = 0;
            lightCurtainDelay = 0;
            /*
            //move stop plate down
            if (fingerX.actualPositon > (JobControlExit.sheetSize - 7.5) && DWELL_SOUTH != stacker.reading.offsetState) {
                offsetControl(SOUTH);
                holdBackControl(SOUTH);
                stacker.reading.offsetState = DWELL_SOUTH;
            }
            */
            if (rabs((deliverTableBelt.actualPosition - distance)) > (stacker.setting.deliveryRunDistance)) deliverFly.state = 7; //make sure the table has feed one full turn
            else if (deliverFly.stateTime > 5.0) deliverFly.state = 7; //something went wrong
            break;
        case 7:
            //TABLE UP TIME CALCULATION
            deliverTableBelt.speed=0;                                              //STOP DELIVERY TABLE
            if (stacker.reading.input.lightCurtain) lightCurtainDelay++;           //bonuncy sheets delay.
            else lightCurtainDelay = 0;
            if (stacker.reading.input.lightCurtain /*lightCurtainDelay>5*/ && deliverFly.stackerUpState == 0) {
            stacker.reading.SFTransCalc = ((0.5 * inSpeed.predict / (cutter.reading.currentPage.sheetLength * 0.001)) * stacker.setting.tableSteps);
            if (stacker.reading.SFTransCalc > 2.0) stacker.reading.SFTransCalc = 2.0;
            if (stacker.reading.SFTransCalc < 0) stacker.reading.SFTransCalc = 0.0;
            stackerY.absMove = fingerY.transformPos - stacker.reading.SFTransCalc;
            deliverFly.stackerMovePos = stackerY.absMove;
            deliverFly.stackerUpState = 1;
            }
            if (rabs (stackerY.actualPositon - fingerY.transformPos) < 2.0) { //kanske was 4.0
                deliverFly.state = 8;
                deliverFly.fingerYtransform = fingerY.transformPos;
            }
            if (stackerY.inMovement && deliverFly.stackerUpState == 1) {
                deliverFly.stackerUpState = 2;
            }
            if (stackerY.inPosition && deliverFly.stackerUpState == 2) {
                deliverFly.debug_8 = rabs(stackerY.actualPositon - fingerY.transformPos);
                deliverFly.debug_7 = stackerY.actualPositon;
                deliverFly.fingerYtransform = fingerY.transformPos;
                deliverFly.stackerUpState = 3;
                deliverFly.state = 8;
            }
            deliverFly.stackerFingerTransState=0;
            break;
        case 8:
            //MEET FINGERS + RETRACT FINGERS
            if (deliverFly.stateTime > 0.02 && deliverFly.stackerFingerTransState == 0) {
                fingerY.move = (-1) * (6.5 + infeedTransport.actualSpeed/2); //drop 6 mm, setFingerY will block this to maximum 7 mm, TODO, make a new measurement on how much i can lower the fingers and not crash into the stacker
                deliverFly.stackerFingerTransState = 1;
            }
            else if (deliverFly.stateTime > 0.25) { //was 0.15
                deliverFly.stackingOnFingers = FALSE;
                deliverFly.state = 9;

                if (lineController.setup.stacker.spaghettiBelts)
                    fingerX.absMove = FINGER_START_POS_SPAGHETTI; //Finger out +10, -15
                else
                    fingerX.absMove = 0.0;
            }
            break;
        case 9:
            //FINGERS TO START POSITION + LEAVE 
            //
            //sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,MAX_SPEED);
            if (deliverFly.stateTime > 0.25 &&  deliverFly.stateTime < (0.25+(SECONDS_UNIT*1.5))) //drop forks on the way back
                fingerY.move = -1 * (infeedTransport.actualSpeed/5);
			
            if (deliverFly.stateTime > 0.75 &&  deliverFly.stateTime < (0.75+(SECONDS_UNIT*1.5))) //drop forks on the way back
            fingerY.move = -1 * (infeedTransport.actualSpeed/5);
         
            if (deliverFly.stateTime > 1.25 &&  deliverFly.stateTime < (1.25+(SECONDS_UNIT*1.5))) //drop forks on the way back
                fingerY.move = -1 * (infeedTransport.actualSpeed/5);
			
            if (deliverFly.stateTime > 1.75 &&  deliverFly.stateTime < (1.75+(SECONDS_UNIT*1.5))) //drop forks on the way back
                fingerY.move = -1 * (infeedTransport.actualSpeed/5);
         
            if (fingerX.inPosition) {
                //deliverFly.debug.simulate_deliverOnFly = 0;
                deliverFly.state = 0;
                deliverFly.inProgress = FALSE;
                deliverFly.pendingDeliver = FALSE;
                fingerY.absMove = fingerY.maxPos;
                sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,MAX_SPEED);
            }
            break;
        case 10:
            deliverFly.inProgress = FALSE;
            deliverFly.pendingDeliver = FALSE;
            deliverFly.state = 0;
            sneakyPage = FALSE;
            break;
        default:
            break;
    }
    if (deliverFly.state>1) stackerInDeliver = TRUE;
    else stackerInDeliver = FALSE;
    stacker.command.instantDeliver = FALSE;
}

REAL filter(REAL x[12], int depth, int pos, int bias)
{
	if (depth < 3) return 0;
	int i;
	REAL z[12];
	REAL s[12];
	REAL lo=100000;
	REAL hi=-100000;
	REAL lo2=100000;
	REAL hi2=-100000;
	REAL tot = 0;
	for (i=0;i<12;i++) {
		z[i]=x[pos];
		if (pos==0) pos=9;
		else pos--;
		if (bias==0) s[i]=0.0;
		else if (bias>0) s[i]=-100000;
		else  s[i]=100000;
	}
   
	for (i=0;i<depth;i++) {
		s[i]=z[i];
		if (s[i]>hi) hi = s[i];
		if (s[i]<lo) lo = s[i];
		tot += s[i];
        
		if (depth>4) {
			if (s[i]>hi2 && s[i]<hi) hi2 = s[i];
			if (s[i]<lo2 && s[i]>lo) lo2 = s[i];
		}
        
	}
	if (bias == 0) {
		tot = tot - hi - lo;
		return tot/(float)(depth-2);
	}
	if (bias < 0) {
		tot = tot - hi + lo;
		if (depth>4) 
		{
			if (bias==-1) tot = tot - hi2/2 + lo2/2;
			else tot = tot - hi2 + lo2;
		}
		return tot/(float)(depth);
	}
	if (bias > 0) {
		tot = tot + hi - lo;
		if (depth>4) tot = tot + hi2/2 - lo2/2;
		return tot/(float)(depth);
	}
	return 0;
}

REAL stepComp (REAL s)
{
	if (s>3) s = 3;
	else if (s<-3) s=3;
	return 1+(s*s*s*0.005)+(s*0.02);    
}
/*
void bubbleSort(REAL arr[], USINT size){
   //BubbleSort algorithm will sort the array elements in ascending order. Outer loop is for passes and inner loop will do comparisons
   int l=1;
   int m=0;
   for(l; l<=size; l++){
      for(m; m<size-1; m++){
         if(arr[m]>arr[m+1]){
            int temp;
            temp = arr[m];
            arr[m] = arr[m+1];
            arr[m+1] = temp;
         }
      }
   }
}

REAL median(REAL arr[], USINT size){
   bubbleSort(arr, size);
   int middle = size/2;
   return arr[middle-1];
}


REAL average(REAL arr[], USINT size){
   if (size==0) return 0.0;
   else if (size<4) return (arr[0]+arr[1]+arr[2]+arr[3])/4;
   REAL sum=0.0;
   REAL max=-20.0;
   REAL min=+20.0;
   int i;
   for (i=0;i<size;i++)
   {
      sum += arr[i];
      if (arr[i]>max) max = arr[i];
      if (arr[i]<min) min = arr[i];
   }
   return (sum-max-min)/(size-2);
}*/


SINT signCheck(REAL arr[]) 
{
   int i,sign=0;
   for (i=0;i<8;i++) {
      if (arr[i]<0) sign--;
      else sign++;
   }
   if (sign>7) return 2;
   if (sign<-7) return -2;
   if (sign>6) return 1;
   if (sign<-6) return -1;
   else return 0;
}

void removeMax (USINT * size)
{
   REAL max = -1000000;
   USINT i,p;
   for (i=0;i<*size;i++) {
      if (spaghettiStep.sheetMeasurment[i]>max) { 
         max = spaghettiStep.sheetMeasurment[i];
         p=i;
      }
   }
   for (i=p;i<(*size-1);i++) {
      spaghettiStep.sheetMeasurment[i] = spaghettiStep.sheetMeasurment[i+1];
   }
   *size = *size-1;
}

void removeMin (USINT * size)
{
   REAL max = 1000000;
   USINT i,p;
   for (i=0;i<*size;i++) {
      if (spaghettiStep.sheetMeasurment[i]<max) { 
         max = spaghettiStep.sheetMeasurment[i];
         p=i;
      }
   }
   for (i=p;i<(*size-1);i++) {
      spaghettiStep.sheetMeasurment[i] = spaghettiStep.sheetMeasurment[i+1];
   }
   *size = *size-1;
}

REAL sumAll (USINT * size)
{
   USINT i;    
   REAL x=0;
   for (i=0;i<*size;i++) x += spaghettiStep.sheetMeasurment[i];     
   return x;    
}

REAL medAvg (USINT size) //signal treatment, biased to the lower half of the measurements
{ 
   if (!size) return 0.0;
   USINT i;
   for (i=0;i<size/3;i++) removeMax(&size); //take away 33% of the top values
   for (i=0;i<size/6;i++) removeMin(&size); //taka away 17% of the rest lowest values
   return sumAll(&size)/(size); //calculate average of the rest
}

void stackerStepSpaghetti (void)
{
   //static BOOL newStack;
   static USINT skip;
   //LREAL stepSize;
   //LREAL tmp;
   //static LREAL rest;
   static REAL lastStackPos;
   static BOOL toggleSent=0;
   //static UINT pagesAfterForkStacking;
   int i;
   spaghettiStep.maxMeasurements = 25;
   
   /*if (stacker.setting.useStepSensor && stacker.reading.stackPages == 0) {
      useFirstStepFactor = TRUE;
   }*/
   
   if (stacker.reading.stackPages>0) spaghettiStep.average = stacker.reading.stackSize/stacker.reading.stackPages;
   else spaghettiStep.average = stacker.reading.stackSize;  //sourceS50.stackSize;
   
   if (stacker.setting.useStepSensor == 1 || stacker.setting.useStepSensor == 2) {
      if (stacker.setting.useStepSensor == 1) {
         spaghettiStep.kP = 0.81;
         spaghettiStep.kI = 0.065;
         spaghettiStep.kD = 0.008;
         spaghettiStep.maxAdjust = 20;
      }
      else if (stacker.setting.useStepSensor == 2) { //Teach tablestep, exit when satisfied (stacker.setting.useStepSensor->1)
         spaghettiStep.kP = 0.85;
         spaghettiStep.kI = 0.075;
         spaghettiStep.kD = 0.015;
         spaghettiStep.maxAdjust = 60;
         if (step.newPageInStack && deliverFly.state == 0) {
            if (stacker.reading.stackSize>25 && rabs(spaghettiStep.lastError[0])<0.05 && rabs(spaghettiStep.lastError[1])<0.05) {  
               stacker.setting.tableSteps = (spaghettiStep.average + stacker.setting.tableSteps*3) / 4;
               spaghettiStep.integral = spaghettiStep.integral * 0.75;
               if (stacker.reading.stackSize>32.0) {
                  stacker.setting.tableSteps = (spaghettiStep.average + stacker.setting.tableSteps)/2;
                  stacker.setting.useStepSensor = 1; //exit to auto
               }
            }
            else if (rabs(spaghettiStep.lastError[0])<0.075 && stacker.reading.stackSize>10) {
               stacker.setting.tableSteps = (spaghettiStep.average + stacker.setting.tableSteps*9) / 10;
               spaghettiStep.integral = spaghettiStep.integral * 0.95;
            }
            else if (rabs(spaghettiStep.lastError[0])<0.05 && stacker.reading.stackSize>3)
               stacker.setting.tableSteps = (spaghettiStep.average + stacker.setting.tableSteps*49) / 50;
            else if (stacker.reading.stackSize>50) {
               stacker.setting.tableSteps = spaghettiStep.average;
               stacker.setting.useStepSensor = 1; //exit to auto
            }             
         }
      }
      spaghettiStep.measumentDist += (transport.Status.ActVelocity*SECONDS_UNIT); //mm
      if (spaghettiStep.measumentDist>(((cutter.reading.sheetSize*stacker.reading.actualOverSpeed)+20)/spaghettiStep.maxMeasurements)) { // take a sample, max 25 measurements per sheet
         //measure up to 25 times on the sheet..
         spaghettiStep.measumentDist=0;
         if (deliverFly.state != 0 || stacker.reading.stackSize<5 || stacker.setting.useStepSensor == 2) spaghettiStep.sheetMeasurment[spaghettiStep.sheetMeasurmentP] = spaghettiStep.inputValue;
         else spaghettiStep.sheetMeasurment[spaghettiStep.sheetMeasurmentP] = spaghettiStep.inputValue - ((stacker.setting.holdingPositionGradient*stacker.reading.stackSize)/100);   
         spaghettiStep.sheetMeasurmentP++;
         //spaghettiStep.measTmp++;
         stacker.reading.stepSensorSetPoint = ((stacker.setting.holdingPositionGradient*stacker.reading.stackSize)/100);
         //just to display what is going on
         if (stacker.reading.stackPages == 0) {
            stacker.reading.stepSensorSetPoint = stacker.reading.stepSensorSetPoint - ((1-stacker.setting.firstStepFactor) *  stacker.setting.tableSteps);
         }
         else if (stacker.reading.stackPages == 1) {
            stacker.reading.stepSensorSetPoint = stacker.reading.stepSensorSetPoint - ((1-stacker.setting.firstStepFactor) *  (stacker.setting.tableSteps/2));
         }
         //
         if (spaghettiStep.sheetMeasurmentP >= spaghettiStep.maxMeasurements) spaghettiStep.sheetMeasurmentP = (spaghettiStep.maxMeasurements-1);
      }
      if (step.newPageInStack) {
         //remove bad measurements by taking the median, 
         //spaghettiStep.sheetMedian = median(spaghettiStep.sheetMeasurment, spaghettiStep.sheetMeasurmentP);
         //spaghettiStep.sheetMedian = average(spaghettiStep.sheetMeasurment, spaghettiStep.sheetMeasurmentP);
         spaghettiStep.sheetMedian = medAvg(spaghettiStep.sheetMeasurmentP);
         //restrain it.
         if (spaghettiStep.sheetMedian >  2.5)  spaghettiStep.sheetMedian =  2.5;
         if (spaghettiStep.sheetMedian < -2.5)  spaghettiStep.sheetMedian = -2.5;
         //reset measurement pointer to the next sheet
         spaghettiStep.sheetMeasurmentP = 0;
         //PID controller
         spaghettiStep.error = spaghettiStep.sheetMedian;
         spaghettiStep.integral = (spaghettiStep.integral + spaghettiStep.error)*spaghettiStep.kI * (deliverFly.stackingOnFingers?0.5:1);
         spaghettiStep.derivative = (spaghettiStep.error - spaghettiStep.lastError[0])*spaghettiStep.kD;
         spaghettiStep.output = (spaghettiStep.kP * spaghettiStep.error) + spaghettiStep.integral + spaghettiStep.derivative;
         //save the last errors used for debugging
         spaghettiStep.lastError[7] = spaghettiStep.lastError[6];
         spaghettiStep.lastError[6] = spaghettiStep.lastError[5];
         spaghettiStep.lastError[5] = spaghettiStep.lastError[4];
         spaghettiStep.lastError[4] = spaghettiStep.lastError[3];
         spaghettiStep.lastError[3] = spaghettiStep.lastError[2];
         spaghettiStep.lastError[2] = spaghettiStep.lastError[1];
         spaghettiStep.lastError[1] = spaghettiStep.lastError[0];
         spaghettiStep.lastError[0] = spaghettiStep.error;
         
         
         
         /*if (stacker.setting.useStepSensor == 2 && step.stackPages%9 == 8)
         {
            if (signCheck(spaghettiStep.lastError)<-1 && spaghettiStep.tuneState<3)
               stacker.setting.tableSteps = stacker.setting.tableSteps*0.935;
            else if (signCheck(spaghettiStep.lastError)>1 && spaghettiStep.tuneState<3)
               stacker.setting.tableSteps = stacker.setting.tableSteps*1.065;
            if (signCheck(spaghettiStep.lastError)<0 && spaghettiStep.tuneState<4)
               stacker.setting.tableSteps = stacker.setting.tableSteps*0.985;
            else if (signCheck(spaghettiStep.lastError)>0 && spaghettiStep.tuneState<4)
               stacker.setting.tableSteps = stacker.setting.tableSteps*1.015;
            else //fine tune
            {
               spaghettiStep.tuneState++;
               if (step.stackPages>200)
                  stacker.setting.tableSteps = spaghettiStep.average;
            }
         }*/
         
         //avoid wind-up
         if (spaghettiStep.integral > spaghettiStep.maxAdjust * 0.008) spaghettiStep.integral = spaghettiStep.maxAdjust * 0.008;
         if (spaghettiStep.integral < spaghettiStep.maxAdjust * -0.008) spaghettiStep.integral = spaghettiStep.maxAdjust * -0.008;
         //limit error correction
         if (spaghettiStep.output>stacker.setting.tableSteps*(spaghettiStep.maxAdjust  * 0.01)) spaghettiStep.output = stacker.setting.tableSteps*(spaghettiStep.maxAdjust *  0.01); 
         if (spaghettiStep.output<stacker.setting.tableSteps*(spaghettiStep.maxAdjust * -0.01)) spaghettiStep.output = stacker.setting.tableSteps*(spaghettiStep.maxAdjust * -0.01);
         //use the new step
         
         if (deliverFly.stackingOnFingers && 0 == deliverFly.stackerFingerTransState) {
            fingerY.move = - (stacker.setting.tableSteps+(spaghettiStep.output/2));
            pagesAfterForkStacking = 0;
         }
         else if (deliverFly.state != 2 && deliverFly.state != 3) {
            if (pagesAfterForkStacking<2) {
               stackerY.move = -stacker.setting.tableSteps; 
               spaghettiStep.output = spaghettiStep.output/2;
            }
            else stackerY.move = - (stacker.setting.tableSteps+spaghettiStep.output);
            pagesAfterForkStacking++;
         }
         step.newPageInStack = 0;
         /*
         if (deliverFly.stackingOnFingers) {
            if (stacker.reading.stackPages==0) {
               fingerY.move = - ((stacker.setting.tableSteps+(spaghettiStep.output/2))*stacker.setting.firstStepFactor);
               spaghettiStep.integral = 0;//restart intrgral part, prevent windup
            }
            else if (stacker.reading.stackPages==1) {
               fingerY.move = - ((stacker.setting.tableSteps+(spaghettiStep.output/2))*((stacker.setting.firstStepFactor+1.0)*0.5));   
            }   
            else   
               fingerY.move = - (stacker.setting.tableSteps+(spaghettiStep.output/2));
         }
         else if (deliverFly.state != 2 && deliverFly.state != 3){
            if (stacker.reading.stackPages==0) {
               stackerY.move = - (stacker.setting.tableSteps+spaghettiStep.output * stacker.setting.firstStepFactor);
            }
            else if (stacker.reading.stackPages==1) {
               stackerY.move = - (stacker.setting.tableSteps+spaghettiStep.output * ((stacker.setting.firstStepFactor+1.0)*0.5));
            }
            else {
               stackerY.move = - (stacker.setting.tableSteps+spaghettiStep.output);
            }
         }
         step.newPageInStack = 0;
      }
      */
      }
   }
   else //step.useStepSensor == 0
   {
      if (step.newPageInStack)
      {
         //if (stackerY.pos == (stackerY.maxPos + stacker.setting.topPosOffset /*GUI.settings.S50_topPosition*/) && FALSE == newStack) {
         if (stackerY.pos > (stacker.setting.topPosOffset - 0.005) && FALSE == newStack) {
            newStack = TRUE;
            skip = 1;
            cutter.command.deliverNextRequest = FALSE;
         }
         if (skip) skip--;
         else {
            if (newStack) rest = 0;
            stepSize = 0.0625;
            thisStep = stacker.setting.tableSteps+rest;
            for (i=0;i<100;i++)
            {
               if ((thisStep - stepSize) < 0) {
                  rest = thisStep; //save the rest
                  break;
               }
               else thisStep -= stepSize;
            }
            iDebug = i;
            if (deliverFly.stackingOnFingers) {
               if ((fingerY.pos > (fingerY.maxPos - 0.005)) && FALSE == newStack) newStack = TRUE;
               fingerY.move = -((LREAL)(i)*stepSize);
            }
            else {
               stackerY.move = -((LREAL)(i)*stepSize);
            }
            newStack = FALSE;
         }
         step.newPageInStack = 0;
         
      }
   }
   //if ((stackerY.maxPos + stacker.setting.topPosOffset /*GUI.settings.S50_topPosition*/ - stackerY.pos) > stacker.setting.maxStackSize && !deliverFly.stackingOnFingers && deliverFly.state != 3 && !deliverFly.pendingDeliver) {
   if ((stacker.setting.topPosOffset - stackerY.pos) > stacker.setting.maxStackSize && !deliverFly.stackingOnFingers && deliverFly.state != 3 && !deliverFly.pendingDeliver) {
      if (!toggleSent) {
         cutter.command.deliverNowRequestToggle = !cutter.command.deliverNowRequestToggle;
         strcpy(dbgPrintStr, "*S50 DELIVER NOW REQUEST TOGGLE (by stack size)");
         strcpy(ULog.text[ULog.ix++], dbgPrintStr);
         toggleSent = TRUE;
      }
      
   }
   else {
      toggleSent = FALSE;
   }
   lastStackPos = stackerY.pos;
   
}

/*
void stackerStep (void)
{
	static BOOL newStack;
	static USINT skip;
	static REAL lastStackPos;
	//REAL adjustStep;
	//static REAL history;
	static REAL lastTableSteps;
	LREAL stepSize;
	LREAL tmp;
	static LREAL rest;
	int i;
	static REAL lastStepSensor;
	//static BOOL lastDI_StepSensor;
	static UDINT pagesSinceSouth;
	static REAL lss[12];
	static USINT lssP;
	
	REAL stepVal = ((REAL)(stacker.reading.input.AnalogStepSensor) - 16383)/5461;
	if (stacker.reading.offsetPosition == south) stepVal -= SOUTH_OFFSET_ADJUST;
	if (stepVal>3) stepVal = 3;
	if (stepVal<-3) stepVal = -3;
	
	//filtering low bias
	if (step.newPageInStack)
	{
		
		lssP++;
		if (lssP>=12) lssP = 0;
		lss[lssP] = stepVal;
	}
	if (stepVal < lss[lssP]) lss[lssP] = stepVal;
	
	if (step.stackPages>0)step.avgStep = stacker.reading.stackSize/step.stackPages;
	else step.avgStep = stacker.reading.stackSize;  //sourceS50.stackSize;

	if (step.stackPages>4 && step.newPageInStack) {
		lastStepSensor = GUI.readings.S50_StepSensorFilt;
		if (infeedTransport.actualSpeed < 0.5)
			GUI.readings.S50_StepSensorFilt = filter(lss,pagesSinceSouth<5?3:5,lssP,-1);
		else if (infeedTransport.actualSpeed < 0.75)
			GUI.readings.S50_StepSensorFilt = filter(lss,pagesSinceSouth<5?3:6,lssP,-2);
		else if (infeedTransport.actualSpeed < 1.0)
			GUI.readings.S50_StepSensorFilt = filter(lss,pagesSinceSouth<5?4:6,lssP,-2);
		else if (infeedTransport.actualSpeed < 1.25)
			GUI.readings.S50_StepSensorFilt = filter(lss,pagesSinceSouth<5?4:7,lssP,-2);
		else if (infeedTransport.actualSpeed < 1.50)
			GUI.readings.S50_StepSensorFilt = filter(lss,pagesSinceSouth<5?5:7,lssP,-2);
		else if (infeedTransport.actualSpeed < 1.75)
			GUI.readings.S50_StepSensorFilt = filter(lss,pagesSinceSouth<5?5:8,lssP,-2);
		else if (infeedTransport.actualSpeed < 2.0)
			GUI.readings.S50_StepSensorFilt = (filter(lss,pagesSinceSouth<5?5:9,lssP,-2) + GUI.readings.S50_StepSensorFilt)/2;
		else 
			GUI.readings.S50_StepSensorFilt = (filter(lss,pagesSinceSouth<5?5:10,lssP,-2) + GUI.readings.S50_StepSensorFilt * 2)/3;
		
		if (GUI.readings.S50_StepSensorFilt>3 || GUI.readings.S50_StepSensorFilt<-3) GUI.readings.S50_StepSensorFilt = lastStepSensor; 
	}
	
	if (step.stackPages<=4) {
		GUI.readings.S50_StepSensorFilt = (GUI.readings.S50_StepSensor + stepVal + GUI.readings.S50_StepSensorFilt*10)/12;
	}
   if (infeedTransport.actualSpeed == 0) {   //reset measurement
		GUI.readings.S50_StepSensorFilt = (GUI.readings.S50_StepSensor + GUI.readings.S50_StepSensorFilt*11)/12;
		if (GUI.readings.S50_StepSensorFilt<2.9 && GUI.readings.S50_StepSensorFilt>(-2.9))
			lss[0] = lss[1] = lss[2] = lss[3] = lss[4] = lss[5] = GUI.readings.S50_StepSensorFilt;
		lss[6] = lss[7] = lss[8] = lss[9] = lss[10] = lss[11] = GUI.readings.S50_StepSensorFilt;
	}		
    	
   else if (stacker.setting.useStepSensor == 2 || stacker.setting.useStepSensor == 1) {  //1-2
		if (autoAdjustState == 0) avgStepLastStack = stacker.setting.tableSteps;
		if (step.newPageInStack)
		{
			if (lastTableSteps != stacker.setting.tableSteps) {
				step.tableSteps = stacker.setting.tableSteps;
				autoAdjustState = 0;
			}
			lastTableSteps = stacker.setting.tableSteps;
			
			if (stacker.reading.offsetPosition == north && step.stackPages % 3 == 0) 
			{
				step.tableSteps = step.tableSteps * stepComp(GUI.readings.S50_StepSensorFilt);
				pagesSinceSouth++;
			}
			if (stacker.reading.offsetPosition == south) {
				pagesSinceSouth = 0;
				if (autoAdjustState == 0) step.tableSteps = stacker.setting.tableSteps;
				else {
					step.tableSteps = avgStepLastStack;//(stacker.setting.tableSteps + )/2;
				}
			}		
			if (deliverFly.stackingOnFingers) {
				if (autoAdjustState == 0) step.tableSteps = stacker.setting.tableSteps;
				else {
					step.tableSteps = avgStepLastStack;//(stacker.setting.tableSteps + )/2;
				}
			}
			if (step.tableSteps>stacker.setting.tableSteps*1.1) step.tableSteps=stacker.setting.tableSteps*1.1;
			if (step.tableSteps<stacker.setting.tableSteps*0.9) step.tableSteps=stacker.setting.tableSteps*0.9;

			if (newStack) rest = 0;
			stepSize = 0.0625;
			tmp = step.tableSteps+rest;
			for (i=0;i<100;i++)
				{
				if ((tmp - stepSize) < 0) {
					rest = tmp;
					break;
				}
				else tmp -= stepSize;
			}
			if (deliverFly.stackingOnFingers)
			{
				if ((fingerY.pos == fingerY.maxPos) && FALSE == newStack) newStack = TRUE;
				fingerY.move = -((LREAL)(i)*stepSize);
			}
			else
			{
				stackerY.move = -((LREAL)(i)*stepSize);//-directAdjust;
			} 
			newStack = FALSE;
			step.newPageInStack = 0;
			//directAdjust = 0;
		}
	} 
	else //step.useStepSensor == 0
	{
		if (step.newPageInStack)
		{
			if (stackerY.pos == (stackerY.maxPos + stacker.setting.topPosOffset) && FALSE == newStack) {
				newStack = TRUE;
				skip = setting2.skipStackerSteps;
				cutter.command.deliverNextRequest = FALSE;
			}
			if (skip) skip--;
			else {
				if (newStack) rest = 0;
				stepSize = 0.0625;
				tmp = stacker.setting.tableSteps+rest;
				for (i=0;i<100;i++)
					{
					if ((tmp - stepSize) < 0) {
						rest = tmp;
						break;
					}
					else tmp -= stepSize;
				}
				if (deliverFly.stackingOnFingers) {
					if ((fingerY.pos == fingerY.maxPos) && FALSE == newStack) newStack = TRUE;
					fingerY.move = -((LREAL)(i)*stepSize);
				}
				else {
					stackerY.move = -((LREAL)(i)*stepSize);
				}
				newStack = FALSE;
			}
			step.newPageInStack = 0;
		}
	}
	if ((stackerY.maxPos + stacker.setting.topPosOffset - stackerY.pos) > stacker.setting.maxStackSize && !deliverFly.stackingOnFingers && deliverFly.state != 3 && !deliverFly.pendingDeliver) //command.stoppedDelivery = ON;
	{
		cutter.command.deliverNowRequestToggle = !cutter.command.deliverNowRequestToggle;
		strcpy(dbgPrintStr, "*S50 DELIVER NOW REQUEST TOGGLE (by stack size)");
		strcpy(ULog.text[ULog.ix++], dbgPrintStr);
	}
	lastStackPos = stackerY.pos;
}
*/

void evalSoftStop (void)
{
	if ((*sStop[readSoftStopStatus])(0)) sheetHandlerLimiter(SET_SPEED,SOFTSTOP,STOP);
	else sheetHandlerLimiter(SET_SPEED,SOFTSTOP,MAX_SPEED);
}

void resetTransport(void)
{
	INT i;
	for (i=0;i<TRANS_SPEED_QUEUE_SIZE;i++) transportFloorSpeed[i].speed = transportFloorSpeed[i].timer = 0.0;
}

void addError(USINT errorNo,USINT stopScenario,BOOL unload, UINT subType)
{
   if (errorNo == LIGHT_CURTAIN) lightCurtainErrTriggerd = TRUE;
   //if (errorNo == WASTE_BOX_FULL) wasteBoxFullErrorTriggerd = TRUE;
   
	error[errorNo].status = ERR;
	error[errorNo].stopType = stopScenario;
	error[errorNo].unload = unload;
	error[errorNo].subType = subType;
	
	machineError.active = TRUE;
	machineError.errType = (UINT)(errorNo) + 1000;
	machineError.errSubType = (UINT)(subType);	
	
	addErrorToStackerLastError(errorNo,subType);
}


void addErrorToStackerLastError(USINT typ,UINT subTyp)
{
	if (stacker.reading.lastErrors.pointer<9) stacker.reading.lastErrors.pointer++;
	else stacker.reading.lastErrors.pointer = 0;
	switch (typ)
	{
		case NOT_USED0:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Not Used Error 0");
			break;
		case TRANSPORT_OUTFEED:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Transport outfeed");
			break;
		case AXIS:
			if (subTyp == 0) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Axis Aligner Transport");
			if (subTyp == 1) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Axis Deliver Transport");
			if (subTyp == 2) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Axis Fork X (in/out)");
			if (subTyp == 3) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Axis Fork T (up/down)");
			if (subTyp == 4) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Axis End Stop");
			if (subTyp == 5) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Axis Stacker Lift");
			break;
		case DELIVER:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Deliver fault");	
			break;
		case TOP_COVER_BYPASSED:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Top Cover bypassed (Internal error)");
			break;
		case TRANSPORT_INFEED:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Transport Infeed");
			break;
		case QUEUE_ERROR:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Queue error");
			break;
		case AXIS_NOT_HOMED:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Axis not homed");
			break;
		case DRIVES_DISABLED:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Drives Disabled");
			break;
		case E_STOP_BY_ME:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"E-stop (by me)");
			break;
		case WASTE_BOX_FULL:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Wastebox full");
			break;
		case LIGHT_CURTAIN:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Light curtain");
			break;
		case GRIPPER_NOT_READY:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Gripper not ready");
			break;
		case STACKER_INFEED_CRASH:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Stacker infeed crash");
			break;
		case ALIGNER_ROLLER_GATE:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Aligner roller gate");
			break;
		case INFEED_ROLLER_GATE:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Infeed roller gate");
			break;
		case TABLE_NOT_CLEAR:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Table not clear");
			break;
		case NO_POWER:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"No Power");
			break;
		case COVER_OPEN:
			if (subTyp == 0) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (during Load)");
			else if (subTyp == 1) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (Deliver speed over 0.1 m/s)");
			else if (subTyp == 2) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (Transport speed over 0.6 m/s)");
			else if (subTyp == 3) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (StackerMoving function triggerd)");
			else if (subTyp == 4) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (Pending Deliver)");
            else if (subTyp == 5) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (feed pressed, wrong situation)");
            else if (subTyp == 6) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (from safety)");
            else if (subTyp == 7) strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (in not loaded)");
			else strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Cover Open (unknown trigger)");
			break;
		case SEC_UNIT_NOT_READY:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Sec Unit Not Ready");
			break;
		case STACKER_BUSY:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Stacker Busy");
			break;
		case NOT_USED1:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Not Used Error 2");
			break;
		case NOT_USED2:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Not Used Error 3");
			break;
		case NOT_USED3:
			strcpy(stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].text,"Not Used Error 4");
			break;
		default:
			break;
	}
	stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].typ = typ;
	stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].subTyp = subTyp;
	stacker.reading.lastErrors.error[stacker.reading.lastErrors.pointer].timeStamp = lineController.reading.timeSinceStart;
}

void resetCommands(void)
{
	command.stoppedDelivery = 0;
	command.load = 0;
	command.findZero = 0;
}

BOOL resetTransportErrors(void)
{
	if (stacker.reading.axisStatus.transport.errorID>0) {
		transport.Command.ErrorAcknowledge = TRUE;
		transport.Command.MoveVelocity = FALSE;
		addDebugNote(21,(REAL)(stacker.reading.axisStatus.transport.errorID),0,0,0);
		return TRUE;
	}
	return FALSE;
}

/*BOOL resetDeliverErrors(void)
{
	return FALSE;
	if (stacker.reading.axisStatus.transport.errorID>0) {
		deliverTransport.Command.ErrorAcknowledge = TRUE;
		deliverTransport.Command.MoveVelocity = FALSE;
		resetMoveVelocityFlagDeliver = TRUE;
		deliverTransport.Command.Power = OFF;
		addDebugNote(21,(REAL)(stacker.reading.axisStatus.deliver.errorID),0,0,1);
		return TRUE;
	}
	else if (deliverTransport.Command.Power == OFF)
	{
		deliverTransport.Command.Power = ON;
	}
	
	return FALSE;
}*/
//Stopped delivery and Load

void resetParameters(BOOL powerOff, BOOL stoppedDelivery)
{
   if (powerOff) {
      fingerPusher.Command.Power = OFF;
      fingerLift.Command.Power = OFF;
      stackerLift.Command.Power = OFF;
      stopPlate.Command.Power = OFF;
   }
   unHome();
   pageInTransportQueue(INIT_QUEUE);
   cutter.command.deliverNextRequest = FALSE;
   resetCommands();
   //gripper.command.stop = TRUE;
   if (stoppedDelivery) warnings[STOPPEDDELIVERING] = FALSE;//stacker.reading.status.warning = FALSE;
}

void setForkStandardSpeed(void)
{
    fingerX.acceleration              = 2.0;
    fingerX.deceleration              = 2.0;
    fingerX.speed                     = 1.5;
}

BOOL load (BOOL forceHome, BOOL deliverWaste, BOOL stoppedDelivery, BOOL findZero, BOOL homeOnly) 
{
    static REAL distance;
    static BOOL stackDeliverFlag;
    static REAL stackDeliverTiming;
    static REAL stopPlateTargetPos;
    static BOOL sheetSignalEnterLoad;
    static USINT rstPosState;
    stackerInDeliver = FALSE;
    stacker.reading.SFTransCalc = 0;
   
    setForkStandardSpeed(); 
    /* check error */
    if (safety.reading.eStop) {
        resetParameters(1,stoppedDelivery);
        return EXIT_THIS;
    }
    if (!stacker.reading.input.topCoverClosed) {
        addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,0);
        resetParameters(0,stoppedDelivery);
        return EXIT_THIS;
    }
    if (!BALLRACK) {
        addError(ALIGNER_ROLLER_GATE,NORMAL_STOP,FORCE_RELOAD,1);
        resetParameters(0,stoppedDelivery);
        return EXIT_THIS;
    } 
    if (!stacker.reading.input.infeedGate) {
        addError(INFEED_ROLLER_GATE,NORMAL_STOP,FORCE_RELOAD,1);
        resetParameters(0,stoppedDelivery);
        return EXIT_THIS;
    }
    if (!stacker.reading.input.seqUnitNotReady) {
        addError(SEC_UNIT_NOT_READY,NORMAL_STOP,FORCE_RELOAD,0);
        resetParameters(0,stoppedDelivery);
        return EXIT_THIS;
    }
    if ((globalPower == 0 && (!(stacker.setting.ignoreError_Dev & 0x01))) || (GlobalCommand.Input.Command.E_Stop == 1)) {
        addError(NO_POWER,NORMAL_STOP,FORCE_RELOAD,1);
        resetParameters(1,stoppedDelivery);
        return EXIT_THIS;
    }
    if (lastState != loadingState) {
        loadingStateTimer = 0.0;
        lastState = loadingState;
    }
    else loadingStateTimer += SECONDS_UNIT;
	
    if (loadingStateTimer > (10.0 - (stoppedDelivery?(5.0):(0.0) + (loadingState==0?(5.0):(0.0))))) {
        if (loadingState == 1) {
            addError(DELIVER,NORMAL_STOP,FORCE_RELOAD,100+homeAx);
            reHome = 1;
            //101 transport (aligner)
            //102 deliver (deliverBelt)
            //104 fork pusher (finger X)
            //108 fork lift (finger Y)
            //116 stop plate (end Stop X)
            //132 stacker lift (stacker elevator Y)
            errorAckAll(0);
        }
        else {
            if (loadingState == tableUp) {
                UINT x = 7;	
                if (!(clear(DELIVERYTABLE_SENSORS,ticksPerSecond/10))) x += 1;
                if (!(stacker.reading.input.lightCurtain)) x += 2;
                if (!(rabs(stopPlateTargetPos-stopPlateX.actualPositon) < 10)) x += 4;
				
                if (!stacker.reading.input.lightCurtain) {
                    addError(LIGHT_CURTAIN,NORMAL_STOP,FORCE_RELOAD,0);
                    reHome = 1;
                }
                else {
                    addError(DELIVER,NORMAL_STOP,FORCE_RELOAD,x);
                    reHome = 1;
                }
            }
            if (loadingState == deliverStackStart) {
                if      (!stacker.reading.input.tableClearSensor1) addError(TABLE_NOT_CLEAR,NORMAL_STOP,FORCE_RELOAD,1);
                else if (!stacker.reading.input.tableClearSensor2) addError(TABLE_NOT_CLEAR,NORMAL_STOP,FORCE_RELOAD,2);
                else if (!stacker.reading.input.tableClearSensor3) addError(TABLE_NOT_CLEAR,NORMAL_STOP,FORCE_RELOAD,3);
                else if (!stacker.reading.input.tableClearSensor4) addError(TABLE_NOT_CLEAR,NORMAL_STOP,FORCE_RELOAD,4);
                else if (!stacker.reading.input.lightCurtain) {
                    addError(TABLE_NOT_CLEAR,NORMAL_STOP,FORCE_RELOAD,5);
                    reHome = 1;
                }
                else                                               addError(TABLE_NOT_CLEAR,NORMAL_STOP,FORCE_RELOAD,0);
            }	
            else {
                addError(DELIVER,NORMAL_STOP,FORCE_RELOAD,loadingState);
                reHome = 1;
            }
        }
        infeedTransport.speed = 0;
        deliverTableBelt.speed = 0;
        //STACK_DELIVER_GRIPPER = OFF;
        //setOutput(&stackDeliverGripper,OFF);
        output.stackDeliverGripper = FALSE;
        loadingStateTimer = 0.0;
        setAxis();
        cutter.command.deliverNextRequest = FALSE;
        lastState = loadingState;
        resetCommands();
        if (stoppedDelivery) warnings[STOPPEDDELIVERING] = FALSE;//stacker.reading.status.warning = FALSE;
        return EXIT_THIS;
    }
    if (command.load || command.findZero) { //init
        addDebugNote(4,5,0,command.load,command.findZero);
        if (forceHome) {
            if (stacker.setting.automaticEndStop) homeAx=ALL;
            else homeAx=NO_STOP_PLATE;
            safety.command.resetStackerLightCurtain = TRUE;
            //gripper.command.home = TRUE;
        }
        errorAckAll(0);
        lastState = loadingState = initial;
        loadingStateTimer = -8.0;
        deliverFly.state = 0;
        firstTimeAfterLoad = 1;
        pagesAfterForkStacking = 3;
        pageInTransportQueue(INIT_QUEUE);
        sheetSignalEnterLoad = cutter.reading.sheetSignal;
        //setOutput(&stackDeliverGripper,OFF);
        output.offsetNorthGripper = FALSE;
        //
        grip.cmd.home.set = 1;
        autoAdjustState = 0;
        addDebugNote(4,10,0,command.load,command.findZero);
    }
    else if (command.stoppedDelivery && stoppedDelivery) {
        loadingState = clearTransportStart;
    }
    resetCommands();
    homeAx = homeAxis(homeAx);
    sneakyPage = FALSE;
    stackerInDeliver = TRUE;
    if (homeAx == 0) {
        switch (loadingState)
        {
            case initial: //0
            case home: //1
                //stackerY.absMove = 0;
                stackerY.absMove = stacker.setting.deliveryPosition;
                if (lineController.setup.stacker.spaghettiBelts) {
                fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                   offsetControl(SOUTH);
                   holdBackControl(SOUTH);
                   stacker.reading.offsetState = DWELL_SOUTH;
                }
                else
                fingerX.absMove = 0;
                fingerY.absMove = 0;
                
                //if (rabs(stacker.reading.axisStatus.transport.pos)>1000) {//reset it
                    if (0 == rstPosState) {
                        transport.Command.Power = OFF;
                        transport.Command.MoveVelocity = OFF;
                        rstPosState = 1;
                    }
                    else if (1 == rstPosState) {
                        transport.Command.Power = ON;
                        rstPosState = 2;
                    }
                    else if (2 == rstPosState) {
                        transport.Command.Home = ON;
                        rstPosState = 3;
                    }
                    else if (3 == rstPosState) {
                        loadingState = clearTransportStart;
                        rstPosState = 0;
                    }
                /*}
                else
                    loadingState = clearTransportStart;*/
                
                
                
                //loadingState = clearTransportStart;
                resetTransportErrors();
                //resetDeliverErrors();
                //deliverFly.state = waitOnStart;
                deliverFly.state = 0;
                deliverFly.stackingOnFingers = FALSE;
                makeGripperReadyCmd = TRUE;
                if (homeOnly) loadingState = exit;
                break;
            case clearTransportStart: //2
                stacker.reading.offsetsInStack = 0;
                stacker.reading.deliveriesSinceLoad = 0;
                reHome = 0;
                if (inSpeed.is > 0.03) break; //wait..
				
                if (lineController.setup.stacker.spaghettiBelts) {
                    fingerX.absMove = FINGER_START_POS_SPAGHETTI;
                    offsetControl(SOUTH);
                    holdBackControl(SOUTH);
                }
				
                sheetHandlerLimiter(SET_SPEED,DELIVERY,STOP);
                transportFloorSpeed[STOPPED_DELIVERY_MINSPEED].timer = 0.1;
                cutter.command.stopDeliverRequest = FALSE;
                //STACK_DELIVER_GRIPPER = OFF;
                //try go to -4.0 MAC
                //fingerX.absMove = -4.0;
                //setOutput(&stackDeliverGripper,OFF);
                output.stackDeliverGripper = FALSE;
                distance = infeedTransport.actualPosition;
                infeedTransport.speed = 0.8;
                if (!resetTransportErrors()) loadingState = clearTransportEnd;
                stackDeliverFlag = OFF;
                clear(RESET_CLEAR);
                if (STEP_SENSOR) stackerY.move = -1.0; //make room for the sheets
                /*X3.DO_3 = FALSE;
                X3.DO_2 = TRUE;
                X3.DO_4 = FALSE;*/
                break;
            case clearTransportEnd: //3
                if ((infeedTransport.actualPosition - distance) > (TRANSPORT_TABLE_LENGTH + 35.0)) {
                    if ((clear(TRANSPORTTABLE_SENSORS,ticksPerSecond/10) || (stacker.setting.ignoreError_Dev & 0x02)) || (loadingStateTimer>5 && TRANSPORT_TABLE_EXIT_1 == 0)) { //????
                        if (!stackDeliverFlag) {
                            stackDeliverTiming = 0;
                            //gripper.command.index = TRUE;
                        }
                        stackDeliverFlag = ON;		
                        stackDeliverTiming += SECONDS_UNIT;
                        //STACK_DELIVER_GRIPPER = ON;
                        //setOutput(&stackDeliverGripper,ON);!()
                        output.stackDeliverGripper = FALSE;
                        if (stackDeliverTiming>0.300) {
                            loadingState = tableToDeliverPosition; //wait 300 ms, leave this state 
                        }
                    }
                }
                if (deliverTransport.Status.ErrorID == 29207) deliverTransport.Command.ErrorAcknowledge = 1;
                break;
            case tableToDeliverPosition: //4
                if (stoppedDelivery) warnings[STOPPEDDELIVERING] = TRUE;//stacker.reading.status.warning = TRUE;
                if (stacker.setting.automaticEndStop) {
                    if (JobControlExit.sheetSize < 150) {
                        stopPlateX.absMove = 150 - BACKSTOP_OFFSET; // min sheet length 150 mm
                    }
                    else {
                        stopPlateX.absMove = JobControlExit.sheetSize - BACKSTOP_OFFSET;
                    }
                    stopPlateTargetPos = stopPlateX.absMove;
                }
                else stopPlateTargetPos = stopPlateX.actualPositon;
                infeedTransport.speed = 0.0;
                if (deliverWaste)
                    stackerY.absMove = 0.0;
                else
                    stackerY.absMove = stacker.setting.deliveryPosition;
                //STACK_DELIVER_GRIPPER = OFF;
                //setOutput(&stackDeliverGripper,OFF);
                output.stackDeliverGripper = FALSE;
                deliverTableBelt.speed=0.01;
                loadingState = deliverStackStart;
                clear(RESET_CLEAR);
                distance = deliverTableBelt.actualPosition;
                //resetDeliverErrors();
                //deliverStackCnt = 0;
                break;
            case deliverStackStart: //5
                if (stoppedDelivery) warnings[STOPPEDDELIVERING] = TRUE;//stacker.reading.status.warning = TRUE;
                /*if (deliverWaste)
                deliverTableBelt.speed=-0.6;
                else*/
                if (!stacker.reading.input.seqUnitNotReady)
                deliverTableBelt.speed=0;
                else if (loadingStateTimer>0.25) deliverTableBelt.speed= stacker.setting.deliverTransportSpeed;
                     
				
                if (clear(DELIVERYTABLE_SENSORS,ticksPerSecond)) {
                loadingState = deliverStackEnd; 					// checks paper on table
                //addDebugNote(7,deliverTableBelt.speed,stacker.setting.deliverTransportSpeed,deliverWaste,0);
                pageInTransportQueue(INIT_QUEUE);
                if (stacker.reading.axisStatus.deliver.actualSpeed == 0) resetMoveVelocityFlagDeliver = TRUE;
                }
                step.stackPages = 0;
            
                //if (loadingStateTimer>0.25 && loadingStateTimer<0.3  && deliverTableBelt.actualSpeed<0.05) deliverTableBelt.speed = 0;
                infeedTransport.speed=deliverTableBelt.speed*2;
                break;
            case deliverStackEnd: //6
                if (stoppedDelivery) warnings[STOPPEDDELIVERING] = TRUE;//stacker.reading.status.warning = TRUE;
                if (rabs((deliverTableBelt.actualPosition - distance)) > (stacker.setting.deliveryRunDistance)) loadingState = tableUp;
                else deliverTableBelt.speed=stacker.setting.deliverTransportSpeed<0.1?0.1:stacker.setting.deliverTransportSpeed*1.07;
                if (stacker.reading.axisStatus.deliver.actualSpeed == 0 && loadingStateTimer<12) loadingStateTimer = 12; // timeout faster 
                clear(RESET_CLEAR);
                stacker.reading.stackTableEmpty = TRUE;
                break;
            case tableUp: //7
                if (stoppedDelivery) warnings[STOPPEDDELIVERING] = TRUE; 
                if (!stacker.setting.automaticEndStop) 
                    stopPlateTargetPos = stopPlateX.actualPositon;
                if (clear(DELIVERYTABLE_SENSORS,ticksPerSecond/10) 
                    && stacker.reading.input.lightCurtain 
                && rabs(stopPlateTargetPos-stopPlateX.actualPositon) < 10) {
            
                    //stackerY.absMove = STACKER_TOPPOS;
                    if (findZero) {
                        //stackerY.absMove = stackerY.maxPos - fingerYMaxPosInit + stacker.setting.StackerFingerZeroAdjust;//STACKER_TOPPOS - fingerY.maxPos + stacker.setting.StackerFingerZeroAdjust;
                        stackerY.absMove = stacker.setting.topPosOffset - fingerY.maxPos + stacker.setting.StackerFingerZeroAdjust;
                        fingerY.absMove = 0;
                        fingerX.absMove = 140;
                        addDebugNote(20,stackerY.absMove,0,0,findZero);
                        unlockTimerCnt = 500;
                        deliverTableBelt.speed=0.0;
                    }
                    else 
                    {
                        //stackerY.absMove = stackerY.maxPos + stacker.setting.topPosOffset; //GUI.settings.S50_topPosition;   //STACKER_TOPPOS;
                        stackerY.absMove = stacker.setting.topPosOffset; //GUI.settings.S50_topPosition;   //STACKER_TOPPOS;
                        elevatorForkDebug[8] = stackerY.absMove;
                        addDebugNote(20,stackerY.absMove,stacker.setting.topPosOffset,stackerY.maxPos,findZero);
                        unlockTimerCnt = 500;
                        deliverTableBelt.speed=0.0;
                    }
                    loadingState = exit;
                }
                if (!findZero) fingerY.absMove = fingerY.maxPos;// - stacker.setting.topPosOffset;
                if (deliverTableBelt.speed != 0.0)
                    deliverTableBelt.speed=stacker.setting.deliverTransportSpeed*0.25;
                deliverFly.pendingDeliver = FALSE;
                sheetHandlerLimiter(SET_SPEED,DELIVERY,MAX_SPEED);
                stackerIsClear = 1;
                stackerInDeliver = FALSE;
                infeedTransport.speed=deliverTableBelt.speed*2;
                break;
            case monitorTableUp:
                if (!stacker.reading.input.lightCurtain) {
                    stackerY.haltMovement = 1;
                    addError(LIGHT_CURTAIN,NORMAL_STOP,FORCE_RELOAD,1);
                    loadingState = exit;
                    reHome = 1;
                }
                if (loadingStateTimer>3.0) loadingState = exit;
                //if (rabs((stackerY.maxPos + stacker.setting.topPosOffset /*GUI.settings.S50_topPosition*/) - stackerY.actualPositon) < 10) loadingState = exit;
                if (rabs((stacker.setting.topPosOffset /*GUI.settings.S50_topPosition*/) - stackerY.actualPositon) < 10) loadingState = exit;
                deliverTableBelt.speed=0.0;
                infeedTransport.speed=deliverTableBelt.speed*2;
                break;
			
            default:
                loadingState = exit;
                break;
        }
        if (loadingState == exit) { //9
            setAxis();
            step.stackPages = 0;
            if (stoppedDelivery) warnings[STOPPEDDELIVERING] = FALSE;//stacker.reading.status.warning = FALSE;
            //STACK_DELIVER_GRIPPER = OFF;
            //setOutput(&stackDeliverGripper,OFF);
            output.stackDeliverGripper = FALSE;
            lastState = loadingState;
            loadingStateTimer = 0.0;
            stackerInDeliver = FALSE;
            pageInTransportQueue(INIT_QUEUE);
            cutter.command.deliverNextRequest = FALSE;
            stopDeliverToExit = FALSE;
            sheetHandlerLimiter(SET_SPEED,DELIVERY,MAX_SPEED);
            addDebugNote(2,0,0,0,0);
            spaghettiStep.tuneState = 0;
            curlDistance = 30;
            //pageInPocket = 0;
            lastOffset = stackerCounter.offsets;
            setWarningFlag(RST);
            if (stoppedDelivery) stackerCounter.stoppedDeliveries++;
            else stackerCounter.loads++;
            if (sheetSignalEnterLoad != cutter.reading.sheetSignal) addDebugNote(60,0,0,0,cutter.reading.sheetSignal);
            sheetHandlerLimiter(SET_SPEED,DELIVER_ON_FLY,MAX_SPEED);
            return EXIT_THIS;
        }
        else {
            setAxis();
            //lastState = loadingState;
            return REPEAT;
        }
    }
    else {
        if (loadingStateTimer > (-14)) output.stackDeliverGripper = TRUE;  //setOutput(&stackDeliverGripper,ON);
        loadingState = home;
        lastState = loadingState;
        //STACK_DELIVER_GRIPPER = OFF;
        //setOutput(&stackDeliverGripper,OFF);
        output.stackDeliverGripper = FALSE;
        return REPEAT;
    }
}

void _EXIT ProgramExit(void)
{
   //OVER AND OUT
}

void drivePower(BOOL onOff)
{
	transport.Command.Power = onOff;
	infeedTransport.homed	= onOff;
  
	//deliverTransport.Command.Power = onOff;
	//deliverTableBelt.homed = onOff;
  
	if (fingerX.homed && !onOff) fingerX.homed = 0;
	fingerPusher.Command.Power = onOff;
  
	if (fingerY.homed && !onOff) fingerY.homed = 0;
	fingerLift.Command.Power = onOff;
  
	if (stopPlateX.homed && !onOff) stopPlateX.homed = 0;
	stopPlate.Command.Power = onOff;
  
	if (stackerY.homed && !onOff) stackerY.homed = 0;
	stackerLift.Command.Power = onOff;
  
	drivePowerStatus = onOff;
}

DWORD homeAxis(DWORD ax) {
   DWORD retval = ax;
   sheetHandlerLimiter (SET_SPEED,HOMING,STOP);
   static USINT resetAfterLC;
   static USINT processCmd;
    
   if (readBit(TRANS,ax)) //transport
   {
      if (transport.Command.Power && transport.Status.DriveStatus.HomingOk && stacker.reading.axisStatus.transport.homeState==3) {
         if (transport.Status.ErrorID != 0) transport.Command.ErrorAcknowledge = 1;
         setBit(TRANS,&homeErrorAx);
         infeedTransport.homed = 1;
         transport.Command.Home = 0;
      }
		
      switch (homeAxisState[TRANS]) {
         case 0:
            transport.Command.MoveVelocity=OFF;
            transport.Command.Power = ON;
            homeAxisState[TRANS] = 1;
            infeedTransport.homed = 0;
            stacker.reading.axisStatus.transport.homeState = 0;
            break;
         case 1:
            if (transport.Status.ErrorID != 0 /* == 29205*/) // movement before homing attempt
               transport.Command.ErrorAcknowledge = 1;
            else {
               transport.Command.Home = ON;
               homeAxisState[TRANS] = 2;
               homingAxisTimer[TRANS] = 0;
            }
            stacker.reading.axisStatus.transport.homeState = 1;
            break;
         case 2:
            homingAxisTimer[TRANS] += SECONDS_UNIT;
            stacker.reading.axisStatus.transport.homeState = 2;
            if (transport.Status.DriveStatus.HomingOk == 1) {
               clearBit(TRANS,&retval);
               homeAxisState[TRANS] = 0;
               infeedTransport.homed = 1;
               transport.Command.MoveVelocity=ON;
               stacker.reading.axisStatus.transport.homeState = 3;
               transport.Command.Home = 0;
            }	
            else if (transport.Status.DriveStatus.AxisError == 1) {
               homeAxisState[TRANS] = 0;
               setBit(TRANS,&homeErrorAx);
               infeedTransport.homed = 0;
               addError(AXIS,NORMAL_STOP,FORCE_RELOAD,TRANS);
			   reHome = 1;
               stacker.reading.axisStatus.transport.homeState = 4;
               transport.Command.Home = 0;
            }
            if (homingAxisTimer[TRANS]>4) {
               homeAxisState[TRANS] = 0;
               transport.Command.Power = OFF;
               transport.Command.Home = OFF;
               transport.Command.ErrorAcknowledge = OFF;
            }
            break;
         default:
            break;
      }
   }
   if (readBit(DELIV,ax)) //deliverTransport
   {
      stacker.reading.axisStatus.deliver.homeState = 3;
      deliverTableBelt.homed = 1;
      clearBit(DELIV,&retval);
      homeAxisState[DELIV] = 0;
   }
   if (readBit(FPUSH,ax)) //fingerPusher
   {
      switch (homeAxisState[FPUSH]) {
         case 0:
            fingerX.homed = 0;
            fingerPusher.Command.MoveVelocity = OFF;
            fingerPusher.Command.Power = ON;
            homeAxisState[FPUSH] = 1;
            fingerX.inPosition = 0;
            fingerX.standStillTime = fingerX.homeSettleTime; // stand still after homing in (s)
            stacker.reading.axisStatus.forkPush.homeState = 0;
            break;
         case 1:
            if (fingerPusher.Status.ErrorID != 0 /* == 29205*/) // movement before homing attempt
               fingerPusher.Command.ErrorAcknowledge = 1;
            else {
               fingerPusher.Command.Home = ON;
               homeAxisState[FPUSH] = 2;
               homingAxisTimer[FPUSH] = 0;
            }
            stacker.reading.axisStatus.forkPush.homeState = 1;
            break;
         case 2:
            homingAxisTimer[FPUSH] += SECONDS_UNIT;
            stacker.reading.axisStatus.forkPush.homeState = 2;
            if (fingerPusher.Status.DriveStatus.HomeSwitch == 1 && fingerPusher.Status.DriveStatus.AxisError == 0
            && deliverTransport.Status.DriveStatus.HomingOk == 1 && !fingerX.inMovement) {
               fingerX.standStillTime -= SECONDS_UNIT;
               if (fingerX.standStillTime < 0.0) {
                  fingerX.homed = 1;
                  clearBit(FPUSH,&retval);
                  fingerX.move = fingerX.pos = fingerX.absMove = fingerPusher.Status.ActPosition;
                  homeAxisState[FPUSH] = 0;
                  fingerX.inPosition = 1;
                  stacker.reading.axisStatus.forkPush.homeState = 3;
               }
            }	
            else if (fingerPusher.Status.DriveStatus.AxisError == 1) {
               homeAxisState[FPUSH] = 0;
               setBit(FPUSH,&homeErrorAx);
               fingerX.homed = 0;
               addError(AXIS,NORMAL_STOP,FORCE_RELOAD,FPUSH);
			   reHome = 1;
               stacker.reading.axisStatus.forkPush.homeState = 4;
            }
            break;
         default:
            break;
      }
   }
   if (fingerX.homed && fingerPusher.Status.ActPosition<10.0 && fingerX.pos<10.0)
   {
      if (readBit(FLIFT,ax)) //fingerLift
      {
         switch (homeAxisState[FLIFT]) {
            case 0:
               fingerY.homed = 0;
               fingerLift.Command.MoveVelocity = OFF;
               fingerLift.Command.Power = ON;
               homeAxisState[FLIFT] = 1;
               fingerY.inPosition = 0;
               fingerY.standStillTime = fingerY.homeSettleTime; // stand still after homing in (s)
               stacker.reading.axisStatus.forkElevator.homeState = 0;
               break;
            case 1:
               stacker.reading.axisStatus.forkElevator.homeState = 1;
               if (fingerLift.Status.ErrorID != 0 /* == 29205*/) // movement before homing attempt
                  fingerLift.Command.ErrorAcknowledge = 1; //clear it!
               else {
                  fingerLift.Command.Home = ON;
                  homeAxisState[FLIFT] = 2;
                  homingAxisTimer[FLIFT] = 0;
               }
               break;
            case 2:
               homingAxisTimer[FLIFT] += SECONDS_UNIT;
               stacker.reading.axisStatus.forkElevator.homeState = 2;
               if (fingerLift.Status.DriveStatus.HomeSwitch == 1 && fingerLift.Status.DriveStatus.AxisError == 0
               && deliverTransport.Status.DriveStatus.HomingOk == 1 && !fingerY.inMovement) {
                  fingerY.standStillTime -= SECONDS_UNIT;
                  if (fingerY.standStillTime < 0.0) {
                     fingerY.homed = 1;
                     clearBit(FLIFT,&retval);
                     fingerY.move = fingerY.pos = fingerY.absMove = fingerLift.Status.ActPosition;
                     homeAxisState[FLIFT] = 0;
                     fingerY.inPosition = 1;
                     stacker.reading.axisStatus.forkElevator.homeState = 3;
                  }
               }	
               else if (fingerLift.Status.DriveStatus.AxisError == 1) {
                  homeAxisState[FLIFT] = 0;
                  setBit(FLIFT,&homeErrorAx);
                  fingerY.homed = 0;
                  addError(AXIS,NORMAL_STOP,FORCE_RELOAD,FLIFT);
				  reHome = 1;
                  stacker.reading.axisStatus.forkElevator.homeState = 4;
               }
               break;
            default:
               break;
         }
      }
      if (readBit(STOPPL,ax)) //stopPlate
      {
         switch (homeAxisState[STOPPL]) {
            case 0:
               stopPlateX.homed = 0;
               stopPlate.Command.MoveVelocity = OFF;
               stopPlate.Command.Power = ON;
               homeAxisState[STOPPL] = 1;
               stopPlateX.inPosition = 0;
               stopPlateX.standStillTime = stopPlateX.homeSettleTime; // stand still after homing in (s)
               stacker.reading.axisStatus.endStop.homeState = 0;
               break;
            case 1:
               stacker.reading.axisStatus.endStop.homeState = 1;
               if (stopPlate.Status.ErrorID != 0 /* == 29205*/) // movement before homing attempt
                  stopPlate.Command.ErrorAcknowledge = 1; //clear it!
               else {
                  stopPlate.Command.Home = ON;
                  homeAxisState[STOPPL] = 2;
                  homingAxisTimer[STOPPL] = 0;
               }
               break;
            case 2:
               homingAxisTimer[STOPPL] += SECONDS_UNIT;
               stacker.reading.axisStatus.endStop.homeState = 2;
               if (stopPlate.Status.DriveStatus.HomeSwitch == 1 && stopPlate.Status.DriveStatus.AxisError == 0
               && deliverTransport.Status.DriveStatus.HomingOk == 1 && !stopPlateX.inMovement) {
                  stopPlateX.standStillTime -= SECONDS_UNIT;
                  if (stopPlateX.standStillTime < 0.0) {
                     stopPlateX.homed = 1;
                     clearBit(STOPPL,&retval);
                     stopPlateX.move = stopPlateX.pos = stopPlateX.absMove = stopPlate.Status.ActPosition;
                     homeAxisState[STOPPL] = 0;
                     stopPlateX.inPosition = 1;
                     stacker.reading.axisStatus.endStop.homeState = 3;
                  }
               }	
               else if (stopPlate.Status.DriveStatus.AxisError == 1) {
                  homeAxisState[STOPPL] = 0;
                  setBit(STOPPL,&homeErrorAx);
                  stopPlateX.homed = 0;
                  addError(AXIS,NORMAL_STOP,FORCE_RELOAD,STOPPL);
				  reHome = 1;
                  stacker.reading.axisStatus.endStop.homeState = 4;
               }
               break;
            default:
               break;
         }
      }
      if (readBit(SLIFT,ax)) //stackerLift
      {
         switch (homeAxisState[SLIFT]) {
            case 0:

               //if (resetAfterLC < 3 && lightCurtainStopTriggerd)
               if (lightCurtainErrTriggerd) {
                  if (processCmd == 0) {
                     switch (resetAfterLC) {
                        case 0:
                           stackerLift.Command.Power = OFF;
                           processCmd=3;
                           resetAfterLC = 1;
                           break;
                        case 1:
                           stackerLift.Command.ErrorAcknowledge = 1;
                           processCmd=3;
                           resetAfterLC = 2;
                           break;
                        case 2:
                           stackerLift.Command.ErrorAcknowledge = 1;
                           processCmd=3;
                           resetAfterLC = 3;
                           break;
                        case 3:
                           stackerLift.Command.ErrorAcknowledge = 1;
                           processCmd=3;
                           resetAfterLC = 4;
                           break;
                        case 4:
                           stackerLift.Command.ErrorAcknowledge = 1;
                           processCmd=3;
                           resetAfterLC = 5;
                           break;
                        case 5:
                           stackerLift.Command.ErrorAcknowledge = 1;
                           processCmd=1;
                           resetAfterLC = 6;
                           break;
                        default:
                           lightCurtainErrTriggerd = 0;
                           resetAfterLC = 0;
                           break;
                     }
                  }
                  else processCmd--;
               }
               else {
                  stackerY.homed = 0;
                  stackerLift.Command.MoveVelocity = OFF;
                  stackerLift.Command.Power = ON;
                  homeAxisState[SLIFT] = 1;
                  stackerY.inPosition = 0;
                  stackerY.standStillTime = stackerY.homeSettleTime; // stand still after homing in (s)
                  stacker.reading.axisStatus.stackerElevator.homeState = 0;
               }
               break;
            case 1:
               if (stackerLift.Status.ErrorID == 35201) {
                  addError(LIGHT_CURTAIN,NORMAL_STOP,FORCE_RELOAD,2);
				  reHome = 1;
                  stackerLift.Command.ErrorAcknowledge = 1; //clear it!
               }
               else if (stackerLift.Status.ErrorID != 0 /* == 29205*/) {// movement before homing attempt
                  stackerLift.Command.ErrorAcknowledge = 1; //clear it!
               }
               else {
                  stackerLift.Command.Home = ON;
                  homeAxisState[SLIFT] = 2;
                  homingAxisTimer[SLIFT] = 0;
               }
               stacker.reading.axisStatus.stackerElevator.homeState = 1;
               break;
            case 2:
               homingAxisTimer[SLIFT] += SECONDS_UNIT;
               stacker.reading.axisStatus.stackerElevator.homeState = 2;
               if (/*stackerLift.Status.DriveStatus.HomeSwitch == 1 &&*/ stackerLift.Status.DriveStatus.AxisError == 0
               && deliverTransport.Status.DriveStatus.HomingOk == 1 && !stackerY.inMovement) {
                  stackerY.standStillTime -= SECONDS_UNIT;
                  if (stackerY.standStillTime < 0.0) {
                     stackerY.homed = 1;
                     clearBit(SLIFT,&retval);
                     stackerY.move = stackerY.pos = stackerY.absMove = stackerLift.Status.ActPosition;
                     homeAxisState[SLIFT] = 0;
                     resetAfterLC = 0;
                     stackerY.inPosition = 1;
                     stacker.reading.axisStatus.stackerElevator.homeState = 3;
                  }
               }	
               else if (stackerLift.Status.DriveStatus.AxisError == 1) {
                  homeAxisState[SLIFT] = 0;
                  resetAfterLC = 0;
                  setBit(SLIFT,&homeErrorAx);
                  stackerY.homed = 0;
                  addError(AXIS,NORMAL_STOP,FORCE_RELOAD,SLIFT);
				  reHome = 1;
                  stacker.reading.axisStatus.stackerElevator.homeState = 4;
               }
               break;
            default:
               break;
         }
      }	
   }
   if (retval==0) sheetHandlerLimiter (SET_SPEED,HOMING,MAX_SPEED);
   return retval;
}

void setAxis(void)
{	
	//if (stacker.setting.forceAxis) return;
	/****************************** INFEED TRANSPORT ******************************/
	setInfeedTransport();
	/************************ DELIVER TABLE TRANSPORT *****************************/
	setDeliverTableBelt();
	/**************************** FINGER PUSHER (X) *******************************/
	setFingerX();	
	/***************************** FINGER LIFT (Y) ********************************/
	setFingerY();
	/***************************** STOP PLATE (X) *********************************/
	setStopPlateX();
	/**************************** STACKER LIFT (X) ********************************/
	setStackerY();
}

void getAxis (void)
{ 
	/**************************** STACKER LIFT (X) ********************************/
	getStackerY();
	/***************************** STOP PLATE (X) *********************************/
	getStopPlateX();
	/**************************** FINGER PUSHER (X) *******************************/
	getFingerX();
	/***************************** FINGER LIFT (Y) ********************************/
	getFingerY();
	/****************************** INFEED TRANSPORT ******************************/
	getInfeedTransport();
	/************************ DELIVER TABLE TRANSPORT *****************************/
	getDeliverTableBelt();
	/*if (allAxisErrorAck)
	{
		allAxisErrorAck = 0;
		errorAckAll();
	}*/
}

void getDeliverTableBelt (void) 
{
	if (!deliverTableBelt.homed) return;
	deliverTableBelt.actualSpeed = deliverTransport.Status.ActVelocity / 1000;
	deliverTransport.Parameter.Acceleration = deliverTableBelt.acceleration * 1000;
	deliverTransport.Parameter.Deceleration = deliverTableBelt.deceleration * 1000;
	deliverTableBelt.actualPosition = deliverTransport.Status.ActPosition;
	stacker.reading.axisStatus.deliver.actualSpeed = deliverTableBelt.actualSpeed;
	stacker.reading.axisStatus.deliver.speed = deliverTableBelt.speed;
	stacker.reading.axisStatus.deliver.pos = deliverTableBelt.actualPosition;
	stacker.reading.axisStatus.deliver.errorID = deliverTransport.Status.ErrorID;
	if (stacker.reading.axisStatus.deliver.errorID != 0) stacker.reading.axisStatus.deliver.lastErrorID = stacker.reading.axisStatus.deliver.errorID;
}

void getInfeedTransport (void) 
{
	infeedTransport.actualSpeed = transport.Status.ActVelocity / 1000;
	if (transport.Status.ErrorID != 0) {
		infeedTransport.error = 1;
	}
	else if (transport.AxisState.ErrorStop)
		infeedTransport.error = 1;
	else 
		infeedTransport.error = 0;
	transport.Parameter.Acceleration = infeedTransport.acceleration * 1000;
	transport.Parameter.Deceleration = infeedTransport.deceleration * 1000;
	infeedTransport.actualPosition = transport.Status.ActPosition;
	stacker.reading.axisStatus.transport.actualSpeed = infeedTransport.actualSpeed;
	stacker.reading.axisStatus.transport.speed = infeedTransport.speed;
	stacker.reading.axisStatus.transport.pos = infeedTransport.actualPosition;
	stacker.reading.axisStatus.transport.errorID = transport.Status.ErrorID;
	if (stacker.reading.axisStatus.transport.errorID != 0) stacker.reading.axisStatus.transport.lastErrorID = stacker.reading.axisStatus.transport.errorID;
	//servoReady = transport.Status.DriveStatus.ControllerReady;
}

void setInfeedTransport (void) {
    REAL spd;
    static BOOL moveStarted;
    static REAL dist;
    static UINT zeroSpeedCnt;

    static dir lastDir;
    if (infeedTransport.homed == 0) {
        transport.Command.MoveVelocity=OFF;
        return;
    }
    if (lastDir != infeedTransport.direction)
    {
        transport.Command.MoveVelocity=OFF;
        lastDir = infeedTransport.direction;
    }
    else 
    {
        if (infeedTransport.speed > infeedTransport.maxSpeed) infeedTransport.speed = infeedTransport.maxSpeed;
        else if (infeedTransport.speed < infeedTransport.minSpeed) infeedTransport.speed = infeedTransport.minSpeed;
        spd = infeedTransport.speed;
        if (spd < 0.0) {
            spd = (-1) * spd;
            infeedTransport.direction = transport.Parameter.Direction = 1;
        }
        else infeedTransport.direction = transport.Parameter.Direction = 0;

        if (spd > 0 && infeedTransport.lastSpeed != infeedTransport.speed) {
            transport.Parameter.Velocity = spd*1000.0;
            transport.Command.MoveVelocity=ON;
            transport.Command.Power=ON;
            moveStarted = TRUE;
            //moveVelCnt = 3;
        }
        else {
            if (rabs(infeedTransport.actualSpeed - infeedTransport.speed) > 0.02) transport.Command.MoveVelocity=ON;
            else if (moveStarted) {
                transport.Command.MoveVelocity=ON;
                moveStarted = FALSE;
            }
            transport.Command.Power=ON;
            //moveVelCnt = 3;
            if (infeedTransport.speed == 0) transport.Parameter.Velocity = 0;
        }
        if (infeedTransport.actualSpeed > 0.05 && infeedTransport.speed == 0) {
            zeroSpeedCnt++;
            if (zeroSpeedCnt % 20 == 10) {
                transport.Command.MoveVelocity = ON;
            }
            if (zeroSpeedCnt == 100 && !stacker.reading.input.topCoverClosed) {
                transport.Command.Halt = ON;
            }
        }
        else zeroSpeedCnt = 0;
        //if (moveVelCnt) moveVelCnt--;
        //if (moveVelCnt == 1 && transport.Command.MoveVelocity==0) transport.Command.MoveVelocity=ON;
        infeedTransport.lastSpeed = infeedTransport.speed;
        
        if (OFF == transport.Command.Power) transport.Command.MoveVelocity = OFF;
    }
    dist = dist + infeedTransport.actualSpeed*SECONDS_UNIT;
    if (dist>1.0)
    {
        stackerCounter.transportTableTravelDistance++;
        dist = dist - 1.0;
    }
}


void setDeliverTableBelt (void) {
	static BOOL moveStarted;
   static REAL dist;
	//static dir lastDir;
	if (!deliverTableBelt.homed) return;
	if (deliverTableBelt.speed > deliverTableBelt.maxSpeed) deliverTableBelt.speed = deliverTableBelt.maxSpeed;
	else if (deliverTableBelt.speed < deliverTableBelt.minSpeed) deliverTableBelt.speed = deliverTableBelt.minSpeed;
    deliverTransport.Parameter.Direction = 0;
	if (deliverTableBelt.lastSpeed != deliverTableBelt.speed) {
		deliverTransport.Parameter.Velocity = deliverTableBelt.speed*1000.0;
      deliverTransport.Command.MoveVelocity=ON;
		moveStarted = TRUE;
	}
	else
	{
      if (rabs(deliverTableBelt.actualSpeed - deliverTableBelt.speed) > 0.02) deliverTransport.Command.MoveVelocity=ON;
      else if (moveStarted && deliverTransport.Command.MoveVelocity == OFF) {
         deliverTransport.Command.MoveVelocity=ON;
         moveStarted = FALSE;
      }
	  if (deliverTableBelt.speed == 0) deliverTransport.Parameter.Velocity = 0;
	}
   deliverTableBelt.lastSpeed = deliverTableBelt.speed;
   dist = dist + deliverTableBelt.actualSpeed*SECONDS_UNIT;
   if (dist>1.0)
   {
      stackerCounter.deliveryTableTravelDistance++;
      dist = dist - 1.0;
   }
}

void setFingerX (void) {

	if (STOP_ALL) {
		fingerPusher.Command.Power = OFF;
		fingerLift.Command.Power = OFF;
		stackerLift.Command.Power = OFF;
		stopPlate.Command.Power = OFF;
		unHome();
		return;
	}
   if (!fingerXmoved)
   {
      if (!stacker.reading.input.topCoverClosed && (fingerX.move != 0 || fingerX.absMove != fingerX.pos)) {
         addError(COVER_OPEN,NORMAL_STOP,FORCE_RELOAD,6);
		 reHome = 1;
         return;
      }
   }

	if (fingerX.move != 0) {
		/******limit check********/
		if ((fingerX.pos + fingerX.move) < fingerX.minPos) fingerX.move = fingerX.minPos - fingerX.pos;
		else if ((fingerX.pos + fingerX.move) > fingerX.maxPos) fingerX.move = fingerX.maxPos - fingerX.pos;
		if (fingerX.move == 0) return;
		/*****don't run into the stack****/
		if ((fingerX.pos + fingerX.move) > CRITICAL_FINGER_X_OUT_LIM && fingerY.pos < (TABLE_CRASH_LIMIT)) fingerX.move = CRITICAL_FINGER_X_OUT_LIM - fingerX.pos;
		if (fingerX.move == 0) return;
		// go on, make the move
		fingerX.inPosition = 0;
		fingerPusher.Command.Power=ON;
		fingerPusher.Parameter.Distance =  fingerX.move;
		fingerX.pos = fingerX.pos + fingerX.move;
		fingerX.absMove = fingerX.pos;
		fingerPusher.Command.MoveAdditive = ON; // GO
		fingerX.move = 0;
	}
	else if (fingerX.absMove != fingerX.pos)
	{
		/******limit check********/
		if (fingerX.absMove < fingerX.minPos) fingerX.absMove = fingerX.minPos;
		else if (fingerX.absMove > fingerX.maxPos) fingerX.absMove = fingerX.maxPos;
		if (fingerX.absMove == fingerX.pos) return;
		/*****don't run into the stack****/
		if (fingerX.absMove > CRITICAL_FINGER_X_OUT_LIM && fingerY.pos < (TABLE_CRASH_LIMIT)) fingerX.absMove = CRITICAL_FINGER_X_OUT_LIM;
		if (fingerX.absMove == fingerX.pos) return;
		// go on, make the move
		fingerX.inPosition = 0;
		fingerPusher.Command.Power=ON;
		fingerPusher.Parameter.Position = fingerX.pos = fingerX.absMove;
		fingerPusher.Command.MoveAbsolute = ON; //GO
	}
}

void getFingerX(void) {

	fingerX.actualPositon = fingerPusher.Status.ActPosition;
	if (fingerPusher.Status.ActVelocity > (0.02) || fingerPusher.Status.ActVelocity < (-0.02)) fingerX.inMovement = 1;
	else {
		if (fingerX.inMovement == 0 && fingerX.actualPositon == fingerX.pos && fingerX.homed) fingerX.inPosition = 1;
		fingerX.inMovement = 0;
	}
		
	fingerX.actualSpeed = fingerPusher.Status.ActVelocity/1000;
		
	if (fingerPusher.Status.ErrorID != 0) {
		fingerX.error = 1;
	}
	else if (fingerPusher.AxisState.ErrorStop)
		fingerX.error = 1;
	else 
		fingerX.error = 0;
		
	if (fingerX.error) fingerX.pos = fingerX.actualPositon;
	fingerPusher.Parameter.Deceleration = fingerX.deceleration*1000;
	fingerPusher.Parameter.Acceleration = fingerX.acceleration*1000;
	fingerPusher.Parameter.Velocity = fingerX.speed*1000;
	stacker.reading.axisStatus.forkPush.actualSpeed = fingerX.actualSpeed;
	stacker.reading.axisStatus.forkPush.speed = fingerX.speed;
	stacker.reading.axisStatus.forkPush.pos = fingerX.actualPositon;
    stacker.reading.forksInOffsetToNorthPos = realEqual ((FINGER_START_POS_SPAGHETTI+FORKS_TO_NORTH_POS), fingerX.actualPositon, 0.25);
	stacker.reading.axisStatus.forkPush.errorID = fingerPusher.Status.ErrorID;
	if (stacker.reading.axisStatus.forkPush.errorID != 0) stacker.reading.axisStatus.forkPush.lastErrorID = stacker.reading.axisStatus.forkPush.errorID;
}

void setFingerY (void) {

    
	if (STOP_ALL) {
		fingerPusher.Command.Power = OFF;
		fingerLift.Command.Power = OFF;
		stackerLift.Command.Power = OFF;
		stopPlate.Command.Power = OFF;
		unHome();
		return;
	}

	if (fingerY.move != 0) {
		/******limit check********/
		if ((fingerY.pos + fingerY.move) < fingerY.minPos) fingerY.move = fingerY.minPos - fingerY.pos;
		else if ((fingerY.pos + fingerY.move) > fingerY.maxPos) fingerY.move = fingerY.maxPos - fingerY.pos;

		if (fingerY.move == 0) return;
		
		/******don't crash in fingers check, if fingers is out********/
		if ((fingerY.pos + fingerY.move) < (TABLE_CRASH_LIMIT) && FINGER_OUT) {
			elevatorForkDebug[4] = fingerY.pos; 
			elevatorForkDebug[5] = fingerY.move; 
			fingerY.move = (stackerY.transformPos - 7.0) - fingerY.pos;
			elevatorForkDebug[6] = fingerY.move; 
			strcpy(dbgPrintStr, "*S50 *WARNING* don't crash fingers guard (move fork)");
			strcpy(ULog.text[ULog.ix++], dbgPrintStr);
			//dbgLatch[0] = TRUE;
		   
		   
		}
		if (fingerY.move == 0) return;
		// go on, make the move
		
		
		// go on, make the move
		fingerY.inPosition = 0;
		fingerLift.Command.Power=ON;
		fingerLift.Parameter.Distance =  fingerY.move;
		fingerY.pos = fingerY.pos + fingerY.move;
		fingerY.absMove = fingerY.pos;
		fingerLift.Command.MoveAdditive = ON; // GO
		fingerY.move = 0;
	}
	else if (fingerY.absMove != fingerY.pos)
	{
		/******limit check********/
		if (fingerY.absMove < fingerY.minPos) fingerY.absMove = fingerY.minPos;
		else if (fingerY.absMove > fingerY.maxPos) fingerY.absMove = fingerY.maxPos;
		if (fingerY.absMove == fingerY.pos) return;
		/******don't crash in fingers check, if fingers is out********/
		if (fingerY.absMove  < (TABLE_CRASH_LIMIT) && FINGER_OUT) {
			fingerY.absMove = (stackerY.transformPos - 6.0);
			strcpy(dbgPrintStr, "*S50 *WARNING* don't crash fingers guard (move fork)");
			strcpy(ULog.text[ULog.ix++], dbgPrintStr);
			elevatorForkDebug[7] = fingerY.absMove; 
			//dbgLatch[1] = TRUE;
		}
		if (fingerY.absMove == fingerY.pos) return;	
		// go on, make the move
		fingerY.inPosition = 0;
		fingerLift.Command.Power=ON;
		fingerLift.Parameter.Position = fingerY.pos = fingerY.absMove;
		fingerLift.Command.MoveAbsolute = ON; //GO
	}
}

void getFingerY (void) {
	fingerY.actualPositon = fingerLift.Status.ActPosition;
	if (fingerLift.Status.ActVelocity > (0.02) || fingerLift.Status.ActVelocity < (-0.02)) fingerY.inMovement = 1;
	else {
		if (fingerY.inMovement == 0 && fingerY.actualPositon == fingerY.pos && fingerY.homed) fingerY.inPosition = 1;
		fingerY.inMovement = 0;
	}
		
	fingerY.actualSpeed = fingerLift.Status.ActVelocity/1000;
		
	if (fingerLift.Status.ErrorID != 0) {
		fingerY.error = 1;
	}
	else if (fingerLift.AxisState.ErrorStop)
		fingerY.error = 1;
	else 
		fingerY.error = 0;
		
	if (fingerY.error) fingerY.pos = fingerY.actualPositon;
	fingerLift.Parameter.Deceleration = fingerY.deceleration*1000;
	fingerLift.Parameter.Acceleration = fingerY.acceleration*1000;
	fingerLift.Parameter.Velocity = fingerY.speed*1000;
	//fingerY.transformPos = transform(IN_STACKER_POS,fingerY.pos,topPos.fingerY,topPos.stackerY);
	//fingerY.transformPos = stackerY.maxPos-fingerYMaxPosInit-stacker.setting.StackerFingerZeroAdjust+fingerY.actualPositon;
   
   
	//fingerY.transformPos = fingerY.actualPositon + (stackerY.maxPos - (fingerYMaxPosInit - stacker.setting.StackerFingerZeroAdjust));
	
   //test
   //fingerY.transformPos = (stackerY.maxPos + stacker.setting.topPosOffset) - (fingerY.maxPos-fingerY.actualPositon);
   fingerY.transformPos = (stacker.setting.topPosOffset) - (fingerY.maxPos-fingerY.actualPositon) + stacker.setting.StackerFingerZeroAdjust;
   stacker.reading.SFTransPos = fingerY.transformPos;

	stacker.reading.axisStatus.forkElevator.actualSpeed = fingerY.actualSpeed;
	stacker.reading.axisStatus.forkElevator.speed = fingerY.speed;
	stacker.reading.axisStatus.forkElevator.pos = fingerY.actualPositon;
	stacker.reading.axisStatus.forkElevator.errorID = fingerLift.Status.ErrorID;
	if (stacker.reading.axisStatus.forkElevator.errorID != 0) stacker.reading.axisStatus.forkElevator.lastErrorID = stacker.reading.axisStatus.forkElevator.errorID;
	
	//fingerY.transformPos = transform(IN_STACKER_POS,fingerY.pos,0,240 - fingerYMaxPosInit + stacker.setting.StackerFingerZeroAdjust);
}

void getStackerY (void) {
   static REAL distance;
	stackerY.actualPositon = stackerLift.Status.ActPosition;
    if (stackerLift.Status.ActVelocity > (0.02) || stackerLift.Status.ActVelocity < (-0.02)) {
      stackerY.inMovement = 1;
      distance = distance + rabs(stackerLift.Status.ActVelocity*SECONDS_UNIT);
      if (distance>1.0) {
         stackerCounter.stackerElevatorTravelDistance++;
         distance = distance - 1.0;
      }
    }
    else {
      if (stackerY.inMovement == 0 && stackerY.actualPositon == stackerY.pos && stackerY.homed) stackerY.inPosition = 1;
      stackerY.inMovement = 0;
    }
    
	stackerY.actualSpeed = stackerLift.Status.ActVelocity/1000;
    
	if (stackerLift.Status.ErrorID != 0) {
		stackerY.error = 1;
	}
	else if (stackerLift.AxisState.ErrorStop)
		stackerY.error = 1;
	else 
		stackerY.error = 0;
		
	if (stackerY.error) stackerY.pos = stackerY.actualPositon;
	stackerLift.Parameter.Deceleration = stackerY.deceleration*1000;
	stackerLift.Parameter.Acceleration = stackerY.acceleration*1000;
	stackerLift.Parameter.Velocity = stackerY.speed*1000;
    //org
	//stackerY.transformPos = (stackerY.actualPositon - (stackerY.maxPos + stacker.setting.topPosOffset /*GUI.settings.S50_topPosition*/)) + (fingerY.maxPos);
    //stackerY.transformPos = (stackerY.transformPos+(stackerY.actualPositon - (stackerY.maxPos - (fingerYMaxPosInit - stacker.setting.StackerFingerZeroAdjust))))/2;
    //???
    //stackerY.transformPos = (stackerY.actualPositon - stacker.setting.topPosOffset) + (fingerY.maxPos);
    //stackerY.transformPos = (stackerY.transformPos+(stackerY.actualPositon - (stacker.setting.topPosOffset - (fingerYMaxPosInit - stacker.setting.StackerFingerZeroAdjust))))/2;
	
    stackerY.transformPos = fingerY.maxPos - (stacker.setting.topPosOffset - stackerY.actualPositon) - stacker.setting.StackerFingerZeroAdjust;
    stacker.reading.axisStatus.stackerElevator.actualSpeed = stackerY.actualSpeed;
    stacker.reading.axisStatus.stackerElevator.speed = stackerY.speed;
    stacker.reading.axisStatus.stackerElevator.pos = stackerY.actualPositon;
    stacker.reading.axisStatus.stackerElevator.errorID = stackerLift.Status.ErrorID;
    if (stacker.reading.axisStatus.stackerElevator.errorID != 0) stacker.reading.axisStatus.stackerElevator.lastErrorID = stacker.reading.axisStatus.stackerElevator.errorID;
}

void setStackerY (void) {

	if (STOP_ALL) {
		fingerPusher.Command.Power = OFF;
		fingerLift.Command.Power = OFF;
		stackerLift.Command.Power = OFF;
		stopPlate.Command.Power = OFF;
		unHome();
		return;
	}
	
	if (stackerY.actualSpeed>0.05 && stackerY.haltMovement) {
		stackerLift.Command.Halt = 1;
		stackerY.absMove = stackerLift.Status.ActPosition;
		stackerY.haltMovement = 0;
		unHome();
		return;
	}
	if (stackerY.actualSpeed<0.01) stackerY.haltMovement = 0;

	if (stackerY.move != 0) {
		/******limit check********/
		if ((stackerY.pos + stackerY.move) < stackerY.minPos) stackerY.move = stackerY.minPos - stackerY.pos;
		else if ((stackerY.pos + stackerY.move) > stacker.setting.topPosOffset /*stackerY.maxPos*/) stackerY.move = /*stackerY.maxPos*/stacker.setting.topPosOffset - stackerY.pos;
		if (stackerY.move == 0) return;
		/******don't crash in fingers check, if fingers is out********/
		/*if ((stackerY.pos + stackerY.move) > fingerY.transformPos && fingerX.pos>16) {
			stackerY.move = fingerY.transformPos - stackerY.pos;
			strcpy(dbgPrintStr, "*S50 *WARNING* don't crash fingers guard (move stacker)");
			strcpy(ULog.text[ULog.ix++], dbgPrintStr);
			elevatorForkDebug[0] = stackerY.move; 
			//dbgLatch[2] = TRUE;
		}*/
		if (stackerY.move == 0) return;
		// go on, make the move
        stackerY.inPosition = 0;
        stackerLift.Command.Power=ON;
        stackerLift.Parameter.Distance =  stackerY.move;
        stackerY.pos = stackerY.pos + stackerY.move;
        stackerY.absMove = stackerY.pos;
        stackerLift.Command.MoveAdditive = ON; // GO
        stackerMoveLog.absRel[stackerMoveLog.pointer] = relative;
        stackerMoveLog.moves[stackerMoveLog.pointer] = stackerY.move;
        if (stackerMoveLog.pointer<49) stackerMoveLog.pointer++;
        else stackerMoveLog.pointer = 0;
		stackerY.move = 0;
	}
	else if (stackerY.absMove != stackerY.pos)
	{
		/******limit check********/
        if (stackerY.absMove < stackerY.minPos) stackerY.absMove = stackerY.minPos;
        else if (stackerY.absMove > stacker.setting.topPosOffset/*stackerY.maxPos*/) stackerY.absMove = stacker.setting.topPosOffset;//stackerY.maxPos;
		if (stackerY.absMove == stackerY.pos) return;
		/******don't crash in fingers check if fingers is out********/  
		if (stackerY.absMove > fingerY.transformPos && fingerX.pos>16) {
            elevatorForkDebug[1] = stackerY.absMove; 
            stackerY.absMove = fingerY.transformPos;
            strcpy(dbgPrintStr, "*S50 *WARNING* don't crash fingers guard (abs move stacker)");
            strcpy(ULog.text[ULog.ix++], dbgPrintStr);
            elevatorForkDebug[2] = fingerY.transformPos; 
            elevatorForkDebug[3] = fingerX.pos; 
			//dbgLatch[3] = TRUE;
		}
		if (stackerY.absMove == stackerY.pos) return;
		// go on, make the move
        stackerY.inPosition = 0;
        stackerLift.Command.Power=ON;
        stackerLift.Parameter.Position = stackerY.pos = stackerY.absMove;
        stackerLift.Command.MoveAbsolute = ON; //GO
        stackerMoveLog.absRel[stackerMoveLog.pointer] = absolute;
        stackerMoveLog.moves[stackerMoveLog.pointer] = stackerY.absMove;
        if (stackerMoveLog.pointer<49) stackerMoveLog.pointer++;
        else stackerMoveLog.pointer = 0;
	}
}

void getStopPlateX (void) {
	stopPlateX.actualPositon = stopPlate.Status.ActPosition;
	if (stopPlate.Status.ActVelocity > (0.02) || stopPlate.Status.ActVelocity < (-0.02)) stopPlateX.inMovement = 1;
	else {
		if (stopPlateX.inMovement == 0 && stopPlateX.actualPositon == stopPlateX.pos && stopPlateX.homed) stopPlateX.inPosition = 1;
		stopPlateX.inMovement = 0;
	}
		
	stopPlateX.actualSpeed = stopPlate.Status.ActVelocity/1000;
		
	if (stopPlate.Status.ErrorID != 0) {
		stopPlateX.error = 1;
	}
	else if (stopPlate.AxisState.ErrorStop)
		stopPlateX.error = 1;
	else 
		stopPlateX.error = 0;
		
	if (stopPlateX.error) stopPlateX.pos = stopPlateX.actualPositon;
	stopPlate.Parameter.Deceleration = stopPlateX.deceleration*1000;
	stopPlate.Parameter.Acceleration = stopPlateX.acceleration*1000;
	stopPlate.Parameter.Velocity = stopPlateX.speed*1000;
	stacker.reading.axisStatus.endStop.actualSpeed = stopPlateX.actualSpeed;
	stacker.reading.axisStatus.endStop.speed = stopPlateX.speed;
	stacker.reading.axisStatus.endStop.pos = stopPlateX.actualPositon;
	stacker.reading.axisStatus.endStop.errorID = stopPlate.Status.ErrorID;
	if (stacker.reading.axisStatus.endStop.errorID != 0) stacker.reading.axisStatus.endStop.lastErrorID = stacker.reading.axisStatus.endStop.errorID;
	
}

void setStopPlateX (void) {

	if (STOP_ALL) {
		fingerPusher.Command.Power = OFF;
		fingerLift.Command.Power = OFF;
		stackerLift.Command.Power = OFF;
		stopPlate.Command.Power = OFF;
		unHome();
		return;
	}

	if (stopPlateX.move != 0) {
		/******limit check********/
		if ((stopPlateX.pos + stopPlateX.move) < stopPlateX.minPos) stopPlateX.move = stopPlateX.minPos - stopPlateX.pos;
		else if ((stopPlateX.pos + stopPlateX.move) > stopPlateX.maxPos) stopPlateX.move = stopPlateX.maxPos - stopPlateX.pos;
		if (stopPlateX.move == 0) return;
		// go on, make the move
		stopPlateX.inPosition = 0;
		stopPlate.Command.Power=ON;
		stopPlate.Parameter.Distance =  stopPlateX.move;
		stopPlateX.pos = stopPlateX.pos + stopPlateX.move;
		stopPlateX.absMove = stopPlateX.pos;
		stopPlate.Command.MoveAdditive = ON; // GO
		stopPlateX.move = 0;
	}
	else if (stopPlateX.absMove != stopPlateX.pos) {
		/******limit check********/
		if (stopPlateX.absMove < stopPlateX.minPos) stopPlateX.absMove = stopPlateX.minPos;
		else if (stopPlateX.absMove > stopPlateX.maxPos) stopPlateX.absMove = stopPlateX.maxPos;
		if (stopPlateX.absMove == stopPlateX.pos) return;
		// go on, make the move
		stopPlateX.inPosition = 0;
		stopPlate.Command.Power=ON;
		stopPlate.Parameter.Position = stopPlateX.pos = stopPlateX.absMove;
		stopPlate.Command.MoveAbsolute = ON; //GO
	}
}

void errorAckAll(BOOL halt) {
	if (deliverTransport.Command.MoveVelocity) deliverTransport.Command.MoveVelocity = 0;
	if ((deliverTransport.Status.ErrorID != 0 || deliverTableBelt.error) && deliverTransport.Command.ErrorAcknowledge == 0) deliverTransport.Command.ErrorAcknowledge = 1;
	if ((transport.Status.ErrorID != 0 || infeedTransport.error) && transport.Command.ErrorAcknowledge == 0) transport.Command.ErrorAcknowledge = 1;
	if (stopPlate.Status.ErrorID != 0 && stopPlate.Command.ErrorAcknowledge == 0) stopPlate.Command.ErrorAcknowledge = 1;
	if (fingerPusher.Status.ErrorID != 0 && fingerPusher.Command.ErrorAcknowledge == 0) fingerPusher.Command.ErrorAcknowledge = 1;
	if (fingerLift.Status.ErrorID != 0 && fingerLift.Command.ErrorAcknowledge == 0) fingerLift.Command.ErrorAcknowledge = 1;
	if (stackerLift.Status.ErrorID != 0 && stackerLift.Command.ErrorAcknowledge == 0) stackerLift.Command.ErrorAcknowledge = 1;
	
	if (halt) {
		if (deliverTransport.AxisState.ContinuousMotion) deliverTransport.Command.Halt = 1;
		if (transport.AxisState.ContinuousMotion) transport.Command.Halt = 1;
	}
	else {
	   deliverTransport.Command.Halt = 0;
	   transport.Command.Halt = 0;
	}
	safety.command.resetStackerLightCurtain = TRUE;
}

REAL sheetHandlerLimiter (INT function, INT id, REAL speed)
{
	int i;
	REAL tmp;

	if (function == GET_SPEED) {
		tmp = 1000.0;
		for (i=0;i<SPEEDS_SIZE;i++)	if (sheetHandlerSpeeds[i]<tmp) tmp = sheetHandlerSpeeds[i];
		return tmp;
	}
	
	if (function == SET_SPEED)
	{
		if (id == ALL) { 
			for (i=0;i<SPEEDS_SIZE;i++) sheetHandlerSpeeds[i] = speed;
			return speed;
		}
		else {
			tmp = 1000.0;
			for (i=0;i<SPEEDS_SIZE;i++)	if (sheetHandlerSpeeds[i]<tmp) tmp = sheetHandlerSpeeds[i];
			sheetHandlerSpeeds[id]=speed;
			if (tmp < speed) return 0; //some one else was lower
			return 1; //you are responsible to lower the speed
		}
	}
	if (function == GET_LOWEST_ID) {
		int j;
		tmp = 1000.0;
		for (i=0;i<SPEEDS_SIZE;i++)	{
			if (sheetHandlerSpeeds[i]<tmp) {
				tmp = sheetHandlerSpeeds[i];
				j=i;
			}
		}
		return (REAL)(j);
	}
	if (function == GET_ID) return sheetHandlerSpeeds[id];
	if (function == GET_NEXT_SPEED) {
		REAL tmp2;
		tmp2 = sheetHandlerLimiter(GET_ID,id,0); //new
		sheetHandlerLimiter(SET_SPEED,id,1000.0);
		tmp = sheetHandlerLimiter(GET_SPEED,0,0);
		sheetHandlerLimiter(SET_SPEED,id,tmp2); //speed
		return tmp;
	}
	return 999; // bad function 
}

void initSpeeds (void)
{
	sheetHandlerLimiter(SET_SPEED,ALL,MAX_SPEED);
	sheetHandlerLimiter(SET_SPEED,READY,STOP);
	if (rt_info.cycle_time)	ticksPerSecond = 1000000 / rt_info.cycle_time;
	else ticksPerSecond = 247;
	(*sStop[releaseAllSoftStop])(0);
	dbgTmpWasFeeding = 0;
}

USINT pageInTransportQueue (USINT a, struct sheet b)
{
	static USINT lastPageTypeAdded;
	
	if (0 == a) { //POST_PAGE
		transportQueue.pgQueue[transportQueue.pIn++] = b;
		lastPageTypeAdded = b.type;
      stackerCounter.sheets++;
		if (transportQueue.pIn>=TRANSPORT_QUEUE_SIZE) transportQueue.pIn = 0;
		return 0;
	}
	if (1 == a) { //PEEK_PAGE
		return transportQueue.pgQueue[transportQueue.pOut].type;
	}
	if (2 == a) { //INIT_QUEUE
		transportQueue.pIn = transportQueue.pOut = 0;
		return 0;
	}
	if (3 == a) { //GET_NO_PAGES_IN_QUEUE
		return ((transportQueue.pIn+TRANSPORT_QUEUE_SIZE)-transportQueue.pOut)%TRANSPORT_QUEUE_SIZE;
	}
	if (4 == a) { //REMOVE_LAST_PAGE
		if (transportQueue.pIn == transportQueue.pOut) return -1;
		transportQueue.pOut++;
		if (transportQueue.pOut>=TRANSPORT_QUEUE_SIZE) transportQueue.pOut = 0;
		return 0;
	}	
	/*if (5 == a) { //PEEK_PAGE_SIZE
		return transportQueue.pgQueue[transportQueue.pOut].size;
	}*/
	if (6 == a) { //FIRST_PAGE
		return lastPageTypeAdded;
	}
   if (7 == a) { //FORWARD_PEAK_PAGE
      USINT tmp;
      tmp = transportQueue.pOut;
      if (0 == tmp) tmp = TRANSPORT_QUEUE_SIZE;
      else tmp--;
      if (0 == pageInTransportQueue(GET_NO_PAGES_IN_QUEUE)) return NORMAL_PAGE;
      return transportQueue.pgQueue[tmp].type;
   }
	return 0;
}

void enforceLimits(void)
{
	realMinMaxCheck(&stacker.setting.deliverTransportSpeed,0.05,1.0);
}

USINT checkPendingError(void)
{
	USINT i;
	BOOL err;
	USINT retVal;
	USINT noOfErrors;
	
	err=FALSE;
	retVal=NO_ERROR;
	noOfErrors = 0;
	
	for (i=0;i<LAST_ERROR;i++) {
		if (ERR == error[i].status)
		{	
			if (retVal<error[i].stopType) 
			{
				retVal=error[i].stopType;
				err=TRUE;
				errorLog.lastErrorPointer++; //point to new slot
				if (errorLog.lastErrorPointer >= errorLog.errorLogSize) errorLog.lastErrorPointer = 0; //rotate
				errorLog.err[errorLog.lastErrorPointer].no = i;
				errorLog.err[errorLog.lastErrorPointer].subType = error[i].subType;
				errorLog.err[errorLog.lastErrorPointer].simultaniousErrors = ++noOfErrors;
			}
		}
	}
	if (err == TRUE && retVal == NO_ERROR) // just in case i forgot to set the stopType, should never happen
		return NORMAL_STOP;
	else 
		return retVal;
}

void retError (REAL *err, REAL *type)
{
	USINT i;
	for (i=0;i<LAST_ERROR;i++) {
		if (ERR == error[i].status)
		{
			*err = (REAL)i;
			*type = (REAL)error[i].subType;
			break;
		}
	}
}

void clearErrors (void)
{
	USINT i;
	for (i=0;i<LAST_ERROR;i++) {
		error[i].status = NO_ERROR;
	}
}

BOOL makeUnloaded(void)
{
	USINT i;
	BOOL unload;
	unload = 0;
	for (i=0;i<LAST_ERROR;i++) {
		if (ERR == error[i].status && TRUE == error[i].unload) {
			unload = TRUE;
		}
	}
	return unload;
}

BOOL hold (USINT id)
{
	ss[id]=TRUE;
	return TRUE;
}
BOOL release (USINT id)
{
	ss[id]=FALSE;
	return FALSE;
}
BOOL readStat (USINT id)
{
	int i;
	for (i=0;i<SOFTSTOPMEMBERSIZE;i++) if (TRUE == ss[i]) return TRUE;
	return FALSE;
}
BOOL amIstopping(USINT id)
{
	return ss[id];
}
BOOL releaseAll (USINT id)
{
	int i;
	for (i=0;i<SOFTSTOPMEMBERSIZE;i++) ss[i] = FALSE;
	return TRUE;
}

void unHome(void)
{
	fingerX.homed = 0;
	fingerY.homed = 0;
	stackerY.homed = 0;
	stopPlateX.homed = 0;
	infeedTransport.homed = 0;
	//deliverTableBelt.homed = 0;
}

void setButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
	if (btnNo == 1) {
		button.color1.green     = grn;
		button.color1.red       = red;
		button.color1.yellow    = yel;
		button.color1.white     = whtBlu;
		if (button.color1.yellow && button.color1.red) stacker.reading.button1 = _ORANGE;
		else if (button.color1.yellow) stacker.reading.button1 = _YELLOW;
		else if (button.color1.red) stacker.reading.button1 = _RED;
		else if (button.color1.green) stacker.reading.button1 = _GREEN;
		else if (button.color1.white) stacker.reading.button1 = _WHITE;
		else stacker.reading.button1 = _OFF;
	}
	if (btnNo == 2) {
		button.color2.green     = grn;
		button.color2.red       = red;
		button.color2.yellow    = yel;
		button.color2.white     = whtBlu;
		if (button.color2.yellow && button.color2.red) stacker.reading.button2 = _ORANGE;
		else if (button.color2.yellow) stacker.reading.button2 = _YELLOW;
		else if (button.color2.red) stacker.reading.button2 = _RED;
		else if (button.color2.green) stacker.reading.button2 = _GREEN;
		else if (button.color2.white) stacker.reading.button2 = _WHITE;
		else stacker.reading.button2 = _OFF;
	}
	if (btnNo == 3) {
		button.color3.green     = grn;
		button.color3.red       = red;
		button.color3.yellow    = yel;
		button.color3.blue      = whtBlu;
		if (button.color3.yellow && button.color3.red) stacker.reading.button3 = _ORANGE;
		else if (button.color3.blue && button.color3.red) stacker.reading.button3 = _MAGENTA;
		else if (button.color3.green && button.color3.blue) stacker.reading.button3 = _TURQUOISE;
		else if (button.color3.yellow) stacker.reading.button3 = _YELLOW;
		else if (button.color3.red) stacker.reading.button3 = _RED;
		else if (button.color3.green) stacker.reading.button3 = _GREEN;
		else if (button.color3.blue) stacker.reading.button3 = _BLUE;
		else stacker.reading.button3 = _OFF;
	}
}

BOOL isButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
	if (btnNo == 1) {
		if (   grn == button.color1.green 
			&& red == button.color1.red 
			&& yel == button.color1.yellow
			&& whtBlu == button.color1.white) return 1;
		if (   grn && red && yel && whtBlu 
			&& (button.color1.green || button.color1.red || button.color1.yellow || button.color1.white)) return 1;
		return 0;
	}
	if (btnNo == 2) {
		if (   grn == button.color2.green 
			&& red == button.color2.red 
			&& yel == button.color2.yellow
			&& whtBlu == button.color2.white) return 1;
		if (  grn && red && yel && whtBlu 
			&& (button.color2.green || button.color2.red || button.color2.yellow || button.color2.white)) return 1;
		return 0;
	}
	if (btnNo == 3) {
		if (   grn == button.color3.green 
			&& red == button.color3.red 
			&& yel == button.color3.yellow
			&& whtBlu == button.color3.blue) return 1;
		if (   grn && red && yel && whtBlu 
			&& (button.color3.green || button.color3.red || button.color3.yellow || button.color3.blue)) return 1;
		return 0;
	}
	return 0;
}
/*
void handleForceOnIntermittentOutput (void)
{
   handleForce(&stackDeliverGripper);
   handleForce(&offsetNorthGripper);
   handleForce(&offsetSouthGripper);
}

void handleForce (struct output_typ *x)
{
   if (x->force) x->oVal = x->fVal;
}

void setOutput (struct output_typ *x, BOOL val)
{
	x->pVal = val;
	if (!x->force)
	{
		x->oVal = val;
		return;
	}  
	x->oVal = x->fVal;
}*/

float speedDelay(int f, float t, float s)
{
	static int p;
	static REAL sp[51],tt,interval;
	static BOOL lastSpeedZero = 1;
	int i;
	switch (f)
	{
		case 0:
			for (i=0;i<51;i++) sp[i] = 0.0;
			interval = t;
			break;
		case 1:
			tt = tt + t;
			if (lastSpeedZero && (s > 0.05)) tt = interval; //add immediately
			if (tt>=interval)
			{
				tt = tt - interval;
				sp[p++] = s;
				if (s < 0.01) lastSpeedZero = TRUE;
				else lastSpeedZero = FALSE;
				if (p>=51) p=0;
			}
			break;
		case 2:
			if (interval>0)
			{
				int x,tp;
				x = (int)(t/interval);
				//printf("ttt %d \n",x);
				if (x>51) x = 51;
				float max=0;
				tp = p;
				do
				{
					if (tp<0) tp = 51-1;
					//printf ("max=%f tp=%d\n",max,tp);
					if (sp[tp]>max) max = sp[tp];
					tp--;
					x--;
				}
				while (x>=0);
				return max;
			}
			break;
        /*case 3:
        {
            int x;
            for (x=0;x<50;x++)
            {
                printf("%f,",sp[x]);
            }
            printf("\n%d\n",p);
        }*/
	}
	return 0.0;
}

void addDebugNote(USINT typ, REAL realDebug1, REAL realDebug2,REAL boolDebug1 ,BOOL boolDebug2)
{
	stacker.reading.debug.infoP++;
	if (stacker.reading.debug.infoP>=30) stacker.reading.debug.infoP = 0;
	stacker.reading.debug.info[stacker.reading.debug.infoP].typ = typ;
	stacker.reading.debug.info[stacker.reading.debug.infoP].bool1 = boolDebug1;
	stacker.reading.debug.info[stacker.reading.debug.infoP].bool2 = boolDebug2;
	stacker.reading.debug.info[stacker.reading.debug.infoP].real1 = realDebug1;
	stacker.reading.debug.info[stacker.reading.debug.infoP].real2 = realDebug2;
	stacker.reading.debug.info[stacker.reading.debug.infoP].timeStamp = lineController.reading.timeSinceStart;
}

void handleStartUpSeq(void)
{
   static UDINT lastTimeSinceStart;
   static USINT startSpaghettiState;
   static USINT startDeliverState;
   stacker.setting.automaticEndStop = lineController.setup.stacker.automaticStopPlate;
   if (stacker.reading.startUpSeq == 15) return;
   if (transport.Status.DriveStatus.DriveEnable && stacker.reading.startUpSeq<3) stacker.reading.startUpSeq = 3;
   if (stacker.reading.startUpSeq == 0 && mainTick>5.0) stacker.reading.startUpSeq = 1;
   if (stacker.reading.startUpSeq == 1 && mainTick>370.0) stacker.reading.startUpSeq = 2; //80
   if (stacker.reading.startUpSeq == 2 && mainTick>400.0) stacker.reading.startUpSeq = 3; //100
   if (stacker.reading.startUpSeq > 2 && stacker.reading.startUpSeq < 15)
   {
      if (TRUE == node[14].station[0]) {
         lineController.setup.stacker.spaghettiBelts = TRUE;
         lineController.setup.stacker.stackerGripperUnit1 = FALSE;
         lineController.setup.stacker.stackerGripperUnit2 = FALSE;
         lineController.setup.stacker.stackerGripperUnit3 = FALSE;
         lineController.setup.stacker.stackerGripperUnit4 = FALSE;
      }
      
      if (lastTimeSinceStart != lineController.reading.timeSinceStart)
      {
         if (deliverTransport.Status.DriveStatus.DriveEnable && deliverTransport.Status.DriveStatus.ControllerReady)
         {
				/*if (deliverTransport.Status.ErrorID != 0) {
					deliverTransport.Command.ErrorAcknowledge = TRUE;
					lastDriveEnable[DELIV] = 0;
					deliverTransport.Command.Power = FALSE;
					lastControllerReady[DELIV] = 0;
					stacker.reading.startUpSeq = 3;
				}
				
				if (!lastDriveEnable[DELIV])
				{
					if (deliverTransport.Status.ErrorID == 0)
					{
						deliverTransport.Command.Power = TRUE;
						lastDriveEnable[DELIV] = deliverTransport.Status.DriveStatus.DriveEnable;
						stacker.reading.startUpSeq = 4;
					}
					//else deliverTransport.Command.ErrorAcknowledge = TRUE;
				}
				else if (lastDriveEnable[DELIV] && !lastControllerReady[DELIV])
				{
					if (deliverTransport.Status.ErrorID == 0)
					{
						deliverTransport.Command.Home = TRUE;
						lastControllerReady[DELIV] = deliverTransport.Status.DriveStatus.ControllerReady;
						stacker.reading.axisStatus.deliver.homeState = 3;
						deliverTableBelt.homed = 1;
						
                  if (lineController.setup.stacker.spaghettiBelts) stacker.reading.startUpSeq = 5;
                  else stacker.reading.startUpSeq = 6;
					}
				}*/
            if (stacker.reading.startUpSeq == 3) {
               if (deliverTransport.Status.ErrorID != 0) {
                  deliverTransport.Command.ErrorAcknowledge = TRUE;
                  deliverTransport.Command.Power = FALSE;
                  startDeliverState = 0;
               }
               else {
                  if (startDeliverState == 2) {
                     stacker.reading.startUpSeq = 4;
                     startDeliverState = 3;
                  }
                  if (startDeliverState == 1) {
                     deliverTransport.Command.Home = TRUE;
                     startDeliverState = 2;
                  }
                  if (startDeliverState == 0) {
                     deliverTransport.Command.Power = TRUE;
                     deliverTransport.Command.ErrorAcknowledge = FALSE;
                     startDeliverState = 1;
                  }
               }
            }
            if (stacker.reading.startUpSeq == 4) //todo check DriveEnable
            {
               if (!lineController.setup.stacker.spaghettiBelts)
                  stacker.reading.startUpSeq = 5;
               else if (spaghettiBelt.Status.DriveStatus.DriveEnable && spaghettiBelt.Status.DriveStatus.ControllerReady) {
                  if (spaghettiBelt.Status.ErrorID != 0) {
                     spaghettiBelt.Command.ErrorAcknowledge = TRUE;
                     spaghettiBelt.Command.Power = FALSE;
                     startSpaghettiState = 0;
                  }
                  else {
                     if (startSpaghettiState == 2) {
                        stacker.reading.startUpSeq = 5;
                        startSpaghettiState = 3;
                     }
                     if (startSpaghettiState == 1) {
                        spaghettiBelt.Command.Home = TRUE;
                        startSpaghettiState = 2;
                     }
                     if (startSpaghettiState == 0) {
                        spaghettiBelt.Command.Power = TRUE;
                        spaghettiBelt.Command.ErrorAcknowledge = FALSE;
                        startSpaghettiState = 1;
                     }
                  }
               }
            }
            // TODO add check for stopPlate
            if (stacker.reading.startUpSeq == 5) {
               if (transport.Status.DriveStatus.DriveEnable && transport.Status.DriveStatus.ControllerReady) stacker.reading.startUpSeq = 6;
            }
            
            if (stacker.reading.startUpSeq == 6) {
               if (stackerLift.Status.DriveStatus.DriveEnable && stackerLift.Status.DriveStatus.ControllerReady) stacker.reading.startUpSeq = 7;
            }
            
            if (stacker.reading.startUpSeq == 7) {
               if (fingerLift.Status.DriveStatus.DriveEnable && fingerLift.Status.DriveStatus.ControllerReady) stacker.reading.startUpSeq = 8;
            }
            
            if (stacker.reading.startUpSeq == 8) {
               if (fingerPusher.Status.DriveStatus.DriveEnable && fingerPusher.Status.DriveStatus.ControllerReady) stacker.reading.startUpSeq = 9;
            }
            if (stacker.reading.startUpSeq == 9) {
               if (stacker.setting.automaticEndStop) {
                  if (stopPlate.Status.DriveStatus.DriveEnable && stopPlate.Status.DriveStatus.ControllerReady) stacker.reading.startUpSeq = 10;
               }
               else stacker.reading.startUpSeq = 10;
            }
            if (stacker.reading.startUpSeq >= 10 && stacker.reading.startUpSeq<15) stacker.reading.startUpSeq++;
         }
			
      }
      if (stacker.reading.startUpSeq>15) stacker.reading.startUpSeq = 15;
   }
   if (transport.Status.DriveStatus.DriveEnable && transport.Status.DriveStatus.ControllerReady && !stackerDriveStartUp) {
      addDebugNote(6,0,0,0,0);
      stackerDriveStartUp = 1;
   }
   lastTimeSinceStart = lineController.reading.timeSinceStart;
}

void keepCoverLocked(void)
{
	if (!safety.reading.stackerTopCoverLocked) safety.command.lockStackerTopCover = TRUE;
}

void keepCoverUnLocked(void)
{
	if (safety.reading.stackerTopCoverLocked) safety.command.unlockStackerTopCover = TRUE;
}

BOOL stackerMoving (void)
{
	if (stacker.reading.axisStatus.transport.actualSpeed > 1.0) {
		return TRUE;
		unlockTimerCnt = 50;
	}
	if (stacker.reading.axisStatus.deliver.actualSpeed > 0.2) return TRUE;
	if (stacker.reading.axisStatus.endStop.actualSpeed > 0) return TRUE;
	if (stacker.reading.axisStatus.forkElevator.actualSpeed > 0) return TRUE;
	if (stacker.reading.axisStatus.forkPush.actualSpeed > 0) return TRUE;
	if (stacker.reading.axisStatus.stackerElevator.actualSpeed > 0) return TRUE;
	if (inSpeed.set > 0 && state == ReadyMode) return TRUE;
    //if (gripper.reading.gripperAxis1status.homeState == 1 || gripper.reading.gripperAxis1status.homeState == 2) return TRUE;
	return FALSE;
}

REAL distTransportCalc(UINT func)
{
static REAL dist,kD,kA;
	switch (func)
	{
		case SETUP:
			dist = 0.0;
			kD = (infeedTransport.deceleration * SECONDS_UNIT);
			kA = (infeedTransport.acceleration * SECONDS_UNIT);
			return kA;
			break;
		case ADD:
			if ((stacker.reading.axisStatus.transport.actualSpeed+kD)<stacker.reading.axisStatus.transport.speed) { //deceleration
				dist += (stacker.reading.axisStatus.transport.actualSpeed - kD/2) * MILLISECONDS_UNIT;
			   return ( -1 *kD);
			}
			if ((stacker.reading.axisStatus.transport.actualSpeed-kA)>stacker.reading.axisStatus.transport.speed) {//acceleration
			   dist += (stacker.reading.axisStatus.transport.actualSpeed + kA/2) * MILLISECONDS_UNIT;
				return kA;
			}
			dist += stacker.reading.axisStatus.transport.actualSpeed * MILLISECONDS_UNIT;
			return 0.0;
			break;
		case FETCH:
			return dist;
			break;
	}
	return dist;
}


void getCutterSpeed(void)
{
	inSpeed.decelerate = FALSE;
	inSpeed.is = cutter.reading.webSpeed;
	if (inSpeed.is < 0.02) inSpeed.is = 0.0;
	
	inSpeed.set = cutter.reading.setSpeed;
	
	if (inSpeed.is > inSpeed.set*1.015) inSpeed.decelerate = TRUE;
	else if (inSpeed.is < inSpeed.set*0.985) inSpeed.accelerate = TRUE;
	
	if (inSpeed.accelerate) {
      if (inSpeed.is<0.05 && cutter.setting.masterAcc>2600)
		   inSpeed.predict = inSpeed.is + (3.12 * SECONDS_UNIT);
      else
         inSpeed.predict = inSpeed.is + (cutter.setting.masterAcc * 0.0012 * SECONDS_UNIT);
      
		inSpeed.follow = (inSpeed.set + inSpeed.predict) / 2;
	}
	else if (inSpeed.decelerate) {
		inSpeed.predict = inSpeed.is - (cutter.setting.masterDec * 0.0012 * SECONDS_UNIT);
		inSpeed.follow = inSpeed.is > inSpeed.set ? inSpeed.is:inSpeed.set;
	}
	else {
		inSpeed.follow = inSpeed.predict = (inSpeed.set + inSpeed.is)/2;
	}
}



TYPE
	moveType_typ : 
		(
		absolute,
		relative
		);
	stackerMoveLog_typ : 	STRUCT 
		pointer : USINT;
		absRel : ARRAY[0..49]OF moveType_typ;
		moves : ARRAY[0..49]OF REAL;
	END_STRUCT;
	spaghettiStep_typ : 	STRUCT 
		measumentDist : REAL;
		sheetMeasurment : ARRAY[0..30]OF REAL;
		maxMeasurements : USINT := 25;
		sheetMedian : REAL;
		error : REAL;
		integral : REAL;
		derivative : REAL;
		kI : REAL;
		kP : REAL;
		lastError : ARRAY[0..7]OF REAL;
		kD : REAL;
		output : REAL;
		sheetMeasurmentP : USINT;
		average : REAL; (*mm*)
		maxAdjust : REAL := 10; (*%*)
		tuneState : USINT := 0; (*mm*)
		inputValue : REAL; (*mm*)
	END_STRUCT;
	gripperSettingsStruct : 	STRUCT 
		twoUpSideBySide : BOOL;
		home : BOOL;
		paddles : BOOL;
	END_STRUCT;
	gripperCmd_enum : 
		(
		done,
		twoUpSideBySideON,
		twoUpSideBySideOFF,
		GripperHomeCmd,
		PaddlesON,
		PaddlesOFF
		);
	forwardBlower_typ : 	STRUCT 
		HighBlowOnSpeed : REAL;
		noBlowPages : UINT;
		lowBlowOnSpeed : REAL;
	END_STRUCT;
	ev_function_enum : 
		(
		UPDATE,
		INIT,
		ADD_EVENT,
		CLEAR
		);
	ev_action_enum : 
		(
		EVENT_PUFF_DOWN_ON,
		EVENT_PUFF_DOWN_OFF,
		EVENT_EMPTY,
		EVENT_PUFF_FORWARD_ON,
		EVENT_PUFF_FORWARD_OFF
		);
	exitTransportStruct : 	STRUCT 
		measuredSize : REAL; (*mm*)
		measuredGap : REAL; (*mm*)
	END_STRUCT;
	tsSpeeStruct : 	STRUCT 
		timer : REAL := 0; (*s*)
		speed : REAL := 0; (*m/s*)
		distance : REAL := 0; (*mm*)
	END_STRUCT;
	fstopDist : 	STRUCT 
		fromExit : LREAL;
		fromEnter : LREAL;
	END_STRUCT;
	tbTable : 	STRUCT 
		deliverPageRecived : BOOL := FALSE;
		pIn : USINT := 0;
		pOut : USINT := 0;
		pgQueue : ARRAY[0..31]OF sheet;
		pgSequence0 : UDINT := 111111; (*new page registred, wait for input sensor rising edge*)
		pgSequence1 : UDINT := 222222; (*increments transportInfeedCrash*)
		infeedSequenceCrashCnt : USINT := 0; (*transportInfeedCrashCnt	USINT	false	false	false	true	0	should not be greater than 1, two pages sent by cutter none seen.	 	 	19*)
	END_STRUCT;
	dir : 
		(
		forward,
		backward
		);
	speedAxis : 	STRUCT 
		lastSpeed : REAL;
		actualSpeed : REAL; (*m/s*)
		error : BOOL;
		speed : REAL; (*m/s*)
		deceleration : REAL; (*m/s�*)
		acceleration : REAL; (*m/s�*)
		maxSpeed : REAL; (*m/s*)
		homed : BOOL;
		direction : dir;
		actualPosition : REAL;
		minSpeed : REAL; (*m/s*)
	END_STRUCT;
	btn : 
		(
		none,
		green,
		orange,
		blue
		);
	masterStates : 
		(
		TestMode,
		ReadyMode,
		NotLoadedMode,
		ErrorMode,
		StoppedDelivery
		);
	posAxis : 	STRUCT 
		pos : REAL;
		move : REAL;
		absMove : REAL;
		speed : REAL;
		deceleration : REAL;
		acceleration : REAL;
		homed : BOOL;
		actualSpeed : REAL;
		actualPositon : REAL;
		error : BOOL;
		inPosition : BOOL;
		inMovement : BOOL;
		lastPos : REAL;
		maxPos : REAL;
		minPos : REAL;
		standStillTime : REAL;
		homeSettleTime : REAL;
		transformPos : REAL;
		haltMovement : BOOL := 0;
	END_STRUCT;
	triggInfeed : 	STRUCT 
		outBlock1 : DINT;
	END_STRUCT;
	cmd : 	STRUCT 
		stoppedDelivery : BOOL := FALSE;
		load : BOOL := FALSE;
		errorAcknowledge : BOOL := FALSE;
		home : BOOL := FALSE;
		findZero : BOOL := FALSE;
	END_STRUCT;
	cmdStat : 	STRUCT 
		loading : BOOL := FALSE;
		deliveringStopped : BOOL := FALSE;
		homing : BOOL := FALSE;
		findingZero : BOOL := FALSE;
	END_STRUCT;
	loadingStates : 
		(
		initial,
		home,
		clearTransportStart,
		clearTransportEnd,
		tableToDeliverPosition,
		deliverStackStart,
		deliverStackEnd,
		tableUp,
		monitorTableUp,
		exit
		);
	errLog : 	STRUCT 
		lastErrorPointer : USINT;
		errorLogSize : USINT := 10;
		err : ARRAY[0..9]OF errInfo;
	END_STRUCT;
	errorStruct : 	STRUCT 
		name : STRING[80] := '';
		stopType : USINT := 0; (*0 no_error, 1 stop after next job, 2 normalStop, 3 stop and make place for paper, 4 panic*)
		cnt : UDINT := 0;
		subType : USINT;
		status : BOOL := FALSE;
		active : BOOL := TRUE;
		unload : BOOL := FALSE;
	END_STRUCT;
	errInfo : 	STRUCT 
		no : USINT;
		subType : USINT;
		simultaniousErrors : USINT;
	END_STRUCT;
	deliverFlyStruct : 	STRUCT 
		stateTime : REAL := 0.0; (*read only, s*)
		state : USINT := 0;
		inProgress : BOOL := FALSE; (*read only*)
		stackingOnFingers : BOOL := FALSE; (*read only*)
		stopForGapSpeed : REAL := 0.05; (*setting, m/s*)
		transportSpeedLastPage : REAL := 2.0; (*setting, m/s*)
		lastSheetInStackTime : REAL := 0.2; (*setting,  s*)
		stackDropFngOutWait : REAL := 0.3; (*setting, s*)
		stopStart : REAL;
		stopEnd : REAL;
		holdSpeed : REAL; (*m/s*)
		pendingDeliver : BOOL;
		lastSheetInStackDistFromSensor : REAL := 200; (*mm*)
		debug_2 : REAL;
		debug_6 : REAL;
		debug_7 : REAL;
		debug_8 : REAL;
		debug_9 : REAL;
		debug_10 : REAL;
		debug_5 : REAL;
		debug_3 : REAL;
		debug_4 : REAL;
		debug_1 : REAL;
		stackerMovePos : REAL;
		fingerYtransform : REAL;
		stackerFingerTransState : USINT := 0;
		stackerUpState : USINT := 0;
	END_STRUCT;
	stackerOutput_typ : 	STRUCT 
		offsetNorthGripper : BOOL;
		offsetSouthGripper : BOOL;
		stackDeliverGripper : BOOL;
		comPulseGripper : BOOL;
	END_STRUCT;
	inSpeed_typ : 	STRUCT 
		is : REAL;
		set : REAL;
		predict : REAL;
		follow : REAL;
		accelerate : BOOL;
		decelerate : BOOL;
	END_STRUCT;
END_TYPE

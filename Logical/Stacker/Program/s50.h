/* ---------------------------------------------*/
/*                                              */
/*  Header file used int Stacker.Program.Main   */
/*                                              */
/* ---------------------------------------------*/
#define MetersPerSecond                     0
#define MetersPerMinute                     1
#define FeetPerMinute                       2
#define NO_LIMIT			                10.0
#define FINGER_START_POS_SPAGHETTI         -20.0

#define SHOW                                0
#define HIDE                                1

#define SETUP                               0
#define ADD                                 1
#define FETCH                               2

#define TOGGLE                              1
#define GO_HOME                             0
#define IN_FINGER_POS                       0
#define IN_STACKER_POS                      1
#define OFF                                 0
#define ON                                  1
#define TRANS                               0
#define DELIV                               1
#define FPUSH                               2
#define FLIFT                               3
#define STOPPL                              4
#define SLIFT                               5
#define TRH                                 0x01
#define DEH                                 0x02
#define FPH                                 0x04
#define FLH                                 0x08
#define STH                                 0x10
#define SLH                                 0x20
#define NO_STOP_PLATE                       0x2f
#define ALL                                 0x3f
#define STOP                                0
#define START                               1
#define FORKS_TO_NORTH_POS                  19 + stacker.setting.forkHoldAtOffsetAdjust//14
#define CRITICAL_FINGER_X_OUT_LIM           16.0
#define CRITICAL_FINGER_Y_BELOW_TABLE_LIM	7.0
#define FINGER_OUT                          fingerX.pos>CRITICAL_FINGER_X_OUT_LIM
#define TABLE_CRASH_LIMIT                   stackerY.transformPos - CRITICAL_FINGER_Y_BELOW_TABLE_LIM
//#define STACKER_TOPPOS                    stackerY.maxPos - GUI.settings.S50_topPosition
#define TRANSPORT_TABLE_LENGTH				860.0
#define TRANSPORT_EXIT_TO_STACK             175
//#define DELIVER_TABLE_LENGTH              620.0
//#define SECONDS_UNIT  				        0.0048
#define SECONDS_UNIT                        (REAL)rt_info.cycle_time/1000000.0
#define MILLISECONDS_UNIT                   (UINT)(rt_info.cycle_time/1000.0)
#define MAX_FORM_SIZE                       438.0
#define BACKSTOP_OFFSET                     MAX_FORM_SIZE - stacker.setting.backStop
#define SOUTH_OFFSET_ADJUST                 0.75

#define DWELL_SOUTH                         0
#define WAIT_HOLDBACK_FORK_IN_POS           1
#define WAIT_STOPPLATE_HOLDBACK_IN_POS      2
#define WAIT_FORKBACK_STOPPLATE_IN_POS      3
#define DWELL_NORTH                         4
#define MOVE_STOPPLATE_TO_SOUTH             5

#define ERR                                 1
#define NO_ERR                              0

#define TRUE                                1
#define FALSE                               0

#define GO_TO_ZERO                          0
#define GO_TO_MINSPEED                      1

#define NOTHING                             0
#define NORTH                               1
#define SOUTH                               2
#define OFFSET_TOGGLE                       3

#define ERR_ACK_CNT                         15
#define REPEAT                              0
#define EXIT_THIS                           1 

#define SET_SPEED                           0
#define GET_SPEED                           1
#define GET_LOWEST_ID                       2
//#define ALL                               63 defined as 0x3f abowe
#define GET_ID                              4
#define GET_NEXT_SPEED                      5
#define LIMITER                             GET_LOWEST_ID,0,0
#define SPEED_LIMIT                         GET_SPEED,0,0
#define SPEEDS_SIZE                         16
#define MAX_SPEED                           5.0 //m/s

#define TRANSPORTTABLE_SENSORS              TRANSPORT_TABLE_EXIT_1,TRANSPORT_TABLE_ENTER_1,ON,ON
#define DELIVERYTABLE_SENSORS               LIFT_TABLE_CLEAR_1,LIFT_TABLE_CLEAR_2,LIFT_TABLE_CLEAR_3,LIFT_TABLE_CLEAR_4
#define RESET_CLEAR                         0,0,0,0,65535
#define STOP_ALL                            globalPower == 0 && !(stacker.setting.ignoreError_Dev & 0x01)
#define TRANSPORT_TABLE_EXIT_1              stacker.reading.input.exitTransport1
#define TRANSPORT_TABLE_ENTER_1             stacker.reading.input.enterTransport
#define LIFT_TABLE_CLEAR_1                  stacker.reading.input.tableClearSensor1
#define LIFT_TABLE_CLEAR_2                  stacker.reading.input.tableClearSensor2
#define LIFT_TABLE_CLEAR_3                  stacker.reading.input.tableClearSensor3
#define LIFT_TABLE_CLEAR_4                  stacker.reading.input.tableClearSensor4
#define STEP_SENSOR                         stacker.reading.input.AnalogStepSensor<stepDownLevel   //0//X4.DI_10
#define BALLRACK                            stacker.reading.input.rackGate
/*------------------- warnings --------------------*/
#define WASTEBOXWARNING                     0
#define SECUNITNOTREADY                     1
#define DELIVERING                          2
#define STOPPEDDELIVERING                   3
#define EMPTY_SLOT1                         4
#define EMPTY_SLOT2                         5
#define MAX_WARNINGID                       6
#define CHK                                 0                                        
#define RST                                 1
/*-------------------------------------------------*/
/*--------------------- load ----------------------*/
#define HOME_LOAD                           0,0,0,0,0
#define FORCED_HOME_LOAD                    1,0,0,0,0
#define FORCED_HOME_LOAD_WASTE              1,1,0,0,0
#define STOPPED_DELIVER                     0,0,1,0,0
#define STOPPED_DELIVER_WASTE               0,1,1,0,0
#define FIND_ZERO                           1,0,0,1,0
#define HOME_ONLY                           1,0,0,0,1
/*-------------------------------------------------*/
/* ----------------  Button color -----------------*/
#define COLOR_OFF                           0,0,0,0
#define COLOR_RED                           0,1,0,0
#define COLOR_YELLOW                        0,0,1,0
#define COLOR_GREEN                         1,0,0,0
#define COLOR_ORANGE                        0,1,1,0
//Button 1,2,3,4 (1,2)
#define COLOR_WHITE                         0,0,0,1
#define COLOR_PINK                          0,1,0,1
//only button 5 (3)
#define COLOR_BLUE                          0,0,0,1
#define COLOR_TURQUOISE                     1,0,0,1
#define COLOR_MAGENTA                       0,1,0,1
#define LIT                                 1,1,1,1
/* ------------------------------------------------*/
/*------------------ Speed Limits -----------------*/
#define MACHINE_MAX                       0
#define TRANSPORT                         1
#define DELIVERY_ON_FLY                   2
#define DELIVERY                          3
#define SOFTSTOP                          4
#define ERROR                             5
#define STOP_TIME                         6
#define NOT_READY                         7
#define FIRST_SHEET                       8
//#define ID_NOT_USED_9						9
//#define ID_NOT_USED_10					10
//#define ID_NOT_USED_11					11
//#define ID_NOT_USED_12					12
#define HOMING                            13
#define READY                             14
//#define ID_NOT_USED_15					15
/*-------------------------------------------------*/
/*------------------- page types ------------------*/
#define NORMAL_PAGE                       0
#define OFFSET_PAGE                       1
#define DELIVER_PAGE                      2
#define FAKE_DELIVER_PAGE                 3
/*-------------------------------------------------*/
/*--------------- transportFloorSpeed -------------*/
#define ENTER_TRANSPORT                   0
#define EXIT_TRANSPORT                    1
#define DELIVER_ON_FLY_MINSPEED           2
#define DELIVER_ON_FLY                    3
#define WEB_SPEED                         4
#define FEED                              5
#define KNIFE_ROUND                       6
#define STOPPED_DELIVERY_MINSPEED         7

#define TRANS_SPEED_QUEUE_SIZE            8
/*------------------------------------------------*/

/*------------- pageInTransportQueue -------------*/
/*----------------- functions --------------------*/
#define POST_PAGE                         0
#define PEEK_PAGE                         1,dummySheet
#define INIT_QUEUE                        2,dummySheet
#define GET_NO_PAGES_IN_QUEUE             3,dummySheet
#define REMOVE_LAST_PAGE                  4,dummySheet
#define PEEK_PAGE_SIZE                    5,dummySheet
#define FIRST_PAGE                        6,dummySheet
#define FORWARD_PEAK_PAGE                 7,dummySheet

#define TRANSPORT_QUEUE_SIZE              32
/*------------------------------------------------*/
/*------------------ SoftStop --------------------*/
#define BRACK                              0		
#define BTN_PRESS                          1
#define FREE2                              2
#define FREE3                              3
#define FREE4                              4
#define SOFTSTOPMEMBERSIZE                 5

#define holdSoftStop                       0
#define releaseSoftStop                    1
#define readSoftStopStatus                 2
#define mySoftStopStatus                   3
#define releaseAllSoftStop                 4
/*-----------------------------------------------*/
/*------------------ error list -----------------*/
/*        ERRORs (24 errors maximum)        */
#define NOT_USED0                          0 //free
#define TRANSPORT_OUTFEED                  1
#define AXIS                               2
#define DELIVER                            3
#define TOP_COVER_BYPASSED                 4
#define TRANSPORT_INFEED                   5
#define QUEUE_ERROR                        6
#define AXIS_NOT_HOMED                     7
#define DRIVES_DISABLED                    8
#define E_STOP_BY_ME                       9
#define WASTE_BOX_FULL                     10 
#define LIGHT_CURTAIN                      11 
#define GRIPPER_NOT_READY                  12 //byt till 5xxx
#define STACKER_INFEED_CRASH               13 //free
#define ALIGNER_ROLLER_GATE                14
#define INFEED_ROLLER_GATE                 15
#define TABLE_NOT_CLEAR                    16
#define NO_POWER                           17
#define COVER_OPEN                         18
#define SEC_UNIT_NOT_READY                 19
#define STACKER_BUSY                       20
#define NOT_USED1                          21 //free
#define NOT_USED2                          22 //free
#define NOT_USED3                          23 //free

#define LAST_ERROR                         24
//error stop types
#define NO_ERROR                           0
#define WARNING                            1
#define NORMAL_STOP                        2
#define CRASH_STOP                         3
#define PANIC_STOP                         4
/*-----------------------------------------------*/
/*--------- Deliver On The Fly states ---------- */
#define PENDING_DELIVER_WAITING            0
#define DELIVER_PAGE_WAITING               1
#define LAST_SHEET_INSTACK_WAITING         2
#define STACK_DROP_WAITING                 3
#define DELIVER_STACK_1                    5
#define DELIVER_STACK_2                    6
#define TABLE_UP                           7
#define STACKER_FINGER_TRANSITON           8
#define RETRACT_FINGERS                    9
#define EXIT                               10
/*-----------------------------------------------*/

REAL speed(REAL spd, USINT unit)
{
	if (unit ==  MetersPerSecond) return spd;
	if (unit ==  MetersPerMinute) return spd*0.0166667;
	if (unit ==  FeetPerMinute)   return spd*0.00508;
	return spd;
}

REAL rabs(REAL a)
{
	if (a<0) return a*-1.0;
	else return a;
}

REAL max (REAL a, REAL b)
{
	if (a>b) return a;
	return b;
}

REAL min (REAL a, REAL b)
{
	if (a<b) return a;
	return b;
}

void realMinMaxCheck (REAL * var, REAL min, REAL max) {
	if (*var > max) *var = max;
	else if (*var < min) *var = min;
}

void setBit (USINT x, DWORD * var)
{
	*var |= 1 << x;
}

void clearBit (USINT x, DWORD * var)
{
	*var &= ~(1 << x);
}

void toggleBit (USINT x, DWORD * var)
{
	*var ^= 1 << x;
}

BOOL readBit (USINT x, DWORD var)
{
 return ((var >> x) & 1);
}

UINT ticks (REAL ms, UDINT CycleTime)
{
	return (UINT)((ms * 1000) / CycleTime);
}

DINT map(DINT x, DINT in_min, DINT in_max, DINT out_min, DINT out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

BOOL clear (BOOL a, BOOL b, BOOL c, BOOL d, UINT clear)
{
	static UINT clearCnt;
	
	if (clear == 65535) clearCnt = 0; //reset 
	
	if (a && b && c && d) clearCnt++;
	else clearCnt = 0; // paper on table
	
	if (clearCnt>clear) return 1; // return 1 when table clear, looks clear to me
	return 0; 
}

REAL transform (USINT func, REAL position, REAL equalPositionFingers, REAL equalPositionStacker)
{
	REAL retval;
	switch (func) {
		case IN_FINGER_POS:
			retval = position - equalPositionStacker + equalPositionFingers;
			break;
		case IN_STACKER_POS:
			retval = position - equalPositionFingers + equalPositionStacker;
			break;
		default:
			retval = 0;
			break;
	}
	return retval;
}

REAL tRtime (REAL distance, REAL speed, REAL limit) //mm,m/s,s
{
	if (speed < 0.015) return limit;
	return distance/(speed*1000);
}

REAL tSpeed (REAL minspeed, REAL offset, REAL gain, REAL source)
{
	REAL followSpeed;
	followSpeed = source * gain + offset;
	if (minspeed<offset && followSpeed<=offset) followSpeed = minspeed; 
	return max(minspeed,followSpeed);
}

char *inttostr(long i, char b[]){
	char const digit[] = "0123456789";
	char* p = b;
	if(i<0) {
		*p++ = '-';
		i *= -1;
	}
	long shifter = i;
	do { 
		++p;
		shifter = shifter/10;
	}while(shifter);
	*p = '\0';
	do { 
		*--p = digit[i%10];
		i = i/10;
	}while(i);
	return b;
}

char *uinttostr(unsigned long i, char b[]){
	char const digit[] = "0123456789";
	char* p = b;
	unsigned long shifter = i;
	do { 
		++p;
		shifter = shifter/10;
	}while(shifter);
	*p = '\0';
	do { 
		*--p = digit[i%10];
		i = i/10;
	}while(i);
	return b;
}

REAL realMod(REAL a, REAL b)
{
   while  (a>b) a = a - b;
   return a;
}

BOOL realEqual (REAL valA, REAL valB, REAL tollerance) 
{
   if (valA > (valB-tollerance) && valB < (valB+tollerance))    return TRUE;
   return FALSE;
}

REAL lowest(REAL a, REAL b, REAL c)
{
	if (a<b) {
		if (a<c) return a;
		return c;
	}
	else {
		if (b<c) return b;
		return c;
	}
}

REAL highest(REAL a, REAL b, REAL c)
{
	if (a>b) {
		if (a>c) return a;
		return c;
	}
	else {
		if (b>c) return b;
		return c;
	}
}


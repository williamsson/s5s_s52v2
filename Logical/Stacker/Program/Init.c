
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif


void _INIT ProgramInit(void)
{
	rt_info.enable = 1;                     /* enables the function */
	RTInfo(&rt_info);                       /* gets information from the software object */
	/*-------------SPEEDAXIS--------------*/
	infeedTransport.acceleration      = 4.0;
	infeedTransport.deceleration      = 4.0;
	infeedTransport.speed             = 0.0;
	infeedTransport.lastSpeed         = 0.0;
	infeedTransport.maxSpeed          = 5.0;
	infeedTransport.minSpeed          = 0.0;
	infeedTransport.homed             = 0;
	infeedTransport.error             = 0;
	infeedTransport.actualPosition    = 0.0;
	infeedTransport.direction         = forward;
	/*-------------SPEEDAXIS--------------*/
	deliverTableBelt.acceleration     = 0.7;
	deliverTableBelt.deceleration     = 4.5;
	deliverTableBelt.speed 			  = 0.0;
	deliverTableBelt.lastSpeed 	      = 0.0;
	deliverTableBelt.maxSpeed 		  = 1.5;
	deliverTableBelt.minSpeed		  = 0.0;//-1.5;
	deliverTableBelt.homed			  = 0;
	deliverTableBelt.error			  = 0;
	deliverTableBelt.actualPosition   = 0.0;
	deliverTableBelt.direction		  = forward;
	/*-----------POSITIONAXIS-------------*/
	fingerX.acceleration              = 2.0;
	fingerX.deceleration              = 2.0;
	fingerX.speed                     = 1.5;
	fingerX.homed                     = 0;
	fingerX.error                     = 0;
	fingerX.maxPos                    = 457;
	fingerX.minPos                    = -20.0;
	fingerX.homeSettleTime            = 1.2;
	/*-----------POSITIONAXIS-------------*/
	fingerY.acceleration              = 1.7;
	fingerY.deceleration              = 1.5;
	fingerY.speed                     = 0.4;
	fingerY.homed                     = 0;
	fingerY.error                     = 0;
	fingerY.maxPos                    = 32.0;//20.65;//LSI demo 20.0// 21.0//23.0; //old stacker (beta) 29.0;
	fingerY.minPos                    = -2.0;
	fingerY.homeSettleTime            = 0.6;
	/*-----------POSITIONAXIS-------------*/
	stackerY.acceleration             = 2.0;
	stackerY.deceleration             = 2.0;
	stackerY.speed 					  = 0.5;//0.35;//0.5;
	stackerY.homed					  = 0;
	stackerY.error                    = 0;
	stackerY.maxPos					  = 293;//234;//100;///243.0;, 240 old stacker
	stackerY.minPos                   = -55;
	stackerY.homeSettleTime           = 0.7;
   /*-----------POSITIONAXIS-------------*/
	stopPlateX.acceleration           = 0.175;
	stopPlateX.deceleration           = 0.15;
	stopPlateX.speed                  = 0.10; //0.05
	stopPlateX.homed				  = 0;
	stopPlateX.error                  = 0;
	stopPlateX.maxPos                 = 2.0;
	stopPlateX.minPos                 = -276; //276.0
	stopPlateX.homeSettleTime         = 0.4;
	
	state = NotLoadedMode;
	step.stackPages = 0;
	version.Program = 9;
	stacker.setting.useStepSensor = 1;
	deliverFly.stackDropFngOutWait = 0.18; //was 0.1, try lower it.. reduces delivery stop time
   
	elevatorForkDebug[0] = 0;
	elevatorForkDebug[1] = 0;
	elevatorForkDebug[2] = 0;
	elevatorForkDebug[3] = 0;
	elevatorForkDebug[4] = 0;
	elevatorForkDebug[5] = 0;
	elevatorForkDebug[6] = 0;
	elevatorForkDebug[7] = 0;
 
	stacker.reading.offsetPosition = north;
	stacker.setting.ignoreError_Dev = 0; ///disable some errors 7
	stacker.setting.forceAxis_Dev = 0;
	stacker.reading.axisStatus.deliver.lastErrorID = 0;
	stacker.reading.axisStatus.endStop.lastErrorID = 0;
	stacker.reading.axisStatus.forkElevator.lastErrorID = 0;
	stacker.reading.axisStatus.forkPush.lastErrorID = 0;
	stacker.reading.axisStatus.stackerElevator.lastErrorID = 0;
	stacker.reading.axisStatus.transport.lastErrorID = 0;
	autoAdjustState = 0;
	errResetCnt = 15;	
	stacker.reading.stackPages = step.stackPages = 0;
    lightCurtainErrTriggerd = 0;
   
    stacker.setting.automaticEndStop = lineController.setup.stacker.automaticStopPlate;
   
    spaghettiBelt.Parameter.Velocity = 0;
    spaghettiBelt.Parameter.Acceleration = 3000;
    spaghettiBelt.Parameter.Deceleration = 3000;
    lastOffset = stackerCounter.offsets;
   
    offsetTime = 20; //ms
    offsetDist = 210; //mm from back edge
   
    holdBackTime = 25; //ms 
    holdBackDist = 50; //mm from back edge
   
    curlDistance = 30;
    
    //stacker.setting.offset.inSouthDelayDistHoldBack = 150;
    //stacker.setting.offset.inNorthDelayDistHoldBack = 150;
    //stacker.setting.offset.inNorthDelayDist = 210;
    //stacker.setting.offset.inSouthDelayDist = 210;
    //stacker.setting.offset.inSouthTimeOffsetHoldBack = -15;
    //stacker.setting.offset.inNorthTimeOffsetHoldBack = -15;
    //stacker.setting.offset.inSouthTimeOffset = -30;
    //stacker.setting.offset.inNorthTimeOffset = -30;
    //stacker.setting.delayNorthOffset = 1;
    
    warnings[0] = warnings[1] = warnings[2] = warnings[3] = warnings[4] = warnings[5] = warnings[6] = 0;
	 fromInit = 1;
}


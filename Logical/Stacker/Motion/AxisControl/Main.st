
PROGRAM _INIT
	//Stacker
   AxisTransportObj := ADR(gAxTransport);
	AxisFingerPushObj := ADR(gAxFingerPush);
	AxisFingerLiftObj := ADR(gAxFingerLift);
	AxisStopPlateObj := ADR(gAxStopPlate);
	AxisStackerDeliveryObj := ADR(gAxStackerDelivery);
   AxisStackerLiftObj := ADR(gAxStackerLift);
   //Streamer
   AxisStreamerSection1Obj := ADR(gAxStrSt1);
   
	
	//io.input.stackerHomeEndStop;
	//io.input.stackerHomeElevator;
	//io.input.stackerHomeForkLift;
	//io.input.stackerHomeForkPush;
	
	//homePosFingerPush;
	//homePosFingerLift;
	//fingerLimit;
	//homePosLift;
	

END_PROGRAM

PROGRAM _CYCLIC
	IF (lineController.setup.stacker.installed) THEN 
	//X2.DI_5 := homePosFingerPush;
    //X2.DI_7 := homePosFingerLift;
	//X2.DI_9 := fingerLimit;
	//X2.DI_11 := homePosLift;
	
    stacker.reading.input.homeElevator := physical.stacker.digital.in14;
	stacker.reading.input.homeEndStop := physical.stacker.digital.in17;
 	stacker.reading.input.homeForkLift := physical.stacker.digital.in10;
	stacker.reading.input.homeForkPush := physical.stacker.digital.in8;
	
	(********************MC_BR_SetHardwareInputs***********************)
	FB_SetHardwareInput_FingerPush.Enable := (NOT(FB_SetHardwareInput_FingerPush.Error));
	FB_SetHardwareInput_FingerPush.Axis := AxisFingerPushObj;
	//FB_SetHardwareInput_FingerPush.HomeSwitch := homePosFingerPush;
	FB_SetHardwareInput_FingerPush.HomeSwitch := physical.stacker.digital.in8;//io.input.stackerHomeForkPush;
	//FB_SetHardwareInput_FingerPush.NegHWSwitch := fingerLimit;
	FB_SetHardwareInput_FingerPush();	

	FB_SetHardwareInput_FingerLift.Enable := (NOT(FB_SetHardwareInput_FingerLift.Error));
	FB_SetHardwareInput_FingerLift.Axis := AxisFingerLiftObj;
	//FB_SetHardwareInput_FingerLift.HomeSwitch := homePosFingerLift;
	FB_SetHardwareInput_FingerLift.HomeSwitch := physical.stacker.digital.in10;//io.input.stackerHomeForkLift;
	FB_SetHardwareInput_FingerLift();	
	
	FB_SetHardwareInput_StopPlate.Enable := (NOT(FB_SetHardwareInput_StopPlate.Error));
	FB_SetHardwareInput_StopPlate.Axis := AxisStopPlateObj;
	//FB_SetHardwareInput_StopPlate.HomeSwitch := homePosStopPlate;
	FB_SetHardwareInput_StopPlate.HomeSwitch := physical.stacker.digital.in17;//io.input.stackerHomeEndStop;
	FB_SetHardwareInput_StopPlate();	

	FB_SetHardwareInput_StackerLift.Enable := (NOT(FB_SetHardwareInput_StackerLift.Error));
	FB_SetHardwareInput_StackerLift.Axis := AxisStackerLiftObj;
	//FB_SetHardwareInput_StackerLift.HomeSwitch := homePosLift;
	FB_SetHardwareInput_StackerLift.HomeSwitch := physical.stacker.digital.in14;//io.input.stackerHomeElevator;
	FB_SetHardwareInput_StackerLift();	
	
	//homeSensors[0] := homePosLift;
	//homeSensors[1] := homePosFingerLift;
	//homeSensors[2] := homePosFingerPush;
      //homeSensors[3] := homePosStopPlate;
   END_IF
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	(************************************************************************
	********************** CUTTER IOT DATA **********************************
	************************************************************************)
	// NEED MORE COMMENTS, WHY NOT USED DIRECTLY ActPosition?
//	IF(Master[0].Status.ActVelocity > 0.0) THEN
//		cutter_runDistance := cutter_runDistance + ((0.1008 * Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity) / 1000.0);
//	END_IF
   IF EDGEPOS(Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.ControllerStatus) OR EDGENEG(Slave[SLAVE_MAININFEED_INDEX].AxisState.Homing) OR EDGEPOS(Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.HomingOk) THEN
      rOldPosCutter				:= Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition;
      IF rOldPosCutter < rRolloverCutter/3.0 THEN
         iRolloverCutterCheck	:= 0;
      ELSIF rOldPosCutter < rRolloverCutter*(2.0/3.0) THEN
         iRolloverCutterCheck	:= 1;
      ELSE
         iRolloverCutterCheck	:= 2;
      END_IF
   END_IF
   IF Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.ControllerStatus AND Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.HomingOk AND NOT Slave[SLAVE_MAININFEED_INDEX].AxisState.Homing THEN
      CASE iRolloverCutterCheck OF
         0:	// 0 to rRolloverCutter/3
            IF Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition >= rRolloverCutter*(2.0/3.0) THEN	// Rollover backward
               rIncrPosCutter			:= ((Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition - rOldPosCutter - rRolloverCutter))/1000.0;
               iRolloverCutterCheck	:= 2;
            ELSE
               IF Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition >= rRolloverCutter/3.0 THEN
                  iRolloverCutterCheck	:= 1;
               END_IF
               rIncrPosCutter		:= ((Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition - rOldPosCutter))/1000.0;
            END_IF
         1:	// rRolloverCutter/3 to rRolloverCutter*(2/3)
            IF Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition >= rRolloverCutter*(2.0/3.0) THEN
               iRolloverCutterCheck	:= 2;
            ELSIF Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition <= rRolloverCutter/3.0 THEN
               iRolloverCutterCheck	:= 0;
            END_IF
            rIncrPosCutter		:= ((Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition - rOldPosCutter))/1000.0;
         2:	// rRolloverCutter(2/3) to rRolloverCutter
            IF Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition <= rRolloverCutter/3.0 THEN	// Rollover forward
               rIncrPosCutter			:= ((Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition - rOldPosCutter + rRolloverCutter))/1000.0;
               iRolloverCutterCheck	:= 0;
            ELSIF Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition <= rRolloverCutter*(2.0/3.0) THEN
               rIncrPosCutter			:= ((Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition - rOldPosCutter))/1000.0;
               iRolloverCutterCheck	:= 1;
            ELSE
               rIncrPosCutter			:= ((Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition - rOldPosCutter))/1000.0;
            END_IF
      END_CASE
      IF rIncrPosCutter >= 0 THEN
         cutter_runDistance		:= cutter_runDistance + rIncrPosCutter;
      END_IF
      rOldPosCutter				:= Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition;
   END_IF
   // Statistics and unit info
   cutter_iot.installed 			:= TRUE; //lineController.setup.cutter.installed;
   cutter_iot.enabled				:= TRUE; //lineController.setup.cutter.installed;
   cutter_iot.knife2_installed		:= lineController.setup.cutter.secondKnife;
   cutter_iot.knife2_enabled		:= cutter.setting.stripCut;
   cutter_iot.cut_length			:= cutter.reading.sheetSize;
   cutter_iot.strip_length			:= cutter.setting.stripSize;
   cutter_iot.speed_limit			:= cutter.setting.infeedRunSpeed;
   IF cutter_runDistance >= 0 THEN
      cutter_iot.run_distance		:= LREAL_TO_UDINT(cutter_runDistance);
   END_IF
   cutter_iot.cut1_counter			:= iot_knife1Count;
   cutter_iot.cut2_counter			:= iot_knife2Count;
   cutter_iot.valid_marks_counter	:= iot_validMarks;
   cutter_iot.missed_marks_counter	:= iot_missedMarks;
   // After 1000mm we increase iot counter by 1, as all units are in meter
   IF iot_blankPaper_mm >= 1000 THEN
      iot_blankPaper_mm			:= iot_blankPaper_mm - 1000;
      cutter_iot.blank_distance	:= cutter_iot.blank_distance + 1;
   END_IF
   cutter_iot.processed_sheets		:= iot_processedSheets;
   cutter_iot.rejected_sheets		:= iot_rejectedSheets;
   // After 1000mm we increase iot counter by 1, as all units are in meter
   IF iot_rejectedLenght_mm >= 1000 THEN
      iot_rejectedLenght_mm			:= iot_rejectedLenght_mm - 1000;
      cutter_iot.rejected_distance	:= cutter_iot.rejected_distance + 1;
   END_IF
   cutter_iot.set_counter			:= iot_setCounter;
   // Cutter state
   IF(cutter.reading.status.state = Status_Error) THEN
      cutter_iot.machine_state := iotMS_Error;
   ELSIF (cutter.reading.status.state = Status_Ready) THEN
      IF(cutter.reading.status.stopped) THEN
         IF (NOT(cutter.reading.input.frontDoor) OR NOT(cutter.reading.input.frontDoorKnife) OR NOT(cutter.reading.input.topCover)) THEN
            cutter_iot.machine_state	:= iotMS_Setup;
         ELSE
            cutter_iot.machine_state	:= iotMS_Stopped;
         END_IF
      ELSE
         IF (cutter_iot.infeed_motor.speed > 0) THEN
            cutter_iot.machine_state	:= iotMS_Run;
         ELSE
            cutter_iot.machine_state	:= iotMS_Idle;
         END_IF
      END_IF
   ELSIF(cutter.reading.status.state = Status_Loading) THEN
      cutter_iot.machine_state	:= iotMS_Loading;
   ELSE // if(cutter.reading.status.state == Status_NotLoaded)
      cutter_iot.machine_state	:= iotMS_NotLoaded;
   END_IF
   // Cutter motor info
   cutter_iot.knife1_motor.drive_error		:= iot_cutterKnife1Error;  
   cutter_iot.knife1_motor.torque			:= iot_cutterKnife1Torque;
   cutter_iot.knife1_motor.current_Q		:= iot_cutterKnife1Current_Q;
   cutter_iot.knife1_motor.current_D		:= iot_cutterKnife1Current_D;
   cutter_iot.knife1_motor.current			:= 0;//FC_rResultant(cutter_iot.knife1_motor.current_Q,cutter_iot.knife1_motor.current_D);
   cutter_iot.knife1_motor.temperature		:= iot_cutterKnife1Temp;
   cutter_iot.knife1_motor.position		:= (Slave[SLAVE_MAINKNIFE_INDEX].Status.ActPosition * 0.3556) / 360000;
   IF (Slave[SLAVE_MAINKNIFE_INDEX].Status.ActVelocity > 200.0) THEN
      //cutter_iot.knife1_motor.speed	:= Slave[SLAVE_MAINKNIFE_INDEX].Status.ActVelocity / 600.0; // 0.01 deg/s to rpm
      cutter_iot.knife1_motor.speed	:= (Slave[SLAVE_MAINKNIFE_INDEX].Status.ActVelocity * 0.3556) / 360000; // 0.01 deg/s to m/s
   ELSE
      cutter_iot.knife1_motor.speed	:= 0;
   END_IF
   //
   cutter_iot.knife2_motor.drive_error		:= iot_cutterKnife2Error;   
   cutter_iot.knife2_motor.torque			:= iot_cutterKnife2Torque;
   cutter_iot.knife2_motor.current_Q		:= iot_cutterKnife2Current_Q;
   cutter_iot.knife2_motor.current_D		:= iot_cutterKnife2Current_D;
   cutter_iot.knife2_motor.current			:= 0;//FC_rResultant(cutter_iot.knife2_motor.current_Q,cutter_iot.knife2_motor.current_D);
   cutter_iot.knife2_motor.temperature		:= iot_cutterKnife2Temp;
   cutter_iot.knife2_motor.position		:= (Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.ActPosition * 0.3556) / 360000;
   IF (Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.ActVelocity > 200.0) THEN
      //cutter_iot.knife2_motor.speed	:= Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.ActVelocity / 600.0; // 0.01 deg/s to rpm
      cutter_iot.knife2_motor.speed	:= (Slave[SLAVE_SECONDARYKNIFE_INDEX].Status.ActVelocity * 0.3556) / 360000; // 0.01 deg/s to m/s
   ELSE
      cutter_iot.knife2_motor.speed	:= 0;
   END_IF
   //
   cutter_iot.infeed_motor.drive_error		:= iot_cutterInfeedError;   
   cutter_iot.infeed_motor.torque			:= iot_cutterInfeedTorque;
   cutter_iot.infeed_motor.current_Q		:= iot_cutterInfeedCurrent_Q;
   cutter_iot.infeed_motor.current_D		:= iot_cutterInfeedCurrent_D;
   cutter_iot.infeed_motor.current			:= 0;//FC_rResultant(cutter_iot.infeed_motor.current_Q,cutter_iot.infeed_motor.current_D);
   cutter_iot.infeed_motor.temperature		:= iot_cutterInfeedTemp;
   cutter_iot.infeed_motor.position		:= Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition / 1000.0;
   IF (Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity > 2.0) THEN
      cutter_iot.infeed_motor.speed	:= Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity / 1000.0;
   ELSE
      cutter_iot.infeed_motor.speed	:= 0;
   END_IF
   //
   cutter_iot.feed_support_motor.drive_error	:= iot_cutterFeedSupportError;   
   cutter_iot.feed_support_motor.torque		:= iot_cutterFeedSupportTorque;
   cutter_iot.feed_support_motor.current_Q		:= iot_cutterFeedSupportCurrent_Q;
   cutter_iot.feed_support_motor.current_D		:= iot_cutterFeedSupportCurrent_D;
   cutter_iot.feed_support_motor.current		:= 0;//FC_rResultant(cutter_iot.feed_support_motor.current_Q,cutter_iot.feed_support_motor.current_D);
   cutter_iot.feed_support_motor.temperature	:= iot_cutterFeedSupportTemp;
   cutter_iot.feed_support_motor.position		:= Slave[SLAVE_SUPPORT_INDEX].Status.ActPosition / 1000.0;
   IF (Slave[SLAVE_SUPPORT_INDEX].Status.ActVelocity > 2.0) THEN
      cutter_iot.feed_support_motor.speed		:= Slave[SLAVE_SUPPORT_INDEX].Status.ActVelocity / 1000.0;
   ELSE
      cutter_iot.feed_support_motor.speed		:= 0;
   END_IF
   //
   cutter_iot.reject_motor.drive_error		:= iot_cutterRejectError;   
   cutter_iot.reject_motor.torque			:= iot_cutterRejectTorque;
   cutter_iot.reject_motor.current_Q		:= iot_cutterRejectCurrent_Q;
   cutter_iot.reject_motor.current_D		:= iot_cutterRejectCurrent_D;
   cutter_iot.reject_motor.current			:= 0;//FC_rResultant(cutter_iot.reject_motor.current_Q,cutter_iot.reject_motor.current_D);
   cutter_iot.reject_motor.temperature		:= iot_cutterRejectTemp;
   cutter_iot.reject_motor.position		:= Slave[SLAVE_REJECT_INDEX].Status.ActPosition / 1000.0;
   IF (Slave[SLAVE_REJECT_INDEX].Status.ActVelocity > 2.0) THEN
      cutter_iot.reject_motor.speed		:= Slave[SLAVE_REJECT_INDEX].Status.ActVelocity / 1000.0;
   ELSE
      cutter_iot.reject_motor.speed		:= 0;
   END_IF
	
	(************************************************************************
	********************** MERGER IOT DATA **********************************
	************************************************************************)
	// NEED MORE COMMENTS, WHY NOT USED DIRECTLY ActPosition?
//	IF (Master[0].Status.ActVelocity > 0.0 AND lineController.setup.merger.installed) THEN
//		merger_runDistance := merger_runDistance + ((0.1008 * Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActVelocity) / 1000.0);
//	END_IF
   IF EDGEPOS(Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.ControllerStatus) OR EDGENEG(Slave[SLAVE_MERGE_INFEED_INDEX].AxisState.Homing) OR EDGEPOS(Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.HomingOk) THEN
      rOldPosMerger				:= Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition;
      IF rOldPosMerger < rRolloverMerger/3.0 THEN
         iRolloverCutterCheck	:= 0;
      ELSIF rOldPosMerger < rRolloverMerger*(2.0/3.0) THEN
         iRolloverCutterCheck	:= 1;
      ELSE
         iRolloverCutterCheck	:= 2;
      END_IF
   END_IF
   IF Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.ControllerStatus AND Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.HomingOk AND NOT Slave[SLAVE_MERGE_INFEED_INDEX].AxisState.Homing THEN
      CASE iRolloverMergerCheck OF
         0:	// 0 to rRolloverMerger/3
            IF Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition >= rRolloverMerger*(2.0/3.0) THEN	// Rollover backward
               rIncrPosMerger		:= ((Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition - rOldPosMerger - rRolloverMerger))/1000.0;
               iRolloverMergerCheck	:= 2;
            ELSE
               IF Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition >= rRolloverMerger/3.0 THEN
                  iRolloverMergerCheck	:= 1;
               END_IF
               rIncrPosMerger		:= ((Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition - rOldPosMerger))/1000.0;
            END_IF
         1:	// rRolloverMerger/3 to rRolloverMerger*(2/3)
            IF Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition >= rRolloverMerger*(2.0/3.0) THEN
               iRolloverMergerCheck	:= 2;
            ELSIF Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition <= rRolloverMerger/3.0 THEN
               iRolloverMergerCheck	:= 0;
            END_IF
            rIncrPosMerger		:= ((Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition - rOldPosMerger))/1000.0;
         2:	// rRolloverMerger(2/3) to rRolloverMerger
            IF Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition <= rRolloverMerger/3.0 THEN	// Rollover forward
               rIncrPosMerger			:= ((Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition - rOldPosMerger + rRolloverMerger))/1000.0;
               iRolloverMergerCheck	:= 0;
            ELSIF Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition <= rRolloverMerger*(2.0/3.0) THEN
               rIncrPosMerger			:= ((Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition - rOldPosMerger))/1000.0;
               iRolloverMergerCheck	:= 1;
            ELSE
               rIncrPosMerger			:= ((Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition - rOldPosMerger))/1000.0;
            END_IF
      END_CASE
      IF rIncrPosMerger >= 0 THEN
         merger_runDistance		:= merger_runDistance + rIncrPosMerger;
      END_IF
      rOldPosMerger				:= Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition;
   END_IF
   // Statistics and unit info
   merger_iot.installed			:= lineController.setup.merger.installed;
   merger_iot.enabled				:= merger.setting.enabled;
   merger_iot.puncher_installed	:= lineController.setup.merger.punchUnitInstalled;
   merger_iot.puncher_enabled		:= merger.setting.punchUnitEnabled;
   merger_iot.speed_limit			:= cutter.setting.infeedRunSpeed;
   merger_iot.punch_counter		:= iot_mergerPunchCount;
   IF merger_runDistance >= 0 THEN
      merger_iot.run_distance		:= LREAL_TO_UDINT(merger_runDistance);
   END_IF
   // Merger state
   IF (merger.reading.status.state = Status_Error) THEN
      merger_iot.machine_state	:= iotMS_Error;
   ELSIF (merger.reading.status.state = Status_Ready) THEN
      IF (merger.reading.status.stopped) THEN
         IF (NOT(merger.reading.input.frontDoor) OR NOT(cutter.reading.input.frontDoor) OR NOT(cutter.reading.input.frontDoorKnife) OR NOT(cutter.reading.input.topCover)) THEN
            merger_iot.machine_state	:= iotMS_Setup;
         ELSE
            merger_iot.machine_state	:= iotMS_Stopped;
         END_IF
      ELSE
         IF (merger_iot.infeed_motor.speed > 0) THEN
            merger_iot.machine_state	:= iotMS_Run;
         ELSE
            merger_iot.machine_state	:= iotMS_Idle;
         END_IF
      END_IF
   ELSIF (merger.reading.status.state = Status_Loading) THEN
      merger_iot.machine_state	:= iotMS_Loading;
   ELSE // if(merger.reading.status.state == Status_NotLoaded)
      merger_iot.machine_state	:= iotMS_NotLoaded;
   END_IF
   // Merger motor info
   merger_iot.infeed_motor.drive_error	:= iot_mergerInfeedError;   
   merger_iot.infeed_motor.torque		:= iot_mergerInfeedTorque;
   merger_iot.infeed_motor.current_Q	:= iot_mergerInfeedCurrent_Q;
   merger_iot.infeed_motor.current_D	:= iot_mergerInfeedCurrent_D;
   merger_iot.infeed_motor.current		:= 0;//FC_rResultant(merger_iot.infeed_motor.current_Q,merger_iot.infeed_motor.current_D);
   merger_iot.infeed_motor.temperature	:= iot_mergerInfeedTemp;
   merger_iot.infeed_motor.position	:= Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition / 1000.0;
   IF (Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActVelocity > 2.0) THEN
      merger_iot.infeed_motor.speed	:= Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActVelocity / 1000.0;
   ELSE
      merger_iot.infeed_motor.speed	:= 0;
   END_IF
   //
   merger_iot.punch_motor.drive_error	:= iot_mergerPunchError;   
   merger_iot.punch_motor.torque		:= iot_mergerPunchTorque;
   merger_iot.punch_motor.current_Q	:= iot_mergerPunchCurrent_Q;
   merger_iot.punch_motor.current_D	:= iot_mergerPunchCurrent_D;
   merger_iot.punch_motor.current		:= 0;//FC_rResultant(merger_iot.punch_motor.current_Q,merger_iot.punch_motor.current_D);
   merger_iot.punch_motor.temperature	:= iot_mergerPunchTemp;
   merger_iot.punch_motor.position		:= (Slave[SLAVE_PUNCH_INDEX].Status.ActPosition * 0.3556) / 360000;
   IF (Slave[SLAVE_PUNCH_INDEX].Status.ActVelocity > 200.0) THEN
      //merger_iot.punch_motor.speed	:= Slave[SLAVE_PUNCH_INDEX].Status.ActVelocity / 600.0; // 0.01 deg/s to rpm
      merger_iot.punch_motor.speed	:= (Slave[SLAVE_PUNCH_INDEX].Status.ActVelocity * 0.3556) / 360000; // 0.01 deg/s to m/s
   ELSE
      merger_iot.punch_motor.speed	:= 0;
   END_IF
	
	(************************************************************************
	********************** FOLDER IOT DATA **********************************
	************************************************************************)
	// Statistics and unit info
   folder_iot.installed	:= lineController.setup.folder.installed;
   folder_iot.enabled		:= lineController.setup.folder.installed;
   folder_iot.speed_limit	:= cutter.setting.infeedRunSpeed;
   // Folder state
   IF (folder.reading.status.state	= Status_Error) THEN
      folder_iot.machine_state	:= iotMS_Error;
   ELSIF (folder.reading.status.state = Status_ManualLoad) THEN
      folder_iot.machine_state	:= iotMS_Setup;
   ELSIF (folder.reading.status.state = Status_Ready) THEN
      IF (folder.reading.status.stopped) THEN
         IF (NOT(folder.reading.input.infeedSideDoor) OR NOT(folder.reading.input.infeedTopCover) OR NOT(folder.reading.input.outfeedTopCover) OR NOT(folder.reading.input.rightFrontDoor)) THEN
            folder_iot.machine_state	:= iotMS_Setup;
         ELSE
            folder_iot.machine_state	:= iotMS_Stopped;
         END_IF
      ELSE
         IF (folder_iot.infeed_motor.speed > 0)  THEN
            folder_iot.machine_state	:= iotMS_Run;
         ELSE
            folder_iot.machine_state	:= iotMS_Idle;
         END_IF
      END_IF
   ELSIF (folder.reading.status.state = Status_Loading) THEN
      folder_iot.machine_state	:= iotMS_Loading;
   ELSE // if(folder.reading.status.state == Status_NotLoaded)
      folder_iot.machine_state	:= iotMS_NotLoaded;
   END_IF
   // Folder motor info
   folder_iot.infeed_motor.drive_error		:= iot_folderInfeedError;   
   folder_iot.infeed_motor.torque			:= iot_folderInfeedTorque;
   folder_iot.infeed_motor.current_Q		:= iot_folderInfeedCurrent_Q;
   folder_iot.infeed_motor.current_D		:= iot_folderInfeedCurrent_D;
   folder_iot.infeed_motor.current			:= 0;//FC_rResultant(folder_iot.infeed_motor.current_Q,folder_iot.infeed_motor.current_D);
   folder_iot.infeed_motor.temperature		:= iot_folderInfeedTemp;
   folder_iot.infeed_motor.position		:= Slave[SLAVE_PLOW_IN_INDEX].Status.ActPosition / 1000.0;
   IF (Slave[SLAVE_PLOW_IN_INDEX].Status.ActVelocity > 2.0) THEN
      folder_iot.infeed_motor.speed	:= Slave[SLAVE_PLOW_IN_INDEX].Status.ActVelocity / 1000.0;
   ELSE
      folder_iot.infeed_motor.speed	:= 0;
   END_IF
   //
   folder_iot.outfeed_motor.drive_error	:= iot_folderOutfeedError;   
   folder_iot.outfeed_motor.torque			:= iot_folderOutfeedTorque;
   folder_iot.outfeed_motor.current_Q		:= iot_folderOutfeedCurrent_Q;
   folder_iot.outfeed_motor.current_D		:= iot_folderOutfeedCurrent_D;
   folder_iot.outfeed_motor.current		:= 0;//FC_rResultant(folder_iot.outfeed_motor.current_Q,folder_iot.outfeed_motor.current_D);
   folder_iot.outfeed_motor.temperature	:= iot_folderOutfeedTemp;
   folder_iot.outfeed_motor.position		:= Slave[SLAVE_PLOW_OUT_INDEX].Status.ActPosition / 1000.0;
   IF (Slave[SLAVE_PLOW_OUT_INDEX].Status.ActVelocity > 2.0) THEN
      folder_iot.outfeed_motor.speed	:= Slave[SLAVE_PLOW_OUT_INDEX].Status.ActVelocity / 1000.0;
   ELSE
      folder_iot.outfeed_motor.speed	:= 0;
   END_IF
	
	(************************************************************************
	********************** BUFFER IOT DATA **********************************
	************************************************************************)
	// Statistics and unit info
   buffer_iot.enabled					:= lineController.setup.buffer.installed;
   buffer_iot.installed				:= lineController.setup.buffer.installed;
   buffer_iot.infeed_loop_level		:= 100.0 - infeedLoop.usage;	//infeedLoop.usage = 100% means completely DOWN, we need reversed	
   buffer_iot.main_loop_level			:= 100.0 - buffer.reading.usage;
   (*FC_LinTrafo(
											rIn				:= buffer.reading.input.innerLoop, 
											rIn_min			:= buffer.setting.infeedLoopBottom, 
											rIn_max			:= buffer.setting.infeedLoopTop, 
											rOut_min		:= 100.0, 
											rOut_max		:= 0.0, 
											xOutputLimited	:= TRUE);*)
   buffer_iot.speed_limit				:= buffer.reading.maximumSpeed;
   // Buffer state
   IF (buffer.reading.status.state = Status_Error) THEN
      buffer_iot.machine_state	:= iotMS_Error;
   ELSIF (buffer.reading.status.state = Status_Ready) THEN
      IF (buffer.reading.status.stopped) THEN
         IF (NOT(buffer.reading.input.coverClosed)) THEN
            buffer_iot.machine_state	:= iotMS_Setup;
         ELSE
            buffer_iot.machine_state	:= iotMS_Stopped;
         END_IF
      ELSE
         IF (buffer_iot.infeed_motor.speed > 0) THEN
            buffer_iot.machine_state	:= iotMS_Run;
         ELSE
            buffer_iot.machine_state	:= iotMS_Idle;
         END_IF
      END_IF
   ELSIF (buffer.reading.status.state = Status_Loading) THEN
      buffer_iot.machine_state	:= iotMS_Loading;
   ELSIF (buffer.reading.status.state = Status_ManualLoad) THEN
      buffer_iot.machine_state	:= iotMS_Setup;
   ELSE // if(buffer.reading.status.state == Status_NotLoaded)
      buffer_iot.machine_state	:= iotMS_NotLoaded;
   END_IF
   // Buffer motor info
   buffer_iot.infeed_motor.drive_error		:= iot_bufferInfeedError;   
   buffer_iot.infeed_motor.torque			:= iot_bufferInfeedTorque;
   buffer_iot.infeed_motor.current_Q		:= iot_bufferInfeedCurrent_Q;
   buffer_iot.infeed_motor.current_D		:= iot_bufferInfeedCurrent_D;
   buffer_iot.infeed_motor.current			:= 0;//FC_rResultant(buffer_iot.infeed_motor.current_Q,buffer_iot.infeed_motor.current_D);
   buffer_iot.infeed_motor.temperature		:= iot_bufferInfeedTemp;
   buffer_iot.infeed_motor.position		:= iot_bufferInfeedPosition / 1000.0;
   IF (buffer.reading.infeedSpeed > 0.05) THEN
      buffer_iot.infeed_motor.speed	:= buffer.reading.infeedSpeed;
   ELSE 
      buffer_iot.infeed_motor.speed	:= 0;
   END_IF
   //
   buffer_iot.outfeed_motor.drive_error	:= iot_bufferOutfeedError;
   buffer_iot.outfeed_motor.torque			:= iot_bufferOutfeedTorque;
   buffer_iot.outfeed_motor.current_Q		:= iot_bufferOutfeedCurrent_Q;
   buffer_iot.outfeed_motor.current_D		:= iot_bufferOutfeedCurrent_D;
   buffer_iot.outfeed_motor.current	      := 0;//FC_rResultant(buffer_iot.outfeed_motor.current_Q,buffer_iot.outfeed_motor.current_D);
   buffer_iot.outfeed_motor.temperature	:= iot_bufferOutfeedTemp;
   buffer_iot.outfeed_motor.position		:= Slave[SLAVE_BUF_OUT_INDEX].Status.ActPosition / 1000.0;
   IF (buffer.reading.outfeedSpeed > 0.05) THEN
      buffer_iot.outfeed_motor.speed	:= buffer.reading.outfeedSpeed;
   ELSE 
      buffer_iot.outfeed_motor.speed	:= 0;
   END_IF
	
	(************************************************************************
	********************** STACKER IOT DATA *********************************
	************************************************************************)
   stacker_runDistance						:= stackerCounter.transportTableTravelDistance;
   // Statistics and unit info
   IF (lineController.setup.stacker.installed OR lineController.setup.streamer.installed) THEN
      stacker_iot.installed	:= TRUE;
      stacker_iot.enabled		:= TRUE;
      ELSE
      stacker_iot.installed	:= FALSE;
      stacker_iot.enabled		:= FALSE;
      END_IF
      
      IF (stacker.reading.stackPages < oldStackPages) THEN // just delivered
      stacker_iot.pages_in_stack			:= oldStackPages;
   END_IF
   oldStackPages							:= stacker.reading.stackPages;
   stacker_iot.speed_limit					:= stacker.reading.maximumSpeed;
   stacker_iot.run_distance				:= LREAL_TO_UDINT(stacker_runDistance);
   stacker_iot.total_delivery_counter		:= stackerCounter.onTheFlyDeliveries + stackerCounter.stoppedDeliveries + stackerCounter.loads;
   IF oldDeliveryCnt <> stacker_iot.total_delivery_counter THEN
      IF stacker.reading.lastStackSize > 0 THEN
         stacker_iot.delivery_counter	:= stacker_iot.delivery_counter + 1;
      END_IF
   END_IF
   oldDeliveryCnt						:= stacker_iot.total_delivery_counter;
   stacker_iot.sheet_thickness				:= stacker.setting.tableSteps;
   stacker_iot.last_StackSize				:= stacker.reading.lastStackSize;
   stacker_iot.current_StackSize			:= stacker.reading.currentStackSize;
   stacker_iot.min_StackSize				:= stacker.setting.minStackSize;
   stacker_iot.max_StackSize				:= stacker.setting.maxStackSize;
   stacker_iot.offset_counter				:= stacker.reading.offsetCounter;
   stacker_iot.total_offset_counter		:= stackerCounter.offsets;
   // Stacker state
   IF (stacker.reading.status.state = Status_Error) THEN
      stacker_iot.machine_state			:= iotMS_Error;
   ELSIF (stacker.reading.status.state = Status_Ready) THEN
      IF (stacker.reading.status.stopped) THEN
         IF (NOT(stacker.reading.input.topCoverClosed)) THEN
            stacker_iot.machine_state	:= iotMS_Setup;
         ELSE
            stacker_iot.machine_state	:= iotMS_Stopped;
         END_IF
      ELSE
         IF (stacker.reading.axisStatus.transport.actualSpeed > 0.05 OR stacker.reading.axisStatus.deliver.actualSpeed > 0.025) THEN
            stacker_iot.machine_state	:= iotMS_Run;
         ELSE
            stacker_iot.machine_state	:= iotMS_Idle;
         END_IF
      END_IF
   ELSIF (stacker.reading.status.state = Status_Loading) THEN
      stacker_iot.machine_state		:= iotMS_Loading;
   ELSIF (stacker.reading.status.state = Status_ManualLoad) THEN
      stacker_iot.machine_state		:= iotMS_Setup;
   ELSE
      stacker_iot.machine_state		:= iotMS_NotLoaded;
   END_IF
   // Stacker motor info
   stacker_iot.fork_lift_motor.drive_error			:= stacker.reading.axisStatus.forkElevator.errorID;
   stacker_iot.fork_lift_motor.torque				:= stacker.reading.axisStatus.forkElevator.torque;
   stacker_iot.fork_lift_motor.current_Q			:= stacker.reading.axisStatus.forkElevator.current_Q;
   stacker_iot.fork_lift_motor.current_D			:= stacker.reading.axisStatus.forkElevator.current_D;
   stacker_iot.fork_lift_motor.current				:= 0;//FC_rResultant(stacker_iot.fork_lift_motor.current_Q,stacker_iot.fork_lift_motor.current_D);
   stacker_iot.fork_lift_motor.temperature			:= stacker.reading.axisStatus.forkElevator.temperature;
   stacker_iot.fork_lift_motor.speed				:= stacker.reading.axisStatus.forkElevator.actualSpeed;
   stacker_iot.fork_lift_motor.position			:= stacker.reading.axisStatus.forkElevator.pos/1000.0;
   //
   stacker_iot.sheet_transport_motor.drive_error	:= stacker.reading.axisStatus.transport.errorID;
   stacker_iot.sheet_transport_motor.torque		:= stacker.reading.axisStatus.transport.torque;
   stacker_iot.sheet_transport_motor.current_Q		:= stacker.reading.axisStatus.transport.current_Q;
   stacker_iot.sheet_transport_motor.current_D		:= stacker.reading.axisStatus.transport.current_D;
   stacker_iot.sheet_transport_motor.current		:= 0;//FC_rResultant(stacker_iot.sheet_transport_motor.current_Q,stacker_iot.sheet_transport_motor.current_D);
   stacker_iot.sheet_transport_motor.temperature	:= stacker.reading.axisStatus.transport.temperature;
   stacker_iot.sheet_transport_motor.speed			:= stacker.reading.axisStatus.transport.actualSpeed;
   stacker_iot.sheet_transport_motor.position		:= stacker.reading.axisStatus.transport.pos/1000.0;
   //
   stacker_iot.fork_out_motor.drive_error			:= stacker.reading.axisStatus.forkPush.errorID;
   stacker_iot.fork_out_motor.torque				:= stacker.reading.axisStatus.forkPush.torque;
   stacker_iot.fork_out_motor.current_Q			:= stacker.reading.axisStatus.forkPush.current_Q;
   stacker_iot.fork_out_motor.current_D			:= stacker.reading.axisStatus.forkPush.current_D;
   stacker_iot.fork_out_motor.current				:= 0;//FC_rResultant(stacker_iot.fork_out_motor.current_Q,stacker_iot.fork_out_motor.current_D);
   stacker_iot.fork_out_motor.temperature			:= stacker.reading.axisStatus.forkPush.temperature;
   stacker_iot.fork_out_motor.speed				:= stacker.reading.axisStatus.forkPush.actualSpeed;
   stacker_iot.fork_out_motor.position				:= stacker.reading.axisStatus.forkPush.pos/1000.0;
   //
   stacker_iot.stack_elevator_motor.drive_error	:= stacker.reading.axisStatus.stackerElevator.errorID;
   stacker_iot.stack_elevator_motor.torque			:= stacker.reading.axisStatus.stackerElevator.torque;
   stacker_iot.stack_elevator_motor.current_Q		:= stacker.reading.axisStatus.stackerElevator.current_Q;
   stacker_iot.stack_elevator_motor.current_D		:= stacker.reading.axisStatus.stackerElevator.current_D;
   stacker_iot.stack_elevator_motor.current		:= 0;//FC_rResultant(stacker_iot.stack_elevator_motor.current_Q,stacker_iot.stack_elevator_motor.current_D);
   stacker_iot.stack_elevator_motor.temperature	:= stacker.reading.axisStatus.stackerElevator.temperature;
   stacker_iot.stack_elevator_motor.speed			:= stacker.reading.axisStatus.stackerElevator.actualSpeed;
   stacker_iot.stack_elevator_motor.position		:= stacker.reading.axisStatus.stackerElevator.pos/1000.0;
   //
   stacker_iot.stack_transport_motor.drive_error	:= stacker.reading.axisStatus.deliver.errorID;
   stacker_iot.stack_transport_motor.torque		:= stacker.reading.axisStatus.deliver.torque;
   stacker_iot.stack_transport_motor.current_Q		:= stacker.reading.axisStatus.deliver.current_Q;
   stacker_iot.stack_transport_motor.current_D		:= stacker.reading.axisStatus.deliver.current_D;
   stacker_iot.stack_transport_motor.current		:= 0;//FC_rResultant(stacker_iot.stack_transport_motor.current_Q,stacker_iot.stack_transport_motor.current_D);
   stacker_iot.stack_transport_motor.temperature	:= stacker.reading.axisStatus.deliver.temperature;
   stacker_iot.stack_transport_motor.speed			:= stacker.reading.axisStatus.deliver.actualSpeed;
   stacker_iot.stack_transport_motor.position		:= stacker.reading.axisStatus.deliver.pos/1000.0;
   //
   stacker_iot.spaghetti_belt_motor.drive_error	:= stacker.reading.axisStatus.spaghettiBelt.errorID;
   stacker_iot.spaghetti_belt_motor.torque			:= stacker.reading.axisStatus.spaghettiBelt.torque;
   stacker_iot.spaghetti_belt_motor.current_Q		:= stacker.reading.axisStatus.spaghettiBelt.current_Q;
   stacker_iot.spaghetti_belt_motor.current_D		:= stacker.reading.axisStatus.spaghettiBelt.current_D;
   stacker_iot.spaghetti_belt_motor.current		:= 0;//FC_rResultant(stacker_iot.spaghetti_belt_motor.current_Q,stacker_iot.spaghetti_belt_motor.current_D);
   stacker_iot.spaghetti_belt_motor.temperature	:= stacker.reading.axisStatus.spaghettiBelt.temperature;
   stacker_iot.spaghetti_belt_motor.speed			:= stacker.reading.axisStatus.spaghettiBelt.actualSpeed;
   stacker_iot.spaghetti_belt_motor.position		:= stacker.reading.axisStatus.spaghettiBelt.pos/1000.0;
   //
   stacker_iot.stop_plate_motor.drive_error		:= stacker.reading.axisStatus.endStop.errorID;
   stacker_iot.stop_plate_motor.torque				:= stacker.reading.axisStatus.endStop.torque;
   stacker_iot.stop_plate_motor.current_Q			:= stacker.reading.axisStatus.endStop.current_Q;
   stacker_iot.stop_plate_motor.current_D			:= stacker.reading.axisStatus.endStop.current_D;
   stacker_iot.stop_plate_motor.current			:= 0;//FC_rResultant(stacker_iot.stop_plate_motor.current_Q,stacker_iot.stop_plate_motor.current_D);
   stacker_iot.stop_plate_motor.temperature		:= stacker.reading.axisStatus.endStop.temperature;
   stacker_iot.stop_plate_motor.speed				:= stacker.reading.axisStatus.endStop.actualSpeed;
   stacker_iot.stop_plate_motor.position			:= stacker.reading.axisStatus.endStop.pos/1000.0;
	
	(************************************************************************
	********************** T50 BYPASS - IOT DATA ****************************
	************************************************************************)
	// T50-Bypass IOT data are copied directly into "bypass_iot" data structure by AsANSL communication protocol
	
	(************************************************************************
	********************** LINE IOT DATA ************************************
	************************************************************************)
   line_iot.version			:= lineController.reading.version;
   IF (machineError.active) THEN
      line_iot.error			:= machineError.errType;
      line_iot.errorSubType	:= machineError.errSubType;
   ELSE
      line_iot.error			:= 0;
      line_iot.errorSubType	:= 0;
   END_IF
	
   IF (
      (cutter_iot.machine_state = iotMS_Error)													OR
      (lineController.setup.buffer.installed AND buffer_iot.machine_state = iotMS_Error)			OR
      (stacker_iot.installed AND stacker_iot.machine_state = iotMS_Error)		OR
      (lineController.setup.folder.installed AND folder_iot.machine_state = iotMS_Error)			OR
      (lineController.setup.merger.installed AND merger_iot.machine_state = iotMS_Error)			OR
      (lineController.setup.transportTable.installed AND bypass_iot.machine_state = iotMS_Error)
      ) THEN
      line_iot.machine_state := iotMS_Error;
   ELSIF (
      (cutter_iot.machine_state = iotMS_Setup)													OR
      (lineController.setup.buffer.installed AND buffer_iot.machine_state = iotMS_Setup)			OR
      (stacker_iot.installed AND stacker_iot.machine_state = iotMS_Setup)		OR
      (lineController.setup.folder.installed AND folder_iot.machine_state = iotMS_Setup)			OR
      (lineController.setup.merger.installed AND merger_iot.machine_state = iotMS_Setup)			OR
      (lineController.setup.transportTable.installed AND bypass_iot.machine_state = iotMS_Setup)
      ) THEN
      line_iot.machine_state := iotMS_Setup;
		
   ELSIF (
      (cutter_iot.machine_state = iotMS_Loading)													OR
      (lineController.setup.buffer.installed AND buffer_iot.machine_state = iotMS_Loading)		OR
      (stacker_iot.installed AND stacker_iot.machine_state = iotMS_Loading)		OR
      (lineController.setup.folder.installed AND folder_iot.machine_state = iotMS_Loading)		OR
      (lineController.setup.merger.installed AND merger_iot.machine_state = iotMS_Loading)		OR
      (lineController.setup.transportTable.installed AND bypass_iot.machine_state = iotMS_Loading)
      ) THEN
      line_iot.machine_state := iotMS_Loading;
	
   ELSIF (
      (cutter_iot.machine_state = iotMS_NotLoaded)												OR
      (lineController.setup.buffer.installed AND buffer_iot.machine_state = iotMS_NotLoaded)		OR
      (stacker_iot.installed AND stacker_iot.machine_state = iotMS_NotLoaded)	OR
      (lineController.setup.folder.installed AND folder_iot.machine_state = iotMS_NotLoaded)		OR
      (lineController.setup.merger.installed AND merger_iot.machine_state = iotMS_NotLoaded)		OR
      (lineController.setup.transportTable.installed AND bypass_iot.machine_state = iotMS_NotLoaded)
      ) THEN
      line_iot.machine_state := iotMS_NotLoaded;
	
   ELSIF (
      (cutter_iot.machine_state = iotMS_Stopped)													OR
      (lineController.setup.buffer.installed AND buffer_iot.machine_state = iotMS_Stopped)		OR
      (stacker_iot.installed AND stacker_iot.machine_state = iotMS_Stopped)		OR
      (lineController.setup.folder.installed AND folder_iot.machine_state = iotMS_Stopped)		OR
      (lineController.setup.merger.installed AND merger_iot.machine_state = iotMS_Stopped)		OR
      (lineController.setup.transportTable.installed AND bypass_iot.machine_state = iotMS_Stopped)
      ) THEN
      line_iot.machine_state := iotMS_Stopped;
		
   ELSIF (cutter_iot.machine_state = iotMS_Run) OR (
      (cutter_iot.machine_state = iotMS_Idle) AND (
      (lineController.setup.buffer.installed AND buffer_iot.machine_state = iotMS_Run)		OR
      (stacker_iot.installed AND stacker_iot.machine_state = iotMS_Run)		OR
      (lineController.setup.folder.installed AND folder_iot.machine_state = iotMS_Run)		OR
      (lineController.setup.merger.installed AND merger_iot.machine_state = iotMS_Run)		OR
      (lineController.setup.transportTable.installed AND bypass_iot.machine_state = iotMS_Run)
      )
      ) THEN
      line_iot.machine_state := iotMS_Run;
   END_IF
   
   folder_iot.foldingType := folder.setting.foldingType;
   line_iot.recipe := configFiles.parameter.number;
   unwinder_iot.rollDiameter := unwinder.reading.rollDiameter;

END_PROGRAM


TYPE
	S_Line_iot : 	STRUCT 
		errorSubType : UDINT;
		error : UDINT;
		machine_state : E_MacState_iot;
		recipe : INT;
		version : UINT;
	END_STRUCT;
	S_Buffer_iot : 	STRUCT 
		installed : BOOL;
		enabled : BOOL;
		infeed_loop_level : REAL;
		main_loop_level : REAL;
		speed_limit : REAL;
		machine_state : E_MacState_iot;
		infeed_motor : S_Motor_iot;
		outfeed_motor : S_Motor_iot;
	END_STRUCT;
	S_Folder_iot : 	STRUCT 
		installed : BOOL;
		enabled : BOOL;
		folder_tension : REAL;
		foldingType : DINT;
		speed_limit : REAL;
		machine_state : E_MacState_iot;
		infeed_motor : S_Motor_iot;
		outfeed_motor : S_Motor_iot;
	END_STRUCT;
	S_Cutter_iot : 	STRUCT 
		installed : BOOL;
		enabled : BOOL;
		knife2_installed : BOOL;
		knife2_enabled : BOOL;
		cut_length : REAL;
		strip_length : REAL;
		speed_limit : REAL;
		cut1_counter : UDINT;
		cut2_counter : UDINT;
		valid_marks_counter : UDINT;
		missed_marks_counter : UDINT;
		processed_sheets : UDINT;
		rejected_sheets : UDINT;
		set_counter : UDINT;
		run_distance : UDINT;
		blank_distance : UDINT;
		rejected_distance : UDINT;
		machine_state : E_MacState_iot;
		infeed_motor : S_Motor_iot;
		feed_support_motor : S_Motor_iot;
		reject_motor : S_Motor_iot;
		knife1_motor : S_Motor_iot;
		knife2_motor : S_Motor_iot;
	END_STRUCT;
	S_Stacker_iot : 	STRUCT 
		installed : BOOL;
		enabled : BOOL;
		speed_limit : REAL;
		run_distance : UDINT;
		delivery_counter : UDINT;
		total_delivery_counter : UDINT;
		offset_counter : UDINT;
		total_offset_counter : UDINT;
		pages_in_stack : UDINT;
		sheet_thickness : REAL;
		last_StackSize : REAL;
		current_StackSize : REAL;
		min_StackSize : REAL;
		max_StackSize : REAL;
		machine_state : E_MacState_iot;
		stack_elevator_motor : S_Motor_iot;
		stack_transport_motor : S_Motor_iot;
		spaghetti_belt_motor : S_Motor_iot;
		fork_lift_motor : S_Motor_iot;
		fork_out_motor : S_Motor_iot;
		sheet_transport_motor : S_Motor_iot;
		stop_plate_motor : S_Motor_iot;
	END_STRUCT;
	S_Bypass_iot : 	STRUCT 
		installed : BOOL;
		enabled : BOOL;
		total_sheets_in_cnt : UDINT;
		total_sheets_out_cnt : UDINT;
		total_stacks_cnt : UDINT;
		actual_sheets_bypass : UINT;
		machine_state : E_MacState_iot;
		bypass_selected : BOOL;
		conveyor_selected : BOOL;
		conveyor_motor : S_Motor_iot;
		bypass_motor : S_Motor_iot;
	END_STRUCT;
	S_Unwinder : 	STRUCT 
		installed : BOOL;
		rollDiameter : REAL; (*mm*)
		enabled : BOOL;
		machine_state : E_MacState_iot;
	END_STRUCT;
	S_Merger_iot : 	STRUCT 
		installed : BOOL;
		enabled : BOOL;
		puncher_installed : BOOL;
		puncher_enabled : BOOL;
		speed_limit : REAL;
		punch_counter : UDINT;
		run_distance : UDINT;
		machine_state : E_MacState_iot;
		infeed_motor : S_Motor_iot;
		punch_motor : S_Motor_iot;
	END_STRUCT;
END_TYPE

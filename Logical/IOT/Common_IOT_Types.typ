
TYPE
	S_Motor_iot : 	STRUCT 
		drive_error			: UINT;
		torque				: REAL;
		current				: REAL;
		current_Q			: REAL;
		current_D			: REAL;
		temperature			: REAL;
		speed				: REAL;
		position			: REAL;
	END_STRUCT;
	E_MacState_iot : (
		iotMS_NotLoaded		:= 0,
		iotMS_Loading		:= 1,
		iotMS_Idle			:= 2,
		iotMS_Error			:= 3,
		iotMS_Run			:= 4,
		iotMS_Stopped		:= 5,
		iotMS_Setup			:= 6
	);
END_TYPE
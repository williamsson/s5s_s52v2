
FUNCTION FC_LinTrafo : REAL
	VAR_INPUT
		rIn : REAL; (* Input *)
		rIn_min : REAL; (* Minimum input value *)
		rIn_max : REAL; (* Maximum input value *)
		rOut_min : REAL; (* Minimum output value *)
		rOut_max : REAL; (* Maximum output value *)
		xOutputLimited : BOOL; (* If TRUE, output value is limited between rOut_min and rOut_max *)
	END_VAR
	VAR
		rM : REAL;
		rQ : REAL;
		rValret : REAL;
	END_VAR
END_FUNCTION


FUNCTION FC_rResultant
	FC_rResultant	:= SQRT(EXPT(rDirect,2) + EXPT(rQuadrature,2));
END_FUNCTION

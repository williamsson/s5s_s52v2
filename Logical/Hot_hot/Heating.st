
PROGRAM _INIT
   heatLeft := 0;
   heatRight := 0;
   heatFront := 0;
   heatRear := 0;
   CutTempLeft := 0;
   CutTempRear := 0;
   CutTempFront := 0;
   CutTempRight := 0;
   frame.ready := FALSE;
   LTowerOut[0] := 0;
	LTowerOut[1] := 0;
	LTowerOut[2] := 0;
	LTowerOut[3] := 2;
	LTowerOut[4] := 1;
	LTowerOut[5] := 0;
	LTowerOut[6] := 1;
   LTowerOut[7] := 0;
   timeCount := 0;
	 
END_PROGRAM

PROGRAM _CYCLIC
	//GUI_info.temp_left := frame.temp_1_left / 10.0;
	cutter.reading.tempLeft := frame.temp_1_left / 10.0; 
	cutter.reading.tempRear := frame.temp_2_rear / 10.0; 
	cutter.reading.tempFront := frame.temp_3_front / 10.0; 
	cutter.reading.tempRight := frame.temp_4_right / 10.0; 
	
	CutTempLeft := frame.temp_1_left / 10;
	CutTempRear := frame.temp_2_rear / 10;
	CutTempFront := frame.temp_3_front / 10;
	CutTempRight := frame.temp_4_right / 10;

	//rampStartLeft := 15.0;
	//rampStartRight := 15.0;
	//rampStartFront := 22.0;
	//rampStartRear := 22.0;

	//rampStopLeft := 49.0;
	//rampStopRight := 49.0;
	//rampStopFront := 52.0;
	//rampStopRear := 52.0;

	rampStartLeft := 5.0;
	rampStartRight := 5.0;
	rampStartFront := 22.0;
	rampStartRear := 15.0;

	rampStopLeft := 49.0;
	rampStopRight := 50.0;
	rampStopFront := 50.0;
	rampStopRear := 51.0;
   
	IF cutter.reading.tempLeft < rampStartLeft THEN
		heatLeft := 100;
	ELSIF cutter.reading.tempLeft > rampStopLeft THEN
		heatLeft := 0;
	ELSE
		heatLeft := 100 - REAL_TO_USINT((cutter.reading.tempLeft - rampStartLeft) / ((rampStopLeft - rampStartLeft) / 100.0));
	END_IF 

	IF cutter.reading.tempRight < rampStartRight THEN
		heatRight := 100;
	ELSIF cutter.reading.tempRight > rampStopRight THEN
		heatRight := 0;
	ELSE
		heatRight := 100 - REAL_TO_USINT((cutter.reading.tempRight - rampStartRight) / ((rampStopRight - rampStartRight) / 100.0));
	END_IF 
   
	IF cutter.reading.tempFront < rampStartFront THEN
		heatFront := 100;
	ELSIF cutter.reading.tempFront > rampStopFront THEN
		heatFront := 0;
	ELSE
		heatFront := 100 - REAL_TO_USINT((cutter.reading.tempFront - rampStartFront) / ((rampStopFront - rampStartFront) / 100.0));
	END_IF 
   
	IF cutter.reading.tempRear < rampStartRear THEN
		heatRear := 100;
	ELSIF cutter.reading.tempRear > rampStopRear THEN
		heatRear := 0;
	ELSE
		heatRear := 100 - REAL_TO_USINT((cutter.reading.tempRear - rampStartRear) / ((rampStopRear - rampStartRear) / 100.0));
	END_IF 
   
	IF heatLeft > 40 THEN
		heatLeft := 40;
	END_IF
	IF heatRight > 40 THEN
		heatRight := 40;
	END_IF
	IF heatFront > 40 THEN
		heatFront := 40;
	END_IF
	IF heatRear > 40 THEN
		heatRear := 40;
	END_IF
      
	timeCount := timeCount + 1;
	IF timeCount = 100 THEN
		timeCount := 0;
	END_IF
   
	IF heatLeft = 0 THEN
		DO_heaterLeft := FALSE;
	ELSE
		DO_heaterLeft := (timeCount <= heatLeft);
	END_IF
         
	IF heatRight = 0 THEN
		DO_heaterRight := FALSE;
	ELSE
		DO_heaterRight := (timeCount <= heatRight);
	END_IF

	IF heatFront = 0 THEN
		DO_heaterFront := FALSE;
	ELSE
		DO_heaterFront := (timeCount <= heatFront);
	END_IF

	IF heatRear = 0 THEN
		DO_heaterRear := FALSE;
	ELSE
		DO_heaterRear := (timeCount <= heatRear);
	END_IF

(*
   IF measureStartTemp = FALSE THEN
		IF CutTempLeft > (frameTemp-3) AND CutTempRight > (frameTemp-3) AND CutTempFront > (frameTemp-3) AND CutTempRear > (frameTemp-3) THEN
			tempState := RegulatingState; // go directly to regulating state
		END_IF
		measureStartTemp := TRUE;
	END_IF

	AvgFrameTemperature := (INT_TO_REAL(frame.temp_4_right) + INT_TO_REAL(frame.temp_1_left) + INT_TO_REAL(frame.temp_3_front) + INT_TO_REAL(frame.temp_2_rear)) / 40.0;

	IF tempState = HeatUpState THEN // over heat
		frame.ready := FALSE;
      DO_heaterLeft := CutTempLeft < (frameTemp + 10);
      DO_heaterRight := CutTempRight < (frameTemp + 10);
      DO_heaterFront := CutTempFront < (frameTemp + 10);
      DO_heaterRear := CutTempRear < (frameTemp + 10);
      IF CutTempLeft > (frameTemp + 8) AND CutTempRight > (frameTemp + 8) AND CutTempFront > (frameTemp + 8) AND CutTempRear > (frameTemp + 8) THEN
         tempState := CoolDownState;
		END_IF
	ELSIF tempState = CoolDownState THEN // cool down
		frame.ready := FALSE;
      DO_heaterLeft := CutTempLeft < frameTemp;
      DO_heaterRight := CutTempRight < frameTemp;
      DO_heaterFront := CutTempFront < frameTemp;
      DO_heaterRear := CutTempRear < frameTemp;
      IF CutTempLeft < (frameTemp + 2) AND CutTempRight < (frameTemp + 2) AND CutTempFront < (frameTemp + 2) AND CutTempRear < (frameTemp + 2) THEN
			tempState := RegulatingState;
		END_IF
	ELSE //RegulatingState
		frame.ready := TRUE;
		DO_heaterLeft := CutTempLeft < frameTemp;
		DO_heaterRight := CutTempRight < frameTemp;
		DO_heaterFront := CutTempFront < frameTemp;
		DO_heaterRear := CutTempRear < frameTemp;
		IF CutTempLeft < (frameTemp - 3) OR CutTempRight < (frameTemp - 3) OR CutTempFront < (frameTemp - 3) OR CutTempRear < (frameTemp - 3) THEN //too cold
			frame.ready := FALSE;
		END_IF
		IF CutTempLeft > (frameTemp + 5) OR CutTempRight > (frameTemp + 5) OR CutTempFront > (frameTemp + 5) OR CutTempRear > (frameTemp + 5) THEN //too warm
			frame.ready := FALSE;
		END_IF
		IF CutTempLeft < (frameTemp - 10) OR CutTempRight < (frameTemp - 10) OR CutTempFront < (frameTemp - 10) OR CutTempRear < (frameTemp - 10) THEN //do a heat cycle
			tempState := HeatUpState;
		END_IF
	END_IF
*)	
	IF frame.forceOff = TRUE THEN
		DO_heaterLeft := FALSE;
		DO_heaterRight := FALSE;
		DO_heaterFront := FALSE;
		DO_heaterRear := FALSE;
	END_IF
	

	IF cutter.setting.forceOutputEnable.heaterRight = TRUE THEN
		physical.cutter.digital.out7 := cutter.reading.output.heaterRight := cutter.setting.forceOutputValue.heaterRight;
	ELSE
		physical.cutter.digital.out7 := cutter.reading.output.heaterRight := DO_heaterRight;
	END_IF
	
	IF cutter.setting.forceOutputEnable.heaterRear = TRUE THEN
		physical.cutter.digital.out9 := cutter.reading.output.heaterRear := cutter.setting.forceOutputValue.heaterRear;
	ELSE
		physical.cutter.digital.out9 := cutter.reading.output.heaterRear := DO_heaterRear;
	END_IF
	
	IF cutter.setting.forceOutputEnable.heaterFront = TRUE THEN
		physical.cutter.digital.out11 := cutter.reading.output.heaterFront := cutter.setting.forceOutputValue.heaterFront;
	ELSE
		physical.cutter.digital.out11 := cutter.reading.output.heaterFront := DO_heaterFront;
	END_IF
	
	IF cutter.setting.forceOutputEnable.heaterLeft = TRUE THEN
		physical.cutter.digital.out5 := cutter.reading.output.heaterLeft := cutter.setting.forceOutputValue.heaterLeft;
	ELSE
		physical.cutter.digital.out5 := cutter.reading.output.heaterLeft := DO_heaterLeft;
	END_IF
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM


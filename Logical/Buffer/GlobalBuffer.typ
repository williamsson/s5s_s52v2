
TYPE
	buffer_typ : 	STRUCT 
		setting : buffer_setting_typ;
		command : buffer_ext_command_typ;
		reading : buffer_reading_typ;
	END_STRUCT;
	command_typ : 	STRUCT 
		stop : BOOL := FALSE;
		manualLoad : BOOL := FALSE;
		load : BOOL := FALSE;
		errorAck : BOOL := FALSE;
	END_STRUCT;
	tensionFactor_typ : 	STRUCT 
		loopAmount : REAL := 0.15; (*0-14 (m) * factor = pressure N*)
		infeedSpeed : REAL := 0; (*0-5 (m/s) * factor = pressure N*)
		outfeedSpeed : REAL := 0; (*0-5 (m/s) * factor = pressure N*)
		loopSpeed : REAL := 0.075; (*-5.0 - 5.0 (m/s) * factor = pressure N*)
		outfeedAcceleration : REAL := -0.06; (*0-3.0 (m/s^2) * factor = pressure N*)
		InfeedAcceleration : REAL := 0.09; (*0-3.0 (m/s^2) * factor = pressure N*)
	END_STRUCT;
	tensionFixed_typ : 	STRUCT 
		stopped : REAL := 30; (*tension when stopped (infeed = 0 outfeed = 0)*)
		running : REAL := 12; (*tension offset when running*)
	END_STRUCT;
	monitorTension_Typ : 	STRUCT 
		InfeedAcc : REAL;
		loopSpeed : REAL;
		outfeedAcc : REAL;
	END_STRUCT;
	infeedOperation_typ : 
		(
		moment,
		fixSpeed,
		loopControl
		);
	innerLoop_typ : 
		(
		manual,
		automatic
		);
	bufferLastErrors_typ : 	STRUCT 
		pointer : USINT;
		error : ARRAY[0..9]OF bufferError_typ;
	END_STRUCT;
	infeedLoop_typ : 	STRUCT 
		usage : REAL := 50;
		maxInputSpeed : REAL := 3.1;
		iCompensation : REAL := 0.1;
		dCompensation : REAL := 0.5;
	END_STRUCT;
	bufferError_typ : 	STRUCT 
		text : STRING[80];
		subTyp : UINT;
		timeStamp : UDINT;
		typ : USINT;
	END_STRUCT;
	buffer_reading_typ : 	STRUCT 
		usage : REAL := 0; (*%*)
		accumulated : REAL := 0; (*m (amount of paper in buffer)*)
		maximumSpeed : REAL := 0; (*m/s (speed limit)*)
		outfeedSpeed : REAL; (*m/s*)
		autoTension : REAL; (*N*)
		infeedSpeed_fpm : REAL; (*fpm*)
		infeedSpeed_mm : REAL; (*m/min*)
		infeedSpeed : REAL; (*m/s*)
		btn2Func : USINT; (*0 nothing. Feed In. Manual load*)
		btn3Func : USINT; (*0 nothing, Feed Out. Unload*)
		startUpSeq : USINT; (*0->15*)
		inDecurl : BOOL; (*TRUE = buffer in decurl mode (not ready)*)
		infeedAcc : BOOL;
		infeedDec : BOOL;
		inBottom : BOOL; (*inner loop in bottom*)
		output : physical_buffer_outputs; (*current state of output*)
		input : physical_buffer_inputs;
		noPaper : BOOL; (*buffer empty*)
		fillBufferByReady : BOOL; (*allow NPRO, to fill buffer*)
		lastErrors : bufferLastErrors_typ;
		status : machineStatus_typ;
		button1 : colorOfButton;
		button2 : colorOfButton;
		button3 : colorOfButton;
		button4 : colorOfButton;
		button5 : colorOfButton;
		calculatedAccumulated : LREAL; (*m, in-out*)
		totalInSinceLoad_Dev : LREAL; (*debug variable (shows the buffer.reading.usage without compensation)*)
		totalOutSinceLoad_Dev : LREAL; (*debug variable (shows the buffer.reading.usage without compensation)*)
		usage_Dev : REAL; (*debug variable (shows the buffer.reading.usage without compensation)*)
		maxAccumulated : REAL := 14.42;
		minutesLeft : REAL;
		fixInfeedRunning : BOOL;
	END_STRUCT;
	buffer_setting_typ : 	STRUCT 
		infeedOperation : infeedOperation_typ := loopControl;
		innerLoopOperation : innerLoop_typ := manual;
		innerLoopManualTension : REAL := 12; (*N*)
		innerLoopAutoConstantFactor : REAL := 25; (*N*)
		innerLoopAutoSpeedDiffFactor : REAL := 10; (*N * abs speed diff (m/s)*)
		maxInfeedSpeed : REAL; (*m/s*)
		innerLoopBottom : DINT;
		innerLoopTop : DINT;
		stopLevel : REAL := 15; (*m*)
		slowLevel : REAL := 10; (*m*)
		pauseLevel : REAL := 12; (*m*)
		decurlTime : DINT := 30; (*s, time in standstill to decurl.*)
		forceOutputEnable : physical_buffer_outputs;
		disableDecurl : BOOL := TRUE;
		forceOutputValue : physical_buffer_outputs;
		loopLevelCompensation : REAL := 0; (*compensation for the loop position (reported amount, reading.usage) is faked to show more than actual depending on infeedSpeed and loop level*)
		infeedLoopTop : DINT := 450;
		infeedLoopBottom : DINT := 32000;
		innerLoopTensionContTyp : DINT := 0;
		infeedOperationSave : DINT := 0;
	END_STRUCT;
	buffer_ext_command_typ : 	STRUCT 
		decurl : BOOL; (*enter decurl mode*)
		preLoadTension : BOOL; (*increase the tension in the inner loop momentarily*)
		releaseDecurl : BOOL; (*exit decurl mode (true to decurl) buffer.readings.inDecurl -> FALSE = done (ready)*)
		pressButton1 : BOOL;
		pressButton2 : BOOL;
		pressButton3 : BOOL;
		pressButton4 : BOOL;
		pressButton5 : BOOL;
		resetInnerLoopFault : BOOL := FALSE; (*do this when feeding out paper. if there is a lot of paper on the floor, you don't want to be desturbed by alarms when feeding it out.*)
		coverFault : BOOL;
		load : BOOL;
	END_STRUCT;
	physical_buffer_inputs : 	STRUCT 
		eStop : BOOL;
		coverClosed : BOOL;
		pressureGuard : BOOL;
		outfeedNipRoller : BOOL;
		infeedPaperEnd : BOOL;
		infeedNipRoller : BOOL;
		infeedLoop : INT; (*analog*)
		innerLoop : INT; (*analog*)
		prototypeVersion : BOOL;
	END_STRUCT;
	physical_buffer_outputs : 	STRUCT 
		loopValve1_2 : BOOL;
		loopValve2 : BOOL;
		loopLockValve : BOOL;
		loopValve1_1 : BOOL;
		setValvePressure : INT;
	END_STRUCT;
END_TYPE


PROGRAM _INIT
   //AxisBufferInfeedObj := ADR(gAxBuf1);
   //AxisBufferInfeedObj
   AxisControlCounter := 0;
   writeOn:=FALSE;
   startAcpBasic := FALSE;
END_PROGRAM

PROGRAM _CYCLIC
   //
  (* AxisBufferInfeedObj := ADR(gABufIn);
   startAcpBasic := TRUE;
   RETURN;
   //
   IF node[23].station[0] = TRUE THEN
      AxisBufferInfeedObj := ADR(gABufIn);
      startAcpBasic := TRUE;
   ELSIF node[21].station[0] = TRUE THEN *)
      AxisBufferInfeedObj := ADR(gABufIn); //gAxBuf1 //Lsi Demo line (.90)
      //AxisBufferInfeedObj := ADR(gAxBuf1); //gABufIn //h�kan lina (.98)
      startAcpBasic := TRUE;
      AxisControlCounter := AxisControlCounter + 1;
      IF (buffer.reading.input.prototypeVersion) THEN
         directionParameter := ncINVERSE;
      ELSE
         directionParameter := ncSTANDARD;
      END_IF
      writePar.Axis := ADR(gAxBuf1);
      writePar.ParID := 96; 
      writePar.DataAddress := ADR(directionParameter);
      writePar.DataType := ncPAR_TYP_USINT;
   
      
      IF writeOn = TRUE THEN
        IF writePar.Done = TRUE THEN
            writePar.Execute := TRUE;
        END_IF
      END_IF
         
      IF safety.reading.initialized <> wasSafetyInitialized THEN
         writePar.Execute := TRUE;
         writeOn:= TRUE;
      END_IF
         
         
      IF (buffer.reading.startUpSeq = 15) THEN
         IF (wasBufferStartUp = 14) THEN
            writePar.Execute := TRUE;
            writeOn:= TRUE;
        END_IF
      END_IF
   
      wasBufferStartUp := buffer.reading.startUpSeq;
      wasSafetyInitialized := safety.reading.initialized; 
         writePar();
   //END_IF
   
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
   
	 
END_PROGRAM



#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	RTInfo(&rt_bufferInfo);
	state = init;
	/*-------------SPEEDAXIS--------------*/
	bufInfeed.acceleration 		= 1.0;//2.5;
	bufInfeed.deceleration 		= 2.0;//6.1;
	bufInfeed.speed            = 0.0;
	bufInfeed.lastSpeed        = 0.0;
	bufInfeed.maxSpeed 			= 5.1;
	bufInfeed.minSpeed			= 0.0;
	bufInfeed.homed				= 0;
	bufInfeed.error				= 0;
	bufInfeed.actualPosition  	= 0.0;
	bufInfeed.direction			= forward;
	homeInfeedState				= 0;
	
	bInfeed.Command.ErrorAcknowledge = 1;
	buffer.setting.infeedOperation = loopControl;
   //buffer.setting.infeedOperation = fixSpeed;
   lineController.setup.unwinder.installed = 1;
   
   homeInfeedState = 0;
	
	infeedStopAmount = 0;
	buttonStopped = 0;
	buttonLedTest = 0;
   
}

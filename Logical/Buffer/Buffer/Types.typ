
TYPE
	dir : 
		(
		forward,
		backward
		);
	speedAxis : 	STRUCT 
		lastSpeed : REAL;
		actualSpeed : REAL; (*m/s*)
		error : BOOL;
		speed : REAL; (*m/s*)
		deceleration : REAL; (*m/s�*)
		acceleration : REAL; (*m/s�*)
		maxSpeed : REAL; (*m/s*)
		homed : BOOL;
		direction : dir;
		actualPosition : REAL;
		minSpeed : REAL; (*m/s*)
	END_STRUCT;
	mainState_enum : 
		(
		load,
		running,
		error,
		manualLoad,
		init
		);
END_TYPE


#include <bur/plctypes.h>
#include <stdlib.h>
#include <string.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#define ON                          1
#define OFF                         0
#define SECONDS_UNIT                0.0096//(REAL)rt_bufferInfo.cycle_time/1000000.0
#define TRUE                        1
#define FALSE                       0
#define LOOP_IS_UP			         FALSE
#define LOOP_IS_DOWN                TRUE
#define LOCK_OFF                    FALSE
#define LOCK_ON			            TRUE
#define LOOP_IN_TOP                 32390
#define LOOP_IN_BOTTOM              4563
#define RUNNING_TOP_POS_OFFSET      2000//	150
#define LOCK_OFFSET                 60
#define MAXIMUM_PAPER_AMOUNT        14.42
#define DECURL_DISTANCE             150

//innerloop
#define FREE                        0,0
#define UP                          1,0
#define DOWN                        2,0
#define TENSION                     3
//innerloop return value
#define DONE                        FALSE
#define BUSY                        TRUE

#define INFEED_PRESSURE_WHEEL       1
#define OUTFEED_PRESSURE_WHEEL      2
#define NO_PRESSURE                 3
#define INFEED_AXIS_ERROR           4
#define INFEED_OVERSPEED            5
#define INFEED_PAPER_END            6
#define B50_E_STOP                  7
#define OVERFULL                    8
#define LOOP_UP_FAILURE             9
#define LOOP_DOWN_FAILURE           10
#define INNER_LOOP_FAULT            11
#define COVER_OPEN                  12
#define NO_PAPER_MOVEMENT           13

#define START                       1
#define EXIT                        2
#define EXECUTE                     0
#define FORCE                       1
#define AUTO                        2
#define NEVER                       0

#define COLOR_OFF                   0,0,0,0
#define COLOR_RED                   0,1,0,0
#define COLOR_YELLOW                0,0,1,0
#define COLOR_GREEN                 1,0,0,0
#define COLOR_ORANGE                0,1,1,0
//only button 1,2
#define COLOR_WHITE                 0,0,0,1
#define COLOR_PINK                  0,1,0,1
//only button 3
#define COLOR_BLUE                  0,0,0,1
#define COLOR_MAGENTA               0,1,0,1
#define LIT                         1,1,1,1

void quarterSecondCycle (void);
void getInfeed (void);
void buttonCommand (void);
void setInfeed (void);
BOOL autoLoad(void);
void infeed (void);
BOOL innerLoop(UINT action, REAL tension);
void addError(USINT err, UINT subType);
void goUp(void);
void goUpSoft(INT p);
void goDown(void);
void unlock(void);
BOOL standStill(void);
void releasePreasure(INT p);
void lock(void);
REAL calculatePaperAmount (void);
void pushDown(void);
void releasePushDown(void);
REAL calculateInnerTension(void);
void setPressure(REAL tension);
BOOL infeedPressureWheel(void);
BOOL outfeedPressureWheel(void);
BOOL infeedPaperEnd(void);
void resetCommand(void);
void rainbowTwinkle(void);
BOOL stdErrCheck(void);
BOOL activateInfeed(void);
void decurl(USINT a);
REAL cutterRunningComp (void);
void goUpStop(void);
void resetDecurl(void);
void calculateDemandTension (void);
void addErrorToBufferLastError(USINT typ,USINT subTyp);
REAL demandTensionToTorque(REAL tension);
BOOL torqueAccContribution(REAL acc);
void setButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL isButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu);
BOOL isLocked(void);
void checkEstop(void);
BOOL isLoopLocked (void);
BOOL isLoopBottom (void);
void setOutput(void);
void setAccOrDec(REAL speed);
REAL printerSpeedLoopLevelCompensation(void);
REAL rabs(REAL a);
REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max);

REAL autoTension (void) //mark 3
{
   REAL deltaSpd, rP, rD, dds;
   static REAL lastDSpd;
   static USINT preLoadTimer;
   deltaSpd =rabs(buffer.reading.infeedSpeed - buffer.reading.outfeedSpeed);
   if (deltaSpd > 2.0) rP = buffer.setting.innerLoopManualTension;
   else if (deltaSpd < 0.05) rP = 11;
   else rP = rmap(deltaSpd,0.05,2.0,11,buffer.setting.innerLoopManualTension);
   
   dds = rabs(lastDSpd - (buffer.reading.infeedSpeed - buffer.reading.outfeedSpeed))/SECONDS_UNIT; // m/s^2
   
   if (dds > 4.0) rD = buffer.setting.innerLoopManualTension;
   else if (dds < 0.1) rD = 11;
   else rD = rmap(dds,0.1,4.0,11,buffer.setting.innerLoopManualTension);
   
   lastDSpd = buffer.reading.infeedSpeed - buffer.reading.outfeedSpeed;
   
   if (buffer.command.preLoadTension) //set when right before deliver...
   {
      buffer.command.preLoadTension = FALSE;
      preLoadTimer = 50;
   }
   
   if (preLoadTimer)
   {
      rP = buffer.setting.innerLoopManualTension;
      preLoadTimer--;
   }   
   
   return (buffer.setting.innerLoopManualTension * 0.30 + rP * 0.15 + rD * 0.55);
}


void calculateTimeLeft(void)
{
   static REAL metersLast120Sec;
   static REAL time120Sec;
   REAL sheetThicknessArea;
   REAL diameterArea;
   REAL metersLeft;
   
   time120Sec += SECONDS_UNIT;
   metersLast120Sec += buffer.reading.infeedSpeed * SECONDS_UNIT;
   
   if (metersLast120Sec <= 0) {
      buffer.reading.minutesLeft = 0;
      return;
   }
   
   if (!u20.connected || !u20.started) return;
   
   if (time120Sec>120)
   {
      sheetThicknessArea = (stacker.reading.stepAvg + stacker.setting.tableSteps*2000)/(3*streamer.setting.sheetsUp);
      diameterArea = (((u20.diameter * 25.4)/2) * ((u20.diameter * 25.4)/2) * 3.14158) - (((6 * 25.4)/2) * ((6 * 25.4)/2) * 3.14158);
      metersLeft = diameterArea/sheetThicknessArea;
      if ((metersLeft/(metersLast120Sec/2))*2 < buffer.reading.minutesLeft || (metersLeft/(metersLast120Sec/2))*0.5 > buffer.reading.minutesLeft)
          buffer.reading.minutesLeft = metersLeft/(metersLast120Sec/2);
      else
          buffer.reading.minutesLeft = (buffer.reading.minutesLeft*1.5 + (metersLeft/(metersLast120Sec/2))) / 2.5;

      time120Sec -= time120Sec;
      metersLast120Sec = 0;
   }
}

REAL rabs(REAL a)
{
   if (a<0) return a*-1.0; 
   else return a;
}

REAL printerSpeedLoopLevelCompensation(void)
{
   REAL tmpSpeedCmp,tmpComp,usage,cmp;
   usage = buffer.reading.accumulated/MAXIMUM_PAPER_AMOUNT;
   buffer.reading.usage_Dev = usage*100;
   cmp = buffer.setting.loopLevelCompensation*0.01;
   tmpSpeedCmp = buffer.reading.infeedSpeed/buffer.setting.maxInfeedSpeed;
   tmpSpeedCmp = ((tmpSpeedCmp*tmpSpeedCmp)+tmpSpeedCmp)*0.5;
   tmpComp = ((((1-usage)*cmp)*((1-usage)*cmp))*tmpSpeedCmp)+1.0;
   return (usage*tmpComp*100.0);
}

REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max)
{
   return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


REAL DdtCmp(void)
{
   static REAL _usage;
   REAL tmp;
   tmp = _usage - infeedLoop.usage;
   _usage = infeedLoop.usage;
   tmp = tmp * infeedLoop.dCompensation;
   if (tmp>(infeedLoop.dCompensation*2.0)) tmp = (infeedLoop.dCompensation*(2.0));
   if (tmp<(infeedLoop.dCompensation*-2.0)) tmp = (infeedLoop.dCompensation*(-2.0));
   if ((tmp+infeedLoop.usage) > 100) tmp = 0;  
   if ((tmp+infeedLoop.usage) < 0)   tmp = 0;
   return (tmp);
}

REAL IdtCmp(BOOL reset)
{
   static REAL _usage;
   static REAL Sdt;
   REAL err,tmp;
   if (reset) { //windup reset
      Sdt = 0;
      _usage = infeedLoop.usage;
      return 0;
   }
   err = _usage - infeedLoop.usage;
   Sdt += err;
   if (err>10) err = 10;
   if (err<-10) err = -10;
   _usage = infeedLoop.usage;
   tmp = Sdt * infeedLoop.iCompensation;
   if ((tmp+infeedLoop.usage) > 100) tmp = 0;  
   if ((tmp+infeedLoop.usage) < 0)   tmp = 0;
   return (tmp);
}

BOOL firstTimeInState(void)
{ 
   if (lastState != state) return 1;
   return 0;
}

void b50infeedLoop (void)
{
   if (buffer.reading.input.infeedLoop < buffer.setting.infeedLoopTop/*infeedLoop.topPos*/) infeedLoop.usage = 0;
   else if (buffer.reading.input.infeedLoop > buffer.setting.infeedLoopBottom/*infeedLoop.bottomPos*/) infeedLoop.usage = 100.0;
   else infeedLoop.usage = rmap(buffer.reading.input.infeedLoop,buffer.setting.infeedLoopTop,buffer.setting.infeedLoopBottom,0,100);
}

BOOL lastFBBR;
//UINT FBBRcnt=0;

void setOutput(void)
{
   
   if (node[22].station[0]) { //PowerLink Air Valve mog�ng
      //EX260_airValveOut[0] = 0;
      physical.buffer.valve.mask = 0;
      if (buffer.setting.forceOutputEnable.loopValve1_1) {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (buffer.setting.forceOutputValue.loopValve1_1?0x80:0x00);
         buffer.reading.output.loopValve1_1 = buffer.setting.forceOutputValue.loopValve1_1;
      }
      else {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (output.loopValve1_1?0x80:0x00);    
         buffer.reading.output.loopValve1_1 = output.loopValve1_1;
      }
      if (buffer.setting.forceOutputEnable.loopValve1_2) {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (buffer.setting.forceOutputValue.loopValve1_2?0x40:0x00);
         buffer.reading.output.loopValve1_2 = buffer.setting.forceOutputValue.loopValve1_2;
      }
      else {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (output.loopValve1_2?0x40:0x00); 
         buffer.reading.output.loopValve1_2 = output.loopValve1_2;
      }
	
      if (buffer.setting.forceOutputEnable.loopValve2) {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (buffer.setting.forceOutputValue.loopValve2?0x10:0x00);
         buffer.reading.output.loopValve2 = buffer.setting.forceOutputValue.loopValve2;
      }
      else {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (output.loopValve2?0x10:0x00);
         buffer.reading.output.loopValve2 = output.loopValve2;    
      }
	
      if (buffer.setting.forceOutputEnable.loopLockValve) {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (buffer.setting.forceOutputValue.loopLockValve?0x04:0x00);
         buffer.reading.output.loopLockValve = buffer.setting.forceOutputValue.loopLockValve;
      }
      else {
         physical.buffer.valve.mask = physical.buffer.valve.mask | (output.loopLockValve?0x04:0x00);
         buffer.reading.output.loopLockValve = output.loopLockValve; 
      }
   }
   else {
      if (buffer.setting.forceOutputEnable.loopValve1_1)
         physical.buffer.digital.out5  = buffer.reading.output.loopValve1_1      = buffer.setting.forceOutputValue.loopValve1_1;
      else physical.buffer.digital.out5  = buffer.reading.output.loopValve1_1    = output.loopValve1_1;    
	
      if (buffer.setting.forceOutputEnable.loopValve1_2)
         physical.buffer.digital.out2 = buffer.reading.output.loopValve1_2       = buffer.setting.forceOutputValue.loopValve1_2;
      else physical.buffer.digital.out2  = buffer.reading.output.loopValve1_2    = output.loopValve1_2;    
	
      if (buffer.setting.forceOutputEnable.loopValve2)
         physical.buffer.digital.out4  = buffer.reading.output.loopValve2        = buffer.setting.forceOutputValue.loopValve2;
      else physical.buffer.digital.out4  = buffer.reading.output.loopValve2      = output.loopValve2;    
	
      if (buffer.setting.forceOutputEnable.loopLockValve)
         physical.buffer.digital.out6  = buffer.reading.output.loopLockValve     = buffer.setting.forceOutputValue.loopLockValve;
      else physical.buffer.digital.out6  = buffer.reading.output.loopLockValve   = output.loopLockValve;    
   }
   
   if (buffer.setting.forceOutputEnable.setValvePressure)
   {
      physical.buffer.analog.out1 = buffer.setting.forceOutputValue.setValvePressure;
   }
   else physical.buffer.analog.out1 = output.setValvePressure;
   buffer.reading.output.setValvePressure = physical.buffer.analog.out1;
}

void getInput(void)
{
   //digital
   buffer.reading.input.infeedPaperEnd = physical.buffer.digital.in1;
   //analog
   buffer.reading.input.infeedLoop = physical.buffer.analog.in2;
   buffer.reading.input.innerLoop = physical.buffer.analog.in1;   
   
   //safety
   buffer.reading.input.coverClosed = physical.buffer.digital.in18;
   buffer.reading.input.eStop = physical.buffer.digital.in17;
   
   if (node[20].station[3]) { //moduleID[3] = 42155 (old buffer type)// check on that if the dummy slot is ever used
      //digital
      buffer.reading.input.infeedNipRoller = physical.buffer.digital.in12;
      buffer.reading.input.outfeedNipRoller = physical.buffer.digital.in10;
      buffer.reading.input.pressureGuard = physical.buffer.digital.in15;
      buffer.reading.input.prototypeVersion = physical.buffer.digital.in16;
   }
   else {
      //digital
      buffer.reading.input.pressureGuard = physical.buffer.digital.in3;
      buffer.reading.input.infeedNipRoller = physical.buffer.digital.in5;
      buffer.reading.input.outfeedNipRoller = physical.buffer.digital.in7;
      buffer.reading.input.prototypeVersion = FALSE; 
   }
}

void _CYCLIC ProgramCyclic(void)
{
   //static USINT lastDecurl;
   buffer.setting.maxInfeedSpeed = 3.77;
   getInput();
   calculateTimeLeft();
   static LREAL lastSecondTick;
   static INT lastAin;
   //static UINT decurlTimeOut;
   static int jogOutfStartStep = 0;
   static int jogOutfStopStep = 0;
   static BOOL outfeedWasConnected = FALSE;
   static USINT haltCnt;
   static USINT overFullCrachCnt;
   static LREAL lastCompensationDist;
   buffer.reading.maxAccumulated = MAXIMUM_PAPER_AMOUNT;
   bufferTick += SECONDS_UNIT;
   buttonCommand ();
   calculateDemandTension();
   if ((bufferTick-lastSecondTick) > 0.25) {
      lastSecondTick = bufferTick;
      quarterSecondCycle();
   }
   
   buffer.setting.infeedOperation = buffer.setting.infeedOperationSave;
   //buffer.setting.infeedOperation = fixSpeed;
   
   if (buffer.setting.decurlTime>0) buffer.setting.disableDecurl = 0;
   else buffer.setting.disableDecurl = 1;
   
   buffer.reading.outfeedSpeed = Slave[SLAVE_BUF_OUT_INDEX].Status.ActVelocity*0.001;
   buffer.reading.calculatedAccumulated += buffer.reading.infeedSpeed*SECONDS_UNIT;
   buffer.reading.calculatedAccumulated -= buffer.reading.outfeedSpeed*SECONDS_UNIT;
   buffer.reading.totalInSinceLoad_Dev += buffer.reading.infeedSpeed*SECONDS_UNIT;
   buffer.reading.totalOutSinceLoad_Dev += buffer.reading.outfeedSpeed*SECONDS_UNIT;
   
   buffer.reading.infeedSpeed_mm = buffer.reading.infeedSpeed * 60;
   buffer.reading.infeedSpeed_fpm = buffer.reading.infeedSpeed_mm * 3.2808;
   
   if (desensitizeLoopFault>0) {
      if (buffer.reading.infeedSpeed>0.1)
         desensitizeLoopFault -= 0.002;
      if (buffer.reading.outfeedSpeed>0.1)
         desensitizeLoopFault -= 0.002;
      if (desensitizeLoopFault>5.0 && desensitizeLoopFault<5.004) buffer.reading.calculatedAccumulated = buffer.reading.accumulated;
      if (desensitizeLoopFault>4.0 && desensitizeLoopFault<4.004) buffer.reading.calculatedAccumulated = (buffer.reading.accumulated + buffer.reading.calculatedAccumulated)/2;
      if (desensitizeLoopFault>3.0 && desensitizeLoopFault<3.004) buffer.reading.calculatedAccumulated = (buffer.reading.accumulated + buffer.reading.calculatedAccumulated)/2;
      if (desensitizeLoopFault>2.0 && desensitizeLoopFault<2.004) buffer.reading.calculatedAccumulated = (buffer.reading.accumulated + buffer.reading.calculatedAccumulated)/2;
      if (desensitizeLoopFault>1.0 && desensitizeLoopFault<1.004) buffer.reading.calculatedAccumulated = (buffer.reading.accumulated + buffer.reading.calculatedAccumulated)/2;
   }
   
   if ((buffer.reading.totalInSinceLoad_Dev - lastCompensationDist) > 20) {//prevents drift
      buffer.reading.calculatedAccumulated = (buffer.reading.accumulated + buffer.reading.calculatedAccumulated)/2;
      lastCompensationDist = buffer.reading.totalInSinceLoad_Dev;
   }
   
   if (cutter.reading.input.feedButton || merger.reading.input.feedButton) { //prevents error during feed on yellow buttons
      buffer.reading.calculatedAccumulated = buffer.reading.accumulated;
      lastCompensationDist = buffer.reading.totalInSinceLoad_Dev;
   }

   b50infeedLoop();
   checkEstop();
	
   if (buffer.reading.startUpSeq<13) {
      buffer.reading.startUpSeq = (USINT)((bufferTick*12)/40);  
   }
   else if (buffer.reading.startUpSeq == 13 && bInfeed.Status.DriveStatus.DriveEnable) buffer.reading.startUpSeq = 14;
   else if (buffer.reading.startUpSeq == 14 && bInfeed.Status.DriveStatus.ControllerReady) buffer.reading.startUpSeq = 15;
	
   if (buffer.reading.fillBufferByReady && buffer.reading.fillBufferByReady != lastFBBR) FBBRcnt = 3000;
   lastFBBR = buffer.reading.fillBufferByReady;
   if (FBBRcnt) {
      FBBRcnt--;
      if ((buffer.reading.usage + buffer.reading.infeedSpeed*10) > 99) FBBRcnt = 0;
   }
   else if (buffer.reading.fillBufferByReady) buffer.reading.fillBufferByReady = FALSE;
	
   buffer.reading.inBottom = isLoopBottom();
	
   if (infeedLoop.usage > 99 && buffer.reading.inBottom && buffer.reading.infeedSpeed>0.5) {
      overFullCrachCnt++;
   }
   else overFullCrachCnt = 0;
	
   if (overFullCrachCnt == 100) {
      addError(OVERFULL,0); 
      overFullCrachCnt = 0;
   }
    
    
   if (0 == lineController.reading.initDone) buffer.command.coverFault = FALSE; 
   if (buffer.command.coverFault && safety.reading.initialized) {
      addError(COVER_OPEN,0); 
   }
   else if (!safety.reading.initialized) buffer.command.coverFault = FALSE;
	
   buffer.reading.status.warning = FALSE;
   buffer.reading.status.stopped = FALSE;
   if (buffer.reading.inDecurl && standStill() && state == running) buffer.reading.status.warning = TRUE;
   if (buttonStopped && state == running) buffer.reading.status.stopped = TRUE;
	
   if (state != init) lineController.setting.bufferValveTest.force = FALSE;
   
   switch (state)
   {
      case init:
         buffer.reading.fixInfeedRunning = FALSE;
         buffer.reading.status.state = Status_NotLoaded;
         //buffer.reading.stateInit = TRUE;
         if (firstTimeInState())
         {
            strcpy(ULog.text[ULog.ix++], "*B50 Enter state init");
            if (buffer.reading.inDecurl) {
               decurl(EXIT);
               strcpy(ULog.text[ULog.ix++], "*B50 Exit Decurl");
            }
            bInfeed.Command.Power = 1;
         }
         lastState = state;
         //buffer.reading.error = 0;
         //buffer.reading.ready = 0;
         bufInfeed.speed = 0;
         if (bufferCommand.load) state = load;
         else if (bufferCommand.manualLoad) state = manualLoad;
         innerLoop(TENSION,0);
         decurlTimeOut=0;
         if (buffer.reading.inDecurl) decurl(EXECUTE);
         else  resetDecurl();
         startRunSlowCnt = 1000;
         haltCnt = 0;
         buffer.reading.calculatedAccumulated = buffer.reading.accumulated;
         break;
      case load:
         buffer.reading.fixInfeedRunning = FALSE;
         buffer.reading.status.state = Status_Loading;
         //buffer.reading.stateLoad = TRUE;
         if (firstTimeInState())
         {
            strcpy(ULog.text[ULog.ix++], "*B50 Enter state load");
         }
         lastState = state;
         haltCnt = 0;
         bufInfeed.speed = 0;
         //buffer.reading.error = 0;
         //buffer.reading.ready = 0;
         //		sourceB50.status = 1;
         decurlTimeOut=0;
         //if (bufferCommand.stop) state = init;
                  
         if (autoLoad()) 
         {
            state = running;
            buffer.reading.calculatedAccumulated = buffer.reading.accumulated;
            lastCompensationDist = buffer.reading.totalInSinceLoad_Dev = buffer.reading.totalOutSinceLoad_Dev = 0;
         }
         break;
      case running:
         buffer.reading.status.state = Status_Ready;
         //buffer.reading.stateRunning = TRUE;
         if (firstTimeInState())
         {
         strcpy(ULog.text[ULog.ix++], "*B50 Enter state running");
         }
         lastState = state;
         haltCnt = 0;
         if (buffer.command.resetInnerLoopFault) {
            buffer.reading.calculatedAccumulated = buffer.reading.accumulated;
            buffer.command.resetInnerLoopFault = FALSE;
         }
         //if (/*bufInfeed.speed == 0*/standStill() && settingB50.decurl == AUTO && !buffer.reading.inDecurl)
         if (!buffer.reading.inDecurl && standStill()) {
            if (abs(lastAin - buffer.reading.input.innerLoop)<40) {
               if (!folder.reading.inManualLoad)
                  decurlTimeOut++;
            }
            lastAin = buffer.reading.input.innerLoop;
            if (decurlTimeOut > (buffer.setting.decurlTime*96)) buffer.command.decurl = 1;
         }
         else decurlTimeOut=0;
			
         if (buffer.reading.inDecurl) {
            decurl(EXECUTE);
            if (!standStill())                           buffer.command.releaseDecurl = 1;
            if (buffer.setting.disableDecurl)            buffer.command.releaseDecurl = 1;
            if (pif.printer.blowerOn 
               || pif.printer.wakeUp 
               || cutter.reading.input.printerBlowerOn) buffer.command.releaseDecurl = 1;
         }
         if (!buffer.reading.inDecurl && buffer.command.decurl && !buffer.setting.disableDecurl) decurl(START);
         else if (buffer.command.releaseDecurl) {
            decurl(EXIT);
            buffer.command.decurl = 0;
            decurlTimeOut = 0;
         }
         else if (!buffer.reading.inDecurl) {
            if (buffer.setting.innerLoopOperation == manual) innerLoop(TENSION,buffer.setting.innerLoopManualTension);
            else innerLoop(TENSION,autoTension()); 
         }
         //debug2 = 202;
         stdErrCheck();
         if (buffer.setting.infeedOperation == fixSpeed) {  //u20
            infeedMode = 0;
            if (slowAcc > 0)
            {
               bufInfeed.acceleration = cutter.setting.masterAcc * 0.00018;
               slowAcc--;
            }
            else
               bufInfeed.acceleration = cutter.setting.masterAcc * 0.0009;
            bufInfeed.deceleration = cutter.setting.masterDec * 0.0005;
            
            if (!u20.started) {//5 sec delay
               U20StartDelay = 8.0;
            }
         
            if (!cutter.reading.output.ready && u20.started)
            {
               
               if (FBBRcnt>0) {
                   bufInfeed.speed = 1.0;
                  if (buffer.reading.usage > buffer.setting.slowLevel) bufInfeed.speed = rmap(buffer.reading.usage,buffer.setting.slowLevel,buffer.setting.stopLevel,1.0,0.0);
               }
               else {
                  bufInfeed.speed = lineController.setting.unwinder.maxSpeed;
                  kneePoint = (buffer.setting.slowLevel + buffer.setting.stopLevel)/2;
                  kneeSpeed = lineController.setting.unwinder.maxSpeed * 0.70;
                  if (buffer.reading.usage > kneePoint) {  //below knee
                     bufInfeed.speed = rmap(buffer.reading.usage,buffer.setting.slowLevel,buffer.setting.stopLevel,kneeSpeed,0.0);
                  }
                  else if (buffer.reading.usage > buffer.setting.slowLevel) {  // over knee
                     bufInfeed.speed = rmap(buffer.reading.usage,buffer.setting.slowLevel,buffer.setting.stopLevel,lineController.setting.unwinder.maxSpeed,kneeSpeed);
                  }
                  //if (buffer.reading.usage > buffer.setting.slowLevel) bufInfeed.speed = rmap(buffer.reading.usage,buffer.setting.slowLevel,buffer.setting.stopLevel,lineController.setting.unwinder.maxSpeed,0.0);
               }
               
               if (bufInfeed.speed > 0.05) buffer.reading.fixInfeedRunning = TRUE;
               else buffer.reading.fixInfeedRunning = FALSE;

               
               if (U20StartDelay>0) {
                  bufInfeed.speed = 0.0;
                  U20StartDelay -= SECONDS_UNIT;
               }
               if (/*!stacker.reading.input.alignerTopCoverClosed || */!cutter.reading.input.topCover) {
                  if (bufInfeed.speed > 0.6) bufInfeed.speed = 0.6;
               }
               
               if ((lineController.reading.maximumSpeed * 0.8) < bufInfeed.speed ) {
                  bufInfeed.speed = (lineController.reading.maximumSpeed * (0.70 + (buffer.reading.usage < 35 ? 0.10:0))) - (buffer.reading.usage > 20 ? rmap(buffer.reading.usage,20,100,0.12,0.3):0);
                  
                  
                  
                  
                  if (bufInfeed.speed < 0.0) bufInfeed.speed = 0.0; 
                  slowAcc = 1000;
               }
               /*if (cutter.reading.processingWhite && bufInfeed.speed > (lineController.setting.unwinder.maxSpeed/2) )
               {
                  bufInfeed.speed = (lineController.setting.unwinder.maxSpeed/2);
               }*/
               if (u20.diameter < 7)
               {
                  bufInfeed.speed = 0.0;
                  bufInfeed.deceleration = cutter.setting.masterDec * 0.00005;
               }   
               else if (u20.diameter < 8)
               {
                  if (bufInfeed.speed > 1.0) bufInfeed.speed = 1.0;
                  bufInfeed.deceleration = cutter.setting.masterDec * 0.00005;
               }
               
            }
            else {
               bufInfeed.speed = 0;
               buffer.reading.fixInfeedRunning = FALSE;
               
               if (FBBRcnt>0 && u20.started) {
                  bufInfeed.speed = 1.0;
                  if (buffer.reading.usage > buffer.setting.slowLevel) bufInfeed.speed = rmap(buffer.reading.usage,buffer.setting.slowLevel,buffer.setting.stopLevel,1.0,0.0);
               }
               
               
            }
         }
         if (buffer.setting.infeedOperation == loopControl) {
            infeedMode = 0;
            bufInfeed.speed = rmap(infeedLoop.usage+DdtCmp()+IdtCmp(0),0,100,0,buffer.setting.maxInfeedSpeed/*infeedLoop.maxInputSpeed*/);
            if (startRunSlowCnt) {
               startRunSlowCnt--;
               if (bufInfeed.speed>(0.35 + (buffer.reading.inDecurl?0.15:0))) {
                  bufInfeed.speed=0.35 + (buffer.reading.inDecurl?0.15:0);
                  IdtCmp(1); // prevent wind-up
               }
						
            }
            if (buffer.reading.input.infeedLoop < buffer.setting.infeedLoopTop) {
               bufInfeed.speed=0;
               if (startRunSlowCnt>25) startRunSlowCnt = 25;
            }
         }
         if (bufferCommand.stop) {
            state = init;
            bufferCommand.stop = FALSE;
         } 
         break;
      case manualLoad:
         buffer.reading.fixInfeedRunning = FALSE;
         buffer.reading.status.state = Status_ManualLoad;
         //buffer.reading.stateManual = TRUE;
         if (firstTimeInState()) {
            strcpy(ULog.text[ULog.ix++], "*B50 Enter state manualLoad");
         }
         lastState = state;
         haltCnt = 0;
         //buffer.reading.error = 0;
         //buffer.reading.ready = 0;
         if (buffer.setting.infeedOperation == loopControl) {
            if (!feed) bufInfeed.speed = 0;
         }
         if(feedOutfeed) {
            if(jogOutfStartStep == 0) {
               strcpy(ULog.text[ULog.ix++], "*B50 Outfeed start");
               outfeedWasConnected = Slave[SLAVE_BUF_OUT_INDEX].AxisState.SynchronizedMotion;
               if(outfeedWasConnected) {
                  strcpy(ULog.text[ULog.ix++], "Halt slave (B50 outfeed)");
                  Slave[SLAVE_BUF_OUT_INDEX].Command.Halt = TRUE;
               }
               jogOutfStartStep = 1;
            }
            if(jogOutfStartStep == 1) {
               if(Slave[SLAVE_BUF_OUT_INDEX].AxisState.StandStill) {
                  strcpy(ULog.text[ULog.ix++], "Start moving (B50 outfeed)");
                  Slave[SLAVE_BUF_OUT_INDEX].Parameter.Acceleration = 5000;
                  Slave[SLAVE_BUF_OUT_INDEX].Parameter.Deceleration = 7000;
                  Slave[SLAVE_BUF_OUT_INDEX].Parameter.Velocity = 150; //190
                  Slave[SLAVE_BUF_OUT_INDEX].Command.MoveVelocity = TRUE;
                  jogOutfStartStep = 2;
               }
            }
         }
         else {
            if(jogOutfStartStep == 2 || jogOutfStartStep == 1) {
               strcpy(ULog.text[ULog.ix++], "Stop moving (B50 outfeed)");
               Slave[SLAVE_BUF_OUT_INDEX].Command.Halt = TRUE;
               Slave[SLAVE_BUF_OUT_INDEX].Parameter.Acceleration = 500000;
               Slave[SLAVE_BUF_OUT_INDEX].Parameter.Deceleration = 500000;
               jogOutfStopStep = 1;
               jogOutfStartStep = 0;
            }
            if(jogOutfStopStep == 1) {
               if(Slave[SLAVE_BUF_OUT_INDEX].AxisState.StandStill) {
                  strcpy(ULog.text[ULog.ix++], "Stopped (B50 outfeed)");
                  if(outfeedWasConnected) {
                     strcpy(ULog.text[ULog.ix++], "Connect slave (B50 outfeed)");
                     Slave[SLAVE_BUF_OUT_INDEX].Command.StartSlave = TRUE;
                     jogOutfStopStep = 2;
                  }
                  else {
                     strcpy(ULog.text[ULog.ix++], "*B50 Outfeed finished (not connected)");
                     jogOutfStopStep = 0;
                  }   
               }
            }
            if(jogOutfStopStep == 2) {
               if(Slave[SLAVE_BUF_OUT_INDEX].AxisState.SynchronizedMotion) {
                  strcpy(ULog.text[ULog.ix++], "*B50 Outfeed finished (connected)");
                  jogOutfStopStep = 0;
               }
            }            
         }
         if (bufferCommand.load)
            state = load;
         break;
      case error:
         buffer.reading.fixInfeedRunning = FALSE;
         buffer.reading.status.state = Status_Error;
         if (firstTimeInState()) {
            strcpy(ULog.text[ULog.ix++], "*B50 Enter state error");
         }
         lastState = state;
         innerLoop(TENSION,0);
         bufInfeed.speed = 0;
         decurlTimeOut=0;
         startRunSlowCnt = 1000;
         if (buffer.setting.infeedOperation == loopControl) {
            bufInfeed.speed = 0;
            if (bufInfeed.actualSpeed > 0.1) haltCnt++;
            if (haltCnt>40 && haltCnt<=60) {
               bInfeed.Command.Halt=haltCnt%2;
               strcpy(ULog.text[ULog.ix++], "*B50 Halt infeed"); //stop for fuck sake
            }
         }
         goUpStop();
         if (bufferCommand.errorAck)
         {
            if (safety.reading.resetPossible && buffer.reading.input.eStop) {
               safety.command.eStopReset = TRUE;
               state = init;
               machineError.active = FALSE;
            }
            buffer.command.coverFault = FALSE;
         }
         break;
   }
   resetCommand();	
   if (state != running) {
      IdtCmp(1);
      standStillTimer=2000;
   }
   if (buffer.reading.inDecurl && startRunSlowCnt<2) startRunSlowCnt = 2;//bufInfeed.speed = 0;
   getInfeed();
   if (state == error) bufInfeed.speed = 0;
   if (buttonStopped && bufInfeed.speed>0.5) bufInfeed.speed = 0.5;
   if (!buffer.reading.input.coverClosed && bufInfeed.speed>0.15) bufInfeed.speed = 0.15;
   setInfeed();
   if (state == error && haltCnt>50) bInfeed.Parameter.Velocity=0; //stop for fuck sake
   buffer.reading.accumulated = calculatePaperAmount();
   //sourceB50.decurlTimer = buffer.setting.decurlTime - (decurlTimeOut/96);
   buffer.reading.infeedSpeed = bufInfeed.actualSpeed;
   
   setAccOrDec(buffer.reading.infeedSpeed);
   
   if (state == running) {
      
      if (buffer.setting.loopLevelCompensation>1)
         buffer.reading.usage = printerSpeedLoopLevelCompensation();
      else
         buffer.reading.usage = 100*buffer.reading.accumulated/MAXIMUM_PAPER_AMOUNT;
      if (buffer.reading.usage>100) buffer.reading.usage = 100;
      else if (buffer.reading.usage<0) buffer.reading.usage = 0.0;
      if (buffer.reading.usage>5) buffer.reading.noPaper = FALSE;
      else buffer.reading.noPaper= TRUE;
      
      if (buffer.reading.inDecurl) buffer.reading.usage = 0.0;
      if (buttonStopped) buffer.reading.maximumSpeed = 0.0;
      else if (buffer.reading.inDecurl) {
         buffer.reading.maximumSpeed = 0.1;
         standStillTimer=3000;
      }
      else {
         if (buffer.reading.infeedSpeed<0.05 && standStillTimer<1000) standStillTimer++;
         if (standStillTimer>500 && buffer.reading.infeedSpeed<0.1)
            buffer.reading.maximumSpeed = 0.5;
         else {
            buffer.reading.maximumSpeed = buffer.reading.infeedSpeed * 2 + 1.0 + buffer.reading.usage*0.0025;
            standStillTimer = 0;
            /*if (lineController.setup.unwinder.installed && cutter.reading.processingWhite)
            {
               if (buffer.reading.maximumSpeed > (cutter.setting.infeedRunSpeed/2000)) buffer.reading.maximumSpeed = cutter.setting.infeedRunSpeed/2000;
            } */  
         }
      }
      if (!buffer.reading.input.coverClosed && buffer.reading.maximumSpeed>0.15) buffer.reading.maximumSpeed = 0.15;
   }
   else {
      if (buffer.reading.usage != 0) strcpy(ULog.text[ULog.ix++], "*B50 Usage 0% (by not ready)");
      buffer.reading.usage = buffer.reading.maximumSpeed = 0.0;
      buffer.reading.noPaper = TRUE;
      buffer.reading.usage_Dev = buffer.reading.usage;
   }
   setOutput();
   if (buffer.setting.loopLevelCompensation<=1) buffer.reading.usage_Dev = buffer.reading.usage;
}

void checkEstop(void)
{
   static UINT errorTimer;
   static UINT stopTimer;
   if (!safety.reading.initialized) return;
   if (state == init) {
      if (!buffer.reading.input.eStop) errorTimer++;
      else errorTimer = 0;
      if (errorTimer>100) {
         addError(B50_E_STOP , 2); 
         //e_stop_error_by_b50 = TRUE;
         strcpy(ULog.text[ULog.ix++], "*B50 ERROR e-stop");
      }
      stopTimer = 0;
   }
   else if (state != error) {
      errorTimer = 0;
      if (!buffer.reading.input.eStop) {
         //addError (B20_E_STOP,2);
         addError(B50_E_STOP , 1);
         //e_stop_error_by_b50 = TRUE;
         strcpy(ULog.text[ULog.ix++], "*B50 ERROR e-stop");
      }
      else if (safety.reading.eStop) {
         if (state == running) {
            if (0 == stopTimer) strcpy(ULog.text[ULog.ix++], "*B50 global e-stop seen -> normal stop");
            buttonStopped = TRUE;
            stopTimer++;
            if (stopTimer>250) {
               state = init;
               strcpy(ULog.text[ULog.ix++], "*B50 global e-stop seen -> init (1)");
            }
         }
         else {
            state = init;   
            strcpy(ULog.text[ULog.ix++], "*B50 global e-stop seen -> init (2)");
         }
      }
      else stopTimer = 0;
   }
   else 	{
      errorTimer = 0;
   }
}

REAL demandTensionToTorque(REAL tension)
{
   return (tension/4)*0.0495;
}

BOOL standStill(void)
{
   if (bufInfeed.actualSpeed < 0.05 && cutter.reading.webSpeed < 0.1) return 1;
   else return 0;
}

BOOL torqueAccContribution(REAL acc)
{
   if (acc > 0.03) return 0.2;
   return 0;
}

REAL cutterRunningComp (void)
{
   if (cutter.reading.webSpeed > 0.2 || cutter.reading.setSpeed > 0.1) return 1.0;
   else return 0.4;
}

void resetDecurl(void)
{
   stateDecurl = 0;
   buffer.command.decurl=0;
   buffer.reading.inDecurl = FALSE;		
   buffer.command.releaseDecurl = FALSE;
}

void decurl(USINT a)
{
   static INT upPreasure;
   static INT downPreasure;
   if (a == START)
   {
      stateDecurl = 1;
   }
   if (a == EXIT)
   {
      stateDecurl = 4;
   }
   switch (stateDecurl)
   {
      case 0:
         buffer.reading.inDecurl = FALSE;		
         buffer.command.releaseDecurl = FALSE;
         break;
      case 1:
         buffer.reading.inDecurl = TRUE;
         analogStartValue = buffer.reading.input.innerLoop;
         goUpSoft(0);
         stateDecurl = 2;
         buffer.command.decurl = 0;
         upPreasure = 0;
         break;
      case 2:
         if ((buffer.reading.input.innerLoop - analogStartValue) > DECURL_DISTANCE)
         {
            goUpStop();
            stateDecurl = 3;
         }
         else if (buffer.reading.input.innerLoop > (buffer.setting.innerLoopTop - RUNNING_TOP_POS_OFFSET - DECURL_DISTANCE - 50)) // too high, stop
         {
            goUpStop();
            stateDecurl = 3;
         }
         goUpSoft(5*(upPreasure++));
         buffer.reading.inDecurl = TRUE;
         break;
      case 3: // in decurl
         upPreasure = 0;
         buffer.reading.inDecurl = TRUE;
         goUpStop();
         break;
      case 4: 
         //analogExitValue = buffer.reading.input.innerLoop;
         if (buffer.reading.inDecurl) {
            stateDecurl = 5;
            buffer.reading.inDecurl = TRUE;
            downPreasure = 8300;
            buffer.command.decurl = 0;
            releasePreasure(downPreasure);
         }
         else stateDecurl = 6;
		   buffer.command.releaseDecurl = FALSE;
         break;
      case 5: 
         releasePreasure(downPreasure);
         buffer.reading.inDecurl = TRUE;
         //drop out quick
         if (bufInfeed.speed>0.5 && downPreasure>1000) downPreasure = 1000;
         else if (bufInfeed.speed>0.3 && downPreasure>6000) downPreasure = 6000;
			
         if (downPreasure>7500)
            downPreasure -= (16 * (bufInfeed.speed>0.1?2:1));
         else if (downPreasure>7000)
            downPreasure -= (25 * (bufInfeed.speed>0.1?2:1));
         else if (downPreasure>100)
            downPreasure -= (50 * (bufInfeed.speed>0.1?2:1));
         else 
         {
            releasePreasure(0);
            stateDecurl = 6;
         }
         break;
      case 6:
         goDown();
         buffer.reading.inDecurl = FALSE;
         stateDecurl = 0;
         startRunSlowCnt = 1000;
         break;
   }
}

REAL calculateInnerTension (void)
{
   return 11 + 0.0015333 * output.setValvePressure; //io.analogOut.bufferSetValvePressure;//io.analogOut.bufferSetValvePressure;
}

REAL calculatePaperAmount (void)
{
   REAL rTop;
   rTop = buffer.setting.innerLoopTop - RUNNING_TOP_POS_OFFSET;
   return MAXIMUM_PAPER_AMOUNT*((rTop - buffer.reading.input.innerLoop)/(rTop-buffer.setting.innerLoopBottom));
}

void goUp(void)
{
   output.setValvePressure = 17500;//15000;
   output.loopValve1_1 = TRUE;
   output.loopValve1_2 = FALSE;
   output.loopValve2 = FALSE;
	
}

void goUpSoft(INT p)
{
   output.setValvePressure = 11500+p;
   output.loopValve1_1 = TRUE;
   output.loopValve1_2 = FALSE;
   output.loopValve2 = FALSE;
}

void releasePreasure (INT p)
{
   output.setValvePressure = p;
   output.loopValve1_1 = TRUE;
   output.loopValve1_2 = FALSE;
   output.loopValve2 = FALSE;
}

void goUpStop(void)
{
   output.setValvePressure = 8750;
   output.loopValve1_1 = TRUE;
   output.loopValve1_2 = FALSE;
}

void goDown(void)
{
   output.setValvePressure = 0;
   output.loopValve1_1 = FALSE;
   output.loopValve1_2 = TRUE;
   output.loopValve2 = FALSE;
}

void unlock(void)
{
   output.loopLockValve = TRUE;
}

void lock(void)
{
   output.loopLockValve = FALSE;
}

BOOL isLocked(void)
{
   return !output.loopLockValve;
}

void pushDown(void)
{
   output.loopValve2 = TRUE;
}

void releasePushDown(void)
{
   output.loopValve2 = FALSE;
}

BOOL infeedPressureWheel(void)
{
   return buffer.reading.input.infeedNipRoller;
}

BOOL outfeedPressureWheel(void)
{
   return buffer.reading.input.outfeedNipRoller;
}

BOOL infeedPaperEnd(void)
{
   return buffer.reading.input.infeedPaperEnd;
}

BOOL sufficientIncomingPreassure(void)
{
   return buffer.reading.input.pressureGuard;
}

void resetCommand(void)
{
   bufferCommand.load = bufferCommand.manualLoad = bufferCommand.stop = bufferCommand.errorAck = FALSE;
}

BOOL stdErrCheck(void)
{
   static USINT spdErrCnt;
   static USINT ipeErrCnt;
   static UINT cycleUPCnt;
   static UINT noPaperCnt;
	
   if (infeedPaperEnd() == FALSE && infeedLoop.usage>0) ipeErrCnt++;
   else ipeErrCnt = 0;
   
   if (cutter.reading.input.printerTensionControl && cycleUPCnt < 6000) cycleUPCnt++;
   
   if (!cutter.reading.input.printerTensionControl) cycleUPCnt = 0;  
   
   if (infeedLoop.usage == 0 && noPaperCnt < 6000 && cycleUPCnt && cycleUPCnt<2000) noPaperCnt++;
   
   if ((buffer.reading.input.infeedLoop < (buffer.setting.infeedLoopTop-300)) && noPaperCnt < 6000 && cycleUPCnt && cycleUPCnt<2000) noPaperCnt++; //probably in load position
   
   if ((buffer.reading.input.infeedLoop < (buffer.setting.infeedLoopTop-500)) && noPaperCnt < 6000 && cycleUPCnt && cycleUPCnt<2000) noPaperCnt++; //definetly in load position
   
   if (infeedLoop.usage) noPaperCnt=0;
   
   if (noPaperCnt > 1000) {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR no paper movement when expected");
      addError(NO_PAPER_MOVEMENT,0);
      noPaperCnt = 0;
      cycleUPCnt = 2000;
   }
   if (!lineController.setup.unwinder.installed)
   {
      if (ipeErrCnt > (55-(bufInfeed.speed*10))) { 
         strcpy(ULog.text[ULog.ix++], "B50 ERROR paper end");
         addError(INFEED_PAPER_END,0);
      }
   }
   if (infeedPressureWheel() == FALSE) {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR infeed pressure wheel");
      addError(INFEED_PRESSURE_WHEEL,0);
   }
   if (outfeedPressureWheel() == FALSE) {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR outfeed pressure wheel");
      addError(OUTFEED_PRESSURE_WHEEL,0);
   }
   if (sufficientIncomingPreassure() == FALSE) {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR sufficient pressure");
      addError(NO_PRESSURE,0);
   }
   if (bufInfeed.error) {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR infeed axis error");
      addError(INFEED_AXIS_ERROR,bInfeed.Status.ErrorID);
      bufInfeed.homed = 0;
   }
   else if (!bufInfeed.homed) {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR infeed not homed");
      addError(INFEED_AXIS_ERROR,0);
   }
   if (bufInfeed.actualSpeed > buffer.setting.maxInfeedSpeed*2)
   {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR infeed overSpeed");
      addError(INFEED_OVERSPEED,1);
   }
   if (bufInfeed.actualSpeed > buffer.setting.maxInfeedSpeed*0.98)
   {
      spdErrCnt++;
      if (spdErrCnt>100)
      {
         strcpy(ULog.text[ULog.ix++], "B50 ERROR infeed overSpeed");
         addError(INFEED_OVERSPEED,0);	
      }
   }
   if (((buffer.reading.accumulated - (REAL)buffer.reading.calculatedAccumulated) > (1.25 + desensitizeLoopFault)) && state == running)
   {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR inner loop fault");
      addError(INNER_LOOP_FAULT,0);	
      desensitizeLoopFault = 6.0;
      buffer.reading.calculatedAccumulated = buffer.reading.accumulated;
   }
   if ((((REAL)buffer.reading.calculatedAccumulated - buffer.reading.accumulated) > (1.25 + desensitizeLoopFault)) && state == running)
   {
      strcpy(ULog.text[ULog.ix++], "B50 ERROR inner loop fault");
      addError(INNER_LOOP_FAULT,1);	
      desensitizeLoopFault = 6.0;
      buffer.reading.calculatedAccumulated = buffer.reading.accumulated;
   }
   else 
   {
      spdErrCnt = 0;	
   }
   if (state == error) return 1;
   else return 0;
}


void setPressure(REAL tension) //N
{
   REAL tmp;
   buffer.reading.autoTension = tension;
   tmp = (tension - 11.0) * 652;
   if (tmp>30000) tmp = 30000;
   if (tmp<0) tmp = 0;
   output.setValvePressure = tmp;
}

BOOL isLoopLocked (void)
{
   if ((buffer.reading.input.innerLoop > (buffer.setting.innerLoopTop-LOCK_OFFSET) && buffer.reading.output.loopLockValve == TRUE)) return TRUE;
   return FALSE;
}

BOOL isLoopBottom (void)
{
   if (buffer.reading.input.innerLoop < (buffer.setting.innerLoopBottom+1000)) return TRUE;
   return FALSE;
}

BOOL innerLoop(UINT action, REAL tension)
{
   static INT lastAnalog;
   static REAL standStillTime;
   if (action == 0)
   {
      standStillTime = 0;
   }
   else if (action == 1) //UP
   {
      releasePushDown();
      goUp();
      if (buffer.reading.input.innerLoop < (buffer.setting.innerLoopTop-LOCK_OFFSET)) { //
         unlock();
         return BUSY;
      }
      else {
         lock();
         return DONE;
      }
      standStillTime = 0;
   }
   else if (action == 2) //DOWN
   {
      releasePushDown();
      goDown();	
      unlock();
      if (buffer.reading.input.innerLoop < (buffer.setting.innerLoopTop-LOCK_OFFSET)) //LOOP_IN_TOP
      {
         if (abs(lastAnalog-buffer.reading.input.innerLoop) < 5) standStillTime += SECONDS_UNIT;
      }
      else standStillTime = 0;
	
      lastAnalog = buffer.reading.input.innerLoop;	
		
      if (standStillTime > 2.5) return DONE;	
         //else if (standStillTime > 0.25 && buffer.reading.input.innerLoop < (LOOP_IN_BOTTOM+10)) return DONE;	// we are standing in the bottom.
      else if (standStillTime > 0.25 && buffer.reading.input.innerLoop < (buffer.setting.innerLoopBottom+10)) return DONE;	// we are standing in the bottom.
      else return BUSY;
   }
   else if (action == 3) //TENSION
   {
      if (tension < 1) 
         releasePushDown();
      else
      {
         goDown();
         pushDown();
         setPressure(tension);
         debugPressure = tension;
         standStillTime = 0;	
      }
   }
   return DONE;
}

void buttonCommand (void)
{
   if (!GlobalCommand.Output.Status.AllDrivesEnable) return;
   static UINT loopCnt;
   BOOL btn1;
   BOOL btn2;
   BOOL btn3;
   BOOL btn4;
   BOOL btn5;
   static USINT resetBinfeed;
   static USINT pressTimeCnt;
   static mainState_enum lastState;
   if (state != manualLoad) {
      loopBusy = FALSE;
   }
   if (b50button.push1) btn1 = TRUE;
   else if (buffer.command.pressButton1) btn1 = TRUE;
   else btn1 = FALSE;
   buffer.command.pressButton1 = FALSE;
	
   if (b50button.push2) btn2 = TRUE;
   else if (buffer.command.pressButton2) btn2 = TRUE;
   else btn2 = FALSE;
   buffer.command.pressButton2 = FALSE;
	
   if (b50button.push3) btn3 = TRUE;
   else if (buffer.command.pressButton3) btn3 = TRUE;
   else btn3 = FALSE;
   buffer.command.pressButton3 = FALSE;
	
   if (b50button.push4) btn4 = TRUE;
   else if (buffer.command.pressButton4) btn4 = TRUE;
   else btn4 = FALSE;
   buffer.command.pressButton4 = FALSE;
	
   if (b50button.push5) btn5 = TRUE;
   else if (buffer.command.pressButton5) btn5 = TRUE;
   else btn5 = FALSE;
   buffer.command.pressButton5 = FALSE;
	
   switch (state)
   {
      case init:
         resetBinfeed = 0;
         /*if (btn1 && !b50button.lastPush1 && isButton(1,COLOR_WHITE) && buffer.reading.input.coverClosed) {
         //bufferCommand.load = TRUE;
         }*/
         if (lastState != init) buffer.reading.fillBufferByReady = FALSE;
         if (btn1 && isButton(1,COLOR_WHITE) && buffer.reading.input.coverClosed) {
            if (pressTimeCnt<255) pressTimeCnt++;
            if (pressTimeCnt>109) {
               if (safety.reading.allCoversClosed) globalPower = TRUE;
            }
            if (pressTimeCnt>115) {
               if (safety.reading.allCoversClosed && (Status_NotLoaded == stacker.reading.status.state)) stacker.command.pressButton1 = TRUE;
               if (safety.reading.allCoversClosed && (Status_NotLoaded == folder.reading.status.state))  folder.command.pressInfeedButton1 = TRUE;
            }
            if (pressTimeCnt>120) {
               if (safety.reading.allCoversClosed) cutter.command.home = TRUE;
            }
            if (pressTimeCnt>125) {
               bufferCommand.load = TRUE;
               lineController.command.beep = TRUE;
               pressTimeCnt = 0;
            }
            FBBRcnt = 0;
         }
         if (btn2 && !b50button.lastPush2 && buffer.reading.input.coverClosed) bufferCommand.manualLoad = TRUE;
         if (!btn1 && isButton(1,COLOR_WHITE) && buffer.reading.input.coverClosed && pressTimeCnt<110 && pressTimeCnt>2)
         {
            bufferCommand.load = TRUE;
            pressTimeCnt = 0;
         }
         if (buffer.command.load && buffer.reading.input.coverClosed) {
            bufferCommand.load = TRUE;
            pressTimeCnt = 0;
         }
         //added for fiserv
         if (btn5 && !b50button.lastPush5) {
            if (isButton(5,COLOR_ORANGE)) buffer.reading.fillBufferByReady = TRUE;
         }
         feedOutfeed=FALSE;
         feed = FALSE;
         loopInMotion = FALSE;
         break;
      case load:
         resetBinfeed = 0;
         pressTimeCnt = 0;
         feed = FALSE;
         feedOutfeed=FALSE;
         loopInMotion = FALSE;
         buffer.reading.fillBufferByReady = FALSE;
         FBBRcnt = 0;
         break;
      case running:
         resetBinfeed = 0;
         pressTimeCnt = 0;
         if (btn1 && !b50button.lastPush1) {
            if (isButton(1,COLOR_RED)) {
               buttonStopped = TRUE;
               if (folder.reading.runLocal) folder.command.pressInfeedButton1 = TRUE;
            }
            if (isButton(1,COLOR_WHITE) && buffer.reading.input.coverClosed) buttonStopped = FALSE;
         }
         if (btn3 && !b50button.lastPush3) {
            if (isButton(3,COLOR_YELLOW)||isButton(3,COLOR_RED)) bufferCommand.stop = TRUE;
         }
         if (btn5 && !b50button.lastPush5) {
            if (isButton(5,COLOR_ORANGE)) buffer.reading.fillBufferByReady = TRUE;
         }
         feedOutfeed=FALSE;
         feed = FALSE;
         loopInMotion = FALSE;
         break;
      case manualLoad:
         resetBinfeed = 0;
         pressTimeCnt = 0;
         FBBRcnt = 0;
         bufInfeed.speed = 0.0;
         if (btn1 && !b50button.lastPush1 && !loopBusy && isButton(1,COLOR_WHITE) && buffer.reading.input.coverClosed) bufferCommand.load = TRUE;
         if ((btn2 && isButton(2,COLOR_YELLOW)) || (btn4 && isButton(4,COLOR_YELLOW))) {
            if (bufInfeed.homed) {
               if (infeedLoop.usage > 1.0) {
                  feed = TRUE;
                  bufInfeed.speed = 0.25;
               }
               else {
                  feed = FALSE;
                  bufInfeed.speed = 0.0;
               }
            }
            else activateInfeed();
         }
         else {
            feed = FALSE;
            bufInfeed.speed = 0.0;
         }
			
         if ((btn3 && isButton(3,COLOR_YELLOW)) || (btn4 && isButton(4,COLOR_YELLOW))) {
            feedOutfeed=TRUE;
         }
         else 
            feedOutfeed=FALSE;
			
         if (btn5 && !b50button.lastPush5 && buffer.reading.input.coverClosed) {
            if (manualLoadloopCntrl == 0) loopCnt = 0;
            if (isLocked() || calculatePaperAmount()<0.1) manualLoadloopCntrl = 2;
            else manualLoadloopCntrl = 1;
			   
         }
			
         if (manualLoadloopCntrl == 1) {
            loopBusy = innerLoop(UP);
            loopInMotion = TRUE;
            loopCnt++;
            if (!safety.reading.bufferTopCoverLocked) safety.command.lockBufferTopCover = TRUE;
            if (!loopBusy) {
               manualLoadloopCntrl = 0;
               loopInMotion = FALSE;
               loopCnt = 0;
            }
         }
         else if (manualLoadloopCntrl == 2) {
            loopBusy = innerLoop(DOWN);
            loopInMotion = TRUE;
            loopCnt++;
            if (!safety.reading.bufferTopCoverLocked) safety.command.lockBufferTopCover = TRUE;
            if (!loopBusy) {
               manualLoadloopCntrl = 0;
               loopInMotion = FALSE;
               loopCnt = 0;
            }
         }
         else {
            if (safety.reading.bufferTopCoverLocked) safety.command.unlockBufferTopCover = TRUE;
         }
         buffer.reading.fillBufferByReady = FALSE;
         if (loopCnt>1500 && manualLoadloopCntrl == 1) {
            addError(LOOP_UP_FAILURE,0);
            manualLoadloopCntrl = 0;
            goDown();
         }
         if (loopCnt>1500 && manualLoadloopCntrl == 2) {
            addError(LOOP_DOWN_FAILURE,0);
            manualLoadloopCntrl = 0;
            goDown();
         }
         //	hold = 0;
         break;
      case error:
         pressTimeCnt = 0;
         if (btn5 && !b50button.lastPush5 && isButton(5,COLOR_BLUE))
         {
         if (bufInfeed.error) {
         resetBinfeed = 10;
         }
         else {
         bufferCommand.errorAck = TRUE;
         if (!bInfeed.Status.DriveStatus.HomingOk) bInfeed.Command.Home = 1;
         }
         //bufferCommand.errorAck = TRUE;
         }
            
         if (resetBinfeed)   {
         if (1 == bInfeed.Command.ErrorAcknowledge)
         bInfeed.Command.ErrorAcknowledge = 0;  
         else
         bInfeed.Command.ErrorAcknowledge = 1;
         resetBinfeed--;
         if (!resetBinfeed) bufferCommand.errorAck = TRUE;
         }
         feedOutfeed=FALSE;
         feed = FALSE;
         loopInMotion = FALSE;
         buffer.reading.fillBufferByReady = FALSE;
         break;
   }
   b50button.lastPush1 = btn1;
   b50button.lastPush2 = btn2;
   b50button.lastPush3 = btn3;
   b50button.lastPush4 = btn4;
   b50button.lastPush5 = btn5;
   buffer.command.load = FALSE;
   lastState = state;
}

void quarterSecondCycle (void)
{
   //static BOOL StopAvail;
    
   if (buttonLedTest <= 2000)
   {
      if (buttonLedTest < 5) {
         setButton(1,COLOR_OFF);
         setButton(2,COLOR_OFF);
         setButton(3,COLOR_OFF);
         setButton(4,COLOR_OFF);
         setButton(5,COLOR_OFF);
      }
      else if (buttonLedTest<25) {
         if (isButton(1,COLOR_YELLOW)) setButton(1,COLOR_OFF);
         else setButton(1,COLOR_YELLOW);
      }
      else if (buttonLedTest<50) {
         setButton(1,COLOR_YELLOW);
      }
      else if (buttonLedTest<75) {
         if (isButton(2,COLOR_YELLOW)) setButton(2,COLOR_OFF);
         else setButton(2,COLOR_YELLOW);
      }
      else if (buttonLedTest<100) {
         setButton(2,COLOR_YELLOW);
      }
      else if (buttonLedTest<125) {
         if (isButton(3,COLOR_YELLOW)) setButton(3,COLOR_OFF);
         else setButton(3,COLOR_YELLOW);
      }
      else if (buttonLedTest<150) {
         setButton(3,COLOR_YELLOW);
      }
      else if (buttonLedTest<175) {
         if (isButton(4,COLOR_YELLOW)) setButton(4,COLOR_OFF);
         else setButton(4,COLOR_YELLOW);
      }
      else if (buttonLedTest<190) {
         setButton(4,COLOR_YELLOW);
      }
      else if (buttonLedTest<205) {
         if (isButton(5,COLOR_YELLOW)) setButton(5,COLOR_OFF);
         else setButton(5,COLOR_YELLOW);
      }
      else if (buttonLedTest<220) {
         setButton(5,COLOR_YELLOW);
      }
      buttonLedTest++;
      buttonStopped = FALSE;
         
      if (GlobalCommand.Output.Status.AllDrivesEnable) buttonLedTest = 2001; 
      return;
   }

   if (!GlobalCommand.Output.Status.AllDrivesEnable) return;
    
   if (state == init)
   {
      buffer.reading.btn2Func = 2;
      buffer.reading.btn3Func = 0;
      if (buffer.reading.input.coverClosed) {
         setButton(1,COLOR_WHITE); //start
         if (isButton(2,COLOR_WHITE)) setButton(2,COLOR_OFF); //manual load
         else setButton(2,COLOR_WHITE);
      }
      else {
         setButton(1,COLOR_OFF);
         setButton(2,COLOR_OFF);
      }
      setButton(3,COLOR_OFF);
      setButton(4,COLOR_OFF);
      setButton(5,COLOR_OFF);
      if (!buffer.reading.fillBufferByReady) setButton(5,COLOR_ORANGE);
      buttonStopped = FALSE;
      if (safety.reading.bufferTopCoverLocked) safety.command.unlockBufferTopCover = TRUE;
   }
   else if (state == load)
   {
      buffer.reading.btn2Func = 0;
      buffer.reading.btn3Func = 0;
      setButton(1,COLOR_OFF);
      setButton(2,COLOR_OFF);
      setButton(3,COLOR_OFF);
      setButton(4,COLOR_OFF);
      setButton(5,COLOR_OFF);
      buttonStopped = FALSE;
      if (!safety.reading.bufferTopCoverLocked) safety.command.lockBufferTopCover = TRUE;
   }
   else if (state == manualLoad)
   {
      buffer.reading.btn2Func = 1;
      buffer.reading.btn3Func = 1;
      if (buffer.reading.input.coverClosed) setButton(1,COLOR_WHITE); //start
      else setButton(1,COLOR_OFF);
      setButton(3,COLOR_YELLOW);
      if (buffer.reading.input.infeedLoop > buffer.setting.infeedLoopTop) { 
         setButton(2,COLOR_YELLOW); //feed input
         // setButton(3,COLOR_YELLOW); //feed output
         setButton(4,COLOR_YELLOW); //feed all
      }
      else {  //no paper to feed from
         setButton(2,COLOR_OFF); //feed input
         // setButton(3,COLOR_OFF); //feed output
         setButton(4,COLOR_OFF); //feed all
         buffer.reading.status.warning = TRUE;
      }
      if (loopInMotion) {
         if(isButton(5,COLOR_GREEN)) setButton(5,COLOR_OFF);
         else setButton(5,COLOR_GREEN); 
      }
      else {
         if (buffer.reading.input.coverClosed) setButton(5,COLOR_GREEN); //start
         else setButton(5,COLOR_OFF);
      }
      buttonStopped = FALSE;
   }
   else if (state == running)
   {	
      buffer.reading.btn2Func = 0;
      buffer.reading.btn3Func = 0;
      if (!buttonStopped) {
         if (!safety.reading.bufferTopCoverLocked) safety.command.lockBufferTopCover = TRUE;
         setButton(1,COLOR_RED);	   
         setButton(2,COLOR_OFF);
         setButton(3,COLOR_OFF);
         setButton(4,COLOR_OFF);
         if (!pif.machine.ready && buffer.reading.usage < 40)
            setButton(5,COLOR_ORANGE);
         else 
            setButton(5,COLOR_OFF);
      }
      else {
         if (safety.reading.bufferTopCoverLocked) safety.command.unlockBufferTopCover = TRUE;
         if (buffer.reading.input.coverClosed) setButton(1,COLOR_WHITE);
         else setButton(1,COLOR_OFF);
         if (isButton(3,COLOR_RED)) setButton(3,COLOR_YELLOW);
         else setButton(3,COLOR_RED);
         setButton(4,COLOR_OFF);
         setButton(5,COLOR_OFF);
         buffer.reading.btn3Func = 2;
      }
      if (buffer.reading.accumulated > (100*buffer.setting.stopLevel/buffer.reading.maxAccumulated)) buffer.reading.fillBufferByReady = FALSE;
   }
   else if (state == error)
   {
      buffer.reading.btn2Func = 0;
      buffer.reading.btn3Func = 0;
      if (safety.reading.bufferTopCoverLocked) safety.command.unlockBufferTopCover = TRUE;
      if (isButton(1,COLOR_RED)) {
         setButton(1,COLOR_OFF);
         setButton(2,COLOR_OFF);
			
         if (!buffer.reading.input.eStop/*e_stop_error_by_b50*/) {
            setButton(3,COLOR_RED);
            setButton(4,COLOR_RED);
         }
         else {
            setButton(3,COLOR_OFF);
            setButton(4,COLOR_OFF);
         }
      }
      else
      {
         setButton(1,COLOR_RED);
         setButton(2,COLOR_RED);
         if (!buffer.reading.input.eStop/*e_stop_error_by_b50*/) {
            setButton(3,COLOR_OFF);
            setButton(4,COLOR_OFF);
         }
         else {
            setButton(3,COLOR_RED);
            setButton(4,COLOR_RED);
         }
      }
      if (safety.reading.resetPossible) setButton(5,COLOR_BLUE);
      else setButton(5,COLOR_OFF);
      buttonStopped = FALSE;
   }
	
}

void calculateDemandTension (void)
{
   if (buffer.reading.infeedSpeed>0.1 || buffer.reading.outfeedSpeed>0.1) {
      if (rabs(buffer.reading.infeedSpeed - buffer.reading.outfeedSpeed)<2.0)
         buffer.reading.autoTension = buffer.setting.innerLoopAutoConstantFactor + rabs(buffer.reading.infeedSpeed - buffer.reading.outfeedSpeed) * buffer.setting.innerLoopAutoSpeedDiffFactor;
      else
         buffer.reading.autoTension = buffer.setting.innerLoopAutoConstantFactor + 2.0 * buffer.setting.innerLoopAutoSpeedDiffFactor;
   }
   else
      buffer.reading.autoTension = buffer.setting.innerLoopAutoConstantFactor/2;
}

void addError(USINT err, UINT subType)
{
   if (err) {
      state = error;
      machineError.active = TRUE;
      machineError.errType = (UINT)(err) + 2000;
      machineError.errSubType = subType;
      addErrorToBufferLastError(err,subType);
   }
}

BOOL activateInfeed(void)
{
   static int wait = 10;
   static int ErrAccCnt;
   //static int homeOkCheckCnt;
   if (homeInfeedState>4) homeInfeedState = 0;
   if (bInfeed.Status.DriveStatus.HomingOk && !bufInfeed.homed) {
      bufInfeed.homed = TRUE;
      bInfeed.Command.Stop = OFF;
   }
   if (!bufInfeed.homed) {
      switch (homeInfeedState) {
         case 0:
            ErrAccCnt = 0;
            homeInfeedState = 1;
            bInfeed.Command.Power = OFF;
            bInfeed.Command.ErrorAcknowledge = ON;
         //homeOkCheckCnt=0;
         case 1:
            bInfeed.Command.Halt = ON;
            bInfeed.Command.Stop = OFF;
            bInfeed.Command.MoveAbsolute = OFF;
            bInfeed.Command.MoveAdditive = OFF;
            bInfeed.Command.MoveJogNeg = OFF;
            bInfeed.Command.MoveJogPos = OFF;
            bInfeed.Command.MoveVelocity = OFF;
            homeInfeedState = 2;
            bufInfeed.homed = 0;
            bInfeed.Command.Power = ON;
            bInfeed.Command.ErrorAcknowledge = OFF;
            break;
         case 2:
            bInfeed.Command.ErrorAcknowledge = OFF;
            bInfeed.Command.Halt = OFF;
            homeInfeedState = 3;
            bufInfeed.homed = 0;
            wait = 20;
            break;
         case 3:
            if (bInfeed.Status.ErrorID != 0 && ErrAccCnt<10) {// movement before homing attempt
               if (wait == 10) {
                  bInfeed.Command.Home = OFF;
                  bInfeed.Command.ErrorAcknowledge = ON;
                  ErrAccCnt++;
               } 
               if (wait == 0) homeInfeedState = 1;
               wait--;
            }
            else {
               bInfeed.Command.Home = ON;
               bInfeed.Command.Stop = OFF;
               bInfeed.Command.ErrorAcknowledge = OFF;
               bInfeed.Command.Halt = OFF;
               homeInfeedState = 4;
            }
            break;
         case 4:
            if (bInfeed.Status.DriveStatus.HomingOk == 1) {
               bufInfeed.homed = 1;
               bInfeed.Command.MoveVelocity=ON;
               bInfeed.Command.ErrorAcknowledge = OFF;
            }	
            else if (bInfeed.Status.DriveStatus.AxisError == 1 && ErrAccCnt<10) {
				
               bufInfeed.homed = 0;
               homeInfeedState = 0;
               bInfeed.Command.Power = OFF;
            }
            break;
         default:
            break;
      }
   }
   if (bufInfeed.homed) {
      homeInfeedState = 0;
      return TRUE;
   }
   if (ErrAccCnt>5) {
      homeInfeedState = 99;
      return TRUE; // home failed
   }
   return FALSE;
}

BOOL autoLoad(void)
{
   resetDecurl();
   if (!activateInfeed()) {
      autoLoadState++;
      if (autoLoadState>200) {
         addError(INFEED_AXIS_ERROR,1);
         autoLoadState=0;
      }
      return FALSE;
   }
   autoLoadState = 2;
   if (stdErrCheck()) 				return FALSE;
   autoLoadState = 3;
   if (innerLoop(DOWN) != DONE) 	return FALSE;
   autoLoadState = 0;
   //buffer.setting.infeedOperation = loopControl;
   startRunSlowCnt = 3000;
   return TRUE;
}


void getInfeed (void) 
{
   //buffer.reading.input.prototypeVersion;
   bufInfeed.actualSpeed = bInfeed.Status.ActVelocity / 1000;
   if (bInfeed.Status.ErrorID != 0) {
      bufInfeed.error = 1;
   }
   else if (bInfeed.AxisState.ErrorStop)
      bufInfeed.error = 1;
   else 
      bufInfeed.error = 0;
   bInfeed.Parameter.Acceleration = bufInfeed.acceleration * 1000;
   bInfeed.Parameter.Deceleration = bufInfeed.deceleration * 1000;
   bufInfeed.actualPosition = bInfeed.Status.ActPosition;
}

void setInfeed (void) {
   REAL spd;
   static USINT stopTimeOut;
   static dir lastDir;
   if (bufInfeed.homed) {
      if (lastDir != bufInfeed.direction)
      {
         bInfeed.Command.MoveVelocity=OFF;
         lastDir = bufInfeed.direction;
      }
      else 
      {
         if (bufInfeed.speed > bufInfeed.maxSpeed) bufInfeed.speed = bufInfeed.maxSpeed;
         else if (bufInfeed.speed < bufInfeed.minSpeed) bufInfeed.speed = bufInfeed.minSpeed;
         spd = bufInfeed.speed;
         if (spd < 0.0) {
            spd = (-1) * spd;
            bufInfeed.direction = bInfeed.Parameter.Direction = 1;
         }
         else bufInfeed.direction = bInfeed.Parameter.Direction = 0;
	
         if (spd > 0 && bufInfeed.lastSpeed != bufInfeed.speed) {
            bInfeed.Parameter.Velocity = spd*1000.0;
            bInfeed.Command.MoveVelocity=ON;
            bInfeed.Command.Power=ON;
            stopTimeOut = 0;
         }
         else {
            bInfeed.Command.MoveVelocity=ON;
            bInfeed.Command.Power=ON;
            if (bufInfeed.speed == 0) {
               bInfeed.Parameter.Velocity = 0;
               if (stopTimeOut < 150) stopTimeOut++;
               if (stopTimeOut>100 && bufInfeed.actualSpeed>0.015) {
                  bInfeed.Parameter.Velocity = 0.02;
                  stopTimeOut=0;
               }
            }
         }	
         bufInfeed.lastSpeed = bufInfeed.speed;
      }
   }
}

void setButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
   if (btnNo == 1) {
      b50button.color1.green 	= grn;
      b50button.color1.red   	= red;
      b50button.color1.yellow  = yel;
      b50button.color1.white 	= whtBlu;
      if      (b50button.color1.yellow && b50button.color2.red) buffer.reading.button1 = _ORANGE;
      else if (b50button.color1.yellow)                         buffer.reading.button1 = _YELLOW;
      else if (b50button.color1.red)                            buffer.reading.button1 = _RED;
      else if (b50button.color1.green)                          buffer.reading.button1 = _GREEN;
      else if (b50button.color1.white)                          buffer.reading.button1 = _WHITE;
      else                                                      buffer.reading.button1 = _OFF;
   }
   if (btnNo == 2) {
      b50button.color2.green 	= grn;
      b50button.color2.red   	= red;
      b50button.color2.yellow  = yel;
      b50button.color2.white 	= whtBlu;
      if      (b50button.color2.yellow && b50button.color2.red) buffer.reading.button2 = _ORANGE;
      else if (b50button.color2.yellow)                         buffer.reading.button2 = _YELLOW;
      else if (b50button.color2.red)                            buffer.reading.button2 = _RED;
      else if (b50button.color2.green)                          buffer.reading.button2 = _GREEN;
      else if (b50button.color2.white)                          buffer.reading.button2 = _WHITE;
      else                                                      buffer.reading.button2 = _OFF;
   }
   if (btnNo == 3) {
      b50button.color3.green 	= grn;
      b50button.color3.red   	= red;
      b50button.color3.yellow  = yel;
      b50button.color3.white 	= whtBlu;
      if      (b50button.color3.yellow && b50button.color2.red) buffer.reading.button3 = _ORANGE;
      else if (b50button.color3.yellow)                         buffer.reading.button3 = _YELLOW;
      else if (b50button.color3.red)                            buffer.reading.button3 = _RED;
      else if (b50button.color3.green)                          buffer.reading.button3 = _GREEN;
      else if (b50button.color3.white)                          buffer.reading.button3 = _WHITE;
      else                                                      buffer.reading.button3 = _OFF;
   }
   if (btnNo == 4) {
      b50button.color4.green 	= grn;
      b50button.color4.red   	= red;
      b50button.color4.yellow  = yel;
      b50button.color4.white 	= whtBlu;
      if      (b50button.color4.yellow && b50button.color2.red) buffer.reading.button4 = _ORANGE;
      else if (b50button.color4.yellow)                         buffer.reading.button4 = _YELLOW;
      else if (b50button.color4.red)                            buffer.reading.button4 = _RED;
      else if (b50button.color4.green)                          buffer.reading.button4 = _GREEN;
      else if (b50button.color4.white)                          buffer.reading.button4 = _WHITE;
      else                                                      buffer.reading.button4 = _OFF;
   }
   if (btnNo == 5) {
      b50button.color5.green 	= grn;
      b50button.color5.red   	= red;
      b50button.color5.yellow  = yel;
      b50button.color5.blue 	= whtBlu;
      if (b50button.color5.yellow && b50button.color5.red)      buffer.reading.button5 = _ORANGE;
      else if (b50button.color5.blue && b50button.color5.red)   buffer.reading.button5 = _MAGENTA;
      else if (b50button.color5.green && b50button.color5.blue) buffer.reading.button5 = _TURQUOISE;
      else if (b50button.color5.yellow)                         buffer.reading.button5 = _YELLOW;
      else if (b50button.color5.red)                            buffer.reading.button5 = _RED;
      else if (b50button.color5.green)                          buffer.reading.button5 = _GREEN;
      else if (b50button.color5.blue)                           buffer.reading.button5 = _BLUE;
      else                                                      buffer.reading.button5 = _OFF;
   }	
}

BOOL isButton(USINT btnNo, BOOL grn, BOOL red, BOOL yel, BOOL whtBlu)
{
   if (btnNo == 1) {
      if (   grn == b50button.color1.green 
         && red == b50button.color1.red 
         && yel == b50button.color1.yellow
         && whtBlu == b50button.color1.white) return 1;
      if (   grn && red && yel && whtBlu 
         && (b50button.color1.green || b50button.color1.red || b50button.color1.yellow || b50button.color1.white)) return 1;
      return 0;
   }
   if (btnNo == 2)	{
      if (   grn == b50button.color2.green 
         && red == b50button.color2.red 
         && yel == b50button.color2.yellow
         && whtBlu == b50button.color2.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (b50button.color2.green || b50button.color2.red || b50button.color2.yellow || b50button.color2.white)) return 1;
      return 0;
   }
   if (btnNo == 3)	{
      if (   grn == b50button.color3.green 
         && red == b50button.color3.red 
         && yel == b50button.color3.yellow
         && whtBlu == b50button.color3.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (b50button.color3.green || b50button.color3.red || b50button.color3.yellow || b50button.color3.white)) return 1;
      return 0;
   }
   if (btnNo == 4)	{
      if (   grn == b50button.color4.green 
         && red == b50button.color4.red 
         && yel == b50button.color4.yellow
         && whtBlu == b50button.color4.white) return 1;
      if (  grn && red && yel && whtBlu 
         && (b50button.color4.green || b50button.color4.red || b50button.color4.yellow || b50button.color4.white)) return 1;
      return 0;
   }
   if (btnNo == 5)	{
      if (   grn == b50button.color5.green 
         && red == b50button.color5.red 
         && yel == b50button.color5.yellow
         && whtBlu == b50button.color5.blue) return 1;
      if (   grn && red && yel && whtBlu 
         && (b50button.color5.green || b50button.color5.red || b50button.color5.yellow || b50button.color5.blue)) return 1;
      return 0;
   }
   return 0;
}

void addErrorToBufferLastError(USINT typ,USINT subTyp)
{
   if (buffer.reading.lastErrors.pointer<9) buffer.reading.lastErrors.pointer++;
   else buffer.reading.lastErrors.pointer = 0;
   switch (typ)
   {
      case INFEED_PRESSURE_WHEEL:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Infeed pressure wheel");
         break;
      case OUTFEED_PRESSURE_WHEEL:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Outfeed pressure wheel");
         break;
      case NO_PRESSURE:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"No air pressure");
         break;
      case INFEED_OVERSPEED:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Infeed over speed");
         break;
      case INFEED_AXIS_ERROR:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Infeed Axis Error");
         break;
      case INFEED_PAPER_END:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Infeed paper end");
         break;
      case B50_E_STOP:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"e-stop");
         break;
      case OVERFULL:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Infeed buffer overfull");
         break;
      case LOOP_UP_FAILURE:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Loop up timeout");
         break;
      case LOOP_DOWN_FAILURE:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Loop down timeout");
         break;
      case INNER_LOOP_FAULT:
         strcpy(buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].text,"Inner loop fault");
         break;
      default:
         break;
   }
   buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].typ = typ;
   buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].subTyp = subTyp;
   buffer.reading.lastErrors.error[buffer.reading.lastErrors.pointer].timeStamp = lineController.reading.timeSinceStart;
}


void setAccOrDec(REAL speed)
{
   static REAL lastSpeed[10];
   static USINT lp;
   int x;
   buffer.reading.infeedAcc = FALSE;
   buffer.reading.infeedDec = FALSE;

   x += speed > lastSpeed[9] ? 1 : -1;
   x += speed > lastSpeed[8] ? 1 : -1;
   x += speed > lastSpeed[7] ? 1 : -1;
   x += speed > lastSpeed[6] ? 1 : -1;
   x += speed > lastSpeed[5] ? 1 : -1;
   x += speed > lastSpeed[4] ? 1 : -1;
   x += speed > lastSpeed[3] ? 1 : -1;
   x += speed > lastSpeed[2] ? 1 : -1;
   x += speed > lastSpeed[1] ? 1 : -1;
   x += speed > lastSpeed[0] ? 1 : -1;
   
   lastSpeed[lp] = speed;
   lp++;
   if (lp>=10) lp=0;
   
   if (x > 2) buffer.reading.infeedAcc = TRUE;
   if (x < -2) buffer.reading.infeedDec = TRUE;
}
   

   

		



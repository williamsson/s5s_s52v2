
#include <bur/plctypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF; 

#define STACKER      0
#define CUTTER       1
#define MERGER       2
#define FOLDER       3
#define BUFFER       4

#define TRUE         1
#define FALSE        0

#define MAX_RECIPE_NO                  10
#define MAXIMUM_PAPER_AMOUNT           14.42
//speed conversions
#define FROM__m_min__TO__m_s                0.0166667
#define FROM__m_s__TO__m_min                60.0
#define FROM__ft_m__TO__m_s                 0.00508
#define FROM__mm_s__TO__m_s                 0.001
#define FROM__mm_s__TO__m_min               0.06
#define FROM__m_min__TO__mm_s               16.6667
#define FROM__m_s__TO__mm_s                 1000
#define FROM__m_s__TO__ft_m                 196.8504

#define M_MIN                               1
#define M_S									2
#define MM_S								3
#define FT_M                                4
//length conversions
#define FROM__m__TO__mm                     1000
#define FROM__f__TO__mm                     304.8
#define FROM__inch__TO__mm                  25.4
#define FROM__inch6__TO__mm                 4.23333
#define FROM__mm__TO__m                     0.001
#define FROM__mm__TO__f                     0.00328084
#define FROM__mm__TO__inch                  0.03937008
#define FROM__mm__TO__inch6                 0.23622049

#define M                                   1
#define MM                                  2
#define F                                   3
#define INCH                                4
#define INCH_6                              5

#define GUI_SPEED_FORMAT                    M_MIN

REAL lengthConversion(REAL inputValue, USINT inputUnit, USINT outputUnit)
{
   REAL tmp_mm; // temp mm
   if (inputUnit == outputUnit) return inputValue;
   switch(inputUnit)
   {
      case M:
         tmp_mm = inputValue*FROM__m__TO__mm;
	  break;
	  case F:
	     tmp_mm = inputValue*FROM__f__TO__mm;
	  break;
	  case INCH:
	     tmp_mm = inputValue*FROM__inch__TO__mm;
	  break;
	  case INCH_6:
	     tmp_mm = inputValue*FROM__inch6__TO__mm;
	  break;
	  default:
	     tmp_mm = inputValue;
	  break;
   }
   
   switch(outputUnit)
   {
      case M:
         return tmp_mm*FROM__mm__TO__m;
	  break;
	  case F:
	     return tmp_mm*FROM__mm__TO__f;
	  break;
	  case INCH:
	     return tmp_mm*FROM__mm__TO__inch;
	  break;
	  case INCH_6:
	     return tmp_mm*FROM__mm__TO__inch6;
	  break;
	  default:
	     return tmp_mm;
	  break;
   }
}
                  
REAL speedConversion(REAL inputValue, USINT inputUnit, USINT outputUnit)
{
   REAL tmp_ms; // temp m/s
   if (inputUnit == outputUnit) return inputValue;
   switch(inputUnit)
   {
      case M_MIN:
         tmp_ms = inputValue*FROM__m_min__TO__m_s;
	  break;
	  case MM_S:
	     tmp_ms = inputValue*FROM__mm_s__TO__m_s;
	  break;
	  case FT_M:
	     tmp_ms = inputValue*FROM__ft_m__TO__m_s;
	  break;
	  default:
	     tmp_ms = inputValue;
	  break;
   }
   
   switch(outputUnit)
   {
      case M_MIN:
         return tmp_ms*FROM__m_s__TO__m_min;
	  break;
	  case MM_S:
	     return tmp_ms*FROM__m_s__TO__mm_s;
	  break;
	  case FT_M:
	     return tmp_ms*FROM__m_s__TO__ft_m;
	  break;
	  default:
	     return tmp_ms;
	  break;
   }
}

USINT lastRECIPE;

REAL limitsD(REAL input, REAL max, REAL min, REAL def)
{
	if (input>max) return def;
	if (input<min) return def;
	return input;
}

void _INIT ProgramInit(void)
{
	// Insert code here
	int i;
	if (GUI.currentRECIPE > MAX_RECIPE_NO) GUI.currentRECIPE = 0;
	lastRECIPE = GUI.currentRECIPE; 
	//check for junk
	for (i=0;i<MAX_RECIPE_NO;i++)
	{
	   RECIPE[i].B50_decurlTime           = (short unsigned int)(limitsD(RECIPE[i].B50_decurlTime,3000,0,40));
	   RECIPE[i].B50_maxInfeedSpeed       = limitsD(RECIPE[i].B50_maxInfeedSpeed,300,50,190);
	   RECIPE[i].B50_pauseLevel           = limitsD(RECIPE[i].B50_pauseLevel,100,0,70);
	   RECIPE[i].B50_slowSpeedLevel       = limitsD(RECIPE[i].B50_slowSpeedLevel,100,0,50);
	   RECIPE[i].B50_stopLevel            = limitsD(RECIPE[i].B50_stopLevel,100,0,90);
	   //RECIPE[i].C50_maxSpeed             = limitsD(RECIPE[i].C50_maxSpeed,250,6,50);
	   RECIPE[i].S50_alignerMinSpeed      = limitsD(RECIPE[i].S50_alignerMinSpeed,80,5,45);
	   //RECIPE[i].S50_automaticTableSteps  = (unsigned char)(limitsD(RECIPE[i].S50_automaticTableSteps,3,0,1));
	   RECIPE[i].S50_backStopOffset       = limitsD(RECIPE[i].S50_backStopOffset,10,-10,0);
	   RECIPE[i].S50_deliverPosition      = limitsD(RECIPE[i].S50_deliverPosition,200,-5,0);
	   RECIPE[i].S50_deliverSpeed         = limitsD(RECIPE[i].S50_deliverSpeed,100,2,12);
	   RECIPE[i].S50_fingerTopPosition    = limitsD(RECIPE[i].S50_fingerTopPosition,100,-100,0);
	   //RECIPE[i].S50_maxStackSize         = limitsD(RECIPE[i].S50_maxStackSize,500,30,200);
	   //RECIPE[i].S50_minStackSize         = limitsD(RECIPE[i].S50_minStackSize,300,20,100);
	   RECIPE[i].S50_overSpeedFactor      = limitsD(RECIPE[i].S50_overSpeedFactor,2,1,1.32);
	   RECIPE[i].S50_sheets               = (unsigned char)(limitsD(RECIPE[i].S50_sheets,4,1,1));
	   RECIPE[i].S50_stepDownLevel        = (unsigned char)(limitsD(RECIPE[i].S50_stepDownLevel,30800,29900,30500));
	   RECIPE[i].S50_stopTime             = limitsD(RECIPE[i].S50_stopTime,2,0.1,0.6);
	   RECIPE[i].S50_tableSteps           = limitsD(RECIPE[i].S50_tableSteps,0.5,0.01,0.1);
	   RECIPE[i].S50_topPosition          = limitsD(RECIPE[i].S50_topPosition,10,-10,-6.0);
	   //RECIPE[i].C50_format               = limitsD(RECIPE[i].C50_format,1000,100,305);
	   //RECIPE[i].C50_RegMarkOffset        = limitsD(RECIPE[i].C50_RegMarkOffset,1000,-1000,0);
	} 
	GUI.common.B50_infeedBottomPosition = (short int)(limitsD(GUI.common.B50_infeedBottomPosition,32767,25000,32000));
	GUI.common.B50_infeedStopPosition   = (short int)(limitsD(GUI.common.B50_infeedStopPosition,5000,100,650));
	GUI.common.B50_innerLoopBottom      = (short int)(limitsD(GUI.common.B50_innerLoopBottom,8000,0,4500));
	GUI.common.B50_innerLoopTop         = (short int)(limitsD(GUI.common.B50_innerLoopTop,35000,28000,32390));
   GUI.settings = RECIPE[GUI.currentRECIPE];
}

BOOL outputTest (BOOL *fEna, BOOL *fVal, BOOL *Output, BOOL Force, BOOL Value)
{
   *fEna = Force;
   *fVal = Value;
   return *Output;
}

REAL limits(REAL input, REAL max, REAL min)
{
	if (input>max) return max;
	if (input<min) return min;
	return input;
}

INT lowest(INT a, INT b, INT c)
{
	if (a<b)
	{
		if (a<c) return a;
		return c;
	}
	else
	{
		if (b<c) return b;
		return c;
	}
}

USINT usintTmp;

void _CYCLIC ProgramCyclic(void)
{
   static INT lastStepSensor[4];
   static char debugText[100];
   static BOOL lastInputTestStackerResult;
   static BOOL lastInputTestFolderResult;
   static BOOL lastInputTestBufferResult;
   static BOOL lastInputTestMergerResult;
   static BOOL lastInputTestCutterResult;
   static BOOL oldValveDown,oldValveUp;
   
	
   GUI.maxRECIPE = MAX_RECIPE_NO;
	
   if (TRUE == GUI.copyRECIPE)
   {  
      RECIPE[GUI.copyTo] = RECIPE[GUI.copyFrom];
      GUI.copyRECIPE = FALSE;
      sprintf(debugText, "*GuiCom: copy RECIPE from %d to %d",GUI.copyFrom,GUI.copyTo);
      strcpy(ULog.text[ULog.ix++], debugText);	
   }
	
   if (GUI.currentRECIPE > MAX_RECIPE_NO) GUI.currentRECIPE = 0;
   if (lastRECIPE != GUI.currentRECIPE) {
      GUI.settings = RECIPE[GUI.currentRECIPE];
      sprintf(debugText, "*GuiCom: change RECIPE from %d to %d",lastRECIPE,GUI.currentRECIPE);
      strcpy(ULog.text[ULog.ix++], debugText);
   } 
   //readings
   //GUI.readings.X50_SpeedLimit           = speedConversion(SpeedLimit,MM_S,GUI_SPEED_FORMAT);
   
   //GUI.readings.C50_CurrentSpeed         = speedConversion(Master[0].Status.ActVelocity,MM_S,GUI_SPEED_FORMAT);
   //GUI.readings.B50_InfeedSpeed          = speedConversion(buffer.reading.infeedSpeed,M_S,GUI_SPEED_FORMAT);
   
   GUI.readings.B50_LoopUsage            = buffer.reading.usage;
   GUI.readings.S50_stackSize            = stacker.reading.stackSize;
	/*GUI.readings.B50_InfeedLoopPosition   = (io.analogIn.bufferOuterLoop*15+GUI.readings.B50_InfeedLoopPosition*85)/100;
	GUI.readings.B50_InnerLoopPosition    = (io.analogIn.bufferInnerLoop*15+GUI.readings.B50_InnerLoopPosition*85)/100;*/
	
   GUI.readings.B50_InfeedLoopPosition   = (buffer.reading.input.infeedLoop*15+GUI.readings.B50_InfeedLoopPosition*85)/100;
   GUI.readings.B50_InnerLoopPosition    = (buffer.reading.input.innerLoop*15+GUI.readings.B50_InnerLoopPosition*85)/100;
	
   GUI.readings.S50_StepSensor           = (REAL)((lowest(lastStepSensor[3],lastStepSensor[2],lastStepSensor[1]) 
      + lowest(lastStepSensor[0],lastStepSensor[1],lastStepSensor[2]) 
      + lowest(stacker.reading.input.AnalogStepSensor,lastStepSensor[3],lastStepSensor[2]) 
      + stacker.reading.input.AnalogStepSensor + 2)/4);
   GUI.readings.S50_StepSensor           = (GUI.readings.S50_StepSensor - 16383)/5461;
   topOfStackDistance = ((REAL)(GUI.readings.S50_StepSensor - stepDownLevel))/(-100.0);
   if ((GUI.readings.S50_TopOfStackDistance - topOfStackDistance)>1 && (GUI.readings.S50_TopOfStackDistance - topOfStackDistance)<-1) GUI.readings.S50_TopOfStackDistance = topOfStackDistance;
   else GUI.readings.S50_TopOfStackDistance   = (topOfStackDistance + GUI.readings.S50_TopOfStackDistance)/2;  
   lastStepSensor[0] = lastStepSensor[1];
   lastStepSensor[1] = lastStepSensor[2];
   lastStepSensor[2] = lastStepSensor[3];
   lastStepSensor[3] = stacker.reading.input.AnalogStepSensor;
   //settings
   //buffer.setting.decurlTime             = (short unsigned int)(limits(GUI.settings.B50_decurlTime,3000,5));
   //buffer.setting.maxInfeedSpeed             = GUI.settings.B50_maxInfeedSpeed*FROM__m_min__TO__m_s;
   /*buffer.setting.pauseLevel             = (limits(GUI.settings.B50_pauseLevel,100,0)*(REAL)MAXIMUM_PAPER_AMOUNT)/100;
   buffer.setting.slowLevel              = (limits(GUI.settings.B50_slowSpeedLevel,100,0)*(REAL)MAXIMUM_PAPER_AMOUNT)/100;
   buffer.setting.stopLevel              = (limits(GUI.settings.B50_stopLevel,100,0)*(REAL)MAXIMUM_PAPER_AMOUNT)/100;*/
   //stacker.setting.deliverTransportSpeed    = limits(GUI.settings.S50_deliverSpeed,120,6)*FROM__m_min__TO__m_s;
   //stacker.setting.maxStackSize             = limits(GUI.settings.S50_maxStackSize,stacker.reading.maxStackSize,20);
   //stacker.setting.transportTableSpeed      = limits(GUI.settings.S50_overSpeedFactor,2.0,1.25);
   //stacker.setting.tableSteps               = limits(GUI.settings.S50_tableSteps,0.5,0.05);
   //stacker.setting.topPosOffset             = limits(GUI.settings.S50_topPosition,3.0,-1.0);
   //stacker.setting.maxStackSize             = limits(GUI.settings.S50_minStackSize,200,20);
   //stacker.setting.backStop                 = limits(GUI.settings.S50_backStopOffset,400,-10);
   //stacker.setting.transportIdleSpeed       = limits(GUI.settings.S50_alignerMinSpeed,120,6)*FROM__m_min__TO__m_s;
   //setting.deliverStopTime               = limits(GUI.settings.S50_stopTime,5.0,0.0);
   //cutter.setting.infeedRunSpeed                        = limits(cutter.setting.maxSpeed,250,0)*FROM__m_min__TO__mm_s;
   //cutter.setting.format                                = limits(cutter.setting.format,1000,100);
   //stacker.setting.useStepSensor         = (unsigned char)(limits(GUI.settings.S50_automaticTableSteps,3,0));
   if (stacker.setting.useStepSensor == 3) grip.setup.mechStep.set = 1;
   else grip.setup.mechStep.set = 0;
   stepDownLevel                         = (short int)(limits(GUI.settings.S50_stepDownLevel,30800,29900));
   //common settings
   //infeedLoop.topPos                     = (short int)(limits(GUI.common.B50_infeedStopPosition,5000,100));
   //infeedLoop.bottomPos                  = (short int)(limits(GUI.common.B50_infeedBottomPosition,32767,25000));
   //buffer.setting.innerLoopTop           = (short int)(limits(GUI.common.B50_innerLoopTop,35000,28000));
   //buffer.setting.innerLoopBottom            = (short int )(limits(GUI.common.B50_innerLoopBottom,8000,0));
   //stacker.setting.deliveryPosition      = limits(GUI.settings.S50_deliverPosition,100,-55);
   //input test
   /*	if (GUI.settings.S50_sheets != step.sheets)
   {
   if (step.sheets == 1)
   {
   if (GUI.settings.S50_sheets == 2) step.tableSteps = step.tableSteps*2;
   if (GUI.settings.S50_sheets == 3) step.tableSteps = step.tableSteps*3;
   }
   if (step.sheets == 2)
   {
   if (GUI.settings.S50_sheets == 1) step.tableSteps = step.tableSteps*0.5;
   if (GUI.settings.S50_sheets == 3) step.tableSteps = step.tableSteps*1.5;
   }
   if (step.sheets == 3)
   {
   if (GUI.settings.S50_sheets == 1) step.tableSteps = step.tableSteps*0.333333;
   if (GUI.settings.S50_sheets == 2) step.tableSteps = step.tableSteps*0.666667;
   }
   if (GUI.settings.S50_sheets == 1) step.tableSteps = limits(step.tableSteps,1.8,0.4);
   if (GUI.settings.S50_sheets == 2) step.tableSteps = limits(step.tableSteps,3.2,0.8);
   if (GUI.settings.S50_sheets == 3) step.tableSteps = limits(step.tableSteps,4.2,1.4);
   if (GUI.settings.S50_sheets == 4) step.tableSteps = limits(step.tableSteps,5.3,2);
   step.sheets = (unsigned char)(limits(GUI.settings.S50_sheets,4,1));
   } */
   //Input test
   if (lineController.setup.stacker.installed)
   {
      switch (InputTestStackerIndex)
      {
         case 0:
            InputTestStackerResult = stacker.reading.input.rackGate;
            break;
         case 1:
            InputTestStackerResult = stacker.reading.input.tableClearSensor1;
            break;
         case 2:
            InputTestStackerResult = stacker.reading.input.tableClearSensor2;
            break;
         case 3:
            InputTestStackerResult = stacker.reading.input.tableClearSensor3;
            break;
         case 4:
            InputTestStackerResult = stacker.reading.input.tableClearSensor4;
            break;
         case 5:
            InputTestStackerResult = stacker.reading.input.enterTransport; //
            break;
         case 6:
            InputTestStackerResult = stacker.reading.input.exitTransport1; 
            break;
         case 7:
            InputTestStackerResult = stacker.reading.input.homeElevator;
            break;
         case 8:
            InputTestStackerResult = stacker.reading.input.homeForkLift;
            break;
         case 9:
            InputTestStackerResult = stacker.reading.input.homeForkPush;
            break;
         case 10:
            InputTestStackerResult = stacker.reading.input.homeEndStop;
            break;
         case 11:
            InputTestStackerResult = stacker.reading.input.lightCurtain;
            break;
         case 12:
            InputTestStackerResult = stacker.reading.input.readyGripper;
            break;
         case 13:
            InputTestStackerResult = stacker.reading.input.wasteBoxFull;
            break;
         case 14:
            InputTestStackerResult = stacker.reading.input.wasteBoxWarning;
            break;
         case 15:
            InputTestStackerResult = stacker.reading.input.seqUnitNotReady;
            break;
         case 16:
            InputTestStackerResult = stacker.reading.input.infeedGate; //
            break; 
         case 17:
            InputTestStackerResult = stacker.reading.input.userInput1; //
            break;  
         case 18:
            InputTestStackerResult = stacker.reading.input.userInput2; //
            break;  
         case 19:
            InputTestStackerResult = stacker.reading.input.userInput3; //
            break;  
         case 20:
            InputTestStackerResult = stacker.reading.input.topCoverClosed; //
            break;  
         case 21:
            InputTestStackerResult = stacker.reading.input.antistaticOk; //
            break; 
         case 22:
            InputTestStackerResult = 0;
            break;
         default:
            InputTestStackerResult = stacker.reading.input.lightCurtain;
            break;
      }
   }
   else if (lineController.setup.streamer.installed)
   {
      switch (InputTestStackerIndex)
      {
      case 0:
        InputTestStackerResult = stacker.reading.input.enterTransport;
      break;
      case 1:
         InputTestStackerResult = stacker.reading.input.homeForkPush;
      break;
      case 2:
         InputTestStackerResult = stacker.reading.input.wasteBoxWarning;
      break;
      case 3:
         InputTestStackerResult = stacker.reading.input.homeForkLift;
      break;
      case 4:
         InputTestStackerResult = stacker.reading.input.wasteBoxFull;
      break;
      case 5:
         InputTestStackerResult = stacker.reading.input.homeElevator; //
      break;
      case 6:
         InputTestStackerResult = stacker.reading.input.rackGate; 
      break;
      case 7:
         InputTestStackerResult = stacker.reading.input.streamerGap1;
      break;
      case 8:
         InputTestStackerResult = stacker.reading.input.antistaticOk;
      break;
      case 9:
         InputTestStackerResult = stacker.reading.input.streamerGap2;
      break;
      case 10:
         InputTestStackerResult = stacker.reading.input.homePosJogger1;
      break;
      case 11:
         InputTestStackerResult = stacker.reading.input.streamerGap3;
      break;
      case 12:
         InputTestStackerResult = stacker.reading.input.homePosJogger2;
      break;
      case 13:
         InputTestStackerResult = stacker.reading.input.tableClearSensor1;
      break;
      case 14:
         InputTestStackerResult = stacker.reading.input.exitStreamerSection2;
      break;
      case 15:
         InputTestStackerResult = stacker.reading.input.tableClearSensor2;
      break;
      case 16:
         InputTestStackerResult = stacker.reading.input.exitStreamerSection3;
      break; 
      case 17:
         InputTestStackerResult = stacker.reading.input.tableClearSensor3; //
      break;  
      case 18:
         InputTestStackerResult = stacker.reading.input.streamerCrashRight; //
      break;  
      case 19:
         InputTestStackerResult = stacker.reading.input.tableClearSensor4; //
      break;  
      case 20:
         InputTestStackerResult = stacker.reading.input.streamerCrashLeft; //
      break;  
      case 21:
         InputTestStackerResult = stacker.reading.input.seqUnitNotReady; //
      break; 
      case 22:
         InputTestStackerResult = stacker.reading.input.manFeed; 
      break;
      case 23:
         InputTestStackerResult = stacker.reading.input.alignerOutfeedDigital1;
      break;
      case 24:
         InputTestStackerResult = stacker.reading.input.userInput1;
      break;
      case 25:
         InputTestStackerResult = stacker.reading.input.alignerOutfeedDigital2;
      break;
      case 26:
         InputTestStackerResult = stacker.reading.input.userInput2;
      break;
      case 27:
         InputTestStackerResult = stacker.reading.input.exitStreamerSection1;
      break;
      case 28:
         InputTestStackerResult = stacker.reading.input.userInput3;
         break;
      case 29:
         InputTestStackerResult =  stacker.reading.input.streamerGate;
         break;
      case 30:
         InputTestStackerResult = stacker.reading.input.pressUnitSensor;
         break; 
      case 31:
        InputTestStackerResult = stacker.reading.input.offsetMarkReader;
      break;      
      case 32:
         InputTestStackerResult = stacker.reading.input.lightCurtain;
         break;  
      case 33:
         InputTestStackerResult = stacker.reading.input.eStop;
         break;  
      case 34:
         InputTestStackerResult = stacker.reading.input.topCoverClosed;
         break;  
      case 35:
         InputTestStackerResult = stacker.reading.input.eStopExt;
         break;  
      case 36:
         InputTestStackerResult = stacker.reading.input.stackerHatch;
         break;  
      case 37:
         InputTestStackerResult = stacker.reading.input.ebmEncoder;
         break;
      case 38:
         InputTestStackerResult = stacker.reading.input.glueOffsetMarkSensor;
         break;
      case 39:
         InputTestStackerResult = stacker.reading.input.pressureGuard;
         break;
      default:
         InputTestStackerResult = stacker.reading.input.lightCurtain;
      break;
      }
   }
   
   switch (InputTestCutterIndex)
   {
      case 0:
         InputTestCutterResult = cutter.reading.input.frontDoor;
         break;
      case 1:
         InputTestCutterResult = cutter.reading.input.frontDoorKnife;
         break;
      case 2:
         InputTestCutterResult = cutter.reading.input.topCover;
         break;
      case 3:
         InputTestCutterResult =  cutter.reading.input.knifeModule;
         break;
      case 4:
         InputTestCutterResult = cutter.reading.input.printerBlowerOn;
         break;
      case 5:
         InputTestCutterResult = cutter.reading.input.feedButton;
         break;
      case 6:
         InputTestCutterResult = cutter.reading.input.printerTensionControl;
         break;
      case 7:
         InputTestCutterResult = cutter.reading.input.markReader1;
         break;
      case 8:
         InputTestCutterResult = cutter.reading.input.markReader2;
         break;
      case 9:
         InputTestCutterResult = cutter.reading.input.knifeOutfeedCrash1;
         break;
      case 10:
         InputTestCutterResult = cutter.reading.input.knifeOutfeedCrash2;
         break;
      case 11:
         InputTestCutterResult = cutter.reading.input.knifeOutfeedCrash3;
         break;
      case 12:
         InputTestCutterResult = cutter.reading.input.extractionCrash;
         break;
      default:
         break;
   }

   
   switch (InputTestMergerIndex)
   {
      case 0:
         InputTestMergerResult = 0;
         break;
      default:
         break;
   }
   
   switch (InputTestFolderIndex)
   {
      case 0:
         InputTestFolderResult = folder.reading.input.infeedTopCover;
         break;
      case 1:
         InputTestFolderResult = folder.reading.input.outfeedTopCover;
         break;
      case 2:
         InputTestFolderResult = folder.reading.input.rightFrontDoor;
         break;
      case 3:
         InputTestFolderResult = folder.reading.input.feedAllButton;
         break;
      case 4:
         InputTestFolderResult = folder.reading.input.webWatch1;
         break;
      case 5:
         InputTestFolderResult = folder.reading.input.webWatch2;
         break;
      case 6:
         InputTestFolderResult = folder.reading.input.webWatch3;
         break;
      case 7:
         InputTestFolderResult = folder.reading.input.infeedSideDoor;
         break;
      case 8:
         InputTestFolderResult = folder.reading.input.feedAllButtonInfeed;
         break;
      default:
         break;
   }
   
   switch (InputTestBufferIndex)
   {
      case 0:
         InputTestBufferResult = buffer.reading.input.infeedNipRoller;
         break;
      case 1:
         InputTestBufferResult = buffer.reading.input.outfeedNipRoller;
         break;
      case 2:
         InputTestBufferResult = buffer.reading.input.pressureGuard;
         break;
      case 3:
         InputTestBufferResult = buffer.reading.input.infeedPaperEnd;
         break;
      case 4:
         InputTestBufferResult = buffer.reading.input.coverClosed;
         break;
      case 5:
         InputTestBufferResult = 0;
         break;
      default:
         //IOtestInputIndex = IOtestInputListStart;
         InputTestBufferResult = 0;
         break;
   }
   
   //Output Test Stacker 
   if (lineController.setup.stacker.installed) {
      if (0 == OutputTestStackerIndex)
      {	
         stacker.setting.forceOutputEnable.offsetValveSpaghettiBelt1 = OutputTestStackerForce;
         stacker.setting.forceOutputValue.offsetValveSpaghettiBelt1  = OutputTestStackerValue;	
         OutputTestStackerResult = stacker.reading.output.offsetValveSpaghettiBelt1;	
      } 
      else stacker.setting.forceOutputEnable.offsetValveSpaghettiBelt1 = FALSE;
      if (1 == OutputTestStackerIndex)
      {
         stacker.setting.forceOutputEnable.offsetValveSpaghettiBelt2 = OutputTestStackerForce;
         stacker.setting.forceOutputValue.offsetValveSpaghettiBelt2  = OutputTestStackerValue;	
         OutputTestStackerResult = stacker.reading.output.offsetValveSpaghettiBelt2;
      } 
      else stacker.setting.forceOutputEnable.offsetValveSpaghettiBelt2 = FALSE;
      if (2 == OutputTestStackerIndex)
      {
         stacker.setting.forceOutputEnable.secUnitControl = OutputTestStackerForce;
         stacker.setting.forceOutputValue.secUnitControl  = OutputTestStackerValue;	
         OutputTestStackerResult = stacker.reading.output.secUnitControl;	
      } 
      else stacker.setting.forceOutputEnable.secUnitControl = FALSE;
      if (3 == OutputTestStackerIndex)
      {
         stacker.setting.forceOutputEnable.antistatic = OutputTestStackerForce;
         stacker.setting.forceOutputValue.antistatic = OutputTestStackerValue;
         OutputTestStackerResult = stacker.reading.output.antistatic;
      } 
      else stacker.setting.forceOutputEnable.antistatic = FALSE;
      if (4 == OutputTestStackerIndex)
      {
         stacker.setting.forceOutputEnable.userOut1 = OutputTestStackerForce;
         stacker.setting.forceOutputValue.userOut1 = OutputTestStackerValue;
         OutputTestStackerResult = stacker.reading.output.userOut1;
      } 
      else stacker.setting.forceOutputEnable.userOut1 = FALSE;
      if (5 == OutputTestStackerIndex)
      {
         stacker.setting.forceOutputEnable.userOut2 = OutputTestStackerForce;
         stacker.setting.forceOutputValue.userOut2 = OutputTestStackerValue;
         OutputTestStackerResult = stacker.reading.output.userOut2;
      } 
      else stacker.setting.forceOutputEnable.userOut2 = FALSE;
   
   }
   else if (lineController.setup.streamer.installed) {
      
      if (0 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.mainAirValve,&stacker.setting.forceOutputValue.mainAirValve,&stacker.reading.output.mainAirValve,OutputTestStackerForce,OutputTestStackerValue);
      else stacker.setting.forceOutputEnable.mainAirValve = FALSE;
      if (1 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.antistatic,&stacker.setting.forceOutputValue.antistatic,&stacker.reading.output.antistatic,OutputTestStackerForce,OutputTestStackerValue);
      else stacker.setting.forceOutputEnable.antistatic = FALSE;
      if (2 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.enableAirGlue,&stacker.setting.forceOutputValue.enableAirGlue,&stacker.reading.output.enableAirGlue,OutputTestStackerForce,OutputTestStackerValue);
      else stacker.setting.forceOutputEnable.enableAirGlue = FALSE;
      if (3 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.pressUnitStart,&stacker.setting.forceOutputValue.pressUnitStart,&stacker.reading.output.pressUnitStart,OutputTestStackerForce,OutputTestStackerValue);
      else stacker.setting.forceOutputEnable.pressUnitStart = FALSE;
      if (4 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.offsetValve1,&stacker.setting.forceOutputValue.offsetValve1,&stacker.reading.output.offsetValve1,OutputTestStackerForce,OutputTestStackerValue);
      else if (12 != OutputTestStackerIndex) stacker.setting.forceOutputEnable.offsetValve1 = FALSE;  
      if (5 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.offsetValve2,&stacker.setting.forceOutputValue.offsetValve2,&stacker.reading.output.offsetValve2,OutputTestStackerForce,OutputTestStackerValue);
      else if (12 != OutputTestStackerIndex) stacker.setting.forceOutputEnable.offsetValve2 = FALSE;
      if (6 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.offsetValve3,&stacker.setting.forceOutputValue.offsetValve3,&stacker.reading.output.offsetValve3,OutputTestStackerForce,OutputTestStackerValue);
      else if (12 != OutputTestStackerIndex) stacker.setting.forceOutputEnable.offsetValve3 = FALSE;
      if (7 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.offsetValve4,&stacker.setting.forceOutputValue.offsetValve4,&stacker.reading.output.offsetValve4,OutputTestStackerForce,OutputTestStackerValue);
      else if (12 != OutputTestStackerIndex) stacker.setting.forceOutputEnable.offsetValve4 = FALSE;
      if (8 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.offsetValve5,&stacker.setting.forceOutputValue.offsetValve5,&stacker.reading.output.offsetValve5,OutputTestStackerForce,OutputTestStackerValue);
      else if (12 != OutputTestStackerIndex) stacker.setting.forceOutputEnable.offsetValve5 = FALSE;
      if (9 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.userOut1,&stacker.setting.forceOutputValue.userOut1,&stacker.reading.output.userOut1,OutputTestStackerForce,OutputTestStackerValue);
      else stacker.setting.forceOutputEnable.userOut1 = FALSE;
      if (10 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.userOut2,&stacker.setting.forceOutputValue.userOut2,&stacker.reading.output.userOut2,OutputTestStackerForce,OutputTestStackerValue);
      else stacker.setting.forceOutputEnable.userOut2 = FALSE;
      if (11 == OutputTestStackerIndex)
         OutputTestStackerResult = outputTest(&stacker.setting.forceOutputEnable.userOut3,&stacker.setting.forceOutputValue.userOut3,&stacker.reading.output.userOut3,OutputTestStackerForce,OutputTestStackerValue);
      else stacker.setting.forceOutputEnable.userOut3 = FALSE;
      
      if (12 == OutputTestStackerIndex)
      {
         if (OutputTestStackerForce && OutputTestStackerValue)
         {
            switch (exerciseOffsetState)
            {
               case 0:
                  OutputTestStackerResult = true;
                  stacker.setting.forceOutputEnable.offsetValve1 = true;
                  stacker.setting.forceOutputEnable.offsetValve2 = true;
                  stacker.setting.forceOutputEnable.offsetValve3 = true;
                  stacker.setting.forceOutputEnable.offsetValve4 = true;
                  stacker.setting.forceOutputEnable.offsetValve5 = true;
                  exerciseOffsetState = 1;
                  break;
               case 1:
                  stacker.setting.forceOutputValue.offsetValve1 = true;
                  exerciseOffsetState = 3;//2
                  break;
               case 2:
                  exerciseOffsetState = 3;
                  break;
               case 3:
                  stacker.setting.forceOutputValue.offsetValve2 = true;
                  exerciseOffsetState = 5;//4
                  break;
               case 4:
                  exerciseOffsetState = 5;
                  break;
               case 5:
                  stacker.setting.forceOutputValue.offsetValve3 = true;
                  exerciseOffsetState = 7;//6
                  break;
               case 6:
                  stacker.setting.forceOutputValue.offsetValve4 = true;
                  exerciseOffsetState = 7;
                  break;
               case 7:
                  stacker.setting.forceOutputValue.offsetValve5 = true;
                  exerciseOffsetState = 10; //8
                  break;
               case 8:
                  exerciseOffsetState = 9;
                  break;
               case 9:
                  exerciseOffsetState = 10;
                  break;
               case 10:
                  OutputTestStackerResult = false;
                  stacker.setting.forceOutputValue.offsetValve1 = false;
                  exerciseOffsetState = 12;//11
                  break;
               case 11:
                  exerciseOffsetState = 12;
                  break;
               case 12:
                  stacker.setting.forceOutputValue.offsetValve2 = false;
                  exerciseOffsetState = 14;//13
                  break;
               case 13:
                  exerciseOffsetState = 14;
                  break;
               case 14:
                  stacker.setting.forceOutputValue.offsetValve3 = false;
                  exerciseOffsetState = 15;
                  break;
               case 15:
                  stacker.setting.forceOutputValue.offsetValve4 = false;
                  exerciseOffsetState = 16;
                  break;
               case 16:
                  stacker.setting.forceOutputValue.offsetValve5 = false;
                  exerciseOffsetState = 1;//17
                  exerciseCounter++;
                  break;
               case 17:
                  exerciseOffsetState = 0;
                  
                  break;
            }
         }
      } 
      else {
         /*stacker.setting.forceOutputEnable.offsetValve1 = false;
         stacker.setting.forceOutputEnable.offsetValve2 = false;
         stacker.setting.forceOutputEnable.offsetValve3 = false;
         stacker.setting.forceOutputEnable.offsetValve4 = false;
         stacker.setting.forceOutputEnable.offsetValve5 = false;*/
      }
      
      
      
      
      
   }

   
   //Output Test Cutter 
   if (0 == OutputTestCutterIndex)
   {
      cutter.setting.forceOutputEnable.ready = OutputTestCutterForce;
      cutter.setting.forceOutputValue.ready = OutputTestCutterValue;
      OutputTestCutterResult = cutter.reading.output.ready;
		
   } 
   else cutter.setting.forceOutputEnable.ready = FALSE;
   if (1 == OutputTestCutterIndex)
   {
      cutter.setting.forceOutputEnable.pause = OutputTestCutterForce;
      cutter.setting.forceOutputValue.pause = OutputTestCutterValue;
      OutputTestCutterResult = cutter.reading.output.pause;
   } 
   else cutter.setting.forceOutputEnable.pause = FALSE;
   if (2 == OutputTestCutterIndex)
   {
      cutter.setting.forceOutputEnable.slowDown = OutputTestCutterForce;
      cutter.setting.forceOutputValue.slowDown = OutputTestCutterValue;
      OutputTestCutterResult = cutter.reading.output.slowDown;
   } 
   else cutter.setting.forceOutputEnable.slowDown = FALSE;
   if (3 == OutputTestCutterIndex)
   {
      cutter.setting.forceOutputEnable.fanOutput = OutputTestCutterForce;
      cutter.setting.forceOutputValue.fanOutput = OutputTestCutterValue;
      OutputTestCutterResult = cutter.reading.output.fanOutput;
   } 
   else cutter.setting.forceOutputEnable.fanOutput = FALSE; 
   if (4 == OutputTestCutterIndex)
   {
      cutter.setting.forceOutputEnable.granulatorStart = OutputTestCutterForce;
      cutter.setting.forceOutputValue.granulatorStart = OutputTestCutterValue;
      OutputTestCutterResult = cutter.reading.output.granulatorStart;
   } 
   else cutter.setting.forceOutputEnable.granulatorStart = FALSE; 
   if (5 == OutputTestCutterIndex)
   {
      cutter.setting.forceOutputEnable.antistatic = OutputTestCutterForce;
      cutter.setting.forceOutputValue.antistatic = OutputTestCutterValue;
      OutputTestCutterResult = cutter.reading.output.antistatic;
   } 
   else cutter.setting.forceOutputEnable.antistatic = FALSE; 
        
    
    
    
    
   //Output Test Folder 
   if (0 == OutputTestFolderIndex)
   {
      folder.setting.forceOutputEnable.beeper = OutputTestFolderForce;
      folder.setting.forceOutputValue.beeper = OutputTestFolderValue;
      OutputTestFolderResult = folder.reading.output.beeper;
   } 
   else folder.setting.forceOutputEnable.beeper = FALSE;  //stacker.setting.forceOutputEnable.comPulseGripper = FALSE;//  folderBeeper.force = FALSE;
   
   
   
   lineController.reading.bufferValvesInfo.controlledPressureValve = buffer.reading.output.loopValve2;
   lineController.reading.bufferValvesInfo.lockValve = buffer.reading.output.loopLockValve;
   if (buffer.reading.output.loopValve1_1)
   {
      if (buffer.reading.output.loopValve1_2) lineController.reading.bufferValvesInfo.upDownValve = 3;
      else lineController.reading.bufferValvesInfo.upDownValve = 1;
   }
   else
   {
      if (buffer.reading.output.loopValve1_2) lineController.reading.bufferValvesInfo.upDownValve = 2;
      else lineController.reading.bufferValvesInfo.upDownValve = 0;
   }
   lineController.reading.bufferValvesInfo.pressureControl = buffer.reading.output.setValvePressure*0.0002745;
   
   if (lineController.setting.bufferValveTest.force)
   {
      buffer.setting.forceOutputEnable.loopLockValve = TRUE;
      buffer.setting.forceOutputValue.loopLockValve = lineController.setting.bufferValveTest.lockValve;
      buffer.setting.forceOutputEnable.loopValve2 = TRUE;
      buffer.setting.forceOutputValue.loopValve2 = lineController.setting.bufferValveTest.varPressValve;
      buffer.setting.forceOutputEnable.setValvePressure = TRUE;
      if (lineController.setting.bufferValveTest.SetPressureControl) {
         buffer.setting.forceOutputValue.setValvePressure = lineController.setting.bufferValveTest.pressureControl*3643;
         lineController.setting.bufferValveTest.SetPressureControl = FALSE;
      }
      
      buffer.setting.forceOutputEnable.loopValve1_1 = buffer.setting.forceOutputEnable.loopValve1_2 = TRUE;
      if (lineController.setting.bufferValveTest.controlledPressureValveUp)
      {
         if (!oldValveUp) lineController.setting.bufferValveTest.controlledPressureValveDown = FALSE;
         buffer.setting.forceOutputValue.loopValve1_1 = TRUE;
         buffer.setting.forceOutputValue.loopValve1_2 = FALSE; 
         oldValveUp = TRUE;
         oldValveDown = FALSE;
      }
      if (lineController.setting.bufferValveTest.controlledPressureValveDown)
      {
         if (!oldValveDown) lineController.setting.bufferValveTest.controlledPressureValveUp = FALSE;
         buffer.setting.forceOutputValue.loopValve1_1 = FALSE;
         buffer.setting.forceOutputValue.loopValve1_2 = TRUE;
         oldValveUp = FALSE;
         oldValveDown = TRUE;
      }
   }
   else
   {
      buffer.setting.forceOutputEnable.loopLockValve = FALSE;
      buffer.setting.forceOutputEnable.loopValve2 = FALSE;
      buffer.setting.forceOutputEnable.setValvePressure = FALSE;
      buffer.setting.forceOutputEnable.loopValve1_1 =  buffer.setting.forceOutputEnable.loopValve1_2 = FALSE;
      lineController.setting.bufferValveTest.lockValve = FALSE;
      lineController.setting.bufferValveTest.controlledPressureValveDown = FALSE;
      lineController.setting.bufferValveTest.controlledPressureValveUp = FALSE;
      lineController.setting.bufferValveTest.varPressValve = FALSE;
   }
   
   
	/*
	//Output Test Buffer 
	if (0 == OutputTestBufferIndex)
	{
		buffer.setting.forceOutputEnable.loopLockValve = OutputTestBufferForce;
		buffer.setting.forceOutputValue.loopLockValve = OutputTestBufferValue;		
		OutputTestBufferResult = buffer.reading.output.loopLockValve;
	} 
	else buffer.setting.forceOutputEnable.loopLockValve = FALSE;

    
    if (1 == OutputTestBufferIndex)
    {
        if (OutputTestBufferForce)
        {
            buffer.setting.forceOutputEnable.loopValve1_2 = TRUE;
            buffer.setting.forceOutputEnable.loopValve1_1 = TRUE;
            if (OutputTestBufferValue)
            {
                buffer.setting.forceOutputValue.loopValve1_2 = TRUE;
                buffer.setting.forceOutputValue.loopValve1_1 = FALSE;
                OutputTestBufferResult = TRUE;
            }
            else
            {
                buffer.setting.forceOutputValue.loopValve1_2 = FALSE;
                buffer.setting.forceOutputValue.loopValve1_1 = TRUE;
                OutputTestBufferResult = FALSE;
            }
        }
        else
        {
            buffer.setting.forceOutputEnable.loopValve1_2 = FALSE;
            buffer.setting.forceOutputEnable.loopValve1_1 = FALSE;
        }
    }
    else if (2 == OutputTestBufferIndex)
    {
        if (OutputTestBufferForce)
        {
            buffer.setting.forceOutputEnable.loopValve1_2 = TRUE;
            buffer.setting.forceOutputEnable.loopValve1_1 = TRUE;
            if (OutputTestBufferValue)
            {
                buffer.setting.forceOutputValue.loopValve1_2 = TRUE;
                buffer.setting.forceOutputValue.loopValve1_1 = TRUE;
                OutputTestBufferResult = TRUE;
            }
            else
            {
                buffer.setting.forceOutputValue.loopValve1_2 = FALSE;
                buffer.setting.forceOutputValue.loopValve1_1 = FALSE;
                OutputTestBufferResult = FALSE;
            }
        }
        else
        {
            buffer.setting.forceOutputEnable.loopValve1_2 = FALSE;
            buffer.setting.forceOutputEnable.loopValve1_1 = FALSE;
        }
    }
    else
    {
        buffer.setting.forceOutputEnable.loopValve1_2 = FALSE;
        buffer.setting.forceOutputEnable.loopValve1_1 = FALSE;
    }
      
	if (3 == OutputTestBufferIndex)
	{
		buffer.setting.forceOutputEnable.loopValve2 = OutputTestBufferForce;
		buffer.setting.forceOutputValue.loopValve2 = OutputTestBufferValue;		
		OutputTestBufferResult = buffer.reading.output.loopValve2;
	} 
	else buffer.setting.forceOutputEnable.loopValve2 = FALSE;
   */
	//save the settings
   RECIPE[GUI.currentRECIPE] = GUI.settings;
   lastRECIPE = GUI.currentRECIPE;
   
   if (InputTestStackerBeep && lastInputTestStackerResult != InputTestStackerResult) lineController.command.beep = TRUE;
   if (InputTestBufferBeep && lastInputTestBufferResult != InputTestBufferResult) lineController.command.beep = TRUE;
   if (InputTestCutterBeep && lastInputTestCutterResult != InputTestCutterResult) lineController.command.beep = TRUE;
   if (InputTestMergerBeep && lastInputTestMergerResult != InputTestMergerResult) lineController.command.beep = TRUE;
   if (InputTestFolderBeep && lastInputTestFolderResult != InputTestFolderResult) lineController.command.beep = TRUE;
   
   lastInputTestStackerResult = InputTestStackerResult;
   lastInputTestFolderResult  = InputTestFolderResult;
   lastInputTestBufferResult  = InputTestBufferResult;
   lastInputTestMergerResult  = InputTestMergerResult;
   lastInputTestCutterResult  = InputTestCutterResult;
}

void _EXIT ProgramExit(void)
{
	// Insert code here 
}

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <AsUDP.h>
#include <ctype.h>

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFFF; 


#define MSG_BUF_SIZE 256

char UWState[MSG_BUF_SIZE];
char u20MSG[MSG_BUF_SIZE];
char temp[100];
//unsigned char u20IP[] = "192.168.0.99"; //H�kans Lina
//unsigned char u20IP[] = "192.168.0.39"; //Mickes Lina
//unsigned char u20IP[] = "192.168.111.71"; //LSI Lina, h�kan
//unsigned char u20IP[] = "192.168.111.71"; //LSI Italien
unsigned char u20IP[] = "192.168.113.70"; //LSI Lina Micke
UINT u20Port = 9595;

void u20Comm(void)
{
   static unsigned char logOut = 0;
   static int fState = 0;
   static int stateTimer = 0;
   static unsigned long currentIdent = 0;
   static UdpSend_typ udpS;
   static UdpOpen_typ udpO;
   static UdpClose_typ udpC;
   static UdpRecv_typ udpR;
   static UdpConnect_typ udpCnct;
   
   if(fState == 0) // Initialize
   {
      memset((void *)&udpS, 0, sizeof(UdpSend_typ));
      memset((void *)&udpO, 0, sizeof(UdpOpen_typ));
      memset((void *)&udpC, 0, sizeof(UdpClose_typ));
      memset((void *)&udpR, 0, sizeof(UdpRecv_typ));
      memset((void *)&udpCnct, 0, sizeof(UdpConnect_typ));
      fState = 1;
   }
   else if(fState == 1) // Open comm
   {
      udpO.pIfAddr = (UDINT)0;
      udpO.port = u20Port;
      udpO.options = 0;
      udpO.enable = 1;
      UdpOpen(&udpO);
      stateTimer = 0;
      fState = 2;
   }
   else if(fState == 2) // Wait for UDP open to complete
   {
      if(udpO.status == ERR_FUB_BUSY)
      {
         if(stateTimer > 100)
            fState = 100;
         else
            UdpOpen(&udpO);   
      }
      else if(udpO.status != 0)
      {
         fState = 101;
      }
      else
      {
         currentIdent = udpO.ident;
         fState = 5;
      }
   }
   else if(fState == 3) // Wait for respons from u20
   {
      udpR.datamax = MSG_BUF_SIZE;
      udpR.pData = (DINT)UWState;
      udpR.ident = currentIdent;
      udpR.pIpAddr = (DINT)u20IP;
      udpR.port = u20Port;
      udpR.flags = 0;
      udpR.enable = 1;
      UdpRecv(&udpR);
      stateTimer = 0;
      fState = 4;
   }
   else if(fState == 4) // Wait for request from u20 (check FUB result)
   {
      if(udpR.status == ERR_OK)
      {
         UWState[udpR.recvlen] = 0;
         strcpy(u20Debug, UWState);
         u20.connected = true;
         u20.started = (UWState[67] == '0') ? false : true;
         u20.slowMode = (UWState[73] == '-') ? false : true;
         strncpy(temp, &(UWState[18]), 5);
         temp[5] = 0;
         u20.diameter = atof(temp);
         strncpy(temp, &(UWState[38]), 5);
         temp[5] = 0;
         u20.speed = atof(temp);
         strncpy(u20.timeRemaining, &(UWState[58]), 5);
         u20.timeRemaining[5] = 0;         
         fState = 5;
      }
      else if(stateTimer > 100)
      {
         u20.connected = false;
         u20.started = false;
         u20.slowMode = false;
         u20.diameter = 0;
         u20.speed = 0;
         strcpy(u20.timeRemaining, "--:--");
         fState = 5;
      }
      else if(udpR.status == ERR_FUB_BUSY)
         UdpRecv(&udpR);
         //fState = 3;
      else if(udpR.status == udpERR_NO_DATA)
         UdpRecv(&udpR);
         //fState = 3;
      else
         fState = 104;
   }
   else if(fState == 5) // Transmit data
   {
      //sprintf(UWState, "UW:%20s%20s%20s%5d%5d%c", vncHMI.diameter, vncHMI.speed, vncHMI.timeRemaining, vncHMI.startedStatus, vncHMI.stoppedStatus, vncHMI.slowMode ? '*' : '-');
      strcpy(u20MSG, "UW?");
      udpS.pData = (DINT)u20MSG;
      udpS.datalen = strlen((const char*)u20MSG);
      udpS.ident = currentIdent;
      udpS.pHost = (DINT)u20IP;
      udpS.port = u20Port;
      udpS.flags = 0;
      udpS.enable = 1;
      UdpSend(&udpS);
      stateTimer = 0;
      fState = 6;
   }
   else if(fState == 6) // Wait for UDP send to complete
   {
      if(udpS.status == ERR_FUB_BUSY)
      {
         if(stateTimer > 100)
            fState = 102;
         else
            UdpSend(&udpS);   
      }
      else if(udpS.status != 0)
      {
         fState = 103;
      }
      else
      {
         fState = 3;
      }
   }
   else if(fState == 100) // error: 
   {      
      strcpy(u20Debug, "UDP open: timeout");
   }
   else if(fState == 101) // error: 
   {      
      sprintf(u20Debug, "%s %d", "UDP open", udpO.status);
   }
   else if(fState == 102) // error: 
   {      
      strcpy(u20Debug, "UDP send: timeout");
   }
   else if(fState == 103) // error: 
   {      
      sprintf(u20Debug, "%s %d", "UDP send", udpS.status);
   }
   else if(fState == 104) // error: 
   {      
      sprintf(u20Debug, "%s %d", "UDP recv", udpR.status);
   }
   if(fState >= 100)
   {
      u20.connected = false;
      u20.started = false;
      u20.slowMode = false;
      u20.diameter = 0;
      u20.speed = 0;
      strcpy(u20.timeRemaining, "--:--");
   }
   stateTimer++;
   stateDebug = fState;
}


void _INIT ProgramInit(void)
{
   // Insert code here 

}

void _CYCLIC ProgramCyclic(void)
{
   // Insert code here 
   u20Comm();
}

void _EXIT ProgramExit(void)
{
   // Insert code here 

}

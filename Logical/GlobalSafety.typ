
TYPE
	safety_settings_typ : 	STRUCT 
		bufferType : USINT := 1;
		cutterType : USINT := 1;
		folderType : USINT := 0;
		mergerType : USINT := 1;
		stackerType : USINT := 2;
		gripperLockCoverRequest : BOOL;
		initializingTime : UDINT := 75; (*s, was 103, 106, 120,150*)
	END_STRUCT;
	safety_typ : 	STRUCT 
		command : safety_ext_command_typ;
		reading : safety_readings_typ;
		setting : safety_settings_typ;
	END_STRUCT;
	safety_readings_typ : 	STRUCT 
		bufferTopCoverLocked : BOOL;
		stackerTopCoverLocked : BOOL;
		globalEstopStatus : UINT;
		resetPossible : BOOL;
		maximumSpeed : REAL := 5.0;
		lightCurtainStatus : BOOL;
		stackerServoP3_1 : p3_typ;
		stackerServoP3_2 : p3_typ;
		mergerServoP2 : p2_typ;
		cutterAcposM2_1 : p2_typ;
		cutterAcposM2_2 : p2_typ;
		cutterAcposM2_3 : p2_typ;
		bufferServoP2 : p2_typ;
		stackerServoP3_3 : p3_typ;
		eStop : BOOL;
		initialized : BOOL := FALSE;
		LClatch : BOOL;
		stackerTopCoverBypassed : BOOL;
		allCoversClosed : BOOL;
		SafetyVersion : UINT := 0;
	END_STRUCT;
	safety_ext_command_typ : 	STRUCT 
		eStopReset : BOOL;
		lockBufferTopCover : BOOL;
		unlockBufferTopCover : BOOL;
		lockStackerTopCover : BOOL;
		resetStackerLightCurtain : BOOL;
		resetLClatch : BOOL;
		unlockStackerTopCover : BOOL;
		resetStackerFork : BOOL;
		bypassStackerTopCover : BOOL;
	END_STRUCT;
	p2_typ : 	STRUCT 
		ax1 : safeFunc_typ;
		ax2 : safeFunc_typ;
	END_STRUCT;
	p3_typ : 	STRUCT 
		ax1 : safeFunc_typ;
		ax2 : safeFunc_typ;
		ax3 : safeFunc_typ;
	END_STRUCT;
	safeFunc_typ : 	STRUCT 
		name : STRING[80];
		id : USINT;
		SDI : BOOL;
		SS : BOOL;
		SLS : BOOL;
		Operational : BOOL;
		STO : BOOL;
	END_STRUCT;
END_TYPE

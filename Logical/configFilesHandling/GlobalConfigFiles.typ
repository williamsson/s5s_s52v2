TYPE
   S_Configuration_command : 	STRUCT 
      writeFile : BOOL := FALSE;
      readFile : BOOL := FALSE;
      setDefaults : BOOL := FALSE;
   END_STRUCT;
   S_Configuration_parameter : 	STRUCT
      number : INT := 0;      
   END_STRUCT;
   S_Configuration_status : 	STRUCT
      number : INT := 0;
      modified : BOOL := FALSE;      
      error : UINT := 0;
   END_STRUCT;
   S_Configuration : 	STRUCT 
      command : S_Configuration_command;
      parameter : S_Configuration_parameter;
      status : S_Configuration_status;
   END_STRUCT;
END_TYPE


#include <bur/plctypes.h>
#include <fileio.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF; 

#define FIO_CYCLIC     0
#define FIO_WRITEFILE  1
#define FIO_READFILE   2

#define GLOBAL true
#define LOCAL false

char cfgBufLocal[50000];
char cfgBufGlobal[50000];

USINT fileIO(int action, char *data, char *fName)
{
   static BOOL bOK;
   static USINT byStep, byErrorLevel;
   static USINT *byWriteData;
   static char fileName[100];
   static char *readData;
   static UINT wStatus;
   static UDINT dwIdent;
   static UDINT bytesRead;
   static FileOpen_typ FOpen;
   static FileClose_typ FClose;
   static FileCreate_typ FCreate;
   static FileReadEx_typ FRead;
   static FileWrite_typ FWrite;
   static FileDelete_typ FDelete;
   static unsigned int fSize;
   static bool reading = false;
   static bool deleteAndRetry = false;
    
   if(action == FIO_WRITEFILE)
   {
      if(byStep == 0)
      {   
         configFiles.status.error = 0;
         byWriteData = (USINT *)data;
         strcpy(fileName, fName);
         fSize = strlen(data);
         reading = false;
         bytesRead = 0;
         byStep = 2; // 1
         //debugIO = 0;
      }
   }
   if(action == FIO_READFILE)
   {
      if(byStep == 0)
      {   
         configFiles.status.error = 0;
         data[0] = 0;
         readData = data;
         strcpy(fileName, fName);
         reading = true;
         bytesRead = 0;
         byStep = 1;
      }
   }
   switch (byStep)
   {
      case 0: /**** Error step ****/
         bOK = 0;
         break;
      case 1: /**** Try to open existing file ****/
         /* Initialize file open structrue */
         FOpen.enable      = 1;
         FOpen.pDevice   = (UDINT) "configs";
         FOpen.pFile     = (UDINT) fileName;
         FOpen.mode      = fiREAD_ONLY;                        /* Read access */

         /* Call FUB */
         FileOpen(&FOpen);

         /* Get FBK output information */
         dwIdent = FOpen.ident;
         wStatus = FOpen.status;
         /* Verify status (20708 -> File doesn't exist) */
         if (wStatus == 20708)
         {
            byStep = 0;
            configFiles.status.error = 20708;
         }
         else if (wStatus == 0)
         {
            if(reading)
            {
               reading = false;
               byStep = 4;
            }
            else
               byStep = 3; 
         }
         else if (wStatus != 65535)
         {
            byErrorLevel = 1;
            byStep = 0;
            if (wStatus == 20799)
            {
               configFiles.status.error = FileIoGetSysError();
            }
         }
         break;
      case 2: /**** Create file ****/
         /* Initialize file create structure */
         FCreate.enable    = 1;
         FCreate.pDevice = (UDINT) "configs";
         FCreate.pFile   = (UDINT) fileName;
         /* Call FUB */
         FileCreate(&FCreate);
         /* Get output information of FBK */
         dwIdent = FCreate.ident;
         wStatus = FCreate.status;
         /* Verify status */
         if (wStatus == 0)
         {
            byStep = 3;
         }
         else if (wStatus != 65535)
         {
            if(wStatus == fiERR_EXIST)
            {
               deleteAndRetry = true;
               byStep = 6;
            }
            else
            {
               byErrorLevel = 2;
               byStep = 0;
               if (wStatus == 20799)
               {
                  configFiles.status.error = FileIoGetSysError();
               }
            }
         }
         break;

      case 3: /**** Write data to file ****/
         /* Initialize file write structure */
         FWrite.enable  = 1;
         FWrite.ident   = dwIdent;
         FWrite.offset  = 0;
         FWrite.pSrc    = (UDINT) &byWriteData[0];
         FWrite.len     = fSize;
         /* Call FBK */
         FileWrite(&FWrite);
         /* Get status */
         wStatus = FWrite.status;
         /* Verify status */
         if (wStatus == 0)
         {
            byStep = 5;
         }
         else if (wStatus != 65535)
         {
            byErrorLevel = 3;
            byStep = 0;
            if (wStatus == 20799)
            {
               configFiles.status.error = FileIoGetSysError();
            }
         }
         break;

      case 4: /**** Read data from file ****/
         /* Initialize file read structure */
         FRead.enable      = 1;
         FRead.ident     = dwIdent;
         FRead.offset    = 0;
         FRead.pDest     = (UDINT) readData;
         FRead.len       = 50000;
         /* Call FBK */
         FileReadEx(&FRead);
         /* Get status */
         wStatus = FRead.status;
         /* Verify status */
         if (wStatus == 0)
         {
            byStep = 5;
            bytesRead = FRead.bytesread;
         }
         else if (wStatus != 65535)
         {
            byErrorLevel = 4;
            byStep = 0;
            if (wStatus == 20799)
            {
               configFiles.status.error = FileIoGetSysError();
            }
         }                        
         break;
      
      case 5: /**** Close file ****/
         /* Initialize file close structure */
         FClose.enable  = 1;
         FClose.ident   = dwIdent;
                        
         /* Call FBK */
         FileClose(&FClose);

         /* Get status */
         wStatus = FClose.status;

         /* Verify status */
         if (wStatus == 0)
         {
            byStep = 0;
         }
         else if (wStatus != 65535)
         {
            byErrorLevel = 5;
            byStep = 0;
            if (wStatus == 20799)
            {
               configFiles.status.error = FileIoGetSysError();
            }
         }                        
         break;
      
      case 6: /**** Delete file ****/
         /* Initialize file delete structure */
         FDelete.enable = 1;
         FDelete.pDevice = (UDINT) "configs";
         FDelete.pName = (UDINT) fileName;
                        
         /* Call FBK */
         FileDelete(&FDelete);

         /* Get status */
         wStatus = FDelete.status;

         /* Verify status */
         if (wStatus == 0)
         {
            if(deleteAndRetry)
               byStep = 2;
            else
               byStep = 0;
            deleteAndRetry = false;
         }
         else if (wStatus != 65535)
         {
            deleteAndRetry = false;
            byErrorLevel = 5;
            byStep = 0;
            if (wStatus == 20799)
            {
               configFiles.status.error = FileIoGetSysError();
            }
         }                        
         //debugIO = wStatus;
         break;
   }
   return byStep;
}

#define MAXITEMS 1000
#define MAXFILES 100

struct Skey
{
   char name[50];
   char value[50];
   bool global;
};

struct Sitems
{
   int numItems;
   struct Skey key[MAXITEMS];
}items, itemsBup;

void initItems(void)
{
   items.numItems = 0;
}

struct Svar
{
   char name[50];
   USINT varType;
   bool global;
   DINT *dintVar;
   REAL *realVar;
   char *stringVar;
   BOOL *boolVar;
   DINT dintDefault;
   REAL realDefault;
   char stringDefault[50];
   BOOL boolDefault;
};

struct Svars
{
   int numVars;
   struct Svar v[MAXITEMS];
} vars;

void initVars(void)
{
   vars.numVars = 0;
}

void writeCfgBuf(void)
{
   char row[255];
   
   cfgBufGlobal[0] = 0;
   cfgBufLocal[0] = 0;
   for(int i = 0; i < items.numItems; i++)
   {   
      sprintf(row, "%s: %s\r\n", items.key[i].name, items.key[i].value);
      if(items.key[i].global)
         strcat(cfgBufGlobal, row);
      else
         strcat(cfgBufLocal, row);
   }
}

int getRow(char *row, int maxChars, char *data)
{
   int ix = 0;
   while(*data != 0 && *data != '\r' && *data != '\n')
   {
      row[ix++] = *data++;
      if(ix == maxChars)
         return maxChars;
   }
   while(*data == '\r'|| *data == '\n')
   {
      row[ix++] = *data++;
      if(ix == maxChars)
         return maxChars;
   }
   row[ix] = 0;
   return ix;
}

int getIx(const char *name)
{
   if(items.numItems > 0)
   {
      for(int i = 0; i < items.numItems; i++)
      {
         if(strcmp(name, items.key[i].name) == 0)
         {
            return i;   
         }
      }
   }
   return -1;  
}

void backupItems(void)
{
   for(int i = 0; i < items.numItems; i++)
   {
      itemsBup.key[i].global = items.key[i].global;
      strcpy(itemsBup.key[i].name, items.key[i].name);
      strcpy(itemsBup.key[i].value, items.key[i].value);
   }
   itemsBup.numItems = items.numItems;
}

bool confChanged(void)
{
   for(int i = 0; i < items.numItems; i++)
   {
      if(itemsBup.key[i].global != items.key[i].global)
         return true;
      if(strcmp(itemsBup.key[i].name, items.key[i].name))
         return true;
      if(strcmp(itemsBup.key[i].value, items.key[i].value))
      {
         strcpy(debugString1, items.key[i].name);  
         strcpy(debugString2, items.key[i].value);  
         strcpy(debugString3, itemsBup.key[i].name);  
         strcpy(debugString4, itemsBup.key[i].value);  
         return true;
      }
   }
   return false;
}

void readCfgBuf(void)
{
   char *buf;
   char row[255];
   char *end;
   char *valueAdr;
   int numChars;
   int cix;
   
   for(int fix = 0; fix < 2; fix++)
   {
      if(fix == 0)
         buf = cfgBufGlobal;
      else
         buf = cfgBufLocal;      
      while((numChars = getRow(row, 255, buf)) > 0)
      {
         buf += numChars;
         end = row + strlen(row) - 1;
         while(end > row && isspace((unsigned char)*end)) end--;
         end[1] = '\0';
         if(strlen(row))
         {
            if(NULL != (valueAdr = strstr(row, ":")))
            {
               valueAdr[0] = '\0';
               valueAdr += 2;   
               
               cix = getIx(row);
               if(cix >= 0)
               {
                  strcpy(items.key[cix].name, row);
                  strcpy(items.key[cix].value, valueAdr);
               }
            }
         }
      }
   }
}

int setVar(DINT var, const char *name, bool global)
{
   printf("d1\n");
   int cix = getIx(name);
   
   if(cix == -1)
   {
      if(items.numItems >= MAXITEMS - 1)
         return 0;
      cix = items.numItems;
      items.numItems++;
   }
   sprintf(items.key[cix].name, "%s", name);
   sprintf(items.key[cix].value, "%d", var);
   items.key[cix].global = global;
   return 1;
}

int setVar(REAL var, const char *name, bool global)
{
   printf("d2\n");
   int cix = getIx(name);
   
   if(cix == -1)
   {
      if(items.numItems >= MAXITEMS - 1)
         return 0;
      cix = items.numItems;
      items.numItems++;
   }
   sprintf(items.key[cix].name, "%s", name);
   sprintf(items.key[cix].value, "%.5f", var);
   items.key[cix].global = global;
   return 1;
}

int setVar(char *var, const char *name, bool global)
{
   printf("d3\n");
   int cix = getIx(name);
   
   if(cix == -1)
   {
      if(items.numItems >= MAXITEMS - 1)
         return 0;
      cix = items.numItems;
      items.numItems++;
   }
   sprintf(items.key[cix].name, "%s", name);
   sprintf(items.key[cix].value, "%s", var);
   items.key[cix].global = global;
   return 1;
}

int setVar(BOOL var, const char *name, bool global)
{
   printf("d4\n");
   int cix = getIx(name);
   
   if(cix == -1)
   {
      if(items.numItems >= MAXITEMS - 1)
         return 0;
      cix = items.numItems;
      items.numItems++;
   }
   sprintf(items.key[cix].name, "%s", name);
   sprintf(items.key[cix].value, "%s", var ? "TRUE" : "FALSE");
   items.key[cix].global = global;
   return 1;
}

int getVar(DINT *var, const char *name, DINT def)
{
   printf("g1\n");  
   int cix = getIx(name);

   if(cix == -1)
   {
      *var = def;
      return 0;
   }
   else
   {
      *var = atoi(items.key[cix].value);   
      return 1;
   }
}

int getVar(REAL *var, const char *name, REAL def)
{
   printf("g2\n");  
   int cix = getIx(name);

   if(cix == -1)
   {
      *var = def;
      return 0;
   }
   else
   {
      *var = atof(items.key[cix].value);   
      return 1;
   }
}

int getVar(char *var, const char *name, const char *def)
{
   printf("g3\n");  
   int cix = getIx(name);

   if(cix == -1)
   {
      strcpy(var, def);
      return 0;
   }
   else
   {
      strcpy(var, items.key[cix].value);   
      return 1;
   }
}

int getVar(BOOL *var, const char *name, BOOL def)
{
   printf("g4\n");  
   int cix = getIx(name);

   if(cix == -1)
   {
      *var = def;
      return 0;
   }
   else
   {
      *var = items.key[cix].value[0] == 'T' ? true : false;   
      return 1;
   }
}

void setParameters(void)
{
   for(int i = 0; i < vars.numVars; i++)
   {
      if(vars.v[i].varType == 0)
         setVar(*vars.v[i].dintVar, vars.v[i].name, vars.v[i].global);        
      else if(vars.v[i].varType == 1)
         setVar(*vars.v[i].realVar, vars.v[i].name, vars.v[i].global);        
      else if(vars.v[i].varType == 2)
         setVar(vars.v[i].stringVar, vars.v[i].name, vars.v[i].global);        
      else if(vars.v[i].varType == 3)
         setVar(*vars.v[i].boolVar, vars.v[i].name, vars.v[i].global);        
   }
}

void getParameters(void)
{
   for(int i = 0; i < vars.numVars; i++)
   {
      if(vars.v[i].varType == 0)
         getVar(vars.v[i].dintVar, vars.v[i].name, vars.v[i].dintDefault);        
      else if(vars.v[i].varType == 1)
         getVar(vars.v[i].realVar, vars.v[i].name, vars.v[i].realDefault);        
      else if(vars.v[i].varType == 2)
         getVar(vars.v[i].stringVar, vars.v[i].name, vars.v[i].stringDefault);        
      else if(vars.v[i].varType == 3)
         getVar(vars.v[i].boolVar, vars.v[i].name, vars.v[i].boolDefault);        
   }
}

int addVar(DINT *var, const char *name, DINT def, bool global)
{
   vars.v[vars.numVars].varType = 0;
   vars.v[vars.numVars].dintVar = var;
   strcpy(vars.v[vars.numVars].name, name);
   vars.v[vars.numVars].dintDefault = def;
   vars.v[vars.numVars].global = global;
   vars.numVars++;
}

int addVar(REAL *var, const char *name, REAL def, bool global)
{
   vars.v[vars.numVars].varType = 1;
   vars.v[vars.numVars].realVar = var;
   strcpy(vars.v[vars.numVars].name, name);
   vars.v[vars.numVars].realDefault = def;
   vars.v[vars.numVars].global = global;
   vars.numVars++;
}

int addVar(char *var, const char *name, char *def, bool global)
{
   vars.v[vars.numVars].varType = 2;
   strcpy(vars.v[vars.numVars].stringVar, var);
   strcpy(vars.v[vars.numVars].name, name);
   strcpy(vars.v[vars.numVars].stringDefault, def);
   vars.v[vars.numVars].global = global;
   vars.numVars++;
}

int addVar(BOOL *var, const char *name, BOOL def, bool global)
{
   vars.v[vars.numVars].varType = 3;
   vars.v[vars.numVars].boolVar = var;
   strcpy(vars.v[vars.numVars].name, name);
   vars.v[vars.numVars].boolDefault = def;
   vars.v[vars.numVars].global = global;
   vars.numVars++;
}

void addParameters(void)
{
   addVar(&cutter.setting.format , "cutterFormat", 305.0, LOCAL);        
   addVar(&cutter.setting.infeedRunSpeed, "cutterInfeedRunSpeed", 500.0, LOCAL);        
   addVar(&cutter.setting.knifeDiff, "cutterKnifeDiff", 123.0, LOCAL);  
   addVar(&cutter.setting.mainInfeedOverSpeed, "cutterMainInfeedOverSpeed", 21, LOCAL);  
   addVar(&cutter.setting.masterAcc, "cutterMasterAcc", 300.0, LOCAL);   
   addVar(&cutter.setting.masterDec, "cutterMasterDec", 6074.0, LOCAL);        
   addVar(&cutter.setting.outfeedCrash1Enable, "cutterOutfeedCrash1Enable", false, LOCAL);        
   addVar(&cutter.setting.outfeedCrash2Enable, "cutterOutfeedCrash2Enable", false, LOCAL);        
   addVar(&cutter.setting.outfeedCrash3Enable, "cutterOutfeedCrash3Enable", false, LOCAL);        
   addVar(&cutter.setting.regMarkMaxWidth, "cutterRegMarkMaxWidth", 5.0, LOCAL);        
   addVar(&cutter.setting.regMarkMinWidth, "cutterRegMarkMinWidth", 0.5, LOCAL);        
   addVar(&cutter.setting.jobMarkSeparation, "cutterJobMarkSeparation", false, LOCAL);        
   addVar(&cutter.setting.jobMarkDelay, "cutterJobMarkDelay", 5, LOCAL);        
   addVar(&cutter.setting.jobMarkMaxWidth, "cutterJobMarkMaxWidth", 17.5, LOCAL);        
   addVar(&cutter.setting.jobMarkMinWidth, "cutterJobMarkMinWidth", 12.5, LOCAL);        
   addVar(&cutter.setting.regMarkOffset, "cutterRegMarkOffset", 0.0, LOCAL);        
   addVar(&cutter.setting.reloadOnWhite, "cutterReloadOnWhite", false, LOCAL);        
   addVar(&cutter.setting.sensorKnifeDist, "cutterSensorKnifeDist", 2600.0, LOCAL);        
   addVar(&cutter.setting.simulateOffset, "cutterSimulateOffset", 0, LOCAL);        
   addVar(&cutter.setting.stopOffset, "cutterStopOffset", 87.0, LOCAL);        
   addVar(&cutter.setting.stripCut, "cutterStripCut", false, LOCAL);        
   addVar(&cutter.setting.stripSize, "cutterStripSize", 8.0, LOCAL);        
   addVar(&cutter.setting.supportOverSpeed, "cutterSupportOverSpeed", 50, LOCAL);        
   addVar(&cutter.setting.deliverStopTime, "cutterDeliverStopTime", 400, LOCAL);        
   addVar(&cutter.setting.markRegistration, "cutterMarkRegistration", false, LOCAL);        
   addVar(&cutter.setting.mainKnifeHomeOffset, "cutterMainKnifeHomeOffset", 0.0, GLOBAL);
   addVar(&cutter.setting.skipToPrintLength, "cutterSkipToPrintLength", 20.0, GLOBAL);
   addVar(&cutter.setting.whitePagesLength, "cutteWhitePagesLength", 10.0, GLOBAL);
   addVar(&cutter.setting.stackWhitePages, "cutterStackWhitePages", false, LOCAL);        
   addVar(&cutter.setting.tensionIdealPerc, "cutterTensionIdealPerc", 50, LOCAL);
   addVar(&cutter.setting.tensionLowPos, "cutterTensionLowPos", 24000, GLOBAL);
   addVar(&cutter.setting.tensionHighPos, "cutterTensionHighPos", 4000, GLOBAL);
   addVar(&cutter.setting.maxMissedMarks     , "maxMissedMarks", 0, LOCAL);
   addVar(&cutter.setting.offsetOnMissedMark , "offsetOnMissedMark", 0, LOCAL);
   addVar(&cutter.setting.rejectSkipToPrint , "rejectSkipToPrint", false, LOCAL);
   addVar(&cutter.setting.slowSpeedLoad , "slowSpeedLoad", true, LOCAL);
   addVar(&cutter.setting.cutMarkAbove0Below1 , "cutterCutMarkAbove0Below1", 0, LOCAL);
   addVar(&cutter.setting.jobMarkAbove0Below1 , "cutterJobMarkAbove0Below1", 0, LOCAL);
   addVar(&cutter.setting.rejectDelay, "cutterRejectDelay", 100.0, LOCAL);
   addVar(&cutter.setting.rejectBannerPages, "cutterRejectBannerPages", 0, LOCAL);      
   addVar(&cutter.setting.bufferHighPos, "cutterBufferHighPos", 10.0, LOCAL);
   addVar(&cutter.setting.bufferLowPos, "cutterBufferLowPos", 15.0, LOCAL);
   addVar(&cutter.setting.maxPrinterOverSpeed, "cutterMaxPrinterOverSpeed", 0, GLOBAL);
   addVar(&cutter.setting.stopWhenDelivering, "cutterStopWhenDelivering", true, LOCAL);        
   addVar(&cutter.setting.deliverDecStartSpeed, "cutterDeliverDecStartSpeed", 4.0, LOCAL);
   addVar(&cutter.setting.deliverDecMinSpeed, "cutterDeliverDecMinSpeed", 0.2, LOCAL);
   addVar(&cutter.setting.spitterTorqueLimit, "cutterSpitterTorqueLimit", 10.0, LOCAL);
   addVar(&cutter.setting.noMarksSlowSpeed, "cutterNoMarksSlowSpeed", 1.25, LOCAL);        
   
  // addVar(cutter.setting.confName, "cutterConfName", "Default configuration", LOCAL);

   addVar(&merger.setting.enabled, "mergerEnabled", false, LOCAL);        
   addVar(&merger.setting.infeedOverSpeed, "mergerInfeedOverSpeed", 4, LOCAL);        
   addVar(&merger.setting.punchUnitEnabled, "mergerPunchUnitEnabled", false, LOCAL);        
   addVar(&merger.setting.sensorPunchDist, "mergerSensorPunchDist", 1000.0, LOCAL);        
   addVar(&merger.setting.tensionIdealPerc, "mergerTensionIdealPerc", 30, LOCAL);
   addVar(&merger.setting.tensionLowPos, "mergerTensionLowPos", 24000, GLOBAL);
   addVar(&merger.setting.tensionHighPos, "mergerTensionHighPos", 4000, GLOBAL);
   
   addVar(&buffer.setting.decurlTime, "bufferDecurlTime", 5, LOCAL);        
   addVar(&buffer.setting.disableDecurl, "bufferDisableDecurl", true, LOCAL);        
   addVar(&buffer.setting.innerLoopBottom, "bufferInnerLoopBottom", 4563, GLOBAL);        
   addVar(&buffer.setting.innerLoopManualTension, "bufferInnerLoopManualTension", 12.0, LOCAL);        
   addVar(&buffer.setting.innerLoopTop, "bufferInnerLoopTop", 32390, GLOBAL);        
   addVar(&buffer.setting.maxInfeedSpeed, "bufferMaxInfeedSpeed", 3.166673, LOCAL);        
   addVar(&buffer.setting.pauseLevel, "bufferPauseLevel", 85.0, LOCAL);        
   addVar(&buffer.setting.slowLevel, "bufferSlowLevel", 80.0, LOCAL);        
   addVar(&buffer.setting.stopLevel, "bufferStopLevel", 90.0, LOCAL);
   addVar(&buffer.setting.infeedLoopTop, "bufferInfeedLoopTop", 450, GLOBAL); 
   addVar(&buffer.setting.innerLoopBottom, "bufferInfeedLoopBottom", 30000, GLOBAL);
   addVar(&buffer.setting.innerLoopTensionContTyp, "bufferTensionContTyp", 0, LOCAL);
   addVar(&buffer.setting.infeedOperationSave, "bufferInfeedOperation", 1, LOCAL);
   
   addVar(&folder.setting.tensionInIdealPerc, "folderTensionInIdealPerc", 30, LOCAL);
   addVar(&folder.setting.tensionInLowPos, "folderTensionInLowPos", 24000, GLOBAL);
   addVar(&folder.setting.tensionInHighPos, "folderTensionInHighPos", 4000, GLOBAL);
   addVar(&folder.setting.tensionIdealPerc, "folderTensionIdealPerc", 30, LOCAL);
   addVar(&folder.setting.tensionLowPos, "folderTensionLowPos", 24000, GLOBAL);
   addVar(&folder.setting.tensionHighPos, "folderTensionHighPos", 4000, GLOBAL);
   addVar(&folder.setting.foldingType, "folderFoldingType", 2, LOCAL);
   
   addVar(&folderAutomation.setting.position.plowH1HomePos, "folderAutomationPositionPlowH1HomePos", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV1HomePos, "folderAutomationPositionPlowV1HomePos", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowH2HomePos, "folderAutomationPositionPlowH2HomePos", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV2HomePos, "folderAutomationPositionPlowV2HomePos", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPLHomePos, "folderAutomationPositionPlowPLHomePos", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPRHomePos, "folderAutomationPositionPlowPRHomePos", 538.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowH1Offset, "folderAutomationPositionPlowH1Offset", -82.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV1Offset, "folderAutomationPositionPlowV1Offset", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowH2Offset, "folderAutomationPositionPlowH2Offset", 164.2, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV2Offset, "folderAutomationPositionPlowV2Offset", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPLOffset, "folderAutomationPositionPlowPLOffset", -32.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPROffset, "folderAutomationPositionPlowPROffset", 32.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowH1LimLow, "folderAutomationPositionPlowH1LimLow", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV1LimLow, "folderAutomationPositionPlowV1LimLow", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowH2LimLow, "folderAutomationPositionPlowH2LimLow", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV2LimLow, "folderAutomationPositionPlowV2LimLow", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPLLimLow, "folderAutomationPositionPlowPLLimLow", 0.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPRLimLow, "folderAutomationPositionPlowPRLimLow", 40.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowH1LimHigh, "folderAutomationPositionPlowH1LimHigh", 310.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV1LimHigh, "folderAutomationPositionPlowV1LimHigh", 360.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowH2LimHigh, "folderAutomationPositionPlowH2LimHigh", 300.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowV2LimHigh, "folderAutomationPositionPlowV2LimHigh", 250.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPLLimHigh, "folderAutomationPositionPlowPLLimHigh", 500.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowPRLimHigh, "folderAutomationPositionPlowPRLimHigh", 545.0, GLOBAL);
   addVar(&folderAutomation.setting.position.plowHead2VerExtra, "folderAutomationPositionPlowHead2VerExtra", 50.0, GLOBAL);
   addVar(&folderAutomation.setting.marginLeft, "folderAutomationMarginLeft", 0.0, LOCAL);
   addVar(&folderAutomation.setting.marginRight, "folderAutomationMarginRight", 0.0, LOCAL);
   addVar(&folderAutomation.setting.position.plowH2Act, "folderAutomationPositionPlowH2Act", 100.0, LOCAL);
   addVar(&folderAutomation.setting.position.plowV2Act, "folderAutomationPositionPlowV2Act", 100.0, LOCAL);
   addVar(&folderAutomation.setting.position.plowH1Act, "folderAutomationPositionPlowH1Act", 100.0, LOCAL);
   addVar(&folderAutomation.setting.position.plowV1Act, "folderAutomationPositionPlowV1Act", 100.0, LOCAL);
   addVar(&folderAutomation.setting.position.plowPLAct, "folderAutomationPositionPlowPLAct", 0.0, LOCAL);
   addVar(&folderAutomation.setting.position.plowPRAct, "folderAutomationPositionPlowPRAct", 543.0, LOCAL);
   addVar(&folderAutomation.setting.usePerfLeft, "folderAutomationUsePerfLeft", true, LOCAL);
   addVar(&folderAutomation.setting.usePerfCenter, "folderAutomationUsePerfCenter", true, LOCAL);
   addVar(&folderAutomation.setting.usePerfRight, "folderAutomationUsePerfRight", true, LOCAL);   
   
   addVar(&stacker.setting.automaticEndStop, "stackerAutomaticEndStop", true, LOCAL);        
   addVar(&stacker.setting.backStop, "stackerBackStop", 0.0, LOCAL);        
   addVar(&stacker.setting.deliverTransportSpeed, "stackerDeliverTransportSpeed", 0.20, GLOBAL);        
   addVar(&stacker.setting.deliveryPosition, "stackerDeliveryPosition", 20.0, GLOBAL);        
   addVar(&stacker.setting.fingerHightAdjust, "stackerFingerHightAdjust", 0.0, LOCAL);        
   addVar(&stacker.setting.maxStackSize, "stackerMaxStackSize", 140.0, LOCAL);        
   addVar(&stacker.setting.minStackSize, "stackerMinStackSize", 50.0, LOCAL);        
   addVar(&stacker.setting.StackerFingerZeroAdjust, "stackerFingerZeroAdjust", 0.0, LOCAL);        
   addVar(&stacker.setting.startInSouth, "stackerStartInSouth", false, LOCAL);        
   addVar(&stacker.setting.tableSteps, "stackerTableSteps", 0.18, LOCAL);        
   addVar(&stacker.setting.topPosOffset, "stackerTopPosOffset", -1.0, LOCAL);        
   addVar(&stacker.setting.transportIdleSpeed, "stackerTransportIdleSpeed", 0.166667, LOCAL);        
   addVar(&stacker.setting.transportTableSpeed, "stackerTransportTableSpeed", 1.32, LOCAL);        
   addVar(&stacker.setting.useStepSensor, "stackerUseStepSensor", 0, LOCAL);
   addVar(&stacker.setting.deliveryRunDistance, "stackerDeliveryRunDistance", 620, LOCAL);
   addVar(&stacker.setting.holdingPositionGradient, "stackerHoldingPositionGradient", 0.0, LOCAL);
   addVar(&stacker.setting.spaghettiOverSpeed, "stackerSpaghettiOverSpeed", 1.05, LOCAL);
   addVar(&stacker.setting.delayOffsetPage, "delayOffsetPage", 1, LOCAL);
   addVar(&stacker.setting.forkHoldAtOffsetAdjust,"forkHoldAtOffsetAdjust",0.0,LOCAL); 
   addVar(&stacker.setting.forkDeliverPositionAdjust,"forkDeliverPositionAdjust",0.0,LOCAL);
   addVar(&streamer.setting.jogger2Position,"streamerJogger2Position",0.0,LOCAL);
   addVar(&streamer.setting.enableOffset,"streamerEnableOffset",true,LOCAL);
   
   
   addVar(&ebmGlue.setting.offset.nozzleDelay , "ebmOffsetNozzleDelay",39,LOCAL);
   addVar(&ebmGlue.setting.offset.maxMarkSize , "ebmOffsetMaxMarkSize", 5.5,LOCAL);
   addVar(&ebmGlue.setting.offset.minMarkSize , "ebmOffsetMinMarkSize", 3.0,LOCAL);
   addVar(&ebmGlue.setting.offset.offset      , "ebmOffsetOffset", 0.0 ,LOCAL);
  
   //addVar(&ebmGlue.setting.offset.nozzleDelay , "ebmGlueOffsetNozzleDelay",39,LOCAL);
   addVar(&ebmGlue.setting.glueGun.maxMarkSize ,  "ebmGlueMaxMarkSize", 5.5,LOCAL);
   addVar(&ebmGlue.setting.glueGun.minMarkSize ,  "ebmGlueMinMarkSize", 3.0,LOCAL);
   addVar(&ebmGlue.setting.glueGun.offset      ,  "ebmGlueOffset", 0.0 ,LOCAL);
   addVar(&ebmGlue.setting.glueGun.pattern      , "ebmGluePattern", 0.0 ,LOCAL);
   addVar(&ebmGlue.setting.glueGun.separation   ,  "glueGunSeparation", 2.5, LOCAL);
   
   //addVar(&stacker.setting.firstStepFactor, "stackerFirstStepFactor", 1.0, LOCAL);
   
   addVar(&streamer.setting.jogSpeed,"streamerJogSpeed",1.0,LOCAL); 
   addVar(&streamer.setting.overlap,"streamerOverlap",20.0,LOCAL);
   addVar(&streamer.setting.forkBackTapPos,"streamerForkBackTapPos",3.0,LOCAL);
   addVar(&streamer.setting.lastSheetDist,"streamerLastSheetDist",45.0,LOCAL);
   addVar(&streamer.setting.streamerDeliverySpeed,"streamerDeliverySpeed",0.45,LOCAL);
   //addVar(&streamer.setting.overMinStackSlowDown,"streamerOverMinStackSlowDown",false,LOCAL);
   addVar(&streamer.setting.maxSpeedDeliveryInSpeed,"strMaxSpeedDeliveryInSpeed",1.025,LOCAL);
   addVar(&streamer.setting.maxSpeedDeliveryInMaxSpeed,"strMaxSpeedDeliveryInMaxSpeed",0.75,LOCAL);
   addVar(&streamer.setting.lastSheetStepPosition,"strLastSheetStepPosition",2.0,LOCAL);
   addVar(&streamer.setting.lastSheetDist,"strLastSheetDist",120.0,LOCAL);
   //addVar(&streamer.setting.offsetMarkDelay,"offsetMarkDelay",1.0,LOCAL);
   addVar(&streamer.setting.deliverOnlyInSouth,"deliverOnlyInSouth",false,LOCAL);
   addVar(&streamer.setting.tagliaTorqueCrash ,"tagliaTorqueCrash",2.0,LOCAL);
   addVar(&streamer.setting.streamerSeq2Crash ,"streamerSeq2Crash",2.0,LOCAL);
   addVar(&streamer.setting.offsetBlindDistance ,"offsetBlindDistance",0.0,LOCAL);
   addVar(&streamer.setting.tableDropDistance ,"tableDropDistance",25.0,LOCAL);
   addVar(&streamer.setting.stackJoggerSpeed ,"stackJoggerSpeed",1.05,LOCAL);
   addVar(&streamer.setting.stepSensorNo ,"stepSensorNo",0,LOCAL);
  
   
   

   addVar(&lineController.setup.cutter.secondKnife, "setupCutterSecondKnife", true, GLOBAL);        
   addVar(&lineController.setup.merger.installed, "setupMergerInstalled", true, GLOBAL);        
   addVar(&lineController.setup.merger.punchUnitInstalled, "setupMergerPunchUnitInstalled", true, GLOBAL);             
   addVar(&lineController.setup.stacker.installed, "setupStackerInstalled", false, GLOBAL);             
   addVar(&lineController.setup.stacker.spaghettiBelts, "setupStackerSpaghetti", false, GLOBAL);             
   addVar(&lineController.setup.folder.installed, "setupFolderInstalled", false, GLOBAL);
   addVar(&lineController.setup.stacker.stackerGripperUnit1, "setupStackerGripper1", true, GLOBAL);             
   addVar(&lineController.setup.stacker.stackerGripperUnit2, "setupStackerGripper2", false, GLOBAL);             
   addVar(&lineController.setup.stacker.stackerGripperUnit3, "setupStackerGripper3", false, GLOBAL);             
   addVar(&lineController.setup.stacker.stackerGripperUnit4, "setupStackerGripper4", false, GLOBAL); 
   addVar(&lineController.setup.streamer.installed , "setupStreamerInstalled", false, GLOBAL);
   //addVar(&lineController.setup.streamer.electricalVersion , "setupStreamerElectricalVersion", 0, GLOBAL);  
   
   addVar(&lineController.setting.trigger1.contactState  , "trigger1contactState", true, LOCAL);             
   addVar(&lineController.setting.trigger2.contactState  , "trigger2contactState", true, LOCAL);
   addVar(&lineController.setting.trigger3.contactState  , "trigger3contactState", true, LOCAL);
   addVar(&lineController.setting.trigger4.contactState  , "trigger4contactState", true, LOCAL);
   addVar(&lineController.setting.trigger5.contactState  , "trigger5contactState", true, LOCAL);
   addVar(&lineController.setting.trigger6.contactState  , "trigger6contactState", true, LOCAL);
   addVar(&lineController.setting.trigger1.delay  , "trigger1delay", 0.5, LOCAL);
   addVar(&lineController.setting.trigger2.delay  , "trigger2delay", 0.5, LOCAL);
   addVar(&lineController.setting.trigger3.delay  , "trigger3delay", 0.5, LOCAL);
   addVar(&lineController.setting.trigger4.delay  , "trigger4delay", 0.5, LOCAL);
   addVar(&lineController.setting.trigger5.delay  , "trigger5delay", 0.5, LOCAL);
   addVar(&lineController.setting.trigger6.delay  , "trigger6delay", 0.5, LOCAL);
   addVar(&lineController.setting.trigger1.parameter  , "trigger1parameter", 0.0, LOCAL);
   addVar(&lineController.setting.trigger2.parameter  , "trigger2parameter", 0.0, LOCAL);
   addVar(&lineController.setting.trigger3.parameter  , "trigger3parameter", 0.0, LOCAL);
   addVar(&lineController.setting.trigger4.parameter  , "trigger4parameter", 0.0, LOCAL);
   addVar(&lineController.setting.trigger5.parameter  , "trigger5parameter", 0.0, LOCAL);
   addVar(&lineController.setting.trigger6.parameter  , "trigger6parameter", 0.0, LOCAL);
   addVar(&lineController.setting.trigger1.release , "trigger1release", 0.0, LOCAL);
   addVar(&lineController.setting.trigger2.release , "trigger2release", 0.0, LOCAL);
   addVar(&lineController.setting.trigger3.release , "trigger3release", 0.0, LOCAL);
   addVar(&lineController.setting.trigger4.release , "trigger4release", 0.0, LOCAL);
   addVar(&lineController.setting.trigger5.release , "trigger5release", 0.0, LOCAL);
   addVar(&lineController.setting.trigger6.release , "trigger6release", 0.0, LOCAL);
   addVar(&lineController.setting.trigger1.source , "trigger1source", 0, LOCAL);
   addVar(&lineController.setting.trigger2.source , "trigger2source", 0, LOCAL);
   addVar(&lineController.setting.trigger3.source , "trigger3source", 0, LOCAL);
   addVar(&lineController.setting.trigger4.source , "trigger4source", 0, LOCAL);
   addVar(&lineController.setting.trigger5.source , "trigger5source", 0, LOCAL);
   addVar(&lineController.setting.trigger6.source , "trigger6source", 0, LOCAL);
   
   addVar(&lineController.setting.function1.activate , "function1activate", false, LOCAL);
   addVar(&lineController.setting.function2.activate , "function2activate", false, LOCAL);
   addVar(&lineController.setting.function3.activate , "function3activate", false, LOCAL);
   addVar(&lineController.setting.function4.activate , "function4activate", false, LOCAL);
   addVar(&lineController.setting.function1.inputA , "function1inputA", 1, LOCAL);
   addVar(&lineController.setting.function1.inputB , "function1inputB", 2, LOCAL);
   addVar(&lineController.setting.function2.inputA , "function2inputA", 3, LOCAL);
   addVar(&lineController.setting.function2.inputB , "function2inputB", 4, LOCAL);
   addVar(&lineController.setting.function3.inputA , "function3inputA", 5, LOCAL);
   addVar(&lineController.setting.function3.inputB , "function3inputB", 6, LOCAL);
   addVar(&lineController.setting.function4.inputA , "function4inputA", 1, LOCAL);
   addVar(&lineController.setting.function4.inputB , "function4inputB", 1, LOCAL);
   addVar(&lineController.setting.function1.logic , "function1logic", false, LOCAL);
   addVar(&lineController.setting.function2.logic , "function2logic", false, LOCAL);
   addVar(&lineController.setting.function3.logic , "function3logic", false, LOCAL);
   addVar(&lineController.setting.function4.logic , "function4logic", false, LOCAL);
   addVar(&lineController.setting.function1.parameter1, "function1parameter1", 0.0, LOCAL);
   addVar(&lineController.setting.function2.parameter1, "function2parameter1", 0.0, LOCAL);
   addVar(&lineController.setting.function3.parameter1, "function3parameter1", 0.0, LOCAL);
   addVar(&lineController.setting.function4.parameter1, "function4parameter1", 0.0, LOCAL);
   addVar(&lineController.setting.function1.parameter2, "function1parameter2", 0.0, LOCAL);
   addVar(&lineController.setting.function2.parameter2, "function2parameter2", 0.0, LOCAL);
   addVar(&lineController.setting.function3.parameter2, "function3parameter2", 0.0, LOCAL);
   addVar(&lineController.setting.function4.parameter2, "function4parameter2", 0.0, LOCAL);
   addVar(&lineController.setting.function1.setting1, "function1setting1", 0, LOCAL);
   addVar(&lineController.setting.function2.setting1, "function2setting1", 0, LOCAL);
   addVar(&lineController.setting.function3.setting1, "function3setting1", 0, LOCAL);
   addVar(&lineController.setting.function4.setting1, "function4setting1", 0, LOCAL);
   addVar(&lineController.setting.function1.setting2, "function1setting2", 0, LOCAL);
   addVar(&lineController.setting.function2.setting2, "function2setting2", 0, LOCAL);
   addVar(&lineController.setting.function3.setting2, "function3setting2", 0, LOCAL);
   addVar(&lineController.setting.function4.setting2, "function4setting2", 0, LOCAL);
   addVar(&lineController.setting.function1.typ, "function1typ", 0, LOCAL);
   addVar(&lineController.setting.function2.typ, "function2typ", 0, LOCAL);
   addVar(&lineController.setting.function3.typ, "function3typ", 0, LOCAL);
   addVar(&lineController.setting.function4.typ, "function4typ", 0, LOCAL);
   
   addVar(&lineController.setting.invertReady, "invertReady", false, GLOBAL);
   addVar(&lineController.setting.buzzerVolume, "buzzerVolume", 0, GLOBAL);
   addVar(&lineController.setting.ImperialUnits, "imperialUnits", 0, GLOBAL);
   
   
   /*addVar(&ebmGlue.setting.glueGunDelay, "glueGunDelay", 6, GLOBAL);
   addVar(&ebmGlue.setting.gluePattern, "gluePattern", 1, LOCAL);
   addVar(&ebmGlue.setting.guns , "glueGuns", 0, LOCAL);
   addVar(&ebmGlue.setting.glueTime, "glueTime", 4, GLOBAL);
   addVar(&ebmGlue.setting.offset, "glueOffset", 20.0, LOCAL);
   addVar(&ebmGlue.setting.scale, "glueScale", 5.0, GLOBAL);
   addVar(&ebmGlue.setting.minMarkSize, "glueMinMark", 2.0, LOCAL);
   addVar(&ebmGlue.setting.maxMarkSize, "glueMaxMark", 12.0, LOCAL);
   addVar(&ebmGlue.setting.separation, "glueSeparation", 4.5, GLOBAL);*/
   
   addVar(&lineController.setting.unwinder.maxSpeed, "unwinderMaxSpeed", 1.0, LOCAL);
}

void _INIT ProgramInit(void)
{
   initItems();
   initVars();
   addParameters();
   getParameters();
   setParameters();
}

void _CYCLIC ProgramCyclic(void)
{
   static unsigned int fStep = 0;
   static USINT fileStatus = 0;
   char row[255];
   static char fileName[255];
   static bool startUp = true;
   static unsigned int tick = 0;
   static unsigned int delayWrite = 0;

   tick++;
   
   if(startUp)
   {
      if(fStep == 0)
      {
         sprintf(fileName, "CURCONF");
         fileStatus = fileIO(FIO_READFILE, row, fileName);
         fStep = 1;
      }
      if(fStep == 1)
      {
         fileStatus = fileIO(FIO_CYCLIC, "", "");
         if(fileStatus == 0)
         {
            row[3] = 0;
            configFiles.parameter.number = atoi(row);
            configFiles.command.readFile = true;
            startUp = false;
            fStep = 0;
         }
      }      
   }
   else if(configFiles.command.readFile)
   {
      if(fStep == 0)
      {
         sprintf(fileName, "CONF%03d.CFG", configFiles.parameter.number);
         fileStatus = fileIO(FIO_READFILE, cfgBufLocal, fileName);
         fStep = 1;
      }
      if(fStep == 1)
      {
         fileStatus = fileIO(FIO_CYCLIC, "", "");
         if(fileStatus == 0)
            fStep = 2;
      }
      if(fStep == 2)
      {
         strcpy(fileName, "COMMON.CFG");
         fileStatus = fileIO(FIO_READFILE, cfgBufGlobal, fileName);
         fStep = 3;
      }      
      if(fStep == 3)
      {
         fileStatus = fileIO(FIO_CYCLIC, "", "");
         if(fileStatus == 0)
         {
            fStep = 4;
         }   
      }
      if(fStep == 4)
      {
         strcpy(fileName, "CURCONF");
         sprintf(row, "%03d-", configFiles.parameter.number);
         fileStatus = fileIO(FIO_WRITEFILE, row, fileName);
         fStep = 5;
      }      
      if(fStep == 5)
      {
         fileStatus = fileIO(FIO_CYCLIC, "", "");
         if(fileStatus == 0)
         {
            fStep = 0;
            readCfgBuf();
            getParameters();
            backupItems();
            configFiles.status.modified = false;
            configFiles.status.number = configFiles.parameter.number;
            configFiles.command.readFile = false;
         }   
      }
   }
   else if(configFiles.command.writeFile)
   {
      if(fStep == 0)
      {
         delayWrite++;
         if(delayWrite == 10)
            fStep = 1;
      }   
      if(fStep == 1)
      {
         delayWrite = 0;
         setParameters();
         writeCfgBuf();
         sprintf(fileName, "CONF%03d.CFG", configFiles.parameter.number);
         fileStatus = fileIO(FIO_WRITEFILE, cfgBufLocal, fileName);
         fStep = 2;
      }
      if(fStep == 2)
      {
         fileStatus = fileIO(FIO_CYCLIC, "", "");
         if(fileStatus == 0)
            fStep = 3;
      }
      if(fStep == 3)
      {
         strcpy(fileName, "COMMON.CFG");
         fileStatus = fileIO(FIO_WRITEFILE, cfgBufGlobal, fileName);
         fStep = 4;
      }      
      if(fStep == 4)
      {
         fileStatus = fileIO(FIO_CYCLIC, "", "");
         if(fileStatus == 0)
         {
            fStep = 5;
         }   
      }
      if(fStep == 5)
      {
         strcpy(fileName, "CURCONF");
         sprintf(row, "%03d-", configFiles.parameter.number);
         fileStatus = fileIO(FIO_WRITEFILE, row, fileName);
         fStep = 6;
      }      
      if(fStep == 6)
      {
         fileStatus = fileIO(FIO_CYCLIC, "", "");
         if(fileStatus == 0)
         {
            fStep = 0;
            readCfgBuf();
            getParameters();
            backupItems();
            configFiles.status.modified = false;
            configFiles.status.number = configFiles.parameter.number;
            configFiles.command.writeFile = false;
         }   
      }
   }
   else if(configFiles.command.setDefaults)
   {
      initItems();
      initVars();
      configFiles.status.number = configFiles.parameter.number = 0;
      getParameters();
      setParameters();
      configFiles.command.setDefaults = false;
   }
   else
   {
      if(tick % 100 == 0)
      {
         setParameters();
         configFiles.status.modified = confChanged();
         if(configFiles.parameter.number != configFiles.status.number)
            configFiles.status.modified = true;
      }
   }
}

void _EXIT ProgramExit(void)
{
}

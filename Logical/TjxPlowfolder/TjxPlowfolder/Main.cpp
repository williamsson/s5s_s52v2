#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <AsUDP.h>
#include <fsm.hpp>
#include <ctype.h>

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFFF; 


#define TJX_BUF_SIZE 256

#define TJX_CYCLIC    0
#define TJX_TERMINATE 1
#define TJX_ADDMSG    2

#define OPEN_STATE_STARTUP  0
#define OPEN_STATE_INIT     1
#define OPEN_STATE_BUSY     2
#define OPEN_STATE_READY    3
#define OPEN_STATE_ERROR    4

#define SEND_STATE_STARTUP 0
#define SEND_STATE_IDLE    1
#define SEND_STATE_BUSY    2
#define SEND_STATE_ERROR   3

#define RECV_STATE_STARTUP 0
#define RECV_STATE_IDLE    1
#define RECV_STATE_BUSY    2
#define RECV_STATE_ERROR   3

unsigned char UDPLogIP[] = "192.168.0.45";
UINT UDPLogPort = 9600;

int tjxFunc(int tjxAction, char *msg)
{
   static unsigned char tjxOut = 0;
   static int openState = OPEN_STATE_STARTUP;
   static int sendState = SEND_STATE_STARTUP;
   static int recvState = RECV_STATE_STARTUP;
   static int errorState = 0;
   static int openTimer = 0;
   static int sendTimer = 0;
   static int recvTimer = 0;
   static unsigned long currentIdent = 0;
   static UdpSend_typ udpS;
   static UdpOpen_typ udpO;
   static UdpClose_typ udpC;
   static UdpRecv_typ udpR;
   static UdpConnect_typ udpCnct;
   static unsigned char recvBuf[256];
   
   switch(tjxAction)
   {
      case TJX_CYCLIC:
         if(openState == OPEN_STATE_STARTUP) // Initialize
         {
            memset((void *)&udpS, 0, sizeof(UdpSend_typ));
            memset((void *)&udpO, 0, sizeof(UdpOpen_typ));
            memset((void *)&udpC, 0, sizeof(UdpClose_typ));
            memset((void *)&udpR, 0, sizeof(UdpRecv_typ));
            memset((void *)&udpCnct, 0, sizeof(UdpConnect_typ));
            openState = OPEN_STATE_INIT;
         }
         if(openState == OPEN_STATE_INIT) // Open comm
         {
            udpO.pIfAddr = (UDINT)0;
            udpO.port = UDPLogPort;
            udpO.options = 0;
            udpO.enable = 1;
            UdpOpen(&udpO);
            openTimer = 0;
            openState = OPEN_STATE_BUSY;
         }
         if(openState == OPEN_STATE_BUSY) // Wait for UDP open to complete
         {
            if(udpO.status == ERR_FUB_BUSY)
            {
               if(openTimer > 100)
               {
                  errorState = 100;
                  openState = OPEN_STATE_ERROR;
               }
               else
                  UdpOpen(&udpO);   
            }
            else if(udpO.status != 0)
            {
               errorState = 101;
               openState = OPEN_STATE_ERROR;
            }
            else
            {
               currentIdent = udpO.ident;
               openState = OPEN_STATE_READY;
               sendState = SEND_STATE_IDLE;
               recvState = RECV_STATE_IDLE;
            }
         }
         if(sendState == SEND_STATE_IDLE) // Transmit data
         {
            if(tjxMsg.ix != tjxOut)
            {
               udpS.pData = (DINT)tjxMsg.text[tjxOut];
               udpS.datalen = strlen((const char*)tjxMsg.text[tjxOut]);
               udpS.ident = currentIdent;
               udpS.pHost = (DINT)UDPLogIP;
               udpS.port = UDPLogPort;
               udpS.flags = 0;
               udpS.enable = 1;
               UdpSend(&udpS);
               sendTimer = 0;
               sendState = SEND_STATE_BUSY;
               tjxOut++;
            }   
         }
         if(sendState == SEND_STATE_BUSY) // Wait for UDP send to complete
         {
            if(udpS.status == ERR_FUB_BUSY)
            {
               if(sendTimer > 100)
               {
                  errorState = 102;
                  sendState = SEND_STATE_ERROR;
               }
               else
                  UdpSend(&udpS);   
            }
            else if(udpS.status != 0)
            {
               errorState = 103;
               sendState = SEND_STATE_ERROR;
            }
            else
            {
               sendState = SEND_STATE_IDLE;
            }
         }
         
         
         if(recvState == RECV_STATE_IDLE)           
         {
            udpR.enable = 1;           
            udpR.ident = currentIdent;    
            udpR.pData = (UDINT)&recvBuf[0];   
            udpR.datamax = 256;
            udpR.pIpAddr = (DINT)UDPLogIP;
            UdpRecv(&udpR);            
            recvState = RECV_STATE_BUSY; 
            recvTimer = 0;
         }
         if(recvState == RECV_STATE_BUSY)
         {
            if(udpR.status == udpERR_NO_DATA)
            {
               UdpRecv(&udpR);   
            }
            else if(udpR.status == ERR_FUB_BUSY)
            {
               UdpRecv(&udpR);   
            }
            else if(udpR.status != 0)
            {
               errorState = 105;
               recvState = RECV_STATE_ERROR;
            }
            else
            {
               if(udpR.recvlen)
               {
                  recvBuf[udpR.recvlen] = 0;
                  sprintf(tjxDebug, "%s", recvBuf);
               }
               recvState = RECV_STATE_IDLE;
            }
         }         
         if(errorState == 100) // error: 
         {      
            strcpy(tjxDebug, "UDP open: timeout");
         }
         if(errorState == 101) // error: 
         {      
            sprintf(tjxDebug, "%s %d", "UDP open", udpO.status);
         }
         if(errorState == 102) // error: 
         {      
            strcpy(tjxDebug, "UDP send: timeout");
         }
         if(errorState == 103) // error: 
         {      
            sprintf(tjxDebug, "%s %d", "UDP send", udpS.status);
         }
         if(errorState == 104) // error: 
         {      
            strcpy(tjxDebug, "UDP receive: timeout");
         }
         if(errorState == 105) // error: 
         {      
            sprintf(tjxDebug, "%s %d", "UDP receive", udpR.status);
         }
         openTimer++;
         sendTimer++;
         recvTimer++;
         break;
      
      case TJX_TERMINATE:
         if(currentIdent)
         {
            udpC.enable = 1;
            udpC.ident = currentIdent;
            UdpClose(&udpC);
         }
         break;
      
      case TJX_ADDMSG:
         strcpy((char *)tjxMsg.text[tjxMsg.ix], msg);
         tjxMsg.ix++;
         break;
      
      default:
         break;
   }   
}


void _INIT ProgramInit(void)
{
   // Insert code here 

}

void _CYCLIC ProgramCyclic(void)
{
   static unsigned int tick = 0;
   char tjxMsg[256];
   
   tick++;
   if(tick % 417 == 0)
   {   
      sprintf(tjxMsg, "To Trajexia: %u", tick / 417);
      tjxFunc(TJX_ADDMSG, tjxMsg);
   }
   tjxFunc(TJX_CYCLIC, "");   
}

void _EXIT ProgramExit(void)
{
   // Insert code here 

}


#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <AsUDP.h>
#include <fsm.hpp>
#include <ctype.h>
// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFFF; 

#define MERGER_ERROR_OFFSET   4000
#define CUTTER_ERROR_OFFSET   0

#define ERR_DRIVEERROR           1
#define ERR_SPEEDERROR           2
#define ERR_MISSINGCUTMARKS      3
#define ERR_OUTFFED_CRASH1       4
#define ERR_OUTFFED_CRASH2       5
#define ERR_OUTFFED_CRASH3       6
#define ERR_EXTRACTION           7
#define ERR_CUTTER_EM_STOP       8
#define ERR_FREE                 9
#define ERR_KNIFE_MODULE         10
#define ERR_CUTTER_EM_STOP_EXT   11
#define ERR_DRIVE_SUPPORT        12
#define ERR_DRIVE_MAINKNIFE      13
#define ERR_DRIVE_SECONDARYKNIFE 14
#define ERR_DRIVE_MAININFEED     15
#define ERR_DRIVE_REJECT         16
#define ERR_DRIVE_MERGE_INFEED   17
#define ERR_DRIVE_PUNCH          18
#define ERR_DRIVE_BUF_OUT        19
#define ERR_DRIVE_PLOW_IN        20
#define ERR_DRIVE_PLOW_OUT       21
#define ERR_SPITTER_TORQUE       22

#define ERR_MERGER_EM_STOP    1+MERGER_ERROR_OFFSET
#define ERR_MERGER_WEB_BREAK  2+MERGER_ERROR_OFFSET

#define INIT                  0
#define SET_ON                1
#define SET_OFF               2
#define IS_ON                 3
#define IS_OFF                4

#define COMPLETE              15

//limiter Defines
#define LIM_CUTTER            0
#define LIM_STACKER           1
#define LIM_BUFFER            2
#define LIM_FOLDER            3
#define LIM_SAFETY            4
#define LIM_LOADING           5
#define LIM_MAXSPEED          6
#define LIM_GRIPPER           7
#define LIM_FEEDING           8
#define LIM_LINECONTROLLER    9
#define LIM_MAXOVERSPEED      10
#define LIM_WHITEPAGES        11

void setOutput (struct output_typ *x, BOOL val);
void pulseGripper(BOOL setup);
void setOutputs(void);
void getInputs(void);
REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max);  //map function, mac
void constrain (REAL * var,REAL min,REAL max); //limits var, mac
REAL sCurve (REAL currentSpeed, REAL goalSpeed ,REAL acc, REAL startSlope, REAL endSlope); //s ramp, mac

//BOOL nissePisse;

enum machine_state
{
   STATE_STARTING_UP,
   STATE_INITIAL,
   STATE_READY,
   STATE_ERROR,
   STATE_ESTOP
};

void setOutputs(void)
{
	if (cutter.setting.forceOutputEnable.slowDown)
		physical.cutter.digital.out37  = cutter.reading.output.slowDown   = cutter.setting.forceOutputValue.slowDown;
	else physical.cutter.digital.out37  = cutter.reading.output.slowDown  = output.slowDown;    
	
	if (cutter.setting.forceOutputEnable.ready)
		physical.cutter.digital.out35 = cutter.reading.output.ready = cutter.setting.forceOutputValue.ready;
	else physical.cutter.digital.out35 = cutter.reading.output.ready  = output.ready;
	
	if (cutter.setting.forceOutputEnable.pause)
		physical.cutter.digital.out33 = cutter.reading.output.pause = cutter.setting.forceOutputValue.pause;
	else physical.cutter.digital.out33 = cutter.reading.output.pause  = output.pause;
	
	if (cutter.setting.forceOutputEnable.fanOutput)
		physical.cutter.digital.out4 = physical.cutter.digital.out6 = cutter.reading.output.fanOutput = cutter.setting.forceOutputValue.fanOutput;
	else physical.cutter.digital.out4 = physical.cutter.digital.out6 = cutter.reading.output.fanOutput  = output.fanOutput;
   
	if (cutter.setting.forceOutputEnable.airStripEject)
		physical.cutter.digital.out1 = cutter.reading.output.airStripEject = cutter.setting.forceOutputValue.airStripEject;
	else physical.cutter.digital.out1 = cutter.reading.output.airStripEject  = output.airStripEject;

   if (cutter.setting.forceOutputEnable.vacuum)
      physical.cutter.digital.out8 = cutter.reading.output.vacuum = cutter.setting.forceOutputValue.vacuum;
   else physical.cutter.digital.out8 = cutter.reading.output.vacuum  = output.vacuum;

	if (cutter.setting.forceOutputEnable.reject)
		physical.cutter.digital.out3 = !(cutter.reading.output.reject = cutter.setting.forceOutputValue.reject);
	else physical.cutter.digital.out3 = !(cutter.reading.output.reject = output.reject);

   if (cutter.setting.forceOutputEnable.granulatorStart)
      physical.cutter.digital.out13 = cutter.reading.output.granulatorStart = cutter.setting.forceOutputValue.granulatorStart;
   else physical.cutter.digital.out13 = cutter.reading.output.granulatorStart = output.granulatorStart;
   
   if (cutter.setting.forceOutputEnable.antistatic)
      physical.cutter.digital.out10 = cutter.reading.output.antistatic = cutter.setting.forceOutputValue.antistatic;
   else physical.cutter.digital.out10 = cutter.reading.output.antistatic = output.antistatic;

    if (cutter.setting.forceOutputEnable.cutMarkAbove0Below1)
        physical.cutter.digital.out12 = cutter.reading.output.cutMarkAbove0Below1 = cutter.setting.forceOutputValue.cutMarkAbove0Below1;
    else physical.cutter.digital.out12 = cutter.reading.output.cutMarkAbove0Below1 = output.cutMarkAbove0Below1;
 
    if (cutter.setting.forceOutputEnable.jobMarkAbove0Below1)
        physical.cutter.digital.out14 = cutter.reading.output.jobMarkAbove0Below1 = cutter.setting.forceOutputValue.jobMarkAbove0Below1;
    else physical.cutter.digital.out14 = cutter.reading.output.jobMarkAbove0Below1 = output.jobMarkAbove0Below1;
    
}
void getInputs(void)
{
   cutter.reading.input.knifeOutfeedCrash1 = physical.cutter.digital.in35;
   cutter.reading.input.knifeOutfeedCrash2 = physical.cutter.digital.in36;
   cutter.reading.input.knifeOutfeedCrash3 = physical.cutter.digital.in37;
   cutter.reading.input.knifeOutCrashCount1 = physical.cutter.counter.cnt1;
   cutter.reading.input.knifeOutCrashCount2 = physical.cutter.counter.cnt2;
   cutter.reading.input.knifeOutCrashCount3 = physical.cutter.counter.cnt3;
   cutter.reading.input.extractionCrash = physical.cutter.digital.in11;
   cutter.reading.input.feedButton = physical.cutter.digital.in13;
	
   cutter.reading.input.printerBlowerOn = physical.cutter.digital.in25;
	
   cutter.reading.input.eStop = physical.cutter.digital.in29;
   cutter.reading.input.topCover = physical.cutter.digital.in30;
   cutter.reading.input.frontDoor = physical.cutter.digital.in31;
   cutter.reading.input.frontDoorKnife = physical.cutter.digital.in32;
   cutter.reading.input.knifeModule = physical.cutter.digital.in33;
   cutter.reading.input.eStopExt = physical.cutter.digital.in34;
   cutter.reading.input.webTensionInfeed = physical.cutter.analog.in1; 
   cutter.reading.input.printerTensionControl = physical.cutter.digital.in28;
   
   cutter.reading.input.markReader1 = Slave[SLAVE_MAINKNIFE_INDEX].Status.DriveStatus.Trigger1;
   cutter.reading.input.markReader2 = Slave[SLAVE_MAINKNIFE_INDEX].Status.DriveStatus.Trigger2;
   cutter.reading.input.homePosMainKnife = physical.cutter.digital.in3;
   cutter.reading.ioBusModuleOk = node[0].station[1];

	//if merger installed
	merger.reading.input.feedButton = physical.merger.digital.in2;
   merger.reading.input.webBreak = physical.merger.digital.in3;
	
	merger.reading.input.eStop = physical.merger.digital.in9;
   merger.reading.input.frontDoor = physical.merger.digital.in10;
   merger.reading.input.webTensionInfeed = physical.merger.analog.in1;      
	merger.reading.ioBusModuleOk = node[8].station[2];
	
}

class FSM_machine : public FSM
{

   private:
   int debugIt;
	
   public:	
   FSM_machine() : FSM(STATE_STARTING_UP)
   {
      debugIt = 0;
   }
  
   int update(void)
   {
      switch(currentState)
      {
         case STATE_STARTING_UP:
            if(entered())
            {
               debugIt = 1;
            }
            if(currentStateTimer > 200)
            {
               debugIt = 2;
            }
            switchState(STATE_INITIAL, currentStateTimer > 1000);
            break;
         case STATE_INITIAL:
            switchState(STATE_READY, currentStateTimer > 1000);
            break;
         case STATE_READY:
            break;
         case STATE_ERROR:
            break;
      }
      showDebug++;
      machineState = currentState;
      FSM::update();
      return(getState());
   }
};
 
float modFloat(float value)
{
   unsigned long wrapAround_i  = (unsigned long)(wrapAround * 100);
   unsigned long value_i = (unsigned long)(value * 100);
   return ((float)(value_i % wrapAround_i)) / 100.0;
}

int isPosGreater(float maybeGreaterPos, float orgPos)
{
   if(maybeGreaterPos > orgPos && orgPos + (wrapAround - maybeGreaterPos) <= wrapAround / 2.0)
      return 0;  
   else if(maybeGreaterPos < orgPos && maybeGreaterPos + (wrapAround - orgPos) <=  wrapAround / 2.0)
      return 1;
   else if(maybeGreaterPos > orgPos)
      return 1;
   else
      return 0;
}

float addPos(float aPos, float distance)
{
   unsigned long wrapAround_i  = (unsigned long)(wrapAround * 100);
   unsigned long aPos_i = (unsigned long)(aPos * 100);
   unsigned long distance_i = (unsigned long)(distance * 100);
   return ((float)((aPos_i + distance_i) % wrapAround_i)) / 100.0;
}

float subPos(float aPos, float distance)
{
   unsigned long wrapAround_i  = (unsigned long)(wrapAround * 100);
   unsigned long aPos_i = (unsigned long)(aPos * 100);
   unsigned long distance_i = (unsigned long)(distance * 100);
   if(distance_i > aPos_i)
      return ((float)(aPos_i + wrapAround_i - distance_i)) / 100.0;
   else
      return ((float)(aPos_i - distance_i)) / 100.0;
}

enum event_action
{
   EVENT_TOGGLE_OUTPUT,
   EVENT_OUTPUT_ON,
   EVENT_OUTPUT_OFF,
   EVENT_CALLBACK
};

#define MAX_EVENTS 50

typedef void (*fPtr)(REAL);

int pageType = 0;

void newSheetToStacker(REAL pos)
{
   static char debugText[100];
   static char accDec[100];
   static bool sendSheetSignal = false;
   static REAL lastcuttersettingregMarkOffset;
   cutter.reading.currentPage.sheetLength = cutter.reading.sheetSize;
   if((int)GlobalCommand.Input.Parameter.Velocity > (int)(Master[0].Status.ActVelocity))
      strcpy(accDec, "ACC");
   else
      strcpy(accDec, "DEC");

   if(deliveryPending)
   {  
/*      sprintf(debugText, "To stop: %d   Pos: %f ASPos: %f", (int)subPos(stopControl.actualStopPos, Master[0].Status.ActPosition), Master[0].Status.ActPosition, stopControl.actualStopPos);
      strcpy(ULog.text[ULog.ix++], debugText);
      if(stopControl.actualStopPos > 0.0 && isPosGreater(100, subPos(stopControl.actualStopPos, Master[0].Status.ActPosition)))
      {
         //rCommToStacker.page.type = 2;
         cutter.reading.currentPage.type = 2;
         deliveryPending = false;
         if(cutter.setting.masterAcc > 400)
         {
            holdAcceleration = 600; // 1.5 seconds of slow acc after delivery
            Master[0].Parameter.Acceleration = 400;
         }
      }
      else
      {
         //rCommToStacker.page.type = 0;         
         cutter.reading.currentPage.type = 0;	
      }*/
      sprintf(debugText, "To stop: %d   Pos: %f ASPos: %f", (int)subPos(stopControl.actualStopPos, Master[0].Status.ActPosition), Master[0].Status.ActPosition, stopControl.actualStopPos);
      strcpy(ULog.text[ULog.ix++], debugText);
      if(stopControl.actualStopPos > 0.0 && isPosGreater(100, subPos(stopControl.actualStopPos, Master[0].Status.ActPosition)))
      {
         stopControl.actualStopPos = 0.0;
         //rCommToStacker.page.type = 2;
         if(cutter.command.stopNext)
            cutter.reading.currentPage.type = 3;
         else
            cutter.reading.currentPage.type = 2;
         deliveryPending = false;
         if(cutter.setting.masterAcc > 400)
         {
            holdAcceleration = 600; // 1.5 seconds of slow acc after delivery
            Master[0].Parameter.Acceleration = 400;
         }
      }
      else
      {
         //rCommToStacker.page.type = 0;         
         cutter.reading.currentPage.type = 0;	
      }
   }
   else if(stopControl.offsetPos > 0.0 && isPosGreater(cutter.setting.format - 50, subPos(stopControl.offsetPos, Master[0].Status.ActPosition)))
   {
      stopControl.offsetPos = 0.0;
      //rCommToStacker.page.type = 1;
      cutter.reading.currentPage.type = 1;		
   }
   else
   {
      //rCommToStacker.page.type = 0;          
      cutter.reading.currentPage.type = 0;
   }

   sheetDebug = sheetDebug ? false : true;
   //if(/*rCommToStacker.page.type == 2*/cutter.reading.currentPage.type == 2)
   //   sprintf(debugText, "******* Sending deliver page (Speed = %d, %s) %d, %d", (int)(Master[0].Status.ActVelocity), accDec, (int)stopControl.offsetPos, (int)Master[0].Status.ActPosition);
   //else if(/*rCommToStacker.page.type == 1*/ cutter.reading.currentPage.type == 1)
   //   sprintf(debugText, "******* Offset page (Speed = %d, %s) %d, %d", (int)(Master[0].Status.ActVelocity), accDec, (int)stopControl.offsetPos, (int)Master[0].Status.ActPosition);
   //else
   //   sprintf(debugText, "******* Normal page (Speed = %d, %s) %d, %d", (int)(Master[0].Status.ActVelocity), accDec, (int)stopControl.offsetPos, (int)Master[0].Status.ActPosition);
   //strcpy(ULog.text[ULog.ix++], debugText);
   //modbus
   cutCountKnife1x1++;
   if (cutCountKnife1x1>=1000)
   {
      cutCountKnife1x1=0;
      cutCountKnife1x1++;
   }
   if(!output.reject) 
   {
      if(sendSheetSignal) // don't send the sheet signal on first cut after reject
      {
         cutter.reading.sheetSignal = !cutter.reading.sheetSignal;  //rCommToStacker.sheetSignal = !rCommToStacker.sheetSignal;
         sprintf(ULog.text[ULog.ix++], "--- Cut (*) %d %d %f", (int)cutter.reading.currentPage.type, (int)Master[0].Status.ActPosition, Master[0].Status.ActVelocity);
         iot_processedSheets++;
         if (cutter.setting.regMarkOffset != lastcuttersettingregMarkOffset) cutter.reading.correctionValueAvg = cutter.reading.correctionValue;
         cutter.reading.correctionValueAvg = (cutter.reading.correctionValue + 9*cutter.reading.correctionValueAvg)/10.0;
         lastcuttersettingregMarkOffset = cutter.setting.regMarkOffset;
      }
      else
      {
         sprintf(ULog.text[ULog.ix++], "--- Cut (-) %d %d %f", (int)cutter.reading.currentPage.type, (int)Master[0].Status.ActPosition, Master[0].Status.ActVelocity);
         iot_rejectedSheets++;
      }
      sendSheetSignal = true;
   }
   else
   {
      sprintf(ULog.text[ULog.ix++], "--- Cut (-) %d %d %f", (int)cutter.reading.currentPage.type, (int)Master[0].Status.ActPosition, Master[0].Status.ActVelocity);
      iot_rejectedSheets++;
      sendSheetSignal = false;
   }
}

class eventHandler
{

   private:
   REAL evPos[MAX_EVENTS];
   enum event_action evAction[MAX_EVENTS];
   void *evAdr[MAX_EVENTS];
   BOOL usedEvents[MAX_EVENTS];
	
   public:	
   eventHandler()
   {
      for(int i = 0; i < MAX_EVENTS; i++)
         usedEvents[i] = false;
   }

   void update(REAL position)//int update(REAL position)
   {
      for(int i = 0; i < MAX_EVENTS; i++)
      {
         if(usedEvents[i])
         {
            if(position >= evPos[i])
            {
               switch(evAction[i])
               {
                  case EVENT_TOGGLE_OUTPUT:
                     funcPosDebug2 = position - funcPosDebug;
                     *(BOOL *)evAdr[i] = *(BOOL *)evAdr[i] ? false : true; 
                     break;
                  case EVENT_OUTPUT_ON:
                     *(BOOL *)evAdr[i] = true; 
                     break;
                  case EVENT_OUTPUT_OFF:
                     *(BOOL *)evAdr[i] = false; 
                     break;
                  case EVENT_CALLBACK:
                     (*(fPtr)evAdr[i])(position); 
                     break;
               }
               usedEvents[i] = false;
            }
         }
      }
   }
   
   BOOL addEvent(enum event_action event, REAL position, void *adr)
   {
      for(int i = 0; i < MAX_EVENTS; i++)
      {
         if(!usedEvents[i])
         {
            evPos[i] = position;
            evAction[i] = event;
            evAdr[i] = adr;
            usedEvents[i] = true;
            return true;
         }
      }
      return false; // if no free event slots
   }
   
   int clearEvents(void)
   {
      int numCleared = 0;
      for(int i = 0; i < MAX_EVENTS; i++)
      {
         if(usedEvents[i])
         {
            numCleared++;
            usedEvents[i] = false;
         }
      }
      return numCleared;
   }
};   

FSM_machine theMachine;
eventHandler theEventHandler;

REAL speedLimit(void)
{
   REAL returnSpeed = 10.0;
   float distToDeliver = 0.0;
   static float lastDistToDeliver = 0.0;
    
   cutter.reading.limiter = LIM_CUTTER;
   if(safety.reading.maximumSpeed < returnSpeed) {
		returnSpeed = safety.reading.maximumSpeed;
		cutter.reading.limiter = LIM_SAFETY;
   }
   if(buffer.reading.maximumSpeed < returnSpeed && lineController.setup.buffer.installed) {
		returnSpeed = buffer.reading.maximumSpeed;
		cutter.reading.limiter = LIM_BUFFER;
   }
	if(lineController.setup.folder.installed && lineController.setup.folder.installed)
	{
		if(folder.reading.maximumSpeed < returnSpeed)
		{
			returnSpeed = folder.reading.maximumSpeed;
			cutter.reading.limiter = LIM_FOLDER;
		}
	}
   if(!output.reject)
   {
      if(stacker.reading.maximumSpeed < returnSpeed && (lineController.setup.stacker.installed  ||  lineController.setup.streamer.installed) ) {
         returnSpeed = stacker.reading.maximumSpeed;
         cutter.reading.limiter = LIM_STACKER;
      }
		/*if (gripper.reading.maximumSpeed < returnSpeed 
			&& (lineController.setup.stacker.stackerGripperUnit1 
			|| lineController.setup.stacker.stackerGripperUnit2
			|| lineController.setup.stacker.stackerGripperUnit3))
		{
			returnSpeed = gripper.reading.maximumSpeed;
			cutter.reading.limiter = LIM_GRIPPER;
		}*/
   }
   if(loadingSpeedLimit < returnSpeed)
   {
      returnSpeed = loadingSpeedLimit;
      cutter.reading.limiter = LIM_LOADING;
   }
   if(whitePagesSpeedLimit < returnSpeed)
   {
      returnSpeed = whitePagesSpeedLimit;
      cutter.reading.limiter = LIM_LOADING;
   }
   if (lineController.reading.maximumSpeed < returnSpeed)
   {
      returnSpeed = lineController.reading.maximumSpeed;
      cutter.reading.limiter = LIM_LINECONTROLLER;
   }

   if(startRejectPos != 0.0)
   {
      if(returnSpeed > 1.5)
      {
         returnSpeed = 1.5;
         cutter.reading.limiter = LIM_LOADING;
      }
   }
   
   if(cutter.setting.maxPrinterOverSpeed > 0)
   {
      if(buffer.reading.infeedSpeed + (cutter.setting.maxPrinterOverSpeed / 1000.0) < returnSpeed)
      {
         returnSpeed = buffer.reading.infeedSpeed + (cutter.setting.maxPrinterOverSpeed / 1000.0);
         cutter.reading.limiter = LIM_MAXOVERSPEED;
      }
   }
	
   if(cutter.setting.noMarksSlowSpeed > 0.0)
   {
      if(cutter.reading.goodPages < 10 || cutter.reading.wastePages > 20)
         if(returnSpeed > cutter.setting.noMarksSlowSpeed)
            returnSpeed = cutter.setting.noMarksSlowSpeed;
   }
   
   //if(stopControl.actualStopPos > 0.0)
   //{
   //    distToDeliver = subPos(stopControl.actualStopPos, Master[0].Status.ActPosition);  
   //    returnSpeed = returnSpeed > deliverDecMinSpeed ? (deliverDecRate * distToDeliver) / 1000.0 : returnSpeed; // was 0.8 and 2.0 * distToDeliver
   //}
   if(stopControl.slowSpeedPos > 0.0)
   {
      distToDeliver = subPos(stopControl.slowSpeedPos, Master[0].Status.ActPosition);
      if(distToDeliver > lastDistToDeliver) // wrap around
      {
         sprintf(ULog.text[ULog.ix++], "dTD = %f, reset slowSpeedPos", distToDeliver);
         distToDeliver = 0.0;
         stopControl.slowSpeedPos = 0.0;
      }
      lastDistToDeliver = distToDeliver;
      returnSpeed = returnSpeed > cutter.setting.deliverDecMinSpeed ? (cutter.setting.deliverDecStartSpeed * distToDeliver) / 1000.0 : returnSpeed; // was 0.8 and 2.0 * distToDeliver
      if(returnSpeed < cutter.setting.deliverDecMinSpeed)
         returnSpeed = cutter.setting.deliverDecMinSpeed;
   }
   else
      lastDistToDeliver = wrapAround;
   
   
   //if(commandStop_state)
   //   returnSpeed = returnSpeed > 0.98 ? 0.98 : returnSpeed;
   
   return returnSpeed;	
}

void swap(int *a, int *b)
{
   int t;
   t = *a;
   *a = *b;
   *b = t;
}
int getNumerator(int currentPos, int standardPos, int slowPos, int fastPos, int defaultNumerator, int minNumerator, int maxNumerator)
{
   float cp;
   float currentPerc;
   float standardPerc;
   float slowPerc;
   float fastPerc;
   float x1;
   float x2;
   float fDefNum = defaultNumerator;

   if(fastPos < slowPos)
   {
      swap(&fastPos, &slowPos);
      currentPos = fastPos - (currentPos - slowPos);
      //standardPos = fastPos - (standardPos - slowPos);
   }

   if(currentPos < slowPos)
      return 0;
   if(currentPos > fastPos)
      return maxNumerator;

   // map to percent
   cp = (fastPos - slowPos) / 100.0;
   fastPerc = 100.0;
   currentPerc = (currentPos - slowPos) / cp;
   //standardPerc = (standardPos - slowPos) / cp;
   standardPerc = standardPos;
   slowPerc = 0.0;

   if(currentPerc < 10.0)
      return 0;
   x1 = (fDefNum - minNumerator)  / (standardPerc);   
   x2 = (maxNumerator - fDefNum)  / (100.0 - standardPerc);
   if(currentPerc < standardPerc)
      return (int) (minNumerator + (currentPerc * x1));  
   else
      return (int) (fDefNum + ((currentPerc - standardPerc) * x2));  
}

/*void readAxisStatus (void)
{
   cutter.reading.axisStatus.mergerInfeed.actualSpeed = Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActVelocity;
   cutter.reading.axisStatus.mergerInfeed.pos =  Slave[SLAVE_MERGE_INFEED_INDEX].Status.ActPosition;
   cutter.reading.axisStatus.mergerInfeed.synchronized = Slave[SLAVE_MERGE_INFEED_INDEX].AxisState.SynchronizedMotion;
   Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus
   cutter.reading.axisStatus.mergerInfeed.errorID = 
   if ()

}*/

void _INIT ProgramInit(void)
{
   // Insert code here 
   showDebug = 1;
   cutter.setting.masterDec = 6074;
	cutter.setting.masterAcc = 300;	
   cutter.reading.startUpSeq = merger.reading.startUpSeq = 0;
}



void _CYCLIC ProgramCyclic(void)
{
   //REAL speedDiff;
   REAL highPos, lowPos, curPos, midPos;
   static unsigned int logAlive = 0;
   static unsigned int airStripEjectCounter = 0;
   static UDINT tmpTime;     
   static unsigned int oldSheetCounter = 0;
   static BOOL startup=1;
   static BOOL homingStarted;
   static BOOL oldDeliverNowRequestToggle = false;
   static BOOL ODPWasZero = true;
   static BOOL curReject = false;
   static REAL distToNextCut = 0.0;
   static REAL loopStopPos = 0.0;
   static REAL pageToStacker = 0.0;
   static int loopStopState = 0;
   static char debugText[100];
   static int simPageCounter = 0;
   static UDINT oldErrType = 0;
   static UDINT oldErrSubType = 0;
   static int confirmDriveErrors = 0;
   static int cDEtime = 0;
   static unsigned int stopTimer = 0;
   static unsigned int tensionTimer = 0;
   static USINT oldCommandStopState = 0;
   static UDINT markCounter = 0;
   static REAL debugMarkWidth = 0.0;
   static BOOL startupFinished = false;
   static BOOL lastMergerFeedButton;
   static BOOL lastCutterFeedButton;
   static REAL lastToggleCrash_1 = 0.0;
   static REAL lastToggleCrash_2 = 0.0;
   static REAL lastToggleCrash_3 = 0.0;
   static UINT outCrashCount_1 = 0;
   static UINT outCrashCount_2 = 0;
   static UINT outCrashCount_3 = 0;
   static UINT extractionCrashTime = 0;
   static UINT feedRisingEdge;
   static bool wasEMstopCutterExt = false;
   static bool wasEMstopCutter = false;
   static bool wasEMstopMerger = false;
   static bool knifeRemoved = false;
   static REAL rejectSpeed = 0.0;
   static UINT keepRejectRunning = 0;
   static UINT continueReject = 0;
   static UINT releaseLoadingSpeedLimit = 0;
   static BOOL oldMarkStatus = false;
   static USINT m50WebBreakCnt;
   static BOOL wasStoppedByBuffer = false;
   static UINT ftcTimeOut = 0;
   //static UDINT tick;
   
   getInputs();
   
   cutter.setting.stopWhenDelivering = false; //mac

   if(GlobalCommand.Output.Status.AllDrivesEnable)
   {
      if(lineController.setup.buffer.installed)
      {
         if(buffer.reading.noPaper && GlobalCommand.Output.Status.AllHomingsOk)
         {
            wasStoppedByBuffer = true;
            Master[0].Command.Stop = true;
         }
         else
         {
            Master[0].Command.Stop = false;
            if(wasStoppedByBuffer)
            {
               if(machineStarted)
               {   
                  Master[0].Command.MoveVelocity = true;
                  wasStoppedByBuffer = false;
               }
            }
         }
      }
      else
         Master[0].Command.Stop = false;
   }

   //tick++;
   cutter.reading.webSpeed_mm = cutter.reading.webSpeed * 60;
   cutter.reading.webSpeed_fpm = cutter.reading.webSpeed_mm * 3.2808;
   cutter.reading.formatImperial = cutter.setting.format / 25.4;
   
   cutter.reading.highPosMax = cutter.setting.bufferLowPos-4;
   cutter.reading.lowPosMin = cutter.setting.bufferHighPos+5;
   
   if (startup)
   {
      pulseGripper(1);
      startup = 0;
   }

   // feed to next stop pos
   if(cutter.command.feedToCut && feedToCutState == 0)
   {
      cutter.command.feedToCut = false;
      feedToCutState = 1;
   }
   else if(feedToCutState == 1)
   {
      if(Master[0].Status.ActVelocity == 0)
      {
         cutter.command.start = true;
         ftcTimeOut = 0;
         feedToCutState = 2;
      }
      else
      {
         feedToCutState = 101;
      }
   }
   else if(feedToCutState == 2)
   {
      if(Master[0].Status.ActVelocity > 95)
      {
         //         cutter.command.stopRequest = true;
         cutter.command.stop = true;
         ftcTimeOut = 0;
         feedToCutState = 3;
      }
      else
      {
         ftcTimeOut++;
         if(ftcTimeOut > 400) // no movement in one second
         {
            feedToCutState = 102;
            //            cutter.command.stopRequest = true;
            cutter.command.stop = true;
         }
      }
   }
   else if(feedToCutState == 3)
   {
      if(Master[0].Status.ActVelocity = 0)
      {
         feedToCutState = 103;
      }
      else
      {
         ftcTimeOut++;
         if(ftcTimeOut > 800) // no stop in two seconds
         {
            feedToCutState = 104;            
         }
      }
   }
   else if(feedToCutState > 100)
   {
      debugFTC = feedToCutState;
      feedToCutState = 0;
   }
   
   if (tmpTime != lineController.reading.timeSinceStart) //new second
   {
      if (lineController.reading.timeSinceStart%9 == 2 && merger.reading.startUpSeq < 13)  //140 sec
      {
         merger.reading.startUpSeq++;
      }
      if (merger.reading.startUpSeq==13) // then wait for bus module to be ok
      {
         if (merger.reading.ioBusModuleOk) merger.reading.startUpSeq++;
      }
      if (Slave[SLAVE_MERGE_INFEED_INDEX].Status.DriveStatus.DriveEnable && merger.reading.ioBusModuleOk && merger.reading.startUpSeq>1) merger.reading.startUpSeq = COMPLETE; 
        
      if (!lineController.setup.merger.installed) merger.reading.startUpSeq = COMPLETE;
        
      if (lineController.reading.timeSinceStart%11 == 9 && cutter.reading.startUpSeq < 13) cutter.reading.startUpSeq++;
      if (cutter.reading.startUpSeq==13) // then wait for bus module to be ok
      {
         if (cutter.reading.ioBusModuleOk) cutter.reading.startUpSeq++;
      }
      if (Slave[SLAVE_MAININFEED_INDEX].Status.DriveStatus.DriveEnable && cutter.reading.ioBusModuleOk && cutter.reading.startUpSeq>1) cutter.reading.startUpSeq = COMPLETE;
   }
   tmpTime = lineController.reading.timeSinceStart;
   if (feedRisingEdge) feedRisingEdge--;
   if (lastMergerFeedButton != merger.reading.input.feedButton)
   {
      if (feedRisingEdge) cutter.reading.fastFeed = true;
      feedRisingEdge = 200;
      if (merger.reading.input.feedButton) cutter.command.feed = true;
      else cutter.command.feed = false;
      buffer.command.resetInnerLoopFault = true;
   }
   lastMergerFeedButton = merger.reading.input.feedButton;
  
   if (lastCutterFeedButton != cutter.reading.input.feedButton)
   {
      if (feedRisingEdge) cutter.reading.fastFeed = true;
      feedRisingEdge = 200;
      if (cutter.reading.input.feedButton) cutter.command.feed = true;
      else cutter.command.feed = false;
      buffer.command.resetInnerLoopFault = true;
   }
   lastCutterFeedButton = cutter.reading.input.feedButton;

   if (!merger.reading.input.feedButton && !cutter.reading.input.feedButton) cutter.reading.fastFeed = false;
   
   if ((!merger.reading.input.frontDoor && lineController.setup.merger.installed) || !cutter.reading.input.frontDoor) {
      cutter.reading.fastFeed = false;
      cutter.command.feed = false;
   }
	
   // handle power
   if(powerOff)
      GlobalCommand.Input.Command.Power = false;
   else
      GlobalCommand.Input.Command.Power = globalPower;
   
   if(debugMarkWidth != OMRMark.status.width)
   {
      markCounter++;
      sprintf(ULog.text[ULog.ix++], "OMR MARK: Width:%f  Speed:%f - %d", OMRMark.status.width, Master[0].Status.ActVelocity, markCounter);
      if(OMRMark.status.width > 55.0 && OMRMark.status.width < 65.0)
      {
         sprintf(ULog.text[ULog.ix++], "OMR MARK: OFFSET FOUND - %d", markCounter);
         markCounter = 0;
      }
      debugMarkWidth = OMRMark.status.width;
   }
      	
   GUI_status.statusBarInit = GlobalCommand.Output.Status.AllDrivesEnable ? 1 : 0;
   GUI_status.statusBarDrivesDisabled = GlobalCommand.Output.Status.AllControllersReady ? 1 : 0;
   GUI_status.statusBarNotLoaded = GlobalCommand.Output.Status.AllSlavesSynchronizedMotion ? 1 : 0;
   GUI_status.statusBarStopped = machineStarted ? 1 : 0;
	
   GUI_status.configChanged = configFiles.status.modified ? 0 : 1;
	
   GUI_status.loadButton = (!GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && GlobalCommand.Output.Status.AllControllersReady && (!lineController.setup.folder.installed || folder.reading.status.state != Status_ManualLoad)) ? 0 : 1;
   GUI_status.reloadButton = (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && GlobalCommand.Output.Status.AllControllersReady && !machineStarted) ? 0 : 1;
   GUI_status.skipToPrintButton = (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && GlobalCommand.Output.Status.AllControllersReady && skipToPrintLength == 0.0 && !machineStarted) ? 0 : 1;
   GUI_status.skipToPrintAmount = (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && GlobalCommand.Output.Status.AllControllersReady && cutter.reading.pagesToSkip > 0) ? 0 : 1;
   //GUI_status.markLED = cutter.reading.input.markReader ? 0 : 1;
   GUI_status.startButton = (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && !machineStarted) ? 0 : 1;   
   
   GUI_status.jogKnifeButton = 1;
   
   GUI_status.stopNextButton = 1;
   if (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && cutter.reading.setSpeed > 0.05)
      GUI_status.stopNextButton = 0;
   
   GUI_status.feedButton = (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && !machineStarted) ? 0 : 1;
   GUI_status.powerButton = GlobalCommand.Output.Status.AllDrivesEnable ? 0 : 1;
   GUI_status.cutPlusButton = (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && GlobalCommand.Output.Status.AllControllersReady) ? 0 : 1;
   GUI_status.cutMinusButton = (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion && GlobalCommand.Output.Status.AllControllersReady) ? 0 : 1;
   if(GlobalCommand.Output.Status.AllDrivesEnable && !startupFinished)
   {
      startupFinished = true;
      //folder.command.beep = true;  //F50Beep = true;
      lineController.command.beep = true;
   }
   
   if(machineError.active && (GUI_status.errorActive & 0x01))
   {
      cutter.command.stop = true;
   }
   
   if(confirmDriveErrors)
   {
      if(confirmDriveErrors % 2 == 0)
         GlobalCommand.Input.Command.ErrorAcknowledge = true;
      else
         GlobalCommand.Input.Command.ErrorAcknowledge = false;
      confirmDriveErrors--;
   }
   
   
   GUI_status.errorActive = machineError.active ? 0 : 1;
   //GUI_status.errorActive = 1;
   if(GUI_command.confirmError)
   {
      if(cutter.reading.input.eStopExt && wasEMstopCutterExt ||
         cutter.reading.input.eStop && wasEMstopCutter ||
         merger.reading.input.eStop && wasEMstopMerger ||
         cutter.reading.input.knifeModule && knifeRemoved)
      {
         if(safety.reading.resetPossible)
         {
            safety.command.eStopReset = true;
            GUI_command.confirmError = true;
            confirmDriveErrors = 10;
         } 
      }
      if(cutterDriveError)
      {
         cutterDriveError = false;
         confirmDriveErrors = 10;
      }
      
      //clear it
      machineError.active = GUI_command.confirmError = false;      
      machineError.errType = machineError.errSubType = 0;
   }
  
   if(cutterDriveError && !machineError.active && !confirmDriveErrors)
   {
      if(cDEtime < 10)
         cDEtime++;
      if(cDEtime >= 10)
      {   
         machineError.active = true;
         machineError.errType = ERR_DRIVEERROR;
         machineError.errSubType = 5;
      }   
   }
   if(!cutterDriveError)
      cDEtime = 0;
   
   if(machineError.active) 
   {
      if(oldErrType != machineError.errType || oldErrSubType != machineError.errSubType)
      {
         sprintf(debugText, "ERROR: %d, %d", machineError.errType, machineError.errSubType);
         strcpy(ULog.text[ULog.ix++], debugText);
         oldErrType = machineError.errType;
         oldErrSubType = machineError.errSubType;
      }
      //cutter.reading.status.state = Status_Error; //TODO
   }

   // cutter state
   if(machineError.active)
   {
      if (machineError.errType>MERGER_ERROR_OFFSET && machineError.errType<MERGER_ERROR_OFFSET+1000)
      {
         merger.reading.status.state = Status_Error;
         merger.reading.status.stopped = true;
         merger.reading.status.warning = false;
      }
      else if (machineError.errType<CUTTER_ERROR_OFFSET+1000)
      {
         cutter.reading.status.state = Status_Error;
         cutter.reading.status.stopped = true;
         cutter.reading.status.warning = false; 
      }
   }
   else
   {
      if (GlobalCommand.Output.Status.AllSlavesSynchronizedMotion) {
         cutter.reading.status.state = Status_Ready;
         cutter.reading.status.stopped = machineStarted ? 0 : 1;
         cutter.reading.status.warning = false;
         homingStarted = false;
      }
      else {
         cutter.reading.status.state = Status_NotLoaded;
         cutter.reading.status.warning = GlobalCommand.Output.Status.AllControllersReady ? 0 : 1;
         cutter.reading.status.stopped = true;
         if (GlobalCommand.Input.Command.Home) homingStarted = true;
         if (!globalPower) homingStarted = false;
         if (homingStarted) cutter.reading.status.state = Status_Loading;
         else cutter.reading.status.state = Status_NotLoaded;
      }
      merger.reading.status.state = cutter.reading.status.state;
      merger.reading.status.warning = !merger.reading.input.frontDoor;
      merger.reading.status.stopped = cutter.reading.status.stopped;
      
   }
   
   /*// handle mark beep
   if(cutter.setting.markBeep)
   {
      if(cutter.reading.input.markReader != oldMarkStatus)
      {
         oldMarkStatus = cutter.reading.input.markReader;
         lineController.command.beep = true;
      }
   }*/
     
   // check for drive errors
   if((Slave[SLAVE_SUPPORT_INDEX].Error.AxisErrorCount || Slave[SLAVE_SUPPORT_INDEX].Error.FunctionBlockErrorCount) && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_SUPPORT;
      machineError.errSubType = Slave[SLAVE_SUPPORT_INDEX].Error.ErrorRecord.Number;
   }
   if((Slave[SLAVE_MAINKNIFE_INDEX].Error.AxisErrorCount || Slave[SLAVE_MAINKNIFE_INDEX].Error.FunctionBlockErrorCount) && !machineError.active)
   {
      if(!GlobalCommand.Input.Command.Power)
      {
         confirmDriveErrors = 10;
      }
      else  
      {
         cutterDriveError = true;
         machineError.active = true;
         machineError.errType = ERR_DRIVE_MAINKNIFE;
         machineError.errSubType = Slave[SLAVE_MAINKNIFE_INDEX].Error.ErrorRecord.Number;      
      }
   }
   if((Slave[SLAVE_SECONDARYKNIFE_INDEX].Error.AxisErrorCount || Slave[SLAVE_SECONDARYKNIFE_INDEX].Error.FunctionBlockErrorCount) && lineController.setup.cutter.secondKnife && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_SECONDARYKNIFE;
      machineError.errSubType = Slave[SLAVE_SECONDARYKNIFE_INDEX].Error.ErrorRecord.Number;      
   }
   if((Slave[SLAVE_MAININFEED_INDEX].Error.AxisErrorCount || Slave[SLAVE_MAININFEED_INDEX].Error.FunctionBlockErrorCount) && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_MAININFEED;
      machineError.errSubType = Slave[SLAVE_MAININFEED_INDEX].Error.ErrorRecord.Number;      
   }
   if((Slave[SLAVE_REJECT_INDEX].Error.AxisErrorCount || Slave[SLAVE_REJECT_INDEX].Error.FunctionBlockErrorCount) && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_REJECT;
      machineError.errSubType = Slave[SLAVE_REJECT_INDEX].Error.ErrorRecord.Number;      
   }
   if((Slave[SLAVE_MERGE_INFEED_INDEX].Error.AxisErrorCount || Slave[SLAVE_MERGE_INFEED_INDEX].Error.FunctionBlockErrorCount) && lineController.setup.merger.installed && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_MERGE_INFEED;
      machineError.errSubType = Slave[SLAVE_MERGE_INFEED_INDEX].Error.ErrorRecord.Number;      
   }
   if((Slave[SLAVE_PUNCH_INDEX].Error.AxisErrorCount || Slave[SLAVE_PUNCH_INDEX].Error.FunctionBlockErrorCount) && lineController.setup.merger.installed && lineController.setup.merger.punchUnitInstalled && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_PUNCH;
      machineError.errSubType = Slave[SLAVE_PUNCH_INDEX].Error.ErrorRecord.Number;      
   }
   if((Slave[SLAVE_BUF_OUT_INDEX].Error.AxisErrorCount || Slave[SLAVE_BUF_OUT_INDEX].Error.FunctionBlockErrorCount) && lineController.setup.buffer.installed && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_BUF_OUT;
      machineError.errSubType = Slave[SLAVE_BUF_OUT_INDEX].Error.ErrorRecord.Number;      
   }
   if((Slave[SLAVE_PLOW_IN_INDEX].Error.AxisErrorCount || Slave[SLAVE_PLOW_IN_INDEX].Error.FunctionBlockErrorCount) && lineController.setup.folder.installed && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_PLOW_IN;
      machineError.errSubType = Slave[SLAVE_PLOW_IN_INDEX].Error.ErrorRecord.Number;      
   }
   if((Slave[SLAVE_PLOW_OUT_INDEX].Error.AxisErrorCount || Slave[SLAVE_PLOW_OUT_INDEX].Error.FunctionBlockErrorCount) && lineController.setup.folder.installed && !machineError.active)
   {
      cutterDriveError = true;
      machineError.active = true;
      machineError.errType = ERR_DRIVE_PLOW_OUT;
      machineError.errSubType = Slave[SLAVE_PLOW_OUT_INDEX].Error.ErrorRecord.Number;      
   }
   
   // handle cutter EM-stop
   if(!cutter.reading.input.eStop && safety.reading.initialized && !machineError.active)
   {
      machineError.active = true;
      machineError.errType = ERR_CUTTER_EM_STOP;
      machineError.errSubType = 0;
      wasEMstopCutter = true;
   }
   // handle merger EM-stop external
   if(!cutter.reading.input.eStopExt && safety.reading.initialized && !machineError.active)
   {
      machineError.active = true;
      machineError.errType = ERR_CUTTER_EM_STOP_EXT;
      machineError.errSubType = 0;
      wasEMstopCutterExt = true;
   }
   // handle merger EM-stop
   if(lineController.setup.merger.installed && !merger.reading.input.eStop && safety.reading.initialized && !machineError.active)
   {
      machineError.active = true;
      machineError.errType = ERR_MERGER_EM_STOP;
      machineError.errSubType = 0;
      wasEMstopMerger = true;
   }
   // handle knife module removed
   if(!cutter.reading.input.knifeModule && safety.reading.initialized && !machineError.active)
   {
      machineError.active = true;
      machineError.errType = ERR_KNIFE_MODULE;
      machineError.errSubType = 0;
      knifeRemoved = true;
   }
   
   //handle merger web break
   if(lineController.setup.merger.installed && merger.setting.enabled && merger.reading.input.webBreak && !machineError.active)
   {
      if(Master[0].Status.ActVelocity > 10 && !cutter.reading.input.feedButton && !merger.reading.input.feedButton)
      {
         if (m50WebBreakCnt<255) m50WebBreakCnt++;
      }
      else m50WebBreakCnt = 0;
      
      if (m50WebBreakCnt>250) {
         machineError.active = true;
         machineError.errType = ERR_MERGER_WEB_BREAK;
         machineError.errSubType = 0;
      }
         
   }
   else {
      m50WebBreakCnt = 0;
   }
   
   if(!globalPower)
      rejectSheets = false;
   if(cutter.command.rejectSheets)
   {
      output.reject = true;  //DO_reject = true; open reject
      cutter.reading.status.warning = true;
   }
   else
   {
      output.reject = rejectSheets;	//DO_reject = rejectSheets;
   }
#ifdef NEVER
   if(curReject && !output.reject && releaseLoadingSpeedLimit == 0)
   {
      releaseLoadingSpeedLimit = 10; // 400
      loadingSpeedLimit = 0.5;
      sprintf(ULog.text[ULog.ix++], "*** Setting release speed limit %f : %f (%d)", loadingSpeedLimit, Master[0].Status.ActVelocity, (int)commandStop_state);
   }
   if(releaseLoadingSpeedLimit)
   {
      releaseLoadingSpeedLimit--;
      if(releaseLoadingSpeedLimit == 0)
      {
         sprintf(ULog.text[ULog.ix++], "*** Back to 5.0 : %f", Master[0].Status.ActVelocity);
         loadingSpeedLimit = 5.0; // speed limit reset when reject gate is closed. Was set in MainKnife::slaveCyclic.st.
      }
   }
#endif    
   curReject = output.reject;

   // OMR reader control 
   if(cutter.setting.cutMarkAbove0Below1)
      output.cutMarkAbove0Below1 = false;
   else 
      output.cutMarkAbove0Below1 = true;
 
   if(cutter.setting.jobMarkAbove0Below1)
      output.jobMarkAbove0Below1 = false;
   else 
      output.jobMarkAbove0Below1 = true;
    
   // fan control
   if(Master[0].Status.ActVelocity > 1000 || cutter.reading.input.printerBlowerOn || cutter.reading.input.printerTensionControl || buffer.reading.fixInfeedRunning) //mac
   {
      //DO_fanOutput = true;
      output.fanOutput = true;
      fanOutputCnt = 4000; // 10 seconds
   }
   else
   {
      if(fanOutputCnt)
      {
         fanOutputCnt--;
         if(fanOutputCnt == 0)
         {
            //DO_fanOutput = false;
            output.fanOutput = false;
         }
      }
   }

   // vacuum control
   if(cutter.setting.stripCut && (Master[0].Status.ActVelocity > 1 || cutter.reading.input.printerBlowerOn || cutter.reading.input.printerTensionControl || cutter.setting.forceVacuum))
   {
      output.vacuum = true;
      if(Master[0].Status.ActVelocity > 1 || cutter.reading.input.printerBlowerOn || cutter.reading.input.printerTensionControl || buffer.reading.fixInfeedRunning) //mac
         vacuumOutputCnt = 5 * 24000; // 5 minutes on if automatically started
   }
   else
   {
      if(vacuumOutputCnt)
         vacuumOutputCnt--;
      else
         output.vacuum = false;
   }

   // granulator control
   if(Master[0].Status.ActVelocity > 1 || (cutter.reading.input.printerBlowerOn || cutter.reading.input.printerTensionControl || buffer.reading.fixInfeedRunning)) //mac
   {
      output.granulatorStart = true; 
      if (Master[0].Status.ActVelocity>1000) //mac 
         output.fanOutput = true; //mac
      output.antistatic = cutter.reading.input.topCover; //mac
      granulatorOutputCnt = 10 * 400; // 10 seconds on when started
   }
   else
   {
      if(granulatorOutputCnt) {
         granulatorOutputCnt--;
         output.antistatic = cutter.reading.input.topCover; //mac
      }
      else
         output.granulatorStart = false; //mac
      output.antistatic = false;
      output.fanOutput = false;
   }
   
   // reject motor control
   if(output.reject)
      continueReject = 10 * 400;
   if(Master[0].Status.ActVelocity > 1 && ((output.reject || cutter.reading.processingWhite) || continueReject || (startRejectPos != 0.0)))
   {
      if(continueReject)
         continueReject--;
      if(Master[0].Status.ActVelocity > 250)
         rejectSpeed = Master[0].Status.ActVelocity * 1.5;
      else
         rejectSpeed = 250 * 1.5;
      keepRejectRunning = 10 * 400; // continue to run reject 10 seconds after infeed has stopped
      Slave[SLAVE_REJECT_INDEX].Parameter.Velocity = rejectSpeed;
      Slave[SLAVE_REJECT_INDEX].Parameter.Acceleration = 10000; // 2000
      Slave[SLAVE_REJECT_INDEX].Parameter.Deceleration = 2000; //Master[0].Parameter.Deceleration;
      Slave[SLAVE_REJECT_INDEX].Command.MoveVelocity = true;
   }
   else
   {
      if(keepRejectRunning)
      {
         keepRejectRunning--;
         if(keepRejectRunning == 0)
         {
            Slave[SLAVE_REJECT_INDEX].Parameter.Velocity = 0;
            Slave[SLAVE_REJECT_INDEX].Command.MoveVelocity = true;
         }
      }
   }

   
   // handle gui parameters
   Slave[SLAVE_SUPPORT_INDEX].Parameter.RatioDenominator = 10600 - cutter.setting.supportOverSpeed;
   Slave[SLAVE_MAININFEED_INDEX].Parameter.RatioDenominator = 10000 - cutter.setting.mainInfeedOverSpeed;
   //Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.RatioDenominator = 10000 - merger.setting.infeedOverSpeed;
   
   // Tension control                                    getNumerator(currentPos, standardPos (percent), slowPos, fastPos, defaultNumerator, minNumerator, maxNumerator);
   tensionTimer++;
   if(tensionTimer % 10 == 0)
   {
      if(lineController.setup.folder.installed)
         Slave[SLAVE_BUF_OUT_INDEX].Parameter.RatioNumerator = 10 * getNumerator(folder.reading.input.webTensionInfeed, folder.setting.tensionInIdealPerc, folder.setting.tensionInLowPos, folder.setting.tensionInHighPos, 1000, 950, 1050);
         //Slave[SLAVE_BUF_OUT_INDEX].Parameter.RatioNumerator = tempBufRatio;
      else if(lineController.setup.merger.installed)
         Slave[SLAVE_BUF_OUT_INDEX].Parameter.RatioNumerator = 10 * getNumerator(merger.reading.input.webTensionInfeed, merger.setting.tensionIdealPerc, merger.setting.tensionLowPos, merger.setting.tensionHighPos, 1000, 850, 1150);
      else
         Slave[SLAVE_BUF_OUT_INDEX].Parameter.RatioNumerator = 10 * getNumerator(cutter.reading.input.webTensionInfeed, cutter.setting.tensionIdealPerc, cutter.setting.tensionLowPos, cutter.setting.tensionHighPos, 1000, 950, 1050); //fiserv
      Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.RatioNumerator = 10 * getNumerator(cutter.reading.input.webTensionInfeed, cutter.setting.tensionIdealPerc, cutter.setting.tensionLowPos, cutter.setting.tensionHighPos, 1000, 950, 1050); //fiserv
      Slave[SLAVE_PLOW_OUT_INDEX].Parameter.RatioNumerator = 10 * getNumerator(cutter.reading.input.webTensionInfeed, cutter.setting.tensionIdealPerc, cutter.setting.tensionLowPos, cutter.setting.tensionHighPos, 1000, 950, 1050);//950, 1050); //fiserv
      if (folder.setting.foldingType == 0)
         Slave[SLAVE_PLOW_IN_INDEX].Parameter.RatioNumerator = 9500 + (folder.setting.tensionIdealPerc * 10);
      else
         Slave[SLAVE_PLOW_IN_INDEX].Parameter.RatioNumerator = 10 * getNumerator(folder.reading.input.webTensionInside, folder.setting.tensionIdealPerc, folder.setting.tensionLowPos, folder.setting.tensionHighPos, 1000, 950, 1050); //fiserv
      if((cutter.reading.input.feedButton || merger.reading.input.feedButton) &&
         Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.RatioNumerator == 0)
      {
         Slave[SLAVE_MERGE_INFEED_INDEX].Parameter.RatioNumerator = 9500;
         Slave[SLAVE_PLOW_OUT_INDEX].Parameter.RatioNumerator = 9500;
      }
   }
   
   if(!holdAcceleration)
      Master[0].Parameter.Acceleration = cutter.setting.masterAcc;
   else
      holdAcceleration--;
   Master[0].Parameter.Deceleration = cutter.setting.masterDec;
	
   //mac testings sRamp...
   Master[0].Parameter.Acceleration = sCurve (Master[0].Status.ActVelocity, GlobalCommand.Input.Parameter.Velocity ,cutter.setting.masterAcc, 100 , 50); //125,50
   
   // update run state
   //cutter.reading.runState.ready =
   //cutter.reading.ready =  machineStarted;
   //cutter.reading.runState.error = 0;
   //cutter.reading.error = 0;
   cutter.reading.maximumSpeed = 5.0;
	
   //cutter.reading.runState.speedLimit = 5;
   
   //AvgFrameTemperature = ((REAL)(frame.temp_3_right) + (REAL)(frame.temp_1_left))/20.0;
   
   logAlive++;
   if(logAlive == (60 * 416) - 30) // once per minute
   {
      //strcpy(ULog.text[ULog.ix++], "Not dead");
      //sprintf(debugText, "Temp: %.1f;%.1f;%.1f;%.1f", cutter.reading.tempLeft, cutter.reading.tempRight, cutter.reading.tempFront, cutter.reading.tempRear);
      //strcpy(ULog.text[ULog.ix++], debugText);
      logAlive = 0;
   }
   //if(logAlive % 416 == 0) // once per second
   //{
   //   sprintf(debugText, "ss:%d s:%d b:%d", (int)commandStop_state, (int)(1000 * sourceS50.maximumSpeed), (int)(1000 * buffer.reading.maximumSpeed));
   //   strcpy(ULog.text[ULog.ix++], debugText);
   //}
   
   theMachine.update();
   theEventHandler.update(Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition);
      
   pulseGripper(0);

   if(sheetCounter != oldSheetCounter) // cut, lower knife
   {
      // simulate OMR sep   
      if(cutter.setting.simulateOffset)
      {
         simPageCounter++;
         if(simPageCounter >= cutter.setting.simulateOffset)
         {
            simPageCounter = 0;
            if(cutter.command.deliverNextRequest  /*rCommToCutter.deliverNextRequest*/)
            {   
               stopControl.OMRDeliverPos = modFloat(cutPosition + cutter.setting.stopOffset + cutter.setting.format * 4);
               stopControl.actualStopPos = stopControl.OMRDeliverPos; // for delivery without stop 
               stopControl.slowSpeedPos = stopControl.OMRDeliverPos + 100.0; // for slow speed at delivery page
               if(!cutter.setting.stopWhenDelivering)
                  stopControl.slowSpeedPos = stopControl.slowSpeedPos + 130.0; // cutter.setting.format / 2.0;
               if(stopControl.slowSpeedPos > wrapAround)
                  stopControl.slowSpeedPos = stopControl.slowSpeedPos - wrapAround;
            }
            else
               stopControl.offsetPos = modFloat(cutPosition + cutter.setting.stopOffset + cutter.setting.format * 4);
         }
      }
      else
         simPageCounter = 0;
      oldSheetCounter = sheetCounter;

      //sprintf(debugText, "n: %d c:%d m:%d", (int)(100 * (stopControl.nextStopPos - Master[0].Status.ActPosition)), cutPosition, regMarkPos);
      //strcpy(ULog.text[ULog.ix++], debugText);
      
      // send page info to stacker at least 20mm from now and 20mm before stop (if possible)
      if(cutter.setting.stopOffset  > 40)
         pageToStacker = cutter.setting.stopOffset - 30;
      else
         pageToStacker = 10.0;
            
      // handle skip to print
      if(skipMarksAfterWhite == 0)
         cutter.reading.pagesToSkip = 0;
      else
      {
         cutter.reading.processingWhite = true;
         cutter.reading.pagesToSkip = skipMarksAfterWhite - sheetCounter;
      }
      if(skipToPrintLength > 0.0)
      {
         if(startingSkipToPrint)
         {
            startingSkipToPrint = false;
            whitePagesSpeedLimit = 5.0;
            if(!cutter.command.rejectSheets)
               output.reject = rejectSheets = false; //close reject
            if (cutter.setting.rejectSkipToPrint)
               output.reject = rejectSheets = true; //keep it fucking open
         }
         cutter.reading.processingWhite = true;
         if(skipToPrintLength < 1.0)
         {
            sprintf(ULog.text[ULog.ix++],"+++ White pages: back to normal speed");
            whitePagesSpeedLimit = 5.0;
         }
         else if(skipToPrintLength < 2.0)
         {
            if(!rejectSheets || cutter.command.rejectSheets)
            {
               sprintf(ULog.text[ULog.ix++],"+++ White pages: start rejecting");
               cutter.command.rejectSheets = false;
               output.reject = rejectSheets = true; //open reject               
            }
         }
         else if(skipToPrintLength < 3.0)
         {
            if(whitePagesSpeedLimit > 1.0)
            {
               sprintf(ULog.text[ULog.ix++],"+++ White pages: reduce speed for reject and deliver");
               if(!cutter.command.rejectSheets) {
                  if (!cutter.setting.rejectSkipToPrint)
                     cutter.reading.pendingDeliverToggle = !cutter.reading.pendingDeliverToggle;
               }
               whitePagesSpeedLimit = 1.0;
            }
         }
         skipToPrintLength -= (cutter.setting.format / 1000.0);
         if(skipToPrintLength <= 0.0)
         {
            afterSkipToPrint = true; // signal to mainKnife::cyclic (currently not used)
            rejectLoadPos = 0.0;
            searchForMark_mainKnife = true;
            searchForMark_stripKnife = true;
            searchForMark_punch = true;
            lineController.command.beep = true;
            skipToPrintLength = 0.0;
         }         
      }
      //theEventHandler.addEvent(EVENT_CALLBACK, Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition + pageToStacker, (void *)&newSheetToStacker);
      newSheetToStacker(Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition);
      if(skipToPrintLength < 3.0 && skipToPrintLength > (3.0 - (cutter.setting.format / 1000.0)))
      {
         sprintf(ULog.text[ULog.ix++],"+++ White pages: last page to stacker");
         cutter.reading.currentPage.type = 2;
      }         
      funcPosDebug = Slave[SLAVE_MAININFEED_INDEX].Status.ActPosition + 50;
   }
   if(cutter.command.stopDeliverRequest && !cutter.command.deliver)
   {
      cutter.command.deliver = true;
   }
   
   if(stopControl.OMRDeliverPos != 0.0)
   {
      if(ODPWasZero)
      {
         sprintf(ULog.text[ULog.ix++], "set up OMR deliver at %f (now = %f)", stopControl.OMRDeliverPos, Master[0].Status.ActPosition);
         ODPWasZero = false;
      }
      if(isPosGreater(addPos(stopControl.nextStopPos, 150), stopControl.OMRDeliverPos))      
      {
         cutter.command.deliver = true;
         deliveryPending = true;
         //rCommToStacker.pendingDeliverToggle = !rCommToStacker.pendingDeliverToggle;
         cutter.reading.pendingDeliverToggle = !cutter.reading.pendingDeliverToggle;
         sprintf(ULog.text[ULog.ix++], "OMR deliver NOW (now = %f)", Master[0].Status.ActPosition);
         sprintf(ULog.text[ULog.ix++], "ODP: %f nSP: %f ODe: %d", stopControl.OMRDeliverPos, stopControl.nextStopPos, OMRDelay);
         stopControl.OMRDeliverPos = 0;
      }
   }
   else
      ODPWasZero = true;
   
#ifdef NEVER
   if(cutter.reading.pendingDeliverToggle != oldDeliverNowRequestToggle && !cutter.command.deliver)
   {
      cutter.command.deliver = true;
      deliveryPending = true;
      //rCommToStacker.pendingDeliverToggle = !rCommToStacker.pendingDeliverToggle;
      cutter.reading.pendingDeliverToggle = !cutter.reading.pendingDeliverToggle;
      oldDeliverNowRequestToggle = cutter.command.deliverNowRequestToggle;
      strcpy(ULog.text[ULog.ix++], "Deliver request from stacker");
   }
#endif

   if(cutter.command.deliverNowRequestToggle != oldDeliverNowRequestToggle && stopControl.OMRDeliverPos == 0.0)
   {
      stopControl.OMRDeliverPos = modFloat(cutPosition + cutter.setting.stopOffset + cutter.setting.format * 6);
      stopControl.actualStopPos = stopControl.OMRDeliverPos; // for delivery without stop 
      stopControl.slowSpeedPos = stopControl.OMRDeliverPos + 100.0; // for slow speed at delivery page
      if(!cutter.setting.stopWhenDelivering)
         stopControl.slowSpeedPos = stopControl.slowSpeedPos + 130.0; //cutter.setting.format / 2.0;
      if(stopControl.slowSpeedPos > wrapAround)
         stopControl.slowSpeedPos = stopControl.slowSpeedPos - wrapAround;
      //cutter.command.deliver = true;
      //deliveryPending = true;
      //cutter.reading.pendingDeliverToggle = !cutter.reading.pendingDeliverToggle;
      oldDeliverNowRequestToggle = cutter.command.deliverNowRequestToggle;
      strcpy(ULog.text[ULog.ix++], "Deliver request from stacker");
   }
   
 
   
   
   if(commandStop_state == 3 && commandStop_state != oldCommandStopState)
   {
      sprintf(ULog.text[ULog.ix++], "Stop at %f (now = %f)", timeToBrake, Master[0].Status.ActPosition);
      sprintf(ULog.text[ULog.ix++], "distToStop %f  minStopDist %f", distToStop, minStopDist);
   }
   if(commandStop_state == 2 && commandStop_state != oldCommandStopState)
   {
      sprintf(ULog.text[ULog.ix++], "BRAKE!");
      sprintf(ULog.text[ULog.ix++], "Stop at %f (now = %f)", timeToBrake, Master[0].Status.ActPosition);
      sprintf(ULog.text[ULog.ix++], "distToStop %f  minStopDist %f", distToStop, minStopDist);      
   }
   oldCommandStopState = commandStop_state;
   
   //speedDiff = Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity / 1000.0 - rCommToStacker.webSpeed;
   //if(speedDiff > 0.01 || speedDiff < -0.01)
   //   rCommToStacker.webSpeed = Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity / 1000.0;
   //rCommToStacker.webSpeed = Master[0].Status.ActVelocity / 1000.0;
   cutter.reading.webSpeed = Master[0].Status.ActVelocity / 1000.0;
   //rCommToStacker.sheetSize = format - (stripSize * ((GlobalCommand.Output.Command.EnableCutMain==1 && GlobalCommand.Output.Command.EnableCutSecondary==1)?1.0:0.0));
   
   cutter.reading.sheetSize = cutter.setting.format - (cutter.setting.stripSize * ((GlobalCommand.Output.Command.EnableCutMain==1 && GlobalCommand.Output.Command.EnableCutSecondary==1)?1.0:0.0));


   // air control
   if(GlobalCommand.Input.Parameter.Velocity > 0 && Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity > 1)
   {
      airStripEjectCounter = 5 * 416; // 5 seconds of air
   }
   else
   {
      if(airStripEjectCounter)
         airStripEjectCounter--;
   }
   //airStripEject = airStripEjectCounter ? true : false;
   output.airStripEject = airStripEjectCounter ? true : false;
	
	

   // EM stop (temporary)
   //if(tempEMStop)
   //{
   //   GlobalCommand.Input.Command.E_Stop = 1;
   //}

   d_distToCut = distToNextCut = (cutPosition - Master[0].Status.ActPosition);
   d_loopStopState = loopStopState;
   d_loopStopPos = loopStopPos;

   if(commandStop_state == 0 && commandDeliver_state == 0 && machineStarted)
   {
      // Buffer control
      if(cutter.setting.infeedRunSpeed > 0 && machineStarted)
      {   
         highPos = cutter.setting.bufferHighPos;//10.0; // 5.0
         lowPos = cutter.setting.bufferLowPos; //15.0; // 10.0
         midPos = (highPos * 2.5 + lowPos) / 3.5; //2,3
         //cutter.setting.bufferHighPos = 10;
         //cutter.setting.bufferLowPos = 15;
         
         curPos = buffer.reading.usage;
         if(curPos < highPos)
            curPos = highPos;
         if(curPos > lowPos)
            curPos = lowPos;
         //demandSpeed = cutter.setting.infeedRunSpeed * (curPos - highPos) / (lowPos - highPos);
         if(curPos >= midPos)
            demandSpeed = (buffer.reading.infeedSpeed * 990) + (cutter.setting.infeedRunSpeed - (buffer.reading.infeedSpeed * 990)) * ((curPos - midPos) / (lowPos - midPos));
         else
            demandSpeed = (buffer.reading.infeedSpeed * 990) * ((curPos - highPos) / (midPos - highPos));
         if(demandSpeed < 0)
            demandSpeed = 0;
         
         if(demandSpeed < 200 && buffer.reading.usage < 20.0) // Controlled stop when loop is high enough to result in a speed below 200
         {
            if(loopStopState == 0)
            {   
               demandSpeed = 195;
               if(distToNextCut > 400)
               {
                  demandSpeed = 0;
                  loopStopState = 2;
               }
               else
               {
                  loopStopPos = Master[0].Status.ActPosition + distToNextCut + cutter.setting.stopOffset - 6;
                  loopStopState = 1;
               }
            }
            if(loopStopState == 1)
            {
               demandSpeed = 195;
               if(Master[0].Status.ActPosition >= loopStopPos)
               {
                  demandSpeed = 0;
                  loopStopState = 2;
               }
            }         
            if(loopStopState == 2)
            {
               demandSpeed = 0;
            }
         }
         else
         {
            loopStopState = 0;
         }
      }
      else
      {
         loopStopState = 0;
      }

   

/* if(rCommToCutter.maximumSpeed * 1000 < demandSpeed)
setSpeed = rCommToCutter.maximumSpeed * 1000;
else
setSpeed = demandSpeed;*/
//		nissePisse = kalleBalle;
      if((speedLimit() * 1000) < demandSpeed)
         setSpeed = (speedLimit() * 1000);
      else
      {
         cutter.reading.limiter = LIM_CUTTER;
         setSpeed = demandSpeed;
      }

      if(feedToCutState > 0)
         setSpeed = 200;
      
      if (setSpeed >= (cutter.setting.infeedRunSpeed*0.99)) cutter.reading.limiter = LIM_MAXSPEED; //show max speed limiter
      GUI.readings.X50_SpeedLimit = 0.06 * setSpeed;  // convert to m/min and show as limit in GUI

      //if (lastSetSpeed != setSpeed && setSpeed > lastSetSpeed)
      //{
      //   speedUpTimer = 10;
      //}

      //lastSetSpeed = setSpeed;

      //if (setSpeed > toServoSpeed && speedUpTimer == 0) toServoSpeed = setSpeed;
      //else if (setSpeed < toServoSpeed) toServoSpeed = setSpeed;

      //if (speedUpTimer) speedUpTimer--;

      if(commandStop_state == 0)
         GlobalCommand.Input.Parameter.Velocity = setSpeed;
      else
         cutter.reading.limiter = LIM_CUTTER;

      
/*if(rCommToCutter.s50maxSpeed * 1000 < demandSpeed)
GlobalCommand.Input.Parameter.Velocity = rCommToCutter.s50maxSpeed * 1000;
else
demandSpeed = GlobalCommand.Input.Parameter.Velocity;*/



//rCommToStacker.setSpeed = GlobalCommand.Input.Parameter.Velocity / 1000;//demandSpeed / 1000;

   }
   else //mac added
   {
      if(commandStop_state == 0 && !cutter.command.feed)
         GlobalCommand.Input.Parameter.Velocity = 0;
      GUI.readings.X50_SpeedLimit =	GlobalCommand.Input.Parameter.Velocity * 0.6;
      cutter.reading.limiter = LIM_CUTTER;
   }

   // Don't wait forever to stop when decelerating from slow speed
   if(!machineStarted && Master[0].Status.ActVelocity > 0.0 && !cutter.command.feed)
   {
      if(stopTimer < 3 * 416) // unconditional stop after 3 seconds
      {
         stopTimer++;
      }
      else
      {
         /* if(!machineError.active)
         {
            machineError.active = true;
            machineError.errType = ERR_SPEEDERROR;
            machineError.errSubType = (int)GlobalCommand.Input.Parameter.Velocity;
         }*/
         GlobalCommand.Input.Parameter.Velocity = 0;
         stopTimer = 0;
      }
   }
   else
      stopTimer = 0;
   // Extraction crash detection
   if(!cutter.reading.input.extractionCrash &&
      GlobalCommand.Output.Status.AllSlavesSynchronizedMotion &&
      lineController.setup.cutter.secondKnife)
      extractionCrashTime++;
   else
      extractionCrashTime = 0;
   if(extractionCrashTime > 4000) // 10 seconds
   {
      extractionCrashTime = 4000;
      if(!machineError.active)
      {
         machineError.active = true;
         machineError.errType = ERR_EXTRACTION;
         machineError.errSubType = 0;
      }
   }
   
   // Spitter torque crash detection
   if(Slave[SLAVE_REJECT_INDEX].Status.ActVelocity > 1)
   {
      if(iot_cutterRejectTorque > cutter.setting.spitterTorqueLimit)
      {
         if(!machineError.active)
         {
            machineError.active = true;
            machineError.errType = ERR_SPITTER_TORQUE;
            machineError.errSubType = 0;
         }
      }
   }
   
   // Outfeed crash detection
   if(Master[0].Status.ActVelocity == 0.0)
   {
      lastToggleCrash_1 = Master[0].Status.ActPosition;
      lastToggleCrash_2 = Master[0].Status.ActPosition;
      lastToggleCrash_3 = Master[0].Status.ActPosition;
   }
   if(machineStarted)
   {
      if(cutter.setting.outfeedCrash1Enable)
      {
         if(cutter.reading.input.knifeOutCrashCount1 != outCrashCount_1)
         {
            debugCrash1 = Master[0].Status.ActPosition - lastToggleCrash_1;
            lastToggleCrash_1 = Master[0].Status.ActPosition;
            outCrashCount_1 = cutter.reading.input.knifeOutCrashCount1;
         }
         if((Master[0].Status.ActPosition - lastToggleCrash_1) > ((cutter.setting.format*3) + 50))
         {
            if(!machineError.active)
            {
               machineError.active = true;
               machineError.errType = ERR_OUTFFED_CRASH1;
               machineError.errSubType = 0;
            }
         }
      }
      if(cutter.setting.outfeedCrash2Enable)
      {
         if(cutter.reading.input.knifeOutCrashCount2 != outCrashCount_2)
         {
            debugCrash2 = Master[0].Status.ActPosition - lastToggleCrash_2;
            lastToggleCrash_2 = Master[0].Status.ActPosition;
            outCrashCount_2 = cutter.reading.input.knifeOutCrashCount2;
         }
         if((Master[0].Status.ActPosition - lastToggleCrash_2) > ((cutter.setting.format*3) + 50))
         {
            if(!machineError.active)
            {
               machineError.active = true;
               machineError.errType = ERR_OUTFFED_CRASH2;
               machineError.errSubType = 0;
            }
         }
      }
      if(cutter.setting.outfeedCrash3Enable)
      {
         if(cutter.reading.input.knifeOutCrashCount3 != outCrashCount_3)
         {
            debugCrash3 = Master[0].Status.ActPosition - lastToggleCrash_3;
            lastToggleCrash_3 = Master[0].Status.ActPosition;
            outCrashCount_3 = cutter.reading.input.knifeOutCrashCount3;
         }
         if((Master[0].Status.ActPosition - lastToggleCrash_3) > ((cutter.setting.format*3) + 50))
         {
            if(!machineError.active)
            {
               machineError.active = true;
               machineError.errType = ERR_OUTFFED_CRASH3;
               machineError.errSubType = 0;
            }
         }
      }
   }
   
   if(machineStarted && errMissingCutMarks)
   {
      if(!machineError.active)
      {
         machineError.active = true;
         machineError.errType = ERR_MISSINGCUTMARKS;
         machineError.errSubType = 0;
      }
   }   
   errMissingCutMarks = false;
  
   if(!machineStarted)
      setSpeed = 0;
   //rCommToStacker.setSpeed = setSpeed / 1000;
   cutter.reading.setSpeed = setSpeed / 1000;
	
   output.ready = lineController.reading.ready;
   output.pause = lineController.reading.pause;
   output.slowDown = lineController.reading.slowDown;
   setOutputs();	
}   

void _EXIT ProgramExit(void)
{
   // Insert code here 
}

void setOutput (struct output_typ *x, BOOL val)
   {
   x->pVal = val;
   if (!x->force)
   {
      x->oVal = val;
      return;
   }  
   x->oVal = x->fVal;
}

void pulseGripper(BOOL setup)
{
   static USINT repeatHome;
   if (setup)
   {
      grip.setup.twoUp.pulseLengthON = 3;
	  grip.setup.twoUp.pulseLengthOFF = 8;
	  grip.setup.tamper.pulseLengthON = 16;
	  grip.setup.tamper.pulseLengthOFF = 20;
	  grip.setup.mechStep.pulseLengthON = 25;
	  grip.setup.mechStep.pulseLengthOFF = 29;
	  grip.cmd.home.pulseLength = 12;
   }
   else
   {   
	   if ((gripperPulseCnt == 0) && (grip.cmd.home.set || repeatHome)) {
	      if (grip.cmd.home.set) repeatHome = 4;
	      setGripperPulse = grip.cmd.home.pulseLength;
		  grip.cmd.home.set = 0;
		  repeatHome--;
	   }
	   if (gripperPulseCnt == 40 && grip.setup.twoUp.set) setGripperPulse = grip.setup.twoUp.pulseLengthON;
	   if (gripperPulseCnt == 40 && !grip.setup.twoUp.set) setGripperPulse = grip.setup.twoUp.pulseLengthOFF;
	   if (gripperPulseCnt == 80 && grip.setup.tamper.set) setGripperPulse = grip.setup.tamper.pulseLengthON;
	   if (gripperPulseCnt == 80 && grip.setup.tamper.set) setGripperPulse = grip.setup.tamper.pulseLengthOFF;
	   if (gripperPulseCnt == 120 && grip.setup.mechStep.set) setGripperPulse = grip.setup.mechStep.pulseLengthON;
	   if (gripperPulseCnt == 120 && !grip.setup.mechStep.set) setGripperPulse = grip.setup.mechStep.pulseLengthOFF;
	   
	   gripperPulseCnt++;

	 /*  if (setGripperPulse) {
	      setOutput(&comPulseGripper, 1);
		  setGripperPulse--;
	   }
	   else setOutput(&comPulseGripper, 0); */
     //TODO fix this, output
	   
   }
}

REAL rmap(REAL x, REAL in_min, REAL in_max, REAL out_min, REAL out_max)  //map function
{  
   return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
 
void constrain (REAL * var,REAL min,REAL max) //limits var
{
   if (*var < min) *var = min;
   if (*var > max) *var = max;
}
 
REAL sCurve (REAL currentSpeed, REAL goalSpeed ,REAL acc, REAL startSlope, REAL endSlope) 
{
    /*-------------------------------
       returns the acceleration    
       startSlope 0-200 -> amount of s ramp
       endSlope 0-200 -> amount of s ramp
       acc maximum acceleration
    ----------------------------------*/
   constrain(&startSlope,0,200);
   constrain(&endSlope,0,200);
 
   REAL linearAmount=0, sinAmount=0, sinSquareAmount=0, spdPos = 0;
   REAL  s[12] =  {0.118 ,0.346,0.578,0.831,0.953,1.00,0.953,0.831,0.578,0.346,0.118 ,1.000}; //sin curve
   REAL  ss[12] = {0.0139,0.120,0.334,0.691,0.908,1.00,0.908,0.691,0.334,0.120,0.0139,1.000}; //sin^2 curve
   REAL  p[12] =  {7.5   ,15   ,25   ,37.5 ,43   ,57  ,62.5 ,75   ,85   ,92.5 ,99.9  ,10000}; //% to goal speed acc change points
   USINT i;
    
   spdPos = rmap(currentSpeed,0,goalSpeed,0,100);
   for (i=0;i<12;i++) if (p[i]>spdPos) break;
   if (i==5 || i == 11) return acc;
    
   if (i < 5) {
        
      if (startSlope>100) {
         sinSquareAmount = startSlope-100;
         sinAmount = 100 - sinSquareAmount;
      }
      else {
         sinAmount = startSlope;
         linearAmount = 100 - sinAmount; 
      }
   }
   else {
      if (endSlope>100) {
         sinSquareAmount = endSlope-100;
         sinAmount = 100 - sinSquareAmount;
      }
      else {
         sinAmount = endSlope;
         linearAmount = 100 - sinAmount; 
      }
        
   }
   return ((linearAmount) + (sinAmount * s[i]) + (sinSquareAmount * ss[i])) * acc/100;
}


#include <bur/plctypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <AsUDP.h>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFFF; 

#define ETH_BUFFER_SIZE 2000

unsigned char *tCS = (unsigned char *)&rComm;
unsigned char *tCS_copy = (unsigned char *)&rComm_copy;

unsigned int timeSinceReceived = 0;

class UDPComm
{
   UdpSend_typ	udpS;
   UdpOpen_typ udpO;
   UdpClose_typ udpC;
   UdpRecv_typ udpR;
   UdpConnect_typ udpCnct;

   char currentIP[16];
   //char myIP[16];
   unsigned short currentPort;
   unsigned long currentIdent;
   public:
   UDPComm(char *ipAdr, unsigned short port);
   ~UDPComm();
   bool openComm(void);
   void sendUDPMessage(unsigned char *msg, int length);
   int receiveUDPMessage(unsigned char *msg, int maxLength);
};

UDPComm::UDPComm(char *ipAdr, unsigned short port)
{
   memset((void *)&udpS, 0, sizeof(UdpSend_typ));
   memset((void *)&udpO, 0, sizeof(UdpOpen_typ));
   memset((void *)&udpC, 0, sizeof(UdpClose_typ));
   memset((void *)&udpR, 0, sizeof(UdpRecv_typ));
   memset((void *)&udpCnct, 0, sizeof(UdpConnect_typ));
   strcpy(currentIP, ipAdr);
   currentPort = port;
}

UDPComm::~UDPComm()
{
   udpC.enable = 1;
   udpC.ident = currentIdent;
   UdpClose(&udpC);
}

bool UDPComm::openComm(void)
{
   udpO.pIfAddr = (UDINT)0;
   udpO.port = currentPort;
   udpO.options = 0;
   udpO.enable = 1;
   UdpOpen(&udpO);
   currentIdent = udpO.ident;
   debugComm3 = udpO.status;
   if(udpO.status == 0)
      return true;
   else
      return false;   	
}

void UDPComm::sendUDPMessage(unsigned char *msg, int length)
{
   if(!udpO.status)
   {
      udpS.pData = (DINT)msg;
      udpS.datalen = length;
      udpS.ident = currentIdent;
      udpS.pHost = (DINT)currentIP;
      udpS.port = currentPort;
      udpS.flags = 0;
      udpS.enable = 1;

      UdpSend(&udpS);
      debugComm2 = udpS.status;
      UdpSend(&udpS);
   }
}

int UDPComm::receiveUDPMessage(unsigned char *msg, int maxLength)
{
   static int rLen;
	
   if(!udpO.status)
   {
      udpR.pData = (DINT)msg;
      udpR.datamax = maxLength;
      udpR.flags = 0;
      udpR.pIpAddr = (DINT)0;//currentIP;
      udpR.ident = currentIdent;
      udpR.enable = 1;
      UdpRecv(&udpR);
      rLen = udpR.recvlen;
      debugComm1 = udpR.status;
      UdpRecv(&udpR);

      return rLen;
   }
   else
      return 0;
}

UDPComm *CPUComm;

struct smChange_typ
{
   unsigned short index;
   unsigned char value;
} smChange;

unsigned char commBuffer_Out[ETH_BUFFER_SIZE];
unsigned char commBuffer_In[ETH_BUFFER_SIZE];

int buildOutMessage(unsigned char *buf)
{
   int byteCounter = 0;
   unsigned int i;
   static int bufferFullCounter = 0;
	

   byteCounter = 0;

   for(i = 0; i < sizeof(struct rComm_typ); i++)
   {
      if(tCS_copy[i] != tCS[i] || ((timeSinceReceived > 200) && tCS[i] != 0))
      {
         tCS_copy[i] = tCS[i];
         smChange.index = i;
         smChange.value = tCS[i];
         memcpy((void *)&buf[byteCounter], (void *)&smChange, sizeof(smChange));
         byteCounter += sizeof(smChange);
         if(byteCounter >= ETH_BUFFER_SIZE - sizeof(smChange))
         {
            commStateDebug = byteCounter;
            return byteCounter;
         }
      }
   }
   return byteCounter;
}

int parseInMessage(unsigned char *buf, int msgSize)
{
   unsigned char *dPtr;
   int byteCounter;

   if(msgSize > 0 && (msgSize % sizeof(smChange) == 0))
   {
      timeSinceReceived = 0;
      byteCounter = 0;
      dPtr = (unsigned char *)buf;
      while(byteCounter < msgSize)
      {
         memcpy((void *)&smChange, (void *)dPtr, sizeof(smChange));
         dPtr += sizeof(smChange);
         byteCounter += sizeof(smChange);
         if(smChange.index < sizeof(rComm_typ))
         {
            tCS_copy[smChange.index] = tCS[smChange.index] = smChange.value;   
         }
      }
      return 1;
   }
   else
      return 0;
}

void _INIT ProgramInit(void)
{
   memset((void *)&rComm, 0, sizeof(rComm_typ));
   memset((void *)&rComm_copy, 0, sizeof(rComm_typ));
   testStuff = 1;
   commStateDebug = 0;
}

void _CYCLIC ProgramCyclic(void)
{
   int msgSize = 0;
   static UDINT bandWidth = 0;
   static UDINT totMsgSize = 0;
   static UDINT bwCnt = 0;
   static char displayIP[20];
   static int syncTime = 300;
   static int dummyVar = 0;

	
   enum commState
   {
      INIT,
      OPENCOM,
      SYNCING,
      RUNNING
   };   
   static int commState = INIT;
   
   if(commState == INIT)
   {
      CPUComm = new UDPComm("192.168.0.40", 4111);	
      commState = OPENCOM;
      testStuff += 2;
   }
   if(commState == OPENCOM)
   {
      if(CPUComm->openComm())
      {
         commState = SYNCING;
         syncTime = 300;
      }
   }
   if(commState == SYNCING)
   {
      if(syncTime)
         syncTime--;
      if(syncTime == 0)
         commState = RUNNING;
   }
   if(commState == RUNNING)
   {
      dummyVar++;
      timeSinceReceived++;
      msgCount++;

      bwCnt++;
      if(bwCnt == 100) //1 sec
      {
         rComm.cutterAlive++;
         bandWidth = totMsgSize;
         bwCnt = 1;
         
         totMsgSize = 0;
      }

      msgSize = buildOutMessage(commBuffer_Out);
      if(msgSize)
      {
         debugOutMsgCount++;
         CPUComm->sendUDPMessage(commBuffer_Out, msgSize);
      }
      totMsgSize += msgSize;
   
      msgSize = CPUComm->receiveUDPMessage(commBuffer_In, ETH_BUFFER_SIZE);
      if(msgSize)
      {
         debugInMsgCount++;
         parseInMessage(commBuffer_In, msgSize);
      }
   }
}

void _EXIT ProgramExit(void)
{
   testStuff++;
   delete CPUComm;
}

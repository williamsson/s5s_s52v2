
TYPE
	unitSelection_typ : 	STRUCT 
		infeedSpeed_Big : REAL := 0.0;
		infeedSpeed_Small : REAL := 0.0;
		Speed_Big : REAL := 0.0;
		statAvgSpeed : REAL := 0.0;
		statDtotal : LREAL := 0.0;
		statMeters : LREAL := 0.0;
		Speed_Small : REAL := 0.0;
	END_STRUCT;
	userFunctionOutput_typ : 	STRUCT 
		folder : userOutput_machine_typ;
		stacker : userOutput_machine_typ;
		merger : userOutput_machine_typ;
		buffer : userOutput_machine_typ;
		cutter : userOutput_machine_typ;
	END_STRUCT;
	userOutput_typ : 	STRUCT 
		status : BOOL;
		functionControlled : BOOL;
	END_STRUCT;
	userOutput_machine_typ : 	STRUCT 
		_1 : userOutput_typ;
		_2 : userOutput_typ;
	END_STRUCT;
	lastErrors_typ : 	STRUCT 
		minutes : ARRAY[0..11]OF UDINT := [12(0)];
		sub : ARRAY[0..11]OF UDINT := [12(0)];
		error : ARRAY[0..11]OF UDINT := [12(0)];
	END_STRUCT;
	lineController_ext_command_typ : 	STRUCT 
		remoteControlled : BOOL := FALSE;
		shortBeep : BOOL; (*bep*)
		trippleBeep : BOOL; (*beep, beep*)
		doubleBeep : BOOL; (*beep, beep*)
		beep : BOOL; (*beep*)
		logPress1 : BOOL;
		logPress2 : BOOL;
		logPress3 : BOOL;
		mediumSpeed : BOOL; (*enable/disable. 1 slow speed on, 0 slow speed off*)
		flash : BOOL;
		slowSpeed : BOOL; (*enable/disable. 1 slow speed on, 0 slow speed off*)
	END_STRUCT;
	lineController_typ : 	STRUCT 
		setup : lineSetup_typ;
		reading : lineController_readings_typ;
		setting : lineController_settings_typ;
		command : lineController_ext_command_typ;
	END_STRUCT;
	lineController_readings_typ : 	STRUCT 
		version : UINT;
		folderStatus : USINT;
		cutterStatus : USINT;
		mergerStatus : USINT;
		bufferStatus : USINT;
		stackerStatus : USINT;
		folderStatusAddInfo : USINT;
		cutterStatusAddInfo : USINT;
		mergerStatusAddInfo : USINT;
		bufferStatusAddInfo : USINT;
		stackerStatusAddInfo : USINT;
		slowDown : BOOL;
		pause : BOOL;
		ready : BOOL;
		initDone : BOOL;
		LTmode : USINT;
		userOutput : userFunctionOutput_typ;
		segmentColor1 : colorOfButton;
		segmentColor2 : colorOfButton;
		segmentColor3 : colorOfButton;
		segmentColor4 : colorOfButton;
		segmentColor5 : colorOfButton;
		maximumSpeed : REAL;
		trigger1 : BOOL;
		trigger2 : BOOL;
		trigger3 : BOOL;
		trigger4 : BOOL;
		trigger5 : BOOL;
		trigger6 : BOOL;
		function1 : BOOL;
		function2 : BOOL;
		function3 : BOOL;
		function4 : BOOL;
		timeSinceStart : UDINT; (*s (UDINT)*)
		funcResult_Dev : USINT;
		mismatches : USINT := 0;
		nodesFound : UINT := 0;
		modulesFound : UINT := 0;
		modulesMismatch : ARRAY[0..9]OF mismatch_typ;
		guiControShowWhenStackCoverCl : UINT := 1;
		guiControShowMonkey : UINT := 1;
		guiControShowWhenStopped : UINT := 1; (*set to 0 when level 5 is logged in (Developement mode) *)
		guiControlLevel5Unlock : UINT := 2; (*set to 0 when level 5 is logged in (Developement mode) *)
		guiControlLevel4Unlock : UINT := 2; (*set to 0 when level 4 is logged in (Super User Mode)*)
		guiControlLevel3Unlock : UINT := 2; (*set to 0 when level 3 is logged in (Service Mode)*)
		guiControlLevel2Unlock : UINT := 2; (*set to 0 when level 2 is logged in (Enhanced Operator mode)*)
		guiControlLevel4Hide : UINT := 1; (*set to 1 when level 4 or higher logged in*)
		guiControlLevel5Show : UINT := 1; (*set to 0 when level 5 is logged in (Developement mode)*)
		guiControlLevel4Show : UINT := 1; (*set to 0 when level 4 is logged in (Super User Mode)*)
		guiControlLevel3Show : UINT := 1; (*set to 0 when level 3 is logged in (Service Mode)*)
		guiControlHideC51 : UINT := 1; (*set to 0 when double knife*)
		guiControlRemoteControlled : UINT := 1;
		guiControlLevel2Show : UINT := 1; (*set to 0 when level 2 is logged in (Enhanced Operator mode)*)
		guiControlLog : logParam;
		guiControlServiceUnlock : UINT := 0; (*set to 2 if in service mode*)
		logOutTime : UINT := 0; (*minutes*)
		guiControlLevel : UINT := 1; (*1-5*)
		guiControlShowNoFolder : UINT := 1;
		guiControlShowNoMerger : UINT := 1;
		guiControlShowMerger : UINT := 1;
		guiContolShowMergerInstalled : UINT := 1;
		guiContolShowFolderInstalled : UINT := 1;
		guiControlShowFolder : UINT := 1;
		guiControlShowCoverOpenFolder : UINT := 0;
		guiControlShowDuringInit : UINT := 0;
		guiControlShowCoverOpenBuffer : UINT := 0;
		guiControlUnwinder : machineGui_typ;
		guiControlPostCut : guiControlStackerStreamer;
		guiControlShowMergerFolder : UINT := 0;
		m50f50 : UINT := 1;
		lastErrors : lastErrors_typ;
		bufferValvesInfo : ioTestBufferAirValvesR_typ;
		timeToStartSeconds : UINT;
		timeToStartMinutes : UINT;
		displayUnits : unitSelection_typ;
		lineReady : BOOL;
	END_STRUCT;
	lightTowerTest_typ : 	STRUCT 
		segment1 : machineStatus_typ;
		segment2 : machineStatus_typ;
		segment3 : machineStatus_typ;
		segment4 : machineStatus_typ;
		segment5 : machineStatus_typ;
		Test : BOOL := 0; (*test ON/OFF*)
	END_STRUCT;
	lineController_settings_typ : 	STRUCT 
		lightTowerSegmentSetup : lightTower_typ;
		trigger1 : trigger_typ;
		trigger2 : trigger_typ;
		trigger3 : trigger_typ;
		trigger4 : trigger_typ;
		trigger5 : trigger_typ;
		trigger6 : trigger_typ;
		function1 : userFunction_typ;
		function2 : userFunction_typ;
		function3 : userFunction_typ;
		function4 : userFunction_typ;
		loginCode : UDINT;
		onlySoftStop : BOOL := FALSE;
		invertReady : BOOL; (*makes ready work oposite*)
		buzzerVolume : DINT; (*0-2*)
		ltTest : lightTowerTest_typ;
		bufferValveTest : ioTestBufferAirValves_typ;
		ImperialUnits : BOOL;
		unwinder : unwinderSettings_typ;
	END_STRUCT;
	machines_enum : 
		(
		MachineBuffer,
		MachineCutter,
		MachineFolder,
		MachineMerger,
		MachineStacker,
		MachineUnwinder,
		Machine_user1,
		Machine_user2,
		Machine_user3,
		Machine_user4
		);
	lineSetup_typ : 	STRUCT 
		buffer : bufferSetup_typ;
		unwinder : folderSetup_typ;
		folder : folderSetup_typ;
		merger : mergerSetup_typ;
		cutter : cutterSetup_typ;
		stacker : stackerSetup_typ;
		transportTable : transportTableSetup_typ;
		streamer : streamerSetup_typ;
	END_STRUCT;
	lightTower_typ : 	STRUCT 
		machineSegment1 : machines_enum := MachineStacker;
		machineSegment2 : machines_enum := MachineCutter;
		machineSegment3 : machines_enum := MachineMerger;
		machineSegment4 : machines_enum := MachineBuffer;
		machineSegment5 : machines_enum := MachineUnwinder;
		buzzerVolume : USINT := 30;
		beepOnWarning : BOOL := FALSE;
		simpleMode : BOOL := TRUE;
		runningMode : USINT := 0; (*0 = show speed, 1 = show buffer leve l 2 = stackLevel 3, steady green*)
	END_STRUCT;
	bufferSetup_typ : 	STRUCT 
		installed : BOOL := TRUE;
	END_STRUCT;
	cutterSetup_typ : 	STRUCT 
		installed : {REDUND_UNREPLICABLE} BOOL;
		secondKnife : BOOL := TRUE;
	END_STRUCT;
	unwinderSettings_typ : 	STRUCT 
		maxSpeed : REAL;
	END_STRUCT;
	unwinderSetup_typ : 	STRUCT 
		installed : BOOL := FALSE;
	END_STRUCT;
	folderSetup_typ : 	STRUCT 
		installed : BOOL := FALSE;
		automationInstalled : BOOL := FALSE;
	END_STRUCT;
	transportTableSetup_typ : 	STRUCT 
		installed : BOOL := TRUE;
	END_STRUCT;
	mergerSetup_typ : 	STRUCT 
		installed : BOOL := TRUE;
		punchUnitInstalled : BOOL := TRUE;
	END_STRUCT;
	streamerSetup_typ : 	STRUCT 
		installed : BOOL := FALSE;
		jogger3 : BOOL := FALSE;
		electricalVersion : UINT := 0;
	END_STRUCT;
	stackerSetup_typ : 	STRUCT 
		installed : BOOL := TRUE;
		stackerGripperUnit1 : BOOL := TRUE;
		stackerGripperUnit2 : BOOL := FALSE;
		stackerGripperUnit3 : BOOL := FALSE;
		stackerGripperUnit4 : BOOL := FALSE;
		automaticStopPlate : BOOL := FALSE;
		electricalVersion : UINT := 1;
		spaghettiBelts : BOOL := TRUE;
	END_STRUCT;
	userFunction_typ : 	STRUCT 
		activate : BOOL := FALSE;
		inputA : DINT := 1; (*trigger 1 - 6*)
		inputB : DINT := 1; (*trigger 1- 6*)
		logic : BOOL := FALSE; (*0=A OR B 1=A AND B*)
		typ : DINT := 0;
		parameter1 : REAL := 0.0;
		parameter2 : REAL := 0.0;
		setting1 : DINT := 0;
		setting2 : DINT := 0;
	END_STRUCT;
	mismatch_typ : 	STRUCT 
		errorActive : BOOL := FALSE;
		missing : BOOL := FALSE;
		unnoticed : BOOL := FALSE;
		node : USINT := 0;
		station : USINT := 0;
	END_STRUCT;
	trigger_typ : 	STRUCT 
		contactState : BOOL := TRUE; (*normaly closed (false) normaly open (true)*)
		release : REAL := 0;
		delay : REAL := 0.5;
		source : DINT := 1;
		parameter : REAL := 0;
	END_STRUCT;
	machineGui_typ : 	STRUCT 
		Level5Unlock : UINT := 2; (*set to 0 when level 5 is logged in (Developement mode) *)
		Level4Unlock : UINT := 2; (*set to 0 when level 4 is logged in (Super User Mode)*)
		Level3Unlock : UINT := 2; (*set to 0 when level 3 is logged in (Service Mode)*)
		Level2Unlock : UINT := 2; (*set to 0 when level 2 is logged in (Enhanced Operator mode)*)
		Level4Hide : UINT := 1; (*set to 1 when level 4 or higher logged in*)
		Level5Show : UINT := 1; (*set to 0 when level 5 is logged in (Developement mode)*)
		Level4Show : UINT := 1; (*set to 0 when level 4 is logged in (Super User Mode)*)
		Level3Show : UINT := 1; (*set to 0 when level 3 is logged in (Service Mode)*)
		Level1Show : UINT := 1; (*set to 0 when level 2 is logged in (Enhanced Operator mode)*)
		Level2Show : UINT := 1; (*set to 0 when level 2 is logged in (Enhanced Operator mode)*)
	END_STRUCT;
	logParam : 	STRUCT 
		showStreamer : UINT := 0;
		showDeb : UINT := 0;
	END_STRUCT;
	ioTestBufferAirValves_typ : 	STRUCT 
		controlledPressureValveDown : BOOL := 0; (*valve 2*)
		controlledPressureValveUp : BOOL := 0; (*valve 2*)
		varPressValve : BOOL := 0; (*valve 1*)
		lockValve : BOOL := 0; (*valve 1*)
		force : BOOL := 0;
		SetPressureControl : BOOL;
		pressureControl : INT;
	END_STRUCT;
	guiControlStackerStreamer : 	STRUCT 
		stacker : machineGui_typ;
		stackerAndStreamer : machineGui_typ;
		streamer : machineGui_typ;
		showOffset : UINT := 0;
		showGlue : UINT := 0;
		stMenu3Tab : UINT := 0;
		showAdvanced : UINT := 0;
	END_STRUCT;
	ioTestBufferAirValvesR_typ : 	STRUCT 
		controlledPressureValve : BOOL := 0; (*valve 2*)
		lockValve : BOOL := 0; (*valve 1*)
		upDownValve : USINT := 0; (*valve 3*)
		pressureControl : INT;
	END_STRUCT;
END_TYPE

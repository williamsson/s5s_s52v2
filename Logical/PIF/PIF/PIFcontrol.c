
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <AsUDP.h>

#define PIF_CYCLIC      0
#define PIF_INIT        1

union PIFsBS
{
   unsigned long all;
   struct
   {
      unsigned connected:        1; //PPU
      unsigned stopRequest:      1; //PPU
      unsigned ready:            1; //PPU
      unsigned slowRequest:      1; //PPU
    
      unsigned eject:            1; //PPU
      unsigned pause:            1; //PPU
      unsigned backward:         1; //Printer
      unsigned wakeUp:           1; //Printer
    
      unsigned tensionControl:   1; //Printer
      unsigned fastStop:         1; //PPU
      unsigned splice:           1; //PPU
      unsigned blowerOn:         1; //Printer
    
      unsigned decurl:           1; //Printer
      unsigned decurlExit:       1; //PPU
      unsigned jobOffset:        1; //Printer-Internal
      unsigned pagePulse:        1; //Printer-Internal
    
      unsigned sixPPi:           1; //Printer-Internal
      unsigned warning:          1; //PPU
      unsigned loaded:           1; //PPU // not used
	  unsigned emStopPrinter:	 1; //Printer

	  unsigned emStopPPU:	     1; //PPU
      unsigned notUsed:          11;
    
   }bits;
};

struct S_tecdev
{
   int nodeNumber;
   union PIFsBS status;
   int speedLimit;
   int currentSpeed;   
}PIFtecdev;

struct S_printer
{
   union PIFsBS status;
   union PIFsBS available;
   int speed;
   int currentSpeedLimit;   
}PIFprinter;

int parseInt (const char *dBuffer, int lowChar, int maxNoChar, int * x)
{
   int i;
   int retval;
   retval = 0;
   for (i=lowChar;i<(lowChar+maxNoChar);i++) {
      if (dBuffer[i]==':' || dBuffer[i]=='\0' || dBuffer[i]==0 || dBuffer[i]=='\n' || dBuffer[i]==';') break;
      if (isdigit((unsigned char)dBuffer[i])) {
         retval = retval * 10;
         retval += dBuffer[i] - 0x30;
      }
   }
   *x = i+1;
   return retval;
}

char *inttostr(long i, char b[]){
   char const digit[] = "0123456789";
   char* p = b;
   if(i<0)
   {
      *p++ = '-';
      i *= -1;
   }
   long shifter = i;
   do
   { 
      ++p;
      shifter = shifter/10;
   }while(shifter);
   *p = '\0';
   do
   { 
      *--p = digit[i%10];
      i = i/10;
   }while(i);
   return b;
}

char *uinttostr(unsigned long i, char b[]){
   char const digit[] = "0123456789";
   char* p = b;
   unsigned long shifter = i;
   do
   { 
      ++p;
      shifter = shifter/10;
   }while(shifter);
   *p = '\0';
   do
   { 
      *--p = digit[i%10];
      i = i/10;
   }while(i);
   return b;
}

int PIFupdate(int action)
{
   static UdpSend_typ PIFudpS;
   static UdpOpen_typ PIFudpO;
   static UdpClose_typ PIFudpC;
   static UdpRecv_typ PIFudpR;
   static UdpConnect_typ PIFudpCnct;
   static int receive_ready = 1;
   static int close_ready = 1;
   static unsigned char recvBuf[256];
   static unsigned char transmitBuf[256];
   static unsigned char transmitMsgNum = 0;
   static unsigned int msgAge = 5000;
   static unsigned int transmitTimer = 0;
   static unsigned char PIFAddr[16];
   static unsigned int PIFport = 10372;
   static int PIFconnected = 0;
   static int tBuf[255];
   int colonPos;
  
   if(action == PIF_INIT) 
   {
      memset((void *)&PIFudpS, 0, sizeof(UdpSend_typ));
      memset((void *)&PIFudpO, 0, sizeof(UdpOpen_typ));
      memset((void *)&PIFudpC, 0, sizeof(UdpClose_typ));
      memset((void *)&PIFudpR, 0, sizeof(UdpRecv_typ));
      memset((void *)&PIFudpCnct, 0, sizeof(UdpConnect_typ));

      PIFudpO.enable = 1;      
      PIFudpO.port = PIFport;  
      UdpOpen(&PIFudpO);      
 
      receive_ready = 1;      
      close_ready = 1;
      debugPIFmsglen = 0;
      msgAge = 5000;
      transmitTimer = 0;
      /*PIFprinter.status.all = 0;
      PIFprinter.speed = 0;
      PIFtecdev.status.all = 0;
      PIFtecdev.status.bits.connected = 1;
      PIFtecdev.status.bits.ready = 1;
      PIFtecdev.nodeNumber = 12;
      PIFtecdev.speedLimit = 5000;
      PIFtecdev.currentSpeed = 3124;*/
   }

   if(action == PIF_CYCLIC) 
   {
      if(msgAge < 1000)
         msgAge++;
      if(PIFudpO.status != 0)
      {
         UdpOpen(&PIFudpO);            
      }
      else if(receive_ready == 1)           
      {
         PIFudpR.enable = 1;           
         PIFudpR.ident = PIFudpO.ident;    
         debugPIFmsglen = PIFudpR.ident;     
         PIFudpR.pData = (UDINT)&recvBuf[0];   
         PIFudpR.datamax = 256;
         PIFAddr[0] = 0;
         PIFudpR.pIpAddr = (UDINT)PIFAddr;
         UdpRecv(&PIFudpR);            
         receive_ready = 0;          
      }
      else if(PIFudpR.status != 0)
      {
         UdpRecv(&PIFudpR);       
      }
      else if(close_ready == 1)
      {
         recvBuf[PIFudpR.recvlen] = 0;
         if(PIFudpR.recvlen)
         {
            if(strncmp("PI:", (const char *)recvBuf, 3) == 0)
            {
               strcpy((char *)debugPIFtext, (const char *)recvBuf);
               msgAge = 0;
               PIFprinter.status.all = parseInt((const char *)recvBuf, 3, 10, &colonPos);
               PIFprinter.speed = parseInt((const char *)recvBuf, colonPos, 5, &colonPos);
               PIFprinter.currentSpeedLimit = parseInt((const char *)recvBuf, colonPos, 5, &colonPos);
			   pif.limiter = parseInt((const char *)recvBuf, colonPos, 5, &colonPos);
			   parseInt((const char *)recvBuf, colonPos, 10, &colonPos); //not used
			   PIFprinter.available.all = parseInt((const char *)recvBuf, colonPos, 10, &colonPos);
			   parseInt((const char *)recvBuf, colonPos, 10, &colonPos); //not used
			   pif.msgCnt = parseInt((const char *)recvBuf, colonPos, 3, &colonPos);
            }
         }
         UdpRecv(&PIFudpR);            
         receive_ready = 0;           
      }

      if(PIFudpS.status != 0)
      {
         UdpSend(&PIFudpS);            
      }
      else if(msgAge < 800) // if communication is alive
      {
         if(transmitTimer == 0)
         {
            transmitTimer = 40;
            PIFudpS.enable = 1;           
            PIFudpS.ident = PIFudpO.ident;
            PIFudpS.pHost = (UDINT)PIFAddr;
            PIFudpS.port = PIFport;
            //sprintf((char *)transmitBuf, "MD:12:%lu:%d:%d:0:%d", PIFtecdev.status.all, PIFtecdev.speedLimit, PIFtecdev.currentSpeed, transmitMsgNum);
            strcpy((char *)transmitBuf, "MD:");
            strcat((char *)transmitBuf, inttostr(PIFtecdev.nodeNumber, (char *)tBuf));
            strcat((char *)transmitBuf, ":");
            strcat((char *)transmitBuf, uinttostr(PIFtecdev.status.all, (char *)tBuf));
            strcat((char *)transmitBuf, ":");
            strcat((char *)transmitBuf, inttostr(PIFtecdev.speedLimit, (char *)tBuf));
            strcat((char *)transmitBuf, ":");
            strcat((char *)transmitBuf, inttostr(PIFtecdev.currentSpeed, (char *)tBuf));
            strcat((char *)transmitBuf, ":0:");
            strcat((char *)transmitBuf, uinttostr(transmitMsgNum, (char *)tBuf));
            transmitMsgNum++;
            PIFudpS.pData = (UDINT)&transmitBuf[0];   
            PIFudpS.datalen = strlen((const char *)transmitBuf);
            UdpSend(&PIFudpS);
         }
         if(transmitTimer > 0)
            transmitTimer--;
      }
      if(msgAge < 800)
         PIFconnected = 1;
      else
         PIFconnected = 0;
      /*else if(PIFudpC.status != 0)
      {
         UdpClose(&PIFudpC);
      }
      else
      {
         PIFudpO.enable = 1;      
         PIFudpO.port = PIFport;  
         UdpOpen(&PIFudpO);      

         receive_ready = 1;      
         close_ready = 1;
      }*/
   }

   return 0;      
}

void readGlobal(void)
{
	PIFtecdev.nodeNumber = 11;
	PIFtecdev.status.bits.connected = 1;
	PIFtecdev.status.bits.ready = pif.machine.ready;
	PIFtecdev.status.bits.stopRequest = pif.machine.stopRequest;
	PIFtecdev.status.bits.slowRequest = pif.machine.slowRequest;
	PIFtecdev.status.bits.eject = pif.machine.eject;
	PIFtecdev.status.bits.pause = pif.machine.pause;
	PIFtecdev.status.bits.fastStop = pif.machine.fastStop;
	PIFtecdev.status.bits.splice = pif.machine.splice;
	PIFtecdev.status.bits.decurlExit = pif.machine.decurlExit;
	PIFtecdev.status.bits.warning = pif.machine.warning;
	PIFtecdev.status.bits.loaded = pif.machine.loaded;
	PIFtecdev.status.bits.emStopPPU = pif.machine.emStopPPU;
	PIFtecdev.speedLimit = (INT)(pif.machine.speedLimit);
	PIFtecdev.currentSpeed = (INT)(pif.machine.currentSpeed);
}

void setGlobal(void)
{
	pif.printer.backward = PIFprinter.status.bits.backward;
	pif.printer.wakeUp = PIFprinter.status.bits.wakeUp;
	pif.printer.tensionControl = PIFprinter.status.bits.tensionControl;
	pif.printer.blowerOn = PIFprinter.status.bits.blowerOn;
	pif.printer.decurl = PIFprinter.status.bits.decurl;
	pif.printer.emStopPrinter = PIFprinter.status.bits.emStopPrinter;
	pif.printer.printerSpeed = (REAL)(PIFprinter.speed);
	pif.printer.currentSpeedLimit = (REAL)(PIFprinter.currentSpeedLimit);	

	pif.availiableMachineSignals.ready = PIFprinter.available.bits.ready;
	pif.availiableMachineSignals.slowRequest = PIFprinter.available.bits.slowRequest;
	pif.availiableMachineSignals.stopRequest = PIFprinter.available.bits.stopRequest;
	pif.availiableMachineSignals.eject = PIFprinter.available.bits.eject;
	pif.availiableMachineSignals.pause = PIFprinter.available.bits.pause;
	pif.availiableMachineSignals.fastStop = PIFprinter.available.bits.fastStop;
	pif.availiableMachineSignals.splice = PIFprinter.available.bits.splice;
	pif.availiableMachineSignals.decurlExit = PIFprinter.available.bits.decurlExit;
	pif.availiableMachineSignals.loaded = PIFprinter.available.bits.loaded;
	pif.availiableMachineSignals.emStopPPU = PIFprinter.available.bits.emStopPPU;
	pif.availiablePrinterSignals.backward = PIFprinter.available.bits.backward;
	pif.availiablePrinterSignals.wakeUp = PIFprinter.available.bits.wakeUp;
	pif.availiablePrinterSignals.blowerOn = PIFprinter.available.bits.blowerOn;
	pif.availiablePrinterSignals.decurl = PIFprinter.available.bits.decurl;
	pif.availiablePrinterSignals.emStopPrinter = PIFprinter.available.bits.emStopPrinter;
}

void _INIT ProgramInit(void)
{
   PIFupdate(PIF_INIT);
}

void _CYCLIC ProgramCyclic(void)
{
   static unsigned int PIFcount = 0;
   PIFcount++;
   //if(PIFcount == 4) // 100 times per second
   //{
	readGlobal();
   PIFupdate(PIF_CYCLIC);
	setGlobal();
   //   PIFcount = 0;
   //}
}

void _EXIT ProgramExit(void)
{

}



#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

// amount of memory to be allocated for heap storage must be specified for every ANSI C++ program with the bur_heap_size variable
unsigned long bur_heap_size = 0xFFFF; 

#define ROLL_DIAM_OVER_50      15
#define ROLL_DIAM_45_50        14
#define ROLL_DIAM_40_45        13
#define ROLL_DIAM_35_40        12
#define ROLL_DIAM_31_35        11
#define ROLL_DIAM_27_31        10
#define ROLL_DIAM_23_27        9
#define ROLL_DIAM_19_23        8
#define ROLL_DIAM_16_19        7
#define ROLL_DIAM_14_16        6
#define ROLL_DIAM_12_14        5
#define ROLL_DIAM_10_12        4
#define ROLL_DIAM_09_10        3
#define ROLL_DIAM_08_09        2
#define ROLL_DIAM_07_08        1
#define ROLL_DIAM_UNDER_7      0

#define SPEED_OVER_750         15
#define SPEED_700_750          14
#define SPEED_650_700          13
#define SPEED_600_650          12
#define SPEED_550_600          11
#define SPEED_500_550          10
#define SPEED_450_500          9
#define SPEED_400_450          8
#define SPEED_350_400          7
#define SPEED_300_350          6
#define SPEED_250_300          5
#define SPEED_200_250          4
#define SPEED_150_200          3
#define SPEED_100_150          2
#define SPEED_50_100           1
#define SPEED_0_50             0

#define SECONDS_UNIT           100.8

UDINT timeSinceReset;

void _INIT ProgramInit(void)
{
	// Insert code here 
   rollChangeDetectionState = 0;
   timeSinceReset = 0;
}

void _CYCLIC ProgramCyclic(void)
{
   lP++;
   if (lP>=10) lP = 0;
   
   if (lP == 9) lastSpd = lastSpeeds[0];
   else lastSpd = lastSpeeds[lP+1];
   
   lastSpeeds[lP] = cutter.reading.webSpeed;
   
   secTick += SECONDS_UNIT;
   
   if (secTick > 1000)
   {
      timeSinceReset++;
      if (buffer.reading.infeedSpeed > 0.05) 
      {
         if (unwinder.reading.rollDiameter > 50 * 25.4) groupedCnt.rollDiaGrouped[15].timeCnt++;
         else if (unwinder.reading.rollDiameter > 45 * 25.4) groupedCnt.rollDiaGrouped[14].timeCnt++;
         else if (unwinder.reading.rollDiameter > 40 * 25.4) groupedCnt.rollDiaGrouped[13].timeCnt++;
         else if (unwinder.reading.rollDiameter > 35 * 25.4) groupedCnt.rollDiaGrouped[12].timeCnt++;
         else if (unwinder.reading.rollDiameter > 31 * 25.4) groupedCnt.rollDiaGrouped[11].timeCnt++;
         else if (unwinder.reading.rollDiameter > 27 * 25.4) groupedCnt.rollDiaGrouped[10].timeCnt++;
         else if (unwinder.reading.rollDiameter > 23 * 25.4) groupedCnt.rollDiaGrouped[9].timeCnt++;
         else if (unwinder.reading.rollDiameter > 19 * 25.4) groupedCnt.rollDiaGrouped[8].timeCnt++;
         else if (unwinder.reading.rollDiameter > 16 * 25.4) groupedCnt.rollDiaGrouped[7].timeCnt++;
         else if (unwinder.reading.rollDiameter > 14 * 25.4) groupedCnt.rollDiaGrouped[6].timeCnt++;
         else if (unwinder.reading.rollDiameter > 12 * 25.4) groupedCnt.rollDiaGrouped[5].timeCnt++;
         else if (unwinder.reading.rollDiameter > 10 * 25.4) groupedCnt.rollDiaGrouped[4].timeCnt++;
         else if (unwinder.reading.rollDiameter > 9 * 25.4) groupedCnt.rollDiaGrouped[3].timeCnt++;
         else if (unwinder.reading.rollDiameter > 8 * 25.4) groupedCnt.rollDiaGrouped[2].timeCnt++;
         else if (unwinder.reading.rollDiameter > 7 * 25.4) groupedCnt.rollDiaGrouped[1].timeCnt++;
         else  groupedCnt.rollDiaGrouped[0].timeCnt++;
      }
      
      if (lastSpd > 750 * 196.85) groupedCnt.speedGrouped[15].timeCnt++;
      else if (lastSpd > 700 * 196.85) groupedCnt.speedGrouped[14].timeCnt++;
      else if (lastSpd > 650 * 196.85) groupedCnt.speedGrouped[13].timeCnt++;
      else if (lastSpd > 600 * 196.85) groupedCnt.speedGrouped[12].timeCnt++;
      else if (lastSpd > 550 * 196.85) groupedCnt.speedGrouped[11].timeCnt++;
      else if (lastSpd > 500 * 196.85) groupedCnt.speedGrouped[10].timeCnt++;
      else if (lastSpd > 450 * 196.85) groupedCnt.speedGrouped[9].timeCnt++;
      else if (lastSpd > 400 * 196.85) groupedCnt.speedGrouped[8].timeCnt++;
      else if (lastSpd > 350 * 196.85) groupedCnt.speedGrouped[7].timeCnt++;
      else if (lastSpd > 300 * 196.85) groupedCnt.speedGrouped[6].timeCnt++;
      else if (lastSpd > 250 * 196.85) groupedCnt.speedGrouped[5].timeCnt++;
      else if (lastSpd > 200 * 196.85) groupedCnt.speedGrouped[4].timeCnt++;
      else if (lastSpd > 150 * 196.85) groupedCnt.speedGrouped[3].timeCnt++;
      else if (lastSpd > 100 * 196.85) groupedCnt.speedGrouped[2].timeCnt++;
      else if (lastSpd > 50 * 196.85) groupedCnt.speedGrouped[1].timeCnt++;
      else groupedCnt.speedGrouped[0].timeCnt++;
      
      if (lineController.reading.lineReady) kpi.readyTime;
      if (timeSinceReset < 0) timeSinceReset = 0;
      
      if (timeSinceReset>10)
      {
         kpi.ready = (100.0 * (REAL)kpi.readyTime) / (REAL)timeSinceReset;
      }
      
      secTick -= 1000;
      
      
      switch (rollChangeDetectionState)
      {
         case 0:
            if (unwinder.reading.rollDiameter < 20 && unwinder.reading.status.state == Status_Ready) rollChangeDetectionState = 1; 
         break;
         case 1:
            if (unwinder.reading.rollDiameter > 40 && unwinder.reading.status.state == Status_Ready) rollChangeDetectionState = 0; 
            rollChanges++;
         break;
      }
      
      
      
   }
   
   if (lastMeter != stacker.reading.tripCounter.meters && stacker.reading.tripCounter.meters > 0)
   {
      if (stacker.reading.tripCounter.meters < 1) kpi.waste = 0;
      if (cutter.reading.output.reject) kpi.waste++;
      kpi.quality = ( 100.0 * (REAL)stacker.reading.tripCounter.meters - (REAL)kpi.waste )  / ((REAL)stacker.reading.tripCounter.meters);
   }
   
   
	 
   
   if (machineError.active && lastInError)
   {
   
        if (lastSpd > 750 * 196.85) groupedCnt.speedGrouped[15].errCnt++;
        else if (lastSpd > 700 * 196.85) groupedCnt.speedGrouped[14].errCnt++;
        else if (lastSpd > 650 * 196.85) groupedCnt.speedGrouped[13].errCnt++;
        else if (lastSpd > 600 * 196.85) groupedCnt.speedGrouped[12].errCnt++;
        else if (lastSpd > 550 * 196.85) groupedCnt.speedGrouped[11].errCnt++;
        else if (lastSpd > 500 * 196.85) groupedCnt.speedGrouped[10].errCnt++;
        else if (lastSpd > 450 * 196.85) groupedCnt.speedGrouped[9].errCnt++;
        else if (lastSpd > 400 * 196.85) groupedCnt.speedGrouped[8].errCnt++;
        else if (lastSpd > 350 * 196.85) groupedCnt.speedGrouped[7].errCnt++;
        else if (lastSpd > 300 * 196.85) groupedCnt.speedGrouped[6].errCnt++;
        else if (lastSpd > 250 * 196.85) groupedCnt.speedGrouped[5].errCnt++;
        else if (lastSpd > 200 * 196.85) groupedCnt.speedGrouped[4].errCnt++;
        else if (lastSpd > 150 * 196.85) groupedCnt.speedGrouped[3].errCnt++;
        else if (lastSpd > 100 * 196.85) groupedCnt.speedGrouped[2].errCnt++;
        else if (lastSpd > 50 * 196.85) groupedCnt.speedGrouped[1].errCnt++;
        else groupedCnt.speedGrouped[0].errCnt++;
      
        if (unwinder.reading.rollDiameter > 50 * 25.4) groupedCnt.rollDiaGrouped[15].errCnt++;
      else if (unwinder.reading.rollDiameter > 45 * 25.4) groupedCnt.rollDiaGrouped[14].errCnt++;
      else if (unwinder.reading.rollDiameter > 40 * 25.4) groupedCnt.rollDiaGrouped[13].errCnt++;
      else if (unwinder.reading.rollDiameter > 35 * 25.4) groupedCnt.rollDiaGrouped[12].errCnt++;
      else if (unwinder.reading.rollDiameter > 31 * 25.4) groupedCnt.rollDiaGrouped[11].errCnt++;
      else if (unwinder.reading.rollDiameter > 27 * 25.4) groupedCnt.rollDiaGrouped[10].errCnt++;
      else if (unwinder.reading.rollDiameter > 23 * 25.4) groupedCnt.rollDiaGrouped[9].errCnt++;
      else if (unwinder.reading.rollDiameter > 19 * 25.4) groupedCnt.rollDiaGrouped[8].errCnt++;
      else if (unwinder.reading.rollDiameter > 16 * 25.4) groupedCnt.rollDiaGrouped[7].errCnt++;
      else if (unwinder.reading.rollDiameter > 14 * 25.4) groupedCnt.rollDiaGrouped[6].errCnt++;
      else if (unwinder.reading.rollDiameter > 12 * 25.4) groupedCnt.rollDiaGrouped[5].errCnt++;
      else if (unwinder.reading.rollDiameter > 10 * 25.4) groupedCnt.rollDiaGrouped[4].errCnt++;
      else if (unwinder.reading.rollDiameter > 9 * 25.4) groupedCnt.rollDiaGrouped[3].errCnt++;
      else if (unwinder.reading.rollDiameter > 8 * 25.4) groupedCnt.rollDiaGrouped[2].errCnt++;
      else if (unwinder.reading.rollDiameter > 7 * 25.4) groupedCnt.rollDiaGrouped[1].errCnt++;
      else  groupedCnt.rollDiaGrouped[0].errCnt++;
      
      
        lE[24] = lE[23];
        lE[23] = lE[22];
        lE[22] = lE[21];    
        lE[21] = lE[20];
        lE[20] = lE[19];
        lE[19] = lE[18];
        lE[18] = lE[17];    
        lE[17] = lE[16];
        lE[16] = lE[15];
        lE[15] = lE[14];    
        lE[14] = lE[13];
        lE[13] = lE[12];
        lE[12] = lE[11];
        lE[11] = lE[10];    
        lE[10] = lE[9];
        lE[9] = lE[8]; 
        lE[8] = lE[7];
        lE[7] = lE[6];    
        lE[6] = lE[5];
        lE[5] = lE[4];
        lE[4] = lE[3];
        lE[3] = lE[2];    
        lE[2] = lE[1];
        lE[1] = lE[0];    
      
        lE[0].dist = stacker.reading.tripCounter.meters;
        lE[0].no = machineError.errType;
        lE[0].sub = machineError.errSubType;
        lE[0].rollDia = unwinder.reading.rollDiameter;
        lE[0].speed = lastSpd;
        lE[0].tss = lineController.reading.timeSinceStart;
   }
   
   
   if (buffer.setting.maxInfeedSpeed>0)
   {
      kpi.speedPerformance = 1.667 * stacker.reading.tripCounter.avgSpeed / buffer.setting.maxInfeedSpeed;
   }
   
   kpi.performance = (kpi.speedPerformance + kpi.quality + kpi.ready) / 3.0;
   
   lastMeter = stacker.reading.tripCounter.meters;
   lastInError = machineError.active;
}

void _EXIT ProgramExit(void)
{
}

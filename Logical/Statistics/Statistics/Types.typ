
TYPE
	stat_typ : 	STRUCT 
		rollDiaGrouped : ARRAY[0..15]OF s_error;
		speedGrouped : ARRAY[0..15]OF s_error;
	END_STRUCT;
	s_error : 	STRUCT 
		timeCnt : UDINT;
		errCnt : UINT;
	END_STRUCT;
	l_error : 	STRUCT 
		dist : REAL; (*m*)
		rollDia : REAL; (*mm*)
		speed : REAL; (*m/s*)
		tss : UDINT; (*s*)
		recipe : USINT; (*no*)
		sub : UINT; (*no*)
		no : UINT; (*no*)
	END_STRUCT;
	kpi_typ : 	STRUCT 
		quality : REAL;
		ready : REAL;
		speedPerformance : REAL;
		performance : REAL;
		readyTime : UDINT;
		waste : UDINT;
	END_STRUCT;
END_TYPE

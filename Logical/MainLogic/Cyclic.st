
PROGRAM _CYCLIC

   IF ((*machineStarted AND *)cutter.setting.markRegistration) THEN
      
      // check if ActPosition is greater than startRejectPos considering possible wraparound 
      IF (Master[0].Status.ActPosition > startRejectPos AND startRejectPos + (wrapAround - Master[0].Status.ActPosition) <=  wrapAround / 2.0) THEN
          isGreater := FALSE;
      ELSIF (Master[0].Status.ActPosition < startRejectPos AND Master[0].Status.ActPosition + (wrapAround - startRejectPos) <=  wrapAround / 2.0) THEN
          isGreater := TRUE;
      ELSIF (Master[0].Status.ActPosition > startRejectPos) THEN
          isGreater := TRUE;
      ELSE
          isGreater := FALSE;
      END_IF            
      IF (startRejectPos > 0.0 AND isGreater) THEN
          startRejectPos := 0.0;
          //searchForMark_mainKnife := TRUE;
          //searchForMark_stripKnife := TRUE;
          //searchForMark_punch := TRUE;
          rejectSheets := TRUE;
         //rejectLoadPos := 0.0;
         lineController.command.beep := TRUE;
          //IF (cutter.setting.stackWhitePages) THEN
             stacker.command.instantDeliver := TRUE;
          //END_IF      
      END_IF
   END_IF
   IF (rejectSheets(* AND machineStarted*)) THEN
      IF ( cutter.setting.markRegistration  AND skipMarksAfterWhite = 0) THEN
         // check if ActPosition is greater than rejectLoadPos considering possible wraparound                         
         IF (Master[0].Status.ActPosition > rejectLoadPos AND rejectLoadPos + (wrapAround - Master[0].Status.ActPosition) <=  wrapAround / 2.0) THEN
             isGreater := FALSE;
         ELSIF (Master[0].Status.ActPosition < rejectLoadPos AND Master[0].Status.ActPosition + (wrapAround - rejectLoadPos) <=  wrapAround / 2.0) THEN
             isGreater := TRUE;
         ELSIF (Master[0].Status.ActPosition > rejectLoadPos) THEN
             isGreater := TRUE;
         ELSE
             isGreater := FALSE;
         END_IF            
         IF (rejectLoadPos > 0.0 AND isGreater) THEN
            loadingSpeedLimit := 5.0;
            cutter.reading.processingWhite := FALSE;
            rejectSheets := FALSE;
            IF cutter.command.rejectSheets THEN
               cutter.command.rejectSheets := FALSE;
            END_IF
            rejectLoadPos := 0;
            ULog.text[ULog.ix] := '??? 1';
            ULog.ix := ULog.ix + 1;
            //sheetCounter := 0;
         END_IF   
      ELSE
         IF (sheetCounter = sheetCountAtLoad + 2) THEN // second cut after loading
            rejectSheets := FALSE;
            ULog.text[ULog.ix] := '??? 2';
            ULog.ix := ULog.ix + 1;
            //sheetCounter := 0;
         END_IF
      END_IF
   END_IF
   
   IF (sheetCounter <> oldSheetCounter) THEN
      oldSheetCounter := sheetCounter;
   END_IF

   // ***************** Calculate next stopping position ************************************
   realCutPos := cutPosition;
   //distToNextCut := cutter.setting.format - (Master[0].Status.ActPosition - (UDINT_TO_REAL(cutPosition) / 100.0));
   IF (realCutPos < Master[0].Status.ActPosition) THEN
      distToNextCut := realCutPos + wrapAround - Master[0].Status.ActPosition;
   ELSE
      distToNextCut := realCutPos - Master[0].Status.ActPosition;
   END_IF
   minStopDist := (Master[0].Status.ActVelocity * Master[0].Status.ActVelocity) / (2.0 * cutter.setting.masterDec);
   distToStop := distToNextCut + cutter.setting.stopOffset;
         
   // check if we could stop faster
   WHILE (distToStop > cutter.setting.format + minStopDist + 100) DO  // + 100 so we have som margin for the speed compensation
      distToStop := distToStop - cutter.setting.format;
   END_WHILE
   // or if we can't stop in time
   WHILE (distToStop < minStopDist + 100) DO
      distToStop := distToStop + cutter.setting.format;
   END_WHILE

   // some minor speed compensation
   //distToStop := distToStop - ((41.0 * Master[0].Status.ActVelocity) / 2000.0); // was 31
   stopControl.nextStopPos := distToStop + Master[0].Status.ActPosition;
   IF(stopControl.nextStopPos > wrapAround) THEN
      stopControl.nextStopPos := stopControl.nextStopPos - wrapAround;
   END_IF      
   
   // ***************** HOME(LOAD) COMMAND *********************************************************
   IF (cutter.command.home = TRUE AND commandHome_state = 0) THEN
      skipMarksAfterWhite := 0;
      searchForMark_mainKnife := TRUE;
      searchForMark_stripKnife := TRUE;
      searchForMark_punch := TRUE;
      rejectSheets := TRUE;
      rejectLoadPos := 0.0;
      skipToPrintLength := 0.0;
      cutter.reading.processingWhite := FALSE;
      whitePagesSpeedLimit := 5.0;
      loadingSpeedLimit := 5.0;
      commandHome_state := 1;
		commandHome_time := 0;
      stopControl.actualStopPos := 0.0;
      stopControl.offsetPos := 0.0;
      stopControl.OMRDeliverPos := 0.0;
      stopControl.slowSpeedPos := 0.0;
   END_IF
	IF (commandHome_state = 1) THEN
		GlobalCommand.Input.Command.Home := TRUE;
		commandHome_state := 2;
	END_IF
	IF (commandHome_state = 2) THEN
		commandHome_time := commandHome_time + 1;
		IF (commandHome_time > 12500) THEN
         GlobalCommand.Input.Command.Power := FALSE;
         globalPower := FALSE;
			commandHome_state := 0;
			cutter.command.home := FALSE;
		END_IF
		IF (GlobalCommand.Output.Status.AllHomingsOk = TRUE AND GlobalCommand.Input.Command.Home = FALSE) THEN
			GlobalCommand.Input.Command.ConnectSlavesToMaster := TRUE;
			commandHome_state := 3;
		END_IF
	END_IF
	IF (commandHome_state = 3) THEN
		commandHome_time := commandHome_time + 1;
		IF (commandHome_time > 12500) THEN
         GlobalCommand.Input.Command.Power := FALSE;
         globalPower := FALSE;
			commandHome_state := 0;
			cutter.command.home := FALSE;
		END_IF
		IF(GlobalCommand.Input.Command.ConnectSlavesToMaster = FALSE) THEN
			GlobalCommand.Input.Command.EnableCutMain := TRUE;
			IF(cutter.setting.stripCut) THEN
				GlobalCommand.Input.Command.EnableCutSecondary := TRUE;
			END_IF
         IF(merger.setting.punchUnitEnabled) THEN
            GlobalCommand.Input.Command.EnableCutPunch := TRUE;
         END_IF
         GlobalCommand.Input.Command.InitDataMain := TRUE;
			GlobalCommand.Input.Command.InitDataSecondary := TRUE;
			GlobalCommand.Input.Command.InitDataPunch := TRUE;
			// cutter.setting.markRegistration := TRUE;
         GlobalCommand.Input.Parameter.Velocity := 0;
         demandSpeed := 0;
         GlobalCommand.Input.Command.MoveVelocity := TRUE;
         sheetCountAtLoad := sheetCounter;
         oldSheetCounter := sheetCounter;
			commandHome_state := 0;
			cutter.command.home := FALSE;
         //stopUpdateSpeed := FALSE;
      END_IF			
	END_IF

   
   (***************** RELOAD COMMAND *****************************************************************
   IF (cutter.command.reload = TRUE AND cutter.setting.markRegistration) THEN
      skipMarksAfterWhite := 0;
      stacker.command.stoppedDeliveryRequest := TRUE;
      rejectSheets := TRUE;
      rejectLoadPos := 0.0;
      searchForMark_mainKnife := TRUE;
      searchForMark_stripKnife := TRUE;
      searchForMark_punch := TRUE;
      lineController.command.beep := TRUE;
      skipToPrintLength := 0.0;
      cutter.reading.processingWhite := FALSE;
      whitePagesSpeedLimit := 5.0;
      loadingSpeedLimit := 5.0;
      cutter.command.reload := FALSE;
   END_IF *)
   
   // ***************** RELOAD COMMAND *****************************************************************
   IF (cutter.command.reload = TRUE) THEN
      IF (commandReload_state = 0) THEN
         lineController.command.beep := TRUE;
         streamer.command.feedToStack := TRUE; //mac
         IF (GlobalCommand.Input.Command.Power = TRUE) THEN
            GlobalCommand.Input.Command.Power := FALSE;
            powerOff := TRUE; // power would otherwise be instantly restarted in main::main 
            commandReload_time := 0;      
            commandReload_state := 1;
         ELSE
            commandReload_time := 500;      
            commandReload_state := 2;            
         END_IF
      END_IF
      IF (commandReload_state = 1) THEN
         commandReload_time := commandReload_time + 1;
         IF (commandReload_time > 500) THEN
            GlobalCommand.Input.Command.Power := TRUE;
            powerOff := FALSE;
            commandReload_state := 2;
            commandReload_time := 0;
         END_IF   
      END_IF      
      IF (commandReload_state = 2) THEN
         commandReload_time := commandReload_time + 1;
         IF (commandReload_time > 500) THEN
            cutter.command.reload := FALSE;
            cutter.command.home := TRUE;
            commandReload_state := 0;
         END_IF   
      END_IF
   END_IF
   
   // ***************** SKIP TO PRINT COMMAND **********************************************************
   IF (cutter.command.skipToPrint = TRUE AND cutter.setting.markRegistration) THEN
      stacker.command.stoppedDeliveryRequest := TRUE;
      
      skipMarksAfterWhite := sheetCounter + REAL_TO_UDINT((cutter.setting.skipToPrintLength * 1000.0) / cutter.setting.format);         
      IF cutter.setting.rejectSkipToPrint THEN
          cutter.command.rejectSheets := TRUE;
      ELSE
          sheetCountAtLoad := sheetCounter;      
      END_IF
            
      //skipToPrintLength := cutter.setting.skipToPrintLength + buffer.reading.accumulated - 0.2;
      //startingSkipToPrint := TRUE;
      //whitePagesSpeedLimit := 1.0;
      cutter.command.start := TRUE;
      cutter.command.skipToPrint := FALSE;
      rejectLoadPos := 0.0;
   END_IF
   
      // ***************** CUT POS FWD COMMAND *********************************************************
   IF (cutter.command.cutPlus = TRUE) THEN
      cutter.command.cutPlus := FALSE;
      cutter.setting.regMarkOffset := cutter.setting.regMarkOffset + 0.2;
   END_IF

   // ***************** CUT POS BWD COMMAND *********************************************************
   IF (cutter.command.cutMinus = TRUE) THEN
      cutter.command.cutMinus := FALSE;
      cutter.setting.regMarkOffset := cutter.setting.regMarkOffset - 0.2;
   END_IF

   // ***************** FEED COMMAND *********************************************************   
   IF (cutter.command.feed = TRUE AND commandFeed_state = 0) THEN
      commandFeed_state := 1;
      commandFeed_time := 0;
      IF (buffer.reading.noPaper = FALSE) THEN
         Master[0].Command.MoveVelocity := TRUE; // in case it's halted by buffer
         commandFeed_initialSpeed := GlobalCommand.Input.Parameter.Velocity;
         IF cutter.reading.fastFeed = TRUE THEN
            demandSpeed := 180;
            GlobalCommand.Input.Parameter.Velocity := 180;
         ELSE 
            demandSpeed := 50;
            GlobalCommand.Input.Parameter.Velocity := 50;
         END_IF
            
      ELSE //No paper MW
         commandFeed_initialSpeed := 0;
         demandSpeed := 0;
         GlobalCommand.Input.Parameter.Velocity := 0;
      END_IF
   END_IF
   IF(commandFeed_state > 0) THEN
      commandFeed_time := commandFeed_time + 1;
   END_IF
   IF (cutter.command.feed = FALSE AND commandFeed_state = 1) THEN
      commandFeed_state := 0;
      demandSpeed := commandFeed_initialSpeed;
      GlobalCommand.Input.Parameter.Velocity := commandFeed_initialSpeed;
   END_IF
   IF (cutter.command.feed = TRUE AND commandFeed_state = 1 AND buffer.reading.noPaper = TRUE) THEN //No paper MW
      commandFeed_state := 0;
      demandSpeed := commandFeed_initialSpeed;
      GlobalCommand.Input.Parameter.Velocity := commandFeed_initialSpeed;
   END_IF
	
	
   // ***************** START COMMAND *********************************************************
   IF (cutter.command.start = TRUE AND commandStart_state = 0) THEN
      cutter.command.stopNext := FALSE;
      afterSkipToPrint := FALSE;
      machineStarted := TRUE;
      buffer.command.releaseDecurl := TRUE;
      commandStart_state := 1;
      commandStart_time := 0;
      ULog.text[ULog.ix] := '----------------------------------------STARTING----------------------------------------------';
      ULog.ix := ULog.ix + 1;
   END_IF
   IF (commandStart_state = 1) THEN
      demandSpeed := cutter.setting.infeedRunSpeed ;
      commandStart_state := 0;
      cutter.command.start := FALSE;
   END_IF
	
   // ***************** INCREASE SPEED COMMAND ***********************************************
   IF (cutter.command.incSpeed = TRUE) THEN
      IF (Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity > 10) THEN         
         cutter.setting.infeedRunSpeed  := cutter.setting.infeedRunSpeed  + 100;
         demandSpeed := cutter.setting.infeedRunSpeed ;
      END_IF   
      cutter.command.incSpeed := FALSE;
   END_IF
   
   // ***************** DECREASE SPEED COMMAND ***********************************************
   IF (cutter.command.decSpeed = TRUE) THEN 
      IF (Slave[SLAVE_MAININFEED_INDEX].Status.ActVelocity > 10) THEN         
         cutter.setting.infeedRunSpeed  := cutter.setting.infeedRunSpeed  - 100;
         IF (cutter.setting.infeedRunSpeed  < 0) THEN
            cutter.setting.infeedRunSpeed  := 0;
         END_IF
         demandSpeed := cutter.setting.infeedRunSpeed ;
      END_IF      
      cutter.command.decSpeed := FALSE;
   END_IF   
   
   // ***************** DELIVER COMMAND ******************************************************
   IF (cutter.command.deliver = TRUE AND commandDeliver_state = 0) THEN
      IF (cutter.setting.stopWhenDelivering OR cutter.command.stopNext) THEN
         commandDeliver_time := 0;
         IF (Master[0].Status.ActVelocity > 0) THEN
            cutter.command.stop := TRUE;
            commandDeliver_state := 1;
         ELSE
            commandDeliver_state := 0;
            cutter.command.deliver := FALSE;
         END_IF
      ELSE
         commandDeliver_state := 0;
         cutter.command.deliver := FALSE;
      END_IF
   END_IF
   IF (commandDeliver_state = 1) THEN
      commandDeliver_time := commandDeliver_time + 1;
      IF(Master[0].Status.ActVelocity = 0) THEN
         zeroSpeedHold := 0;
         commandDeliver_state := 2;
      END_IF
   END_IF
   IF (commandDeliver_state = 2) THEN
      commandDeliver_time := commandDeliver_time + 1;
      zeroSpeedHold := zeroSpeedHold + 1;
      IF(zeroSpeedHold > ((cutter.setting.deliverStopTime * 10) / 24)) THEN
         IF(NOT cutter.command.stopNext) THEN
            ULog.text[ULog.ix] := 'Starting up after delivery';
            ULog.ix := ULog.ix + 1;
            cutter.command.start := TRUE;
         ELSE
            cutter.command.stopNext := FALSE;
            machineStarted := FALSE;
         END_IF   
         commandDeliver_state := 0;
         cutter.command.deliver := FALSE;
      END_IF      
   END_IF
   
   // ***************** STOP REQUEST COMMAND (GUI stop button)********************************************
   IF (cutter.command.stopRequest = TRUE OR cutter.command.stop = TRUE) THEN
       cutter.command.stopRequest := FALSE;
       cutter.command.stop := FALSE;
      IF (NOT cutter.command.deliver) THEN
         machineStarted := FALSE;
      END_IF;      
      IF (Master[0].Status.ActVelocity < 90) THEN
           demandSpeed := 0;
      ELSE
           absMoveStop := 1;
           commandStop_state := 1; // reset to zero in VirtualMaster::masterCyclic
      END_IF
   (*
      // speed limited to 980 in main.c on cutter.command.stopRequest
      IF (Master[0].Status.ActVelocity < 1000) THEN
         cutter.command.stop := TRUE;
      END_IF
   *)
   END_IF
   
   // ***************** STOP COMMAND (immediate) *********************************************************
   (*
   IF (cutter.command.stop = TRUE AND commandStop_state = 0) THEN
      IF (NOT cutter.command.deliver) THEN
            machineStarted := FALSE;
      END_IF;
      commandStop_time := 0;      
      
      //IF (GlobalCommand.Input.Parameter.Velocity = 0) THEN
      IF (Master[0].Status.ActVelocity < 90) THEN
         demandSpeed := 0;
         commandStop_state := 0;
         cutter.command.stopRequest := FALSE;
         cutter.command.stop := FALSE;
      ELSE
         // Hold current speed if acc or dec
         GlobalCommand.Input.Parameter.Velocity := Master[0].Status.ActVelocity;
         demandSpeed := Master[0].Status.ActVelocity;
         cutter.command.stopRequest := FALSE;
         
         stopControl.actualStopPos := stopControl.nextStopPos;
         
         // how long should we run before braking
         timeToBrake := stopControl.nextStopPos - minStopDist;
         IF(timeToBrake < 0.0) THEN
            timeToBrake := timeToBrake + wrapAround;
         END_IF
         
         //timeToBrake := (distToStop - minStopDist) + Master[0].Status.ActPosition;
         commandStop_state := 3;
      END_IF      
   END_IF

   IF (commandStop_state = 1) THEN
      IF((waitForCut <> sheetCounter) OR (DiffT(clock_ms(), waitForCutTimeout) > 3000)) THEN
         GlobalCommand.Input.Parameter.Velocity := 0;
         demandSpeed := 0;
         commandStop_state := 2;
      END_IF                  
   END_IF

   IF (commandStop_state = 2) THEN
      GlobalCommand.Input.Parameter.Velocity := 0;
      demandSpeed := 0;
      IF (Master[0].Status.ActVelocity = 0 AND commandDeliver_state = 0) THEN
			stopControl.actualStopPos := 0;
			commandFeed_initialSpeed := 0;
			cutter.command.stop := FALSE;
         commandStop_state := 0;
      END_IF
   END_IF   

   IF (commandStop_state = 3) THEN
      IF (Master[0].Status.ActVelocity = 0) THEN  // speed should never be 0 but if still is -> leave the state
         GlobalCommand.Input.Parameter.Velocity := 0;
         demandSpeed := 0;
         commandStop_state := 2;         
      END_IF

      // check if ActPosition is greater than timeToBrake considering possible wraparound 
      IF (Master[0].Status.ActPosition < timeToBrake AND Master[0].Status.ActPosition + (wrapAround - timeToBrake) <=  wrapAround / 2.0) THEN
         isGreater := TRUE;
      ELSIF (Master[0].Status.ActPosition > timeToBrake) THEN
         isGreater := TRUE;
      ELSE
         isGreater := FALSE;
      END_IF            
      IF (isGreater) THEN
         GlobalCommand.Input.Parameter.Velocity := 0;
         demandSpeed := 0;
         commandStop_state := 2;
      END_IF
   END_IF   
   *)
      
   // ***************** Check for new format *********************************************************
   IF (oldFormat <> cutter.setting.format) THEN
      oldFormat := cutter.setting.format;
      Slave[SLAVE_PUNCH_INDEX].Parameter.ProductLength := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.ProductLength := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.ProductLength := cutter.setting.format * 305 / 307;
      Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.ProductLength := Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.RegMarkConfiguration.ProductLength := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.ProductLength := cutter.setting.format;
      //IF (cutter.setting.format < 355.6) THEN
      //   Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := cutter.setting.format / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
      //ELSE	
      //   Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := 355.6 / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
      //END_IF
      IF (cutter.setting.format < 254.0) THEN
         Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := cutter.setting.format / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
      ELSE	
         Slave[SLAVE_SECONDARYKNIFE_INDEX].Parameter.CutRangeMaster := Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeMaster := 254.0 / (36000 / Slave[SLAVE_MAINKNIFE_INDEX].Parameter.CutRangeSlave);
      END_IF
      GlobalCommand.Input.Command.InitDataMain := TRUE;
      GlobalCommand.Input.Command.InitDataSecondary := TRUE;
      GlobalCommand.Input.Command.InitDataPunch := TRUE;
   END_IF

   // ***************** Check for new strip size *********************************************************
   IF (oldStripSize <> cutter.setting.stripSize) THEN

      IF(cutter.setting.stripCut) THEN
         Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := 500 + cutter.setting.stripSize;
         Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := cutter.setting.sensorKnifeDist + cutter.setting.stripSize;
      ELSE
         Slave[SLAVE_MAINKNIFE_INDEX].Parameter.AdvancedParameter.MasterStartDistance := 500;
         Slave[SLAVE_MAINKNIFE_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := cutter.setting.sensorKnifeDist;
      END_IF
      Slave[SLAVE_PUNCH_INDEX].Parameter.AdvancedParameter.MasterStartDistance := 500 + cutter.setting.stripSize / 2.0;
      Slave[SLAVE_PUNCH_INDEX].Parameter.RegMarkConfiguration.DistanceToSensor := merger.setting.sensorPunchDist + cutter.setting.stripSize / 2.0;
            
      oldStripSize := cutter.setting.stripSize;
   END_IF
	
	
   // ***************** Set Register Mark Control on or off *********************************************************
   //IF skipToPrintLength > 0.0 THEN
    IF skipMarksAfterWhite > 0 THEN
      IF wasMarkReg THEN
            wasMarkReg := FALSE;
            ULog.text[ULog.ix] := REAL_TO_STRING(Master[0].Status.ActPosition);
            ULog.text[ULog.ix] := CONCAT('=== Mark reg OFF ', ULog.text[ULog.ix]);
            ULog.ix := ULog.ix + 1;
      END_IF
        
      //GlobalCommand.Input.Command.EnableRegMarkControlMain := FALSE;
      //GlobalCommand.Input.Command.EnableRegMarkControlSecondary := FALSE;
      //GlobalCommand.Input.Command.EnableRegMarkControlPunch := FALSE;
      //searchForMark_mainKnife := TRUE;
      //searchForMark_stripKnife := TRUE;
      //searchForMark_punch := TRUE;
    ELSE
      IF NOT wasMarkReg THEN
            wasMarkReg := TRUE;
            ULog.text[ULog.ix] := REAL_TO_STRING(Master[0].Status.ActPosition);
            ULog.text[ULog.ix] := CONCAT('=== Mark reg ON ', ULog.text[ULog.ix]);
            ULog.ix := ULog.ix + 1;
      END_IF
      GlobalCommand.Input.Command.EnableRegMarkControlMain := cutter.setting.markRegistration;
      GlobalCommand.Input.Command.EnableRegMarkControlSecondary := cutter.setting.markRegistration;
      GlobalCommand.Input.Command.EnableRegMarkControlPunch := cutter.setting.markRegistration;
   END_IF

   // ***************** Connect/disconnect slaves *********************************************************
   IF GlobalCommand.Output.Status.AllSlavesSynchronizedMotion = TRUE THEN
      showDisConnectButton.0 := 0;
   ELSE
      showDisConnectButton.0 := 1;
   END_IF

   IF GlobalCommand.Output.Status.AllSlavesNotSynchronizedMotion = TRUE THEN
      showConnectButton.0 := 0;
   ELSE
      showConnectButton.0 := 1;
   END_IF
		
	
	END_PROGRAM



PROGRAM _INIT
   cutter.command.deliver := FALSE;
   commandDeliver_state := FALSE;
   commandDeliver_time := 0;
	cutter.command.home := FALSE;
	commandHome_state := 0;
	cutter.command.feed := FALSE;
	commandFeed_state := 0;
	commandFeed_time := 0;
	cutter.command.start := FALSE;
	commandStart_state := 0;
	commandStart_time := 0;
	cutter.command.stop := FALSE;
	commandStop_state := 0;	
	commandStop_time := 0;
	//infeedRunSpeed := 100;
	//cutter.setting.infeedRunSpeed := 100;
	oldFormat := 0;
	//cutter.setting.format := 305;
	oldStripSize := 0;
	//cutter.setting.stripSize := 8;
END_PROGRAM
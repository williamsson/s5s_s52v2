
TYPE
	u20_typ : 	STRUCT 
		connected : BOOL := FALSE;
		started : BOOL := FALSE;
		slowMode : BOOL := FALSE;
		diameter : REAL;
		speed : REAL;
		timeRemaining : STRING[6];
	END_STRUCT;
	machineError_typ : 	STRUCT 
		active : BOOL := FALSE;
		errType : UDINT := 0;
		errSubType : UDINT := 0;
		stopType : USINT := 0; (*0 = normal stop, 1 = immediate stop*)
	END_STRUCT;
	machineState_enum : 
		(
		Status_NotLoaded,
		Status_Loading,
		Status_ManualLoad,
		Status_Ready,
		Status_Error
		);
	machineStatus_typ : 	STRUCT 
		state : machineState_enum;
		warning : BOOL;
		stopped : BOOL;
	END_STRUCT;
	OMRMark_parameter_typ : 	STRUCT 
		pos : REAL := 0;
		width : REAL := 0;
		errMargin : REAL;
	END_STRUCT;
	OMRMark_status_typ : 	STRUCT 
		searching : BOOL := FALSE;
		validMark : BOOL := FALSE;
		pos : REAL := 0;
		width : REAL := 0;
	END_STRUCT;
	OMRMark_typ : 	STRUCT 
		status : OMRMark_status_typ;
		parameter : OMRMark_parameter_typ;
	END_STRUCT;
	output_typ : 	STRUCT 
		pVal : BOOL := FALSE; (*program value*)
		fVal : BOOL := FALSE; (*forced value*)
		oVal : BOOL := FALSE; (*output value*)
		force : BOOL := FALSE; (*force output*)
	END_STRUCT;
	parameter_typ : 	STRUCT  (*parameter structure*)
		Position : REAL; (*target position for MoveAbsolute-Command*)
		Distance : REAL; (*distance for MoveAdditive-Command*)
		Velocity : REAL; (*velocity for MoveVelocity-Command*)
		Direction : USINT; (*direction for commanded movements*)
		Acceleration : REAL; (*acceleration for commanded movements*)
		Deceleration : REAL; (*deceleration for commanded movements*)
		HomePosition : REAL; (*target position for referencing the axis*)
		HomeMode : USINT; (*homing mode*)
		JogVelocity : REAL; (*velocity for jogging movement*)
		RatioNumerator : INT;
		RatioDenominator : UINT;
		CutRangeMaster : REAL;
		CutRangeSlave : REAL;
		ProductLength : REAL;
		ProductLengthCorrection : REAL;
		AdvancedParameter : MC_ADVANCED_CUT_PAR_REF;
		RegMarkConfiguration : MC_BR_CFG_RM2_REF;
		RegMarkAdvancedParameter : MC_BR_ADV_RM2_REF;
	END_STRUCT;
	physical_io_typ : 	STRUCT 
		buffer : physical_machine;
		cutter : physical_machine;
		folder : physical_machine;
		merger : physical_machine;
		streamer : physical_machine;
		stacker : physical_machine;
	END_STRUCT;
	physical_machine : 	STRUCT 
		digital : p_digital_typ;
		analog : p_analog_typ;
		valve : p_valve_typ;
		counter : p_counter_typ;
	END_STRUCT;
	pifGlobal_typ : 	STRUCT 
		printer : sourcePrinter_typ;
		machine : sourceMachine_typ;
		availiableMachineSignals : sourceMachine_typ;
		availiablePrinterSignals : sourcePrinter_typ;
		limiter : USINT;
		msgCnt : USINT;
	END_STRUCT;
	psm_command_typ : 	STRUCT  (*command structure for power supply modules*)
		Power : BOOL; (*switch on the controller of the psm*)
		StartPowerMeter : BOOL; (*start the power evaluation of psm*)
		StopPowerMeter : BOOL; (*stop the power evaluation of psm*)
		RestartInterval : BOOL; (*restart the interval of power evaluation*)
	END_STRUCT;
	psm_parameter_typ : 	STRUCT  (*parameter structure for psm*)
		IntervalTime : UINT; (*interval time for the power evaluation*)
	END_STRUCT;
	psm_status_typ : 	STRUCT  (*status structure for psm*)
		PowerData : MC_POWERDATA_REF; (*evaluated power data*)
		MissedInterval : UINT; (*MissedIntervals with power evaluation*)
		DriveStatus : MC_DRIVESTATUS_TYP; (*actual status of the axis*)
	END_STRUCT;
	psm_typ : 	STRUCT  (*substructure for psm*)
		Command : psm_command_typ; (*command structure for psm*)
		Parameter : psm_parameter_typ; (*parameter structure for psm*)
		Status : psm_status_typ; (*status structure for psm*)
		AxisState : axisState_typ; (*axis state structure*)
		Error : error_typ; (*error structure*)
	END_STRUCT;
	p_analog_typ : 	STRUCT 
		in1 : INT;
		in2 : INT;
		in3 : INT;
		in4 : INT;
		out1 : INT;
		out2 : INT;
		out3 : INT;
		out4 : INT;
	END_STRUCT;
	p_valve_typ : 	STRUCT 
		mask : USINT;
	END_STRUCT;
	p_counter_typ : 	STRUCT 
		cnt1 : UINT;
		cnt2 : UINT;
		cnt3 : UINT;
		cnt4 : UINT;
	END_STRUCT;
	p_digital_typ : 	STRUCT 
		in1 : BOOL;
		in2 : BOOL;
		in3 : BOOL;
		in4 : BOOL;
		in5 : BOOL;
		in6 : BOOL;
		in7 : BOOL;
		in8 : BOOL;
		in9 : BOOL;
		in10 : BOOL;
		in11 : BOOL;
		in12 : BOOL;
		in13 : BOOL;
		in14 : BOOL;
		in15 : BOOL;
		in16 : BOOL;
		in17 : BOOL;
		in18 : BOOL;
		in19 : BOOL;
		in20 : BOOL;
		in21 : BOOL;
		in22 : BOOL;
		in23 : BOOL;
		in24 : BOOL;
		in25 : BOOL;
		in26 : BOOL;
		in27 : BOOL;
		in28 : BOOL;
		in29 : BOOL;
		in30 : BOOL;
		in31 : BOOL;
		in32 : BOOL;
		in33 : BOOL;
		in34 : BOOL;
		in35 : BOOL;
		in36 : BOOL;
		in37 : BOOL;
		in38 : BOOL;
		in39 : BOOL;
		in40 : BOOL;
		in41 : BOOL;
		in42 : BOOL;
		in43 : BOOL;
		in44 : BOOL;
		in45 : BOOL;
		in46 : BOOL;
		in47 : BOOL;
		in48 : BOOL;
		in49 : BOOL;
		in50 : BOOL;
		in51 : BOOL;
		in52 : BOOL;
		in53 : BOOL;
		in54 : BOOL;
		in55 : BOOL;
		in56 : BOOL;
		in57 : BOOL;
		in58 : BOOL;
		in59 : BOOL;
		in60 : BOOL;
		out1 : BOOL;
		out2 : BOOL;
		out3 : BOOL;
		out4 : BOOL;
		out5 : BOOL;
		out6 : BOOL;
		out7 : BOOL;
		out8 : BOOL;
		out9 : BOOL;
		out10 : BOOL;
		out11 : BOOL;
		out12 : BOOL;
		out13 : BOOL;
		out14 : BOOL;
		out15 : BOOL;
		out16 : BOOL;
		out17 : BOOL;
		out18 : BOOL;
		out19 : BOOL;
		out20 : BOOL;
		out21 : BOOL;
		out22 : BOOL;
		out23 : BOOL;
		out24 : BOOL;
		out25 : BOOL;
		out26 : BOOL;
		out27 : BOOL;
		out28 : BOOL;
		out29 : BOOL;
		out30 : BOOL;
		out31 : BOOL;
		out32 : BOOL;
		out33 : BOOL;
		out34 : BOOL;
		out35 : BOOL;
		out36 : BOOL;
		out37 : BOOL;
		out38 : BOOL;
		out39 : BOOL;
		out40 : BOOL;
	END_STRUCT;
	real_store_type : 	STRUCT 
		value : REAL;
		min : REAL;
		max : REAL;
	END_STRUCT;
	settingsF50_typ : 	STRUCT 
		tension : REAL;
	END_STRUCT;
	sheet : 	STRUCT 
		type : USINT := 0; (*Page type, 0=Normal, 1=offset, 2=deliver Page, 3=diverter page, 4=waste page, 5 =end page, 6=bypass page*)
		slitMerge : BOOL := FALSE; (*two pages, slit/merge mode*)
		code : STRING[80] := 'no_code'; (*page code (for future use)*)
		sequence : UDINT := 0; (*jobPageCounter, Must change for every new page.*)
		sheets : USINT := 1; (*1-up (1), 2-up (2), 3-up (3)....*)
		estimatedGap : REAL := 10; (*mm*)
		measuredGap : REAL := 10; (*mm*)
		sheetLength : REAL := 305; (*mm*)
		measuredSheetLength : REAL := 0; (*mm*)
		continuous : BOOL := FALSE; (*the current job/set continues after delivery*)
		jobCode : STRING[80] := 'no_code'; (*current job code (for future use)*)
	END_STRUCT;
	slave_command_typ : 	STRUCT  (*command structure for slave axes*)
		Power : BOOL; (*switch on the controller*)
		Home : BOOL; (*reference axis*)
		MoveJogPos : BOOL; (*move in positive direction as long as is set*)
		MoveJogNeg : BOOL; (*move in negative direction as long as is set*)
		MoveAbsolute : BOOL; (*move to a defined position*)
		MoveAdditive : BOOL; (*move a defined distance*)
		MoveVelocity : BOOL; (*start a movement with a defined velocity*)
		Stop : BOOL; (*stop every active movement on the axis as long as is set*)
		Halt : BOOL; (*stop every active movement on the axis*)
		StartSlave : BOOL;
		DisengageSlave : BOOL;
		StartCrossCutterControl : BOOL;
		StopCrossCutterControl : BOOL;
	END_STRUCT;
	slave_status_typ : 	STRUCT  (*status structure*)
		ActPosition : REAL; (*actual position of the axis*)
		ActVelocity : REAL; (*actual velocity of the axis*)
		DriveStatus : MC_DRIVESTATUS_TYP; (*actual status of the axis*)
	END_STRUCT;
	slave_typ : 	STRUCT  (*substructure for slave axis*)
		Command : slave_command_typ; (*command structure for slave axes*)
		Parameter : parameter_typ; (*parameter structure*)
		Status : slave_status_typ; (*status structure*)
		AxisState : axisState_typ; (*axis state structure*)
		Error : error_typ; (*error structure*)
	END_STRUCT;
	sourceMachine_typ : 	STRUCT 
		loaded : BOOL;
		warning : BOOL;
		decurlExit : BOOL;
		splice : BOOL;
		fastStop : BOOL;
		pause : BOOL;
		eject : BOOL;
		slowRequest : BOOL;
		stopRequest : BOOL;
		emStopPPU : BOOL;
		ready : BOOL;
		currentSpeed : REAL; (*mm/s*)
		speedLimit : REAL; (*mm/s*)
	END_STRUCT;
	sourcePrinter_typ : 	STRUCT 
		decurl : BOOL;
		blowerOn : BOOL;
		tensionControl : BOOL;
		wakeUp : BOOL;
		backward : BOOL;
		emStopPrinter : BOOL;
		disconnected : BOOL;
		currentSpeedLimit : REAL; (*mm/s*)
		printerSpeed : REAL; (*mm/s*)
	END_STRUCT;
	status_typ : 	STRUCT  (*status structure*)
		ActPosition : REAL; (*actual position of the axis*)
		ActVelocity : REAL; (*actual velocity of the axis*)
		DriveStatus : MC_DRIVESTATUS_TYP; (*actual status of the axis*)
	END_STRUCT;
	unwinderReading_typ : 	STRUCT 
		status : machineStatus_typ;
		rollDiameter : REAL; (*mm*)
	END_STRUCT;
	int_store_type : 	STRUCT 
		value : DINT;
		min : DINT;
		max : DINT;
	END_STRUCT;
	illum5Buttons_typ : 	STRUCT 
		lastPush1 : BOOL;
		lastPush2 : BOOL;
		lastPush3 : BOOL;
		lastPush4 : BOOL;
		lastPush5 : BOOL;
		push1 : BOOL;
		push2 : BOOL;
		push3 : BOOL;
		push4 : BOOL;
		push5 : BOOL;
		color1 : btnColor1_typ;
		color2 : btnColor1_typ;
		color3 : btnColor1_typ;
		color4 : btnColor1_typ;
		color5 : btnColor2_typ;
	END_STRUCT;
	illum3Buttons_typ : 	STRUCT 
		lastPush1 : BOOL;
		lastPush2 : BOOL;
		lastPush3 : BOOL;
		push1 : BOOL;
		push2 : BOOL;
		push3 : BOOL;
		color1 : btnColor1_typ;
		color2 : btnColor1_typ;
		color3 : btnColor2_typ;
	END_STRUCT;
	GUI_Typ : 	STRUCT 
		readings : GUIReadings;
		common : GUICommonSettings;
		settings : GUISettings;
		currentRECIPE : USINT;
		copyTo : USINT;
		copyFrom : USINT;
		copyRECIPE : BOOL := FALSE;
		maxRECIPE : USINT;
	END_STRUCT;
	axisState_typ : 	STRUCT  (*axis state structure*)
		Disabled : BOOL; (*if set, axis is in state Disabled*)
		StandStill : BOOL; (*if set, axis is in state StandsStill*)
		Homing : BOOL; (*if set, axis is in state Homing*)
		Stopping : BOOL; (*if set, axis is in state Stopping*)
		DiscreteMotion : BOOL; (*if set, axis is in state DiscreteMotion*)
		ContinuousMotion : BOOL; (*if set, axis is in state ContinuousMotion*)
		SynchronizedMotion : BOOL; (*if set, axis is in state SynchronizedMotion*)
		ErrorStop : BOOL; (*if set, axis is in state ErrorStop*)
	END_STRUCT;
	axis_command_typ : 	STRUCT  (*command structure for single and master axes*)
		Power : BOOL; (*switch on the controller*)
		Home : BOOL; (*reference axis*)
		MoveJogPos : BOOL; (*move in positive direction as long as is set*)
		MoveJogNeg : BOOL; (*move in negative direction as long as is set*)
		MoveAbsolute : BOOL; (*move to a defined position*)
		MoveAdditive : BOOL; (*move a defined distance*)
		MoveVelocity : BOOL; (*start a movement with a defined velocity*)
		Stop : BOOL; (*stop every active movement on the axis as long as is set*)
		Halt : BOOL; (*stop every active movement on the axis*)
	END_STRUCT;
	axis_typ : 	STRUCT  (*substructure for single and master axes*)
		Command : axis_command_typ; (*command structure for single and master axes*)
		Parameter : parameter_typ; (*parameter structure*)
		Status : status_typ; (*status structure*)
		AxisState : axisState_typ; (*axis state structure*)
		Error : error_typ; (*error structure*)
	END_STRUCT;
	bool_store_type : 	STRUCT 
		value : BOOL;
	END_STRUCT;
	btnColor1_typ : 	STRUCT 
		green : BOOL;
		yellow : BOOL;
		red : BOOL;
		white : BOOL;
	END_STRUCT;
	btnColor2_typ : 	STRUCT 
		green : BOOL;
		yellow : BOOL;
		red : BOOL;
		blue : BOOL;
	END_STRUCT;
	colorOfButton : 
		(
		_GREEN,
		_YELLOW,
		_RED,
		_BLUE,
		_WHITE,
		_ORANGE,
		_TURQUOISE,
		_MAGENTA,
		_OFF
		);
	error_typ : 	STRUCT  (*error structure *)
		AxisErrorCount : UINT; (*number of active axis errors*)
		AxisWarningCount : UINT; (*number of active axis warinings*)
		FunctionBlockErrorCount : UINT; (*number of active function block errors*)
		ErrorRecord : MC_ERRORRECORD_REF; (*error record information*)
		ErrorText : ARRAY[0..3]OF STRING[79]; (*description of first active error or warning*)
	END_STRUCT;
	frame_temp : 	STRUCT 
		ready : BOOL;
		temp_1_left : INT;
		temp_2_rear : INT;
		temp_3_front : INT;
		temp_4_right : INT;
		forceOff : BOOL := FALSE;
	END_STRUCT;
	gCmdSetup_typ : 	STRUCT 
		pulseLength : USINT;
		set : BOOL;
	END_STRUCT;
	station_typ : 	STRUCT 
		moduleID : ARRAY[0..13]OF UINT;
		station : ARRAY[0..13]OF BOOL := [14(FALSE)];
	END_STRUCT;
	gCmd_typ : 	STRUCT 
		home : gCmdSetup_typ;
	END_STRUCT;
	gFunction_typ : 	STRUCT 
		pulseLengthOFF : USINT;
		pulseLengthON : USINT;
		set : BOOL;
	END_STRUCT;
	global_error_typ : 	STRUCT  (*error structure for global errors*)
		AxisTyp : STRING[10]; (*axis type (Axis, Master or Slave)*)
		AxisIndex : USINT; (*index of error reporting axis (total axis index)*)
		ErrorText : ARRAY[0..3]OF STRING[79]; (*description of first active error or warning*)
		ErrorRecord : MC_ERRORRECORD_REF; (*error record information*)
	END_STRUCT;
	unwinder_typ : 	STRUCT 
		reading : unwinderReading_typ;
	END_STRUCT;
	global_input_command_typ : 	STRUCT 
		Power : BOOL; (*switch on the controller of all axes*)
		Home : BOOL; (*reference all axes*)
		ConnectSlavesToMaster : BOOL;
		DisconnectSlavesFromMaster : BOOL;
		MoveVelocity : BOOL; (*start a movement with a defined velocity*)
		StopMovement : BOOL;
		EnableCutMain : BOOL;
		EnableCutSecondary : BOOL;
		EnableCutPunch : BOOL;
		InitDataMain : BOOL;
		InitDataSecondary : BOOL;
		InitDataPunch : BOOL;
		EnableRegMarkControlMain : BOOL;
		EnableRegMarkControlSecondary : BOOL;
		EnableRegMarkControlPunch : BOOL;
		E_Stop : BOOL; (*stop everey active movement as long as is set (emergency stop)*)
		ErrorAcknowledge : BOOL; (*reset active errors*)
	END_STRUCT;
	global_input_typ : 	STRUCT 
		Command : global_input_command_typ;
		Parameter : global_input_parameter_typ;
	END_STRUCT;
	global_output_command_typ : 	STRUCT 
		Power : BOOL; (*switch on the controller of all axes*)
		Home : BOOL; (*reference all axes*)
		ConnectSlavesToMaster : BOOL;
		DisconnectSlavesFromMaster : BOOL;
		MoveVelocity : BOOL; (*start a movement with a defined velocity*)
		EnableCutMain : BOOL;
		EnableCutSecondary : BOOL;
		EnableCutPunch : BOOL;
		InitDataMain : BOOL;
		InitDataSecondary : BOOL;
		InitDataPunch : BOOL;
		EnableRegMarkControlMain : BOOL;
		EnableRegMarkControlSecondary : BOOL;
		EnableRegMarkControlPunch : BOOL;
		E_Stop : BOOL; (*stop everey active movement as long as is set (emergency stop)*)
		ErrorAcknowledge : BOOL; (*reset active errors*)
	END_STRUCT;
	global_dbg_typ : 	STRUCT 
		real2 : REAL;
		real3 : REAL;
		real4 : REAL;
		real1 : REAL;
	END_STRUCT;
	global_output_status_typ : 	STRUCT 
		AllDrivesEnable : BOOL; (*shows that the enable input is closed on all axes*)
		AllControllersReady : BOOL; (*shows that the controller is ready to be turned on on all axes*)
		AllControllersStatus : BOOL; (*shows that the controller is turned on on all axes*)
		AllHomingsOk : BOOL; (*shows that the axis is referenced on all axes*)
		NoHomeMovement : BOOL; (*shows that the axes are not doing a home movement*)
		AllSlavesSynchronizedMotion : BOOL; (*if set, all the slave axes are in state SynchronizedMotion*)
		AllSlavesNotSynchronizedMotion : BOOL;
	END_STRUCT;
	global_output_typ : 	STRUCT 
		Command : global_output_command_typ;
		Status : global_output_status_typ;
	END_STRUCT;
	global_typ : 	STRUCT  (*command structure for global commands*)
		Input : global_input_typ;
		Output : global_output_typ;
	END_STRUCT;
	grip_typ : 	STRUCT 
		cmd : gCmd_typ;
		setup : gSetup_typ;
	END_STRUCT;
	gSetup_typ : 	STRUCT 
		mechStep : gFunction_typ;
		tamper : gFunction_typ;
		twoUp : gFunction_typ;
	END_STRUCT;
	GUICommonSettings : 	STRUCT 
		B50_infeedStopPosition : INT := 450;
		B50_infeedBottomPosition : INT := 32000;
		B50_innerLoopTop : INT := 32390;
		B50_innerLoopBottom : INT := 4563;
	END_STRUCT;
	GUIReadings : 	STRUCT 
		B50_InfeedSpeed : REAL; (*m/min*)
		S50_stackSize : REAL; (*mm*)
		B50_LoopUsage : REAL; (*%*)
		S50_StepSensorFilt : REAL; (*mm*)
		S50_StepSensor : REAL; (*mm*)
		C50_CurrentSpeed : REAL; (*m/min*)
		S50_TopOfStackDistance : REAL; (*mm*)
		B50_InnerLoopPosition : INT; (*x*)
		B50_InfeedLoopPosition : INT; (*x*)
		X50_SpeedLimit : REAL; (*m/min*)
	END_STRUCT;
	GUISettings : 	STRUCT 
		B50_decurlTime : UINT; (*s*)
		B50_stopLevel : REAL; (*%*)
		B50_pauseLevel : REAL; (*%*)
		B50_slowSpeedLevel : REAL; (*%*)
		B50_maxInfeedSpeed : REAL; (*m/min*)
		S50_stepDownLevel : INT; (*x*)
		S50_tableSteps : REAL; (*mm*)
		S50_automaticTableSteps : USINT; (*off,mode1,mode2*)
		S50_fingerTopPosition : REAL; (*mm*)
		S50_topPosition : REAL; (*mm*)
		S50_overSpeedFactor : REAL; (*factor*)
		S50_backStopSpeed : REAL; (*m/min*)
		S50_deliverSpeed : REAL; (*m/min*)
		S50_deliverPosition : REAL; (*mm*)
		S50_minStackSize : REAL; (*mm*)
		S50_maxStackSize : REAL; (*mm*)
		S50_alignerMinSpeed : REAL; (*m/min*)
		S50_stopTime : REAL; (*s*)
		S50_sheets : USINT; (*number of sheets, 2 = slit/merge and one fold 3 = z fold*)
		S50_backStopOffset : REAL; (*mm*)
	END_STRUCT;
	guivars_typ : 	STRUCT 
		webTension : REAL;
	END_STRUCT;
	GUI_command_typ : 	STRUCT 
		confirmError : BOOL;
	END_STRUCT;
	GUI_status_typ : 	STRUCT 
		statusBarNotLoaded : UINT := 0;
		stopNextButton : UINT;
		statusBarStopped : UINT := 0;
		statusBarDrivesDisabled : UINT := 0;
		jogKnifeButton : UINT := 1;
		powerButton : UINT := 1;
		loadButton : UINT := 1;
		feedButton : UINT := 1;
		cutMinusButton : UINT := 1;
		cutPlusButton : UINT := 1;
		startButton : UINT := 1;
		statusBarInit : UINT := 0;
		reloadButton : UINT := 1;
		errorActive : UINT;
		configChanged : UINT := 0;
		skipToPrintButton : UINT := 1;
		markLED : UINT := 1;
		skipToPrintAmount : UINT := 1;
	END_STRUCT;
	global_input_parameter_typ : 	STRUCT 
		Velocity : REAL; (*velocity for MoveVelocity-Command*)
	END_STRUCT;
	stopControl_typ : 	STRUCT 
		OMRDeliverPos : REAL := 0;
		actualStopPos : REAL := 0;
		slowSpeedPos : REAL := 0;
		nextStopPos : REAL := 0;
		stopQueue : ARRAY[0..9]OF REAL;
		offsetPos : REAL := 0;
	END_STRUCT;
END_TYPE

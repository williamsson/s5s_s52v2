[LIT]
52
0	54	337	55	337
1	54	337	54	326
2	54	326	103	326
3	103	326	116	326
4	103	326	103	317
5	103	317	133	317
6	128	326	133	326
7	128	343	132	343
8	129	362	132	362
9	129	380	133	380
58	109	339	109	335
59	109	335	132	335
60	55	337	55	339
61	55	339	79	339
62	92	339	109	339
69	109	358	109	353
70	55	339	55	358
71	55	358	84	358
78	108	376	108	380
79	108	371	133	371
80	104	376	108	376
81	108	376	108	371
82	109	343	116	343
83	109	339	109	343
84	109	353	132	353
85	97	358	109	358
86	109	362	117	362
87	109	358	109	362
88	108	380	117	380
89	55	358	55	376
90	55	376	91	376
97	55	400	65	400
98	55	376	55	400
105	78	400	101	400
106	114	400	134	400
107	46	337	54	337
126	75	14	101	14
148	75	37	102	37
154	75	61	102	61
228	75	85	103	85
229	75	109	103	109
230	75	134	104	134
231	75	158	104	158
232	75	182	104	182
325	132	207	138	207
334	75	207	104	207
386	75	232	104	232
387	75	257	104	257
388	75	283	105	283
395	103	288	104	288
396	104	288	104	287
397	104	287	105	287
[TET]
115
10	62	342	79	344	4	5	SAFETIME#50ms
11	65	361	84	363	4	5	SAFETIME#250ms
12	71	379	91	381	4	5	SAFETIME#1000ms
13	133	325	153	327	4	3	E_Stop_state_Rev
14	133	316	148	318	4	3	E_Stop_state
15	132	334	153	336	4	3	E_Stop_state_50ms
16	132	352	154	354	4	3	E_Stop_state_250ms
17	133	370	156	372	4	3	E_Stop_state_1000ms
18	132	342	158	344	4	3	E_Stop_state_50ms_Rev
19	132	361	159	363	4	3	E_Stop_state_250ms_Rev
20	133	379	161	381	4	3	E_Stop_state_1000ms_Rev
21	81	403	101	405	4	5	SAFETIME#1000ms
22	45	403	65	405	4	5	SAFETIME#1050ms
23	134	399	147	401	4	3	ResetPulse
46	12	17	27	19	4	5	E_Stop_1_NC
47	12	88	27	90	4	5	E_Stop_4_NC
48	12	112	27	114	4	5	E_Stop_5_NC
49	12	137	27	139	4	5	E_Stop_6_NC
50	12	161	27	163	4	5	E_Stop_7_NC
51	12	185	27	187	4	5	E_Stop_8_NC
116	5	21	27	23	4	5	Reset_Global_E_Stop
117	10	25	27	27	4	5	SAFETIME#10ms
118	129	13	147	15	4	3	E_Stop_1_LOCAL
119	16	336	34	338	4	5	E_Stop_1_LOCAL
120	81	17	101	19	4	5	E_Stop_1_Present
155	82	40	102	42	4	5	E_Stop_2_Present
156	82	64	102	66	4	5	E_Stop_3_Present
157	12	40	27	42	4	5	E_Stop_2_NC
158	12	64	27	66	4	5	E_Stop_3_NC
159	5	44	27	46	4	5	Reset_Global_E_Stop
160	5	68	27	70	4	5	Reset_Global_E_Stop
161	10	48	27	50	4	5	SAFETIME#10ms
162	10	72	27	74	4	5	SAFETIME#10ms
233	5	92	27	94	4	5	Reset_Global_E_Stop
234	5	116	27	118	4	5	Reset_Global_E_Stop
235	5	141	27	143	4	5	Reset_Global_E_Stop
236	5	165	27	167	4	5	Reset_Global_E_Stop
237	5	189	27	191	4	5	Reset_Global_E_Stop
238	83	88	103	90	4	5	E_Stop_4_Present
239	83	112	103	114	4	5	E_Stop_5_Present
240	84	137	104	139	4	5	E_Stop_6_Present
241	84	161	104	163	4	5	E_Stop_7_Present
242	84	185	104	187	4	5	E_Stop_8_Present
243	10	96	27	98	4	5	SAFETIME#10ms
244	10	120	27	122	4	5	SAFETIME#10ms
245	10	145	27	147	4	5	SAFETIME#10ms
246	10	193	27	195	4	5	SAFETIME#10ms
247	130	36	148	38	4	3	E_Stop_2_LOCAL
248	130	60	148	62	4	3	E_Stop_3_LOCAL
249	131	84	149	86	4	3	E_Stop_4_LOCAL
250	131	108	149	110	4	3	E_Stop_5_LOCAL
251	132	133	150	135	4	3	E_Stop_6_LOCAL
252	132	157	150	159	4	3	E_Stop_7_LOCAL
253	132	181	150	183	4	3	E_Stop_8_LOCAL
254	16	340	34	342	4	5	E_Stop_2_LOCAL
255	16	344	34	346	4	5	E_Stop_3_LOCAL
256	16	348	34	350	4	5	E_Stop_4_LOCAL
257	16	352	34	354	4	5	E_Stop_5_LOCAL
258	16	356	34	358	4	5	E_Stop_6_LOCAL
259	16	360	34	362	4	5	E_Stop_7_LOCAL
260	16	364	34	366	4	5	E_Stop_8_LOCAL
280	73	422	87	424	4	3	StatusWord
281	35	422	53	424	4	5	E_Stop_1_LOCAL
282	35	426	53	428	4	5	E_Stop_2_LOCAL
283	35	430	53	432	4	5	E_Stop_3_LOCAL
284	35	434	53	436	4	5	E_Stop_4_LOCAL
285	35	438	53	440	4	5	E_Stop_5_LOCAL
286	35	442	53	444	4	5	E_Stop_6_LOCAL
287	35	446	53	448	4	5	E_Stop_7_LOCAL
288	35	450	53	452	4	5	E_Stop_8_LOCAL
289	33	454	53	456	4	5	E_Stop_1_Present
290	33	458	53	460	4	5	E_Stop_2_Present
291	33	462	53	464	4	5	E_Stop_3_Present
292	33	466	53	468	4	5	E_Stop_4_Present
293	33	470	53	472	4	5	E_Stop_5_Present
294	33	474	53	476	4	5	E_Stop_6_Present
295	33	478	53	480	4	5	E_Stop_7_Present
296	33	482	53	484	4	5	E_Stop_8_Present
297	16	13	27	15	4	5	SAFETRUE
298	16	36	27	38	4	5	SAFETRUE
299	16	60	27	62	4	5	SAFETRUE
300	16	84	27	86	4	5	SAFETRUE
301	16	108	27	110	4	5	SAFETRUE
302	16	133	27	135	4	5	SAFETRUE
303	16	157	27	159	4	5	SAFETRUE
304	16	181	27	183	4	5	SAFETRUE
305	10	169	27	171	4	5	SAFETIME#25ms
314	16	206	27	208	4	5	SAFETRUE
315	12	210	27	212	4	5	E_Stop_9_NC
316	5	214	27	216	4	5	Reset_Global_E_Stop
317	10	218	27	220	4	5	SAFETIME#10ms
323	84	210	104	212	4	5	E_Stop_9_Present
324	138	206	156	208	4	3	E_Stop_9_LOCAL
330	16	368	34	370	4	5	E_Stop_9_LOCAL
331	15	372	34	374	4	5	E_Stop_10_LOCAL
332	15	376	34	378	4	5	E_Stop_11_LOCAL
333	15	380	34	382	4	5	E_Stop_12_LOCAL
343	16	231	27	233	4	5	SAFETRUE
375	10	243	27	245	4	5	SAFETIME#10ms
376	10	268	27	270	4	5	SAFETIME#10ms
377	10	294	27	296	4	5	SAFETIME#10ms
378	11	235	27	237	4	5	E_Stop_10_NC
379	11	260	27	262	4	5	E_Stop_11_NC
380	11	286	27	288	4	5	E_Stop_12_NC
381	16	256	27	258	4	5	SAFETRUE
382	16	282	27	284	4	5	SAFETRUE
383	5	239	27	241	4	5	Reset_Global_E_Stop
384	5	264	27	266	4	5	Reset_Global_E_Stop
385	5	290	27	292	4	5	Reset_Global_E_Stop
389	83	235	104	237	4	5	E_Stop_10_Present
390	83	260	104	262	4	5	E_Stop_11_Present
391	82	287	103	289	4	5	E_Stop_12_Present
392	132	231	151	233	4	3	E_Stop_10_LOCAL
393	132	256	151	258	4	3	E_Stop_11_LOCAL
394	133	282	152	284	4	3	E_Stop_12_LOCAL
[FBS]
35
41	36	333	44	385	1	AND_S	
42	118	339	126	347	1	NOT_S	
43	119	358	127	366	1	NOT_S	
44	119	376	127	384	1	NOT_S	
45	118	322	126	330	1	NOT_S	
56	81	335	90	347	0	TOF_S	TOF_S_1
67	86	354	95	366	0	TON_S	TON_S_2
76	93	372	102	384	0	TOF_S	TOF_S_3
95	67	396	76	408	0	TON_S	TON_S_4
103	103	396	112	408	0	TP_S	TP_S_1
114	29	10	73	30	0	E_STOP	E_STOP_1
124	103	10	127	22	0	UP_ESTOP	UP_ESTOP_1
133	29	33	73	53	0	E_STOP	E_STOP_2
141	29	57	73	77	0	E_STOP	E_STOP_3
146	104	33	128	45	0	UP_ESTOP	UP_ESTOP_2
152	104	57	128	69	0	UP_ESTOP	UP_ESTOP_3
169	29	81	73	101	0	E_STOP	E_STOP_4
177	29	105	73	125	0	E_STOP	E_STOP_5
185	29	130	73	150	0	E_STOP	E_STOP_6
193	29	154	73	174	0	E_STOP	E_STOP_7
201	29	178	73	198	0	E_STOP	E_STOP_8
206	105	81	129	93	0	UP_ESTOP	UP_ESTOP_4
211	105	105	129	117	0	UP_ESTOP	UP_ESTOP_5
216	106	130	130	142	0	UP_ESTOP	UP_ESTOP_6
221	106	154	130	166	0	UP_ESTOP	UP_ESTOP_7
226	106	178	130	190	0	UP_ESTOP	UP_ESTOP_8
278	55	419	71	487	0	BITS_TO_WORD	BITS_TO_WORD_1
312	29	203	73	223	0	E_STOP	E_STOP_9
321	106	203	130	215	0	UP_ESTOP	UP_ESTOP_9
341	29	228	73	248	0	E_STOP	E_STOP_10
350	29	253	73	273	0	E_STOP	E_STOP_11
358	29	279	73	299	0	E_STOP	E_STOP_12
363	106	228	130	240	0	UP_ESTOP	UP_ESTOP_10
368	106	253	130	265	0	UP_ESTOP	UP_ESTOP_11
373	107	279	131	291	0	UP_ESTOP	UP_ESTOP_12
[FPT]
166
24	36	336	38	338		0	1665	0	ANY_SAFEBIT	
25	36	340	38	342		0	1665	0	ANY_SAFEBIT	
26	43	336	44	338		1	0	643	ANY_SAFEBIT	
27	36	344	38	346		0	1665	0	ANY_SAFEBIT	
28	36	348	38	350		0	1665	0	ANY_SAFEBIT	
29	36	352	38	354		0	1665	0	ANY_SAFEBIT	
30	36	356	38	358		0	1665	0	ANY_SAFEBIT	
31	36	360	38	362		0	1665	0	ANY_SAFEBIT	
32	36	364	38	366		0	1665	0	ANY_SAFEBIT	
33	118	342	120	344		0	641	0	ANY_SAFEBIT	
34	125	342	126	344		1	0	641	ANY_SAFEBIT	
35	119	361	121	363		0	641	0	ANY_SAFEBIT	
36	126	361	127	363		1	0	641	ANY_SAFEBIT	
37	119	379	121	381		0	641	0	ANY_SAFEBIT	
38	126	379	127	381		1	0	641	ANY_SAFEBIT	
39	118	325	120	327		0	641	0	ANY_SAFEBIT	
40	125	325	126	327		1	0	641	ANY_SAFEBIT	
52	81	338	86	340	IN	0	129	0	SAFEBOOL	
53	81	342	86	344	PT	0	128	0	SAFETIME	
54	87	338	90	340	Q	1	0	129	SAFEBOOL	
55	86	342	90	344	ET	1	0	128	SAFETIME	
63	86	357	91	359	IN	0	129	0	SAFEBOOL	
64	86	361	91	363	PT	0	128	0	SAFETIME	
65	92	357	95	359	Q	1	0	129	SAFEBOOL	
66	91	361	95	363	ET	1	0	128	SAFETIME	
72	93	375	98	377	IN	0	129	0	SAFEBOOL	
73	93	379	98	381	PT	0	128	0	SAFETIME	
74	99	375	102	377	Q	1	0	129	SAFEBOOL	
75	98	379	102	381	ET	1	0	128	SAFETIME	
91	67	399	72	401	IN	0	129	0	SAFEBOOL	
92	67	403	72	405	PT	0	128	0	SAFETIME	
93	73	399	76	401	Q	1	0	129	SAFEBOOL	
94	72	403	76	405	ET	1	0	128	SAFETIME	
99	103	399	108	401	IN	0	129	0	SAFEBOOL	
100	103	403	108	405	PT	0	128	0	SAFETIME	
101	109	399	112	401	Q	1	0	129	SAFEBOOL	
102	108	403	112	405	ET	1	0	128	SAFETIME	
108	29	13	46	15	ReverseOutput	0	129	0	SAFEBOOL	
109	29	17	47	19	E_Stop_input_NC	0	129	0	SAFEBOOL	
110	29	21	44	23	reset_E_Stop	0	129	0	BOOL	
111	29	25	43	27	OutputDelay	0	128	0	SAFETIME	
112	47	13	73	15	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
113	49	17	73	19	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
121	103	13	111	15	Input	0	129	0	SAFEBOOL	
122	103	17	119	19	Unit_Pressent	0	129	0	BOOL	
123	119	13	127	15	Output	1	0	129	SAFEBOOL	
127	29	36	46	38	ReverseOutput	0	129	0	SAFEBOOL	
128	29	40	47	42	E_Stop_input_NC	0	129	0	SAFEBOOL	
129	29	44	44	46	reset_E_Stop	0	129	0	BOOL	
130	29	48	43	50	OutputDelay	0	128	0	SAFETIME	
131	47	36	73	38	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
132	49	40	73	42	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
135	29	60	46	62	ReverseOutput	0	129	0	SAFEBOOL	
136	29	64	47	66	E_Stop_input_NC	0	129	0	SAFEBOOL	
137	29	68	44	70	reset_E_Stop	0	129	0	BOOL	
138	29	72	43	74	OutputDelay	0	128	0	SAFETIME	
139	47	60	73	62	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
140	49	64	73	66	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
143	104	36	112	38	Input	0	129	0	SAFEBOOL	
144	104	40	120	42	Unit_Pressent	0	129	0	BOOL	
145	120	36	128	38	Output	1	0	129	SAFEBOOL	
149	104	60	112	62	Input	0	129	0	SAFEBOOL	
150	104	64	120	66	Unit_Pressent	0	129	0	BOOL	
151	120	60	128	62	Output	1	0	129	SAFEBOOL	
163	29	84	46	86	ReverseOutput	0	129	0	SAFEBOOL	
164	29	88	47	90	E_Stop_input_NC	0	129	0	SAFEBOOL	
165	29	92	44	94	reset_E_Stop	0	129	0	BOOL	
166	29	96	43	98	OutputDelay	0	128	0	SAFETIME	
167	47	84	73	86	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
168	49	88	73	90	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
171	29	108	46	110	ReverseOutput	0	129	0	SAFEBOOL	
172	29	112	47	114	E_Stop_input_NC	0	129	0	SAFEBOOL	
173	29	116	44	118	reset_E_Stop	0	129	0	BOOL	
174	29	120	43	122	OutputDelay	0	128	0	SAFETIME	
175	47	108	73	110	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
176	49	112	73	114	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
179	29	133	46	135	ReverseOutput	0	129	0	SAFEBOOL	
180	29	137	47	139	E_Stop_input_NC	0	129	0	SAFEBOOL	
181	29	141	44	143	reset_E_Stop	0	129	0	BOOL	
182	29	145	43	147	OutputDelay	0	128	0	SAFETIME	
183	47	133	73	135	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
184	49	137	73	139	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
187	29	157	46	159	ReverseOutput	0	129	0	SAFEBOOL	
188	29	161	47	163	E_Stop_input_NC	0	129	0	SAFEBOOL	
189	29	165	44	167	reset_E_Stop	0	129	0	BOOL	
190	29	169	43	171	OutputDelay	0	128	0	SAFETIME	
191	47	157	73	159	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
192	49	161	73	163	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
195	29	181	46	183	ReverseOutput	0	129	0	SAFEBOOL	
196	29	185	47	187	E_Stop_input_NC	0	129	0	SAFEBOOL	
197	29	189	44	191	reset_E_Stop	0	129	0	BOOL	
198	29	193	43	195	OutputDelay	0	128	0	SAFETIME	
199	47	181	73	183	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
200	49	185	73	187	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
203	105	84	113	86	Input	0	129	0	SAFEBOOL	
204	105	88	121	90	Unit_Pressent	0	129	0	BOOL	
205	121	84	129	86	Output	1	0	129	SAFEBOOL	
208	105	108	113	110	Input	0	129	0	SAFEBOOL	
209	105	112	121	114	Unit_Pressent	0	129	0	BOOL	
210	121	108	129	110	Output	1	0	129	SAFEBOOL	
213	106	133	114	135	Input	0	129	0	SAFEBOOL	
214	106	137	122	139	Unit_Pressent	0	129	0	BOOL	
215	122	133	130	135	Output	1	0	129	SAFEBOOL	
218	106	157	114	159	Input	0	129	0	SAFEBOOL	
219	106	161	122	163	Unit_Pressent	0	129	0	BOOL	
220	122	157	130	159	Output	1	0	129	SAFEBOOL	
223	106	181	114	183	Input	0	129	0	SAFEBOOL	
224	106	185	122	187	Unit_Pressent	0	129	0	BOOL	
225	122	181	130	183	Output	1	0	129	SAFEBOOL	
261	55	422	62	424	IN_0	0	128	0	BOOL	
262	55	426	62	428	IN_1	0	128	0	BOOL	
263	55	430	62	432	IN_2	0	128	0	BOOL	
264	55	434	62	436	IN_3	0	128	0	BOOL	
265	55	438	62	440	IN_4	0	128	0	BOOL	
266	55	442	62	444	IN_5	0	128	0	BOOL	
267	55	446	62	448	IN_6	0	128	0	BOOL	
268	55	450	62	452	IN_7	0	128	0	BOOL	
269	55	454	62	456	IN_8	0	128	0	BOOL	
270	55	458	62	460	IN_9	0	128	0	BOOL	
271	55	462	63	464	IN_10	0	128	0	BOOL	
272	55	466	63	468	IN_11	0	128	0	BOOL	
273	55	470	63	472	IN_12	0	128	0	BOOL	
274	55	474	63	476	IN_13	0	128	0	BOOL	
275	55	478	63	480	IN_14	0	128	0	BOOL	
276	55	482	63	484	IN_15	0	128	0	BOOL	
277	66	422	71	424	OUT	1	0	128	WORD	
306	29	206	46	208	ReverseOutput	0	129	0	SAFEBOOL	
307	29	210	47	212	E_Stop_input_NC	0	129	0	SAFEBOOL	
308	29	214	44	216	reset_E_Stop	0	129	0	BOOL	
309	29	218	43	220	OutputDelay	0	128	0	SAFETIME	
310	47	206	73	208	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
311	49	210	73	212	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
318	106	206	114	208	Input	0	129	0	SAFEBOOL	
319	106	210	122	212	Unit_Pressent	0	129	0	BOOL	
320	122	206	130	208	Output	1	0	129	SAFEBOOL	
326	36	368	38	370		0	1665	0	ANY_SAFEBIT	
327	36	372	38	374		0	1665	0	ANY_SAFEBIT	
328	36	376	38	378		0	1665	0	ANY_SAFEBIT	
329	36	380	38	382		0	1665	0	ANY_SAFEBIT	
335	29	231	46	233	ReverseOutput	0	129	0	SAFEBOOL	
336	29	235	47	237	E_Stop_input_NC	0	129	0	SAFEBOOL	
337	29	239	44	241	reset_E_Stop	0	129	0	BOOL	
338	29	243	43	245	OutputDelay	0	128	0	SAFETIME	
339	47	231	73	233	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
340	49	235	73	237	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
344	29	256	46	258	ReverseOutput	0	129	0	SAFEBOOL	
345	29	260	47	262	E_Stop_input_NC	0	129	0	SAFEBOOL	
346	29	264	44	266	reset_E_Stop	0	129	0	BOOL	
347	29	268	43	270	OutputDelay	0	128	0	SAFETIME	
348	47	256	73	258	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
349	49	260	73	262	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
352	29	282	46	284	ReverseOutput	0	129	0	SAFEBOOL	
353	29	286	47	288	E_Stop_input_NC	0	129	0	SAFEBOOL	
354	29	290	44	292	reset_E_Stop	0	129	0	BOOL	
355	29	294	43	296	OutputDelay	0	128	0	SAFETIME	
356	47	282	73	284	E_Stop_Active_Immediate	1	0	129	SAFEBOOL	
357	49	286	73	288	E_Stop_Active_Delayed	1	0	129	SAFEBOOL	
360	106	231	114	233	Input	0	129	0	SAFEBOOL	
361	106	235	122	237	Unit_Pressent	0	129	0	BOOL	
362	122	231	130	233	Output	1	0	129	SAFEBOOL	
365	106	256	114	258	Input	0	129	0	SAFEBOOL	
366	106	260	122	262	Unit_Pressent	0	129	0	BOOL	
367	122	256	130	258	Output	1	0	129	SAFEBOOL	
370	107	282	115	284	Input	0	129	0	SAFEBOOL	
371	107	286	123	288	Unit_Pressent	0	129	0	BOOL	
372	123	282	131	284	Output	1	0	129	SAFEBOOL	
[KOT]
0
[VER]
0

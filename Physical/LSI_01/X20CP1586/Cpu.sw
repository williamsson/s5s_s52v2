﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.5.60 SP?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="MainLogic" Source="MainLogic.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="AxisContro" Source="Motion.AxisControl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="PowerSuppl" Source="Motion.PowerSup.gAxisPower.PowerSupply.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="VirtualMas" Source="Motion.VirtAxis.gAxVirtMaobj.VirtualMaster.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="MergeInfee" Source="Motion.InfeedAxis.gAxisMergeInfeed.MergeInfeed.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="MainInfeed" Source="Motion.InfeedAxis.gAxisMainInfeed.MainInfeed.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="MainKnife" Source="Motion.CutterAxis.CutterMainKnife.gAxisCutterMainKnife.MainKnife.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="StripKnife" Source="Motion.CutterAxis.CutterStripKnife.gAxisCutterStripKnife.StripKnife.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Punch" Source="Motion.CutterAxis.Punch.gAxisPunch.Punch.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Support" Source="Motion.InfeedAxis.gAxisSupp.Support.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Reject" Source="Motion.InfeedAxis.gAxisReject.Reject.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Main" Source="Main.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="UDPLog" Source="UDPLog.UDPLog.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="TjxPlowfol" Source="TjxPlowfolder.TjxPlowfolder.prg" Memory="UserROM" Language="ANSIC" Debugging="true" Disabled="true" />
    <Task Name="PIF" Source="PIF.PIF.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="AxisContr1" Source="Stacker.Motion.AxisControl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="AxisContr2" Source="Buffer.Motion.AxisControlBuffer.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Basic" Source="Stacker.LibACP10MC_AlignerTransport.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Basic7" Source="Stacker.LibACP10MC_Spaghetti.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="BufOut" Source="Motion.InfeedAxis.gAxisBufOut.BufOut.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="PlowIn" Source="Motion.InfeedAxis.gAxisPlowIn.PlowIn.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Stream" Source="Stream.Stream.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrSec1Bas" Source="Stream.gAxStrSt1obj.LibACP10MC_SingleAx_C.StrSec1Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrSec2Bas" Source="Stream.gAxStrSt2obj.LibACP10MC_SingleAx_C.StrSec2Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrFoPuBas" Source="Stream.gAxStrFPUobj.LibACP10MC_SingleAx_C.StrFoPuBasic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrFoLiBas" Source="Stream.gAxStrFLIobj.LibACP10MC_SingleAx_C.StrFoLiBasic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrAliBasi" Source="Stream.gAxStrAliobj.LibACP10MC_SingleAx_C.StrAliBasic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrStLiBas" Source="Stream.gAxStrSLIobj.LibACP10MC_SingleAx_C.StrStLiBasic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrSpaBasi" Source="Stream.gAxStrSPAobj.LibACP10MC_SingleAx_C.StrSpaBasic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrJog1Bas" Source="Stream.gAxStrJ1obj.LibACP10MC_SingleAx_C.StrJog1Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrJog2Bas" Source="Stream.gAxStrJ2obj.LibACP10MC_SingleAx_C.StrJog2Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrDelBasi" Source="Stream.gAxStrDELobj.LibACP10MC_SingleAx_C.StrDelBasic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="StrJog3Bas" Source="Stream.gAxStrJ3obj.LibACP10MC_SingleAx_C.StrJog3Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="HomeSetupS" Source="Stream.HomeSetupStreamer.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="PlowOut" Source="Motion.InfeedAxis.gAxisPlowOut.PlowOut.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Program1" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <Task Name="Basic8" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2">
    <Task Name="Basic1" Source="Stacker.LibACP10MC_StackerLift.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Basic2" Source="Buffer.LibACP10MC_Infeed.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Basic3" Source="Stacker.LibACP10MC_FingerPush.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Basic4" Source="Stacker.LibACP10MC_FingerLift.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Basic5" Source="Stacker.LibACP10MC_StopPlate.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Basic6" Source="Stacker.LibACP10MC_DeliverTransport.Basic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Program" Source="Stacker.Program.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="BasicPR" Source="FolderAutomation.gAxP_Robj.BasicPR.BasicPR.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="BasicPLV2" Source="FolderAutomation.gAxPLV_2obj.BasicPLV2.BasicPLV2.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="BasicPL" Source="FolderAutomation.gAxP_Lobj.BasicPL.BasicPL.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="BasicPLH1" Source="FolderAutomation.gAxPLH_1obj.BasicPLH1.BasicPLH1.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="BasicPLV1" Source="FolderAutomation.gAxPLV_1obj.BasicPLV1.BasicPLV1.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="BasicPLH2" Source="FolderAutomation.gAPLH_2obj.BasicPLH2.BasicPLH2.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="FolderAuto" Source="FolderAutomation.FolderAuto.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#3">
    <Task Name="Buffer" Source="Buffer.Buffer.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="FolderMain" Source="FolderMain.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="u20IF" Source="u20IF.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#4">
    <Task Name="Hot_hot" Source="Hot_hot.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="LineContro" Source="LineController.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="IOtest" Source="IOtest.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Safety" Source="Safety.Safety.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="IOT" Source="IOT.IOT.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Statistics" Source="Statistics.Statistics.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5">
    <Task Name="configFile" Source="configFilesHandling.configFilesHandling.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#6">
    <Task Name="EBMcom" Source="EBMCom.EBMcom.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8" />
  <DataObjects>
    <DataObject Name="Acp10sys" Source="" Memory="UserROM" Language="Binary" />
    <DataObject Name="assl1" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <NcDataObjects>
    <NcDataObject Name="acp10etxen" Source="Motion.Misc.acp10etxen.dob" Memory="UserROM" Language="Ett" />
    <NcDataObject Name="gAxTra" Source="Stacker.Motion.AxisObjects.gAxTransport.gAxTra.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxTri" Source="Stacker.Motion.AxisObjects.gAxTransport.gAxTri.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxFiPua" Source="Stacker.Motion.AxisObjects.gAxFingerPush.gAxFiPua.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxFiPui" Source="Stacker.Motion.AxisObjects.gAxFingerPush.gAxFiPui.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxFiLia" Source="Stacker.Motion.AxisObjects.gAxFingerLift.gAxFiLia.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxFiLii" Source="Stacker.Motion.AxisObjects.gAxFingerLift.gAxFiLii.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStPla" Source="Stacker.Motion.AxisObjects.gAxStopPlate.gAxStPla.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStPli" Source="Stacker.Motion.AxisObjects.gAxStopPlate.gAxStPli.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStDea" Source="Stacker.Motion.AxisObjects.gAxStackerDelivery.gAxStDea.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStDei" Source="Stacker.Motion.AxisObjects.gAxStackerDelivery.gAxStDei.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStLia" Source="Stacker.Motion.AxisObjects.gAxStackerLift.gAxStLia.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStLii" Source="Stacker.Motion.AxisObjects.gAxStackerLift.gAxStLii.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxBuf1a" Source="Motion.InfeedAxis.gAxisBufOut.gAxBuf1a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxBuf1i" Source="Motion.InfeedAxis.gAxisBufOut.gAxBuf1i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxBuf2a" Source="Buffer.Motion.AxisObjects.gAxBuf1obj.gAxBuf2a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxBuf2i" Source="Buffer.Motion.AxisObjects.gAxBuf1obj.gAxBuf2i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxPowera" Source="Motion.PowerSup.gAxisPower.gAxPowera.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPoweri" Source="Motion.PowerSup.gAxisPower.gAxPoweri.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxVirtMaa" Source="Motion.VirtAxis.gAxVirtMaobj.gAxVirtMaa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxVirtMai" Source="Motion.VirtAxis.gAxVirtMaobj.gAxVirtMai.dob" Memory="UserROM" Language="Vax" />
    <NcDataObject Name="gAxSuppa" Source="Motion.InfeedAxis.gAxisSupp.C50beta.gAxSuppa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxSuppi" Source="Motion.InfeedAxis.gAxisSupp.C50beta.gAxSuppi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxCMaKnif" Source="Motion.CutterAxis.CutterMainKnife.gAxisCutterMainKnife.C50beta.gAxCMaKnifea.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxCMaKni1" Source="Motion.CutterAxis.CutterMainKnife.gAxisCutterMainKnife.C50beta.gAxCMaKnifei.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxCStKnif" Source="Motion.CutterAxis.CutterStripKnife.gAxisCutterStripKnife.C50beta.gAxCStKnifea.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxCStKni1" Source="Motion.CutterAxis.CutterStripKnife.gAxisCutterStripKnife.C50beta.gAxCStKnifei.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxMaInfa" Source="Motion.InfeedAxis.gAxisMainInfeed.C50beta.gAxMaInfa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxMaInfi" Source="Motion.InfeedAxis.gAxisMainInfeed.C50beta.gAxMaInfi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxPFOuta" Source="Motion.InfeedAxis.gAxisPlowOut.gAxPFOuta.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPFOuti" Source="Motion.InfeedAxis.gAxisPlowOut.gAxPFOuti.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxPFIna" Source="Motion.InfeedAxis.gAxisPlowIn.gAxPFIna.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPFIni" Source="Motion.InfeedAxis.gAxisPlowIn.gAxPFIni.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxReja" Source="Motion.InfeedAxis.gAxisReject.C50beta.gAxReja.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxReji" Source="Motion.InfeedAxis.gAxisReject.C50beta.gAxReji.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxPuncha" Source="Motion.CutterAxis.Punch.gAxisPunch.C50beta.gAxPuncha.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPunchi" Source="Motion.CutterAxis.Punch.gAxisPunch.C50beta.gAxPunchi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxMeInfa" Source="Motion.InfeedAxis.gAxisMergeInfeed.C50beta.gAxMeInfa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxMeInfi" Source="Motion.InfeedAxis.gAxisMergeInfeed.C50beta.gAxMeInfi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis02a" Source="Stacker.Motion.AxisObjects.gAxis02obj.gAxis02a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis02i" Source="Stacker.Motion.AxisObjects.gAxis02obj.gAxis02i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis03a" Source="Stacker.Motion.AxisObjects.gAxis03obj.gAxis03a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis03i" Source="Stacker.Motion.AxisObjects.gAxis03obj.gAxis03i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxSpagha" Source="Stacker.Motion.AxisObjects.gAxSpaghobj.gAxSpagha.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxSpaghi" Source="Stacker.Motion.AxisObjects.gAxSpaghobj.gAxSpaghi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrAlia" Source="Stream.gAxStrAliobj.gAxStrAlia.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrAlii" Source="Stream.gAxStrAliobj.gAxStrAlii.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrSt1a" Source="Stream.gAxStrSt1obj.gAxStrSt1a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrSt1i" Source="Stream.gAxStrSt1obj.gAxStrSt1i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrSt2a" Source="Stream.gAxStrSt2obj.gAxStrSt2a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrSt2i" Source="Stream.gAxStrSt2obj.gAxStrSt2i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrFLIa" Source="Stream.gAxStrFLIobj.gAxStrFLIa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrFLIi" Source="Stream.gAxStrFLIobj.gAxStrFLIi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrFPUa" Source="Stream.gAxStrFPUobj.gAxStrFPUa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrFPUi" Source="Stream.gAxStrFPUobj.gAxStrFPUi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrDELa" Source="Stream.gAxStrDELobj.gAxStrDELa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrDELi" Source="Stream.gAxStrDELobj.gAxStrDELi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrSLIa" Source="Stream.gAxStrSLIobj.gAxStrSLIa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrSLIi" Source="Stream.gAxStrSLIobj.gAxStrSLIi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrSPAa" Source="Stream.gAxStrSPAobj.gAxStrSPAa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrSPAi" Source="Stream.gAxStrSPAobj.gAxStrSPAi.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrJ1a" Source="Stream.gAxStrJ1obj.gAxStrJ1a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrJ1i" Source="Stream.gAxStrJ1obj.gAxStrJ1i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrJ2a" Source="Stream.gAxStrJ2obj.gAxStrJ2a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrJ2i" Source="Stream.gAxStrJ2obj.gAxStrJ2i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxStrJ3a" Source="Stream.gAxStrJ3obj.gAxStrJ3a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxStrJ3i" Source="Stream.gAxStrJ3obj.gAxStrJ3i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAPLH_2a" Source="FolderAutomation.gAPLH_2obj.gAPLH_2a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAPLH_2i" Source="FolderAutomation.gAPLH_2obj.gAPLH_2i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxP_Ra" Source="FolderAutomation.gAxP_Robj.gAxP_Ra.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxP_Ri" Source="FolderAutomation.gAxP_Robj.gAxP_Ri.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxPLV_2a" Source="FolderAutomation.gAxPLV_2obj.gAxPLV_2a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPLV_2i" Source="FolderAutomation.gAxPLV_2obj.gAxPLV_2i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxP_La" Source="FolderAutomation.gAxP_Lobj.gAxP_La.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxP_Li" Source="FolderAutomation.gAxP_Lobj.gAxP_Li.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxPLH_1a" Source="FolderAutomation.gAxPLH_1obj.gAxPLH_1a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPLH_1i" Source="FolderAutomation.gAxPLH_1obj.gAxPLH_1i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxPLV_1a" Source="FolderAutomation.gAxPLV_1obj.gAxPLV_1a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxPLV_1i" Source="FolderAutomation.gAxPLV_1obj.gAxPLV_1i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gABufIna" Source="Buffer.Motion.AxisObjects.gABufInobj.gABufIna.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gABufIni" Source="Buffer.Motion.AxisObjects.gABufInobj.gABufIni.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gABufOuta" Source="Motion.InfeedAxis.gABufOutobj.gABufOuta.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gABufOuti" Source="Motion.InfeedAxis.gABufOutobj.gABufOuti.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxCutter1" Source="" Memory="UserROM" Language="Binary" />
  </NcDataObjects>
  <VcDataObjects>
    <VcDataObject Name="Visune" Source="Visunew.dob" Memory="UserROM" Language="Vc" WarningLevel="2" />
  </VcDataObjects>
  <Binaries>
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="vccbmp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccline" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfile" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arial" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcrt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcbclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbtn" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcmgr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsint" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcnet" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfx20" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcshared" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arialbd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccnum" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdvnc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctext" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdsw" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfntttf" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpkat" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccovl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsloc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vctcal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchspot" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="consolab" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="consola" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccddbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccpopup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccshape" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccstr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visune02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visune03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visune01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcclbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asiol2" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="OpcUaMap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asiol1" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="acp10cfg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Acp10map" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcctrend" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbar" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchtml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccurl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcxml" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Visu02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccdt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccslider" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="MainVi02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="MainVi03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="MainVi01" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="brsystem" Source="MainLogic.Libraries.brsystem.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="MainLogic.Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="MainLogic.Libraries.runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="FileIO" Source="MainLogic.Libraries.FileIO.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsXml" Source="MainLogic.Libraries.AsXml.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="standard" Source="MainLogic.Libraries.standard.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="astime" Source="MainLogic.Libraries.astime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10man" Source="Acp10man.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="NcGlobal" Source="NcGlobal.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10par" Source="Acp10par.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="MainLogic.Libraries.AsBrStr.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="operator" Source="MainLogic.Libraries.operator.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIecCon" Source="MainLogic.Libraries.AsIecCon.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10_MC" Source="Acp10_MC.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MC_RegMa" Source="MC_RegMa.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsMath" Source="MainLogic.Libraries.AsMath.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10sdc" Source="Acp10sdc.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsArLog" Source="MainLogic.Libraries.AsArLog.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Library" Source="Library.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="powerlnk" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="vcresman" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="ashw" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="dataobj" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asudp" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>